﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Views;
using QuickSal.DataAcces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using static QuickSal.DataAcces.QuickSalEnum;
using System.Threading.Tasks;

namespace QuickSal.Services
{
    public class BusinessService : IBusinessService
    {
        private IServiceError serviceError;
        private IBusinessRepository businessRepository;
        private IBranchOfficeRepository branchOfficeRepository;
        private ISubscriptionsRepository subscriptionsRepository;
        private IPrivilegeRepository privilegeRepository;
        private ISecurityRepository securityRepository;
        private IAccountService accountingService;
        private IGlobalService globalService;
        private ISecurityService securityService;
        private IPlanService planService;
        private IAmazonService amazonService;


        public BusinessService(IServiceError serviceError, IPlanService planService, IAmazonService amazonService,
                               IBusinessRepository businessRepository, IBranchOfficeRepository BranchOfficeRepository,
                               ISubscriptionsRepository subscriptionsRepository, IPrivilegeRepository privilegeRepository,
                               ISecurityRepository securityRepository, IAccountService accountingService,
                               IGlobalService globalService, ISecurityService securityService)
        {
            this.serviceError = serviceError;
            this.businessRepository = businessRepository;
            this.branchOfficeRepository = BranchOfficeRepository;
            this.subscriptionsRepository = subscriptionsRepository;
            this.privilegeRepository = privilegeRepository;
            this.securityRepository = securityRepository;
            this.accountingService = accountingService;
            this.globalService = globalService;
            this.securityService = securityService;
            this.planService = planService;
            this.amazonService = amazonService;
        }

        //============================================================================Seccion para la empresa
        #region Empresa
        /// <summary>
        /// Proceso para registrar una empresa en el sistema
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<TaskResult> CreateBusiness(TblBusiness entity, short planId)
        {
            try
            {
                var task = await BusinessValidateCreate(entity);

                if (task.ExecutedSuccesfully)
                {
                    var sesion = securityService.GetSession();
                    var user = accountingService.GetUser(sesion.UserId);


                    //empresa
                    entity.User = user;
                    entity.CountryId = entity.CountryId;
                    entity.CreateDate = DateTime.Now;
                    entity.ModifyDate = DateTime.Now;
                    entity.Estatus = true;

                    var tempPicture = globalService.GetTempFiles(user.Id, (byte)TypeFiles.BusinessLogo);

                    //Movemos la foto a su carpeta final
                    if (tempPicture.Count() > 0 && !string.IsNullOrEmpty(entity.Logo))
                    {
                        entity.Logo = tempPicture.LastOrDefault().FileName;
                        await globalService.MoveFile(entity.Logo, TypeFiles.BusinessLogo);
                    }

                    //Removemos las fotos temporales que tenga el usuario
                    if (tempPicture.Count() > 0)
                        globalService.DeleteRangeTempFile(tempPicture);


                    //Sucursal              
                    TblBranchOffice branchOffice = new TblBranchOffice();
                    entity.TblBranchOffice.FirstOrDefault().BranchOfficeNumber = 1;
                    entity.TblBranchOffice.FirstOrDefault().CreateDate = DateTime.Now;
                    entity.TblBranchOffice.FirstOrDefault().Estatus = true;
                    entity.TblBranchOffice.FirstOrDefault().ModifyDate = DateTime.Now;
                    entity.TblBranchOffice.FirstOrDefault().Principal = true;
                    entity.TblBranchOffice.FirstOrDefault().Name = "Principal";

                    //Agregando el almacen principal a la sucursal
                    TblWarehouse Warehouse = new TblWarehouse();
                    Warehouse.Name = "Almacén principal";
                    Warehouse.Description = "Almacén principal";
                    Warehouse.CreateDate = DateTime.Now;
                    Warehouse.UpdateDate = DateTime.Now;
                    Warehouse.Estatus = true;
                    Warehouse.Principal = true;

                    //agregando suscripcion
                    Tblsubscriptions subscriptions = new Tblsubscriptions();
                    var plan = planService.GetPlan(planId);
                    subscriptions.IdPlan = plan.Id;
                    subscriptions.Estatus = true;

                    if (plan.DaysOfPlan != 0)
                        subscriptions.DateStart = DateTime.Now;

                    if (plan.DaysOfPlan != 0)
                        subscriptions.DateEnd = DateTime.Now.AddDays(plan.DaysOfPlan);


                    //Creando el privilegio principal
                    TblPrivileges privilege = new TblPrivileges();
                    privilege.UserId = entity.User.Id;
                    privilege.Name = "Super Usuario";
                    privilege.CreateDate = DateTime.Now;
                    privilege.EditDate = DateTime.Now;
                    privilege.Principal = true;

                    //Agreando los objetos padres permitidos
                    var ParentObjects = (from c in subscriptionsRepository.GetSubObject(plan.Id)
                                         select new TblSegurity
                                         {
                                             SubObjectId = c.SubObjectId,
                                             TypeMethodId = c.TypeMethodId
                                         }).ToList();


                    //Buscando los objetos hijos
                    var childrenObjects = (from c in securityRepository.GetObjectChildrens(ParentObjects.Select(x => x.SubObjectId).ToList())
                                           select new TblSegurity
                                           {
                                               SubObjectId = c.SubObjectChildren,
                                               TypeMethodId = c.TypeMethodId
                                           }).ToList();


                    foreach (var item in ParentObjects)
                        privilege.TblSegurity.Add(item);

                    foreach (var item in childrenObjects)
                        privilege.TblSegurity.Add(item);


                    ///Agregamos el usuario como empleado a su empresa
                    TblEmployeeBusiness employeeBusiness = new TblEmployeeBusiness();
                    var userData = accountingService.GetUser(entity.User.Id);

                    employeeBusiness.UserId = entity.User.Id;
                    employeeBusiness.Estatus = true;
                    employeeBusiness.IntoDate = DateTime.Now;
                    employeeBusiness.CreateDate = DateTime.Now;
                    employeeBusiness.EditDate = DateTime.Now;
                    employeeBusiness.Name = userData.Name;
                    employeeBusiness.EmployeeCode = globalService.GenerateCode(Support.EnumTypes.CodeEntities.Employee);
                    employeeBusiness.BusinessOwner = true;

                    //Relacionamos todas las entidades
                    privilege.TblEmployeeBusiness.Add(employeeBusiness);
                    entity.TblEmployeeBusiness.Add(employeeBusiness);

                    entity.TblBranchOffice.FirstOrDefault().TblWarehouse.Add(Warehouse);
                    entity.Tblsubscriptions.Add(subscriptions);
                    entity.TblPrivileges.Add(privilege);

                    businessRepository.AddEntity(entity);
                }

                return task;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateBusiness", "BusineesService");
                serviceError.WarningMessage("Error al registrar la empresa");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Proceso para actualizar una empresa en el sistema
        /// </summary>
        /// <param name="entity">Modelo que contiene la empresa y la sucursal</param>
        /// <returns></returns>
        public async Task<TaskResult> UpdateBusiness(TblBusiness entity)
        {
            var task = await BusinessValidateUpdate(entity);

            if (task.ExecutedSuccesfully)
            {
                var session = securityService.GetSession();
                TblBusiness business = businessRepository.Get(entity.Id);

                business.Name = entity.Name;
                business.CountryId = entity.CountryId;
                business.DocumentTypeId = entity.DocumentTypeId;
                business.DocumentNumber = entity.DocumentNumber;
                business.WebPage = entity.WebPage;
                business.Description = entity.Description;
                business.ModifyDate = DateTime.Now;
                business.DateConstitution = entity.DateConstitution;

                var tempPicture = globalService.GetTempFiles(session.UserId, (byte)TypeFiles.BusinessLogo);

                //Movemos la foto a su carpeta final
                if (tempPicture.Count() > 0 && !string.IsNullOrEmpty(entity.Logo))
                {
                    entity.Logo = tempPicture.LastOrDefault().FileName;
                    await globalService.MoveFile(entity.Logo, TypeFiles.BusinessLogo);

                    if (business.Logo != null)
                        await amazonService.DeleteFile(business.Logo, TypeFiles.BusinessLogo);//Eliminamos la foto de amazon web service   


                    business.Logo = entity.Logo;
                }                

                //Removemos las fotos temporales que tenga el usuario
                if (tempPicture.Count() > 0 && !string.IsNullOrEmpty(business.Logo))
                    globalService.DeleteRangeTempFile(tempPicture);


                businessRepository.Update(business);
            }

            return task;
        }


        /// <summary>
        /// Proceso para hacer las validaciones al usuario cuando se esta registrando
        /// </summary>
        /// <param name="business">Modelo que contiene la empresa y la sucursal</param>
        /// <returns></returns>
        private async Task<TaskResult> BusinessValidateCreate(TblBusiness business)
        {
            try
            {
                var documentType = await globalService.GetDocumentIdentification(business.DocumentTypeId);

                if (string.IsNullOrEmpty(business.Name))
                    return serviceError.WarningMessage("Falta el nombre de la empresa.");
                else if (string.IsNullOrEmpty(business.DocumentNumber))
                    return serviceError.WarningMessage("Falta el número de documento.");
                else if (business.DocumentTypeId == 0)
                    return serviceError.WarningMessage("Falta el tipo de documento.");
                else if (business.DocumentNumber.Length < documentType.MaxlengthChar)
                    return serviceError.WarningMessage("El número de " + documentType.Name + " es demasiado corto, favor revisar el número ingresado");
                else if (business.TblBranchOffice == null || business.TblBranchOffice.Count == 0)
                {
                    serviceError.LogError("Faltan los datos de la sucursal.", "BusinessValidateCreate", "BusinessService", "Los datos no se han podido registra correctamente");
                    return serviceError.TaskStatus();
                }
                else if (business.DocumentNumber != null)
                {
                    bool document = false;
                    document = businessRepository.Validationdocument(business.DocumentNumber);
                    if (document == true)
                    {
                        return serviceError.WarningMessage("El número de documento exíste, favor de revisar.");
                    }
                }
                else
                {
                    var branchOffice = business.TblBranchOffice.FirstOrDefault();

                    if (string.IsNullOrEmpty(branchOffice.AddressBranchOffice))
                        return serviceError.WarningMessage("Falta registrar los datos de la dirección.");

                }
                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "BusinessValidateCreate", "BusineesService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }


        /// <summary>
        /// Metodo para hacer las validaciones de la empresa
        /// </summary>
        /// <param name="business">Modelo que contiene la empresa</param>
        /// <returns></returns>
        private async Task<TaskResult> BusinessValidateUpdate(TblBusiness business)
        {
            try
            {
                var session = securityService.GetSession();
                var documentType = await globalService.GetDocumentIdentification(business.DocumentTypeId);

                if (string.IsNullOrEmpty(business.Name))
                    return serviceError.WarningMessage("Falta el nombre de la empresa.");
                else if (string.IsNullOrEmpty(business.DocumentNumber))
                    return serviceError.WarningMessage("Falta el número de documento.");
                else if (business.DocumentTypeId == 0)
                    return serviceError.WarningMessage("Falta el tipo de documento.");
                else if (business.DocumentNumber.Length < documentType.MaxlengthChar)
                    return serviceError.WarningMessage("El número de " + documentType.Name + " es demasiado corto, favor revisar el número ingresado");
                else if (business.DocumentNumber != null)
                {
                    if (businessRepository.ValidationEditdocument(business.DocumentNumber, session.BusinessId))
                        return serviceError.WarningMessage("El número de documento exíste, favor de revisar.");

                }
                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "BusinessValidateUpdate", "BusineesService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }


        /// <summary>
        /// Proceso para obtener los objetos mediante un listado de ID
        /// </summary>
        /// <param name="SubObjectsId"></param>
        /// <returns></returns>
        public IQueryable<vwPlansDetail> GetPlansDetails(int businessTypeId)
        {
            return subscriptionsRepository.GetPlansDetails(businessTypeId);
        }

        /// <summary>
        /// Metodo para obtener una empresa
        /// </summary>
        /// <param name="id">Primary key de la empresa</param>
        /// <returns></returns>
        public TblBusiness GetBusinessById(long id)
        {
            return businessRepository.Get(x => x.Id == id).FirstOrDefault();
        }


        /// <summary>
        /// Obtiene todos los tipos de negocios
        /// </summary>
        /// <returns></returns>
        public IQueryable<TblBusinessType> GetAllBusinessType()
        {
            return businessRepository.GetAllBusinessType();
        }

        /// <summary>
        /// Proceso para obtener todas las empresas de un usuario
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public IQueryable<TblBusiness> GetBusiness(long UserId)
        {
            return businessRepository.GetBusinessByUserId(UserId);
        }

        /// <summary>
        /// Proceso para una empresa registrada
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        public TblBusiness GetBusinessData(long BusinessId)
        {
            return businessRepository.Get(BusinessId);
        }



        /// <summary>
        /// Proceso para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            var task = await businessRepository.Savechanges();

            if (!task.ExecutedSuccesfully)
                serviceError.LogError(task.MessageList[0], "Savechanges", "BusinessService", "Error al intentar guardar los datos.");

            return task;
        }

        /// <summary>
        /// Proceso para obtener los objetos permitidos para la empresa
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <param name="privilegeId">Primary key del privilegio</param>
        /// <param name="Principal">Para obtener el privilegio super usuario</param>
        /// <returns></returns>
        public IQueryable<vwObjectsBusiness> GetObjects(long BusinessId, int privilegeId = 0, bool Principal = false)
        {
            return businessRepository.GetObjects(BusinessId, privilegeId, Principal);
        }
        #endregion


        //==============================================================================Seccion para los privilegios===============================================
        #region Privilegios

        /// <summary>
        /// Metodo para obtener la seguridad con sus accesos
        /// </summary>
        /// <param name="privilegeId"></param>
        /// <returns></returns>
        public IQueryable<vwSegurityAccess> GetSegurityAccess(int privilegeId)
        {
            return businessRepository.GetSegurityAccess(privilegeId);
        }
        /// <summary>
        /// Proceso para obtener los privilegios registrados que tiene una empresa
        /// </summary>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        public IQueryable<TblPrivileges> GetPrivileges(long businessId)
        {
            return privilegeRepository.GetPrivileges(businessId);
        }

        /// <summary>
        /// Proceso para crear un privilegio en la empresa
        /// </summary>
        /// <param name="entity">Datos del privilegio</param>
        /// <param name="Objetos">Listados de objetos que tiene el privilegio</param>
        /// <returns></returns>
        public TaskResult CreatePrivilege(TblPrivileges entity, List<TblSegurity> Objetos)
        {
            var task = PrivilegeValidateCreate(entity, Objetos);

            if (task.ExecutedSuccesfully)
            {
                var sesion = securityService.GetSession();
                entity.BusinessId = sesion.BusinessId;
                entity.UserId = sesion.UserId;
                entity.CreateDate = DateTime.Now;
                entity.EditDate = DateTime.Now;
                entity.Estatus = true;


                var parentObjects = Objetos.GroupBy(x => x.SubObjectId).Select(x => x.Key);
                foreach (var parent in parentObjects)
                {
                    TblSegurity segurity = new TblSegurity();
                    segurity.TypeMethodId = 1;
                    segurity.SubObjectId = parent;
                    entity.TblSegurity.Add(segurity);


                    //Buscando los objetos hijos
                    var childrenObjects = (from c in securityRepository.GetObjectChildrens(parent, Objetos.Select(x => x.TypeMethodId).ToList())
                                           select new TblSegurity
                                           {
                                               SubObjectId = c.SubObjectChildren,
                                               TypeMethodId = c.TypeMethodId
                                           }).ToList();


                    foreach (var item in childrenObjects)
                        entity.TblSegurity.Add(item);
                }

                entity.Principal = false;
                privilegeRepository.AddEntity(entity);
            }

            return task;
        }

        /// <summary>
        /// Proceso para actualizar un privilegio en la empresa
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public TaskResult UpdatePrivilege(TblPrivileges entity, List<TblSegurity> Objetos)
        {
            var sesion = securityService.GetSession();
            entity.UserId = sesion.UserId;

            var task = PrivilegeValidateUpdate(entity, Objetos);
            if (task.ExecutedSuccesfully)
            {
                var privilege = privilegeRepository.Get(x => x.Id == entity.Id && x.BusinessId == sesion.BusinessId).FirstOrDefault();

                privilege.Name = entity.Name;
                privilege.UserId = sesion.UserId;
                privilege.EditDate = DateTime.Now;

                //Eliminamos la seguridad anterior
                var seguritydata = securityRepository.Get(x => x.PrivilegesId == privilege.Id);

                //Eliminamos la referencia en la clase
                List<TblSegurity> MemorySegurity = new List<TblSegurity>();

                //Agregamos la seguridad nueva
                var parentObjects = Objetos.GroupBy(x => x.SubObjectId).Select(x => x.Key);
                foreach (var parent in parentObjects)
                {
                    TblSegurity segurity = new TblSegurity();
                    segurity.TypeMethodId = 1;
                    segurity.SubObjectId = parent;
                    segurity.PrivilegesId = privilege.Id;
                    MemorySegurity.Add(segurity);

                    //Traemos los tipos de acceso dependiendo del objeto
                    var tipeMetod = Objetos.Where(x => x.SubObjectId == parent).Select(x => x.TypeMethodId).ToList();

                    //Buscando los objetos hijos
                    var childrenObjects = (from c in securityRepository.GetObjectChildrens(parent, tipeMetod)
                                           select new TblSegurity
                                           {
                                               SubObjectId = c.SubObjectChildren,
                                               TypeMethodId = c.TypeMethodId,
                                               PrivilegesId = privilege.Id
                                           }).ToList();


                    foreach (var item in childrenObjects)
                        MemorySegurity.Add(item);
                }

                var ExistRow = (from p in MemorySegurity
                                where (from b in seguritydata
                                       select b.Id)
                                          .Contains(p.Id)
                                select p).Distinct().ToList();

                //Las filas que fueron agregadas
                var NewRows = (from p in MemorySegurity
                               where !(from b in seguritydata
                                       select b.SubObjectId)
                                         .Contains(p.SubObjectId)
                               select p).Distinct().ToList();


                //Las filas que fueron eliminadas
                var DeleteRows = (from p in seguritydata
                                  where !(from b in MemorySegurity
                                          select b.SubObjectId)
                                            .Contains(p.SubObjectId)
                                  select p).Distinct().ToList();


                foreach (var item in NewRows)
                    privilege.TblSegurity.Add(item);


                if (DeleteRows.Count > 0)
                    securityRepository.DeleteRange(DeleteRows);

                //privilege.TblSegurity = null;
                privilegeRepository.Update(privilege);
            }
            return task;
        }

        /// <summary>
        /// Metodo para obtener un privilegio
        /// </summary>
        /// <param name="PrivilegeId">Primary key del privilegio</param>
        /// <returns></returns>
        public TblPrivileges GetPrivilege(int PrivilegeId, long businessId)
        {
            return privilegeRepository.Get(x => x.Id == PrivilegeId && x.BusinessId == businessId).FirstOrDefault();
        }

        /// <summary>
        /// Metodo para validar la informacion al crear un privilegio
        /// </summary>
        /// <param name="privilege">Modelo que contiene el privilegio y los objetos</param>
        /// <returns></returns>
        private TaskResult PrivilegeValidateCreate(TblPrivileges privilege, List<TblSegurity> Objetos)
        {
            try
            {
                if (privilege == null)
                    return serviceError.WarningMessage("No se han encontrado datos para registrar.");
                else if (string.IsNullOrEmpty(privilege.Name))
                    return serviceError.WarningMessage("Falta el nombre del privilegio.");
                else if (Objetos == null || Objetos.Count == 0)
                {
                    serviceError.LogError("Faltan los datos de la seguridad.", "PrivilegeValidateCreate", "BusinessService", "Los datos no se han podido registra correctamente");
                    return serviceError.TaskStatus();
                }
                else
                {
                    if (Objetos.Where(x => x.SubObjectId == 0).Any())
                    {
                        serviceError.LogError("Falta el id de un objeto para la seguridad.", "PrivilegeValidateCreate", "BusinessService", "Los datos no se han podido registra correctamente");
                    }
                }
                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "PrivilegeValidateCreate", "BusineesService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }


        /// <summary>
        /// Metodo para validar la informacion al actualizar un privilegio
        /// </summary>
        /// <param name="privilege">Modelo que contiene el privilegio y los objetos</param>
        /// <returns></returns>
        private TaskResult PrivilegeValidateUpdate(TblPrivileges privilege, List<TblSegurity> Objetos)
        {
            try
            {
                if (string.IsNullOrEmpty(privilege.Name))
                    return serviceError.WarningMessage("Falta el nombre del privilegio.");
                else if (privilege.UserId == 0)
                    return serviceError.WarningMessage("Falta el id del usuario que modifica.");
                else if (privilege.Id == 0)
                    return serviceError.WarningMessage("Falta el id del privilegio.");
                else if (Objetos == null || Objetos.Count == 0)
                {
                    serviceError.LogError("Faltan los datos de la seguridad.", "PrivilegeValidateCreate", "BusinessService", "Los datos no se han podido registra correctamente");
                    return serviceError.TaskStatus();
                }
                else
                {
                    if (Objetos.Where(x => x.SubObjectId == 0).Any())
                    {
                        serviceError.LogError("Falta el id de un objeto para la seguridad.", "PrivilegeValidateCreate", "BusinessService", "Los datos no se han podido registra correctamente");
                    }
                }
                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "PrivilegeValidateCreate", "BusineesService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para activar y desactivar un privilegio
        /// </summary>
        /// <param name="status">Estatus al que pasa el privilegio</param>
        /// <param name="Id">Primary key del privilegio</param>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        public TaskResult StatusPrivilege(bool status, int Id, long businessId)
        {
            TaskResult task = new TaskResult();

            if (Id == 0)
                task = serviceError.WarningMessage("El primary key del privilegio no puede ser 0.");
            else if (businessId == 0)
                task = serviceError.WarningMessage("El primary key de la empresa no puede ser 0.");
            else
            {
                var privilege = privilegeRepository.Get(x => x.Id == Id).FirstOrDefault();
                privilege.Estatus = status;
                privilegeRepository.Update(privilege);
                task.ExecutedSuccesfully = true;
            }

            return task;
        }
        #endregion


        //===============================================================================Seccion para las sucursales================================================
        #region Sucursal
        /// <summary>
        /// Metodo para obtener la sucursal principal
        /// </summary>
        /// <param name="BussinesId">Primary key de la empresa</param>
        /// <returns></returns>
        public TblBranchOffice GetPrincipalBranchOffice(long BussinesId)
        {
            return branchOfficeRepository.Get(x => x.IdBusiness == BussinesId && x.Principal).FirstOrDefault();
        }

        /// <summary>
        /// Metodo para obtener el primary key de la sucursal principal
        /// </summary>
        /// <param name="BussinesId">Primary key de la empresa</param>
        /// <returns></returns>
        public long GetPrincipalBranchOfficeId(long BussinesId)
        {
            return branchOfficeRepository.Get(x => x.IdBusiness == BussinesId && x.Principal).Select(x => x.Id).FirstOrDefault();
        }

        /// <summary>
        /// Metodo para registrar una sucusal
        /// </summary>
        /// <param name="Model">Modelo con la informacion de la sucursal</param>
        /// <returns></returns>
        public TaskResult CreateBranchOffice(TblBranchOffice entity)
        {
            try
            {
                var task = BranchOfficeValidateCreate(entity);

                if (task.ExecutedSuccesfully)
                {
                    var session = securityService.GetSession();
                    var branchOffice = branchOfficeRepository.Get(x => x.IdBusiness == session.BusinessId).ToList();

                    entity.EmployeeModifyId = session.UserId;
                    entity.EmployeeCreateId = session.UserId;
                    entity.IdBusiness = session.BusinessId;
                    entity.Estatus = true;
                    entity.ModifyDate = DateTime.Now;
                    entity.CreateDate = DateTime.Now;
                    entity.BranchOfficeNumber = (byte)(branchOffice.Count() + 1);

                    //Validamos si esta sucursal sera la principal para quitar las otras como principales
                    if (entity.Principal && branchOffice.Count > 1)
                    {
                        foreach (var item in branchOffice)
                        {
                            item.Principal = false;
                            branchOfficeRepository.Update(item);
                        }
                    }

                    //Agregando el almacen principal a la sucursal
                    TblWarehouse Warehouse = new TblWarehouse();
                    Warehouse.Name = "Principal";
                    Warehouse.Description = "almacén principal";
                    Warehouse.CreateDate = DateTime.Now;
                    Warehouse.UpdateDate = DateTime.Now;
                    Warehouse.Estatus = true;
                    entity.TblWarehouse.Add(Warehouse);

                    branchOfficeRepository.AddEntity(entity);
                }

                return task;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateBranchOffice", "BusineesService");
                serviceError.WarningMessage("Error al registrar la sucursal");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para validar cuando se crea una sucursal
        /// </summary>
        /// <param name="business">Informacion de la sucursal</param>
        /// <returns></returns>
        private TaskResult BranchOfficeValidateCreate(TblBranchOffice branchOffice)
        {
            try
            {
                if (branchOffice == null)
                    return serviceError.WarningMessage("Faltan los datos de la sucursal.");
                else if (string.IsNullOrEmpty(branchOffice.Name))
                    return serviceError.WarningMessage("Falta el nombre de la sucursal.");
                else if (string.IsNullOrEmpty(branchOffice.AddressBranchOffice))
                    return serviceError.WarningMessage("Falta registrar los datos de la dirección.");


                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "BranchOfficeValidateCreate", "BusineesService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }


        /// <summary>
        /// Metodo para actualizar los datos de una sucursal
        /// </summary>
        /// <param name="entity">Modelo con la información de la sucursal</param>
        /// <returns></returns>
        public TaskResult UpdateBranchOffice(TblBranchOffice entity)
        {
            var task = BranchOfficeValidateUpdate(entity);

            if (task.ExecutedSuccesfully)
            {
                var session = securityService.GetSession();
                var BranchOffices = branchOfficeRepository.Get(x => x.IdBusiness == session.BusinessId && x.Id != entity.Id).ToList();
                TblBranchOffice branchOffice = branchOfficeRepository.Get(x => x.Id == entity.Id && x.IdBusiness == session.BusinessId).FirstOrDefault();

                //Sucursal 
                branchOffice.ModifyDate = DateTime.Now;
                branchOffice.EmployeeModifyId = session.UserId;
                branchOffice.ProvinceId = entity.ProvinceId;
                branchOffice.AddressBranchOffice = entity.AddressBranchOffice;
                branchOffice.PhoneNumber = entity.PhoneNumber;
                branchOffice.PhoneNumber2 = entity.PhoneNumber2;
                branchOffice.Email = entity.Email;
                branchOffice.Name = entity.Name;
                branchOffice.Principal = entity.Principal;

                //Validamos si esta sucursal sera la principal para quitar las otras como principales
                if (entity.Principal && BranchOffices.Count > 1)
                {
                    foreach (var item in BranchOffices)
                    {
                        item.Principal = false;
                        branchOfficeRepository.Update(item);
                    }
                }

                branchOfficeRepository.Update(branchOffice);
            }
            return task;
        }

        /// <summary>
        /// Metodo para validar la actualización de una sucursal
        /// </summary>
        /// <param name="branchOffice">Modelo que contiene la información de la sucursal</param>
        /// <returns></returns>
        private TaskResult BranchOfficeValidateUpdate(TblBranchOffice branchOffice)
        {
            try
            {
                if (branchOffice == null)
                    return serviceError.WarningMessage("Faltan los datos de la sucursal.");
                else if (string.IsNullOrEmpty(branchOffice.Name))
                    return serviceError.WarningMessage("Falta el nombre de la sucursal.");
                else if (string.IsNullOrEmpty(branchOffice.AddressBranchOffice))
                    return serviceError.WarningMessage("Falta registrar los datos de la dirección.");

                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "BranchOfficeValidateUpdate", "BusineesService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        ///  Proceso para buscar una sucursal especifica
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        public TblBranchOffice GetBranchOffice(long BranchOfficeId)
        {
            return branchOfficeRepository.Get(BranchOfficeId);
        }

        /// <summary>
        /// Metodo para cambiar de estado una sucursal
        /// </summary>
        /// <param name="BrancOfficeId">Primary key de la sucursal</param>
        /// <param name="status">Estado</param>
        /// <returns></returns>
        public TaskResult SetBranchOfficeStatus(long BrancOfficeId, bool status)
        {
            TaskResult task = new TaskResult();
            if (BrancOfficeId == 0)
            {
                task.ExecutedSuccesfully = false;
                serviceError.LogError("Falta el ID de la sucursal", "BranchOfficeValidateUpdate", "BranchOfficeService", "Los datos no se han podido actualizar correctamente");
            }

            if (task.ExecutedSuccesfully)
            {
                var branchOffice = GetBranchOffice(BrancOfficeId);
                branchOffice.Estatus = status;
                branchOfficeRepository.Update(branchOffice);
            }
            return task;
        }


        /// <summary>
        /// Proceso para obtener el listado de sucursales de una empresa
        /// </summary>
        /// <param name="businesId">Primary key de la empresa</param>
        /// <returns></returns>
        public IQueryable<TblBranchOffice> getBranchOffices(long businesId)
        {
            return branchOfficeRepository.Get(x => x.IdBusiness == businesId);
        }

        #endregion

        //===============================================================================Sección para los planes======================================
        #region plans
        /// <summary>
        /// Metodo para obtener el id del plan que tiene una empresa
        /// </summary>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        public int GetPlanId(long businessId)
        {
            return planService.GetPlanId(businessId);
        }

        /// <summary>
        /// Metodo para obtener el nombre del plan asociado a una empresa
        /// </summary>
        /// <param businessId="id">Primary key de la empresa</param>
        /// <returns></returns>
        public string GetPlanName(long businessId)
        {
            return planService.GetPlanName(businessId);
        }

        /// Metodo para actualizar la suscripcion de una empresa
        /// </summary>
        /// <param name="businessId"></param>
        /// <param name="PlanId"></param>
        /// <returns></returns>
        public bool UpdateBusinessPlan(long businessId, int PlanId)
        {
            return subscriptionsRepository.UpdateBusinessPlan(businessId, PlanId);
        }

        /// <summary>
        /// Metodo para obtener a que tipo de empresa pertenece un negocio
        /// </summary>
        /// <param name="BusinessId">Primary key del negocio</param>
        /// <returns></returns>
        public short GetBusinesTypeId(long BusinessId)
        {
            return subscriptionsRepository.GetBusinessTypeId(BusinessId);
        }
        #endregion

    }

    public interface IBusinessService
    {
        /// <summary>
        /// Proceso para registrar una empresa en el sistema
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<TaskResult> CreateBusiness(TblBusiness entity, short planId);

        /// <summary>
        /// Proceso para crear un privilegio en la empresa
        /// </summary>
        /// <param name="entity">Datos del privilegio</param>
        /// <param name="Objetos">Listados de objetos que tiene el privilegio</param>
        /// <returns></returns>
        TaskResult CreatePrivilege(TblPrivileges entity, List<TblSegurity> Objetos);

        /// <summary>
        /// Proceso para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();

        /// <summary>
        /// Metodo para obtener la seguridad con sus accesos
        /// </summary>
        /// <param name="privilegeId"></param>
        /// <returns></returns>
        IQueryable<vwSegurityAccess> GetSegurityAccess(int privilegeId);

        /// <summary>
        /// Proceso para obtener los objetos mediante un listado de ID
        /// </summary>
        /// <param name="SubObjectsId"></param>
        /// <returns></returns>
        IQueryable<vwPlansDetail> GetPlansDetails(int businessTypeId);

        /// <summary>
        /// Proceso para obtener los objetos permitidos para la empresa
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <param name="privilegeId">Primary key del privilegio</param>
        /// <param name="Principal">Para obtener el privilegio super usuario</param>
        /// <returns></returns>
        IQueryable<vwObjectsBusiness> GetObjects(long BusinessId, int privilegeId = 0, bool Principal = false);

        /// <summary>
        /// Metodo para obtener un privilegio
        /// </summary>
        /// <param name="PrivilegeId">Primary key del privilegio</param>
        /// <returns></returns>
        TblPrivileges GetPrivilege(int PrivilegeId, long businessId);

        /// <summary>
        /// Proceso para obtener los privilegios registrados que tiene una empresa
        /// </summary>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        IQueryable<TblPrivileges> GetPrivileges(long businessId);

        /// <summary>
        /// Proceso para actualizar un privilegio en la empresa
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        TaskResult UpdatePrivilege(TblPrivileges entity, List<TblSegurity> Objetos);


        /// <summary>
        /// Proceso para actualizar una empresa en el sistema
        /// </summary>
        /// <param name="entity">Modelo que contiene la empresa y la sucursal</param>
        /// <returns></returns>
        Task<TaskResult> UpdateBusiness(TblBusiness entity);

        /// <summary>
        /// Proceso para una empresa registrada
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        TblBusiness GetBusinessData(long BusinessId);

        IQueryable<TblBusinessType> GetAllBusinessType();
        IQueryable<TblBusiness> GetBusiness(long UserId);

        /// <summary>
        /// Metodo para obtener una empresa
        /// </summary>
        /// <param name="id">Primary key de la empresa</param>
        /// <returns></returns>
        TblBusiness GetBusinessById(long id);

        /// <summary>
        /// Metodo para activar y desactivar un privilegio
        /// </summary>
        /// <param name="status">Estatus al que pasa el privilegio</param>
        /// <param name="Id">Primary key del privilegio</param>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        TaskResult StatusPrivilege(bool status, int Id, long businessId);


        #region Sucursales
        /// <summary>
        /// Proceso para obtener el listado de sucursales de una empresa
        /// </summary>
        /// <param name="businesId">Primary key de la empresa</param>
        /// <returns></returns>
        IQueryable<TblBranchOffice> getBranchOffices(long businesId);
        /// <summary>
        /// Metodo para obtener la sucursal principal
        /// </summary>
        /// <param name="BussinesId">Primary key de la empresa</param>
        /// <returns></returns>
        TblBranchOffice GetPrincipalBranchOffice(long BussinesId);

        /// <summary>
        ///  Proceso para buscar una sucursal especifica
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        TblBranchOffice GetBranchOffice(long BranchOfficeId);

        /// <summary>
        /// Metodo para registrar una sucusal
        /// </summary>
        /// <param name="Model">Modelo con la informacion de la sucursal</param>
        /// <returns></returns>
        TaskResult CreateBranchOffice(TblBranchOffice entity);

        /// <summary>
        /// Metodo para actualizar los datos de una sucursal
        /// </summary>
        /// <param name="entity">Modelo con la información de la sucursal</param>
        /// <returns></returns>
        TaskResult UpdateBranchOffice(TblBranchOffice entity);

        /// <summary>
        /// Metodo para cambiar de estado una sucursal
        /// </summary>
        /// <param name="BrancOfficeId">Primary key de la sucursal</param>
        /// <param name="status">Estado</param>
        /// <returns></returns>
        TaskResult SetBranchOfficeStatus(long BrancOfficeId, bool status);

        /// <summary>
        /// Metodo para obtener el primary key de la sucursal principal
        /// </summary>
        /// <param name="BussinesId">Primary key de la empresa</param>
        /// <returns></returns>
        long GetPrincipalBranchOfficeId(long BussinesId);
        #endregion

        #region Plans
        /// <summary>
        /// Metodo para obtener el id del plan que tiene una empresa
        /// </summary>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        int GetPlanId(long businessId);

        /// Metodo para actualizar la suscripcion de una empresa
        /// </summary>
        /// <param name="businessId"></param>
        /// <param name="PlanId"></param>
        /// <returns></returns>
        bool UpdateBusinessPlan(long businessId, int PlanId);

        /// <summary>
        /// Metodo para obtener el nombre del plan asociado a una empresa
        /// </summary>
        /// <param businessId="id">Primary key de la empresa</param>
        /// <returns></returns>
        string GetPlanName(long businessId);

        /// <summary>
        /// Metodo para obtener a que tipo de empresa pertenece un negocio
        /// </summary>
        /// <param name="BusinessId">Primary key del negocio</param>
        /// <returns></returns>
        short GetBusinesTypeId(long BusinessId);
        #endregion
    }
}
