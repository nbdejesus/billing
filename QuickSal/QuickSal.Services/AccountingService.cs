﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.Services
{
    public class AccountingService : IAccountingService
    {
        private ITaxRepository taxRepository;
        private IServiceError serviceError;
        private ISecurityService securityService;
        private INcfRepository ncfRepository;
        private INcfRowRepository ncfRowRepository;

        public AccountingService(ITaxRepository taxRepository, IServiceError serviceError,
                                ISecurityService securityService,
                                INcfRepository ncfRepository, INcfRowRepository ncfRowRepository)
        {
            this.ncfRepository = ncfRepository;
            this.taxRepository = taxRepository;
            this.serviceError = serviceError;
            this.securityService = securityService;
            this.ncfRowRepository = ncfRowRepository;
        }

        #region ncf
        /// <summary>
        /// Metodo para obtener los NFC registrados de la empresa
        /// </summary>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        public IQueryable<Ncfs> GetNcfs(long businessId)
        {

            return ncfRepository.NCF(businessId);
        }

        /// <summary>
        /// Metodo para obtener los tipo comprobantes de la una empresa
        /// </summary>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        public IQueryable<TblNcfprefix> GettypeNcf(long businessId)
        {
            return ncfRepository.TypeNcf( businessId);
        }

        /// <summary>
        /// Metodo para obtener un Ncf
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="businessId"></param>
        /// <returns></returns>
        public TblNcf GetNcf(int Id, long businessId)
        {
            return ncfRepository.Get(x => x.Id == Id && x.BusinessId == businessId).FirstOrDefault();
        }
        /// <summary>
        /// Metodo para registrar un Ncf
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion del Ncf</param>
        /// <returns></returns>
        public TaskResult CreateNcf(TblNcf Ncf)
        {

            var task = NcfValidateCreate(Ncf);
            string newPassword = string.Empty;

            if (task.ExecutedSuccesfully)
            {
                var Inicio = Convert.ToInt32(Ncf.CounterFrom);
                var Fin = Convert.ToInt32(Ncf.CounterTo);               
                               
                for (int i = Inicio; i <= Fin; i++)
                {                 
                    var valorCodigo = Convert.ToString(i).PadLeft(8, '0');                    
                    TblNcfrow modelo = new TblNcfrow();
                    modelo.Ncf = valorCodigo;
        
                    Ncf.TblNcfrow.Add(modelo);
                }
                //Seteamos datos por defecto del cliente                       
                var sesion = securityService.GetSession();
                Ncf.BusinessId = sesion.BusinessId;
                Ncf.PrefixId = Ncf.PrefixId;
                Ncf.CreateDate = DateTime.Now;
                Ncf.Estatus = true;
                Ncf.EmployeeCreateId = sesion.EmployeeId;
                Ncf.EmployeeModifyId = sesion.EmployeeId;
                Ncf.IsExhausted = false;
                ncfRepository.AddEntity(Ncf);
            }

            return task;
        }

        /// <summary>
        /// Metodo para validar un Ncf
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion del Ncf</param>
        /// <returns></returns>
        public bool ValidarForm(TblNcf Ncf)
        {
            var session = securityService.GetSession();
            Ncf.BusinessId = session.BusinessId;
            var Estado = ncfRepository.ValidarForm(Ncf);
            return Estado;
        }

        /// <summary>
        /// Metodo para validar un Ncf
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion del Ncf</param>
        /// <returns></returns>
        public bool ValidarFormEdit(Ncfs Ncf)
        {
            var session = securityService.GetSession();
            Ncf.BusinessId = session.BusinessId;
            var Estado = ncfRepository.ValidarFormEdit(Ncf);
            return Estado;
        }

        /// <summary>
        /// Metodo para hacer las validaciones al Ncf cuando se esta registrando
        /// </summary>
        /// <param name="customer">Modelo que contiene la informacion del Ncf</param>
        /// <returns></returns>
        private TaskResult NcfValidateCreate(TblNcf Ncf)
        {
            try
            {
                if (Ncf.PrefixId == 0)
                    return serviceError.WarningMessage("El PrefixId del Ncf es obligatorio.");
                else if (Ncf.CounterFrom == 0)
                    return serviceError.WarningMessage("El campo Desde es obligatorio");
                else if (Ncf.CounterTo == 0)
                    return serviceError.WarningMessage("El campo Hasta es obligatorio");
                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "NcfValidateCreate", "NcfService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// metodo para actualizar un Ncf
        /// </summary>
        /// <param name="model">Entidad con la informacion</param>
        /// <returns></returns>
        public TaskResult UpdateNcf(TblNcf Ncf)
        {
            try
            {
                var task = TblNcfValidateUpdate(Ncf);

                if (task.ExecutedSuccesfully)
                {
                    var Inicio = Convert.ToInt32(Ncf.CounterFrom);
                    var Fin = Convert.ToInt32(Ncf.CounterTo);                   

                    var session = securityService.GetSession();
                    var NcfHeader = ncfRepository.Get(x => x.Id == Ncf.Id).FirstOrDefault();
                    var NcfRow = ncfRowRepository.Get(x => x.Ncfid == Ncf.Id && x.Used == false).ToList();
                    ncfRowRepository.DeleteRange(NcfRow);
  
                    for (int i = Inicio; i <= Fin; i++)
                    {                       
                        var valorCodigo = Convert.ToString(i).PadLeft(8, '0');                      

                        TblNcfrow modelo = new TblNcfrow();
                        modelo.Ncf = valorCodigo;
                        NcfHeader.TblNcfrow.Add(modelo);
                    }
                    NcfHeader.CounterTo = Ncf.CounterTo;
                    NcfHeader.CounterFrom = Ncf.CounterFrom;
                    NcfHeader.EmployeeModifyId = session.EmployeeId;
                    NcfHeader.Estatus = true;               
                    ncfRepository.Update(NcfHeader);
                }

                return task;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateNcf", "AccountingService");
                serviceError.WarningMessage("Error al actualizar el Ncf");
                return serviceError.TaskStatus();
            }
        }

        private TaskResult TblNcfValidateUpdate(TblNcf Ncf)
        {
            try
            {
                if (Ncf.PrefixId == 0)
                    return serviceError.WarningMessage("Falta el Prefix del impuesto.");
                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "TblNcfValidateUpdate", "AccountingService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }

        public TaskResult SetNcfStatus(int Id, bool status, long businessId)
        {
            TaskResult task = new TaskResult();

            if (Id == 0)
            {
                task.ExecutedSuccesfully = false;
                serviceError.LogError("Falta el ID del Ncf", "SetNcfStatus", "AccoutingService", "Los datos no se han podido actualizar correctamente.");
            }

            if (task.ExecutedSuccesfully)
            {
                var ncf = GetNcf(Id, businessId);
                ncf.Estatus = status;
                ncfRepository.Update(ncf);
            }
            return task;
        }

        /////Metodo para traer el datos del nfccompleto
        ///// </summary> 
        ///// <returns></returns>
        public IQueryable<Ncfs> ListVoucher(long businessID, int Id)
        {
            return ncfRepository.ListVoucher(businessID, Id);
        }

        #endregion

        #region impuesto
        /// <summary>
        /// Metodo para obtener todos los impuestos del sistema
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        public IQueryable<TblTax> GetTaxes(long BusinessId)
        {
            return taxRepository.GetTaxes(BusinessId);
        }

        /// <summary>
        /// Metodo para agregar un impuesto al sistema
        /// </summary>
        /// <param name="entity">entidad con la informacion</param>
        /// <returns></returns>
        public TaskResult CreateTax(TblTax entity)
        {
            try
            {
                var task = TaskValidateCreate(entity);

                if (task.ExecutedSuccesfully)
                {
                    var sesion = securityService.GetSession();

                    entity.BusinessId = sesion.BusinessId;
                    entity.CreateDate = DateTime.Now;
                    entity.UpdateDate = DateTime.Now;
                    entity.EmployeeCreateId = sesion.EmployeeId;
                    entity.EmployeeModifyId = sesion.EmployeeId;
                    entity.Estatus = true;
                    entity.IsCustom = true;

                    taxRepository.AddEntity(entity);
                }

                return task;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateTax", "AccountingService");
                serviceError.WarningMessage("Error al registrar el impuesto");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para hacer las validaciones al impuesto cuando se esta registrando
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion del impuesto</param>
        /// <returns></returns>
        private TaskResult TaskValidateCreate(TblTax entity)
        {
            try
            {
                if (string.IsNullOrEmpty(entity.Name))
                    return serviceError.WarningMessage("Falta el nombre del impuesto.");
                else if (string.IsNullOrEmpty(entity.Abbreviation))
                    return serviceError.WarningMessage("Falta el nombre la abreviatura.");


                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "TaskValidateCreate", "AccountingService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para obtener un impuesto
        /// </summary>
        /// <param name="Id">Primary key del impuesto</param>
        /// <returns></returns>
        public TblTax GetTax(int Id)
        {
            return taxRepository.Get(x => x.Id == Id).FirstOrDefault();
        }


        /// <summary>
        /// metodo para actualizar un impuesto
        /// </summary>
        /// <param name="model">Entidad con la informacion</param>
        /// <returns></returns>
        public TaskResult UpdateTax(TblTax entity)
        {
            try
            {
                var task = TaxValidateUpdate(entity);

                if (task.ExecutedSuccesfully)
                {
                    var sesion = securityService.GetSession();
                    var tax = taxRepository.Get(x => x.Id == entity.Id).FirstOrDefault();

                    tax.UpdateDate = DateTime.Now;
                    tax.EmployeeModifyId = sesion.EmployeeId;
                    tax.Name = entity.Name;
                    tax.Description = entity.Description;

                    taxRepository.Update(tax);
                }

                return task;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateTax", "AccountingService");
                serviceError.WarningMessage("Error al actualizar el impuesto");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para cambiar de estado un impuesto
        /// </summary>
        /// <param name="Id">Primary key del impuesto</param>
        /// <param name="status">Estado</param>
        /// <returns></returns>
        public TaskResult SetTaxStatus(int Id, bool status)
        {
            TaskResult task = new TaskResult();

            if (Id == 0)
            {
                task.ExecutedSuccesfully = false;
                serviceError.LogError("Falta el ID del impuesto", "SetCategoryStatus", "AccountingService", "Los datos no se han podido actualizar correctamente.");
            }

            if (task.ExecutedSuccesfully)
            {
                var tax = GetTax(Id);
                tax.Estatus = status;
                taxRepository.Update(tax);
            }
            return task;
        }

        /// <summary>
        /// Metodo para hacer las validaciones al impuesto cuando se esta actualizando
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion del impuesto</param>
        /// <returns></returns>
        private TaskResult TaxValidateUpdate(TblTax entity)
        {
            try
            {
                if (string.IsNullOrEmpty(entity.Name))
                    return serviceError.WarningMessage("Falta el nombre del impuesto.");
                else if (string.IsNullOrEmpty(entity.Abbreviation))
                    return serviceError.WarningMessage("Falta el nombre la abreviatura.");


                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "TaxValidateUpdate", "AccountingService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }


        #endregion

        /// <summary>
        /// Proceso para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            var task = await taxRepository.Savechanges();

            if (!task.ExecutedSuccesfully)
                serviceError.LogError(task.MessageList[0], "Savechanges", "AccountingService", "Error al intentar guardar los datos.");

            return task;
        }

    }

    public interface IAccountingService
    {
        #region impuestos
        /// <summary>
        /// Metodo para validar un Ncf
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion del Ncf</param>
        /// <returns></returns>
        bool ValidarFormEdit(Ncfs Ncf);
        /// <summary>
        /// Metodo para validar un Ncf
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion del Ncf</param>
        /// <returns></returns>
        bool ValidarForm(TblNcf Ncf);
        /// <summary>
        /// Metodo para obtener todos los impuestos del sistema
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        IQueryable<TblTax> GetTaxes(long BusinessId);

        /// <summary>
        /// Metodo para agregar un impuesto al sistema
        /// </summary>
        /// <param name="entity">entidad con la informacion</param>
        /// <returns></returns>
        TaskResult CreateTax(TblTax entity);

        /// <summary>
        /// Metodo para obtener un impuesto
        /// </summary>
        /// <param name="Id">Primary key del impuesto</param>
        /// <returns></returns>
        TblTax GetTax(int Id);

        /// <summary>
        /// metodo para actualizar un impuesto
        /// </summary>
        /// <param name="model">Entidad con la informacion</param>
        /// <returns></returns>
        TaskResult UpdateTax(TblTax entity);

        /// <summary>
        /// Metodo para cambiar de estado un impuesto
        /// </summary>
        /// <param name="Id">Primary key del impuesto</param>
        /// <param name="status">Estado</param>
        /// <returns></returns>
        TaskResult SetTaxStatus(int Id, bool status);
        #endregion

        #region NumeroFical
        /////Metodo para traer el datos del nfccompleto
        ///// </summary> 
        ///// <returns></returns>
        IQueryable<Ncfs> ListVoucher(long businessID, int Id);



        /// <summary>
        /// Metodo para obtener los tipo NFC registrados de la empresa
        /// </summary>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        IQueryable<TblNcfprefix> GettypeNcf(long businessId);
        /// <summary>
        /// Metodo para obtener los NFC registrados de la empresa
        /// </summary>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        IQueryable<Ncfs> GetNcfs(long businessId);

        /// <summary>
        /// Metodo para obtener un Ncf
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="businessId"></param>
        /// <returns></returns>
        TblNcf GetNcf(int Id, long businessId);

        /// <summary>
        /// Metodo para registrar un Ncf
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion del Ncf</param>
        /// <returns></returns>
        TaskResult CreateNcf(TblNcf Ncf);

        /// <summary>
        /// metodo para actualizar un Ncf
        /// </summary>
        /// <param name="model">Entidad con la informacion</param>
        /// <returns></returns>
        TaskResult UpdateNcf(TblNcf Ncf);

        /// <summary>
        /// Metodo para cambiar de estado un nfc
        /// </summary>
        /// <param name="Id">Primary key de la nfc</param>
        /// <param name="status">Estado</param>
        /// <returns></returns>
        TaskResult SetNcfStatus(int Id, bool status, long businessId);
        #endregion

        /// <summary>
        /// Proceso para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
