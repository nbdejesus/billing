﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Microsoft.Extensions.Configuration;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Repositories;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static QuickSal.DataAcces.QuickSalEnum;

namespace QuickSal.Services
{
    public class AmazonService : IAmazonService
    {
        private IServiceError logService;
        private IGlobalRepository globalRepository;


        public AmazonService(IServiceError logService, IGlobalRepository globalRepository)
        {
            this.logService = logService;
            this.globalRepository = globalRepository;

        }

        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.USEast1;
        private static IAmazonS3 s3Client;

        /// <summary>
        /// Metodo para cargar los archivos a Amazon web service
        /// </summary>
        /// <param name="filePath">Ruta donde se encuentra el archivo</param>
        /// <param name="keyName">Nombre que tendrá el archivo</param>
        /// <param name="typeFile">Tipo de archivo</param>
        /// <returns></returns>
        public async Task<TaskResult> UploadFile(string filePath, string keyName, TypeFiles typeFile)
        {
            try
            {
                var amazon = await globalRepository.GetSettingValue("amazon");
                var setting = amazon.ToList();

                string subDirectoryInBucket = string.Empty;
                string accesKey = setting.Where(x => x.ValueName == "AccesKey").FirstOrDefault().SettingValue;
                string secretAccesKey = setting.Where(x => x.ValueName == "SecretAccesKey").FirstOrDefault().SettingValue;
                string bucketName = setting.Where(x => x.ValueName == "bucketName").FirstOrDefault().SettingValue;


                s3Client = new AmazonS3Client(accesKey, secretAccesKey, bucketRegion);
                var fileTransferUtility = new TransferUtility(s3Client);

                if (typeFile == TypeFiles.BusinessLogo)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "BusinessLogo").FirstOrDefault().SettingValue;
                else if (typeFile == TypeFiles.EmployeePhoto)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "EmployeePhoto").FirstOrDefault().SettingValue;
                else if (typeFile == TypeFiles.CustomerPhoto)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "CustomerPhoto").FirstOrDefault().SettingValue;
                else if (typeFile == TypeFiles.SupplierPhoto)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "SupplierPhoto").FirstOrDefault().SettingValue;
                else if (typeFile == TypeFiles.Product)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "ProductLogo").FirstOrDefault().SettingValue;
                else if (typeFile == TypeFiles.ProfilePhoto)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "ProfilePhoto").FirstOrDefault().SettingValue;


                string awsPath = string.Empty;
                if (subDirectoryInBucket == "" || subDirectoryInBucket == null)
                {
                    awsPath = bucketName; //no subdirectory just bucket name
                }
                else
                {   // subdirectory and bucket name
                    awsPath = bucketName + @"/" + subDirectoryInBucket;
                }

                // Option 4. Specify advanced settings.
                var fileTransferUtilityRequest = new TransferUtilityUploadRequest
                {
                    BucketName = awsPath,
                    FilePath = filePath,
                    StorageClass = S3StorageClass.StandardInfrequentAccess,
                    PartSize = 6291456, // 6 MB.
                    Key = keyName,
                    CannedACL = S3CannedACL.PublicRead
                };


                await fileTransferUtility.UploadAsync(fileTransferUtilityRequest);

                return logService.TaskStatus();
            }
            catch (AmazonS3Exception e)
            {
                logService.LogError(e.Message, "UploadFile", "AmazonService", "Tuvimos un problema al cargar el archivo");
                return logService.TaskStatus();
            }
            catch (Exception e)
            {
                logService.LogError(e.Message, "UploadFile", "AmazonService", "Tuvimos un problema al cargar el archivo");
                return logService.TaskStatus();
            }
        }


        /// <summary>
        /// Metodo para cargar un archivo a la carpeta temporal de quicksal
        /// </summary>
        /// <param name="file">Archivo en memoria</param>
        /// <param name="keyName">Nombre del archivo</param>
        /// <returns></returns>
        public async Task<TaskResult> UploadTempFile(Stream file, string keyName)
        {
            try
            {
                TaskResult task = new TaskResult();
                var amazon = await globalRepository.GetSettingValue("amazon");
                var setting = amazon.ToList();

                string subDirectoryInBucket = setting.Where(x => x.ValueName == "Temp").FirstOrDefault().SettingValue;
                string accesKey = setting.Where(x => x.ValueName == "AccesKey").FirstOrDefault().SettingValue;
                string secretAccesKey = setting.Where(x => x.ValueName == "SecretAccesKey").FirstOrDefault().SettingValue;
                string bucketName = setting.Where(x => x.ValueName == "bucketName").FirstOrDefault().SettingValue;

                //S3CannedACL s3CannedACL = new S3CannedACL(accesKey);
                //s3CannedACL.Value = S3CannedACL.PublicRead;
                s3Client = new AmazonS3Client(accesKey, secretAccesKey, bucketRegion);
                var fileTransferUtility = new TransferUtility(s3Client);


                string awsPath = string.Empty;
                if (subDirectoryInBucket == "" || subDirectoryInBucket == null)
                {
                    awsPath = bucketName; //no subdirectory just bucket name
                }
                else
                {   // subdirectory and bucket name
                    awsPath = bucketName + @"/" + subDirectoryInBucket;
                }


                await fileTransferUtility.UploadAsync(file, awsPath, keyName);


                task.ExecutedSuccesfully = true;
                return task;
            }
            catch (AmazonS3Exception e)
            {
                logService.LogError(e.Message, "UploadFile", "AmazonService", "Tuvimos un problema al cargar el archivo");
                return logService.TaskStatus();
            }
            catch (Exception e)
            {
                logService.LogError(e.Message, "UploadFile", "AmazonService", "Tuvimos un problema al cargar el archivo");
                return logService.TaskStatus();
            }
        }


        /// <summary>
        /// Metodo para descargar los archivos a Amazon web service
        /// </summary>
        /// <param name="SaveFilePath">Ruta donde se va a salvar el archivo temporal</param>
        /// <param name="typeFile">Tipo de archivo</param>
        /// <returns></returns>
        public async Task<MemoryStream> DownloadFile(string keyFile)
        {
            try
            {
                var amazon = await globalRepository.GetSettingValue("amazon");
                var setting = amazon.ToList();

                string accesKey = setting.Where(x => x.ValueName == "AccesKey").FirstOrDefault().SettingValue;
                string secretAccesKey = setting.Where(x => x.ValueName == "SecretAccesKey").FirstOrDefault().SettingValue;
                string bucketName = setting.Where(x => x.ValueName == "bucketName").FirstOrDefault().SettingValue;

                s3Client = new AmazonS3Client(accesKey, secretAccesKey, bucketRegion);
                var fileTransferUtility = new TransferUtility(s3Client);


                GetObjectResponse response = await s3Client.GetObjectAsync(bucketName, keyFile);
                MemoryStream memoryStream = new MemoryStream();

                using (Stream responseStream = response.ResponseStream)
                {
                    responseStream.CopyTo(memoryStream);
                }

                return memoryStream;
            }
            catch (AmazonS3Exception e)
            {
                logService.LogError(e.Message, "DownloadFile", "AmazonService", "Tuvimos un problema al descargar el archivo");
                return null;
            }
            catch (Exception e)
            {
                logService.LogError(e.Message, "DownloadFile", "AmazonService", "Tuvimos un problema al descargar el archivo");
                return null;
            }
        }

        /// <summary>
        /// Metodo para eliminar un archivo
        /// </summary>
        /// <param name="keyFile">Llave o nombre del archivo</param>
        /// <param name="typeFile">Tipo de archivo que se desea eliminar</param>
        /// <returns></returns>
        public async Task<TaskResult> DeleteFile(string keyFile, TypeFiles typeFile)
        {
            try
            {
                var amazon = await globalRepository.GetSettingValue("amazon");
                var setting = amazon.ToList();

                string subDirectoryInBucket = string.Empty;
                string accesKey = setting.Where(x => x.ValueName == "AccesKey").FirstOrDefault().SettingValue;
                string secretAccesKey = setting.Where(x => x.ValueName == "SecretAccesKey").FirstOrDefault().SettingValue;
                string bucketName = setting.Where(x => x.ValueName == "bucketName").FirstOrDefault().SettingValue;

                s3Client = new AmazonS3Client(accesKey, secretAccesKey, bucketRegion);

                if (typeFile == TypeFiles.BusinessLogo)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "BusinessLogo").FirstOrDefault().SettingValue;
                else if (typeFile == TypeFiles.EmployeePhoto)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "EmployeePhoto").FirstOrDefault().SettingValue;
                else if (typeFile == TypeFiles.CustomerPhoto)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "CustomerPhoto").FirstOrDefault().SettingValue;
                else if (typeFile == TypeFiles.SupplierPhoto)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "SupplierPhoto").FirstOrDefault().SettingValue;
                else if (typeFile == TypeFiles.Product)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "ProductLogo").FirstOrDefault().SettingValue;
                else if (typeFile == TypeFiles.ProfilePhoto)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "ProfilePhoto").FirstOrDefault().SettingValue;


                await s3Client.DeleteObjectAsync(bucketName + "/" + subDirectoryInBucket, keyFile);
                return logService.TaskStatus();
            }
            catch (AmazonS3Exception e)
            {
                logService.LogError(e.Message, "DownloadFile", "AmazonService", "Error encontrado al intentar actualizar el archivo.");
                return logService.TaskStatus();
            }
            catch (Exception e)
            {
                logService.LogError(e.Message, "DownloadFile", "AmazonService", "Error encontrado al intentar actualizar el archivo.");
                return logService.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para obtener una url temporal del archivo en la carpeta temporal
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <returns></returns>
        public async Task<string> GetTempUrlObjet(string fileName)
        {
            try
            {
                var amazon = await globalRepository.GetSettingValue("amazon");
                var setting = amazon.ToList();

                string subDirectoryInBucket = setting.Where(x => x.ValueName == "Temp").FirstOrDefault().SettingValue; ;
                string accesKey = setting.Where(x => x.ValueName == "AccesKey").FirstOrDefault().SettingValue;
                string secretAccesKey = setting.Where(x => x.ValueName == "SecretAccesKey").FirstOrDefault().SettingValue;
                string bucketName = setting.Where(x => x.ValueName == "bucketName").FirstOrDefault().SettingValue;


                s3Client = new AmazonS3Client(accesKey, secretAccesKey, bucketRegion);
                var fileTransferUtility = new TransferUtility(s3Client);


                string awsPath = string.Empty;
                if (subDirectoryInBucket == "" || subDirectoryInBucket == null)
                {
                    awsPath = bucketName; //no subdirectory just bucket name
                }
                else
                {   // subdirectory and bucket name
                    awsPath = bucketName + @"/" + subDirectoryInBucket;
                }

                // Create a CopyObject request
                GetPreSignedUrlRequest request = new GetPreSignedUrlRequest
                {
                    BucketName = awsPath,
                    Key = fileName,
                    Expires = DateTime.UtcNow.AddHours(3)
                };


                // Get path for request
                string path = s3Client.GetPreSignedURL(request);

                return path;
            }
            catch (AmazonS3Exception e)
            {
                logService.LogError(e.Message, "DownloadFile", "AmazonService", "Error encontrado al intentar actualizar el archivo");
                return null;
            }
            catch (Exception e)
            {
                logService.LogError(e.Message, "DownloadFile", "AmazonService", "Error encontrado al intentar actualizar el archivo");
                return null;
            }
        }



        /// <summary>
        /// Metodo para obtener una url de un archivo, el tiempo de esta url es de 30 minutos
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <param name="typeFile">Tipo de archivo</param>
        /// <returns></returns>
        public async Task<string> GetUrlObjet(string fileName, TypeFiles typeFile)
        {
            try
            {
                var amazon = await globalRepository.GetSettingValue("amazon");
                var setting = amazon.ToList();
                string subDirectoryInBucket = string.Empty;

                string accesKey = setting.Where(x => x.ValueName == "AccesKey").FirstOrDefault().SettingValue;
                string secretAccesKey = setting.Where(x => x.ValueName == "SecretAccesKey").FirstOrDefault().SettingValue;
                string bucketName = setting.Where(x => x.ValueName == "bucketName").FirstOrDefault().SettingValue;


                s3Client = new AmazonS3Client(accesKey, secretAccesKey, bucketRegion);
                var fileTransferUtility = new TransferUtility(s3Client);

                if (typeFile == TypeFiles.BusinessLogo)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "BusinessLogo").FirstOrDefault().SettingValue;
                else if (typeFile == TypeFiles.EmployeePhoto)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "EmployeePhoto").FirstOrDefault().SettingValue;
                else if (typeFile == TypeFiles.CustomerPhoto)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "CustomerPhoto").FirstOrDefault().SettingValue;
                else if (typeFile == TypeFiles.SupplierPhoto)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "SupplierPhoto").FirstOrDefault().SettingValue;
                else if (typeFile == TypeFiles.Product)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "ProductLogo").FirstOrDefault().SettingValue;
                else if (typeFile == TypeFiles.ProfilePhoto)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "ProfilePhoto").FirstOrDefault().SettingValue;


                string awsPath = string.Empty;
                if (subDirectoryInBucket == "" || subDirectoryInBucket == null)
                {
                    awsPath = bucketName; //no subdirectory just bucket name
                }
                else
                {   // subdirectory and bucket name
                    awsPath = bucketName + @"/" + subDirectoryInBucket;
                }

                // Create a CopyObject request
                GetPreSignedUrlRequest request = new GetPreSignedUrlRequest
                {
                    BucketName = awsPath,
                    Key = fileName,
                    Expires = DateTime.UtcNow.AddMinutes(30)
                };


                // Get path for request
                string path = s3Client.GetPreSignedURL(request);

                return path;
            }
            catch (AmazonS3Exception e)
            {
                logService.LogError(e.Message, "DownloadFile", "AmazonService", "Tuvimos un problema al obtener la url de la imagen.");
                return null;
            }
            catch (Exception e)
            {
                logService.LogError(e.Message, "DownloadFile", "AmazonService", "Tuvimos un problema al obtener la url de la imagen.");
                return null;
            }
        }


        /// <summary>
        /// Metodo para mover una foto desde su carpeta temporal hasta su carpeta de destino
        /// </summary>
        /// <param name="keyFile">Llave o nombre del archivo</param>
        /// <param name="typeFile">Tipo de archivo que se desea eliminar</param>
        /// <returns></returns>
        public async Task<TaskResult> MoveFileFromTempFolder(string keyFile, TypeFiles typeFile)
        {
            try
            {
                var amazon = await globalRepository.GetSettingValue("amazon");
                var setting = amazon.ToList();

                string subDirectoryInBucket = string.Empty;
                string TempSubDirectoryInBucket = setting.Where(x => x.ValueName == "Temp").FirstOrDefault().SettingValue;
                string accesKey = setting.Where(x => x.ValueName == "AccesKey").FirstOrDefault().SettingValue;
                string secretAccesKey = setting.Where(x => x.ValueName == "SecretAccesKey").FirstOrDefault().SettingValue;
                string bucketName = setting.Where(x => x.ValueName == "bucketName").FirstOrDefault().SettingValue;

                s3Client = new AmazonS3Client(accesKey, secretAccesKey, bucketRegion);

                if (typeFile == TypeFiles.BusinessLogo)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "BusinessLogo").FirstOrDefault().SettingValue;
                else if (typeFile == TypeFiles.EmployeePhoto)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "EmployeePhoto").FirstOrDefault().SettingValue;
                else if (typeFile == TypeFiles.CustomerPhoto)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "CustomerPhoto").FirstOrDefault().SettingValue;
                else if (typeFile == TypeFiles.SupplierPhoto)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "SupplierPhoto").FirstOrDefault().SettingValue;
                else if (typeFile == TypeFiles.Product)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "ProductLogo").FirstOrDefault().SettingValue;
                else if (typeFile == TypeFiles.ProfilePhoto)
                    subDirectoryInBucket = setting.Where(x => x.ValueName == "ProfilePhoto").FirstOrDefault().SettingValue;


                var copy = await s3Client.CopyObjectAsync(bucketName + "/" + TempSubDirectoryInBucket, keyFile, bucketName + "/" + subDirectoryInBucket, keyFile);

                await s3Client.DeleteObjectAsync(bucketName + "/" + TempSubDirectoryInBucket, keyFile);

                TaskResult task = new TaskResult();
                if (copy.HttpStatusCode == System.Net.HttpStatusCode.OK)
                    task.ExecutedSuccesfully = true;
                else
                {
                    task.ExecutedSuccesfully = false;
                    task.AddErrorMessage("El proceso de carga o de eliminar foto presentó problemas");
                }

                return task;
            }
            catch (AmazonS3Exception e)
            {
                logService.LogError(e.Message, "DownloadFile", "AmazonService", "Tuvimos un problema al copiar el archivo.");
                return logService.TaskStatus();
            }
            catch (Exception e)
            {
                logService.LogError(e.Message, "DownloadFile", "AmazonService", "Tuvimos un problema al copiar el archivo.");
                return logService.TaskStatus();
            }
        }
    }

    public interface IAmazonService
    {
        /// <summary>
        /// Metodo para cargar los archivos a Amazon web service
        /// </summary>
        /// <param name="filePath">Ruta donde se encuentra el archivo</param>
        /// <param name="keyName">Nombre que tendrá el archivo</param>
        /// <param name="typeFile">Tipo de archivo</param>
        /// <returns></returns>
        Task<TaskResult> UploadFile(string filePath, string keyName, TypeFiles typeFile);


        /// <summary>
        /// Metodo para cargar un archivo a la carpeta temporal de quicksal
        /// </summary>
        /// <param name="file">Archivo en memoria</param>
        /// <param name="keyName">Nombre del archivo</param>
        /// <returns></returns>
        Task<TaskResult> UploadTempFile(Stream file, string keyName);


        /// <summary>
        /// Metodo para descargar los archivos a Amazon web service
        /// </summary>
        /// <param name="SaveFilePath">Ruta donde se va a salvar el archivo temporal</param>
        /// <param name="typeFile">Tipo de archivo</param>
        /// <returns></returns>
        Task<MemoryStream> DownloadFile(string keyFile);

        /// <summary>
        /// Metodo para obtener una url temporal del archivo en la carpeta temporal
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <returns></returns>
        Task<string> GetTempUrlObjet(string fileName);


        /// <summary>
        /// Metodo para obtener una url de un archivo, el tiempo de esta url es de 30 minutos
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <param name="typeFile">Tipo de archivo</param>
        /// <returns></returns>
        Task<string> GetUrlObjet(string fileName, TypeFiles typeFile);


        /// <summary>
        /// Metodo para mover una foto desde su carpeta temporal hasta su carpeta de destino
        /// </summary>
        /// <param name="keyFile">Llave o nombre del archivo</param>
        /// <param name="typeFile">Tipo de archivo que se desea eliminar</param>
        /// <returns></returns>
        Task<TaskResult> MoveFileFromTempFolder(string keyFile, TypeFiles typeFile);


        /// <summary>
        /// Metodo para eliminar un archivo
        /// </summary>
        /// <param name="keyFile">Llave o nombre del archivo</param>
        /// <param name="typeFile">Tipo de archivo que se desea eliminar</param>
        /// <returns></returns>
        Task<TaskResult> DeleteFile(string keyFile, TypeFiles typeFile);
    }
}
