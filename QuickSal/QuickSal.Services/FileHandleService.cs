﻿using ClosedXML.Excel;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.CustomModels.Models.LoadData;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.Services
{
    public class FileHandleService : IFileHandleService
    {
        private IServiceError serviceError;
        private IAmazonService amazonService;
        private IGlobalService globalService;
        private ISecurityService securityService;
        private ISupplierRepository supplierRepository;
        private ICategoryRepository categoryRepository;
        private IMeasureRepository measureRepository;
        private ITaxRepository taxRepository;
        private ICustomerService customerService;
        private ICustomerRepository customerRepository;
        private ISupplierService supplierService;
        private IWarehouseRepository warehouseRepository;
        private IInventoryService InventoryService;

        public FileHandleService(IServiceError serviceError, IAmazonService amazonService, IGlobalService globalService, IMeasureRepository measureRepository,
            ISecurityService securityService, ISupplierRepository supplierRepository, ICategoryRepository categoryRepository, ITaxRepository taxRepository,
            ICustomerService customerService, ICustomerRepository customerRepository, ISupplierService supplierService, IWarehouseRepository warehouseRepository,
            IInventoryService InventoryService)
        {
            this.serviceError = serviceError;
            this.amazonService = amazonService;
            this.globalService = globalService;
            this.securityService = securityService;
            this.supplierRepository = supplierRepository;
            this.categoryRepository = categoryRepository;
            this.measureRepository = measureRepository;
            this.taxRepository = taxRepository;
            this.customerService = customerService;
            this.customerRepository = customerRepository;
            this.supplierService = supplierService;
            this.warehouseRepository = warehouseRepository;
            this.InventoryService = InventoryService;
        }

        #region Excel template
        /// <summary>
        /// Metodo para obtener la plantilla para la carga de datos de los clientes
        /// </summary>
        /// <returns></returns>
        public async Task<MemoryStream> GetCustomerTemplate()
        {
            try
            {
                var setting = await globalService.GetSettingValue("amazon");
                string key = setting.Where(x => x.ValueName == "PlantillaClientes").FirstOrDefault().SettingValue;

                var excelInMemory = await amazonService.DownloadFile(key);
                var workbook = new XLWorkbook(excelInMemory);
                IXLWorksheet workSheet = workbook.Worksheet(1);

                int startCell = 4;
                int limite = 1000;
                int totalCells = 1048576;

                #region Exportando tipo de documento
                //Agregamos las opciones de tipo de indentificación a las celdas
                var tipoDocumento = globalService.GetAllDocumentIdentification();
                int totalDocument = tipoDocumento.Count();

                workSheet.Cell(totalCells - totalDocument, "XFD").InsertData(tipoDocumento.Select(x => x.Name));
                for (int i = startCell; i <= limite; i++)
                    workSheet.Cell(i, "C").DataValidation.List(workSheet.Range("XFD" + (totalCells - totalDocument) + ":XFD" + totalCells));

                #endregion

                #region Exportando tipo de credito
                //Obtenemos los nombres de los tipos de credito
                List<string> NombresEnum = new List<string>();
                foreach (var item in Enum.GetValues(typeof(Support.EnumTypes.CreditType)))
                    NombresEnum.Add(globalService.GetListDesc((Support.EnumTypes.CreditType)item));


                //Los enviamos al excel
                int totalNameEnum = NombresEnum.Count();
                workSheet.Cell(totalCells - totalNameEnum, "XFC").InsertData(NombresEnum);
                for (int i = startCell; i <= limite; i++)
                    workSheet.Cell(i, "I").DataValidation.List(workSheet.Range("XFC" + (totalCells - totalNameEnum) + ":XFC" + totalCells));

                #endregion

                #region Exportando tipo de comprobante
                //Agregamos las opciones de tipo de indentificación a las celdas
                var tipoNCF = globalService.GetAllPrefixNCF();


                int totalNCF = tipoNCF.Count();
                workSheet.Cell(totalCells - totalNCF, "XFB").InsertData(tipoNCF.Select(x => x.Description));
                for (int i = startCell; i <= limite; i++)
                    workSheet.Cell(i, "J").DataValidation.List(workSheet.Range("XFB" + (totalCells - totalNCF) + ":XFB" + totalCells));
                #endregion


                MemoryStream stream = new MemoryStream();
                workbook.SaveAs(stream);

                return stream;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "GetCustomerTemplate", "CustomerService");
                serviceError.WarningMessage("Error al obtener la plantilla.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para obtener la plantilla para la carga de datos de los proveedores
        /// </summary>
        /// <returns></returns>
        public async Task<MemoryStream> GetSuplierTemplate()
        {
            try
            {
                var setting = await globalService.GetSettingValue("amazon");
                string key = setting.Where(x => x.ValueName == "PlantillaProveedores").FirstOrDefault().SettingValue;

                var excelInMemory = await amazonService.DownloadFile(key);
                var workbook = new XLWorkbook(excelInMemory);
                IXLWorksheet workSheet = workbook.Worksheet(1);

                int startCell = 4;
                int limite = 1000;
                int totalCells = 1048576;

                #region Exportando tipo de documento
                //Agregamos las opciones de tipo de indentificación a las celdas
                var tipoDocumento = globalService.GetAllDocumentIdentification();
                int totalDocument = tipoDocumento.Count();

                workSheet.Cell(totalCells - totalDocument, "XFD").InsertData(tipoDocumento.Select(x => x.Name));
                for (int i = startCell; i <= limite; i++)
                    workSheet.Cell(i, "C").DataValidation.List(workSheet.Range("XFD" + (totalCells - totalDocument) + ":XFD" + totalCells));

                #endregion

                #region Exportando tipo de credito
                //Obtenemos los nombres de los tipos de credito
                List<string> NombresEnum = new List<string>();
                foreach (var item in Enum.GetValues(typeof(Support.EnumTypes.CreditType)))
                    NombresEnum.Add(globalService.GetListDesc((Support.EnumTypes.CreditType)item));


                //Los enviamos al excel
                int totalNameEnum = NombresEnum.Count();
                workSheet.Cell(totalCells - totalNameEnum, "XFC").InsertData(NombresEnum);
                for (int i = startCell; i <= limite; i++)
                    workSheet.Cell(i, "J").DataValidation.List(workSheet.Range("XFC" + (totalCells - totalNameEnum) + ":XFC" + totalCells));

                #endregion

                #region Exportando tipo de comprobante
                //Agregamos las opciones de tipo de indentificación a las celdas
                var tipoNCF = globalService.GetAllPrefixNCF();


                int totalNCF = tipoNCF.Count();
                workSheet.Cell(totalCells - totalNCF, "XFB").InsertData(tipoNCF.Select(x => x.Description));
                for (int i = startCell; i <= limite; i++)
                    workSheet.Cell(i, "K").DataValidation.List(workSheet.Range("XFB" + (totalCells - totalNCF) + ":XFB" + totalCells));
                #endregion

                MemoryStream stream = new MemoryStream();
                workbook.SaveAs(stream);

                return stream;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "GetSuplierTemplate", "SupplierService");
                serviceError.WarningMessage("Error al obtener la plantilla");
                return null;
            }
        }

        /// <summary>
        /// Metodo para obtener la plantilla para la carga de datos de los productos
        /// </summary>
        /// <returns></returns>
        public async Task<MemoryStream> GetProductTemplate()
        {
            try
            {
                var setting = await globalService.GetSettingValue("amazon");
                string key = setting.Where(x => x.ValueName == "PlantillaProductos").FirstOrDefault().SettingValue;
                var session = securityService.GetSession();

                var excelInMemory = await amazonService.DownloadFile(key);
                var workbook = new XLWorkbook(excelInMemory);
                IXLWorksheet workSheet = workbook.Worksheet(1);

                int startCell = 4;
                int limite = 1000;
                int totalCells = 1048576;

                #region Exportando listado de proveedores
                //Agregamos las opciones de tipo de indentificación a las celdas
                var suppliers = supplierRepository.Get(x => x.Id > 0).Select(x => x.Names).ToList();
                int totalSuppliers = suppliers.Count();

                workSheet.Cell(totalCells - totalSuppliers, "XFD").InsertData(suppliers);
                for (int i = startCell; i <= limite; i++)
                    workSheet.Cell(i, "C").DataValidation.List(workSheet.Range("XFD" + (totalCells - totalSuppliers) + ":XFD" + totalCells));

                #endregion           

                #region Exportando listado de medidas
                var measurements = measureRepository.GetMeasurements(session.BusinessId);

                int totalMeasurements = measurements.Count();
                if (totalMeasurements > 0)
                {
                    workSheet.Cell(totalCells - totalMeasurements, "XFB").InsertData(measurements.Select(x => x.Name));
                    for (int i = startCell; i <= limite; i++)
                        workSheet.Cell(i, "D").DataValidation.List(workSheet.Range("XFB" + (totalCells - totalMeasurements) + ":XFB" + totalCells));
                }

                #endregion

                #region Exportando listado de Impuestos
                var taxes = taxRepository.GetTaxes(session.BusinessId);

                int totalTaxes = taxes.Count();
                if (totalTaxes > 0)
                {
                    workSheet.Cell(totalCells - totalTaxes, "XFA").InsertData(taxes.Select(x => x.Abbreviation));
                    for (int i = startCell; i <= limite; i++)
                    {
                        workSheet.Cell(i, "G").DataValidation.List(workSheet.Range("XFA" + (totalCells - totalTaxes) + ":XFA" + totalCells));
                        workSheet.Cell(i, "J").DataValidation.List(workSheet.Range("XFA" + (totalCells - totalTaxes) + ":XFA" + totalCells));
                    }
                }

                #endregion

                #region Exportando opciones para la configuración
                List<string> YesNo = new List<string> { "Si", "No" };
                int totalYesNo = YesNo.Count();

                workSheet.Cell(totalCells - totalYesNo, "XEZ").InsertData(YesNo);
                for (int i = startCell; i <= limite; i++)
                {
                    workSheet.Cell(i, "M").DataValidation.List(workSheet.Range("XEZ" + (totalCells - totalYesNo) + ":XEZ" + totalCells));
                    workSheet.Cell(i, "N").DataValidation.List(workSheet.Range("XEZ" + (totalCells - totalYesNo) + ":XEZ" + totalCells));
                    workSheet.Cell(i, "O").DataValidation.List(workSheet.Range("XEZ" + (totalCells - totalYesNo) + ":XEZ" + totalCells));
                }

                #endregion

                MemoryStream stream = new MemoryStream();
                workbook.SaveAs(stream);

                return stream;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "GetProductTemplate", "InventoryService");
                serviceError.WarningMessage("Error al obtener la plantilla");
                return null;
            }
        }

        /// <summary>
        /// Metodo para procesar los template que se cargan al aplicativo
        /// </summary>
        /// <param name="file">MemoryStream que contiene el template a cargar</param>
        /// <returns></returns>
        public async Task<List<TemplateStatus>> ProcessTemplate(MemoryStream file)
        {
            var workbook = new XLWorkbook(file);
            IXLWorksheet workSheet = workbook.Worksheet(1);
            string title = workSheet.Cell(1, "A").Value.ToString();
            List<TemplateStatus> statusTemplate = new List<TemplateStatus>();

            if (title.Contains("Listado de Clientes"))
                return await ProcessCustomerTemplate(file);
            else if (title.Contains("Listado de Proveedores"))
                return await ProcessSupplierTemplate(file);
            else if (title.Contains("Listado de Productos"))
                return await ProcessProductTemplate(file);
            else
            {
                statusTemplate.Add(new TemplateStatus
                {
                    RowNumber = 1,
                    Object = "Error general",
                    Error = "Debe cargar una de las plantillas predeterminadas"
                });

                return statusTemplate;
            }

        }

        /// <summary>
        /// Metodo para procesar la plantilla de carga de un cliente
        /// </summary>
        /// <param name="file">MemoryStream que contiene el template a cargar</param>
        /// <returns></returns>
        private async Task<List<TemplateStatus>> ProcessCustomerTemplate(MemoryStream file)
        {
            List<TemplateStatus> statusTemplate = new List<TemplateStatus>();

            try
            {
                var session = securityService.GetSession();
                var workbook = new XLWorkbook(file);
                IXLWorksheet workSheet = workbook.Worksheet(1);

                int startCell = 4;
                int limite = 1000;
                bool emptyCode = workSheet.Range(startCell, 1, limite, 1).IsEmpty();
                bool emptyName = workSheet.Range(startCell, 2, limite, 2).IsEmpty();

                if (emptyCode && emptyCode)
                {
                    statusTemplate.Add(new TemplateStatus
                    {
                        RowNumber = 1,
                        Object = "Error general",
                        Error = "La plantilla debe tener datos."
                    });

                    return statusTemplate;
                }
                else
                {
                    //Obtenemos listado de los tipos de identificación
                    var DocumentType = globalService.GetAllDocumentIdentification();

                    //Obtenemos listado de los tipos de credito
                    Dictionary<byte, string> CreditType = new Dictionary<byte, string>();
                    foreach (var item in Enum.GetValues(typeof(Support.EnumTypes.CreditType)))
                        CreditType.Add(Convert.ToByte(item), globalService.GetListDesc((Support.EnumTypes.CreditType)item));


                    //Obtenemos listado de los tipos de comprobantes
                    var NCFType = globalService.GetAllPrefixNCF();
                    List<TblCustomers> Customers = new List<TblCustomers>();

                    for (int i = startCell; i <= limite; i++)
                    {
                        if (!workSheet.Cell(i, 1).IsEmpty() || !workSheet.Cell(i, 2).IsEmpty())
                        {
                            TblCustomers customer = new TblCustomers();
                            customer.Code = workSheet.Cell(i, "A").Value.ToString();
                            customer.Names = workSheet.Cell(i, "B").Value.ToString();
                            customer.BusinessId = session.BusinessId;

                            #region Obteniendo el ID TipoDocumento                
                            string DocumentTypeName = workSheet.Cell(i, "C").Value.ToString();

                            customer.DocumentTypeId = (from c in DocumentType
                                                       where c.Name == DocumentTypeName
                                                       select c.Id).FirstOrDefault();

                            #endregion

                            customer.DocumentNumber = workSheet.Cell(i, "D").Value.ToString();
                            customer.Movil = workSheet.Cell(i, "E").Value.ToString();
                            customer.PhoneNumber = workSheet.Cell(i, "F").Value.ToString();
                            customer.Email = workSheet.Cell(i, "G").Value.ToString();
                            customer.AddressCustomers = workSheet.Cell(i, "H").Value.ToString();

                            #region Obteniendo el ID Tipo de crédito                
                            string CreditTypeName = workSheet.Cell(i, "I").Value.ToString();
                            customer.CreditTypeId = (from c in CreditType
                                                     where c.Value == CreditTypeName
                                                     select c.Key).FirstOrDefault();

                            customer.CreditTypeId = customer.CreditTypeId == 0 ? null : customer.CreditTypeId;
                            #endregion

                            #region Obteniendo el ID Tipo de NCF                
                            string NcfTypeName = workSheet.Cell(i, "J").Value.ToString();
                            customer.Ncfid = (from c in NCFType
                                              where c.Description == NcfTypeName
                                              select c.Id).FirstOrDefault();


                            customer.Ncfid = customer.Ncfid == 0 ? null : customer.Ncfid;
                            #endregion

                            customer.CreditLimit = Convert.ToDecimal(((workSheet.Cell(i, "K").Value.ToString() != "") ? workSheet.Cell(i, "K").Value : 0));
                            customer.PendingBalance = Convert.ToDecimal(((workSheet.Cell(i, "L").Value.ToString() != "") ? workSheet.Cell(i, "L").Value : 0));
                            customer.Discount = Convert.ToDecimal(((workSheet.Cell(i, "M").Value.ToString() != "") ? workSheet.Cell(i, "M").Value : 0));
                            customer.Obervation = workSheet.Cell(i, "N").Value.ToString();

                            var result = await customerService.CreateCustomer(customer);

                            if (result.MessageList.Count > 0)
                                statusTemplate.Add(new TemplateStatus
                                {
                                    RowNumber = i,
                                    Object = customer.Names,
                                    Error = result.MessageList[result.MessageList.Count - 1]//obtenemos los ultimos errores
                                });

                        }
                        else
                            break;

                    }

                    if (statusTemplate.Count <= 0)
                    {
                        var task = await Savechanges();

                        if (task.ExecutedSuccesfully)
                            return null;
                        else
                        {
                            statusTemplate.Add(new TemplateStatus
                            {
                                RowNumber = 1,
                                Object = "Error general",
                                Error = "Error al procesar grabar los datos."//obtenemos los ultimos errores
                            });
                            return statusTemplate;
                        }

                    }
                    else
                        return statusTemplate;
                }
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "ProcessCustomerTemplate", "FileHandleService");

                statusTemplate.Add(new TemplateStatus
                {
                    RowNumber = 1,
                    Object = "Error general",
                    Error = "Error durante el procesamiento de la plantilla."//obtenemos los ultimos errores
                });
                return statusTemplate;
            }
        }


        /// <summary>
        /// Metodo para procesar la plantilla de carga de un proveedor
        /// </summary>
        /// <param name="file">MemoryStream que contiene el template a cargar</param>
        /// <returns></returns>
        private async Task<List<TemplateStatus>> ProcessSupplierTemplate(MemoryStream file)
        {
            List<TemplateStatus> statusTemplate = new List<TemplateStatus>();

            try
            {
                var session = securityService.GetSession();
                var workbook = new XLWorkbook(file);
                IXLWorksheet workSheet = workbook.Worksheet(1);

                int startCell = 4;
                int limite = 1000;
                bool emptyCode = workSheet.Range(startCell, 1, limite, 1).IsEmpty();
                bool emptyName = workSheet.Range(startCell, 2, limite, 2).IsEmpty();

                if (emptyCode && emptyCode)
                {
                    statusTemplate.Add(new TemplateStatus
                    {
                        RowNumber = 1,
                        Object = "Error general",
                        Error = "La plantilla debe tener datos."
                    });

                    return statusTemplate;
                }
                else
                {
                    //Obtenemos listado de los tipos de identificación
                    var DocumentType = globalService.GetAllDocumentIdentification();

                    //Obtenemos listado de los tipos de credito
                    Dictionary<byte, string> CreditType = new Dictionary<byte, string>();
                    foreach (var item in Enum.GetValues(typeof(Support.EnumTypes.CreditType)))
                        CreditType.Add(Convert.ToByte(item), globalService.GetListDesc((Support.EnumTypes.CreditType)item));


                    //Obtenemos listado de los tipos de comprobantes
                    var NCFType = globalService.GetAllPrefixNCF();
                    List<TblSuppliers> suppliers = new List<TblSuppliers>();


                    for (int i = startCell; i <= limite; i++)
                    {
                        if (!workSheet.Cell(i, 1).IsEmpty() || !workSheet.Cell(i, 2).IsEmpty())
                        {
                            TblSuppliers supplier = new TblSuppliers();
                            supplier.Code = workSheet.Cell(i, "A").Value.ToString();
                            supplier.Names = workSheet.Cell(i, "B").Value.ToString();
                            supplier.BusinessId = session.BusinessId;

                            #region Obteniendo el ID TipoDocumento                
                            string DocumentTypeName = workSheet.Cell(i, "C").Value.ToString();
                            supplier.DocumentTypeId = (from c in DocumentType
                                                       where c.Name == DocumentTypeName
                                                       select c.Id).FirstOrDefault();
                            #endregion

                            supplier.DocumentNumber = workSheet.Cell(i, "D").Value.ToString();
                            supplier.Movil = workSheet.Cell(i, "E").Value.ToString();
                            supplier.PhoneNumber = workSheet.Cell(i, "F").Value.ToString();
                            supplier.Email = workSheet.Cell(i, "G").Value.ToString();
                            supplier.AddressSuppliers = workSheet.Cell(i, "H").Value.ToString();
                            supplier.ContactPerson = workSheet.Cell(i, "I").Value.ToString();

                            #region Obteniendo el ID Tipo de crédito                
                            string CreditTypeName = workSheet.Cell(i, "J").Value.ToString();
                            supplier.DocumentTypeId = (from c in CreditType
                                                       where c.Value == CreditTypeName
                                                       select c.Key).FirstOrDefault();
                            #endregion

                            #region Obteniendo el ID Tipo de NCF                
                            string NcfTypeName = workSheet.Cell(i, "K").Value.ToString();
                            supplier.Ncfid = (from c in NCFType
                                              where c.Description == NcfTypeName
                                              select c.Id).FirstOrDefault();

                            supplier.Ncfid = supplier.Ncfid == 0 ? null : supplier.Ncfid;
                            #endregion

                            supplier.PendingBalance = Convert.ToDecimal(((workSheet.Cell(i, "L").Value.ToString() != "") ? workSheet.Cell(i, "L").Value : 0));
                            supplier.Obervation = workSheet.Cell(i, "M").Value.ToString();

                            var result = await supplierService.CreateSupplier(supplier);

                            if (result.MessageList.Count > 0)
                                statusTemplate.Add(new TemplateStatus
                                {
                                    RowNumber = i,
                                    Object = supplier.Names,
                                    Error = result.MessageList[result.MessageList.Count - 1]//obtenemos los ultimos errores
                                });

                        }
                        else
                            break;
                    }

                    if (statusTemplate.Count <= 0)
                    {
                        var task = await Savechanges();

                        if (task.ExecutedSuccesfully)
                            return null;
                        else
                        {
                            statusTemplate.Add(new TemplateStatus
                            {
                                RowNumber = 1,
                                Object = "Error general",
                                Error = "Error al procesar grabar los datos."//obtenemos los ultimos errores
                            });
                            return statusTemplate;
                        }

                    }
                    else
                        return statusTemplate;
                }
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "ProcessSupplierTemplate", "FileHandleService");
                statusTemplate.Add(new TemplateStatus
                {
                    RowNumber = 1,
                    Object = "Error general",
                    Error = "Error durante el procesamiento de la plantilla."//obtenemos los ultimos errores
                });
                return statusTemplate;
            }
        }


        /// <summary>
        /// Metodo para procesar la plantilla de carga de un proveedor
        /// </summary>
        /// <param name="file">MemoryStream que contiene el template a cargar</param>
        /// <returns></returns>
        private async Task<List<TemplateStatus>> ProcessProductTemplate(MemoryStream file)
        {
            List<TemplateStatus> statusTemplate = new List<TemplateStatus>();

            try
            {
                var session = securityService.GetSession();
                var workbook = new XLWorkbook(file);
                IXLWorksheet workSheet = workbook.Worksheet(1);

                int startCell = 4;
                int limite = 1000;
                bool emptyCode = workSheet.Range(startCell, 1, limite, 1).IsEmpty();
                bool emptyName = workSheet.Range(startCell, 2, limite, 2).IsEmpty();


                if (emptyCode && emptyCode)
                {
                    statusTemplate.Add(new TemplateStatus
                    {
                        RowNumber = 1,
                        Object = "Error general",
                        Error = "La plantilla debe tener datos."
                    });

                    return statusTemplate;
                }
                else
                {

                    //Obtenemos el listado de objetos que utilizaremos para busquedas
                    var suppliers = supplierRepository.Get(x => x.BusinessId == session.BusinessId).ToList();
                    var measures = measureRepository.Get(x => x.BusinessId == session.BusinessId).ToList();
                    var taxes = taxRepository.GetTaxes(session.BusinessId);
                    var PrincipalWareHouse = warehouseRepository.Get(x => x.BranchOfficeId == session.BranchOfficeId && x.Principal).FirstOrDefault();

                    List<TblProducts> products = new List<TblProducts>();

                    for (int i = startCell; i <= limite; i++)
                    {
                        if (!workSheet.Cell(i, 1).IsEmpty() || !workSheet.Cell(i, 2).IsEmpty())
                        {

                            TblProducts product = new TblProducts();
                            product.Code = workSheet.Cell(i, "A").Value.ToString();
                            product.Name = workSheet.Cell(i, "B").Value.ToString();
                            product.BusinessId = session.BusinessId;

                            #region Obteniendo el ID del proveedor                
                            string SupplierName = workSheet.Cell(i, "C").Value.ToString();
                            product.SupplierId = (from c in suppliers
                                                  where c.Names == SupplierName
                                                  select c.Id).FirstOrDefault();

                            product.SupplierId = product.SupplierId == 0 ? null : product.SupplierId;
                            #endregion

                            #region Obteniendo el ID de la medida                
                            string measureName = workSheet.Cell(i, "D").Value.ToString();
                            product.MeasureId = (from c in measures
                                                 where c.Name == measureName
                                                 select c.Id).FirstOrDefault();

                            product.MeasureId = product.MeasureId == 0 ? null : product.MeasureId;
                            #endregion

                            product.Reference = workSheet.Cell(i, "E").Value.ToString();
                            product.Description = workSheet.Cell(i, "F").Value.ToString();

                            #region Obteniendo el ID del impuesto de compra                
                            string taxName = workSheet.Cell(i, "G").Value.ToString();
                            product.TaxId = (from c in taxes
                                             where c.Name == taxName
                                             select c.Id).FirstOrDefault();

                            product.TaxId = product.TaxId == 0 ? null : product.TaxId;
                            #endregion

                            product.Cost = Convert.ToDecimal(String.IsNullOrWhiteSpace(workSheet.Cell(i, "H").Value.ToString()) ? "0" : workSheet.Cell(i, "H").Value);

                            string productQuantity = workSheet.Cell(i, "I").Value.ToString();
                            if (!string.IsNullOrWhiteSpace(productQuantity))
                            {
                                TblInventoryWarehouse inventory = new TblInventoryWarehouse();
                                inventory.ProductAmount = Convert.ToDecimal(productQuantity);
                                inventory.WarehouseId = PrincipalWareHouse.Id;
                                product.TblInventoryWarehouse.Add(inventory);
                            }

                            #region Obteniendo el ID del impuesto de venta                
                            string taxNameSell = workSheet.Cell(i, "J").Value.ToString();
                            int TaxIdSell = (from c in taxes
                                             where c.Abbreviation == taxNameSell
                                             select c.Id).FirstOrDefault();
                            #endregion

                            string productPrice = workSheet.Cell(i, "L").Value.ToString();
                            string utility = workSheet.Cell(i, "K").Value.ToString();


                            TblProductsPrices price = new TblProductsPrices();
                            price.TaxId = TaxIdSell;
                            price.Price = Convert.ToDecimal(string.IsNullOrWhiteSpace(productPrice) ? "0" : productPrice);
                            price.Name = "Precio principal";
                            price.IsDefault = true;
                            price.Utility = Convert.ToDecimal(string.IsNullOrWhiteSpace(utility) ? "0" : utility);
                            product.TblProductsPrices.Add(price);


                            string isService = workSheet.Cell(i, "M").Value.ToString();
                            if (!string.IsNullOrWhiteSpace(isService))
                                product.IsService = isService.Contains("si") ? true : false;

                            string sellWithOutExit = workSheet.Cell(i, "N").Value.ToString();
                            if (!string.IsNullOrWhiteSpace(sellWithOutExit))
                                product.InvoiceWithoutExistence = sellWithOutExit.Contains("si") ? true : false;


                            string sellLowCost = workSheet.Cell(i, "O").Value.ToString();
                            if (!string.IsNullOrWhiteSpace(sellLowCost))
                                product.SellLowCost = sellLowCost.Contains("si") ? true : false;


                            product.Observation = workSheet.Cell(i, "P").Value.ToString();

                            TaskResult result = await InventoryService.CreateProduct(product);

                            if (result.MessageList.Count > 0)
                            {
                                statusTemplate.Add(new TemplateStatus
                                {
                                    RowNumber = i,
                                    Object = product.Name,
                                    Error = result.MessageList[result.MessageList.Count - 1]//obtenemos los ultimos errores
                                });

                                //quitamos el ultimo mensaje agregado
                                result.MessageList.RemoveAt(result.MessageList.Count - 1);
                            }
                        }
                        else
                            break;
                    }

                    if (statusTemplate.Count <= 0)
                    {
                        var task = await Savechanges();

                        if (task.ExecutedSuccesfully)
                            return null;
                        else
                        {
                            statusTemplate.Add(new TemplateStatus
                            {
                                RowNumber = 1,
                                Object = "Error general",
                                Error = "Error al procesar grabar los datos."//obtenemos los ultimos errores
                            });
                            return statusTemplate;
                        }

                    }
                    else
                        return statusTemplate;

                }
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "ProcessProductTemplate", "FileHandleService");
                statusTemplate.Add(new TemplateStatus
                {
                    RowNumber = 1,
                    Object = "Error general",
                    Error = "Error durante el procesamiento de la plantilla."//obtenemos los ultimos errores
                });
                return statusTemplate;
            }
        }
        #endregion


        /// <summary>
        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            var task = await customerRepository.Savechanges();

            if (!task.ExecutedSuccesfully)
                serviceError.LogError(task.MessageList[0], "Savechanges", "FileHandleController", "Error al intentar guardar los datos.");

            return task;
        }

    }

    public interface IFileHandleService
    {
        #region Excel template
        /// <summary>
        /// Metodo para obtener la plantilla para la carga de datos de los clientes
        /// </summary>
        /// <returns></returns>
        Task<MemoryStream> GetCustomerTemplate();

        /// <summary>
        /// Metodo para obtener la plantilla para la carga de datos de los proveedores
        /// </summary>
        /// <returns></returns>
        Task<MemoryStream> GetSuplierTemplate();

        /// <summary>
        /// Metodo para obtener la plantilla para la carga de datos de los productos
        /// </summary>
        /// <returns></returns>
        Task<MemoryStream> GetProductTemplate();


        /// <summary>
        /// Metodo para procesar los template que se cargan al aplicativo
        /// </summary>
        /// <param name="file">MemoryStream que contiene el template a cargar</param>
        /// <returns></returns>
        Task<List<TemplateStatus>> ProcessTemplate(MemoryStream file);
        #endregion
    }
}
