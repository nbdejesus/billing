﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using QuickSal.DataAcces.Models;
using QuickSal.Services.Support;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace QuickSal.Services.Services
{
    internal class ServiceNotifications : INotificationsService
    {
        private IConfiguration _Configuration;
        private IServiceError serviceError;
        private IHostingEnvironment hostingEnvironment;
        private IGlobalService globalService;

        public ServiceNotifications(IConfiguration configuration, IGlobalService globalService,
                                     IServiceError serviceError,
                                     IHostingEnvironment env)
        {
            this._Configuration = configuration;
            this.serviceError = serviceError;
            this.hostingEnvironment = env;
            this.globalService = globalService;
        }


        /// <summary>
        /// Metodo para enviar por correo el template de bienvenida
        /// </summary>
        /// <param name="UrlConfirmation">Url con la clave de confirmacion</param>       
        /// <returns></returns>
        public string WelcomeToQuicksal(string UrlConfirmation)
        {
            try
            {
                string templateChangePassword = globalService.GetSettingValue("LocalResources", "TemplateBienvenida").Result.FirstOrDefault().SettingValue;
                string templateMaster = globalService.GetSettingValue("LocalResources", "Master").Result.FirstOrDefault().SettingValue;

                string FilePath = hostingEnvironment.ContentRootPath + templateChangePassword;
                string masterPath = hostingEnvironment.ContentRootPath + templateMaster;
                string Template = File.ReadAllText(FilePath, Encoding.ASCII);

                string master = File.ReadAllText(masterPath);
                StringBuilder TemplateInMemory = new StringBuilder(Template);

                TemplateInMemory.Replace("{Ruta-Confirmacion}", UrlConfirmation);

                return TemplateInMemory.ToString();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "GetTemplateBienvenida", "ServiceNotifications");
                return null;
            }
        }

        /// <summary>
        /// Metodo para enviar la clave generada a un empleado registrado
        /// </summary>
        /// <param name="Password">Contrasena generada</param>
        /// <param name="businessName">Empresa a la que fue agregado</param>
        /// <param name="urlAplication">Url de la aplicacion para volver a entrar</param>
        /// <returns></returns>
        public string WelcomeToQuicksalEmployee(string Password, string businessName, string urlAplication)
        {
            try
            {
                string templateChangePassword = globalService.GetSettingValue("LocalResources", "TemplateBienvenidaEmpleado").Result.FirstOrDefault().SettingValue;
                string templateMaster = globalService.GetSettingValue("LocalResources", "Master").Result.FirstOrDefault().SettingValue;

                string FilePath = hostingEnvironment.ContentRootPath + templateChangePassword;
                string masterPath = hostingEnvironment.ContentRootPath + templateMaster;
                string Template = File.ReadAllText(FilePath, Encoding.ASCII);

                string master = File.ReadAllText(masterPath);
                StringBuilder TemplateInMemory = new StringBuilder(Template);

                TemplateInMemory.Replace("{business-name}", businessName);
                TemplateInMemory.Replace("{password-employee}", Password);
                TemplateInMemory.Replace("{url-aplication}", urlAplication);


                return TemplateInMemory.ToString();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "GetTemplateBienvenida", "ServiceNotifications");
                return null;
            }
        }

        /// <summary>
        /// Proceso para enviar un correo de confirmacion al usuario
        /// </summary>
        /// <param name="Email">Correo del usuario</param>
        /// <param name="keySend">Llave de confirmacion que se enviara</param>
        /// <returns></returns>
        public async Task<TaskResult> SendMailConfirmation(Mail MailModel)
        {
            string expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

            try
            {
                if (string.IsNullOrEmpty(MailModel.To))
                    return serviceError.WarningMessage("El correo electronico no puede estar en blanco.");
                else if (string.IsNullOrEmpty(MailModel.Body))
                    return serviceError.WarningMessage("El template no puede estar en blanco.");
                else if (!Regex.IsMatch(MailModel.To, expresion))
                    return serviceError.WarningMessage("El correo no tiene el formato correcto.");
                else
                    return await SendMail(MailModel);

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "SendMailConfirmation", "ServieNotification");
                return serviceError.TaskStatus();
            }
        }


        /// <summary>
        /// Proceso para generar el template de cambio de contrasena
        /// </summary>
        /// <param name="UrlConfirmation">Url con el ID de confirmacion de cuenta</param>
        /// <param name="titulo">Titulo para la confirmacion</param>
        /// <param name="email">Email del usuario</param>
        /// <returns></returns>
        public async Task<TaskResult> SendMailChangePassword(string UrlConfirmation, string titulo, string email)
        {
            try
            {
                string templateChangePassword = globalService.GetSettingValue("LocalResources", "TemplatePasswordChange").Result.FirstOrDefault().SettingValue;
                string templateMaster = globalService.GetSettingValue("LocalResources", "Master").Result.FirstOrDefault().SettingValue;

                string FilePath = hostingEnvironment.ContentRootPath + templateChangePassword;
                string masterPath = hostingEnvironment.ContentRootPath + templateMaster;
                string Template = File.ReadAllText(FilePath, Encoding.ASCII);

                string master = File.ReadAllText(masterPath);
                StringBuilder TemplateInMemory = new StringBuilder(Template);

                TemplateInMemory.Replace("{{Ruta-Confirmacion}}", UrlConfirmation);

                Mail mail = new Mail();
                mail.Subject = titulo;
                mail.To = email;
                mail.Body = master.Replace("{{MailContent}}", TemplateInMemory.ToString());

                return await SendMail(mail);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "SendConfirmation", "ServiceNotification", "Error intentando enviar el correo");
                return serviceError.TaskStatus();
            }
        }


        public async Task<TaskResult> SendMail(Mail MailModel)
        {
            try
            {
                var emailParametre = globalService.GetSettingValue("MailCredentials").Result;                

                string server = emailParametre.Where(x => x.ValueName == "email_server").FirstOrDefault().SettingValue;
                string username = emailParametre.Where(x => x.ValueName == "email_username").FirstOrDefault().SettingValue;
                string password = emailParametre.Where(x => x.ValueName == "email_password").FirstOrDefault().SettingValue;
                int port = int.Parse(emailParametre.Where(x => x.ValueName == "email_port").FirstOrDefault().SettingValue);
                bool UseSsl = bool.Parse(emailParametre.Where(x => x.ValueName == "email_ssl").FirstOrDefault().SettingValue);

                MailMessage mailMessage = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(server);
                SmtpServer.Port = port;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new NetworkCredential(username, password);
                SmtpServer.EnableSsl = UseSsl;

                mailMessage.From = new MailAddress(username);
                mailMessage.To.Add(String.Join(",", MailModel.To));

                mailMessage.Subject = MailModel.Subject;
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = MailModel.Body;

                AlternateView alternate = AlternateView
                    .CreateAlternateViewFromString(MailModel.Body, null, MediaTypeNames.Text.Html);

                await SmtpServer.SendMailAsync(mailMessage);
                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "SendMail", "ServieNotification", "Error intentando enviar correo.");
                return serviceError.TaskStatus();
            }
        }
    }

    public interface INotificationsService
    {
        Task<TaskResult> SendMailConfirmation(Mail MailModel);

        /// <summary>
        /// Metodo para enviar por correo el template de bienvenida
        /// </summary>
        /// <param name="UrlConfirmation">Url con la clave de confirmacion</param>       
        /// <returns></returns>
        string WelcomeToQuicksal(string UrlConfirmation);

        /// <summary>
        /// Metodo para enviar la clave generada a un empleado registrado
        /// </summary>
        /// <param name="Password">Contrasena generada</param>
        /// <param name="businessName">Empresa a la que fue agregado</param>
        /// <param name="urlAplication">Url de la aplicacion para volver a entrar</param>
        /// <returns></returns>
        string WelcomeToQuicksalEmployee(string Password, string businessName, string urlAplication);


        /// <summary>
        /// Proceso para generar el template de cambio de contrasena
        /// </summary>
        /// <param name="UrlConfirmation">Url con el ID de confirmacion de cuenta</param>
        /// <param name="titulo">Titulo para la confirmacion</param>
        /// <param name="email">Email del usuario</param>
        /// <returns></returns>
        Task<TaskResult> SendMailChangePassword(string UrlConfirmation, string titulo, string email);

        Task<TaskResult> SendMail(Mail MailModel);
    }
}
