﻿using QuickSal.DataAcces;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Repositories;
using QuickSal.DataAcces.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
//using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static QuickSal.DataAcces.QuickSalEnum;

namespace QuickSal.Services
{
    public class AccountService : IAccountService
    {
        private IUserRepository userRepository;
        private IServiceError serviceError;
        private IGlobalService globalService;
        private IUserConfirmationRepository userConfirmationRepository;
        private ISubscriptionsRepository subscriptionsRepository;
        private ISecurityService securityService;
        private IAmazonService amazonService;

        public AccountService(IUserRepository userRepository, ISecurityService securityService,
                                 IServiceError serviceError, IAmazonService amazonService,
                                 IGlobalService globalService,
                                 IUserConfirmationRepository userConfirmation,
                                 ISubscriptionsRepository subscriptionsRepository)
        {
            this.userRepository = userRepository;
            this.serviceError = serviceError;
            this.globalService = globalService;
            this.userConfirmationRepository = userConfirmation;
            this.subscriptionsRepository = subscriptionsRepository;
            this.securityService = securityService;
            this.amazonService = amazonService;
        }


        /// <summary>
        /// Proceso para verficar si existe un usuario registrado
        /// </summary>
        /// <param name="User">Clase que contiene los datos del usuario</param>
        /// <returns></returns>
        public TblUsers VerifyAccount(TblUsers User)
        {
            return userRepository.GetUser(User.Email);
        }

        /// <summary>
        /// Metodo para obtener el usuario
        /// </summary>
        /// <param name="Email">Email del usuario</param>
        /// <param name="password">Contrasena del usuario</param>
        /// <returns></returns>
        public async Task<TblUsers> GetUser(string email, string password)
        {
            string passwordEncript = globalService.SHA1Encrypt(password);
            return userRepository.Get(x => x.Email == email && x.UserPassword == passwordEncript).FirstOrDefault();
        }

        /// <summary>
        /// Metodo para generar una nueva llave para recuperar contrasena
        /// </summary>
        /// <param name="email">Email del usuario</param>
        /// <param name="key">Nueva llave generada</param>
        /// <returns></returns>
        public TaskResult GenerateConfirmationKey(string email, out string key)
        {
            key = string.Empty;

            try
            {
                var TypeOfKey = QuickSalEnum.TypeOfKey.RecuperacionContraseña;

                var task = ValidateGenerateConfirmationKey(email, TypeOfKey);

                if (task.ExecutedSuccesfully)
                {
                    var user = userRepository.Get(x => x.Email == email).FirstOrDefault();

                    //Generamos la clave de confirmación     
                    TblUserConfirmation Confirmacion = new TblUserConfirmation();
                    Confirmacion.ConfirmationKey = globalService.SHA1Encrypt(email + DateTime.Now.ToShortDateString());
                    Confirmacion.UserId = user.Id;
                    Confirmacion.ExpirationKey = DateTime.Now.AddDays(2);
                    Confirmacion.IdForms = (int)QuickSalEnum.TypeOfKey.RecuperacionContraseña;

                    userConfirmationRepository.AddEntity(Confirmacion);

                    key = Confirmacion.ConfirmationKey;
                    return serviceError.TaskStatus();
                }

                return task;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "RecoveryPassword", "AccountService", "Tuvimos un problema intentando generar key recuperación contraseña.");
                return serviceError.TaskStatus();
            }
        }


        /// <summary>
        /// Metodo para validar los datos del usuario al momento de obtener la nueva clave
        /// </summary>
        /// <param name="email">Email del usuario</param>
        /// <returns></returns>
        private TaskResult ValidateGenerateConfirmationKey(string email, QuickSalEnum.TypeOfKey TypeOfKey)
        {
            try
            {
                var isEmailValid = new EmailAddressAttribute();

                if (string.IsNullOrEmpty(email))
                    return serviceError.WarningMessage("El email es obligatorio.");
                else if (email.Length > 100)
                    return serviceError.WarningMessage("El tamaño del email no puede pasar los 100 caracteres.");
                else if (!isEmailValid.IsValid(email))
                    return serviceError.WarningMessage("Debe ingresar un email valido.");

                var user = userRepository.Get(x => x.Email == email).FirstOrDefault();

                if (user == null)
                    return serviceError.WarningMessage("Email no registrado.");

                if (userConfirmationRepository.Get(x => x.UserId == user.Id && x.ExpirationKey > DateTime.Now && x.IdForms == (byte)TypeOfKey).Any())
                    return serviceError.WarningMessage("Ya existe una clave confirmación generada, por favor verifica en tu correo.");

                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "ValidateGenerateConfirmationKey", "AccountService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        ///  Metodo para validar la recuperacion de contrasena
        /// </summary>
        /// <param name="UriSalt"></param>
        /// <returns></returns>
        public TaskResult ValidateRecoveryPassword(string UriSalt)
        {
            try
            {
                if (string.IsNullOrEmpty(UriSalt))
                    return serviceError.WarningMessage("Clave de confirmación no encontrada.");

                var confirmation = userConfirmationRepository.Get(x => x.ConfirmationKey == UriSalt).FirstOrDefault();

                if (confirmation == null)
                    return serviceError.WarningMessage("Clave de confirmación no encontrada.");


                if (confirmation.ExpirationKey <= DateTime.Now)
                {
                    userConfirmationRepository.Delete(confirmation);
                    userConfirmationRepository.Save();
                    return serviceError.WarningMessage("La clave de confirmación ha expirado su tiempo de espera.");
                }

                return serviceError.TaskStatus();

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "ValidateRecoveryPassword", "AccountService");
                return serviceError.WarningMessage("Error al realizar las validaciones");
            }
        }


        /// <summary>
        /// Metodo para actualizar la contrasena de un usuario
        /// </summary>
        /// <param name="password"></param>
        /// <param name="KeyConfirmation"></param>
        public TaskResult ChangePasswordRecovery(string password, string KeyConfirmation)
        {
            try
            {
                if (string.IsNullOrEmpty(KeyConfirmation))
                    return serviceError.WarningMessage("Clave de confirmación no encontrada.");
                else if (string.IsNullOrEmpty(password))
                    return serviceError.WarningMessage("La contraseña no puede estar en blanco.");
                else
                {
                    //Obtenemos los datos del usuario y su confirmacion
                    var confirmation = userConfirmationRepository.Get(x => x.ConfirmationKey == KeyConfirmation).FirstOrDefault();

                    var user = userRepository.Get(x => x.Id == confirmation.UserId).FirstOrDefault();
                    user.UserPassword = globalService.SHA1Encrypt(password);
                    userRepository.Update(user);

                    //Obtenemos todas las confirmaciones generadas
                    var allConfirmation = userConfirmationRepository.Get(x => x.UserId == user.Id);
                    userConfirmationRepository.DeleteRange(allConfirmation);

                    return serviceError.TaskStatus();
                }
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "ChangePasswordRecovery", "AccountService", "Error al intentar actualizar la contraseña.");
                return serviceError.TaskStatus();
            }
        }


        /// <summary>
        /// Proceso para obtener la llave de confirmacion generada al usuario
        /// </summary>
        /// <param name="UserId">Llave primaria del usuario</param>
        /// <returns></returns>
        public string GetUserConfirmation(long UserId)
        {
            return userConfirmationRepository.GetUserConfirmation(UserId);
        }

        /// <summary>
        /// Metodo para obtener el email del usuario
        /// </summary>
        /// <param name="userId">Primary key del usuario</param>
        /// <returns></returns>
        public string GetEmailUser(long userId)
        {
            return userRepository.Get(userId).Email;
        }

        /// <summary>
        /// Proceso para obtener un usuario
        /// </summary>
        /// <param name="Id">Llave primaria del usuario</param>
        /// <returns></returns>
        public TblUsers GetUser(long Id)
        {
            return userRepository.GetUser(Id);
        }

        /// <summary>
        /// Proceso para obtener un usuario
        /// </summary>
        /// <param name="email">Email registrado del usuario</param>
        /// <returns></returns>
        public TblUsers GetUser(string email)
        {
            return userRepository.Get(x => x.Email == email).FirstOrDefault();
        }


        /// <summary>
        /// Proceso para iniciar la session del usuario
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        public async Task<TblUsers> Login(TblUsers User)
        {
            try
            {
                var isEmailValid = new EmailAddressAttribute();

                if (string.IsNullOrEmpty(User.Email) && string.IsNullOrEmpty(User.FacebookId))
                {
                    serviceError.WarningMessage("Falta el email del usuario.");
                    return null;
                }

                if (User.Email.Length > 100)
                {
                    serviceError.WarningMessage("El tamaño del email no puede pasar los 100 caracteres.");
                    return null;
                }

                if (!isEmailValid.IsValid(User.Email))
                {
                    serviceError.WarningMessage("Debe ingresar un email valido.");
                    return null;
                }

                var user = await GetUser(User.Email, User.UserPassword);

                if (user == null)
                {
                    serviceError.WarningMessage("Usuario ingresado no se encuentra registrado.");
                    return null;
                }
                else
                    return user;

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "Login", "AccountController", "Tuvimos un problema intentando acceder a tu cuenta.");
                return null;
            }
        }

        /// <summary>
        /// Proceso para crear un usuario principal en el sistema
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        public TaskResult CreateUser(TblUsers User)
        {
            var task = UserValidateCreate(User);

            if (task.ExecutedSuccesfully)
            {
                
                //Registrando los datos del usuario con  los valores por default
                User.CreateDate = DateTime.Now;
                User.Estatus = true;

                //Generamos la clave de confirmación
                User.UserPassword = globalService.SHA1Encrypt(User.UserPassword);
                TblUserConfirmation Confirmacion = new TblUserConfirmation();
                Confirmacion.ConfirmationKey = globalService.SHA1Encrypt(User.Email + DateTime.Now.ToShortDateString());
                Confirmacion.ExpirationKey = DateTime.Now.AddDays(2);
                Confirmacion.IdForms = (Byte)QuickSalEnum.TypeOfKey.CreacionContraseña;
                //Agreando las informaciones al modelo
                User.TblUserConfirmation.Add(Confirmacion);

                return userRepository.AddUser(User);
            }
            else
                return task;
        }

        /// <summary>
        /// Metodo para actualizar los datos de un usuario
        /// </summary>
        /// <param name="entity">Modelo con la informacion</param>
        /// <returns></returns>
        public TaskResult EditUser(TblUsers entity)
        {
            var task = UserValidateUpdate(entity);

            if (true)
            {
                var session = securityService.GetSession();
                var user = userRepository.Get(session.UserId);

                user.CountryId = entity.CountryId;
                user.ProvinceId = entity.ProvinceId;
                user.DocumentTypeId = entity.DocumentTypeId;
                user.Name = entity.Name;
                user.LastName = entity.LastName;
                user.Direction = entity.Direction;
                user.DocumentNumber = entity.DocumentNumber;
                user.UpdateDate = DateTime.Now;
                user.BirthDate = entity.BirthDate;
                user.CellPhone = entity.CellPhone;
                user.Telephone = entity.Telephone;
                user.Gender = entity.Gender;
                user.CivilStatus = entity.CivilStatus;
                user.Id = session.UserId;


                var tempPicture = globalService.GetTempFiles(session.UserId, (byte)TypeFiles.ProfilePhoto);

                //Movemos la foto a su carpeta final
                if (tempPicture.Count() > 0 && !string.IsNullOrEmpty(entity.Picture))
                {
                    entity.Picture = tempPicture.LastOrDefault().FileName;
                    globalService.MoveFile(entity.Picture, TypeFiles.ProfilePhoto);
                    amazonService.DeleteFile(user.Picture, TypeFiles.ProfilePhoto);//Eliminamos la foto de amazon web service   
                }
                user.Picture = entity.Picture;

                //Removemos las fotos temporales que tenga el usuario
                if (tempPicture.Count() > 0)
                    globalService.DeleteRangeTempFile(tempPicture);


                userRepository.Update(user);
            }

            return task;
        }

        /// <summary>
        /// Metodo para realizar las validaciones cuando se actualiza un usuario
        /// </summary>
        /// <param name="entity">Modelo con la informacion</param>
        /// <returns></returns>
        public TaskResult UserValidateUpdate(TblUsers entity)
        {
            try
            {
                if (VerifyAccount(entity) != null)
                    return serviceError.WarningMessage("Este usuario ya se encuentra registrado.");
                else if (string.IsNullOrEmpty(entity.Email) && string.IsNullOrEmpty(entity.FacebookId))
                    return serviceError.WarningMessage("Falta el email del usuario");
                else if (entity.DocumentNumber != null)
                {
                    bool document = false;
                    document = userRepository.ValidationEditdocument(entity.DocumentNumber);
                    if (document == true)
                    {
                        return serviceError.WarningMessage("El número de documento exíste, favor de revisar.");
                    }
                }

                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UserValidateCreate", "AccountingService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Proceso para hacer las validaciones al usuario cuando se esta registrando
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        private TaskResult UserValidateCreate(TblUsers User)
        {
            try
            {
                var isEmailValid = new EmailAddressAttribute();

                if (string.IsNullOrEmpty(User.UserPassword))
                    return serviceError.WarningMessage("Falta la contraseña del usuario.");

                if (string.IsNullOrEmpty(User.Name))
                    return serviceError.WarningMessage("Falta el nombre del usuario.");

                if (User.Name.Length > 100)
                    return serviceError.WarningMessage("El tamaño su nombre no puede pasar los 100 caracteres.");

                if (globalService.validateSpecialcharacters(User.Name))
                    return serviceError.WarningMessage("Su nombre solo debe de tener letras o números.");

                if (globalService.validateSQLstatement(User.Name))
                    return serviceError.WarningMessage("Su nombre tiene palabras no aceptadas.");

                if (!string.IsNullOrEmpty(User.LastName) && User.Name.Length > 100)
                    return serviceError.WarningMessage("El tamaño su apellido no puede pasar los 100 caracteres.");

                if (!string.IsNullOrEmpty(User.LastName) && globalService.validateSpecialcharacters(User.LastName))
                    return serviceError.WarningMessage("Su apellido solo debe de tener letras o números.");

                if (!string.IsNullOrEmpty(User.LastName) && globalService.validateSQLstatement(User.LastName))
                    return serviceError.WarningMessage("Su apellido tiene palabras no aceptadas.");

                if (string.IsNullOrEmpty(User.Email) && string.IsNullOrEmpty(User.FacebookId))
                    return serviceError.WarningMessage("Falta el email del usuario.");

                if (User.Email.Length > 100)
                    return serviceError.WarningMessage("El tamaño del email no puede pasar los 100 caracteres.");

                if (!isEmailValid.IsValid(User.Email))
                    return serviceError.WarningMessage("Debe ingresar un email valido.");

                if (VerifyAccount(User) != null)
                    return serviceError.WarningMessage("Este usuario ya se encuentra registrado.");



                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UserValidateCreate", "AccountingService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Proceso para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            var task = await userRepository.Savechanges();

            if (!task.ExecutedSuccesfully)
                serviceError.LogError(task.MessageList[0], "Savechanges", "AccountingService", "Error al intentar guardar los datos.");

            return task;
        }
    }

    public interface IAccountService
    {
        /// <summary>
        /// Proceso para verficar si existe un usuario registrado
        /// </summary>
        /// <param name="User">Clase que contiene los datos del usuario</param>
        /// <returns></returns>
        TblUsers VerifyAccount(TblUsers User);

        /// <summary>
        /// Proceso para obtener un usuario desde su llave primaria
        /// </summary>
        /// <param name="Id">Llave primaria del usuario</param>
        /// <returns></returns>
        TblUsers GetUser(long Id);

        /// <summary>
        /// Metodo para generar una nueva llave para recuperar contrasena
        /// </summary>
        /// <param name="email">Email del usuario</param>
        /// <param name="key">Nueva llave generada</param>
        /// <returns></returns>
        TaskResult GenerateConfirmationKey(string email, out string key);

        /// <summary>
        ///  Metodo para validar la recuperacion de contrasena
        /// </summary>
        /// <param name="UriSalt"></param>
        /// <returns></returns>
        TaskResult ValidateRecoveryPassword(string UriSalt);

        /// <summary>
        /// Metodo para actualizar la contrasena de un usuario
        /// </summary>
        /// <param name="password"></param>
        /// <param name="KeyConfirmation"></param>
        TaskResult ChangePasswordRecovery(string password, string KeyConfirmation);

        /// <summary>
        /// Proceso para obtener un usuario
        /// </summary>
        /// <param name="email">Email registrado del usuario</param>
        /// <returns></returns>
        TblUsers GetUser(string email);

        /// <summary>
        /// Metodo para obtener el usuario
        /// </summary>
        /// <param name="Email">Email del usuario</param>
        /// <param name="password">Contrasena del usuario</param>
        /// <returns></returns>
        Task<TblUsers> GetUser(string email, string password);


        /// <summary>
        /// Proceso para iniciar la session del usuario
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        Task<TblUsers> Login(TblUsers User);

        /// <summary>
        /// Proceso para crear un usuario principal en el sistema
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        TaskResult CreateUser(TblUsers User);

        /// <summary>
        /// Metodo para actualizar los datos de un usuario
        /// </summary>
        /// <param name="entity">Modelo con la informacion</param>
        /// <returns></returns>
        TaskResult EditUser(TblUsers entity);

        /// <summary>
        /// Proceso para obtener la llave de confirmacion generada al usuario
        /// </summary>
        /// <param name="UserId">Llave primaria del usuario</param>
        /// <returns></returns>
        string GetUserConfirmation(long UserId);

        /// <summary>
        /// Metodo para obtener el email del usuario del empleado
        /// </summary>
        /// <param name="userId">Primary key</param>
        /// <returns></returns>
        string GetEmailUser(long userId);


        /// <summary>
        /// Proceso para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
