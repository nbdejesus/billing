﻿using Microsoft.EntityFrameworkCore;
using QuickSal.DataAcces;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Procedure;
using QuickSal.DataAcces.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static QuickSal.DataAcces.QuickSalEnum;

namespace QuickSal.Services
{
    public class InventoryService : IInventoryService
    {
        private IWarehouseRepository warehouseRepository;
        private IServiceError serviceError;
        private ISecurityService securityService;
        private IMeasureRepository measureRepository;
        private ICategoryRepository categoryRepository;
        private ISubCategoryRepository subCategoryRepository;
        private IProductRepository productRepository;
        private IProductPriceRepository productPriceRepository;
        private IMovingWareRepository movingWareRepository;
        private IGlobalService globalService;
        private IMovingWareRowRepository movingWareRowRepository;
        private IDebitNoteRepositoy debitNoteRepositoy;
        private IAmazonService amazonService;
        private ISupplierRepository supplierRepository;
        private ITaxRepository taxRepository;


        public InventoryService(IWarehouseRepository warehouseRepository, ISecurityService securityService, ISupplierRepository supplierRepository,
        IMeasureRepository measureRepository, IServiceError serviceError, ICategoryRepository categoryRepository, ITaxRepository taxRepository,
                                ISubCategoryRepository subCategoryRepository, IProductRepository productRepository, IMovingWareRepository movingWareRepository,
                             IGlobalService globalService, IMovingWareRowRepository movingWareRowRepository, IProductPriceRepository productPriceRepository,
                              IDebitNoteRepositoy debitNoteRepositoy, IAmazonService amazonService)
        {
            this.warehouseRepository = warehouseRepository;
            this.serviceError = serviceError;
            this.securityService = securityService;
            this.measureRepository = measureRepository;
            this.categoryRepository = categoryRepository;
            this.subCategoryRepository = subCategoryRepository;
            this.productRepository = productRepository;
            this.productPriceRepository = productPriceRepository;
            this.movingWareRepository = movingWareRepository;
            this.movingWareRowRepository = movingWareRowRepository;
            this.globalService = globalService;
            this.debitNoteRepositoy = debitNoteRepositoy;
            this.amazonService = amazonService;
            this.supplierRepository = supplierRepository;
            this.taxRepository = taxRepository;
        }

        #region Nota de Debito

        /// Metodo que trae el listado de nota de Debito
        /// </summary>
        /// <returns></returns>
        public IQueryable<DebitNote> ListDebitNote(long branchOfficeId)
        {
            return debitNoteRepositoy.ListDebitNote(branchOfficeId);
        }

        /// Metodo que trae el listado de detalle nota de debito
        /// </summary>
        /// <returns></returns>
        public List<DebitNoteRowVM> GetDebitNoteRow(long DebitNoteId)
        {

            return debitNoteRepositoy.GetDebitNoteRow(DebitNoteId);
        }


        /// Metodo que trae el  detalle correspondiente
        /// </summary>
        /// <returns></returns>
        public TblDebitNote DebitNoteDetails(long DebitNoteId)
        {
            return debitNoteRepositoy.DebitNoteDetails(DebitNoteId);
        }



        /// <summary>
        /// Metodo para crear los movimientos de almacen de una sucursal
        /// </summary>
        /// <param name="movingWareHouse">Modelo que contiene el movimiento de producto de un almacen</param>
        /// <returns></returns>
        public TaskResult CreateDebitNote(TblDebitNote debitNote, string tipoProceso)
        {
            var task = DebitNoteValidateCreate(debitNote);
            var sesion = securityService.GetSession();
            var user = sesion.EmployeeId;

            var codes = debitNoteRepositoy.GetLastMoving(sesion.BranchOfficeId);
            var code = globalService.GenerateCode(Support.EnumTypes.CodeEntities.DebitNote, codes);
            if (task.ExecutedSuccesfully)
            {

                debitNote.DocumentNumber = code;
                debitNote.EmployeeCreateId = user;
                debitNote.EmployeeModifyId = user;
                debitNote.CreateDate = DateTime.Now;
                debitNote.UpdateDate = DateTime.Now;

                debitNote.Ncf = "00000000";
                debitNote.Ncfmodified = debitNote.Ncfmodified;

                debitNoteRepositoy.AddEntity(debitNote);
            }

            if (tipoProceso == "Procesar")
            {
                foreach (var item in debitNote.TblDebitNoteRow)
                {
                    var cantidad = debitNoteRepositoy.UpdateInventoryWareHouse(debitNote.TblDebitNoteRow.Select(z => z.WarehouseId).FirstOrDefault(), item.ProductId, item.Quantity);
                }
            }

            return task;
        }


        /// <summary>
        /// Proceso para hacer las validaciones creacion de nota de debito
        /// </summary>
        /// <param name="debitNote">Modelo que contiene la nota de debito</param>
        /// <returns></returns>
        public TaskResult DebitNoteValidateCreate(TblDebitNote debitNote)
        {
            try
            {
                if (debitNote.BranchOfficeId == 0)
                    return serviceError.WarningMessage("Falta la oficina");
                else if (debitNote.PurchaseOrderId == 0)
                    return serviceError.WarningMessage("Falta el Id de la orden de compra");

                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "DebitNoteValidateCreate", "InventoryService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Proceso para cambiar el estado de las notas de debito
        /// </summary>
        /// <param name="Id">parametro que contiene el Id de la nota de debito</param>
        /// <param name="DocumentStatusID">parametro que contiene el Id del estado</param>
        /// <returns></returns>
        public TaskResult SetDebitNotaStatus(int Id, long DocumentStatusID)
        {
            TaskResult task = new TaskResult();

            if (Id == 0)
            {

                task.ExecutedSuccesfully = false;
                serviceError.LogError("Falta el ID del Ncf", "SetNcfStatus", "AccoutingService", "Los datos no se han podido actualizar correctamente.");
            }

            if (task.ExecutedSuccesfully)
            {
                var debitNote = DebitNoteDetails(Id);
                var TblDebitNoteRow = GetDebitNoteRow(debitNote.Id);

                debitNote.DocumentEstatusId = Convert.ToByte(DocumentStatusID);

                if (DocumentStatusID == (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada)
                {
                    foreach (var item in TblDebitNoteRow)
                    {
                        var cantidad = debitNoteRepositoy.UpdateInventoryWareHouse(item.WarehouseId, item.ProductId, item.Quantity);


                    }
                }
                debitNoteRepositoy.Update(debitNote);
            }
            return task;
        }

        /// <summary>
        /// Metodo para actualizar las notas de debitos
        /// </summary>
        /// <param name="debitNote">Modelo que contiene la nota de debito</param>
        /// <returns></returns>
        public TaskResult UpdateDebitNote(TblDebitNote debitNote)
        {
            var sesion = securityService.GetSession();
            var task = DebitNoteValidateUpdate(debitNote);
            if (task.ExecutedSuccesfully)
            {

                var tblDebitNote = debitNoteRepositoy.Get(x => x.Id == debitNote.Id).Include(x => x.TblDebitNoteRow).FirstOrDefault();

                tblDebitNote.Observation = debitNote.Observation;
                tblDebitNote.EmployeeModifyId = sesion.EmployeeId;
                tblDebitNote.UpdateDate = DateTime.Now;

                var debitNoteRow = debitNote.TblDebitNoteRow;

                var NewRows = (from d in debitNoteRow
                               where !(from b in tblDebitNote.TblDebitNoteRow //TblInvoiceRow estan las filas que estan sin modificar en la base de datos
                                       select b.Id).Contains(d.Id)
                               select d).Distinct().ToList();


                //Agregamos nuevos filas si existen nuevos registros
                if (NewRows.Count > 0)
                    foreach (var item in NewRows)
                        tblDebitNote.TblDebitNoteRow.Add(item);

                foreach (var debitRow in tblDebitNote.TblDebitNoteRow)
                {
                    var Row = debitNoteRow.Where(x => x.Id != 0 && x.Id == debitRow.Id).FirstOrDefault();

                    //debitRow.AmountDiscount  = Row.AmountDiscount;
                    //debitRow.AmountTax       = Row.AmountTax;
                    //debitRow.Cost            = Row.Cost;
                    //debitRow.DebitNoteId     = Row.DebitNoteId;
                    //debitRow.Discount        = Row.Discount;
                    //debitRow.MeasureId       = Row.MeasureId;
                    //debitRow.ProductId       = Row.ProductId;
                    //debitRow.TaxAbbreviation = Row.TaxAbbreviation;
                    //debitRow.Tax             = Row.Tax;
                    //debitRow.WarehouseId     = Row.WarehouseId;
                    debitRow.Quantity = Row.Quantity;
                    debitRow.Total = Row.Total;

                }


                debitNoteRepositoy.Update(tblDebitNote);

            }
            return task;
        }


        /// <summary>
        /// Proceso para hacer las validaciones modificacion de nota de debito
        /// </summary>
        /// <param name="debitNoteR">Modelo que contiene  la nota de debito</param>
        /// <returns></returns>
        public TaskResult DebitNoteValidateUpdate(TblDebitNote debitNote)
        {
            try
            {
                if (debitNote.Id <= 0)
                    return serviceError.WarningMessage("Falta el id de la nota de debito");

                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "DebitNoteValidateCreate", "InventoryService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para agregar el NCF a la nota de debito
        /// </summary>
        /// <param name="branchOfficeId">Primary key de la sucursal</param>
        /// <param name="DebitNoteId">Primary key de la cabecera de la nota de debito</param>
        /// <returns></returns>
        public bool AddNcfDebitNote(long branchOfficeId, long DebitNoteId)
        {
            return debitNoteRepositoy.AddNcfDebitNote(branchOfficeId, DebitNoteId);
        }

        #endregion

        #region Movinmiento Almacen

        /// <summary>
        /// Metodo para obtener los movimientos de almacen de una sucursal
        /// </summary>
        /// <param name="WarehouseId">Primary key de la sucursal</param>
        /// <returns></returns>
        public IQueryable<PrcGetAllMovingWare> GetMovingWarehouse(long WarehouseId)
        {
            return movingWareRepository.GetMovingWarehouse(WarehouseId);
        }


        /// <summary>
        /// Metodo para obtener los movimientos de almacen de una sucursal
        /// </summary>
        /// <param name="MovingId">Primary key de la sucursal</param>
        /// <returns></returns>
        public TblMovingWarehouse GetMovingDetails(long MovingId)
        {
            return movingWareRepository.GetMovingDetails(MovingId);
        }

        /// <summary>
        /// Metodo el detalle del movimiento
        /// </summary>
        /// <param name="MovingId">Parametro que contiene el Id del movimiento</param>    
        /// <returns></returns>
        public IQueryable<TblMovingWarehouseRow> DatetailsRows(long MovingId)
        {
            return movingWareRowRepository.DatetailsRows(MovingId);
        }



        /// <summary>
        /// Metodo para llamar el producto del movimineto
        /// </summary>
        /// <param name="MovingId">Parametro que contiene el Id del movimiento</param>    
        /// <param name="ProductId">Parametro que contiene el Id del producto</param>    
        /// <returns></returns>
        public PrGetMovingWareDetails ProductRow(long MovingId, long ProductId)
        {

            return movingWareRowRepository.ProductRow(MovingId, ProductId);
        }

        /// <summary>
        /// Metodo para crear los movimientos de almacen de una sucursal
        /// </summary>
        /// <param name="movingRow">Modelo que contiene el id del detalle del movimiento</param>
        /// <returns></returns>
        public TaskResult UpdateMovingWareRow(PrGetMovingWareDetails movingRow)
        {
            var task = MovingRowValidateUpdate(movingRow);
            if (task.ExecutedSuccesfully)
            {

                TblMovingWarehouseRow movinWaregRow = movingWareRowRepository.Get(movingRow.Id);

                movinWaregRow.ProductId = movingRow.ProductId;
                movinWaregRow.ProductName = movingRow.Product;
                movinWaregRow.State = movingRow.State;
                movinWaregRow.Amount = movingRow.Amount;
                movinWaregRow.Description = movingRow.Description;

                movingWareRowRepository.Update(movinWaregRow);

            }
            return task;
        }

        /// <summary>
        /// Metodo para crear los movimientos de almacen de una sucursal
        /// </summary>
        /// <param name="movingWareHouse">Modelo que contiene el movimiento de producto de un almacen</param>
        /// <returns></returns>
        public TaskResult CreateMovingWare(TblMovingWarehouse movingWareHouse, string tipoProceso)
        {
            var task = MovinWareValidateCreate(movingWareHouse);
            var sesion = securityService.GetSession();
            var user = sesion.EmployeeId;

            var codes = movingWareRepository.GetLastMoving(sesion.BusinessId);
            var code = globalService.GenerateCode(Support.EnumTypes.CodeEntities.Moving, codes);
            if (task.ExecutedSuccesfully)
            {


                movingWareHouse.DocumentNo = code;
                movingWareHouse.EmployeeCreateId = user;
                movingWareHouse.EmployeeModifyId = user;
                movingWareHouse.CreateDate = DateTime.Now;
                movingWareHouse.ModifyDate = DateTime.Now;
                if (tipoProceso == "Procesar")
                {
                    movingWareHouse.State = 1;
                    foreach (var item in movingWareHouse.TblMovingWarehouseRow)
                        item.State = 1;
                }
                else
                {
                    movingWareHouse.State = 2;
                    foreach (var item in movingWareHouse.TblMovingWarehouseRow)
                        item.State = 2;
                }

                movingWareRepository.AddEntity(movingWareHouse);
            }

            if (tipoProceso != "Procesar")
            {
                foreach (var item in movingWareHouse.TblMovingWarehouseRow)
                {
                    var cantidad = movingWareRepository.UpdateInventoryWareHouse(movingWareHouse.SenderWarehouseId, item.ProductId, item.Amount, false);

                }
            }


            return task;
        }


        /// <summary>
        /// Metodo para crear los movimientos de almacen de una sucursal
        /// </summary>
        /// <param name="movingWareHouse">Modelo que contiene el movimiento de producto de un almacen</param>
        /// <returns></returns>
        public TaskResult UpdateMovingWare(TblMovingWarehouse movingWareHouse)
        {
            var task = MovingValidateUpdate(movingWareHouse);
            if (task.ExecutedSuccesfully)
            {
                var session = securityService.GetSession();
                TblMovingWarehouse TblMovingWarehouse = movingWareRepository.Get(movingWareHouse.Id);

                TblMovingWarehouse.CreateDate = movingWareHouse.CreateDate;
                TblMovingWarehouse.Description = movingWareHouse.Description;
                TblMovingWarehouse.DocumentNo = movingWareHouse.DocumentNo;
                TblMovingWarehouse.EmployeeCreateId = movingWareHouse.EmployeeCreateId;
                TblMovingWarehouse.EmployeeModifyId = session.EmployeeId;
                TblMovingWarehouse.ModifyDate = DateTime.Now;
                TblMovingWarehouse.ReceiverWarehouseId = movingWareHouse.ReceiverWarehouseId;
                TblMovingWarehouse.SenderWarehouseId = movingWareHouse.SenderWarehouseId;
                TblMovingWarehouse.State = movingWareHouse.State;

                movingWareRepository.Update(TblMovingWarehouse);

            }
            return task;
        }

        /// <summary>
        /// Metodo para cambiar el estado del movimiento
        /// </summary>
        /// <param name="movingWareHouse">Modelo que contiene el movimiento de producto de un almacen</param>
        /// <returns></returns>
        public TaskResult ChangeStatu(long movingWareHouseId, int estado)
        {
            try
            {

                TblMovingWarehouse TblMovingWarehouse = movingWareRepository.Get(movingWareHouseId);
                IQueryable<TblMovingWarehouseRow> tblMovingWarehouseRow = movingWareRowRepository.DatetailsRows(TblMovingWarehouse.Id);



                if (estado == (int)QuickSalEnum.MovingStatus.In_Transit)
                {
                    foreach (var item in tblMovingWarehouseRow)
                    {
                        var cantidad = movingWareRepository.UpdateInventoryWareHouse(TblMovingWarehouse.SenderWarehouseId, item.ProductId, item.Amount, false);

                    }
                }

                else if (estado == (int)QuickSalEnum.MovingStatus.Canceled && TblMovingWarehouse.State == (int)QuickSalEnum.MovingStatus.In_Transit)
                {
                    foreach (var item in tblMovingWarehouseRow)
                    {
                        var cantidad = movingWareRepository.UpdateInventoryWareHouse(TblMovingWarehouse.SenderWarehouseId, item.ProductId, item.Amount, true);

                    }
                }
                else if (estado == (int)QuickSalEnum.MovingStatus.Received)
                {

                    foreach (var item in tblMovingWarehouseRow)
                    {
                        if (item.State == (int)QuickSalEnum.MovingStatus.In_Transit)
                        {
                            var cantidad = movingWareRepository.UpdateInventoryWareHouse(TblMovingWarehouse.ReceiverWarehouseId, item.ProductId, item.Amount, true);
                            item.State = estado;
                        }
                    }
                }


                TblMovingWarehouse.State = estado;

                movingWareRepository.Update(TblMovingWarehouse);
                return serviceError.TaskStatus();

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "ChangeStatu", "InventoryService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }



        /// <summary>
        /// Metodo para cambiar el estado del movimiento
        /// </summary>
        /// <param name="model">Modelo que contiene el detalle de movimiento de un producto en un almacen</param>
        /// <returns></returns>
        public TaskResult ReturnProduct(PrGetMovingWareDetails model, int estado)
        {

            try
            {
                var returnProduct = movingWareRepository.UpdateInventoryWareHouse(model.SenderWarehouseId, model.ProductId, model.Amount, true);

                if (returnProduct.ExecutedSuccesfully)
                {
                    model.State = 5;
                    return UpdateMovingWareRow(model);
                }

                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "ReturnProduct", "InventoryService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }


        /// <summary>
        /// Metodo para hacer las validaciones del movimineto
        /// </summary>
        /// <param name="movingWareHouse">Modelo que contiene el movimiento</param>
        /// <returns></returns>
        private TaskResult MovingValidateUpdate(TblMovingWarehouse movingWareHouse)
        {
            try
            {
                if (string.IsNullOrEmpty(movingWareHouse.DocumentNo))
                    return serviceError.WarningMessage("Falta el numero de Documento.");
                else if (movingWareHouse.ReceiverWarehouseId <= 0)
                    return serviceError.WarningMessage("Falta el Almacen de destino.");
                else if (movingWareHouse.SenderWarehouseId <= 0)
                    return serviceError.WarningMessage("Falta el Almacen de remitente.");

                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "MovingValidateUpdate", "InventoryService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para hacer las validaciones de la empresa
        /// </summary>
        /// <param name="movingRow">Modelo que contiene el detalle del movimiento</param>
        /// <returns></returns>
        private TaskResult MovingRowValidateUpdate(PrGetMovingWareDetails movingRow)
        {
            try
            {
                if (movingRow.Amount <= 0)
                    return serviceError.WarningMessage("Falta la cantida del producto.");
                else if (movingRow.ProductId <= 0)
                    return serviceError.WarningMessage("Falta el producto.");

                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "MovingRowValidateUpdate", "InventoryService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }


        /// <summary>
        /// Proceso para hacer las validaciones creacion de movimiento
        /// </summary>
        /// <param name="movingWareHouse">Modelo que contiene el movimiento de almacen</param>
        /// <returns></returns>
        public TaskResult MovinWareValidateCreate(TblMovingWarehouse movingWareHouse)
        {
            try
            {
                if (movingWareHouse.DocumentNo == "")
                    return serviceError.WarningMessage("Falta la cantidad del producto.");
                else if (movingWareHouse.ReceiverWarehouseId == 0)
                    return serviceError.WarningMessage("Falta el almacen destinatario.");
                else if (movingWareHouse.Description == "")
                    return serviceError.WarningMessage("Falta la descripcion del envio.");
                else if (movingWareHouse.ReceiverWarehouseId == 0)
                {
                    return serviceError.WarningMessage("Falta el almacen remitente.");
                }

                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "MovinWareValidateCreate", "InventoryService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo que trae todos los producto del inventario del almacen
        /// </summary>
        /// <param name="WarehouseId">Parametro que contiene el Id del almacen</param>
        /// <param name="ProductId">Parametro que contiene el Id del producto</param>
        /// <returns></returns>
        public IQueryable<ProductInventory> GetProducts(long WarehouseId, long ProductId)
        {
            return movingWareRowRepository.GetProducts(WarehouseId, ProductId);
        }

        /// <summary>
        /// Metodo que elimina los productos de los moviminetos
        /// </summary>
        /// <param name="MovingIdRow">Parametro que contiene el Id del detalle del movimiento</param>
        /// <returns></returns>
        public TaskResult DeleteMovingIdRow(long MovingIdRow)
        {
            return movingWareRowRepository.DeleteMovingIdRow(MovingIdRow);
        }

        /// <summary>
        /// Metodo para obtener todos los almacenes de una empresa
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        public IQueryable<TblWarehouse> GetAllWarehouses(long BranchOfficeId)
        {

            return movingWareRepository.GetAllWarehouses(BranchOfficeId);

        }


        #endregion

        #region Almacen

        /// <summary>
        /// Metodo para obtener los almacenes de una sucursal
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <param name="active">Estatus del almacen (opcional)</param>
        /// <returns></returns>
        public async Task<IQueryable<TblWarehouse>> GetWarehouses(long BranchOfficeId, bool? active = null)
        {
            return await warehouseRepository.GetWarehouses(BranchOfficeId, active);
        }

        /// <summary>
        /// Metodo para obtener el almacén principal de una sucursal
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        public TblWarehouse GetPrincipalWarehouse(long BranchOfficeId)
        {
            return warehouseRepository.Get(x => x.BranchOfficeId == BranchOfficeId && x.Principal).FirstOrDefault();
        }

        /// <summary>
        /// Metodo para obtener los almacenes de una empresa
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        public IQueryable<TblWarehouse> GetBusinessWarehouses(long BusinessId)
        {

            return warehouseRepository.GetBusinessWarehouses(BusinessId);
        }

        /// <summary>
        /// Proceso para agregar un almacén al sistema
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<TaskResult> CreateWarehouse(TblWarehouse entity)
        {
            try
            {
                var task = WarehouseValidateCreate(entity);

                if (task.ExecutedSuccesfully)
                {
                    var sesion = securityService.GetSession();
                    entity.BranchOfficeId = sesion.BranchOfficeId;
                    entity.CreateDate = DateTime.Now;
                    entity.UpdateDate = DateTime.Now;
                    entity.EmployeeCreateId = sesion.EmployeeId;
                    entity.EmployeeModifyId = sesion.EmployeeId;
                    entity.Estatus = true;

                    //Creamos las cantidades iniciales para el almacen
                    var productIdList = productRepository.GetALL().Select(x => x.Id).ToList();
                    foreach (var item in productIdList)
                    {
                        TblInventoryWarehouse inventory = new TblInventoryWarehouse();
                        inventory.ProductAmount = 0;
                        inventory.ProductsId = item;
                        entity.TblInventoryWarehouse.Add(inventory);
                    }

                    warehouseRepository.AddEntity(entity);
                }

                return task;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateWarehouse", "InventoryService");
                serviceError.WarningMessage("Error al registrar el almacén");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para hacer las validaciones al almacén cuando se esta registrando
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion del almacén</param>
        /// <returns></returns>
        private TaskResult WarehouseValidateCreate(TblWarehouse entity)
        {
            try
            {
                if (string.IsNullOrEmpty(entity.Name))
                    return serviceError.WarningMessage("Falta el nombre del almacén.");
                else if (string.IsNullOrEmpty(entity.Description))
                    return serviceError.WarningMessage("Falta la descripcion del almacén.");


                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "WarehouseValidateCreate", "InventoryService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }


        /// <summary>
        /// metodo para actualizar un almacén al sistema
        /// </summary>
        /// <param name="entity">Entidad con la informacion</param>
        /// <returns></returns>
        public TaskResult UpdateWarehouse(TblWarehouse entity)
        {
            try
            {
                var task = WarehouseValidateUpdate(entity);

                if (task.ExecutedSuccesfully)
                {
                    var sesion = securityService.GetSession();
                    var warehouse = warehouseRepository.Get(x => x.Id == entity.Id).FirstOrDefault();

                    warehouse.UpdateDate = DateTime.Now;
                    warehouse.EmployeeModifyId = sesion.EmployeeId;
                    warehouse.Name = entity.Name;
                    warehouse.Description = entity.Description;

                    warehouseRepository.Update(warehouse);
                }

                return task;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateWarehouse", "InventoryService");
                serviceError.WarningMessage("Error al actualizar el almacén");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para cambiar de estado un almacén
        /// </summary>
        /// <param name="Id">Primary key del almacén</param>
        /// <param name="status">Estado</param>
        /// <returns></returns>
        public TaskResult SetWarehouseStatus(int Id, bool status)
        {
            TaskResult task = new TaskResult();

            if (Id == 0)
            {
                task.ExecutedSuccesfully = false;
                serviceError.LogError("Falta el ID del almacén", "SetWarehouseStatus", "InventoryService", "Los datos no se han podido actualizar correctamente.");
            }

            if (task.ExecutedSuccesfully)
            {
                var warehouse = GetWarehouse(Id);
                warehouse.Estatus = status;
                warehouseRepository.Update(warehouse);
            }
            return task;
        }

        /// <summary>
        /// Metodo para hacer las validaciones al almacén cuando se esta actualizando
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion del almacén</param>
        /// <returns></returns>
        private TaskResult WarehouseValidateUpdate(TblWarehouse entity)
        {
            try
            {
                if (string.IsNullOrEmpty(entity.Name))
                    return serviceError.WarningMessage("Falta el nombre del almacén.");
                else if (string.IsNullOrEmpty(entity.Description))
                    return serviceError.WarningMessage("Falta la descripcion del almacén.");


                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "WarehouseValidateUpdate", "InventoryService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para obtener un almacén
        /// </summary>
        /// <param name="WarehouseId">Primary key del almacén</param>
        /// <returns></returns>
        public TblWarehouse GetWarehouse(int WarehouseId)
        {
            return warehouseRepository.Get(x => x.Id == WarehouseId).FirstOrDefault();
        }



        #endregion

        #region Medidas
        /// <summary>
        /// Metodo para obtener todas las medidas del sistema
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        public IQueryable<TblMeasure> GetMeasurements(long BusinessId)
        {
            return measureRepository.GetMeasurements(BusinessId);
        }

        /// <summary>
        /// Proceso para agregar una medida al sistema
        /// </summary>
        /// <param name="entity">entidad con la informacion</param>
        /// <returns></returns>
        public TaskResult CreateMeasure(TblMeasure entity)
        {
            try
            {
                var task = MeasureValidateCreate(entity);

                if (task.ExecutedSuccesfully)
                {
                    var sesion = securityService.GetSession();

                    entity.BusinessId = sesion.BusinessId;
                    entity.CreateDate = DateTime.Now;
                    entity.UpdateDate = DateTime.Now;
                    entity.EmployeeCreateId = sesion.EmployeeId;
                    entity.EmployeeModifyId = sesion.EmployeeId;
                    entity.Estatus = true;

                    measureRepository.AddEntity(entity);
                }

                return task;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateMeasure", "InventoryService");
                serviceError.WarningMessage("Error al registrar la medida");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para hacer las validaciones a la medida cuando se esta registrando
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion de la medida</param>
        /// <returns></returns>
        private TaskResult MeasureValidateCreate(TblMeasure entity)
        {
            try
            {
                if (string.IsNullOrEmpty(entity.Name))
                    return serviceError.WarningMessage("Falta el nombre de la medida.");
                else if (string.IsNullOrEmpty(entity.Abbreviation))
                    return serviceError.WarningMessage("Falta la abreviatura para la medida.");
                else if (entity.Quantity == 0)
                    return serviceError.WarningMessage("Falta la cantidad que representa la medida.");


                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "MeasureValidateCreate", "InventoryService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para obtener una medida
        /// </summary>
        /// <param name="Id">Primary key de la medida</param>
        /// <returns></returns>
        public TblMeasure GetMeasure(int Id)
        {
            return measureRepository.Get(x => x.Id == Id).FirstOrDefault();
        }


        /// <summary>
        /// metodo para actualizar una medida
        /// </summary>
        /// <param name="model">Entidad con la informacion</param>
        /// <returns></returns>
        public TaskResult UpdateMeasure(TblMeasure entity)
        {
            try
            {
                var task = MeasureValidateUpdate(entity);

                if (task.ExecutedSuccesfully)
                {
                    var sesion = securityService.GetSession();
                    var measure = measureRepository.Get(x => x.Id == entity.Id).FirstOrDefault();

                    measure.UpdateDate = DateTime.Now;
                    measure.EmployeeModifyId = sesion.EmployeeId;
                    measure.Name = entity.Name;
                    measure.Abbreviation = entity.Abbreviation;
                    measure.Quantity = entity.Quantity;

                    measureRepository.Update(measure);
                }

                return task;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateMeasure", "InventoryService");
                serviceError.WarningMessage("Error al actualizar la medida");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para cambiar de estado una medida
        /// </summary>
        /// <param name="Id">Primary key de la medida</param>
        /// <param name="status">Estado</param>
        /// <returns></returns>
        public TaskResult SetMeasureStatus(int Id, bool status)
        {
            TaskResult task = new TaskResult();

            if (Id == 0)
            {
                task.ExecutedSuccesfully = false;
                serviceError.LogError("Falta el ID de la medida", "SetMeasureStatus", "InventoryService", "Los datos no se han podido actualizar correctamente.");
            }

            if (task.ExecutedSuccesfully)
            {
                var measure = GetMeasure(Id);
                measure.Estatus = status;
                measureRepository.Update(measure);
            }
            return task;
        }

        /// <summary>
        /// Metodo para hacer las validaciones de la medida cuando se esta actualizando
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion de la medida</param>
        /// <returns></returns>
        private TaskResult MeasureValidateUpdate(TblMeasure entity)
        {
            try
            {
                if (string.IsNullOrEmpty(entity.Name))
                    return serviceError.WarningMessage("Falta el nombre de la medida.");
                else if (string.IsNullOrEmpty(entity.Abbreviation))
                    return serviceError.WarningMessage("Falta la abreviatura para la medida.");
                else if (entity.Quantity == 0)
                    return serviceError.WarningMessage("Falta la cantidad que representa la medida.");


                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "MeasureValidateUpdate", "InventoryService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }
        #endregion

        #region categoria
        /// <summary>
        /// Metodo para obtener todas las categorias del sistema
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        public IQueryable<TblCategory> GetCategories(long BusinessId)
        {
            return categoryRepository.GetCategories(BusinessId);
        }

        /// <summary>
        /// Proceso para agregar una categoria al sistema
        /// </summary>
        /// <param name="entity">entidad con la informacion</param>
        /// <returns></returns>
        public TaskResult CreateCategory(TblCategory entity)
        {
            try
            {
                var task = CategoryValidateCreate(entity);

                if (task.ExecutedSuccesfully)
                {
                    var sesion = securityService.GetSession();

                    entity.BusinessId = sesion.BusinessId;
                    entity.CreateDate = DateTime.Now;
                    entity.UpdateDate = DateTime.Now;
                    entity.EmployeeCreateId = sesion.EmployeeId;
                    entity.EmployeeModifyId = sesion.EmployeeId;
                    entity.Estatus = true;
                    entity.IsCustom = true;

                    categoryRepository.AddEntity(entity);
                }

                return task;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateCateogry", "InventoryService");
                serviceError.WarningMessage("Error al registrar la categoría");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para hacer las validaciones a la categoria cuando se esta registrando
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion de la categoria</param>
        /// <returns></returns>
        private TaskResult CategoryValidateCreate(TblCategory entity)
        {
            try
            {
                if (string.IsNullOrEmpty(entity.Name))
                    return serviceError.WarningMessage("Falta el nombre de la categoría.");

                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CategoryValidateCreate", "InventoryService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para obtener una categoria
        /// </summary>
        /// <param name="Id">Primary key de la categoria</param>
        /// <returns></returns>
        public TblCategory GetCategory(int Id)
        {
            return categoryRepository.Get(x => x.Id == Id).FirstOrDefault();
        }


        /// <summary>
        /// metodo para actualizar una categoria
        /// </summary>
        /// <param name="model">Entidad con la informacion</param>
        /// <returns></returns>
        public TaskResult UpdateCategory(TblCategory entity)
        {
            try
            {
                var task = CategoryValidateUpdate(entity);

                if (task.ExecutedSuccesfully)
                {
                    var sesion = securityService.GetSession();
                    var category = categoryRepository.Get(x => x.Id == entity.Id).FirstOrDefault();

                    category.UpdateDate = DateTime.Now;
                    category.EmployeeModifyId = sesion.EmployeeId;
                    category.Name = entity.Name;
                    category.Description = entity.Description;

                    categoryRepository.Update(category);
                }

                return task;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateCategory", "InventoryService");
                serviceError.WarningMessage("Error al actualizar la categoría");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para cambiar de estado una categoria
        /// </summary>
        /// <param name="Id">Primary key de la categoria</param>
        /// <param name="status">Estado</param>
        /// <returns></returns>
        public TaskResult SetCategoryStatus(int Id, bool status)
        {
            TaskResult task = new TaskResult();

            if (Id == 0)
            {
                task.ExecutedSuccesfully = false;
                serviceError.LogError("Falta el ID de la categoria", "SetCategoryStatus", "InventoryService", "Los datos no se han podido actualizar correctamente.");
            }

            if (task.ExecutedSuccesfully)
            {
                var category = GetCategory(Id);
                category.Estatus = status;
                categoryRepository.Update(category);
            }
            return task;
        }

        /// <summary>
        /// Metodo para hacer las validaciones de la categoria cuando se esta actualizando
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion de la categoria</param>
        /// <returns></returns>
        private TaskResult CategoryValidateUpdate(TblCategory entity)
        {
            try
            {
                if (string.IsNullOrEmpty(entity.Name))
                    return serviceError.WarningMessage("Falta el nombre de la categoría.");


                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CategoryValidateUpdate", "InventoryService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }
        #endregion

        #region SubCategoria

        /// <summary>
        ///  Metodo para obtener todas las SubCategorias de una empresa
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        public IQueryable<TblSubCategory> GetBusinesSubCategories(long BusinessId)
        {
            return subCategoryRepository.GetSubCategories(BusinessId);
        }

        /// <summary>
        /// Metodo para obtener todas las SubCategorias de una categoria
        /// </summary>
        /// <param name="CategoryId">Primary key de la categoria</param>
        /// <returns></returns>
        public IQueryable<TblSubCategory> GetSubCategories(long CategoryId)
        {
            return subCategoryRepository.Get(x => x.CategoryId == CategoryId);
        }

        /// <summary>
        /// Proceso para agregar una subcategoria al sistema
        /// </summary>
        /// <param name="entity">entidad con la informacion</param>
        /// <returns></returns>
        public TaskResult CreateSubCateogry(TblSubCategory entity)
        {
            try
            {
                var task = SubCategoryValidateCreate(entity);

                if (task.ExecutedSuccesfully)
                {
                    var sesion = securityService.GetSession();

                    entity.BusinessId = sesion.BusinessId;
                    entity.CategoryId = entity.CategoryId;
                    entity.CreateDate = DateTime.Now;
                    entity.UpdateDate = DateTime.Now;
                    entity.EmployeeCreateId = sesion.EmployeeId;
                    entity.EmployeeModifyId = sesion.EmployeeId;
                    entity.Estatus = true;
                    entity.IsCustom = true;

                    subCategoryRepository.AddEntity(entity);
                }

                return task;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateSubCateogry", "InventoryService");
                serviceError.WarningMessage("Error al registrar la subcategoría");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para hacer las validaciones a la subcategoria cuando se esta registrando
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion de la subcategoria</param>
        /// <returns></returns>
        private TaskResult SubCategoryValidateCreate(TblSubCategory entity)
        {
            try
            {
                if (string.IsNullOrEmpty(entity.Name))
                    return serviceError.WarningMessage("Falta el nombre de la subcategoría.");
                else if (entity.CategoryId == 0)
                    return serviceError.WarningMessage("Debe seleccionar una categoría.");

                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "SubCategoryValidateCreate", "InventoryService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para obtener una SubCategoria
        /// </summary>
        /// <param name="Id">Primary key de la SubCategoria</param>
        /// <returns></returns>
        public TblSubCategory GetSubCategory(int Id)
        {
            return subCategoryRepository.Get(x => x.Id == Id).FirstOrDefault();
        }


        /// <summary>
        /// metodo para actualizar una subcategoria
        /// </summary>
        /// <param name="model">Entidad con la informacion</param>
        /// <returns></returns>
        public TaskResult UpdateSubCategory(TblSubCategory entity)
        {
            try
            {
                var task = SubCategoryValidateUpdate(entity);

                if (task.ExecutedSuccesfully)
                {
                    var sesion = securityService.GetSession();
                    var SubCategory = subCategoryRepository.Get(x => x.Id == entity.Id).FirstOrDefault();

                    SubCategory.UpdateDate = DateTime.Now;
                    SubCategory.EmployeeModifyId = sesion.EmployeeId;
                    SubCategory.Name = entity.Name;
                    SubCategory.Description = entity.Description;
                    SubCategory.CategoryId = entity.CategoryId;

                    subCategoryRepository.Update(SubCategory);
                }

                return task;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateSubCategory", "InventoryService");
                serviceError.WarningMessage("Error al actualizar la categoría");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para cambiar de estado una SubCategoria
        /// </summary>
        /// <param name="Id">Primary key de la SubCategoria</param>
        /// <param name="status">Estado</param>
        /// <returns></returns>
        public TaskResult SetSubCategoryStatus(int Id, bool status)
        {
            TaskResult task = new TaskResult();

            if (Id == 0)
            {
                task.ExecutedSuccesfully = false;
                serviceError.LogError("Falta el ID de la Subcategoria", "SetSubCategoryStatus", "InventoryService", "Los datos no se han podido actualizar correctamente.");
            }

            if (task.ExecutedSuccesfully)
            {
                var SubCategory = GetSubCategory(Id);
                SubCategory.Estatus = status;
                subCategoryRepository.Update(SubCategory);
            }
            return task;
        }

        /// <summary>
        /// Metodo para hacer las validaciones de la SubCategoria cuando se esta actualizando
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion de la Subcategoria</param>
        /// <returns></returns>
        private TaskResult SubCategoryValidateUpdate(TblSubCategory entity)
        {
            try
            {
                if (string.IsNullOrEmpty(entity.Name))
                    return serviceError.WarningMessage("Falta el nombre de la Subcategoría.");


                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "SubCategoryValidateUpdate", "InventoryService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }
        #endregion

        #region Products
        /// <summary>
        /// Metodo para obtener los productos registrados para una empresa
        /// </summary>
        /// <param name="buisinessId"></param>
        /// <returns></returns>
        public IQueryable<PrcGetAllProducts> GetProductsInvoice(long buisinessId)
        {
            return productRepository.GetAllProduct(buisinessId);
        }

        /// <summary>
        /// Metodo para obtener el ultimo código generado
        /// </summary>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        public string LastProductCode(long businessId)
        {
            return productRepository.LastProductCode(businessId);
        }

        /// <summary>
        /// Metodo para obtener un producto
        /// </summary>
        /// <param name="Id">Primary key del producto</param>
        /// <returns></returns>
        public TblProducts GetProduct(long Id)
        {
            return productRepository.GetProduct(Id);
        }

        /// <summary>
        /// Metodo para agregar un producto al sistema
        /// </summary>
        /// <param name="entity">entidad con la informacion</param>
        /// <returns></returns>
        public async Task<TaskResult> CreateProduct(TblProducts entity)
        {
            try
            {
                var task = ProductValidateCreate(entity);

                if (task.ExecutedSuccesfully)
                {
                    var sesion = securityService.GetSession();

                    entity.BusinessId = sesion.BusinessId;
                    entity.CreateDate = DateTime.Now;
                    entity.UpdateDate = DateTime.Now;
                    entity.EmployeeCreateId = sesion.EmployeeId;
                    entity.EmployeeModifyId = sesion.EmployeeId;
                    entity.Estatus = true;

                    var tempPicture = globalService.GetTempFiles(sesion.UserId, (byte)TypeFiles.Product);

                    //Movemos la foto a su carpeta final
                    if (tempPicture != null && !string.IsNullOrEmpty(entity.Picture))
                    {
                        entity.Picture = tempPicture.LastOrDefault().FileName;
                        await globalService.MoveFile(entity.Picture, TypeFiles.Product);
                    }

                    //Removemos las fotos temporales que tenga el producto
                    if (tempPicture != null && !string.IsNullOrEmpty(entity.Picture))
                        globalService.DeleteRangeTempFile(tempPicture);


                    if (entity.TblProductsPrices.Count > 0)
                    {
                        foreach (var item in entity.TblProductsPrices)
                        {
                            item.EmployeeCreateId = sesion.EmployeeId;
                            item.EmployeeModifyId = sesion.EmployeeId;
                        }
                    }

                    //Si no se registraron cantidades para los almacenes los establecemos en 0
                    var wareHouses = await warehouseRepository.Get(x => x.BranchOfficeId == sesion.BranchOfficeId).ToListAsync();
                    foreach (var item in wareHouses)
                    {
                        if (!entity.TblInventoryWarehouse.Where(x => x.WarehouseId == item.Id).Any())
                        {
                            TblInventoryWarehouse inventory = new TblInventoryWarehouse();
                            inventory.ProductAmount = 0;
                            inventory.WarehouseId = item.Id;
                            entity.TblInventoryWarehouse.Add(inventory);
                        }
                    }

                    productRepository.AddEntity(entity);
                }

                return task;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateProduct", "InventoryService", "Se presentó un problema al registrar el producto, favor de contactar a soporte.");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para hacer las validaciones del producto cuando se esta registrando
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion de la medida</param>
        /// <returns></returns>
        private TaskResult ProductValidateCreate(TblProducts entity)
        {
            try
            {
                var sesion = securityService.GetSession();

                if (string.IsNullOrEmpty(entity.Name))
                    return serviceError.WarningMessage("Falta el nombre del producto.");
                else if (string.IsNullOrEmpty(entity.Code))
                    return serviceError.WarningMessage("Falta el código para el producto.");
                else if (entity.Cost < 0)
                    return serviceError.WarningMessage("Falta el costo del producto.");
                else if (entity.TblInventoryWarehouse.Count < 0)
                    return serviceError.WarningMessage("Debe agregar la cantidad inicial al almacén.");
                else if (entity.TblProductsPrices.Count == 0 || entity.TblProductsPrices.Where(x => x.TaxId <= 0).Any())
                    return serviceError.WarningMessage("Debe seleccionar un impuesto para el precio del producto.");
                else if (entity.TblProductsPrices.Count == 0 || entity.TblProductsPrices.Where(x => x.Utility < 0).Any())
                    return serviceError.WarningMessage("La ganancia de un producto no pude estar en blanco o ser menor que 0.");
                else if (entity.TblProductsPrices.Count == 0 || entity.TblProductsPrices.Where(x => x.Price < 0).Any())
                    return serviceError.WarningMessage("El precio de un producto no pude estar en blanco o ser menor que 0.");
                else if (productRepository.Get(x => x.BusinessId == sesion.BusinessId && x.Code == entity.Code).Any())
                    return serviceError.WarningMessage("El código ingresado ya existe en la base de datos, debe cambiar el código ingresado");


                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "ProductValidateCreate", "InventoryService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }


        /// <summary>
        /// Proceso para agregar un producto al sistema
        /// </summary>
        /// <param name="entity">entidad con la informacion</param>
        /// <returns></returns>
        public TaskResult UpdateProduct(TblProducts entity)
        {
            try
            {
                var task = ProductValidateUpdate(entity);

                if (task.ExecutedSuccesfully)
                {
                    var sesion = securityService.GetSession();
                    var product = GetProduct(entity.Id);

                    product.UpdateDate = DateTime.Now;
                    product.EmployeeModifyId = sesion.EmployeeId;
                    product.SupplierId = entity.SupplierId;
                    product.SubCategoryId = entity.SubCategoryId;
                    product.MeasureId = entity.MeasureId;
                    product.TaxId = entity.TaxId;
                    product.Name = entity.Name;
                    product.Description = entity.Description;
                    product.IsService = entity.IsService;
                    product.InvoiceWithoutExistence = entity.InvoiceWithoutExistence;
                    product.MinimumAmount = entity.MinimumAmount;
                    product.MinimumAmount = entity.MinimumAmount;
                    product.Reference = entity.Reference;
                    product.Observation = entity.Observation;
                    product.LimitAmountReport = entity.LimitAmountReport;
                    product.SellLowCost = entity.SellLowCost;

                    var tempPicture = globalService.GetTempFiles(sesion.UserId, (byte)TypeFiles.Product);

                    //Movemos la foto a su carpeta final
                    if (tempPicture != null && !string.IsNullOrEmpty(entity.Picture))
                    {
                        entity.Picture = tempPicture.LastOrDefault().FileName;
                        globalService.MoveFile(entity.Picture, TypeFiles.Product);

                        if (product.Picture != null)
                            amazonService.DeleteFile(product.Picture, TypeFiles.Product);//Eliminamos la foto de amazon web service

                        //
                        product.Picture = entity.Picture;
                    }


                    //Removemos las fotos temporales que tenga el producto
                    if (tempPicture != null && !string.IsNullOrEmpty(product.Picture))
                        globalService.DeleteRangeTempFile(tempPicture);


                    //=======================================Actualizando precio=================================================
                    var productPrice = product.TblProductsPrices;


                    ////Las filas que fueron agregadas
                    var NewRows = (from p in entity.TblProductsPrices
                                   where !(from b in productPrice
                                           select b.Id)
                                             .Contains(p.Id)
                                   select p).Distinct().ToList();


                    ////Las filas que fueron eliminadas
                    var DeleteRows = (from p in productPrice
                                      where !(from b in entity.TblProductsPrices
                                              select b.Id)
                                                .Contains(p.Id)
                                      select p).Distinct().ToList();


                    //Eliminamos los precios si existen
                    if (DeleteRows.Count > 0)
                        productPriceRepository.DeleteRange(DeleteRows);

                    //Agregamos nuevos precios si existen nuevos registros
                    if (NewRows.Count > 0)
                        foreach (var item in NewRows)
                            product.TblProductsPrices.Add(item);


                    //Actualizamos el producto
                    productRepository.Update(product);
                }

                return task;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateProduct", "InventoryService");
                serviceError.WarningMessage("Error al actualizar el producto");
                return serviceError.TaskStatus();
            }
        }


        /// <summary>
        /// Metodo para activar o desactivar un producto
        /// </summary>
        /// <param name="status">Estaus para el producto</param>
        /// <param name="Id">LLave primaria del producto</param>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        public TaskResult StatusProduct(bool status, long Id, long businessId)
        {
            TaskResult task = new TaskResult();

            if (Id == 0)
                task = serviceError.WarningMessage("El primary key del producto no puede ser 0.");
            else
            {
                var CustomerDb = productRepository.Get(Id);
                CustomerDb.Estatus = status;

                productRepository.Update(CustomerDb);
                task.ExecutedSuccesfully = true;
            }

            return task;
        }

        /// <summary>
        /// Metodo para hacer las validaciones del producto cuando se esta actualizando
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion de la medida</param>
        /// <returns></returns>
        private TaskResult ProductValidateUpdate(TblProducts entity)
        {
            try
            {
                if (string.IsNullOrEmpty(entity.Name))
                    return serviceError.WarningMessage("Falta el nombre del producto.");
                else if (entity.Id == 0)
                    return serviceError.WarningMessage("Falta el primary key del producto.");
                else if (entity.Cost < 0)
                    return serviceError.WarningMessage("Falta el costo del producto.");

                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "ProductValidateUpdate", "InventoryService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para obtener el listado de precios que tiene un producto
        /// </summary>
        /// <param name="ProductId">Primary key del producto</param>
        /// <returns></returns>
        public IQueryable<TblProductsPrices> GetProductsPrices(long ProductId)
        {
            return productPriceRepository.Get(x => x.ProductsId == ProductId);
        }
        public IQueryable<TblProducts> GetProducts(long buisinessId)
        {
            return productRepository.Products(buisinessId);
        }
        #endregion



        /// <summary>
        /// Proceso para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            var task = await warehouseRepository.Savechanges();

            if (!task.ExecutedSuccesfully)
                serviceError.LogError(task.MessageList[0], "Savechanges", "InventoryService", "Error al intentar guardar los datos.");

            return task;
        }
    }

    public interface IInventoryService
    {

        #region Nota de Debito

        /// Metodo que trae el listado de nota de Debito
        /// </summary>
        /// <returns></returns>
        IQueryable<DebitNote> ListDebitNote(long branchOfficeId);

        /// Metodo que trae el detalle correspondiente
        /// </summary>
        /// <returns></returns>
        TblDebitNote DebitNoteDetails(long DebitNoteId);


        /// Metodo que trae el listado de detalle nota de debito
        /// </summary>
        /// <returns></returns>
        List<DebitNoteRowVM> GetDebitNoteRow(long DebitNoteId);

        /// <summary>
        /// Metodo para crear los movimientos de almacen de una sucursal
        /// </summary>
        /// <param name="movingWareHouse">Modelo que contiene el movimiento de producto de un almacen</param>
        /// <returns></returns>
        TaskResult CreateDebitNote(TblDebitNote debitNote, string tipoProceso);

        /// <summary>
        /// Metodo para actualizar las notas de debitos
        /// </summary>
        /// <param name="debitNote">Modelo que contiene la nota de debito</param>
        /// <returns></returns>
        TaskResult UpdateDebitNote(TblDebitNote debitNote);


        /// <summary>
        /// Proceso para cambiar el estado de las notas de debito
        /// </summary>
        /// <param name="Id">parametro que contiene el Id de la nota de debito</param>
        /// <param name="businessId">parametro que contiene el Id del negocio</param>
        /// <param name="DocumentStatusID">parametro que contiene el Id del estado</param>
        /// <returns></returns>
        TaskResult SetDebitNotaStatus(int Id, long DocumentStatusID);

        /// <summary>
        /// Metodo para agregar el NCF a la nota de debito
        /// </summary>
        /// <param name="branchOfficeId">Primary key de la sucursal</param>
        /// <param name="DebitNoteId">Primary key de la cabecera de la nota de debito</param>
        /// <returns></returns>
        bool AddNcfDebitNote(long branchOfficeId, long DebitNoteId);

        #endregion

        #region Movinmiento Almacen


        /// <summary>
        /// Metodo para obtener los movimientos de almacen de una sucursal
        /// </summary>
        /// <param name="WarehouseId">Primary key de la sucursal</param>
        /// <returns></returns>
        IQueryable<PrcGetAllMovingWare> GetMovingWarehouse(long WarehouseId);

        /// <summary>
        /// Metodo para obtener los movimientos de almacen de una sucursal
        /// </summary>
        /// <param name="MovingId">Primary key de la sucursal</param>
        /// <returns></returns>
        TblMovingWarehouse GetMovingDetails(long MovingId);

        /// <summary>
        /// Metodo el detalle del movimiento
        /// </summary>
        /// <param name="MovingId">Parametro que contiene el Id del movimiento</param>    
        /// <returns></returns>
        IQueryable<TblMovingWarehouseRow> DatetailsRows(long MovingId);


        /// <summary>
        /// Metodo para llamar el producto del movimineto
        /// </summary>
        /// <param name="MovingId">Parametro que contiene el Id del movimiento</param>    
        /// <param name="ProductId">Parametro que contiene el Id del producto</param>    
        /// <returns></returns>
        PrGetMovingWareDetails ProductRow(long MovingId, long ProductId);

        /// <summary>
        /// Metodo para crear los movimientos de almacen de una sucursal
        /// </summary>
        /// <param name="movingRow">Modelo que contiene el id del detalle del movimiento</param>
        /// <returns></returns>
        TaskResult UpdateMovingWareRow(PrGetMovingWareDetails movingRow);

        /// <summary>
        /// Metodo para crear los movimientos de almacen de una sucursal
        /// </summary>
        /// <param name="movingWareHouse">Modelo que contiene el movimiento de producto de un almacen</param>
        /// <returns></returns>
        TaskResult CreateMovingWare(TblMovingWarehouse movingWareHouse, string tipoProceso);

        /// <summary>
        /// Metodo para crear los movimientos de almacen de una sucursal
        /// </summary>
        /// <param name="movingWareHouse">Modelo que contiene el movimiento de producto de un almacen</param>
        /// <returns></returns>
        TaskResult UpdateMovingWare(TblMovingWarehouse movingWareHouse);


        /// <summary>
        /// Metodo para cambiar el estado del movimiento
        /// </summary>
        /// <param name="movingWareHouse">Modelo que contiene el movimiento de producto de un almacen</param>
        /// <returns></returns>
        TaskResult ChangeStatu(long movingWareHouseId, int estado);

        /// <summary>
        /// Metodo que trae todos los producto del inventario del almacen
        /// </summary>
        /// <param name="WarehouseId">Parametro que contiene el Id del almacen</param>
        /// <param name="ProductId">Parametro que contiene el Id del producto</param>
        /// <returns></returns>
        IQueryable<ProductInventory> GetProducts(long WarehouseId, long ProductId);


        /// <summary>
        /// Metodo que elimina los productos de los moviminetos
        /// </summary>
        /// <param name="MovingIdRow">Parametro que contiene el Id del detalle del movimiento</param>
        /// <returns></returns>
        TaskResult DeleteMovingIdRow(long MovingIdRow);



        /// <summary>
        /// Metodo para cambiar el estado del movimiento
        /// </summary>
        /// <param name="model">Modelo que contiene el detalle de movimiento de un producto en un almacen</param>
        /// <returns></returns>
        TaskResult ReturnProduct(PrGetMovingWareDetails model, int estado);

        /// <summary>
        /// Metodo para obtener todos los almacenes de una empresa
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        IQueryable<TblWarehouse> GetAllWarehouses(long BranchOfficeId);
        #endregion

        #region almacen

        /// <summary>
        /// Metodo para obtener los almacenes de una sucursal
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <param name="active">Estatus del almacen (opcional)</param>
        /// <returns></returns>
        Task<IQueryable<TblWarehouse>> GetWarehouses(long BranchOfficeId, bool? active = null);

        /// <summary>
        /// Metodo para obtener los almacenes de una empresa
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        IQueryable<TblWarehouse> GetBusinessWarehouses(long BusinessId);

        /// <summary>
        /// Proceso para agregar un almacén al sistema
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<TaskResult> CreateWarehouse(TblWarehouse entity);

        /// <summary>
        /// metodo para actualizar un almacén al sistema
        /// </summary>
        /// <param name="entity">Entidad con la informacion</param>
        /// <returns></returns>
        TaskResult UpdateWarehouse(TblWarehouse entity);

        /// <summary>
        /// Metodo para cambiar de estado un almacén
        /// </summary>
        /// <param name="Id">Primary key del almacén</param>
        /// <param name="status">Estado</param>
        /// <returns></returns>
        TaskResult SetWarehouseStatus(int Id, bool status);

        /// <summary>
        /// Metodo para obtener un almacen
        /// </summary>
        /// <param name="WarehouseId">Primary key del almacen</param>
        /// <returns></returns>
        TblWarehouse GetWarehouse(int WarehouseId);

        /// <summary>
        /// Metodo para obtener el almacén principal de una sucursal
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        TblWarehouse GetPrincipalWarehouse(long BranchOfficeId);

        #endregion

        #region Medidas
        /// <summary>
        /// Metodo para obtener todas las medidas del sistema
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>

        IQueryable<TblMeasure> GetMeasurements(long BusinessId);

        /// <summary>
        /// Proceso para agregar una medida al sistema
        /// </summary>
        /// <param name="entity">entidad con la informacion</param>
        /// <returns></returns>
        TaskResult CreateMeasure(TblMeasure entity);

        /// <summary>
        /// Metodo para obtener una medida
        /// </summary>
        /// <param name="Id">Primary key de la medida</param>
        /// <returns></returns>
        TblMeasure GetMeasure(int Id);

        /// <summary>
        /// metodo para actualizar una medida
        /// </summary>
        /// <param name="model">Entidad con la informacion</param>
        /// <returns></returns>
        TaskResult UpdateMeasure(TblMeasure entity);

        /// <summary>
        /// Metodo para cambiar de estado una medida
        /// </summary>
        /// <param name="Id">Primary key de la medida</param>
        /// <param name="status">Estado</param>
        /// <returns></returns>
        TaskResult SetMeasureStatus(int Id, bool status);
        #endregion

        #region categorias
        /// <summary>
        /// Metodo para obtener todas las categorias del sistema
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        IQueryable<TblCategory> GetCategories(long BusinessId);

        /// <summary>
        /// Proceso para agregar una categoria al sistema
        /// </summary>
        /// <param name="entity">entidad con la informacion</param>
        /// <returns></returns>
        TaskResult CreateCategory(TblCategory entity);

        /// <summary>
        /// Metodo para obtener una categoria
        /// </summary>
        /// <param name="Id">Primary key de la medida</param>
        /// <returns></returns>
        TblCategory GetCategory(int Id);

        /// <summary>
        /// metodo para actualizar una categoria
        /// </summary>
        /// <param name="model">Entidad con la informacion</param>
        /// <returns></returns>
        TaskResult UpdateCategory(TblCategory entity);

        /// <summary>
        /// Metodo para cambiar de estado una categoria
        /// </summary>
        /// <param name="Id">Primary key de la categoria</param>
        /// <param name="status">Estado</param>
        /// <returns></returns>
        TaskResult SetCategoryStatus(int Id, bool status);
        #endregion

        #region SubcCategorias

        /// <summary>
        /// Metodo para obtener todas las SubCategorias de una empresa
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        IQueryable<TblSubCategory> GetBusinesSubCategories(long BusinessId);

        /// <summary>
        /// Metodo para obtener todas las SubCategorias de una categoria
        /// </summary>
        /// <param name="CategoryId">Primary key de la categoria</param>
        /// <returns></returns>
        IQueryable<TblSubCategory> GetSubCategories(long CategoryId);

        /// <summary>
        /// Proceso para agregar una subcategoria al sistema
        /// </summary>
        /// <param name="entity">entidad con la informacion</param>
        /// <returns></returns>
        TaskResult CreateSubCateogry(TblSubCategory entity);

        /// <summary>
        /// Metodo para obtener una SubCategoria
        /// </summary>
        /// <param name="Id">Primary key de la SubCategoria</param>
        /// <returns></returns>
        TblSubCategory GetSubCategory(int Id);

        /// <summary>
        /// metodo para actualizar una subcategoria
        /// </summary>
        /// <param name="model">Entidad con la informacion</param>
        /// <returns></returns>
        TaskResult UpdateSubCategory(TblSubCategory entity);

        /// <summary>
        /// Metodo para cambiar de estado una SubCategoria
        /// </summary>
        /// <param name="Id">Primary key de la SubCategoria</param>
        /// <param name="status">Estado</param>
        /// <returns></returns>
        TaskResult SetSubCategoryStatus(int Id, bool status);
        #endregion

        #region productos
        /// <summary>
        /// Metodo para obtener los productos registrados para una empresa
        /// </summary>
        /// <param name="buisinessId"></param>
        /// <returns></returns>
        IQueryable<PrcGetAllProducts> GetProductsInvoice(long buisinessId);



        // <returns></returns>

        IQueryable<TblProducts> GetProducts(long buisinessId);



        /// <summary>
        /// Metodo para obtener un producto
        /// </summary>
        /// <param name="Id">Primary key del producto</param>
        /// <returns></returns>
        TblProducts GetProduct(long Id);

        /// <summary>
        /// Proceso para agregar un producto al sistema
        /// </summary>
        /// <param name="entity">entidad con la informacion</param>
        /// <returns></returns>
        Task<TaskResult> CreateProduct(TblProducts entity);

        /// <summary>
        /// Proceso para agregar un producto al sistema
        /// </summary>
        /// <param name="entity">entidad con la informacion</param>
        /// <returns></returns>
        TaskResult UpdateProduct(TblProducts entity);

        /// <summary>
        /// Metodo para obtener el ultimo código generado
        /// </summary>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        string LastProductCode(long businessId);

        /// <summary>
        /// Metodo para obtener el listado de precios que tiene un producto
        /// </summary>
        /// <param name="ProductId">Primary key del producto</param>
        /// <returns></returns>
        IQueryable<TblProductsPrices> GetProductsPrices(long ProductId);

        /// <summary>
        /// Metodo para activar o desactivar un producto
        /// </summary>
        /// <param name="status">Estaus para el producto</param>
        /// <param name="Id">LLave primaria del producto</param>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        TaskResult StatusProduct(bool status, long Id, long businessId);


        #endregion



        /// <summary>
        /// Proceso para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
