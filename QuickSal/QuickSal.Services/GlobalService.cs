﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using QuickSal.DataAcces;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.CustomModels.Models;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Repositories;
using QuickSal.Services.Support;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace QuickSal.Services
{
    public class GlobalService : IGlobalService
    {
        private IGlobalRepository globalRepository;
        private IServiceError logService;
        private IHostingEnvironment environmentService;
        private IConfiguration configurationService;
        private IAmazonService amazonService;

        public GlobalService(IGlobalRepository globalRepository, IHostingEnvironment environmentService,
                             IServiceError serviceError, IConfiguration configurationService, IAmazonService amazonService)
        {
            this.globalRepository = globalRepository;
            this.logService = serviceError;
            this.environmentService = environmentService;
            this.configurationService = configurationService;
            this.amazonService = amazonService;
        }

        /// <summary>
        /// Metodo para generar una password aleatorio
        /// </summary>
        /// <returns></returns>
        public string GeneratePassword()
        {
            int longitud = 7;
            Guid miGuid = Guid.NewGuid();
            string token = Convert.ToBase64String(miGuid.ToByteArray());
            token = token.Replace("=", "").Replace("+", "");
            return token.Substring(0, longitud);
        }

        /// <summary>
        /// Metodo para obtener la descripcion de un enum
        /// </summary>
        /// <param name="value">Valor del enum</param>
        /// <returns></returns>
        public string GetEnumDescription(Enum value)
        {
            // Get the Description attribute value for the enum value
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                    typeof(DescriptionAttribute), false);

            if (attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return value.ToString();
            }
        }


        /// <summary>
        /// Proceso para obtener todos los paises
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TblCountry> GetAllCountries()
        {
            return globalRepository.GetAllCountries();
        }

        /// <summary>
        /// Meotodo para obtener todos los prefijos de los NCF
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TblNcfprefix> GetAllPrefixNCF()
        {
            return globalRepository.GetAllPrefix();
        }

        /// <summary>
        /// Proceso para obtener todos los tipos de documentos registrados
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TblDocumentIdentification> GetAllDocumentIdentification()
        {
            return globalRepository.GetAllDocumentIdentification();
        }


        /// <summary>
        /// Proceso para obtener todos un tipo de identificacion
        /// </summary>
        /// <param name="Id">Primary key del tipo de identificación</param>
        /// <returns></returns>
        public async Task<TblDocumentIdentification> GetDocumentIdentification(int Id)
        {
            return await globalRepository.GetDocumentIdentification(Id);
        }

        /// <summary>
        /// Metodo para obtener todas las provincias de una ciudad
        /// </summary>
        /// <param name="CountryId">Primary key del pais</param>
        /// <returns></returns>
        public IQueryable<TblProvince> GetAllProvince(int CountryId)
        {
            return globalRepository.GetAllProvince(CountryId);
        }


        /// <summary>
        /// Proceso para obtener los objetos temporales almacenados en la DB
        /// </summary>
        /// <param name="UserId">Primary key del usuario que grabo el archivo</param>
        /// <param name="FileType">Tipo de archivo que se registro en la tabla temporal</param>
        /// <returns></returns>
        public IQueryable<TblTempFile> GetTempFiles(long UserId, byte FileType)
        {
            return globalRepository.GetTempFiles(UserId, FileType);
        }


        /// <summary>
        /// Metodo para convertir un Listado de objetos en un array de objetos
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model">Listado de objectos que se desean convertir</param>
        /// <param name="ColumnsName">Array con el nombre de las columnas que se desean obtener</param>
        /// <returns></returns>
        public List<object[]> ConvertToArray<T>(IEnumerable<T> model, string[] ColumnsName)
        {
            //Construye el link del reporte.
            Type type = typeof(T); // Get type pointer
            PropertyInfo[] properties = typeof(T).GetProperties();

            List<object[]> values = new List<object[]>();

            foreach (var item in model)
            {
                object[] value = new object[ColumnsName.Length];
                foreach (PropertyInfo property in properties)
                {
                    //así obtenemos el nombre del atributo
                    string NombreAtributo = property.Name;
                    for (int i = 0; i < ColumnsName.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(ColumnsName[i]) && ColumnsName[i] == NombreAtributo)
                        {
                            value[i] = type.GetProperty(property.Name).GetValue(item);
                            break;
                        }
                    }
                }
                values.Add(value);
            }

            return values;
        }

        /// <summary>
        /// Metodo para generar nueva secuendia del codigo de una entidad
        /// </summary>
        /// <param name="type">Tipo de entidad</param>
        /// <param name="lastCode">Ultimo codigo generado</param>
        /// <returns></returns>
        public string GenerateCode(EnumTypes.CodeEntities type, string lastCode = null)
        {
            string Quantity = "0000000";
            string code = "";

            if (type == EnumTypes.CodeEntities.Employee)
            {
                int cleanCode = Convert.ToInt16(((string.IsNullOrEmpty(lastCode)) ? "0" : lastCode.Replace("E", "")));
                int TotalLength = cleanCode.ToString().Length;

                code = Convert.ToString("E" + Quantity.Remove(Quantity.Length - TotalLength, TotalLength)) + (cleanCode + 1);
            }
            else if (type == EnumTypes.CodeEntities.Customer)
            {
                int cleanCode = Convert.ToInt16(((string.IsNullOrEmpty(lastCode)) ? "0" : lastCode.Replace("C", "")));
                int TotalLength = cleanCode.ToString().Length;

                code = Convert.ToString("C" + Quantity.Remove(Quantity.Length - TotalLength, TotalLength)) + (cleanCode + 1);
            }
            else if (type == EnumTypes.CodeEntities.Supplier)
            {
                int cleanCode = Convert.ToInt16(((string.IsNullOrEmpty(lastCode)) ? "0" : lastCode.Replace("P", "")));
                int TotalLength = cleanCode.ToString().Length;

                code = Convert.ToString("P" + Quantity.Remove(Quantity.Length - TotalLength, TotalLength)) + (cleanCode + 1);
            }
            else if (type == EnumTypes.CodeEntities.PurshOrder)
            {
                int cleanCode = Convert.ToInt16(((string.IsNullOrEmpty(lastCode)) ? "0" : lastCode.Replace("OR", "")));
                int TotalLength = cleanCode.ToString().Length;
                code = Convert.ToString("OR" + Quantity.Remove(Quantity.Length - TotalLength, TotalLength)) + (cleanCode + 1);
            }
            else if (type == EnumTypes.CodeEntities.Product)
            {
                int cleanCode = Convert.ToInt16(((string.IsNullOrEmpty(lastCode)) ? "0" : lastCode.Replace("PO", "")));
                int TotalLength = cleanCode.ToString().Length;

                code = Convert.ToString("PO" + Quantity.Remove(Quantity.Length - TotalLength, TotalLength)) + (cleanCode + 1);
            }
            else if (type == EnumTypes.CodeEntities.Invoice)
            {
                int cleanCode = Convert.ToInt16(((string.IsNullOrEmpty(lastCode)) ? "0" : lastCode.Replace("F", "")));
                int TotalLength = cleanCode.ToString().Length;

                code = Convert.ToString("F" + Quantity.Remove(Quantity.Length - TotalLength, TotalLength)) + (cleanCode + 1);
            }
            else if (type == EnumTypes.CodeEntities.Moving)
            {
                int cleanCode = Convert.ToInt16(((string.IsNullOrEmpty(lastCode)) ? "0" : lastCode.Replace("M", "")));
                int TotalLength = cleanCode.ToString().Length;

                code = Convert.ToString("M" + Quantity.Remove(Quantity.Length - TotalLength, TotalLength)) + (cleanCode + 1);
            }
            else if (type == EnumTypes.CodeEntities.DebitNote)
            {
                if (lastCode.Contains("ND"))
                {
                    int cleanCode = Convert.ToInt16(((string.IsNullOrEmpty(lastCode)) ? "0" : lastCode.Replace("ND", "")));
                    int TotalLength = cleanCode.ToString().Length;

                    code = Convert.ToString("ND" + Quantity.Remove(Quantity.Length - TotalLength, TotalLength)) + (cleanCode + 1);
                }
            }
            else if (type == EnumTypes.CodeEntities.VendorPayment)
            {
                int cleanCode = Convert.ToInt16(((string.IsNullOrEmpty(lastCode)) ? "0" : lastCode.Replace("PP", "")));
                int TotalLength = cleanCode.ToString().Length;
                code = Convert.ToString("PP" + Quantity.Remove(Quantity.Length - TotalLength, TotalLength)) + (cleanCode + 1);
            }
            else if (type == EnumTypes.CodeEntities.CustomerPayment)
            {
                int cleanCode = Convert.ToInt16(((string.IsNullOrEmpty(lastCode)) ? "0" : lastCode.Replace("CP", "")));
                int TotalLength = cleanCode.ToString().Length;
                code = Convert.ToString("CP" + Quantity.Remove(Quantity.Length - TotalLength, TotalLength)) + (cleanCode + 1);
            }
            else if (type == EnumTypes.CodeEntities.CreditNote)
            {
                int cleanCode = Convert.ToInt16(((string.IsNullOrEmpty(lastCode)) ? "0" : lastCode.Replace("NC", "")));
                int TotalLength = cleanCode.ToString().Length;
                code = Convert.ToString("NC" + Quantity.Remove(Quantity.Length - TotalLength, TotalLength)) + (cleanCode + 1);
            }
            else if (type == EnumTypes.CodeEntities.Order)
            {
                int cleanCode = Convert.ToInt16(((string.IsNullOrEmpty(lastCode)) ? "0" : lastCode.Replace("OP", "")));
                int TotalLength = cleanCode.ToString().Length;
                code = Convert.ToString("OP" + Quantity.Remove(Quantity.Length - TotalLength, TotalLength)) + (cleanCode + 1);
            }

            return code;
        }


        /// <summary>
        /// Proceso para encriptar un string
        /// </summary>
        /// <param name="cadena"></param>
        /// <returns></returns>
        public string SHA1Encrypt(string cadena)
        {
            HashAlgorithm hasvalue = new SHA512CryptoServiceProvider();

            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(cadena);

            byte[] bytehash = hasvalue.ComputeHash(bytes);

            hasvalue.Clear();

            return Convert.ToBase64String(bytehash);
        }

        /// <summary>
        /// Proceso para descriptar un string
        /// </summary>
        /// <param name="textoEncriptado">String encriptado</param>
        /// <returns></returns>
        public string TripleDesDescrypt(string textoEncriptado)
        {
            try
            {
                string key = "Quicksal201808";
                byte[] keyArray;
                byte[] Array_a_Descifrar = Convert.FromBase64String(textoEncriptado);

                //algoritmo MD5
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();

                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));

                hashmd5.Clear();

                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();

                tdes.Key = keyArray;
                tdes.Mode = CipherMode.ECB;
                tdes.Padding = PaddingMode.PKCS7;

                ICryptoTransform cTransform = tdes.CreateDecryptor();

                byte[] resultArray = cTransform.TransformFinalBlock(Array_a_Descifrar, 0, Array_a_Descifrar.Length);

                tdes.Clear();
                textoEncriptado = UTF8Encoding.UTF8.GetString(resultArray);

            }
            catch (Exception)
            {

            }
            return textoEncriptado;
        }

        /// <summary>
        /// Proceso para aplicar un encriptardo con llave
        /// </summary>
        /// <param name="texto">String que se encriptara</param>
        /// <returns></returns>
        public string TripleDesEncrypt(string texto)
        {
            try
            {

                string key = "Quicksal201808"; //llave para encriptar datos

                byte[] keyArray;

                byte[] Arreglo_a_Cifrar = UTF8Encoding.UTF8.GetBytes(texto);

                //Se utilizan las clases de encriptación MD5

                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();

                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));

                hashmd5.Clear();

                //Algoritmo TripleDES
                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();

                tdes.Key = keyArray;
                tdes.Mode = CipherMode.ECB;
                tdes.Padding = PaddingMode.PKCS7;

                ICryptoTransform cTransform = tdes.CreateEncryptor();

                byte[] ArrayResultado = cTransform.TransformFinalBlock(Arreglo_a_Cifrar, 0, Arreglo_a_Cifrar.Length);

                tdes.Clear();

                //se regresa el resultado en forma de una cadena
                texto = Convert.ToBase64String(ArrayResultado, 0, ArrayResultado.Length);

            }
            catch (Exception)
            {

            }
            return texto;
        }

        /// <summary>
        /// Metodo para almacenar una imagen temporal
        /// </summary>
        /// <param name="file">Información del archivo</param>
        /// <param name="fileInMemory">Stream que contiene la foto</param>
        /// <returns></returns>
        public async Task<TaskResult> AddTempFile(TblTempFile file, Stream fileInMemory)
        {
            var ImgUploaded = Image.FromStream(fileInMemory);

            var stream = new MemoryStream();
            var NewImage = CropImage(Image.FromStream(fileInMemory), 0, 0, ImgUploaded.Width, ImgUploaded.Height, 110, 110);
            NewImage.Save(stream, ImageFormat.Jpeg);
            await amazonService.UploadTempFile(stream, file.FileName);


            return globalRepository.AddTempFileEntity(file);
        }

        /// <summary>
        /// Metodo para obtener una url temporal del archivo en la carpeta temporal
        /// </summary>
        /// <param name="objectKey">Nombre del archivo</param>
        /// <returns></returns>
        public async Task<string> GetTempUrlObjet(string objectKey)
        {
            return await amazonService.GetTempUrlObjet(objectKey);
        }


        /// <summary>
        /// Metodo para eliminar un archivo temporal
        /// </summary>
        /// <param name="entity">Modelo con la informacion</param>
        /// <returns></returns>
        public void DeleteTempFile(TblTempFile file)
        {
            globalRepository.DeleteTempFile(file);
        }

        /// <summary>
        /// Metodo para eliminar archivo temporales de la tabal temp files
        /// </summary>
        /// <param name="entity">Modelo con la informacion</param>
        /// <returns></returns>
        public void DeleteRangeTempFile(IEnumerable<TblTempFile> entity)
        {
            globalRepository.DeleteRangeTempFile(entity);
        }

        /// <summary>
        /// Metodo para validar si un request es tipo ajax
        /// </summary>
        /// <param name="request">Request que llega en el controller</param>
        /// <returns></returns>
        public bool IsAjaxRequest(HttpRequest request)
        {
            if (request == null)
            {
                logService.LogError("Error al validar metodo IsAjaxRequest", "IsAjaxRequest", "GlobalService", "La variable request llega con valor null.");
                return false;
            }
            else
                return ((request.Headers["X-Requested-With"] == "XMLHttpRequest") ? true : false);

        }


        /// <summary>
        /// Proceso para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            var task = await globalRepository.Savechanges();

            if (!task.ExecutedSuccesfully)
                logService.LogError(task.MessageList[0], "Savechanges", "GlobalService", "Error al intentar guardar los datos.");

            return task;
        }


        /// <summary>
        /// Metodo para mover un archivo desde la carpeta temporar a su carpeta
        /// </summary>
        /// <param name="name"></param>
        /// <param name="typeFiles"></param>
        /// <returns></returns>
        public async Task<TaskResult> MoveFile(string name, QuickSalEnum.TypeFiles typeFiles)
        {
            try
            {
                return await amazonService.MoveFileFromTempFolder(name, typeFiles);
            }
            catch (Exception ex)
            {
                logService.LogError(ex.Message, "MoveFile", "GlobalService", "Uno de los archivos cargados no se ha encontrado intenta cargarlo nuevamente.");
                return logService.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para recordar una imagen
        /// </summary>
        /// <param name="sourceImage"></param>
        /// <param name="sourceX"></param>
        /// <param name="sourceY"></param>
        /// <param name="sourceWidth"></param>
        /// <param name="sourceHeight"></param>
        /// <param name="destinationWidth"></param>
        /// <param name="destinationHeight"></param>
        /// <returns></returns>
        public Image CropImage(Image sourceImage, int sourceX, int sourceY, int sourceWidth, int sourceHeight, int destinationWidth, int destinationHeight)
        {
            Image destinationImage = new Bitmap(destinationWidth, destinationHeight);
            Graphics g = Graphics.FromImage(destinationImage);

            g.DrawImage(
              sourceImage,
              new Rectangle(0, 0, destinationWidth, destinationHeight),
              new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
              GraphicsUnit.Pixel
            );

            return destinationImage;
        }


        /// <summary>
        /// Metodo para validar si un string tiene alguna sentencia sql
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool validateSQLstatement(string value)
        {
            string[] statement = { "select", "delete", "from", "drop", "*", "alter" };
            bool status = false;

            for (int i = 0; i < statement.Length; i++)
            {
                if (value.ToLower().Contains(statement[i]))
                {
                    status = true;
                    break;
                }
            }
            return status;
        }

        /// <summary>
        /// Metodo para validar que un string no contenga caracteres especiales
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool validateSpecialcharacters(string value)
        {
            return Regex.IsMatch(value, @"[^\w\s.!@$%^&()\-\/]+");
        }


        /// <summary>
        /// Metodo para obtener el o los valores de un parametro
        /// </summary>
        /// <param name="nameSetting">Parametros</param>
        /// <param name="valueName">Nombre del valor</param>
        /// <returns></returns>
        public async Task<IQueryable<Setting>> GetSettingValue(string nameSetting, string valueName = null)
        {
            return await globalRepository.GetSettingValue(nameSetting, valueName);
        }

        /// <summary>
        /// Metodo para obtener la descripcion de un enum
        /// </summary>
        /// <param name="tipoEnum"></param>
        /// <returns></returns>
        public string GetListDesc(Enum tipoEnum)
        {
            var query = tipoEnum.GetType().
                    GetField(tipoEnum.ToString()).GetCustomAttributes(true).
                    Where(a => a.GetType().Equals(typeof(System.ComponentModel.DescriptionAttribute)));

            return (tipoEnum.GetType().
                    GetField(tipoEnum.ToString()).GetCustomAttributes(true).
                    Where(a => a.GetType().Equals(typeof(System.ComponentModel.DescriptionAttribute))).
                    FirstOrDefault() as System.ComponentModel.DescriptionAttribute).Description;
        }

    }

    public interface IGlobalService
    {
        /// <summary>
        /// Proceso para obtener todos los paises
        /// </summary>
        /// <returns></returns>
        IEnumerable<TblCountry> GetAllCountries();

        /// <summary>
        /// Metodo para validar si un request es tipo ajax
        /// </summary>
        /// <param name="request">Request que llega en el controller</param>
        /// <returns></returns>
        bool IsAjaxRequest(HttpRequest request);

        /// <summary>
        /// Metodo para obtener todas las provincias de una ciudad
        /// </summary>
        /// <param name="CountryId">Primary key del pais</param>
        /// <returns></returns>
        IQueryable<TblProvince> GetAllProvince(int CountryId);
        /// <summary>
        /// Proceso para obtener todos los tipos de documentos registrados
        /// </summary>
        /// <returns></returns>
        IEnumerable<TblDocumentIdentification> GetAllDocumentIdentification();

        /// <summary>
        /// Proceso para obtener los objetos temporales almacenados en la DB
        /// </summary>
        /// <param name="UserId">Primary key del usuario que grabo el archivo</param>
        /// <param name="FileType">Tipo de archivo que se registro en la tabla temporal</param>
        /// <returns></returns>
        IQueryable<TblTempFile> GetTempFiles(long UserId, byte FileType);

        /// <summary>
        /// Meotodo para obtener todos los prefijos de los NCF
        /// </summary>
        /// <returns></returns>
        IEnumerable<TblNcfprefix> GetAllPrefixNCF();

        /// <summary>
        /// Metodo para mover un archivo desde la carpeta temporar a su carpeta
        /// </summary>
        /// <param name="name"></param>
        /// <param name="typeFiles"></param>
        /// <returns></returns>
        Task<TaskResult> MoveFile(string name, QuickSalEnum.TypeFiles typeFiles);

        /// <summary>
        /// Proceso para encriptar un string
        /// </summary>
        /// <param name="cadena"></param>
        /// <returns></returns>
        string SHA1Encrypt(string cadena);

        /// <summary>
        /// Proceso para aplicar un encriptardo con llave
        /// </summary>
        /// <param name="texto">String que se encriptara</param>
        /// <returns></returns>
        string TripleDesEncrypt(string texto);

        /// <summary>
        /// Proceso para descriptar un string
        /// </summary>
        /// <param name="textoEncriptado">String encriptado</param>
        /// <returns></returns>
        string TripleDesDescrypt(string textoEncriptado);

        /// <summary>
        /// Metodo para generar nueva secuendia del codigo de una entidad
        /// </summary>
        /// <param name="type">Tipo de entidad</param>
        /// <param name="lastCode">Ultimo codigo generado</param>
        /// <returns></returns>
        string GenerateCode(EnumTypes.CodeEntities type, string lastCode = null);

        /// <summary>
        /// Metodo para obtener la descripcion de un enum
        /// </summary>
        /// <param name="value">Valor del enum</param>
        /// <returns></returns>
        string GetEnumDescription(Enum value);

        /// <summary>
        /// Metodo para generar una password aleatorio
        /// </summary>
        /// <returns></returns>
        string GeneratePassword();


        /// <summary>
        /// Metodo para almacenar una imagen temporal
        /// </summary>
        /// <param name="file">Información del archivo</param>
        /// <param name="fileInMemory">Stream que contiene la foto</param>
        /// <returns></returns>
        Task<TaskResult> AddTempFile(TblTempFile file, Stream fileInMemory);



        /// <summary>
        /// Metodo para obtener una url temporal del archivo en la carpeta temporal
        /// </summary>
        /// <param name="objectKey">Nombre del archivo</param>
        /// <returns></returns>
        Task<string> GetTempUrlObjet(string objectKey);


        /// <summary>
        /// Metodo para eliminar archivo temporales de la tabal temp files
        /// </summary>
        /// <param name="entity">Modelo con la informacion</param>
        /// <returns></returns>
        void DeleteRangeTempFile(IEnumerable<TblTempFile> entity);

        /// <summary>
        /// Metodo para eliminar un archivo temporal
        /// </summary>
        /// <param name="entity">Modelo con la informacion</param>
        /// <returns></returns>
        void DeleteTempFile(TblTempFile file);

        /// <summary>
        /// Metodo para convertir un Listado de objetos en un array de objetos
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model">Listado de objectos que se desean convertir</param>
        /// <param name="ColumnsName">Array con el nombre de las columnas que se desean obtener</param>
        /// <returns></returns>
        List<object[]> ConvertToArray<T>(IEnumerable<T> model, string[] ColumnsName);

        /// <summary>
        /// Metodo para validar si un string tiene alguna sentencia sql
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        bool validateSQLstatement(string value);

        /// <summary>
        /// Metodo para validar que un string no contenga caracteres especiales
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        bool validateSpecialcharacters(string value);


        /// <summary>
        /// Metodo para obtener el o los valores de un parametro
        /// </summary>
        /// <param name="nameSetting">Parametros</param>
        /// <param name="valueName">Nombre del valor</param>
        /// <returns></returns>
        Task<IQueryable<Setting>> GetSettingValue(string nameSetting, string valueName = null);


        /// <summary>
        /// Proceso para obtener todos un tipo de identificacion
        /// </summary>
        /// <param name="Id">Primary key del tipo de identificación</param>
        /// <returns></returns>
        Task<TblDocumentIdentification> GetDocumentIdentification(int Id);


        /// <summary>
        /// Metodo para obtener la descripcion de un enum
        /// </summary>
        /// <param name="tipoEnum"></param>
        /// <returns></returns>
        string GetListDesc(Enum tipoEnum);


        /// <summary>
        /// Proceso para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
