﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using QuickSal.DataAcces.CustomModels.Models;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Repositories;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.Services
{
    public class ServiceError : IServiceError
    {
        private IConfiguration _Configuration { get; set; }
        private IHostingEnvironment _env { get; set; }
        private ILogErrorRepository logErrorRepository;
        private IGlobalRepository globalRepository;
        TaskResult taskResult = new TaskResult();

        public ServiceError(IConfiguration configuration, IHostingEnvironment env, ILogErrorRepository logErrorRepository,
            IGlobalRepository globalRepository)
        {
            this._Configuration = configuration;
            this._env = env;
            this.logErrorRepository = logErrorRepository;
            this.globalRepository = globalRepository;
        }

        /// <summary>
        /// Proceso para registrar los errores del sistema si no se registra en la tabla se registra en un archivo txt
        /// </summary>
        /// <param name="Error"></param>
        /// <param name="MetodoEjecutado"></param>
        /// <param name="UbicacionCode"></param>
        /// <param name="UserError"></param>
        public void LogError(string Error, string MetodoEjecutado, string UbicacionCode, string WarningMessageUser = null)
        {
            try
            {
                var result = logErrorRepository.AddError(Error, MetodoEjecutado, UbicacionCode);


                string date = DateTime.Now.ToShortDateString().Replace("/", "-");
                string fichero = "Log_" + date + ".txt";

                string directoryPath = _env.WebRootPath + "/QuicksalLogs";
                string filePath = directoryPath + "/" + fichero;

                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);

                using (StreamWriter escritor = new StreamWriter(filePath, true))
                {
                    escritor.WriteLine("[" + DateTime.Now.ToString() + "]  Resultado de la operación:  [--" + Error + "--]" + "  Método: [" + MetodoEjecutado + "]" + "  Ubicacion: [" + UbicacionCode + "]");
                    escritor.Flush();
                    escritor.Close();
                    escritor.Dispose();
                }


                if (!string.IsNullOrEmpty(WarningMessageUser))
                    WarningMessage(WarningMessageUser);
            }
            catch (Exception ex)
            {
                taskResult.Exception = ex;
            }

        }

        /// <summary>
        /// Proceso para registrar una alerta
        /// </summary>
        /// <param name="Message"></param>
        /// <returns></returns>
        public TaskResult WarningMessage(string Message)
        {
            taskResult.AddErrorMessage(Message);
            return taskResult;
        }

        /// <summary>
        /// Proceso para obtener todas las alertas generadas
        /// </summary>
        /// <returns></returns>
        public TaskResult TaskStatus()
        {
            return taskResult;
        }
    }

    public interface IServiceError
    {

        /// <summary>
        /// Proceso para registrar los errores del sistema si no se registra en la tabla se registra en un archivo txt
        /// </summary>
        /// <param name="Error"></param>
        /// <param name="MetodoEjecutado"></param>
        /// <param name="UbicacionCode"></param>
        /// <param name="UserError"></param>
        void LogError(string Error, string MetodoEjecutado, string UbicacionCode, string WarningMessageUser = null);

        /// <summary>
        /// Proceso para registrar una alerta
        /// </summary>
        /// <param name="Message"></param>
        /// <returns></returns>
        TaskResult WarningMessage(string Message);

        /// <summary>
        /// Proceso para obtener todas las alertas generadas
        /// </summary>
        /// <returns></returns>
        TaskResult TaskStatus();
    }
}
