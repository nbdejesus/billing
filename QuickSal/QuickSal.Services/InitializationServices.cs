﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Repositories;
using QuickSal.Services.Services;

namespace QuickSal.Services
{
    public static class InitializationServices
    {
        public static void GetGlobal(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<QuickSalContext>(options => options.UseSqlServer(configuration.GetConnectionString("QuickSalConnection")));

            //Repositories
            services.AddScoped<IBusinessRepository, BusinessRepository>();
            services.AddScoped<IBranchOfficeRepository, BranchOfficeRepository>();
            services.AddScoped<ILogErrorRepository, LogErrorRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserConfirmationRepository, UserConfirmationRepository>();
            services.AddScoped<IGlobalRepository, GlobalRepository>();
            services.AddScoped<ISubscriptionsRepository, SubscriptionsRepository>();
            services.AddScoped<IPrivilegeRepository, PrivilegeRepository>();
            services.AddScoped<ISecurityRepository, SecurityRepository>();
            services.AddScoped<IEmployeeRepository, EmployeeRepository>();
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<ISupplierRepository, SupplierRepository>();
            services.AddScoped<IWarehouseRepository, WarehouseRepository>();
            services.AddScoped<IMeasureRepository, MeasureRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<ISubCategoryRepository, SubCategoryRepository>();
            services.AddScoped<ITaxRepository, TaxRepository>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<INcfRepository, NcfRepository>();
            services.AddScoped<INcfRowRepository, NcfRowRepository>();
            services.AddScoped<IMovingWareRepository, MovingWareRepository>();
            services.AddScoped<IMovingWareRowRepository, MovingWareRowRepository>();
            services.AddScoped<IProductPriceRepository, ProductPriceRepository>();
            services.AddScoped<IInvoiceRepository, InvoiceRepository>();
            services.AddScoped<IInvoiceRowRepository, InvoiceRowRepository>();
            services.AddScoped<IDebitNoteRepositoy, DebitNoteRepositoy>();
            services.AddScoped<IVendorPaymentRepository, VendorPaymentRepository>();
            services.AddScoped<ICreditNoteRepository, CreditNoteRepository>();
            services.AddScoped<ICreditNoteRowRepository, CreditNoteRowRepository>();
            services.AddScoped<IPurchaseOrderRepositoy, PurchaseOrderRepositoy>();
            services.AddScoped<ICreditNoteUsedRepository, CreditNoteUsedRepository>();
            services.AddScoped<ICustomerPaymentsRepository, CustomerPaymentsRepository>();
            services.AddScoped<IDashboardRepository, DashboardRepository>();
            services.AddScoped<IPurchaseOrderRepositoy, PurchaseOrderRepositoy>();
            services.AddScoped<IPlanRepository, PlanRepository>();
            services.AddScoped<ISubObjectRepository, SubObjectRepository>();
            services.AddScoped<IPlanSubObjectRepository, PlanSubObjectRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IOrderRowRepository, OrderRowRepository>();



            //Servicies
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IAccountingService, AccountingService>();
            services.AddScoped<IServiceError, ServiceError>();
            services.AddScoped<INotificationsService, ServiceNotifications>();
            services.AddScoped<IBusinessService, BusinessService>();
            services.AddScoped<IGlobalService, GlobalService>();
            services.AddScoped<IEmployeeService, EmployeeService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<ISupplierService, SupplierService>();
            services.AddScoped<ISecurityService, SecurityService>();
            services.AddScoped<IInventoryService, InventoryService>();
            services.AddScoped<IDashboardServices, DashboardServices>();
            services.AddScoped<IPlanService, PlanService>();
            services.AddScoped<IAmazonService, AmazonService>();
            services.AddScoped<IFileHandleService, FileHandleService>();
        }
    }
}
