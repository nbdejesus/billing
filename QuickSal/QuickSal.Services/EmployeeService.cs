﻿using Microsoft.EntityFrameworkCore;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Repositories;
using QuickSal.DataAcces.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static QuickSal.DataAcces.QuickSalEnum;

namespace QuickSal.Services
{
    public class EmployeeService : IEmployeeService
    {
        private IServiceError serviceError;
        private IEmployeeRepository employeeRepository;
        private IGlobalService globalService;
        private IAccountService accountingService;
        private ISecurityService securityService;
        private IAmazonService amazonService;

        public EmployeeService(IServiceError serviceError, IEmployeeRepository employeeRepository,
                               IGlobalService globalService, IAccountService accountingService,
                               IAmazonService amazonService, ISecurityService securityService)
        {
            this.serviceError = serviceError;
            this.employeeRepository = employeeRepository;
            this.globalService = globalService;
            this.accountingService = accountingService;
            this.securityService = securityService;
            this.amazonService = amazonService;
        }

        /// <summary>
        /// Metodo para obtener los empleados de una empresa
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        public IQueryable<TblEmployeeBusiness> GetEmployees(long BusinessId)
        {
            return employeeRepository.Get(x => x.BusinessId == BusinessId).Include(x => x.User).Include(x => x.Privileges);
        }


        /// <summary>
        /// Metodo para obtener las empresas al a que esta relacionado el empleado
        /// </summary>
        /// <param name="userId">Primary key del usuario</param>
        /// <returns></returns>
        public IEnumerable<TblBusiness> GetBusiness(long userId)
        {
            var query = employeeRepository.Get(x => x.UserId == userId && x.Estatus).Include(x => x.Business).ToList();

            var business = from c in query
                           select new TblBusiness
                           {
                               Name = c.Business.Name,
                               Id = c.Business.Id,
                               Estatus = c.Business.Estatus,
                               Description = c.Business.Description
                           };
            business.Where(b => b.Estatus == true).ToList();
            return business;
        }

        /// <summary>
        /// Metodo para obtener un empleado de una empresa
        /// </summary>
        /// <param name="id">Primary key el empleado</param>
        /// <param name="BusinessId">Primary key de la empres</param>
        /// <returns></returns>
        public TblEmployeeBusiness GetEmployee(long id, long BusinessId)
        {
            return employeeRepository.Get(x => x.Id == id && x.BusinessId == BusinessId).FirstOrDefault();
        }



        /// <summary>
        /// Metodo para obtener el primary key de un empleado de una empresa
        /// </summary>
        /// <param name="UserId">Primary key del usuario</param>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        public long GetEmployeeId(long UserId, long BusinessId)
        {
            var data = employeeRepository.Get(x => x.UserId == UserId && x.BusinessId == BusinessId).FirstOrDefault();
            if (data != null)
                return data.Id;
            else
                return 0;
        }


        /// <summary>
        /// Proceso para obtener los objetos permitidos para el empleado
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <param name="privilegeId">Primary key del privilegio</param>
        /// <returns></returns>
        public IQueryable<vwObjectsBusiness> GetObjects(long BusinessId, int privilegeId = 0)
        {
            return employeeRepository.GetObjectsEmployee(BusinessId, privilegeId);
        }


        /// <summary>
        /// Metodo para obtener un empleado de una empresa
        /// </summary>
        /// <param name="id">Primary key el empleado</param>
        /// <param name="BusinessId">Primary key de la empres</param>
        /// <returns></returns>
        public TblEmployeeBusiness GetEmployeeInBusiness(long userId, long BusinessId)
        {
            return employeeRepository.Get(x => x.UserId == userId && x.BusinessId == BusinessId).FirstOrDefault();
        }

        /// <summary>
        /// Proceso para obtener la informacion de un usuario que no es empleado
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public TblUsers GetUserNotEmployee(string email, long businessID)
        {
            return employeeRepository.GetUserNotEmployee(email, businessID);
        }

        /// <summary>
        /// Metodo para obtener el ultimo codigo generado para los empleados
        /// </summary>
        /// <param name="businessID">Primary key de la empresa</param>
        /// <returns></returns>
        public string GetLastEmployeeCode(long businessID)
        {
            var query = employeeRepository.Get(x => x.BusinessId == businessID);

            if (query.Any())
                return query.LastOrDefault().EmployeeCode;
            else
                return null;
        }


        /// <summary>
        /// Metodo para registrar un empleado
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion del empleado</param>    
        /// <returns></returns>
        public async Task<TaskResult> CreateEmployee(TblEmployeeBusiness entity)
        {
            var user = entity.User;
            var task = await EmployeeValidateCreate(entity, entity.User);

            if (task.ExecutedSuccesfully)
            {
                var session = securityService.GetSession();
                //Seteamos datos por defecto al empleado
                entity.BusinessId = session.BusinessId;
                entity.Estatus = true;
                entity.CreateDate = DateTime.Now;
                entity.EditDate = DateTime.Now;
                entity.IntoDate = ((entity.IntoDate == DateTime.MinValue) ? DateTime.Now : entity.IntoDate);
                entity.BusinessOwner = false;

                var existUser = accountingService.GetUser(user.Email);

                //si el usuario existe lo asociamos a la cuenta
                if (existUser == null)
                {
                    //Creamos el usuario en el sistema                   
                    user.CountryId = entity.CountryId;
                    user.ProvinceId = entity.ProvinceId;
                    user.DocumentTypeId = entity.DocumentTypeId;
                    user.Name = entity.Name;
                    user.LastName = entity.LastName;
                    user.Direction = entity.Direction;
                    user.DocumentNumber = entity.DocumentNumber;
                    user.Picture = entity.Picture;
                    user.CreateDate = DateTime.Now;
                    user.UpdateDate = DateTime.Now;
                    user.BirthDate = null;
                    user.CellPhone = entity.CellPhone;
                    user.Telephone = entity.Phone;
                    user.Estatus = true;
                    user.Gender = entity.Gender;
                    user.CivilStatus = entity.CivilStatus;
                    user.BirthDate = entity.BirthDate;
                    entity.User.UserPassword = globalService.GeneratePassword();

                    var tempPicture = globalService.GetTempFiles(session.UserId, (byte)TypeFiles.EmployeePhoto);

                    //Movemos la foto a su carpeta final
                    if (tempPicture != null && !string.IsNullOrEmpty(entity.Picture))
                    {
                        entity.Picture = tempPicture.LastOrDefault().FileName;
                        await globalService.MoveFile(entity.Picture, TypeFiles.EmployeePhoto);
                    }
                    user.Picture = entity.Picture;


                    //Removemos las fotos temporales que tenga el usuario
                    if (tempPicture != null && !string.IsNullOrEmpty(entity.Picture))
                        globalService.DeleteRangeTempFile(tempPicture);


                    user.TblEmployeeBusiness.Add(entity);
                    task = accountingService.CreateUser(user);
                }
                else
                {
                    entity.UserId = existUser.Id;
                    task = employeeRepository.AddEntity(entity);
                }
            }

            return task;
        }


        /// <summary>
        /// Metodo para actualizar un empleado
        /// </summary>
        /// <param name="entity">Modelo con los datos del empleado</param>
        /// <returns></returns>
        public async Task<TaskResult> EditEmployee(TblEmployeeBusiness entity)
        {
            var task = await EmployeeValidateUpdate(entity);

            if (task.ExecutedSuccesfully)
            {
                var employeeDb = employeeRepository.Get(entity.Id);

                employeeDb.CountryId = entity.CountryId;
                employeeDb.ProvinceId = entity.ProvinceId;
                employeeDb.DocumentTypeId = entity.DocumentTypeId;
                employeeDb.DocumentNumber = entity.DocumentNumber;
                employeeDb.Name = entity.Name;
                employeeDb.LastName = entity.LastName;
                employeeDb.PrivilegesId = entity.PrivilegesId;
                employeeDb.BranchOfficeId = entity.BranchOfficeId;
                employeeDb.OutDate = entity.OutDate;
                employeeDb.BirthDate = entity.BirthDate;
                employeeDb.Direction = entity.Direction;
                employeeDb.Phone = entity.Phone;
                employeeDb.CellPhone = entity.CellPhone;
                employeeDb.InstitutionalEmail = entity.InstitutionalEmail;
                employeeDb.Gender = entity.Gender;
                employeeDb.CivilStatus = entity.CivilStatus;
                employeeDb.Salary = entity.Salary;
                employeeDb.Position = entity.Position;
                employeeDb.EditDate = DateTime.Now;
                employeeDb.IntoDate = ((entity.IntoDate == DateTime.MinValue) ? DateTime.Now : entity.IntoDate);

                var session = securityService.GetSession();
                var tempPicture = globalService.GetTempFiles(session.UserId, (byte)TypeFiles.EmployeePhoto);

                //Movemos la foto a su carpeta final
                if (tempPicture.Count() > 0 && !string.IsNullOrEmpty(entity.Picture))
                {
                    entity.Picture = tempPicture.LastOrDefault().FileName;
                    await globalService.MoveFile(entity.Picture, TypeFiles.EmployeePhoto);
                    await amazonService.DeleteFile(employeeDb.Picture, TypeFiles.EmployeePhoto);//Eliminamos la foto de amazon web service  
                }
                employeeDb.Picture = entity.Picture;

                //Removemos las fotos temporales que tenga el usuario
                if (tempPicture.Count() > 0)
                    globalService.DeleteRangeTempFile(tempPicture);

                employeeRepository.Update(employeeDb);
            }

            return task;
        }

        /// <summary>
        /// Proceso para hacer las validaciones al empleado cuando se esta actualizado
        /// </summary>
        /// <param name="employee">Modelo que contiene del empleado</param>
        /// <returns></returns>
        private async Task<TaskResult> EmployeeValidateUpdate(TblEmployeeBusiness employee)
        {
            try
            {
                var session = securityService.GetSession();
                var documentType = await globalService.GetDocumentIdentification(employee.DocumentTypeId.Value);

                if (string.IsNullOrEmpty(employee.Name))
                    return serviceError.WarningMessage("El nombre es obligatorio.");
                else if (employee.PrivilegesId == 0)
                    return serviceError.WarningMessage("Falta el privilegio del empleado.");
                else if (employee.DocumentTypeId == 0)
                    return serviceError.WarningMessage("El tipo de documento es obligatorio.");
                else if (string.IsNullOrEmpty(employee.DocumentNumber))
                    return serviceError.WarningMessage("El número de documento es obligatorio.");
                else if (employee.DocumentNumber.Length < documentType.MaxlengthChar)
                    return serviceError.WarningMessage("El número de " + documentType.Name + " es demasiado corto, favor revisar el número ingresado");
                else if (employeeRepository.Get(x => x.BusinessId == session.BusinessId && x.Id != employee.Id && (x.DocumentTypeId == employee.DocumentTypeId &&
                         x.DocumentNumber == employee.DocumentNumber)).Any())
                    return serviceError.WarningMessage("El número de identificación ingresado ya se encuentra registrado con otro empleado.");
                else
                {
                    var userDB = accountingService.GetUser(employee.User.Email);
                    if (userDB != null)
                    {
                        var employeedb = employeeRepository.Get(x => x.Id == employee.Id).FirstOrDefault();
                        if (userDB.Id != employeedb.UserId)
                            return serviceError.WarningMessage("El correo de acceso se encuentra asociado a otro usuario.");
                    }
                }

                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EmployeeValidateUpdate", "EmployeeService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }


        /// <summary>
        /// Metodo para activar o desactivar un empleado
        /// </summary>
        /// <param name="status">Estaus para el empleado</param>
        /// <param name="Id">LLave primaria del empleado</param>
        /// <returns></returns>
        public TaskResult StatusEmployee(bool status, long Id, long businessId)
        {
            TaskResult task = new TaskResult();

            if (Id == 0)
                task = serviceError.WarningMessage("El primary key del empleado no puede ser 0.");
            else if (businessId == 0)
                task = serviceError.WarningMessage("El primary key de la empresa no puede ser 0.");
            else
            {
                var employeeDb = employeeRepository.Get(Id);
                employeeDb.Estatus = status;
                employeeRepository.Update(employeeDb);
                task.ExecutedSuccesfully = true;
            }

            return task;
        }



        /// <summary>
        /// Proceso para hacer las validaciones al usuario cuando se esta registrando
        /// </summary>
        /// <param name="employee">Modelo que contiene la informacion del empleado</param>
        /// <param name="user">Modelo que contiene la informacion del usuario</param>
        /// <returns></returns>
        private async Task<TaskResult> EmployeeValidateCreate(TblEmployeeBusiness employee, TblUsers user)
        {
            try
            {
                var session = securityService.GetSession();
                var documentType = await globalService.GetDocumentIdentification(employee.DocumentTypeId.Value);

                if (string.IsNullOrEmpty(employee.EmployeeCode))
                    return serviceError.WarningMessage("El código del empleado es obligatorio.");
                else if (string.IsNullOrEmpty(user.Email))
                    return serviceError.WarningMessage("Falta el correo del usuario para crear su cuenta.");
                else if (string.IsNullOrEmpty(employee.Name))
                    return serviceError.WarningMessage("El nombre es obligatorio.");
                else if (employee.PrivilegesId == 0)
                    return serviceError.WarningMessage("Falta el privilegio del empleado.");
                else if (employee.DocumentTypeId == 0)
                    return serviceError.WarningMessage("El tipo de documento es obligatorio.");
                else if (string.IsNullOrEmpty(employee.DocumentNumber))
                    return serviceError.WarningMessage("El número de documento es obligatorio.");
                else if (employee.DocumentNumber.Length < documentType.MaxlengthChar)
                    return serviceError.WarningMessage("El número de " + documentType.Name + " es demasiado corto, favor revisar el número ingresado");
                else if (employeeRepository.Get(x => x.BusinessId == session.BusinessId && x.DocumentTypeId == employee.DocumentTypeId &&
                            x.DocumentNumber == employee.DocumentNumber).Any())
                    return serviceError.WarningMessage("El número de identificación ingresado ya se encuentra registrado con otro empleado.");
                else if (accountingService.GetUser(user.Email) != null)
                    return serviceError.WarningMessage("El correo se encuentra asociado a otro usuario.");


                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EmployeeValidateCreate", "EmployeeService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }


        /// <summary>
        /// Proceso para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            var task = await employeeRepository.Savechanges();

            if (!task.ExecutedSuccesfully)
                serviceError.LogError(task.MessageList[0], "Savechanges", "EmployeeController", "Error al intentar guardar los datos.");

            return task;
        }

    }
    public interface IEmployeeService
    {
        /// <summary>
        /// Metodo para obtener los empleados de una empresa
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        IQueryable<TblEmployeeBusiness> GetEmployees(long BusinessId);

        /// <summary>
        /// Metodo para obtener un empleado de una empresa
        /// </summary>
        /// <param name="id">Primary key el empleado</param>
        /// <param name="BusinessId">Primary key de la empres</param>
        /// <returns></returns>
        TblEmployeeBusiness GetEmployee(long id, long BusinessId);

        /// <summary>
        /// Proceso para obtener la informacion de un usuario que no es empleado
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        TblUsers GetUserNotEmployee(string email, long businessID);

        /// <summary>
        /// Metodo para obtener el ultimo codigo generado para los empleados
        /// </summary>
        /// <param name="businessID">Primary key de la empresa</param>
        /// <returns></returns>
        string GetLastEmployeeCode(long businessID);


        /// <summary>
        /// Metodo para obtener un empleado de una empresa
        /// </summary>
        /// <param name="id">Primary key el empleado</param>
        /// <param name="BusinessId">Primary key de la empres</param>
        /// <returns></returns>
        TblEmployeeBusiness GetEmployeeInBusiness(long userId, long BusinessId);

        /// <summary>
        /// Metodo para obtener las empresas al a que esta relacionado el empleado
        /// </summary>
        /// <param name="userId">Primary key del usuario</param>
        /// <returns></returns>
        IEnumerable<TblBusiness> GetBusiness(long userId);

        /// <summary>
        /// Proceso para obtener los objetos permitidos para el empleado
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <param name="privilegeId">Primary key del privilegio</param>
        /// <returns></returns>
        IQueryable<vwObjectsBusiness> GetObjects(long BusinessId, int privilegeId = 0);


        /// <summary>
        /// Metodo para obtener el primary key de un empleado de una empresa
        /// </summary>
        /// <param name="UserId">Primary key del usuario</param>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        long GetEmployeeId(long UserId, long BusinessId);

        /// <summary>
        /// Metodo para registrar un empleado
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion del empleado</param>
        /// <returns></returns>
        Task<TaskResult> CreateEmployee(TblEmployeeBusiness entity);


        /// <summary>
        /// Metodo para activar o desactivar un empleado
        /// </summary>
        /// <param name="status">Estaus para el empleado</param>
        /// <param name="Id">LLave primaria del empleado</param>
        /// <returns></returns>
        TaskResult StatusEmployee(bool status, long Id, long businessId);

        /// <summary>
        /// Metodo para actualizar un empleado
        /// </summary>
        /// <param name="entity">Modelo con los datos del empleado</param>
        /// <returns></returns>
        Task<TaskResult> EditEmployee(TblEmployeeBusiness entity);

        /// <summary>
        /// Proceso para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
