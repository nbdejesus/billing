﻿using System;
using System.Collections.Generic;

namespace QuickSal.Services.Support
{
    public class Mail
    { 
        public  string To { get; set; }
        public  List<String> CC { get; set; }
        public  List<String> BCC { get; set; }
        public  string Subject { get; set; }
        public  string Body { get; set; }
        public  List<String> Attachments { get; set; }
        public  List<String> Files { get; set; }
        public  string ErrorDescription { get; set; }
        public  DateTime ErrorDate { get; set; }
    }
}
