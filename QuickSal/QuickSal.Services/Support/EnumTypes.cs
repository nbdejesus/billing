﻿using System.ComponentModel;

namespace QuickSal.Services.Support
{
    public class EnumTypes
    {
        public enum CodeEntities
        {
            Employee = 1,
            Customer = 2,
            Supplier = 3,
            Moving = 4,
            Product = 5,
            PurshOrder=6,
            DebitNote = 7,
            Invoice = 8,
            VendorPayment = 9,
            CustomerPayment=10,
            CreditNote = 11,
            Order=12
        }

        public enum Gender
        {
            [Description("Hombre")]
            Male = 1,
            [Description("Mujer")]
            Female = 2
        }

        public enum CivilStatus
        {
            [Description("Soltero")]
            Single = 1,
            [Description("Casado")]
            married = 2,
            [Description("Divorciado")]
            divorced = 3,
            [Description("Viudo")]
            widower = 4,
            [Description("Unión libre")]
            FreeUnion = 5
        }

        public enum AccesType
        {
            [Description("Todas las sucursales")]
            AllConect = 1,
            [Description("Fijo")]
            RestringConect = 2
        }

        public enum CreditType
        {
            [Description("Al contado")]
            Counted = 1,
            [Description("A crédito")]
            Credit = 2
        }

        public enum StatusCode
        {
            [Description("Sesión finalizada")]
            FinishedSession = 429,
            [Description("Acceso denegado")]
            AccessDenied = 401
        }

        public enum TypeReport
        {
            Excel = 1,
            PDF   = 2,
            EXCEL = 3
        }


    }
}
