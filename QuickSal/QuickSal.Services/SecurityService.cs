﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Views;
using QuickSal.DataAcces.Repositories;
using QuickSal.Services.Support;
using System;
using System.Linq;
using System.Collections;

namespace QuickSal.Services
{
    public class SecurityService : ISecurityService
    {
        private IServiceError serviceError;
        private ISecurityRepository securityRepository;        

        public SessionViewModel Session { get; set; }

        public SecurityService(IServiceError serviceError, ISecurityRepository securityRepository)
        {
            this.serviceError = serviceError;
            this.securityRepository = securityRepository;
        }

        /// <summary>
        /// Metodo para retornar la session
        /// </summary>
        /// <returns></returns>
        public SessionViewModel GetSession()
        {
            return Session;
        }

        /// <summary>
        /// Metodo para cargar la session en memoria
        /// </summary>
        /// <param name="session"></param>
        public void LoadSesion(SessionViewModel session)
        {
            try
            {
                Session = session;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "LoadSesion", "SecurityService");
                serviceError.WarningMessage("Error al realizar las validaciones");
            }
        }

        /// <summary>
        /// Metodo para obtener un sub objeto de quicksal
        /// </summary>
        /// <param name="method">Metodo del controller</param>
        /// <param name="controller">Controller</param>
        /// <returns></returns>
        public TblSubObject GetSubObject(string method, string controller)
        {
            return securityRepository.GetSubObject(method, controller);
        }

        /// <summary>
        /// Metodo para obtener un sub objeto de quicksal
        /// </summary>
        /// <param name="Id">Primary key del subObjeto</param>
        /// <returns></returns>
        public TblSubObject GetSubObject(int Id)
        {
            return securityRepository.GetSubObject(Id);
        }      

        /// <summary>
        /// Metodo para obtener la seguridad asignada al empleado
        /// </summary>
        /// <param name="privilegeId">Primary key del privilegio</param>
        /// <param name="method">Nombre del metodo</param>
        /// <param name="controller">Nombre del controller</param>
        /// <returns></returns>
        public vwEmployeeSecurity GetSecurity(int privilegeId, string method, string controller)
        {
            return securityRepository.GetSecurity(privilegeId, method, controller);
        }

        
        /// <summary>
        /// Metodo para obtener las agrupaciones de objetos
        /// </summary>
        /// <returns></returns>
        public IQueryable<TblObject> GetObject()
        {
            return securityRepository.GetObject();
        }
    }

    public interface ISecurityService
    {
        /// <summary>
        /// Metodo para retornar la session
        /// </summary>
        /// <returns></returns>
        SessionViewModel GetSession();

        /// <summary>
        /// Metodo para obtener un sub objeto de quicksal
        /// </summary>
        /// <param name="Id">Primary key del subObjeto</param>
        /// <returns></returns>
        TblSubObject GetSubObject(int Id);

        /// <summary>
        /// Metodo para obtener un sub objeto de quicksal
        /// </summary>
        /// <param name="method">Metodo del controller</param>
        /// <param name="controller">Controller</param>
        /// <returns></returns>
        TblSubObject GetSubObject(string method, string controller);

        /// <summary>
        /// Metodo para obtener la seguridad asignada al empleado
        /// </summary>
        /// <param name="privilegeId">Primary key del privilegio</param>
        /// <param name="method">Nombre del metodo</param>
        /// <param name="controller">Nombre del controller</param>
        /// <returns></returns>
        vwEmployeeSecurity GetSecurity(int privilegeId, string method, string controller);

        /// <summary>
        /// Metodo para obtener las agrupaciones de objetos
        /// </summary>
        /// <returns></returns>
        IQueryable<TblObject> GetObject();

        /// <summary>
        /// Metodo para cargar la session en memoria
        /// </summary>
        /// <param name="session"></param>
        void LoadSesion(SessionViewModel session);
    }
}
