﻿using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Procedure;
using QuickSal.DataAcces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static QuickSal.DataAcces.Repositories.DashboardRepository;

namespace QuickSal.Services
{
   public class DashboardServices : IDashboardServices
    {
        private IServiceError serviceError;
        private ISecurityService securityService;
        private IGlobalService globalService;
        private IDashboardRepository dashboardRepository;


        public DashboardServices(IDashboardRepository dashboardRepository,IServiceError serviceError, ISecurityService securityService, IGlobalService globalService)
        {          
            this.serviceError = serviceError;
            this.securityService = securityService;
            this.globalService = globalService;
            this.dashboardRepository = dashboardRepository;
        }
        public IQueryable<prc_Shopping> GetShopping(long businessId)
        {
            return dashboardRepository.GetShopping(businessId);
        }
        public IQueryable<prc_Sales> GetSSales(long businessId)
        {
            return dashboardRepository.GetSSales(businessId);
        }
        public IQueryable<prc_Profits> GetProfits(long businessId, int month, int year)
        {
            return dashboardRepository.GetProfits(businessId, month, year);
        }
        public IQueryable<prc_expenses> GetExpenses(long businessId, int month, int year)
        {
            return dashboardRepository.GetExpense(businessId, month, year);
        }
        public IQueryable<sp_Sales_for_Month> Getsp_Sales_for_Month(long businessId, int year)
        {
            return dashboardRepository.Getsp_Sales_for_Month(businessId, year);
        }
        public IQueryable<sp_Sales_for_Month_products> Getsp_Sales_for_Month_products(long businessId, int year,int month)
        {
            return dashboardRepository.Getsp_Sales_for_Month_products(businessId, year, month);
        }
        public IQueryable<prc_sp_Sales_for_Month_seller> Getsp_Sales_for_Month_seller(long businessId, int year, int month)
        {
            return dashboardRepository.Getsp_Sales_for_Month_seller(businessId, year, month);
        }
        public IQueryable<prc_Sales_for_Month_branchOffice> GetSales_for_Month_branchOffice(long businessId, int year, int month)
        {
            return dashboardRepository.GetSales_for_Month_branchOffice(businessId, year, month);
        }
        public IQueryable<pr_sp_Sales_for_Month_Customer> GetSales_for_Month_Customer(long businessId, int year, int month)
        {
            return dashboardRepository.GetSales_for_Month_Customer(businessId, year, month);
        }
    }

    public interface IDashboardServices
    {
        IQueryable<prc_Shopping> GetShopping(long businessId);

        IQueryable<prc_Sales> GetSSales(long businessId);

        IQueryable<prc_Profits> GetProfits(long businessId, int month, int year);

        IQueryable<prc_expenses> GetExpenses(long businessId, int month, int year);

        IQueryable<sp_Sales_for_Month> Getsp_Sales_for_Month(long businessId, int year);

        IQueryable<sp_Sales_for_Month_products> Getsp_Sales_for_Month_products(long businessId, int year, int month);

        IQueryable<prc_sp_Sales_for_Month_seller> Getsp_Sales_for_Month_seller(long businessId, int year, int month);
        IQueryable<prc_Sales_for_Month_branchOffice> GetSales_for_Month_branchOffice(long businessId, int year, int month);
        IQueryable<pr_sp_Sales_for_Month_Customer> GetSales_for_Month_Customer(long businessId, int year, int month);
    }
}
