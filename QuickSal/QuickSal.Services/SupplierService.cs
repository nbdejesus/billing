﻿using Microsoft.EntityFrameworkCore;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using QuickSal.DataAcces.Procedure;
using QuickSal.DataAcces;
using static QuickSal.DataAcces.QuickSalEnum;
using System.Threading.Tasks;
using ClosedXML.Excel;
using System.IO;

namespace QuickSal.Services
{
    public class SupplierService : ISupplierService
    {
        private ISupplierRepository supplierRepository;
        private IServiceError serviceError;
        private ISecurityService securityService;
        private IGlobalService globalService;
        private IPurchaseOrderRepositoy purchaseOrderRepositoy;
        private IVendorPaymentRepository vendorPaymentRepository;
        private IDebitNoteRepositoy debitNoteRepositoy;
        private ICustomerPaymentsRepository customerPaymentsRepository;
        private IInvoiceRepository invoiceRepository;
        private IAmazonService amazonService;

        public SupplierService(ISupplierRepository supplierRepository, IServiceError serviceError, ISecurityService securityService,
                                ICustomerPaymentsRepository customerPaymentsRepository, IInvoiceRepository invoiceRepository,
                                IGlobalService globalService, IPurchaseOrderRepositoy purchaseOrderRepositoy, IAmazonService amazonService,
                                IVendorPaymentRepository vendorPaymentRepository, IDebitNoteRepositoy DebitNoteRepositoy)
        {
            this.supplierRepository = supplierRepository;
            this.serviceError = serviceError;
            this.securityService = securityService;
            this.globalService = globalService;
            this.purchaseOrderRepositoy = purchaseOrderRepositoy;
            this.vendorPaymentRepository = vendorPaymentRepository;
            this.debitNoteRepositoy = DebitNoteRepositoy;
            this.customerPaymentsRepository = customerPaymentsRepository;
            this.invoiceRepository = invoiceRepository;
            this.amazonService = amazonService;
        }

        #region Purchase

        /// <summary>
        /// Metodo para obtener los NFC registrados de la empresa
        /// </summary>
        /// <param name="branchOfficeId">Primary key de la empresa</param>
        /// <returns></returns>
        /// 
        public IQueryable<PurchaseOrders> PurchaseOrders(long branchOfficeId)
        {
            return purchaseOrderRepositoy.Purchase(branchOfficeId);
        }


        //el detalle que tienes que devolver dependiendo el Id de  la cabezera
        public List<PurchaseOrders> GetPurchOrderRow(long Id)
        {
            return purchaseOrderRepositoy.GetPurchOrderRow(Id);
        }

        public IQueryable<DebitNote> ListDebitNoteState(long branchOfficeId, long suplierId)
        {
            return debitNoteRepositoy.ListDebitNoteStatus(branchOfficeId, suplierId);
        }

        public TblPurchaseOrder GetPurchase(long Id, long branchOfficeId)
        {
            return purchaseOrderRepositoy.GetPurchaseOrder(Id, branchOfficeId);
        }

        /// <summary>
        /// Metodo para obtener el detalle de la orden de compras
        /// </summary>
        /// <param name="PurchaseOrderId">Primary key de la orden de compras</param>
        /// <returns></returns>
        public IQueryable<TblPurchaseOrderRow> GetPurchaseOrderRow(long PurchaseOrderId)
        {
            return purchaseOrderRepositoy.GetPurchaseOrderRow(PurchaseOrderId);
        }

        public async Task<TaskResult> SetPurchaseOrdertatus(int Id, long branchOfficeId, long DocumentStatusID)
        {
            TaskResult task = new TaskResult();

            if (Id == 0)
            {
                task.ExecutedSuccesfully = false;
                serviceError.LogError("Falta el ID del Ncf", "SetPurchaseOrdertatus", "SupplierService", "Los datos no se han podido actualizar correctamente.");
            }

            if (task.ExecutedSuccesfully)
            {
                var session = securityService.GetSession();
                var purchase = GetPurchase(Id, branchOfficeId);
                var debitnote = debitNoteRepositoy.DebitNotePurchaseOder(purchase.Id);

                decimal total = 0;
                if (purchase.DocumentEstatusId == (int)QuickSalEnum.DocumentEstatus.Aprobada && DocumentStatusID == (int)QuickSalEnum.DocumentEstatus.Anulada)
                {
                    foreach (var item in debitnote)
                    {
                        item.Used = false;
                        total += item.Total;
                        item.UpdateDate = DateTime.Now;
                        item.EmployeeModifyId = session.EmployeeId;
                        debitNoteRepositoy.Update(item);
                    }

                    purchase.Total = (purchase.Total - total);
                }

                purchase.DocumentEstatusId = Convert.ToByte(DocumentStatusID);
                if (purchase.DocumentEstatusId == (int)QuickSalEnum.DocumentEstatus.Aprobada) { }
                {
                    foreach (var note in debitnote)
                    {
                        note.Used = true;
                        total += note.Total;
                        note.UpdateDate = DateTime.Now;
                        note.EmployeeModifyId = session.EmployeeId;
                        debitNoteRepositoy.Update(note);
                    }
                    purchase.Total = (purchase.Total - total);
                    purchase.Balance = (purchase.Balance - total);

                    MovimientoPurchase(Id);
                }
                purchaseOrderRepositoy.Update(purchase);

                await Savechanges();

            }
            return task;
        }

        /// <summary>
        /// Metodo para actualizar la nota de debito cuano es utilizada
        /// </summary>
        /// <param name="debit">listado de las notas de debitos utilizadas</param>
        /// <returns></returns>
        public TaskResult SetDebitNoteStatusUsed(ICollection<TblDebitNote> debit, TblPurchaseOrder purchaseOrder)
        {
            TaskResult task = new TaskResult();
            try
            {
                var session = securityService.GetSession();
                //var purchaseOrder = purchaseOrderRepositoy.Get(x => x.Id == purchaseOrderId).FirstOrDefault();
                decimal total = 0;
                foreach (var item in debit)
                {
                    //TblDebitNoteUsed tblDebitNoteUsed = new TblDebitNoteUsed();
                    //tblDebitNoteUsed.DebitNoteId = item.Id;
                    //tblDebitNoteUsed.PurchaseOrderId = purchaseOrder.Id;
                    // purchaseOrderRepositoy.DebitNoteUsed(tblDebitNoteUsed);
                    var debitNote = debitNoteRepositoy.Get(item.Id);
                    debitNote.PurchaseOrderId = purchaseOrder.Id;
                    debitNote.Ncfmodified = purchaseOrder.Ncf;
                    debitNote.UpdateDate = DateTime.Now;
                    debitNote.EmployeeModifyId = session.EmployeeId;
                    debitNoteRepositoy.Update(debitNote);

                    total += debitNote.Total;
                    if (purchaseOrder.DocumentEstatusId == (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada)
                    {
                        debitNote.Used = true;
                    }
                }

                purchaseOrder.Balance = (purchaseOrder.Balance - total);
                purchaseOrder.Total = (purchaseOrder.Total - total);
                purchaseOrderRepositoy.Update(purchaseOrder);

            }
            catch (Exception)
            {

                throw;
            }
            return task;
        }

        /// <summary>
        /// Metodo para obtener los NFC registrados de la empresa
        /// </summary>
        /// <param name="branchOfficeId">Primary key de la empresa</param>
        /// <returns></returns>
        public IQueryable<PurchaseOrders> PurchaseOrdersId(long branchOfficeId, long Id)
        {
            return purchaseOrderRepositoy.PurchaseId(branchOfficeId, Id);
        }

        /// Metodo para obtener el ultimo codigo generado para los clientes
        /// </summary>
        /// <param name="branchOfficeId">Primary key de la empresa</param>
        /// <returns></returns>
        public string GetLastPuschaseCode(long branchOfficeId)
        {
            var query = purchaseOrderRepositoy.Get(x => x.BranchOfficeId == branchOfficeId);

            if (query.Any())
                return query.LastOrDefault().DocumentNumber;
            else
                return null;
        }

        /// <summary>
        /// Metodo para agregar un impuesto al sistema
        /// </summary>
        /// <param name="entity">entidad con la informacion</param>
        /// <returns></returns>
        public async Task<TaskResult> CreatePurchase(TblPurchaseOrder entity, ICollection<TblDebitNote> debitNote)
        {
            try
            {
                var task = PurchaseValidateCreate(entity);

                if (task.ExecutedSuccesfully)
                {
                    var sesion = securityService.GetSession();

                    entity.BranchOfficeId = sesion.BranchOfficeId;
                    entity.UserId = sesion.UserId;
                    entity.EmployeeBusinessId = sesion.EmployeeId;
                    entity.CreateDate = DateTime.Now;
                    entity.UpdateDate = DateTime.Now;
                    entity.EmployeeCreateId = sesion.EmployeeId;
                    entity.EmployeeModifyId = sesion.EmployeeId;

                    /*Procesamos el balance pendiente*/
                    task = ProcessPendingBalance(entity);

                    purchaseOrderRepositoy.AddEntity(entity);

                    task = await Savechanges();

                    if (task.ExecutedSuccesfully)
                    {
                        if (debitNote != null)
                        {
                            SetDebitNoteStatusUsed(debitNote, entity);
                        }

                        if (entity.DocumentEstatusId == (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada)
                        {
                            MovimientoPurchase(entity.Id);
                        }
                        await Savechanges();
                    }
                }

                return task;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreatePurchaseOrder", "supplierService");
                serviceError.WarningMessage("Error al registrar el impuesto");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para hacer las validaciones al impuesto cuando se esta registrando
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion del impuesto</param>
        /// <returns></returns>
        private TaskResult PurchaseValidateCreate(TblPurchaseOrder entity)
        {
            try
            {
                var Ncf = purchaseOrderRepositoy.ValidarNcf(entity.Ncf).ToList();

                if (Ncf.Count() > 0)
                    return serviceError.WarningMessage("Ya existe Número Comprobante Fiscal insertado");


                if (entity.SuppliersId == 0)
                    return serviceError.WarningMessage("Es necesario el proveedor de la empresa");
                else if (string.IsNullOrEmpty(entity.DocumentNumber))
                    return serviceError.WarningMessage("El número de documento es obligatorio.");
                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "TaskValidateCreate", "AccountingService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para agregar un impuesto al sistema
        /// </summary>
        /// <param name="entity">entidad con la informacion</param>
        /// <returns></returns>
        public async Task<TaskResult> UpdatePurchase(TblPurchaseOrder entity)
        {
            try
            {
                var sesion = securityService.GetSession();
                entity.EmployeeBusinessId = sesion.EmployeeId;
                var task = PurchaseValidateUpdate(entity);
                var purchaseHeader = GetPurchase(entity.Id, sesion.BranchOfficeId);

                if (task.ExecutedSuccesfully)
                {
                    purchaseHeader.BranchOfficeId = sesion.BranchOfficeId;
                    purchaseHeader.Id = entity.Id;
                    purchaseHeader.SuppliersId = entity.SuppliersId;
                    purchaseHeader.UserId = sesion.UserId;
                    purchaseHeader.Ncf = entity.Ncf;
                    purchaseHeader.SubTotal = entity.SubTotal;
                    purchaseHeader.Discount = entity.Discount;
                    purchaseHeader.TotalTax = entity.TotalTax;
                    purchaseHeader.Total = entity.Total;
                    purchaseHeader.Balance = entity.Total;
                    purchaseHeader.CreateDate = DateTime.Now;
                    purchaseHeader.UpdateDate = DateTime.Now;
                    purchaseHeader.DocumentNumber = entity.DocumentNumber;
                    purchaseHeader.DocumentDate = entity.DocumentDate;
                    purchaseHeader.Observation = entity.Observation;
                    purchaseHeader.EmployeeCreateId = sesion.EmployeeId;
                    purchaseHeader.EmployeeModifyId = sesion.EmployeeId;

                    var PurchaseRows = entity.TblPurchaseOrderRow;


                    #region PurchaseRows Details

                    // esa consulta me busca Las filas que fueron agregadas 
                    var NewRows = (from p in PurchaseRows
                                   where !(from b in purchaseHeader.TblPurchaseOrderRow //TblInvoiceRow estan las filas que estan sin modificar en la base de datos
                                           select b.Id).Contains(p.Id)
                                   select p).Distinct().ToList();

                    var DeleteRows = (from p in purchaseHeader.TblPurchaseOrderRow//--Esta parte no puede ir en el servicio, si va en el servicio porque es una consulta en memoria, no es una consulta a base de datos
                                      where !(from b in PurchaseRows
                                              select b.Id).Contains(p.Id)
                                      select p).Distinct().ToList();

                    //Eliminamos las filas si existen
                    if (DeleteRows.Count > 0)
                        purchaseOrderRepositoy.DeleteRange(DeleteRows);

                    //Agregamos nuevos filas si existen nuevos registros
                    if (NewRows.Count > 0)
                        foreach (var item in NewRows)
                            purchaseHeader.TblPurchaseOrderRow.Add(item);


                    //aqui estamos Actualizamos filas si existen modificaciones
                    foreach (var PurchaRow in purchaseHeader.TblPurchaseOrderRow)
                    {
                        var Row = PurchaseRows.Where(x => x.Id != 0 && x.Id == PurchaRow.Id).FirstOrDefault();

                        if (Row != null)//validamos si la fila fue borrada
                        {
                            PurchaRow.Cost = Row.Cost;
                            PurchaRow.Tax = Row.Tax;
                            PurchaRow.Total = Row.Total;
                            PurchaRow.WarehouseId = Row.WarehouseId;
                            PurchaRow.ProductId = Row.ProductId;
                            PurchaRow.MeasureId = Row.MeasureId;
                            PurchaRow.Quantity = Row.Quantity;
                            PurchaRow.AmountTax = Row.AmountTax;
                            PurchaRow.Discount = Row.Discount;
                            PurchaRow.AmountDiscount = Row.AmountDiscount;
                            PurchaRow.TaxAbbreviation = Row.TaxAbbreviation;

                        }
                    }

                    #endregion

                    #region DebitNoteUsed (Balance)
                    // task = ProcessPendingBalance(entity);
                    var debitnote = debitNoteRepositoy.DebitNotePurchaseOder(purchaseHeader.Id);

                    if (debitnote != null)
                    {
                        SetDebitNoteStatusUsed(debitnote, purchaseHeader);
                    }

                    #endregion

                    purchaseOrderRepositoy.Update(purchaseHeader);

                    if (purchaseHeader.DocumentEstatusId == (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada)
                        MovimientoPurchase(purchaseHeader.Id);


                    await Savechanges();
                }

                return task;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "_EditPurchaseOrder", "supplierService");
                serviceError.WarningMessage("Error al registrar el impuesto");
                return serviceError.TaskStatus();
            }
        }

        /// <summary>
        /// Metodo para hacer las validaciones al impuesto cuando se esta registrando
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion del impuesto</param>
        /// <returns></returns>
        private TaskResult PurchaseValidateUpdate(TblPurchaseOrder entity)
        {
            try
            {
                var sesion = securityService.GetSession();
                var Ncf = purchaseOrderRepositoy.ValidarNcfUpdate(entity.Ncf, sesion.BranchOfficeId, entity.Id).ToList();

                if (Ncf.Any())
                    return serviceError.WarningMessage("Ya existe Número Comprobante Fiscal insertado");

                if (entity.SuppliersId == 0)
                    return serviceError.WarningMessage("Es necesario el proveedor de la empresa");
                else if (string.IsNullOrEmpty(entity.DocumentNumber))
                    return serviceError.WarningMessage("El número de documento es obligatorio.");
                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "TaskValidateCreate", "AccountingService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }

        public bool MovimientoPurchase(long PurchaseOrderId)
        {
            return purchaseOrderRepositoy.MovimientoPurchase(PurchaseOrderId);
        }

        /// <summary>
        /// Metodo para procesar el balance de una factura de compras
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private TaskResult ProcessPendingBalance(TblPurchaseOrder entity)
        {
            //Retiramos o agregamos los balances deacuerdo a lo que hizo el usuario
            var purchaseHeader = purchaseOrderRepositoy.Get(x => x.Id == entity.Id).Include(x => x.TblDebitNote).FirstOrDefault();

            if ((entity.TblDebitNote != null && entity.TblDebitNote.Count > 0) || (purchaseHeader != null && purchaseHeader.TblDebitNote.Count > 0))
            {
                List<long> debitsNoteId = entity.TblDebitNote.Select(x => x.Id).ToList();
                decimal TotalPendingBalance = debitNoteRepositoy.GetPendingBalance(debitsNoteId).Sum(x => x.Value);

                if (TotalPendingBalance > entity.Total)
                    return serviceError.WarningMessage("El crédito pendiente no puede ser mayor a total de la factura de compras.");

                if (purchaseHeader != null)
                {
                    purchaseHeader.Balance = entity.Total - TotalPendingBalance;
                    if (purchaseHeader.TblDebitNote.Count > 0)
                    {

                        var DebitNote = purchaseHeader.TblDebitNote;

                        // esa consulta me busca Las filas que fueron agregadas 
                        var NewBalance = (from p in entity.TblDebitNote
                                          where !(from b in DebitNote
                                                  select b.Id).Contains(p.Id)
                                          select p).Distinct().ToList();

                        var DeleteBalance = (from p in DebitNote
                                             where !(from b in entity.TblDebitNote
                                                     select b.Id).Contains(p.Id)
                                             select p).Distinct().ToList();

                        //Eliminamos las filas si existen
                        //if (DeleteBalance.Count > 0)
                        //debitNoteRepository.DeleteRange(DeleteBalance);

                        //Agregamos nuevos filas si existen nuevos registros
                        if (NewBalance.Count > 0)
                            foreach (var item in NewBalance)
                                purchaseHeader.TblDebitNote.Add(item);
                    }
                    else
                    {
                        //Si no existian balances lo agregamos
                        if (entity.TblDebitNote.Count > 0)
                            foreach (var item in entity.TblDebitNote)
                                purchaseHeader.TblDebitNote.Add(item);
                    }

                }
                else
                    entity.Balance = entity.Total - TotalPendingBalance;



                //Actualizamos la nota de debito para establecer que fue usada en un balance
                if (entity.DocumentEstatusId == (int)QuickSalEnum.DocumentEstatus.Aprobada)
                {
                    var ListDebitNote = debitNoteRepositoy.Get(x => entity.TblDebitNote.Select(d => d.Id).Contains(x.Id));

                    foreach (var debitNote in ListDebitNote)
                        debitNote.Used = true;

                    debitNoteRepositoy.UpdateRange(ListDebitNote);
                }

            }
            else
                entity.Balance = entity.Total;


            return serviceError.TaskStatus();
        }

        #endregion

        #region suplier
        /// <summary>
        /// Metodo para obtener los proveedores registrados para una empresa
        /// </summary>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        public IQueryable<TblSuppliers> GetSuppliers(long businessId)
        {
            return supplierRepository.Get(x => x.BusinessId == businessId).Include(x => x.DocumentType);
        }


        /// <summary>
        /// Metodo para obtener un proveedor
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="businessId"></param>
        /// <returns></returns>
        public TblSuppliers GetSupplier(long Id, long businessId)
        {
            return supplierRepository.Get(x => x.Id == Id && x.BusinessId == businessId).FirstOrDefault();
        }

        /// <summary>
        /// Metodo para obtener el ultimo código generado para los proveedores
        /// </summary>
        /// <param name="businessID">Primary key de la empresa</param>
        /// <returns></returns>
        public string GetLastSupplierCode(long businessID)
        {
            var query = supplierRepository.Get(x => x.BusinessId == businessID);

            if (query.Any())
                return query.LastOrDefault().Code;
            else
                return null;
        }


        /// <summary>
        /// Metodo para registrar un proveedor
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion del cliente</param>
        /// <returns></returns>
        public async Task<TaskResult> CreateSupplier(TblSuppliers entity)
        {
            //Pasamos los valores de la session
            var session = securityService.GetSession();
            entity.BusinessId = session.BusinessId;

         
                var task = await SupplierValidateCreate(entity);

                if (task.ExecutedSuccesfully)
                {
                    var tempPicture = globalService.GetTempFiles(session.UserId, (byte)TypeFiles.SupplierPhoto);

                    //Movemos la foto a su carpeta final
                    if (tempPicture != null && !string.IsNullOrEmpty(entity.Picture))
                    {
                        entity.Picture = tempPicture.LastOrDefault().FileName;
                        await globalService.MoveFile(entity.Picture, TypeFiles.SupplierPhoto);
                    }

                    //Removemos las fotos temporales que tenga el cliente
                    if (tempPicture != null && !string.IsNullOrEmpty(entity.Picture))
                        globalService.DeleteRangeTempFile(tempPicture);


                    //Seteamos datos por defecto del proveedor
                    entity.Estatus = true;
                    entity.CreationDate = DateTime.Now;
                    entity.UpdateDate = DateTime.Now;

                    supplierRepository.AddEntity(entity);
                }
           

            return task;
        }


        /// <summary>
        /// Metodo para registrar un proveedor
        /// </summary>
        /// <param name="correo">Parametro que posee el correo del suplidor</param>
        /// <returns></returns>
        public bool validarCorreo(string correo)
        {

            var task = false;
            var email = supplierRepository.Get().Where(x => x.Email == correo).FirstOrDefault();

            if (email != null)
                task = true;
            return task;
        }

        /// <summary>
        /// Metodo para hacer las validaciones al proveedor cuando se está registrando
        /// </summary>
        /// <param name="supplier">Modelo que contiene la informacion del proveedor</param>
        /// <returns></returns>
        private async Task<TaskResult> SupplierValidateCreate(TblSuppliers supplier)
        {
            try
            {
                var session = securityService.GetSession();
                supplier.BusinessId = session.BusinessId;
                var documentType = await globalService.GetDocumentIdentification(supplier.DocumentTypeId);

                if (string.IsNullOrEmpty(supplier.Code))
                    return serviceError.WarningMessage("El código del proveedor es obligatorio.");
                else if (string.IsNullOrEmpty(supplier.Names))
                    return serviceError.WarningMessage("Falta el nombre del proveedor.");
                else if (supplier.DocumentTypeId == 0)
                    return serviceError.WarningMessage("El tipo de documento es obligatorio.");
                else if (supplier.BusinessId == 0)
                    return serviceError.WarningMessage("Es necesario el ID de la empresa");
                else if (string.IsNullOrEmpty(supplier.DocumentNumber))
                    return serviceError.WarningMessage("El número de documento es obligatorio.");
                else if (supplier.DocumentNumber.Length < documentType.MaxlengthChar)
                    return serviceError.WarningMessage("El número de " + documentType.Name + " es demasiado corto, favor revisar el número ingresado");
                else if (supplier.DocumentNumber != null)
                {
                    if (supplierRepository.ValidateDocument(supplier.DocumentTypeId, supplier.DocumentNumber, supplier.BusinessId, supplier.Id))
                        return serviceError.WarningMessage("El número de documento exíste, favor de revisar.");
                }
                else if (supplier.Code != null)
                {
                    bool exist = supplierRepository.ValidationCodeSuplier(supplier.Code, supplier.BusinessId);
                    if (exist)
                        return serviceError.WarningMessage("El código del proveedor exíste, favor de revisar.");

                }


                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "SupplierValidateCreate", "SupplierService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }


        /// <summary>
        /// Metodo para actualizar un proveedor
        /// </summary>
        /// <param name="entity">Modelo con los datos del empleado</param>
        /// <returns></returns>
        public async Task<TaskResult> EditSupplier(TblSuppliers entity)
        {
            var session = securityService.GetSession();
            entity.BusinessId = session.BusinessId;

            var task = await SupplierValidateUpdate(entity);
            if (task.ExecutedSuccesfully)
            {
                var supplier = supplierRepository.Get(entity.Id);

                supplier.DocumentTypeId = entity.DocumentTypeId;
                supplier.DocumentNumber = entity.DocumentNumber;
                supplier.Names = entity.Names;
                supplier.PhoneNumber = entity.PhoneNumber;
                supplier.Movil = entity.Movil;
                supplier.AddressSuppliers = entity.AddressSuppliers;
                supplier.CountryId = entity.CountryId;
                supplier.ProvinceId = entity.ProvinceId;
                supplier.Email = entity.Email;
                supplier.UpdateDate = DateTime.Now;
                supplier.Obervation = entity.Obervation;
                supplier.PendingBalance = entity.PendingBalance;
                supplier.Ncfid = entity.Ncfid;
                supplier.CreditTypeId = entity.CreditTypeId;
                supplier.ContactPerson = entity.ContactPerson;


                var tempPicture = globalService.GetTempFiles(session.UserId, (byte)TypeFiles.SupplierPhoto);

                //Movemos la foto a su carpeta final
                if (tempPicture.Count() > 0 && !string.IsNullOrEmpty(entity.Picture))
                {
                    entity.Picture = tempPicture.LastOrDefault().FileName;
                    await globalService.MoveFile(entity.Picture, TypeFiles.SupplierPhoto);
                    await amazonService.DeleteFile(supplier.Picture, TypeFiles.SupplierPhoto);//Eliminamos la foto de amazon web service   
                }

                supplier.Picture = entity.Picture;

                //Removemos las fotos temporales que tenga el usuario
                if (tempPicture.Count() > 0)
                    globalService.DeleteRangeTempFile(tempPicture);

                supplierRepository.Update(supplier);
            }

            return task;
        }

        /// <summary>
        /// Metodo para hacer las validaciones al cliente cuando se esta actualizado
        /// </summary>
        /// <param name="customer">Modelo que contiene el cliente</param>
        /// <returns></returns>
        private async Task<TaskResult> SupplierValidateUpdate(TblSuppliers supplier)
        {
            try
            {
                var documentType = await globalService.GetDocumentIdentification(supplier.DocumentTypeId);

                if (string.IsNullOrEmpty(supplier.Names))
                    return serviceError.WarningMessage("Falta el nombre del proveedor.");
                else if (supplier.DocumentTypeId == 0)
                    return serviceError.WarningMessage("El tipo de documento es obligatorio.");
                else if (supplier.Id == 0)
                    return serviceError.WarningMessage("El primary key del proveedor es obligatorio.");
                else if (supplier.BusinessId == 0)
                    return serviceError.WarningMessage("Es necesario el ID de la empresa");
                else if (string.IsNullOrEmpty(supplier.DocumentNumber))
                    return serviceError.WarningMessage("El número de documento es obligatorio.");
                else if (supplier.DocumentNumber.Length < documentType.MaxlengthChar)
                    return serviceError.WarningMessage("El número de " + documentType.Name + " es demasiado corto, favor revisar el número ingresado");
                else if (supplier.DocumentNumber != null)
                {
                    if (supplierRepository.ValidateDocument(supplier.DocumentTypeId, supplier.DocumentNumber, supplier.BusinessId, supplier.Id))
                        return serviceError.WarningMessage("El número de documento exíste, favor de revisar.");

                }

                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CustomerValidateUpdate", "CustomerService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }


        /// <summary>
        /// Metodo para activar o desactivar un proveedor
        /// </summary>
        /// <param name="status">Estaus para el proveedor</param>
        /// <param name="Id">LLave primaria del proveedor</param>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        public TaskResult StatusSupplier(bool status, long Id, long businessId)
        {
            TaskResult task = new TaskResult();

            if (Id == 0)
                task = serviceError.WarningMessage("El primary key del proveedor no puede ser 0.");
            else if (businessId == 0)
                task = serviceError.WarningMessage("El primary key de la empresa no puede ser 0.");
            else
            {
                var SupplierDb = supplierRepository.Get(Id);
                SupplierDb.Estatus = status;

                supplierRepository.Update(SupplierDb);
                task.ExecutedSuccesfully = true;
            }

            return task;
        }
        #endregion

        #region vendorPayment
        /// <summary>
        /// Metodo para obtener todas las categorias del sistema
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        public IQueryable<TblVendorPayment> GetVendorPayment(long BusinessId)
        {
            return vendorPaymentRepository.GetVendorPayment(BusinessId);
        }

        /// Metodo que trae el listado de compras
        /// </summary>
        /// <returns></returns>
        public IQueryable<TblVendorPaymentRow> PurchaseRow(long businessID, IEnumerable<long> datos)
        {
            return vendorPaymentRepository.PurchaseRow(businessID, datos);
        }

        public List<VendorPayment> GetVendorPaymentRow(long Id)
        {
            return vendorPaymentRepository.GetVendorPaymentRow(Id);
        }
        public IQueryable<PurchaseOrders> VendorPurchaseOrders(long BranchOfficeId)
        {
            return vendorPaymentRepository.VendorPurchase(BranchOfficeId);
        }
        public TblVendorPayment GetVendorPaymentId(int Id, long businessId)
        {
            return vendorPaymentRepository.Get(x => x.Id == Id && x.EmployeeCreateId == businessId).FirstOrDefault();
        }
        public string GetLastVendorPayment(long businessID)
        {
            var query = vendorPaymentRepository.Get(x => x.EmployeeCreateId == businessID);

            if (query.Any())
                return query.LastOrDefault().DocumentNumber;
            else
                return null;
        }
        public TaskResult SetVendorPaymentstatus(int Id, long businessId, long DocumentStatusID)
        {
            try
            {
                TaskResult task = new TaskResult();

                if (Id == 0)
                {
                    task.ExecutedSuccesfully = false;
                    serviceError.LogError("Error", "SetNcfStatus", "AccoutingService", "Los datos no se han podido actualizar correctamente.");
                }

                if (task.ExecutedSuccesfully)
                {
                    var Vendor = GetVendorPaymentId(Id, businessId);


                    var purchaseHeader = vendorPaymentRepository.Get(x => x.Id == Vendor.Id).Include(x => x.TblVendorPaymentRow).FirstOrDefault();

                    if (DocumentStatusID == (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada)
                    {
                        foreach (var item in purchaseHeader.TblVendorPaymentRow)
                        {
                            var pendiente = ValidateBalance(item.PurchaseOrderId);
                            if (pendiente >= item.Payment)
                            {

                                var purchaseOrder = purchaseOrderRepositoy.Get(x => x.Id == item.PurchaseOrderId).FirstOrDefault();

                                if (purchaseOrder != null || purchaseOrder.Balance != 0)
                                {
                                    if (item.Payment <= purchaseOrder.Balance)
                                    {
                                        var nuevoBalance = purchaseOrder.Balance - item.Payment;

                                        purchaseOrder.Balance = nuevoBalance;
                                    }
                                    else
                                    {
                                        return serviceError.WarningMessage("El balance a pagar es mayor al balance pendiente");
                                    }

                                }
                                Vendor.DocumentEstatusId = Convert.ToByte(DocumentStatusID);
                                vendorPaymentRepository.Update(Vendor);
                                purchaseOrderRepositoy.Update(purchaseOrder);
                            }
                            else
                            {
                                task.ExecutedSuccesfully = false;
                                serviceError.LogError("Error", "SetNcfStatus", "AccoutingService", "El balance a pagar del No. " + item.PurchaseOrderDocument + " es mayor al disponible.");
                                task.AddErrorMessage("error");
                            }

                        }
                    }
                    else
                    {
                        Vendor.DocumentEstatusId = Convert.ToByte(DocumentStatusID);
                        vendorPaymentRepository.Update(Vendor);
                    }

                }
                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "_EditVendorPayment", "supplierService");
                serviceError.WarningMessage("Error al registrar el impuesto");
                return serviceError.TaskStatus();
            }

        }
        private TaskResult VendorValidateCreate(TblVendorPayment entity)
        {
            try
            {

                if (entity.DocumentEstatusId == 0)
                    return serviceError.WarningMessage("Es necesario el proveedor de la empresa");
                else if (string.IsNullOrEmpty(entity.DocumentNumber))
                    return serviceError.WarningMessage("El número de documento es obligatorio.");
                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "TaskValidateCreate", "AccountingService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }
        public TaskResult VendorPaymentCreate(TblVendorPayment entity)
        {
            try
            {
                var Vendor = VendorValidateCreate(entity);

                if (Vendor.ExecutedSuccesfully)
                {
                    var sesion = securityService.GetSession();
                    entity.BranchOfficeId = sesion.BranchOfficeId;
                    entity.DocumentNumber = entity.DocumentNumber;
                    entity.Observation = entity.Observation;
                    entity.Total = entity.Total;
                    entity.CreateDate = DateTime.Now;
                    entity.UpdateDate = DateTime.Now;
                    entity.EmployeeCreateId = sesion.EmployeeId;
                    entity.EmployeeModifyId = sesion.EmployeeId;

                    if (entity.DocumentEstatusId == (int)DataAcces.QuickSalEnum.DocumentEstatus.Proceso)
                        entity.DocumentEstatusId = (int)DataAcces.QuickSalEnum.DocumentEstatus.Proceso;
                    else
                        entity.DocumentEstatusId = (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada;


                    vendorPaymentRepository.AddEntity(entity);

                    if (entity.DocumentEstatusId == (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada)
                    {
                        foreach (var item in entity.TblVendorPaymentRow)
                        {
                            var pendiente = ValidateBalance(item.PurchaseOrderId);
                            if (pendiente >= item.Payment)
                            {
                                var purchaseOrder = purchaseOrderRepositoy.Get(x => x.Id == item.PurchaseOrderId).FirstOrDefault();

                                if (purchaseOrder != null || purchaseOrder.Balance != 0)
                                {
                                    if (item.Payment <= purchaseOrder.Balance)
                                    {
                                        var nuevoBalance = purchaseOrder.Balance - item.Payment;

                                        purchaseOrder.Balance = nuevoBalance;
                                    }
                                    else
                                    {
                                        return serviceError.WarningMessage("El balance a pagar es mayor al balance pendiente");
                                    }

                                }
                                purchaseOrderRepositoy.Update(purchaseOrder);
                            }
                            else
                            {
                                Vendor.ExecutedSuccesfully = false;
                                Vendor.AddErrorMessage("El balance a pagar del No. " + item.PurchaseOrderDocument + " es mayor al disponible");
                            }
                        }
                    }

                }

                return Vendor;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "VendorPaymentCreate", "supplierService");
                serviceError.WarningMessage("Error al registrar el Pago");
                return serviceError.TaskStatus();
            }
        }

        public TaskResult VendorPaymentUpdate(TblVendorPayment entity)
        {
            try
            {
                var sesion = securityService.GetSession();
                entity.EmployeeCreateId = sesion.BusinessId;
                var Purchase = VendorValidateCreate(entity);
                var purchaseHeader = vendorPaymentRepository.Get(x => x.Id == entity.Id).Include(x => x.TblVendorPaymentRow).FirstOrDefault();

                if (Purchase.ExecutedSuccesfully)
                {
                    purchaseHeader.Id = entity.Id;
                    purchaseHeader.Total = entity.Total;
                    purchaseHeader.CreateDate = DateTime.Now;
                    purchaseHeader.UpdateDate = DateTime.Now;
                    purchaseHeader.BranchOfficeId = sesion.BranchOfficeId;
                    purchaseHeader.DocumentEstatusId = entity.DocumentEstatusId;
                    purchaseHeader.DocumentNumber = entity.DocumentNumber;
                    purchaseHeader.EmployeeCreateId = entity.EmployeeCreateId;
                    purchaseHeader.Observation = entity.Observation;
                    purchaseHeader.Total = entity.Total;
                    purchaseHeader.EmployeeCreateId = sesion.EmployeeId;
                    purchaseHeader.EmployeeModifyId = sesion.EmployeeId;

                    var PurchaseRows = entity.TblVendorPaymentRow;

                    // esa consulta me busca Las filas que fueron agregadas       
                    var NewRows = (from p in PurchaseRows
                                   where !(from b in purchaseHeader.TblVendorPaymentRow //TblInvoiceRow estan las filas que estan sin modificar en la base de datos
                                           select b.Id).Contains(p.Id)
                                   select p).Distinct().ToList();

                    var DeleteRows = (from p in purchaseHeader.TblVendorPaymentRow//--Esta parte no puede ir en el servicio, si va en el servicio porque es una consulta en memoria, no es una consulta a base de datos
                                      where !(from b in PurchaseRows
                                              select b.Id).Contains(p.Id)
                                      select p).Distinct().ToList();

                    //Eliminamos las filas si existen
                    if (DeleteRows.Count > 0)
                        vendorPaymentRepository.DeleteRange(DeleteRows);

                    //Agregamos nuevos filas si existen nuevos registros
                    if (NewRows.Count > 0)
                        foreach (var item in NewRows)
                            purchaseHeader.TblVendorPaymentRow.Add(item);

                    //aqui estamos Actualizamos filas si existen modificaciones
                    foreach (var PurchaRow in purchaseHeader.TblVendorPaymentRow)
                    {
                        var Row = PurchaseRows.Where(x => x.Id != 0 && x.Id == PurchaRow.Id).FirstOrDefault();

                        if (Row != null)//validamos si la fila fue borrada
                        {
                            PurchaRow.VendorPaymentId = Row.VendorPaymentId;
                            PurchaRow.SuppliersId = Row.SuppliersId;
                            PurchaRow.PurchaseOrderId = Row.PurchaseOrderId;
                            PurchaRow.SupplierName = Row.SupplierName;
                            PurchaRow.Concept = Row.Concept;
                            PurchaRow.Owed = Row.Owed;
                            PurchaRow.PaidOut = Row.PaidOut;
                            PurchaRow.Payment = Row.Payment;
                            PurchaRow.Pending = Row.Pending;
                            PurchaRow.PurchaseOrderDocument = Row.PurchaseOrderDocument;
                        }
                    }
                    vendorPaymentRepository.Update(purchaseHeader);
                    if (entity.DocumentEstatusId == (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada)
                    {
                        foreach (var item in purchaseHeader.TblVendorPaymentRow)
                        {
                            var pendiente = ValidateBalance(item.PurchaseOrderId);
                            if (pendiente >= item.Payment)
                            {
                                var purchaseOrder = purchaseOrderRepositoy.Get(x => x.Id == item.PurchaseOrderId).FirstOrDefault();

                                if (purchaseOrder != null || purchaseOrder.Balance != 0)
                                {
                                    if (item.Payment <= purchaseOrder.Balance)
                                    {
                                        var nuevoBalance = (purchaseOrder.Balance - item.Payment);
                                        purchaseOrder.Balance = nuevoBalance;
                                    }
                                    else
                                    {
                                        return serviceError.WarningMessage("El balance a pagar es mayor al balance pendiente");
                                    }
                                }
                                purchaseOrderRepositoy.Update(purchaseOrder);
                            }
                            else
                            {
                                Purchase.ExecutedSuccesfully = false;
                                Purchase.AddErrorMessage("El balance a pagar del No. " + item.PurchaseOrderDocument + " es mayor al disponible");
                            }
                        }
                    }
                }
                return Purchase;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "_EditVendorPayment", "supplierService");
                serviceError.WarningMessage("Error al registrar el impuesto");
                return serviceError.TaskStatus();
            }
        }

        private decimal ValidateBalance(long purchaseOrderId)
        {
            var pagado = vendorPaymentRepository.ValidarPagado(purchaseOrderId);
            var balance = purchaseOrderRepositoy.Get(purchaseOrderId).Balance;
            var disponible = balance - pagado;
            return disponible;
        }

        #endregion

        #region DebitNote
        ///Nelson://////////////////
        /// Metodo que trae el listado de ordenes de compra disponible
        /// </summary>
        /// <returns></returns>
        public IEnumerable<prc_PurchaseOrderRow> GetPurchOrderRows(long Id)
        {
            return debitNoteRepositoy.GetPurchOrderRow(Id);
        }

        /// <summary>
        /// Obtiene los totales de las notas de debitos seleccionadas
        /// </summary>
        /// <param name="debitsNoteId">Listados con los primary key de las notas de debito</param>
        /// <returns></returns>
        public Dictionary<long, decimal> GetPendingBalance(List<long> debitsNoteId)
        {
            return debitNoteRepositoy.GetPendingBalance(debitsNoteId);
        }

        /// <summary>
        /// Metodo para crear los movimientos de almacen de una sucursal
        /// </summary>
        /// <param name = "movingWareHouse" > Modelo que contiene el movimiento de producto de un almacen</param>
        /// <returns></returns>
        public TaskResult CreateDebitNote(TblDebitNote debitNote, string tipoProceso)
        {
            var task = DebitNoteValidateCreate(debitNote);
            var sesion = securityService.GetSession();
            var user = sesion.EmployeeId;

            try
            {
                var codes = debitNoteRepositoy.GetLastMoving(sesion.BranchOfficeId);
                var code = globalService.GenerateCode(Support.EnumTypes.CodeEntities.DebitNote, codes);
                if (task.ExecutedSuccesfully)
                {
                    debitNote.DocumentNumber = code;
                    debitNote.EmployeeCreateId = user;
                    debitNote.EmployeeModifyId = user;
                    debitNote.CreateDate = DateTime.Now;
                    debitNote.UpdateDate = DateTime.Now;
                    debitNote.Used = false;
                    debitNote.Ncf = "0000000";

                    debitNoteRepositoy.AddEntity(debitNote);
                }
                if (debitNote.DocumentEstatusId == (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada)
                {
                    //var purchase = purchaseOrderRepositoy.Get(debitNote.PurchaseOrderId);
                    //purchase.Balance = purchase.Balance - debitNote.Total;
                    //purchaseOrderRepositoy.Update(purchase);

                    foreach (var item in debitNote.TblDebitNoteRow)
                    {
                        var cantidad = debitNoteRepositoy.UpdateInventoryWareHouse(debitNote.TblDebitNoteRow.Select(z => z.WarehouseId).FirstOrDefault(), item.ProductId, item.Quantity);

                    }
                }

                return task;
            }
            catch (Exception ex)
            {
                task.ExecutedSuccesfully = false;
                task.AddMessage(ex.Message);
                return task;
            }
        }

        /// <summary>
        /// Proceso para hacer las validaciones creacion de nota de debito
        /// </summary>
        /// <param name="debitNote">Modelo que contiene la nota de debito</param>
        /// <returns></returns>
        public TaskResult DebitNoteValidateCreate(TblDebitNote debitNote)
        {
            try
            {
                if (debitNote.BranchOfficeId == 0)
                    return serviceError.WarningMessage("Falta la oficina");
                else if (debitNote.PurchaseOrderId == 0)
                    return serviceError.WarningMessage("Falta el Id de la orden de compra");

                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "DebitNoteValidateCreate", "InventoryService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }


        /// Metodo que trae el listado de nota de Debito
        /// </summary>
        /// <returns></returns>
        public IQueryable<DebitNote> ListDebitNote(long branchOfficeId)
        {
            return debitNoteRepositoy.ListDebitNote(branchOfficeId);
        }

        /// Metodo que trae el  detalle correspondiente
        /// </summary>
        /// <returns></returns>
        public TblDebitNote DebitNoteDetails(long DebitNoteId)
        {
            return debitNoteRepositoy.DebitNoteDetails(DebitNoteId);
        }

        /// Metodo que trae el listado de detalle nota de debito
        /// </summary>
        /// <returns></returns>
        public List<DebitNoteRowVM> GetDebitNoteRow(long DebitNoteId)
        {
            return debitNoteRepositoy.GetDebitNoteRow(DebitNoteId);
        }

        /////Nelson://////////////////
        /// <summary>
        /// Proceso para cambiar el estado de las notas de debito
        /// </summary>
        /// <param name = "Id" > parametro que contiene el Id de la nota de debito</param>
        /// <param name = "DocumentStatusID" > parametro que contiene el Id del estado</param>
        /// <returns></returns>
        public TaskResult SetDebitNotaStatus(int Id, long DocumentStatusID)
        {
            var sesion = securityService.GetSession();
            TaskResult task = new TaskResult();

            if (Id == 0)
            {

                task.ExecutedSuccesfully = false;
                serviceError.LogError("Falta el ID del Ncf", "SetNcfStatus", "AccoutingService", "Los datos no se han podido actualizar correctamente.");
            }

            if (task.ExecutedSuccesfully)
            {
                var debitNote = DebitNoteDetails(Id);
                var TblDebitNoteRow = GetDebitNoteRow(debitNote.Id);

                debitNote.DocumentEstatusId = Convert.ToByte(DocumentStatusID);

                if (DocumentStatusID == (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada)
                {
                    var purchaseOrderAvailable = debitNoteRepositoy.GetPurchOrderRow(debitNote.PurchaseOrderId).ToList();
                    foreach (var item in TblDebitNoteRow)
                    {
                        var cantidadDisponible = purchaseOrderAvailable.Where(x => x.ProductId == item.ProductId).FirstOrDefault();
                        if (cantidadDisponible.Disponible >= item.Quantity)
                        {
                            var cantidad = debitNoteRepositoy.UpdateInventoryWareHouse(item.WarehouseId, item.ProductId, item.Quantity);
                        }
                        else
                        {
                            task.ExecutedSuccesfully = false;
                            serviceError.WarningMessage("La cantidad de " + cantidadDisponible.Produc_Description + " requeridad es mayor a la disponible");
                            return task;
                        }

                    }
                }
                debitNoteRepositoy.Update(debitNote);
            }
            return task;
        }

        /////Nelson://////////////////
        ///// <summary>
        ///// Metodo para actualizar las notas de debitos
        ///// </summary>
        ///// <param name="debitNote">Modelo que contiene la nota de debito</param>
        ///// <returns></returns>
        public TaskResult UpdateDebitNote(TblDebitNote debitNote, int tipoProceso)
        {
            var sesion = securityService.GetSession();
            var task = DebitNoteValidateUpdate(debitNote);
            if (task.ExecutedSuccesfully)
            {

                var tblDebitNote = debitNoteRepositoy.Get(x => x.Id == debitNote.Id).Include(x => x.TblDebitNoteRow).FirstOrDefault();

                tblDebitNote.Observation = debitNote.Observation;
                tblDebitNote.EmployeeModifyId = sesion.EmployeeId;
                tblDebitNote.UpdateDate = DateTime.Now;
                tblDebitNote.DocumentEstatusId = Convert.ToByte(tipoProceso);
                tblDebitNote.SubTotal = debitNote.SubTotal;
                tblDebitNote.Total = debitNote.Total;

                var debitNoteRow = debitNote.TblDebitNoteRow;

                var NewRows = (from d in debitNoteRow
                               where !(from b in tblDebitNote.TblDebitNoteRow //TblInvoiceRow estan las filas que estan sin modificar en la base de datos
                                       select b.Id).Contains(d.Id)
                               select d).Distinct().ToList();


                //Agregamos nuevos filas si existen nuevos registros
                if (NewRows.Count > 0)
                    foreach (var item in NewRows)
                        tblDebitNote.TblDebitNoteRow.Add(item);

                var DeleteRows = (from p in tblDebitNote.TblDebitNoteRow //--Esta parte no puede ir en el servicio, si va en el servicio porque es una consulta en memoria, no es una consulta a base de datos
                                  where !(from b in debitNoteRow
                                          select b.Id).Contains(p.Id)
                                  select p).Distinct().ToList();

                //Eliminamos las filas si existen
                if (DeleteRows.Count > 0)
                    debitNoteRepositoy.DeleteRange(DeleteRows);

                foreach (var debitRow in tblDebitNote.TblDebitNoteRow)
                {
                    //var disponible = debitNoteRepositoy.ValidarCantidadProducto(tblDebitNote.PurchaseOrderId, debitRow.ProductId);

                    var Row = debitNoteRow.Where(x => x.Id == debitRow.Id).FirstOrDefault();
                    if (Row != null)//validamos si la fila fue borrada
                    {

                        debitRow.Quantity = Row.Quantity;
                        debitRow.Total = Row.Total;
                        debitRow.AmountDiscount = Row.AmountDiscount;
                        debitRow.AmountTax = Row.AmountTax;


                        if (tipoProceso == (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada)
                        {
                            var cantidad = debitNoteRepositoy.UpdateInventoryWareHouse(debitRow.WarehouseId, debitRow.ProductId, debitRow.Quantity);
                        }
                    }
                }
                if (tipoProceso == (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada)
                {
                    var purchase = purchaseOrderRepositoy.Get(debitNote.PurchaseOrderId);
                    purchase.Balance = purchase.Balance - debitNote.Total;
                    purchaseOrderRepositoy.Update(purchase);

                    AddNcfDebitNote(sesion.BusinessId, tblDebitNote.Id);
                }
                debitNoteRepositoy.Update(tblDebitNote);
                Savechanges();

            }
            return task;
        }

        /// <summary>
        /// Proceso para hacer las validaciones modificacion de nota de debito
        /// </summary>
        /// <param name="debitNoteR">Modelo que contiene  la nota de debito</param>
        /// <returns></returns>
        public TaskResult DebitNoteValidateUpdate(TblDebitNote debitNote)
        {
            try
            {
                if (debitNote.Id <= 0)
                    return serviceError.WarningMessage("Falta el id de la nota de debito");

                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "DebitNoteValidateCreate", "InventoryService");
                serviceError.WarningMessage("Error al realizar las validaciones");
                return serviceError.TaskStatus();
            }
        }


        /// <summary>
        /// Metodo para agregar el NCF a la nota de debito
        /// </summary>
        /// <param name="businessId">Primary key de la sucursal</param>
        /// <param name="DebitNoteId">Primary key de la cabecera de la nota de debito</param>
        /// <returns></returns>
        public bool AddNcfDebitNote(long businessId, long DebitNoteId)
        {
            return debitNoteRepositoy.AddNcfDebitNote(businessId, DebitNoteId);
        }

        /// <summary>
        /// Metodo validar la cantidad del productos debitados de una orden de compra
        /// </summary>
        /// <param name="PurchaseId">Parametro que contiene el Id del amacen</param>
        /// <param name="ProductId">Parametro que contiene el Id del producto</param>   
        /// <returns></returns>
        public ValidacionProducto ValidarCantidadProducto(long PurchaseId, long ProductId)
        {
            return debitNoteRepositoy.ValidarCantidadProducto(PurchaseId, ProductId);
        }

        #endregion




        public async Task<TaskResult> Savechanges()
        {
            var task = await supplierRepository.Savechanges();

            if (!task.ExecutedSuccesfully)
                serviceError.LogError(task.MessageList[0], "Savechanges", "SupplierController", "Error al intentar guardar los datos.");

            return task;
        }
    }

    public interface ISupplierService
    {
        #region Purchase

        /// <summary>
        /// Metodo para actualizar la nota de debito cuano es utilizada
        /// </summary>
        /// <param name="debit">listado de las notas de debitos utilizadas</param>
        /// <returns></returns>
        TaskResult SetDebitNoteStatusUsed(ICollection<TblDebitNote> debit, TblPurchaseOrder purchaseOrderId);
        /// <summary>
        /// Metodo para obtener el detalle de la orden de compras
        /// </summary>
        /// <param name="PurchaseOrderId">Primary key de la orden de compras</param>
        /// <returns></returns>
        IQueryable<TblPurchaseOrderRow> GetPurchaseOrderRow(long PurchaseOrderId);

        IQueryable<DebitNote> ListDebitNoteState(long branchOfficeId, long suplierId);

        /// <summary>
        /// Metodo para obtener el ultimo codigo generado para los clientes
        /// </summary>
        /// <param name="branchOfficeId">Primary key de la empresa</param>
        /// <returns></returns>
        string GetLastPuschaseCode(long branchOfficeId);

        //el detalle que tienes que devolver dependiendo el Id de  la cabezera
        List<PurchaseOrders> GetPurchOrderRow(long Id);

        Task<TaskResult> SetPurchaseOrdertatus(int Id, long branchOfficeId, long DocumentStatusID);

        Task<TaskResult> UpdatePurchase(TblPurchaseOrder entity);

        Task<TaskResult> CreatePurchase(TblPurchaseOrder entity, ICollection<TblDebitNote> debitNote);

        /// <summary>
        /// Metodo para obtener los proveedores registrados para una empresa
        /// </summary>
        /// <param name="branchOfficeId">Primary key de la empresa</param>
        /// <returns></returns>
        IQueryable<PurchaseOrders> PurchaseOrders(long branchOfficeId);

        /// <summary>
        /// Metodo para obtener los proveedores registrados para una empresa
        /// </summary>
        /// <param name="branchOfficeId">Primary key de la empresa</param>
        /// <returns></returns>
        IQueryable<PurchaseOrders> PurchaseOrdersId(long branchOfficeId, long Id);
        #endregion

        #region Supplier

        /// <summary>
        /// Metodo para obtener los proveedores registrados para una empresa
        /// </summary>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        IQueryable<TblSuppliers> GetSuppliers(long businessId);

        bool MovimientoPurchase(long PurchaseOrderId);

        /// <summary>
        /// Metodo para obtener el ultimo código generado para los proveedores
        /// </summary>
        /// <param name="businessID">Primary key de la empresa</param>
        /// <returns></returns>
        string GetLastSupplierCode(long businessID);

        /// <summary>
        /// Metodo para obtener un proveedor
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="businessId"></param>
        /// <returns></returns>
        TblSuppliers GetSupplier(long Id, long businessId);

        /// <summary>
        /// Metodo para registrar un proveedor
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion del cliente</param>
        /// <returns></returns>
        Task<TaskResult> CreateSupplier(TblSuppliers entity);

        /// <summary>
        /// Metodo para registrar un proveedor
        /// </summary>
        /// <param name="correo">Parametro que posee el correo del suplidor</param>
        /// <returns></returns>
        bool validarCorreo(string correo);

        /// <summary>
        /// Metodo para actualizar un proveedor
        /// </summary>
        /// <param name="entity">Modelo con los datos del empleado</param>
        /// <returns></returns>
        Task<TaskResult> EditSupplier(TblSuppliers entity);

        /// <summary>
        /// Metodo para activar o desactivar un proveedor
        /// </summary>
        /// <param name="status">Estaus para el proveedor</param>
        /// <param name="Id">LLave primaria del proveedor</param>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        TaskResult StatusSupplier(bool status, long Id, long businessId);

        //aqui verifica la consulta que haces porque le estas devolviendo un objeto y es un listado que tienes que devolver
        TblPurchaseOrder GetPurchase(long Id, long businessId);

        #endregion

        #region VendorPayment
        List<VendorPayment> GetVendorPaymentRow(long Id);

        /// Metodo que trae el listado de compras
        /// </summary>
        /// <returns></returns>
        IQueryable<TblVendorPaymentRow> PurchaseRow(long businessID, IEnumerable<long> datos);

        IQueryable<TblVendorPayment> GetVendorPayment(long BusinessId);

        IQueryable<PurchaseOrders> VendorPurchaseOrders(long BranchOfficeId);

        TblVendorPayment GetVendorPaymentId(int Id, long businessId);

        string GetLastVendorPayment(long businessID);

        TaskResult SetVendorPaymentstatus(int Id, long businessId, long DocumentStatusID);

        TaskResult VendorPaymentCreate(TblVendorPayment entity);

        TaskResult VendorPaymentUpdate(TblVendorPayment entity);
        #endregion

        #region Nota de Debito

        /// <summary>
        /// Obtiene los balances de las notas de debitos seleccionadas
        /// </summary>
        /// <param name="debitsNoteId">Listados con los primary key de las notas de debito</param>
        /// <returns></returns>
        Dictionary<long, decimal> GetPendingBalance(List<long> debitsNoteId);

        /// Metodo que trae el listado de nota de Debito
        /// </summary>
        /// <returns></returns>
        IQueryable<DebitNote> ListDebitNote(long branchOfficeId);

        /// Metodo que trae el detalle correspondiente
        /// </summary>
        /// <returns></returns>
        TblDebitNote DebitNoteDetails(long DebitNoteId);


        /// Metodo que trae el listado de detalle nota de debito
        /// </summary>
        /// <returns></returns>
        List<DebitNoteRowVM> GetDebitNoteRow(long DebitNoteId);


        /////Nelson://////////////////
        /// Metodo que trae el listado de ordenes de compra disponible
        /// </summary>
        /// <returns></returns>
        IEnumerable<prc_PurchaseOrderRow> GetPurchOrderRows(long Id);

        /// <summary>
        /// Metodo para crear los movimientos de almacen de una sucursal
        /// </summary>
        /// <param name="movingWareHouse">Modelo que contiene el movimiento de producto de un almacen</param>
        /// <returns></returns>
        TaskResult CreateDebitNote(TblDebitNote debitNote, string tipoProceso);

        /////Nelson://////////////////
        /// <summary>
        /// Metodo validar la cantidad del productos debitados de una orden de compra
        /// </summary>
        /// <param name="PurchaseId">Parametro que contiene el Id del amacen</param>
        /// <param name="ProductId">Parametro que contiene el Id del producto</param>   
        /// <returns></returns>
        ValidacionProducto ValidarCantidadProducto(long PurchaseId, long ProductId);

        /// <summary>
        /// Metodo para actualizar las notas de debitos
        /// </summary>
        /// <param name="debitNote">Modelo que contiene la nota de debito</param>
        /// <returns></returns>
        TaskResult UpdateDebitNote(TblDebitNote debitNote, int tipoProceso);


        /// <summary>
        /// Proceso para cambiar el estado de las notas de debito
        /// </summary>
        /// <param name="Id">parametro que contiene el Id de la nota de debito</param>
        /// <param name="businessId">parametro que contiene el Id del negocio</param>
        /// <param name="DocumentStatusID">parametro que contiene el Id del estado</param>
        /// <returns></returns>
        TaskResult SetDebitNotaStatus(int Id, long DocumentStatusID);

        /// <summary>
        /// Metodo para agregar el NCF a la nota de debito
        /// </summary>
        /// <param name="branchOfficeId">Primary key de la sucursal</param>
        /// <param name="DebitNoteId">Primary key de la cabecera de la nota de debito</param>
        /// <returns></returns>
        bool AddNcfDebitNote(long businessId, long DebitNoteId);
        #endregion


        Task<TaskResult> Savechanges();
    }

}

