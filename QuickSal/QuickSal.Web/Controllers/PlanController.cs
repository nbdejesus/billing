﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using QuickSal.Services;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.Plan;

namespace QuickSal.Web.Controllers
{
    public class PlanController : Controller
    {
        private IPlanService planService;
        private ISecurityService securityService;
        private IServiceError serviceError;
        private IBusinessService businessService;

        public PlanController(IPlanService planService, ISecurityService securityService, IServiceError serviceError,
            IBusinessService businessService)
        {
            this.planService = planService;
            this.securityService = securityService;
            this.serviceError = serviceError;
            this.businessService = businessService;
        }


        /// <summary>
        /// Metodo para cargar la seccion de los planes en la empresa
        /// </summary>
        /// <param name="businessTypeId"></param>
        /// <returns></returns>
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public async Task<PartialViewResult> _BusinessPlan(int businessTypeId)
        {
            PlanModule model = new PlanModule();
            model.plans = planService.GetPlans(businessTypeId);
            model.ModuleList = await planService.GetAllModule();

            return PartialView(model);
        }


        /// <summary>
        /// Metodo para cargar la vista con los planes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public IActionResult BusinessPlanList()
        {
            try
            {
                var session = securityService.GetSession();

                ViewBag.PlanId = businessService.GetPlanId(session.BusinessId);
                ViewData["BusinessType"] = businessService.GetBusinesTypeId(session.BusinessId);
                return View();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "BusinessPlanList", "BusinessController", "Error intentando cargar los datos de los planes.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para actualizar el plan de la empresa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult UpdateBusinessPlan(int id)
        {
            try
            {
                var sesion = securityService.GetSession();
                bool status = businessService.UpdateBusinessPlan(sesion.BusinessId, id);

                return Json(new
                {
                    message = ((status) ? "Plan actualizado correctamente" : "Plan no se ha podido actualizar"),
                    status = status,
                    redirect = Url.Action("BusinessPlanList"),
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateBusinessPlan", "BusinessController", "Error intentando actualizar el plan seleccionado.");
                return null;
            }
        }
    }
}