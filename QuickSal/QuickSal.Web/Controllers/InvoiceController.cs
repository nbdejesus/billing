﻿using Microsoft.AspNetCore.Mvc;
using QuickSal.DataAcces.Contexts;
using QuickSal.Services;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.Customer;
using QuickSal.Web.ViewModels.Customer.Invoice;
using QuickSal.Web.ViewModels.Customer.Invoice.FormalInvoice;
using QuickSal.Web.ViewModels.Customer.Invoice.SalesPoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static QuickSal.DataAcces.QuickSalEnum;

namespace QuickSal.Web.Controllers
{
    public class InvoiceController : Controller
    {
        private ICustomerService customerService;
        private ISecurityService securityService;
        private IServiceError serviceError;
        private IBusinessService businessService;
        private IInventoryService inventoryService;
        private IGlobalService globalService;
        private IAccountingService accountingService;

        public InvoiceController(ICustomerService customerService, ISecurityService securityService, IServiceError serviceError,
                                 IBusinessService businessService, IInventoryService inventoryService, IGlobalService globalService, IAccountingService accountingService)
        {
            this.customerService = customerService;
            this.securityService = securityService;
            this.serviceError = serviceError;
            this.businessService = businessService;
            this.inventoryService = inventoryService;
            this.globalService = globalService;
            this.accountingService = accountingService;
        }



        #region Sales Point

        /// <summary>
        /// Metodo para cargar la vista de registro de producto
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<IActionResult> InvoiceIndex()
        {
            try
            {
                InvoiceViewModel Model = new InvoiceViewModel();
                var session = securityService.GetSession();

                Model.customers = customerService.GetCustomers(session.BusinessId);
                Model.Warehouses = await inventoryService.GetWarehouses(session.BranchOfficeId, active: true);
                Model.Ncfprefixes = accountingService.GettypeNcf(session.BusinessId);

                return View("~/views/Invoice/SalesPoint/InvoiceIndex.cshtml", Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "InvoiceIndex", "InvoiceController", "Tuvimos un problema cargar la factura.");
                return null;
            }
        }


        /// <summary>
        /// Metodo para cargar el listado de facturas de una empresa
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult InvoiceList()
        {
            try
            {
                InvoiceViewModel Model = new InvoiceViewModel();
                var session = securityService.GetSession();

                Model.Invoices = customerService.GetAllInvoices(session.BranchOfficeId);
                return View("~/views/Invoice/SalesPoint/InvoiceList.cshtml", Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "InvoiceList", "InvoiceController", "Tuvimos un problema cargar listado de facturas.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para cargar la factura
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<IActionResult> EditInvoice(long InvoiceId)
        {
            try
            {
                InvoiceViewModel Model = new InvoiceViewModel();
                Model.InvoiceHeader = customerService.GetInvoiceHeader(InvoiceId);

                var session = securityService.GetSession();
                Model.customers = customerService.GetCustomers(session.BusinessId);
                Model.Warehouses = await inventoryService.GetWarehouses(session.BranchOfficeId, active: true);
                Model.Ncfprefixes = accountingService.GettypeNcf(session.BusinessId);

                return View("~/views/Invoice/SalesPoint/EditInvoice.cshtml", Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EditInvoice", "InvoiceController", "Tuvimos un problema cargar la factura.");
                return null;
            }
        }


        /// <summary>
        /// Metodo para cargar la vista de registro de producto
        /// </summary>
        /// <returns></returns> 
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public async Task<PartialViewResult> _Products(int? warehouse)
        {
            try
            {
                InvoiceViewModel Model = new InvoiceViewModel();
                var session = securityService.GetSession();

                int wareHouseId = 0;
                if (!warehouse.HasValue)
                {
                    Model.Warehouses = await inventoryService.GetWarehouses(session.BranchOfficeId, active: true);
                    wareHouseId = Model.Warehouses.Where(x => x.Principal).FirstOrDefault().Id;
                }
                else
                    wareHouseId = warehouse.Value;


                Model.GetProductsOnSale = customerService.GetProductsOnSale(wareHouseId);
                return PartialView("~/views/Invoice/SalesPoint/Partial/_Products.cshtml", Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "Products", "InvoiceController", "Tuvimos un problema cargar listado de productos.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para cargar la vista de registro de producto
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public PartialViewResult _Customers()
        {
            try
            {
                CustomerViewModel model = new CustomerViewModel();
                var session = securityService.GetSession();

                ////Obtenemos un nuevo codigo para el cliente
                string code = customerService.GetLastCustomerCode(session.BusinessId);
                ViewBag.CustomerCode = globalService.GenerateCode(Services.Support.EnumTypes.CodeEntities.Customer, code);

                //Cargamos las opciones desplegables
                model.Countries = globalService.GetAllCountries();
                model.DocumentIdentifications = globalService.GetAllDocumentIdentification();
                model.Ncfprefixes = globalService.GetAllPrefixNCF();

                return PartialView("~/views/Invoice/SalesPoint/Partial/_Customers.cshtml", model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "_Customers", "InvoiceController", "Tuvimos un problema cargar el registro de clientes.");
                return null;
            }
        }


        ///// <summary>
        ///// Metodo para obtener los productos de un almacén
        ///// </summary>
        ///// <param name="warehouseId">Primary key del almacén</param>
        ///// <returns></returns>
        //[ServiceFilter(typeof(LoginSecurityFilter))]
        //public PartialViewResult GetProductWarehouse(int warehouseId)
        //{
        //    try
        //    {
        //        InvoiceViewModel Model = new InvoiceViewModel();
        //        var session = securityService.GetSession();
        //        //Model.ProductsWareHouse = customerService.GetAllProducts(session.BranchOfficeId, warehouseId);
        //        return PartialView("~/views/Invoice/Partial/_Products.cshtml", Model);
        //    }
        //    catch (Exception ex)
        //    {
        //        serviceError.LogError(ex.Message, "GetProductWarehouse", "InvoiceController", "Tuvimos un problema cargar listado de productos.");
        //        return null;
        //    }
        //}

        /// <summary>
        /// Metodo para realizar busqueda de los productos
        /// </summary>
        /// <param name="warehouseId">Primary key del almacén</param>
        /// <param name="text">Criterio de busqueda</param>
        /// <returns></returns>
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public JsonResult SearchProducts(int warehouseId, string text)
        {
            try
            {
                var sesion = securityService.GetSession();
                var products = customerService.SeacrhProducts(sesion.BranchOfficeId, warehouseId, text);

                return Json(new
                {
                    Products = products
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "SearchProducts", "Invoice", "se presento un problema grabar la información del producto.");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }


        /// <summary>
        /// Metodo para agregar un item al a factura
        /// </summary>
        /// <param name="ProductId"></param>
        /// <returns></returns>
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public PartialViewResult _ItemInvoice(long ProductId)
        {
            try
            {
                InvoiceViewModel Model = new InvoiceViewModel();
                var session = securityService.GetSession();

                var Item = customerService.GetProductSalePoint(ProductId);
                List<InvoiceItem> invoiceItems = new List<InvoiceItem>();

                invoiceItems.Add(new InvoiceItem
                {
                    ProductId = Item.Id,
                    Tax = Item.Tax,
                    Price = Item.Price,
                    TaxAbbreviation = Item.TaxAbbreviation,
                    MeaseureAbbreviation = Item.Abbreviation,
                    ProductName = Item.ProductName,
                    ItemQuantity = 1,
                    ItemDiscount = 0,
                    WarehouseId = 0
                });

                Model.invoiceItems = invoiceItems;

                return PartialView("~/views/Invoice/SalesPoint/Partial/_ItemInvoice.cshtml", Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "_ItemInvoice", "InvoiceController", "Tuvimos un problema cargar listado de productos.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para agregar un item al a factura
        /// </summary>
        /// <param name="ProductId"></param>
        /// <returns></returns>
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public PartialViewResult _DetailInvoice(long InvoiceId)
        {
            try
            {
                InvoiceViewModel Model = new InvoiceViewModel();
                var session = securityService.GetSession();

                var items = customerService.GetInvoiceDetail(InvoiceId).ToList();

                Model.invoiceItems = from c in items
                                     select new InvoiceItem
                                     {
                                         InvoiceRowId = c.Id,
                                         ProductId = c.ProductId,
                                         Tax = c.Tax,
                                         Price = c.Price,
                                         TaxAbbreviation = c.TaxAbbreviation,
                                         MeaseureAbbreviation = c.MeaseureAbbreviation,
                                         ProductName = c.ProductName,
                                         ItemQuantity = c.ItemQuantity,
                                         ItemDiscount = c.ItemDiscount,
                                         Subtotal = c.SubTotal,
                                         AmountDiscount = c.AmountDiscount,
                                         AmountTax = c.AmountTax,
                                         WarehouseId = c.WarehouseId
                                     };


                return PartialView("~/views/Invoice/SalesPoint/Partial/_ItemInvoice.cshtml", Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "_DetailInvoice", "InvoiceController", "Tuvimos un problema cargar listado de productos.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para obtener informacion sobre el cliente
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public JsonResult GetCustomerData(long CustomerId)
        {
            try
            {
                var sesion = securityService.GetSession();
                var customer = customerService.GetCustomer(CustomerId, sesion.BusinessId);

                var dataCustomer = new CustomerDataViewModel()
                {
                    creditType = customer.CreditTypeId,
                    NcfPrefixId = customer.Ncfid
                };

                return Json(new
                {
                    dataCustomer = dataCustomer
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "GetTypeCredit", "Invoice", "se presento un problema grabar la información del cliente.");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }

        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<IActionResult> ViewInvoice(long InvoiceId)
        {
            try
            {
                InvoiceViewModel Model = new InvoiceViewModel();
                Model.InvoiceHeader = customerService.GetInvoiceHeader(InvoiceId);

                var session = securityService.GetSession();
                Model.customers = customerService.GetCustomers(session.BusinessId);
                Model.Warehouses = await inventoryService.GetWarehouses(session.BranchOfficeId);
                Model.Ncfprefixes = accountingService.GettypeNcf(session.BusinessId);

                return View("~/Views/Invoice/EditInvoice.cshtml", Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "InvoiceList", "InvoiceController", "Tuvimos un problema cargar la factura.");
                return null;
            }
        }

        ///// Metodo para cambiar el estado de la factura
        ///// </summary>
        ///// <returns></returns>
        //[ServiceFilter(typeof(HandledSecurityFilter))]
        //public JsonResult SalesPointStatusInvoice(int Id, byte DocumentStatusID)
        //{
        //    try
        //    {
        //        var session = securityService.GetSession();
        //        customerService.SetInvoiceStatus(Id, DocumentStatusID);
        //        customerService.Savechanges();

        //        return Json(new
        //        {
        //            message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" + "Factura de venta actualizada correctamente" : "Desactivada") + "",
        //            status = serviceError.TaskStatus().ExecutedSuccesfully,
        //            redirect = Url.Action("FormalInvoiceList")
        //        });
        //    }
        //    catch (Exception ex)
        //    {
        //        serviceError.LogError(ex.Message, "StatusInvoice", "InvoiceController", "Tuvimos un problema al actualizar la factura de venta.");
        //        return Json(new
        //        {
        //            message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
        //            status = serviceError.TaskStatus().ExecutedSuccesfully
        //        });
        //    }
        //}

        #endregion

        #region Formal Invoice
        /// <summary>
        /// Metodo para cargar el listado de facturas de una empresa
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult FormalInvoiceList()
        {
            try
            {
                InvoiceFormalViewModel Model = new InvoiceFormalViewModel();
                var session = securityService.GetSession();

                Model.Invoices = customerService.GetAllInvoices(session.BranchOfficeId);
                return View("~/views/Invoice/FormalInvoice/FormalInvoiceList.cshtml", Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "FormalInvoiceList", "InvoiceController", "Tuvimos un problema cargar listado de facturas.");
                return null;
            }
        }


        [HttpGet]
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public PartialViewResult _CustomersList()
        {
            InvoiceFormalViewModel Model = new InvoiceFormalViewModel();
            var session = securityService.GetSession();

            Model.customers = customerService.GetCustomers(session.BusinessId);
            return PartialView("~/views/Invoice/FormalInvoice/Partial/_CustomersList.cshtml", Model);
        }

        /// <summary>
        /// Metodo para cargar  el formulario de  Compras
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult InvoiceFormalCreate(long CustomerId)
        {
            try
            {
                InvoiceFormalViewModel Model = new InvoiceFormalViewModel();
                var session = securityService.GetSession();

                Model.Customer = customerService.GetCustomer(CustomerId, session.BusinessId);

                ViewBag.Invoice = "00000000";
                return View("~/views/Invoice/FormalInvoice/InvoiceFormalCreate.cshtml", Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "InvoiceFormalCreate", "InvoiceController", "Tuvimos un problema al cargar el formulario de factura.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para cargar la vista para el actualizar la factura formal
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult EditFormalInvoice(int Id)
        {
            try
            {
                var session = securityService.GetSession();
                InvoiceFormalViewModel Model = new InvoiceFormalViewModel();

                Model.InvoiceHeader = customerService.GetInvoiceHeader(Id);
                Model.InvoiceRows = customerService.GetInvoiceDetail(Id);
                Model.Customer = customerService.GetCustomer(Model.InvoiceHeader.CustomerId.Value, session.BusinessId);

                //Obtenemos el balance pendiente
                if (Model.InvoiceHeader.TblCretitNoteUsed != null && Model.InvoiceHeader.TblCretitNoteUsed.Count > 0)
                {
                    Model.GenericVm = new GenericVM();
                    Model.GenericVm.PendingBalance = customerService.GetPendingBalance(Model.InvoiceHeader.TblCretitNoteUsed.Select(x => x.CreditNoteId).ToList());
                }

                //Cargamos las opciones desplegables
                return View("~/views/Invoice/FormalInvoice/EditFormalInvoice.cshtml", Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EditInvoiceOrder", "InvoiceController", "Tuvimos un problema al cargar los datos de la factura de ventas.");
                return null;
            }
        }


        /// <summary>
        /// Metodo para cargar el listado de productos para la factura
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public async Task<PartialViewResult> _ProductsList()
        {
            try
            {
                InvoiceFormalViewModel Model = new InvoiceFormalViewModel();
                var session = securityService.GetSession();

                var warehouse = inventoryService.GetPrincipalWarehouse(session.BranchOfficeId);
                Model.Warehouses = await inventoryService.GetWarehouses(session.BranchOfficeId);
                Model.ProductsWareHouse = customerService.GetAllProducts(warehouse.Id);

                return PartialView("~/views/Invoice/FormalInvoice/Partial/_ProductsList.cshtml", Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "_ProductsList", "InvoiceController", "Tuvimos un problema cargar listado de productos.");
                return null;
            }
        }


        ///// Metodo para cambiar el estado de la factura
        ///// </summary>
        ///// <returns></returns>
        //[ServiceFilter(typeof(HandledSecurityFilter))]
        //public JsonResult FormalStatusInvoice(int Id, byte DocumentStatusID)
        //{
        //    try
        //    {
        //        var session = securityService.GetSession();
        //        customerService.SetInvoiceStatus(Id, DocumentStatusID);
        //        customerService.Savechanges();

        //        return Json(new
        //        {
        //            message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" + "Factura de venta actualizada correctamente" : "Desactivada") + "",
        //            status = serviceError.TaskStatus().ExecutedSuccesfully,
        //            redirect = Url.Action("FormalInvoiceList")
        //        });
        //    }
        //    catch (Exception ex)
        //    {
        //        serviceError.LogError(ex.Message, "StatusInvoice", "InvoiceController", "Tuvimos un problema al actualizar la factura de venta.");
        //        return Json(new
        //        {
        //            message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
        //            status = serviceError.TaskStatus().ExecutedSuccesfully
        //        });
        //    }
        //}

        #endregion

        #region Generic


        /// <summary>
        /// Metodo para registrar una factura
        /// </summary>
        /// <param name="model">Entidad con la información de la factura</param>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> CreateInvoice(InvoiceViewModel invoiceModel)
        {
            try
            {
                var sesion = securityService.GetSession();
                customerService.CreateInvoice(invoiceModel.InvoiceHeader);

                var task = await customerService.Savechanges();

                if (task.ExecutedSuccesfully)
                {
                    //Pedidos
                    if (invoiceModel.Order != null)
                    {
                        invoiceModel.Order.InvoiceId = invoiceModel.InvoiceHeader.Id;
                        customerService.CreateOrder(invoiceModel.Order);
                        task = await customerService.Savechanges();
                    }

                    if (invoiceModel.InvoiceHeader.DocumentEstatusId == (int)DocumentEstatus.Aprobada)
                    {
                        //Procesando factura
                        task.ExecutedSuccesfully = customerService.ProcessInvoice(sesion.BranchOfficeId, invoiceModel.InvoiceHeader.Id);

                        if (task.ExecutedSuccesfully)
                        {
                            //Pagos
                            invoiceModel.Payment.DocumentEstatusId = (int)DocumentEstatus.Aprobada;
                            invoiceModel.Payment.TblCustomerPaymentsRow.FirstOrDefault().InvoiceId = invoiceModel.InvoiceHeader.Id;
                            invoiceModel.Payment.TblCustomerPaymentsRow.FirstOrDefault().InvoiceDocument = invoiceModel.InvoiceHeader.DocumentNumber;
                            if (string.IsNullOrEmpty(invoiceModel.Payment.TblCustomerPaymentsRow.FirstOrDefault().Concept))
                                invoiceModel.Payment.TblCustomerPaymentsRow.FirstOrDefault().Concept = "Pago a factura no." + invoiceModel.InvoiceHeader.DocumentNumber;


                            task = customerService.CustomerPaymentCreate(invoiceModel.Payment);
                            await customerService.Savechanges();
                        }
                    }
                }

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Factura registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateInvoice", "Invoice", "se presentó un problema al grabar la información de la factura.");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }


        /// <summary>
        /// Metodo para registrar una factura
        /// </summary>
        /// <param name="model">Entidad con la información de la factura</param>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> UpdateInvoice(InvoiceViewModel invoiceModel)
        {
            try
            {
                var sesion = securityService.GetSession();
                customerService.UpdateInvoice(invoiceModel.InvoiceHeader);
                var task = await customerService.Savechanges();

                if (task.ExecutedSuccesfully && invoiceModel.InvoiceHeader.DocumentEstatusId == (int)DocumentEstatus.Aprobada)
                    task.ExecutedSuccesfully = customerService.ProcessInvoice(sesion.BranchOfficeId, invoiceModel.InvoiceHeader.Id);

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Factura actualizada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateInvoice", "Invoice", "se presentó un problema al actualizar la información de la factura.");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }

        /// <summary>
        /// Metodo para mostrar el listado de PurchaseOrder
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public PartialViewResult _BalanceList(long customerId)
        {
            try
            {
                GenericVM genericVM = new GenericVM();
                var session = securityService.GetSession();
                genericVM.ListCreditNoteVM = customerService.GetPendingBalance(session.BranchOfficeId, customerId);
                return PartialView("~/views/Invoice/Partial/_BalanceList.cshtml", genericVM);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "_BalanceList", "InvoiceController", "Problemas al cargar el listado de balances pendientes.");
                return null;
            }
        }



        /// <summary>
        /// Metodo para actualizar el estatus del cliente
        /// </summary>
        /// <param name="status">Estado al que pasará el cliente</param>
        /// <param name="Id">Primary key del cliente</param>
        /// <returns></returns>

        [ServiceFilter(typeof(LoginSecurityFilter))]
        public JsonResult GetCustomersName()
        {
            try
            {
                var session = securityService.GetSession();
                var customers = customerService.GetCustomers(session.BusinessId).Select(x => new TblCustomers { Names = x.Names, Id = x.Id });

                return Json(new
                {
                    Customers = customers
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "StatusCustomer", "CustomerController", "se presento un problema al actualizar la información del cliente");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }


        /// <summary>
        /// Metodo para cargar la vista de registro de producto
        /// </summary>
        /// <returns></returns> 
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public async Task<PartialViewResult> _Payments()
        {
            try
            {
                var session = securityService.GetSession();

                return PartialView("~/views/Invoice/Partial/_Payments.cshtml");
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "_Payments", "InvoiceController", "Tuvimos un problema cargar el modulo de pagos.");
                return null;
            }
        }


        /// <summary>
        /// Metodo para cargar la vista de registro de producto
        /// </summary>
        /// <returns></returns> 
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public async Task<PartialViewResult> _NoteOrder()
        {
            try
            {
                var session = securityService.GetSession();

                return PartialView("~/views/Invoice/Partial/_NoteOrder.cshtml");
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "_NoteOrder", "InvoiceController", "Tuvimos un problema cargar el modal.");
                return null;
            }
        }


        /// Metodo para cambiar el estado de la factura
        /// </summary>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult StatusInvoice(int Id, byte DocumentStatusID)
        {
            try
            {
                var session = securityService.GetSession();
                customerService.SetInvoiceStatus(Id, DocumentStatusID);
                customerService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" + "Factura de venta actualizada correctamente" : "Desactivada") + "",
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("FormalInvoiceList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "StatusInvoice", "InvoiceController", "Tuvimos un problema al actualizar la factura de venta.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }
        #endregion
    }
}
