﻿using Microsoft.AspNetCore.Mvc;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using QuickSal.Services;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.Inventory;
using QuickSal.Web.ViewModels.Nfc;
using System;
using System.Collections.Generic;

namespace QuickSal.Web.Controllers
{

    public class NcfController : Controller
    {
        private IServiceError serviceError;
        private ISecurityService SecurityService;
        private IAccountingService accountingService;

        public NcfController(IServiceError serviceError, ISecurityService SecurityService, IAccountingService accountingService)
        {
            this.serviceError = serviceError;
            this.SecurityService = SecurityService;
            this.accountingService = accountingService;
        }

        /// <summary>
        /// Metodo para mostrar el listado de NCF
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult NcfList()
        {
            try
            {
                NcfViewModel Model = new NcfViewModel();
                var session = SecurityService.GetSession().BusinessId;
                var bussined = Convert.ToInt64(session);
                Model.ListNcfs = accountingService.GetNcfs(bussined);

                //Model.ncf.TblNcfrow//<---fijate que yo acceso a su relacion ok
                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "NcfList", "NcfController", "Tuvimos un problema cargar listado de ncf.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para cargar la vista de registro de ncf
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public PartialViewResult _NcfCreate()
        {
            var session = SecurityService.GetSession().BusinessId;
            var businessId = Convert.ToInt64(session);
            NcfViewModel Model = new NcfViewModel();
            Model.Prefix = accountingService.GettypeNcf(businessId);
            return PartialView(Model);
        }

        /// <summary>
        /// Metodo para registrar una ncf
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult CreateNcf(NcfViewModel Model)
        {

            try
            {

                accountingService.CreateNcf(Model.ncf);
                accountingService.Savechanges();
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Ncf registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateNcf", "NcfController", "Tuvimos un problema al registrar la ncf.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Ncf registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        /// <summary>
        /// Metodo para cargar la vista para el actualizar los ncf
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult _EditNcf(int Id)
        {
            try
            {
                var session = SecurityService.GetSession().BusinessId;
                var businessId = Convert.ToInt64(session);
                NcfViewModel model = new NcfViewModel();
                model.ncf = accountingService.GetNcf(Id, businessId);
                model.Prefix = accountingService.GettypeNcf(businessId);

                //Cargamos las opciones desplegables
                return PartialView(model);

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EditCustomer", "CustomerController", "Tuvimos un problema al cargar los datos del cliente.");
                return null;
            }
        }

        /// <summary>
        /// metodo para actualizar una ncf
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult UpdateNcf(NcfViewModel Model)
        {
            try
            {
                accountingService.UpdateNcf(Model.ncf);
                accountingService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Ncf actualizada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateNcf", "NcfController", "Tuvimos un problema al actualizar la Ncf.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Ncf actualizada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        /// <summary>
        /// Metodo para cambiar de estatus del NCF 
        /// </summary>
        /// <param name="Id">Primary key del  NCF </param>
        /// <param name="status">Estatus del  NCF </param>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult StatusNcf(int Id, bool status)
        {
            try
            {
                var bussineId = SecurityService.GetSession().BusinessId;
                accountingService.SetNcfStatus(Id, status, bussineId);
                accountingService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Ncf " + ((status) ? "habilitada" : "desactivada") + " correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("NcfList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "StatusNcf", "NcfController", "Tuvimos un problema al " + ((status) ? "activar" : "Desactivar") + " el Ncf.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        /// <summary>
        /// Metodo para cargar la vista para ver el detalle de los ncf
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult NcfDetails(int Id)
        {
            try
            {
                var bussineId = SecurityService.GetSession().BusinessId;
                NcfViewModel model = new NcfViewModel();
                model.ListNcfs = accountingService.ListVoucher(bussineId, Id);

                //Cargamos las opciones desplegables
                return PartialView(model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EditNcf", "NcfController", "Tuvimos un problema al cargar los datos del cliente.");
                return null;
            }
        }


        [ServiceFilter(typeof(LoginSecurityFilter))]
        public JsonResult ValidarForm(TblNcf Model)
        {
            try
            {
                bool estatu = accountingService.ValidarForm(Model);
                return Json(new { estatu = estatu });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = true
                });
            }
        }

        [ServiceFilter(typeof(LoginSecurityFilter))]
        public JsonResult ValidarFormEdit(Ncfs Model)
        {
            bool estatu = accountingService.ValidarFormEdit(Model);

            return Json(new { estatu = estatu });

        }
    }
}
