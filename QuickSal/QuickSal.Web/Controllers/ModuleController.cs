﻿using Microsoft.AspNetCore.Mvc;
using QuickSal.Services;
using QuickSal.Web.Infraestructure.Security;

namespace QuickSal.Web.Controllers
{
    public class ModuleController : Controller
    {
        private ISecurityService securityService;

        public ModuleController(ISecurityService securityService)
        {
            this.securityService = securityService;
        }

        [HttpGet]
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public IActionResult Index(int id)
        {
            var subObject = securityService.GetSubObject(id);
            ViewBag.Url = subObject.UrlModules;
            return View();
        }
    }
}