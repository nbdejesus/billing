﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using QuickSal.DataAcces.Contexts;
using QuickSal.Services;
using QuickSal.Web.Api;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.Supplier;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static QuickSal.DataAcces.QuickSalEnum;

namespace QuickSal.Web.Controllers
{
    public class SupplierController : Controller
    {
        private ISupplierService supplierService;
        private IServiceError serviceError;
        private IGenericMethod genericMethod;
        private IGlobalService globalService;
        private ISecurityService securityService;
        private IConfiguration configurationService;
        private IHostingEnvironment environmentService;
        private IAmazonService amazonService;

        public SupplierController(ISupplierService supplierService, IServiceError serviceError, IAmazonService amazonService,
                                  IGenericMethod genericMethod, IGlobalService globalService, ISecurityService securityService,
                                  IConfiguration configurationService, IHostingEnvironment environmentService)
        {
            this.supplierService = supplierService;
            this.serviceError = serviceError;
            this.genericMethod = genericMethod;
            this.securityService = securityService;
            this.globalService = globalService;
            this.configurationService = configurationService;
            this.environmentService = environmentService;
            this.amazonService = amazonService;
        }

        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult SuppliersList()
        {
            try
            {
                SupplierViewModel model = new SupplierViewModel();
                Services.Support.SessionViewModel session = securityService.GetSession();
                model.Suppliers = supplierService.GetSuppliers(session.BusinessId);
                return View(model);

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "SuppliersList", "SupplierController", "Error intentando carga la vista de proveedores.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para cargar la vista para crear proveedor
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<IActionResult> SupplierIndex()
        {
            try
            {
                SupplierViewModel model = new SupplierViewModel();
                Services.Support.SessionViewModel session = securityService.GetSession();

                ////Obtenemos un nuevo código para el proveedor
                string code = supplierService.GetLastSupplierCode(session.BusinessId);
                ViewBag.SupplierCode = globalService.GenerateCode(Services.Support.EnumTypes.CodeEntities.Supplier, code);

                //Cargamos las opciones desplegables
                model.Countries = globalService.GetAllCountries();
                model.DocumentIdentifications = globalService.GetAllDocumentIdentification();
                model.Ncfprefixes = globalService.GetAllPrefixNCF();

                await LoadPictures(model);

                return View(model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "SupplierIndex", "SupplierController", "Tuvimos un problema al cargar la vista para el registro.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para registrar los datos del proveedor
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> CreateSupplier(SupplierViewModel model)
        {
            try
            {
                await supplierService.CreateSupplier(model.Supplier);
                var task = await supplierService.Savechanges();

                return Json(new
                {
                    message = ((task.ExecutedSuccesfully) ? "Proveedor registrado correctamente" : task.MessageList[0]),
                    status = task.ExecutedSuccesfully,
                    redirect = Url.Action("SuppliersList"),
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateSupplier", "SupplierController", "se presento un problema grabar la información del proveedor");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }

        /// <summary>
        /// Metodo para cargar la vista de edición del proveedor
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<IActionResult> EditSupplier(long Id)
        {
            try
            {
                Services.Support.SessionViewModel session = securityService.GetSession();
                SupplierViewModel model = new SupplierViewModel();
                long businessID = session.BusinessId;

                model.Supplier = supplierService.GetSupplier(Id, businessID);

                //Cargamos las opciones desplegables
                model.Countries = globalService.GetAllCountries();
                model.DocumentIdentifications = globalService.GetAllDocumentIdentification();
                model.Ncfprefixes = globalService.GetAllPrefixNCF();

                if (model.Supplier.CountryId.HasValue)
                    model.Provinces = globalService.GetAllProvince(model.Supplier.CountryId.Value);

                await LoadPictures(model);
                return View(model);

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EditSupplier", "SupplierController", "Tuvimos un problema al cargar los datos del proveedor.");
                return null;
            }
        }


        /// <summary>
        /// Metodo para actualizar los datos del proveedor
        /// </summary>
        /// <param name="model">Modelo que contiene la información del proveedor</param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> UpdateSupplier(SupplierViewModel model)
        {
            try
            {

                await supplierService.EditSupplier(model.Supplier);
                var task = await supplierService.Savechanges();

                return Json(new
                {
                    message = ((task.ExecutedSuccesfully) ? "Proveedor actualizado correctamente" : task.MessageList[0]),
                    status = task.ExecutedSuccesfully,
                    redirect = Url.Action("SuppliersList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateSupplier", "SupplierController", "Problemas al actualizar la información del proveedor");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }


        /// <summary>
        /// Metodo para actualizar el estatus del proveedor
        /// </summary>
        /// <param name="status">Estado al que pasará el proveedor</param>
        /// <param name="Id">Primary key del proveedor</param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> StatusSupplier(bool status, long Id)
        {
            try
            {
                Services.Support.SessionViewModel session = securityService.GetSession();
                supplierService.StatusSupplier(status, Id, session.BusinessId);

                var task = await supplierService.Savechanges();

                return Json(new
                {
                    message = ((task.ExecutedSuccesfully) ? "Proveedor actualizado correctamente" : task.MessageList[0]),
                    status = task.ExecutedSuccesfully,
                    redirect = Url.Action("SuppliersList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "StatusCustomer", "SupplierController", "Problema al actualizar la información del proveedor");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }

        /// <summary>
        /// Metodo para guardar la foto del empleado en el sistema
        /// </summary>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> SaveUploadedPicture(IFormFile files)
        {
            try
            {
                var session = securityService.GetSession();
                string guid = Guid.NewGuid().ToString();
                string url = string.Empty;

                TblTempFile file = new TblTempFile();
                file.FileName = guid + files.FileName;
                file.TypeFile = (byte)TypeFiles.SupplierPhoto;
                file.UserId = session.UserId;

                MemoryStream ms = new MemoryStream();
                files.CopyTo(ms);
                await globalService.AddTempFile(file, ms);

                var task = await globalService.Savechanges();

                if (task.ExecutedSuccesfully)
                    url = await globalService.GetTempUrlObjet(file.FileName);


                return Json(new
                {
                    message = ((task.ExecutedSuccesfully) ? "Foto cargada correctamente" : task.MessageList[0]),
                    File = url,
                    fileName = file.FileName,
                    status = task.ExecutedSuccesfully
                });

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "SaveUploadedPicture", "Supplier", "Se presento un problema al cargar la foto");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }


        /// <summary>
        /// Metodo para cargar las imagenes que usa el formulario
        /// </summary>
        /// <param name="model"></param>
        private async Task LoadPictures(SupplierViewModel model)
        {
            if (model.Supplier != null && !string.IsNullOrEmpty(model.Supplier.Picture))
            {
                if (!string.IsNullOrEmpty(model.Supplier.Picture))
                    model.UrlPhoto = await amazonService.GetUrlObjet(model.Supplier.Picture, TypeFiles.SupplierPhoto);
            }
            else
            {
                var productPicture = await globalService.GetSettingValue("amazon", "ProfileDefault");
                model.UrlPhoto = productPicture.FirstOrDefault().SettingValue;
            }
        }
    }
}