﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Procedure;
using QuickSal.Services;
using QuickSal.Services.Support;
using QuickSal.Web.Api;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.Warehouse;

namespace QuickSal.Web.Controllers
{

    public class MovingWareController : Controller
    {
        private IInventoryService inventoryService;
        private ISecurityService securityService;
        private IServiceError serviceError;
        private IGlobalService globalService;
        private IGenericMethod genericService;


        public MovingWareController(IInventoryService inventoryService, ISecurityService securityService, IServiceError serviceError, IGlobalService globalService, IGenericMethod genericService)
        {
            this.inventoryService = inventoryService;
            this.securityService = securityService;
            this.serviceError = serviceError;
            this.globalService = globalService;
            this.genericService = genericService;

        }

        /// <summary>
        /// Metodo para cargar la vista con el listado de movimineto de almancen
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<IActionResult> MovingWareList()
        {
            try
            {

                var session = securityService.GetSession();
                MovingWareVM model = new MovingWareVM();

                model.tblMovingWarehouses = inventoryService.GetMovingWarehouse(session.BranchOfficeId);
                var wareHouses = await inventoryService.GetWarehouses(session.BranchOfficeId);

                var warehouseNameList = wareHouses.Select(x => x.Name).ToList();
                model.MovingWareSender = model.tblMovingWarehouses.Where(x => warehouseNameList.Contains(x.SenderWarehouseId) && (x.State == (int)DataAcces.QuickSalEnum.MovingStatus.In_Procces || x.State == (int)DataAcces.QuickSalEnum.MovingStatus.In_Transit));
                model.MovingWareReciver = model.tblMovingWarehouses.Where(x => warehouseNameList.Contains(x.ReceiverWarehouseId)).Where(x => x.State == (int)DataAcces.QuickSalEnum.MovingStatus.In_Transit);
                model.tblMovingWarehouses = model.tblMovingWarehouses.Where(x => warehouseNameList.Contains(x.SenderWarehouseId) && (x.State == (int)DataAcces.QuickSalEnum.MovingStatus.Received || x.State == (int)DataAcces.QuickSalEnum.MovingStatus.Canceled));

                return View(model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CustomersList", "CustomerController", "Error intentando carga la vista de clientes.");
                return null;
            }

        }

        [ServiceFilter(typeof(HandledSecurityFilter))]
        // GET: MovingWare/Details/5
        public IActionResult MovingWareDetails(long Id, bool sender)
        {
            var model = new MovingWareVM();
            model.MovingWarehouse = inventoryService.GetMovingDetails(Id);

            var session = securityService.GetSession();

            model.Warehouses = inventoryService.GetAllWarehouses(session.BranchOfficeId);

            ViewBag.Sender = sender;

            return View(model);

        }

        /// <summary>
        /// Metodo para llamar el detalle del producto
        /// </summary>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        // GET: MovingWare/Details/5
        public JsonResult DetailsRows(long MovingId)
        {
            try
            {

                //Información que se le pasa desde el controller al datatable.js ejemplo

                GenericController genericController = new GenericController();

                var productList = inventoryService.DatetailsRows(MovingId);

                var restultado = productList.ToList().Select(x => new PrGetMovingWareDetails
                {
                    Id = x.Id,
                    Amount = x.Amount,
                    ProductId = x.ProductId,
                    StateDescripcion = genericController.GetListDesc((DataAcces.QuickSalEnum.MovingStatus)x.State),
                    Product = x.ProductName,
                    Delete = "<button type=\"button\" data-id=\"" + x.Id + "\" class=\"btn btn-default btn-icon " + ((x.Moving.State == (int)DataAcces.QuickSalEnum.MovingStatus.In_Procces) ? "deleteRow" : "") + "\"><i class=\"icon-trash\"></i></button>",
                    Editar = "<button type=\"button\"  data-name=\"" + x.ProductName + "\" data-product=\"" + x.ProductId + "\" data-id=\"" + x.MovingId + "\" class=\"btn btn-default btn-icon Editar\"><i class=\"icon-pencil6\"></i></button>",
                });


                return Json(new
                {
                    prices = globalService.ConvertToArray(restultado,
                    new string[] { "Id", "ProductId", "Product", "Amount", "StateDescripcion", "Delete", "Editar" })
                });


            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CustomersList", "CustomerController", "Error intentando cargar la vista de clientes.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }


        /// <summary>
        /// Metodo para eliminar un poducto del movimiento
        /// </summary>
        /// <returns></returns>
        // POST: MovingWare/Delete
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult DeleteDetailsRow(long MovingRowId)
        {
            try
            {
                inventoryService.DeleteMovingIdRow(MovingRowId);
                inventoryService.Savechanges();
                return Json(true);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CustomersList", "CustomerController", "Error intentando cargar la vista de clientes.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }

        }


        /// <summary>
        /// Metodo para devolver el producto.
        /// </summary>
        /// <returns></returns>
        public JsonResult ReturnProduct(PrGetMovingWareDetails model, int estado)
        {

            try
            {
                inventoryService.ReturnProduct(model, estado);
                inventoryService.Savechanges();
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Producto retornado correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CustomersList", "CustomerController", "Error intentando retornar el producto.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }


        /// <summary>
        /// Metodo para actualizar los datos del detalle de movimiento de almacen
        /// </summary>
        /// <returns></returns>
        // POST: MovingWareRow/Update
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult UpdateDetailsRow(PrGetMovingWareDetails PrGetMovingWareDetails)
        {
            try
            {
                inventoryService.UpdateMovingWareRow(PrGetMovingWareDetails);
                inventoryService.Savechanges();
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Producto modificado correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CustomersList", "CustomerController", "Error intentando actualizar los datos.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }


        /// <summary>
        /// Metodo para actualizar un producto de un movimiento de almacen
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        // GET: MovingWare/Editar/5
        public PartialViewResult _EditProduct(long MovingId, long ProductId)
        {


            var producto = inventoryService.ProductRow(MovingId, ProductId);


            return PartialView(producto);
        }


        /// <summary>
        /// Metodo para llamar la vista de crear el movimiento de almacen
        /// </summary>
        /// <returns></returns>
        // GET: MovingWare/Create
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult CreateMovingWare()
        {

            return View();
        }

        /// <summary>
        /// Metodo para guardar el moviemnto de almacen
        /// </summary>
        /// <returns></returns>
        // POST: MovingWare/Create
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult CreateMovingWareSave(MovingWareVM movingWareVM)
        {
            try
            {
                inventoryService.CreateMovingWare(movingWareVM.MovingWarehouse, movingWareVM.tipoProceso);
                inventoryService.Savechanges();
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Movimiento realizado correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("MovingWareList", "MovingWare")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateMovingWareSave", "MovingWareController", "se presento un problema al crear movimiento");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }


        /// <summary>
        /// Metodo para actualizar el movimiento de almacen
        /// </summary>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult UpdateMovingWare(TblMovingWarehouse MovingWarehouse)
        {
            try
            {
                inventoryService.UpdateMovingWare(MovingWarehouse);
                inventoryService.Savechanges();
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Movimiento modificado correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
            catch (Exception ex)
            {

                serviceError.LogError(ex.Message, "CreateMovingWareSave", "MovingWareController", "se presento un problema al modificar el movimiento");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }

        }


        /// <summary>
        /// Metodo para actualizar el estado del movimiento de almacen
        /// </summary>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult ChangeStatu(long MovingWarehouseId, int estado)
        {
            try
            {

                inventoryService.ChangeStatu(MovingWarehouseId, estado);
                inventoryService.Savechanges();
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Movimiento modificado correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
            catch (Exception ex)
            {

                serviceError.LogError(ex.Message, "CreateMovingWareSave", "MovingWareController", "se presento un problema al modificar el movimiento");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }

        }


        /// <summary>
        /// Metodo para obtener todos los productos de un almacen
        /// </summary>
        /// <returns></returns>
        // GET: Products/Inventory
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult GetProducts(long WareHouseId, long ProductId)
        {
            var listado = inventoryService.GetProducts(WareHouseId, ProductId);

            return Json(listado);
        }

        /// <summary>
        /// Metodo para llamar todos los almacenes de una empresa
        /// </summary>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult GetAllWarehouse()
        {
            var session = securityService.GetSession();

            var wareHouse = inventoryService.GetWarehouses(session.BranchOfficeId);
            var allWareHouse = inventoryService.GetAllWarehouses(session.BranchOfficeId);

            return Json(new { data = wareHouse, allWareHouse });
        }


    }
}