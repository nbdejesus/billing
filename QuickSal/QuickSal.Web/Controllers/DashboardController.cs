﻿using Microsoft.AspNetCore.Mvc;
using QuickSal.DataAcces.Procedure;
using QuickSal.Services;
using QuickSal.Web.Api;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.Web.Controllers
{
    public class DashboardController : Controller
    {
        private IGenericMethod _genericMethod;
        private IBusinessService businessService;
        private IServiceError serviceError;
        private IEmployeeService employeeService;
        private ISecurityService securityService;
        private IDashboardServices dashboardServices;
        private IAmazonService amazonService;
        private IGlobalService globalService;

        public DashboardController(IGenericMethod genericMethod, IGlobalService globalService,
                                   IBusinessService BusinessService,
                                   IServiceError serviceError,
                                   IEmployeeService employeeService,
                                   ISecurityService securityService, IAmazonService amazonService,
                                   IDashboardServices dashboardServices)
        {
            this._genericMethod = genericMethod;
            this.businessService = BusinessService;
            this.serviceError = serviceError;
            this.employeeService = employeeService;
            this.securityService = securityService;
            this.dashboardServices = dashboardServices;
            this.amazonService = amazonService;
            this.globalService = globalService;
        }

        [ServiceFilter(typeof(LoginSecurityFilter))]
        public async Task<IActionResult> DashboardPanel()
        {
            var session = securityService.GetSession();
            PanelViewModel model = new PanelViewModel();

            var business = employeeService.GetBusiness(session.UserId);
            model.BusinessList = business;


            await _genericMethod.CreateSession(this, session.UserId);
            ViewData["clear"] = true;//Almacenamos la primera carga

            ViewBag.UrlFormat = await getCoverLogin();

            return View(model);
        }

        [ServiceFilter(typeof(LoginSecurityFilter))]
        public async Task<IActionResult> DashboardBusinessPrincipal(long BusinessId)
        {
            try
            {
                var sesion = securityService.GetSession();
                long EmployeeId = employeeService.GetEmployeeId(sesion.UserId, BusinessId);
                long branchOfficeId = businessService.GetPrincipalBranchOfficeId(BusinessId);

                await _genericMethod.CreateSession(this, sesion.UserId, BusinessId, EmployeeId: EmployeeId, branchOffie: branchOfficeId);

                return View();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "DashboardBusinessPrincipal", "DashboardController", "Error intentando acceder a los datos de tu empresa.");
                return View();
            }
        }

        [ServiceFilter(typeof(LoginSecurityFilter))]
        public DashboardViewModel GetCompras()
        {
            var model = new DashboardViewModel();
            try
            {
                var BusinessId = securityService.GetSession().BusinessId;
                DateTime thisDay = DateTime.Today;
                var day = thisDay.ToString("d");
                var year = Convert.ToInt32(day.Substring(day.Length - 4));
                var month = Convert.ToInt32(day.Substring(0, 1));
                var response = GetResponse(BusinessId, year, month);

                return response;
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        public DashboardViewModel GetResponse(long BusinessId, int year, int month)
        {

            var model = new DashboardViewModel();

            var getshoopping = dashboardServices.GetShopping(BusinessId).Count();
            if (getshoopping <= 0)
            {
                List<prc_Shopping> listShopping = new List<prc_Shopping>();
                listShopping.Add(new prc_Shopping { compras = 0 });
                model.Shopping = listShopping.ToList();
            }
            else
            {
                model.Shopping = dashboardServices.GetShopping(BusinessId).ToList();
            }
            var sales = dashboardServices.GetSSales(BusinessId).Count();
            if (sales <= 0)
            {
                List<prc_Sales> listSales = new List<prc_Sales>();
                listSales.Add(new prc_Sales { Sales = 0, Numero_Mes = 1 });
                model.sales = listSales.ToList();
            }
            else
            {
                model.sales = dashboardServices.GetSSales(BusinessId).ToList();
            }
            var Profits = dashboardServices.GetProfits(BusinessId, DateTime.Now.Month, DateTime.Now.Year).Count();
            if (Profits <= 0)
            {
                List<prc_Profits> listProfits = new List<prc_Profits>();
                listProfits.Add(new prc_Profits { Profit = 0, month = 1 });
                model.Profits = listProfits.ToList();
            }
            else
            {
                
                model.Profits = dashboardServices.GetProfits(BusinessId, DateTime.Now.Month, DateTime.Now.Year).ToList();
            }
            var expense = dashboardServices.GetExpenses(BusinessId, DateTime.Now.Month, DateTime.Now.Year).Count();
            if (expense <= 0)
            {
                List<prc_expenses> listExpenses = new List<prc_expenses>();
                listExpenses.Add(new prc_expenses { Gasto = 0, Numero_Mes = 1 });
                model.expenses = listExpenses.ToList();
            }
            else
            {
                model.expenses = dashboardServices.GetExpenses(BusinessId, DateTime.Now.Month, DateTime.Now.Year).ToList();
            }

            var Sales_For_Months = dashboardServices.Getsp_Sales_for_Month(BusinessId, DateTime.Now.Year).Count();
            if (Sales_For_Months <= 0)
            {
                List<sp_Sales_for_Month> listSalesMonth = new List<sp_Sales_for_Month>();
                listSalesMonth.Add(new sp_Sales_for_Month { Ventas = 0, año = 1 });
                model.sp_Sales_For_Months = listSalesMonth.ToList();
            }
            else
            {
                model.sp_Sales_For_Months = dashboardServices.Getsp_Sales_for_Month(BusinessId,  DateTime.Now.Year).ToList();
            }

            var Sales_For_Month_Products = dashboardServices.Getsp_Sales_for_Month_products(BusinessId,  DateTime.Now.Year, DateTime.Now.Month).Count();
            if (Sales_For_Month_Products <= 0)
            {
                List<sp_Sales_for_Month_products> sp_Sales_for_Month_products = new List<sp_Sales_for_Month_products>();
                sp_Sales_for_Month_products.Add(new sp_Sales_for_Month_products { Sales = 0, months = "1", Name = "Productos" });
                model.Sales_For_Month_Products = sp_Sales_for_Month_products.ToList();
            }
            else
            {
                model.Sales_For_Month_Products = dashboardServices.Getsp_Sales_for_Month_products(BusinessId, DateTime.Now.Year, DateTime.Now.Month).ToList();
            }
            var month_seller = dashboardServices.Getsp_Sales_for_Month_seller(BusinessId, DateTime.Now.Year, DateTime.Now.Month).Count();

            if (month_seller <= 0)
            {
                List<prc_sp_Sales_for_Month_seller> Sales_for_Month_seller = new List<prc_sp_Sales_for_Month_seller>();
                Sales_for_Month_seller.Add(new prc_sp_Sales_for_Month_seller { Sales = 0, months = "1", Name = "Ventas" });
                model.prc_Sp_Sales_For_Month_Sellers = Sales_for_Month_seller.ToList();
            }
            else
            {
                model.prc_Sp_Sales_For_Month_Sellers = dashboardServices.Getsp_Sales_for_Month_seller(BusinessId, DateTime.Now.Year, DateTime.Now.Month).ToList();
            }
            var month_BranchOffices = dashboardServices.GetSales_for_Month_branchOffice(BusinessId, DateTime.Now.Year, DateTime.Now.Month).Count();
            if (month_BranchOffices <= 0)
            {
                List<prc_Sales_for_Month_branchOffice> listBranchOffices = new List<prc_Sales_for_Month_branchOffice>();
                listBranchOffices.Add(new prc_Sales_for_Month_branchOffice { Sales = 0, months = "1", Name = "Oficinas" });
                model.Sales_For_Month_BranchOffices = listBranchOffices.ToList();
            }
            else
            {
                model.Sales_For_Month_BranchOffices = dashboardServices.GetSales_for_Month_branchOffice(BusinessId, DateTime.Now.Year, DateTime.Now.Month).ToList();
            }
            var Month_Customer = dashboardServices.GetSales_for_Month_Customer(BusinessId, DateTime.Now.Year, DateTime.Now.Month).Count();
            if (Month_Customer <= 0)
            {
                List<pr_sp_Sales_for_Month_Customer> Month_Customers = new List<pr_sp_Sales_for_Month_Customer>();
                Month_Customers.Add(new pr_sp_Sales_for_Month_Customer { Sales = 0, Names = "Clientes" });
                model.Sales_for_Month_Customer = Month_Customers.ToList();

            }
            else
            {
                model.Sales_for_Month_Customer = dashboardServices.GetSales_for_Month_Customer(BusinessId, DateTime.Now.Year, DateTime.Now.Month).ToList();
            }
            return model;
        }


        /// <summary>
        /// Metodo para obtener las imagenes de fondo para el login
        /// </summary>
        /// <returns></returns>
        private async Task<string> getCoverLogin()
        {
            var FilesUrl = await globalService.GetSettingValue("Amazon", "ConverLogin");
            var urls = FilesUrl.ToArray();
            Random d = new Random();

            int randon = d.Next(1, urls.Count());
            return urls[randon].SettingValue;
        }
    }
}