﻿namespace QuickSal.Web.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using QuickSal.DataAcces.Contexts;
    using QuickSal.DataAcces.Models;
    using QuickSal.Services;
    using QuickSal.Web.Infraestructure.Security;
    using QuickSal.Web.ViewModels.DebitNote;
    using QuickSal.Web.ViewModels.Purchase;
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    public class DebitNoteController : Controller
    {

        private IServiceError serviceError;
        private ISecurityService securityService;
        private IAccountingService accountingService;
        private ISupplierService supplierService;
        private IInventoryService inventoryService;
        private IGlobalService globalService;
        private IBusinessService businessService;


        public DebitNoteController(IServiceError serviceError, ISecurityService securityService, IAccountingService accountingService, ISupplierService supplierService, IInventoryService inventoryService, IGlobalService globalService, IBusinessService businessService)
        {
            this.serviceError = serviceError;
            this.securityService = securityService;
            this.accountingService = accountingService;
            this.supplierService = supplierService;
            this.inventoryService = inventoryService;
            this.globalService = globalService;
            this.businessService = businessService;
        }


        /// <summary>
        /// Metodo para mostrar el listado de nota de debito
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult ListDebitNote()
        {
            var session = securityService.GetSession();
            DebitNote model = new DebitNote();

            model.DebitNotes = supplierService.ListDebitNote(session.BranchOfficeId).OrderByDescending(x => x.DocumentEstatusId);

            return View(model);
        }

        /// <summary>
        /// Metodo para mostrar la nota de debito
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult GetDebitNoteDetails()
        {
            var session = securityService.GetSession();
            DebitNoteVM model = new DebitNoteVM();

            //model.tblDebitNotes = inventoryService.GetDebitNote(session.BranchOfficeId);

            return View(model);
        }


        /// <summary>
        /// Metodo para mostrar el listado de PurchaseOrder
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult GetListPurchaseOrder()
        {

            try
            {
                PurchaseViewModel Model = new PurchaseViewModel();
                var session = securityService.GetSession().BranchOfficeId;
                var branchOfficeId = Convert.ToInt64(session);
                Model.PurchaseOrders = supplierService.PurchaseOrders(branchOfficeId).Where(x => x.DocumentEstatusId == (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada && x.Balance > 0);
                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "PurchaseOrdersList", "PurchaseOrdersController", "Tuvimos un problema cargar listado de ordenes.");
                return null;
            }

        }

        /// <summary>
        /// Metodo para cargar la vista para el actualizar las notas de debito
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult CreateDebitNote(int Id)
        {
            try
            {
                var session = securityService.GetSession().BusinessId;
                var bussineId = Convert.ToInt64(session);
                DebitNoteVM Model = new DebitNoteVM();

                Model.PurchaseOrder = supplierService.GetPurchase(Id, bussineId);
                Model.TblBusiness = businessService.GetBusinessById(bussineId);
                Model.Supplierer = supplierService.GetSuppliers(bussineId).FirstOrDefault();

                return View(Model);

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EditPurchaseOrder", "PurchaseOrderController", "Tuvimos un problema al cargar los datos de la orden.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para Crear la nota de debito
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<IActionResult> CreateDebitNoteSave(TblDebitNote debitNote, string tipoProceso)
        {
            try

            {
                var sesion = securityService.GetSession();
                var respuesta = supplierService.CreateDebitNote(debitNote, tipoProceso);
                if (respuesta.ExecutedSuccesfully)
                {
                    var task = await supplierService.Savechanges();

                    if (task.ExecutedSuccesfully & tipoProceso == "Procesar")
                        task.ExecutedSuccesfully = supplierService.AddNcfDebitNote(sesion.BranchOfficeId, debitNote.Id);
                    return Json(new
                    {
                        message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Nota de debito registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                        status = serviceError.TaskStatus().ExecutedSuccesfully,
                        redirect = Url.Action("ListDebitNote")

                    });
                }
                else
                {
                    return Json(new
                    {
                        message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? respuesta.Messages() : serviceError.TaskStatus().MessageList[0]),
                        status = serviceError.TaskStatus().ExecutedSuccesfully,
                    });
                }
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateDebitNoteSave", "DebitNoteController", "Tuvimos un problema al registrar la nota.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        /// <summary>
        /// Metodo para cargar la vista para el actualizar las notas de debito
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult EditDebitNote(int Id)
        {
            try
            {
                var session = securityService.GetSession();
                var branchOfficeId = Convert.ToInt64(session.BranchOfficeId);
                DebitNoteVM Model = new DebitNoteVM();

                Model.DebitNotes = supplierService.DebitNoteDetails(Id);
                Model.Supplierer = supplierService.GetSuppliers(Convert.ToInt64(session.BusinessId)).Where(x => x.Id == Model.DebitNotes.SuppliersId).FirstOrDefault();
                ViewBag.PurchaseOrders = supplierService.PurchaseOrdersId(branchOfficeId, Model.DebitNotes.PurchaseOrderId).FirstOrDefault().DocumentNumber;

                //Cargamos las opciones desplegables
                return View(Model);

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EditPurchaseOrder", "PurchaseOrderController", "Tuvimos un problema al cargar los datos de la orden.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para cargar el modal que contiene el detalle de la orden de compra
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public IActionResult ModalPurchOrder()
        {
            return View();
        }
        /// <summary>
        /// Metodo  actualizar las notas de debito en la base datos
        /// </summary>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult UpdatedebitNote(TblDebitNote tblDebitNote, int tipoProceso)
        {
            try
            {
                var session = securityService.GetSession().BusinessId;
                var respuesta = supplierService.UpdateDebitNote(tblDebitNote, tipoProceso);

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Nota de debito actualizada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("ListDebitNote")

                });                
          
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdatedebitNote", "DebitNoteController", "Tuvimos un problema al actualizar la nota.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }


        /// Metodo para cargar el detalle de la orden
        /// </summary>
        /// <returns></returns>       
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public JsonResult GetPurchaseOrder(long PurchaseId = 0)
        {

            //Luego verifica los campos que retornar que te estan dando error
            var PurchaseList = supplierService.GetPurchOrderRows(PurchaseId).Select(x => new PurchaseViewModel
            {
                
                Articulos = x.Produc_Description,
                MeasureId = x.MeasureId,
                Almacen = x.WareHouse,
                PurchaseOrderId = x.PurchaseOrderId,
                AlmacenId = x.WarehouseId,
                ITBIS = x.Tax,
                ArticulosId = x.ProductId,
                cantidad = x.Disponible,
                compra = x.Cost,
                descuento = x.Discount,
                Impuesto = x.TaxAbbreviation,
                subTotal = x.Total,
                AmountTax = x.AmountTax,
                AmountDiscount = x.AmountDiscount,
                Total = ((x.Total + x.AmountTax) - x.AmountDiscount),
                Measure = x.Measure,
                Accion = "<ul class=\"icons-list\"><li class=\"dropdown\"><a href = \"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"icon-menu9\"></i></a>" +
                "<ul class=\"dropdown-menu dropdown-menu-right\"><li><a type=\"button\" class=\"btn btn-default btn-icon modificarProducto\" data-product=\"" + x.ProductId + "\" data-id=\"" + x.PurchaseOrderRowId + "\" ><i class=\"icon-pencil6\"></i>Editar</a><a type=\"button\" class=\"btn btn-default btn-icon deleteRow\"><i class=\"icon-trash\"></i>Eliminar</a></ul></li></ul>",
                Edit ="<div class=\"form-check\"><input type =\"checkbox\" class=\"form-check-input styled\" id=\"materialUnchecked\"><label class=\"form-check-label selected Agregar\" for=\"materialUnchecked\"></label></div>",//boton de editar     
                //"<div class=\"checkbox\"><label><input type =\"checkbox\" class=\"styled\"></label></div>"
            });


            //
            return Json(new
            {
                prices = globalService.ConvertToArray(PurchaseList.Where(x => x.cantidad > 0),
                new string[] { "Id", "Articulos", "Almacen", "cantidad", "compra", "descuento", "Impuesto", "subTotal", "ArticulosId", "AlmacenId", "ITBIS", "AmountTax", "MeasureId","Measure", "Accion", "Edit" })
            });
        }


        /// <summary>
        /// Metodo para mostrar el listado del row de nota de debito
        /// </summary>
        /// <returns></returns>    
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult GetDebitNoteRow(long id = 0)
        {
            var session = securityService.GetSession().BusinessId;
            var bussineId = Convert.ToInt64(session);
            //Luego verifica los campos que retornar que te estan dando error
            var DebitNoteRowList = inventoryService.GetDebitNoteRow(id);
       
            return Json(new
            {
                prices = globalService.ConvertToArray(DebitNoteRowList,
                new string[] {  "", "Product", "Warehouse", "Quantity",  "Discount", "Tax",  "ProductId","WarehouseId", "MeasureId","Measure", "TaxAbbreviation", "Cost", "AmountTax",
                    "AmountDiscount", "Total", "Accion" })
            });
        }


        /// <summary>
        /// Metodo para mostrar Modificar el detalle de la nota de debito
        /// </summary>
        /// <returns></returns>    
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult EditDebitNoteRow(long PurchaseOrderId = 0, long ProductId = 0)
        {

            //Luego verifica los campos que retornar que te estan dando error
            DebitNoteRowVM DebitNoteRowList = new DebitNoteRowVM();
            var validacion = supplierService.ValidarCantidadProducto(PurchaseOrderId, ProductId);
            DebitNoteRowList.CantidadProducto = validacion.Quantity;
            return View(DebitNoteRowList);
        }


        /// Metodo para cambiar el estado del PurchaseOrder
        /// </summary>
        /// <returns></returns>      
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> StatusDebitNote(int Id, long DocumentStatusID)
        {
            try
            {
                var sesion = securityService.GetSession();
                var respuesta = supplierService.SetDebitNotaStatus(Id, DocumentStatusID);
                if (respuesta.ExecutedSuccesfully)
                {
                    var task = await inventoryService.Savechanges();

                    if (task.ExecutedSuccesfully & DocumentStatusID == (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada)
                        task.ExecutedSuccesfully = supplierService.AddNcfDebitNote(sesion.BusinessId, Id);
                }

                else
                {
                    return Json(new
                    {
                        message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? respuesta.Messages() : serviceError.TaskStatus().MessageList[0]),
                        status = respuesta.ExecutedSuccesfully,
                    });
                }


                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" + "Nota de debito actualizada correctamente" : "desactivada") + "",
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("ListDebitNote")
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }



    }
}