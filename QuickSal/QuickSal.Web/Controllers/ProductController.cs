﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuickSal.DataAcces.Contexts;
using QuickSal.Services;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.Inventory.Product;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static QuickSal.DataAcces.QuickSalEnum;

namespace QuickSal.Web.Controllers
{
    public class ProductController : Controller
    {
        private IServiceError serviceError;
        private ISecurityService SecurityService;
        private IInventoryService inventoryService;
        private ISupplierService supplierService;
        private IAccountingService accountingService;
        private IGlobalService globalService;
        private IAmazonService amazonService;

        public ProductController(IServiceError serviceError, ISupplierService supplierService,
                                ISecurityService SecurityService, IAccountingService accountingService,
                                IInventoryService inventoryService, IGlobalService globalService, IAmazonService amazonService)
        {
            this.serviceError = serviceError;
            this.SecurityService = SecurityService;
            this.inventoryService = inventoryService;
            this.supplierService = supplierService;
            this.accountingService = accountingService;
            this.globalService = globalService;
            this.amazonService = amazonService;
        }


        /// <summary>
        /// Metodo para cargar el listado de productos de una empresa
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult ProductList()
        {
            try
            {
                ProductViewModel Model = new ProductViewModel();
                var session = SecurityService.GetSession();

                Model.Products = inventoryService.GetProductsInvoice(session.BusinessId);
                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "ProductList", "ProductController", "Tuvimos un problema cargar listado de productos.");
                return null;
            }
        }


        /// <summary>
        /// Metodo para cargar la vista de registro de producto
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<IActionResult> ProductIndex()
        {
            try
            {
                ProductViewModel Model = new ProductViewModel();
                var session = SecurityService.GetSession();

                await LoadPictures(Model);

                Model.Suppliers = supplierService.GetSuppliers(session.BusinessId);
                Model.Categories = inventoryService.GetCategories(session.BusinessId);
                Model.Measures = inventoryService.GetMeasurements(session.BusinessId);
                Model.Taxes = accountingService.GetTaxes(session.BusinessId);
                Model.Warehouses = inventoryService.GetBusinessWarehouses(session.BusinessId);

                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "ProductCreate", "ProductController", "Tuvimos un problema cargar listado de productos.");
                return null;
            }
        }


        /// <summary>
        /// Metodo para registrar un producto
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> CreateProduct(ProductViewModel model)
        {
            try
            {
                await inventoryService.CreateProduct(model.Product);
                await inventoryService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Producto registrado correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("ProductList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateProduct", "Product", "Tuvimos un problema al registrar el producto.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "producto registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }


        /// <summary>
        /// Metodo para cargar la vista de actualización de producto
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<IActionResult> EditProduct(long Id)
        {
            try
            {
                ProductViewModel Model = new ProductViewModel();
                var session = SecurityService.GetSession();

                //datos del producto
                Model.Product = inventoryService.GetProduct(Id);

                Model.Suppliers = supplierService.GetSuppliers(session.BusinessId);
                Model.Categories = inventoryService.GetCategories(session.BusinessId);
                Model.Measures = inventoryService.GetMeasurements(session.BusinessId);
                Model.Taxes = accountingService.GetTaxes(session.BusinessId);
                Model.Warehouses = inventoryService.GetBusinessWarehouses(session.BusinessId);

                await LoadPictures(Model);

                if (Model.Product.SubCategoryId.HasValue)
                    Model.SubCategories = inventoryService.GetSubCategories(Model.Product.SubCategory.CategoryId);

                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EditProduct", "ProductController", "Tuvimos un problema cargar el producto.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para actualizar la información de un producto
        /// </summary>
        /// <param name="model">Modelo que contiene la información</param>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult UpdateProduct(ProductViewModel model)
        {
            try
            {
                inventoryService.UpdateProduct(model.Product);
                inventoryService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Producto actualizado correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("ProductList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateProduct", "Product", "Tuvimos un problema al actualizar el producto.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }


        /// <summary>
        /// Metodo para obtener todas las provincias de un pais
        /// </summary>
        /// <param name="CountryId">Primary key del pais</param>
        /// <returns></returns>
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public JsonResult GetAllSubCategories(int CategoryId)
        {
            var SubCategoryList = inventoryService.GetSubCategories(CategoryId);
            return Json(new { subCategoryList = SubCategoryList });
        }

        /// <summary>
        /// Metodo para obtener el listado de precios de un prodcuto
        /// </summary>
        /// <param name="ProductId"></param>
        /// <returns></returns>
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public JsonResult GetProductPrice(long ProductId = 0)
        {
            var productList = inventoryService.GetProductsPrices(ProductId).Select(x => new PricesViewModel
            {
                Id = x.Id,
                TaxName = x.Tax.Abbreviation,
                PriceName = x.Name,
                Utility = x.Utility,
                Price = x.Price.ToString("N2"),
                TotalPrice = ((x.Price * (x.Tax.Quantity / 100)) + x.Price).ToString("N2"),
                Principal = "<span class=\"label label-" + ((x.IsDefault) ? "success" : "info") + "\">" + ((x.IsDefault) ? "Si" : "No") + "</span>",
                Delete = "<button type=\"button\" class=\"btn btn-default btn-icon deleteRow\"><i class=\"icon-trash\"></i></button>",
                TaxId = x.TaxId,
                IsPrincipal = x.IsDefault
            });


            return Json(new
            {
                prices = globalService.ConvertToArray(productList,
                new string[] { "TaxName", "PriceName", "Utility", "Price", "TotalPrice", "Principal", "Delete", "TaxId", "IsPrincipal", "Id" })
            });
        }

        /// <summary>
        /// Proceso para guardar la foto del producto en el sistema
        /// </summary>
        /// <returns></returns>
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public async Task<JsonResult> SaveUploadedPicture(IFormFile files)
        {
            try
            {
                var session = SecurityService.GetSession();
                string guid = Guid.NewGuid().ToString();
                string url = string.Empty;

                TblTempFile file = new TblTempFile();
                file.FileName = guid + files.FileName;
                file.TypeFile = (byte)TypeFiles.Product;
                file.UserId = session.UserId;

                MemoryStream ms = new MemoryStream();
                files.CopyTo(ms);
                await globalService.AddTempFile(file, ms);

                var task = await globalService.Savechanges();

                if (task.ExecutedSuccesfully)
                    url = await globalService.GetTempUrlObjet(file.FileName);


                return Json(new
                {
                    message = ((task.ExecutedSuccesfully) ? "Foto cargada correctamente" : task.MessageList[0]),
                    File = url,
                    fileName = file.FileName,
                    status = task.ExecutedSuccesfully
                });

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "SaveUploadedPicture", "Product", "Se presento un problema al cargar la foto");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }


        /// <summary>
        /// Metodo para actualizar el estatus del producto
        /// </summary>
        /// <param name="status">Estado al que pasará el producto</param>
        /// <param name="Id">Primary key del producto</param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> StatusProduct(bool status, long Id)
        {
            try
            {
                var session = SecurityService.GetSession();
                inventoryService.StatusProduct(status, Id, session.BusinessId);

                var task = await inventoryService.Savechanges();

                return Json(new
                {
                    message = ((task.ExecutedSuccesfully) ? "Producto actualizado correctamente" : task.MessageList[0]),
                    status = task.ExecutedSuccesfully,
                    redirect = Url.Action("ProductList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "StatusCustomer", "CustomerController", "se presento un problema al actualizar la información del cliente");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }



        /// <summary>
        /// Metodo para cargar las imagenes que usa el formulario
        /// </summary>
        /// <param name="model"></param>
        private async Task LoadPictures(ProductViewModel model)
        {
            if (model.Product != null && !string.IsNullOrEmpty(model.Product.Picture))
            {
                if (!string.IsNullOrEmpty(model.Product.Picture))
                    model.UrlProduct = await amazonService.GetUrlObjet(model.Product.Picture, TypeFiles.Product);
            }
            else
            {
                var productPicture = await globalService.GetSettingValue("amazon", "ProductDefault");
                model.UrlProduct = productPicture.FirstOrDefault().SettingValue;
            }
        }
    }
}