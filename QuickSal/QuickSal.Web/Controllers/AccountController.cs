﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using QuickSal.DataAcces;
using QuickSal.DataAcces.Contexts;
using QuickSal.Services;
using QuickSal.Services.Services;
using QuickSal.Services.Support;
using QuickSal.Web.Api;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.Account;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace QuickSal.Web.Controllers
{
    public class AccountController : GenericController
    {
        private IAccountService accountService;
        private IServiceError serviceError;
        private IConfiguration configurationService;
        private INotificationsService notificationsService;
        private IGenericMethod genericMethodService;
        private IGlobalService globalService;
        private IHostingEnvironment _env;
        private ISecurityService securityService;
        private IAmazonService amazonService;

        public AccountController(IAccountService accountingService, IAmazonService amazonService,
        IServiceError serviceError,
                                 IConfiguration configurationService,
                                 IHttpContextAccessor httpContextAccessor,
                                 INotificationsService Notifications,
                                 IGenericMethod genericMethodService,
                                 IGlobalService globalService,
                                 IHostingEnvironment env,
                                 ISecurityService securityService
                                 )
        {
            this.accountService = accountingService;
            this.serviceError = serviceError;
            this.configurationService = configurationService;
            this.genericMethodService = genericMethodService;
            this._env = env;
            this.notificationsService = Notifications;
            this.globalService = globalService;
            this.securityService = securityService;
            this.amazonService = amazonService;
        }

        public async Task<IActionResult> UserAccount()
        {
            ViewBag.Hide = true;
            ViewBag.UrlFormat = await getCoverLogin();

            var site_Key = await globalService.GetSettingValue("GoogleReCaptcha", "site_Key");
            ViewBag.GoogleReCaptcha_site_Key = site_Key.FirstOrDefault().SettingValue;
            return View();
        }

        /// <summary>
        /// Vista para Mostrar el acceso denegado
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public IActionResult AccessDenied()
        {
            return View();
        }

        /// <summary>
        /// Metodo para crear un usuario en el sistema
        /// </summary>
        /// <param name="Model">Modelo con la informacion</param>
        /// <returns></returns>

        [ValidateAntiForgeryToken]
        public async Task<JsonResult> UserCreate(AccountViewModel Model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //Creamos el usuario
                    accountService.CreateUser(Model.User);
                    await accountService.Savechanges();

                    if (serviceError.TaskStatus().ExecutedSuccesfully)
                    {
                        //try
                        //{
                        //    Mail mail = new Mail();
                        //    mail.To = Model.User.Email;
                        //    mail.Subject = "Bienvenido a Quicksal";
                        //    string userConfirmation = Model.User.TblUserConfirmation.FirstOrDefault().ConfirmationKey;

                        //    mail.Body = notificationsService.WelcomeToQuicksal(Url.Action("AccountConfirmation", "Account", new { UriSalt = userConfirmation }, "http"));

                        //    notificationsService.SendMailConfirmation(mail);
                        //}
                        //catch (Exception ex)
                        //{
                        //    serviceError.LogError(ex.Message, "SendMailConfirmation", "AccountController");
                        //}
                        //finally
                        //{
                        await genericMethodService.CreateSession(this, Model.User.Id);
                        //}
                    }
                }
                catch (Exception ex)
                {
                    serviceError.LogError(ex.Message, "CreateUser", "AccountController", "Error intentando crear los datos del usuario, por favor intentelo en unos minutos.");
                }
            }
            else
            {
                serviceError.LogError("Faltan datos para poder guardar el usuario.", "CreateUser", "AccountController", "Error intentando crear los datos del usuario, por favor intentelo en unos minutos.");
            }

            var task = serviceError.TaskStatus();

            return Json(new
            {
                message = ((task.ExecutedSuccesfully) ? "Usuario registrado correctamente" : task.MessageList[0]),
                status = task.ExecutedSuccesfully
            });
        }

        /// <summary>
        /// Metodo para iniciar sesion del usuario
        /// </summary>
        /// <param name="Model">Modelo con la informacion</param>
        /// <returns></returns>   
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> UserLogin(AccountViewModel Model)
        {
            Model.User.HaveFacebookEmail = false;

            await Login(Model);

            return Json(new
            {
                message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Pasando al panel...." : serviceError.TaskStatus().MessageList[0]),
                status = serviceError.TaskStatus().ExecutedSuccesfully
            });
        }


        /// <summary>
        /// Metodo para iniciar sesion
        /// </summary>
        /// <param name="model"></param>
        private async Task Login(AccountViewModel model)
        {
            var user = await accountService.Login(model.User);

            if (serviceError.TaskStatus().ExecutedSuccesfully)
                await genericMethodService.CreateSession(this, user.Id);
        }

        /// <summary>
        /// Metodo para cerrar la sesion de un usuario
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public IActionResult Logout()
        {
            genericMethodService.RemoveSession(this);
            return RedirectToAction("UserAccount", "account");
        }

        [HttpPost]
        public async Task<ActionResult> RecapcahResponse(AccountViewModel Model)
        {
            var mess = "";
            try
            {
                var response = HttpContext.Request.Form["g-recaptcha-response"];
                var l = HttpContext.Request.PathBase;
                var site_Key = await globalService.GetSettingValue("GoogleReCaptcha", "secret_Key");
                var client = new WebClient();
                var result = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", site_Key.FirstOrDefault().SettingValue, response));
                var obj = JObject.Parse(result);
                var status = (bool)obj.SelectToken("success");
                return Json(status);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "RecapcahResponse", "AccountController", "Problema presentado al validar el captcha");
                return Json(mess);
            }

        }

        ///// <summary>
        ///// Metodo para confirmar la cuenta de un usuario desde su email
        ///// </summary>
        ///// <param name="UriSalt">Url con los parametros de confirmacion</param>
        ///// <returns></returns>
        [AllowAnonymous]
        public IActionResult AccountConfirmation()
        {
            return View();
            //try
            //{
            //    if (string.IsNullOrEmpty(UriSalt))
            //    {
            //        //_ApiService.AddError("Clave de confirmación no encontrada");
            //        return RedirectToAction("PageError", "home", new { errors = _ApiService.GetSumaryError() });
            //    }
            //    else
            //    {
            //        var confirmation = _User.ExistsConfirmationkey(UriSalt);

            //        if (confirmation == null)
            //        {
            //            _ApiService.AddError("Clave de confirmación no encontrada");
            //            return RedirectToAction("PageError", "home", new { errors = _ApiService.GetSumaryError() });
            //        }

            //        if (confirmation.ExpirationKey > DateTime.Now)
            //        {
            //            var User = _User.Get(confirmation.UserId);
            //            User.EmailConfirm = true;


            //            _User.Update(User);
            //            _User.ConfirmationDelete(confirmation);

            //            if (_User.Save())
            //            {
            //                //Levantando session
            //                DashboardViewModel Dashboard = new DashboardViewModel();

            //                Dashboard.UserId = User.Id;
            //                Dashboard.UserName = User.Name;
            //                Dashboard.UserPicture = User.Picture;
            //                Dashboard.UserEmail = User.Email;

            //                _genericMethod.CreateSession(this, Dashboard);            
            //                return Redirect("StatusConfirmation");
            //            }
            //            else
            //            {
            //                _ApiService.AddError("Tu correo no pudo ser validado correctamente.");
            //                return RedirectToAction("PageError", "home", new { errors = _ApiService.GetSumaryError() });
            //            }
            //        }
            //        else
            //        {
            //            //Eliminando llave obsoleta
            //            _User.ConfirmationDelete(confirmation);
            //            _User.Save();
            //            _ApiService.AddError("Clave de confirmación ha expirado, en el dashboard puedes solicitar otra clave");
            //            return RedirectToAction("PageError", "home", new { errors = _ApiService.GetSumaryError() });
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    _ApiService.logError(ex.Message, "DeleteKey", "Account", "Se presento un error al intentar validar la llave de confirmacion");
            //    return RedirectToAction("PageError", "home", new { errors = _ApiService.GetSumaryError() });
            //}
        }


        public IActionResult StatusConfirmation()
        {
            return View();
        }

        /// <summary>
        /// Metodo para cargar la vista para recuperar la contrasena
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> PasswordRecovery()
        {
            ViewBag.UrlFormat = await getCoverLogin();
            return View();
        }


        ///// <summary>
        ///// Proceso para enviar un correo desde donde se cambiara los datos del usuario
        ///// </summary>
        ///// <param name="email"></param>
        ///// <returns></returns>
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> SendChangeMail(string email)
        {
            try
            {
                string key = string.Empty;
                accountService.GenerateConfirmationKey(email, out key);

                string url = Url.Action("ChangePassword", "Account", new { UriSalt = key }, "http");
                var task = await notificationsService.SendMailChangePassword(url, "Cambio de contraseña", email);

                if (task.ExecutedSuccesfully)
                {
                    accountService.Savechanges();
                }

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Correo enviado, la clave de validación vence en 24 horas" : serviceError.TaskStatus().MessageList[0]),
                    redirect = Url.Action("UserAccount"),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "SendChangeMail", "AccountController", "Tuvimos un problema intentando recuperar cuenta.");

                return Json(new
                {
                    message = "Se presentaron problemas al momento de enviar el correo",
                    status = false
                });
            }
        }

        ///// <summary>
        ///// Cargando vista de cambio de contraseña
        ///// </summary>
        ///// <param name="UriSalt"></param>
        ///// <returns></returns>
        [HttpGet]
        public IActionResult ChangePassword(string UriSalt)
        {
            try
            {
                var task = accountService.ValidateRecoveryPassword(UriSalt);

                if (task.ExecutedSuccesfully)
                {
                    AccountViewModel Account = new AccountViewModel();
                    Account.RecoveryPassword = true;
                    ViewData["Key"] = UriSalt;
                    return View();
                }
                else
                    return RedirectToAction("PageError", "home",
                        new { errors = serviceError.TaskStatus().MessageList[0] });

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "RecoverPassword", "Account", "Se presento un error al intentar validar la llave de confirmación.");
                return RedirectToAction("PageError", "home", new { errors = serviceError.TaskStatus().MessageList[0] });
            }
        }


        ///// <summary>
        ///// Proceso para cambiar la contraseña de un usuario
        ///// </summary>
        ///// <param name="model"></param>
        ///// <returns></returns>  
        [ValidateAntiForgeryToken]
        public JsonResult ChangePassword(AccountViewModel model)
        {
            try
            {
                var task = accountService.ValidateRecoveryPassword(model.KeyConfirmation);

                if (task.ExecutedSuccesfully)
                {
                    accountService.ChangePasswordRecovery(model.User.UserPassword, model.KeyConfirmation);
                    accountService.Savechanges();
                }

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Contraseña actualizada" : serviceError.TaskStatus().MessageList[0]),
                    redirect = Url.Action("UserAccount"),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "ChangePassword", "AccountController", "Tuvimos un problema intentando recuperar cuenta.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para cargar la vista con los datos del usuario
        /// </summary>
        /// <returns></returns>
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public async Task<IActionResult> UserProfile()
        {
            try
            {
                var session = securityService.GetSession();
                ProfileViewModel model = new ProfileViewModel();

                model.User = accountService.GetUser(session.UserId);
                model.Countries = globalService.GetAllCountries();
                model.DocumentIdentifications = globalService.GetAllDocumentIdentification();
                await LoadPictures(model);


                if (model.User.CountryId.HasValue)
                    model.Provinces = globalService.GetAllProvince(model.User.CountryId.Value);

                return View(model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UserProfile", "AccountController", "Error cargando los datos del usuario");
                return View(null);
            }
        }

        /// <summary>
        /// Metodo para actualizar los datos del usuario
        /// </summary>
        /// <param name="model">Modelo con la informacion del usuario</param>
        /// <returns></returns>
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public async Task<JsonResult> UpdateUserData(ProfileViewModel model)
        {
            try
            {
                accountService.EditUser(model.User);
                var task = await accountService.Savechanges();

                return Json(new
                {
                    message = ((task.ExecutedSuccesfully) ? "Datos actualizados correctamente" : task.MessageList[0]),
                    status = task.ExecutedSuccesfully,
                    redirect = Url.Action("UserProfile")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateUserData", "accountController", "se presento un problema al actualizar información del cliente");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }

        /// <summary>
        /// Metodo para obtener todas las provincias de un pais
        /// </summary>
        /// <param name="CountryId">Primary key del pais</param>
        /// <returns></returns>
        public JsonResult GetAllProvince(int CountryId)
        {
            var provinceList = globalService.GetAllProvince(CountryId);
            return Json(new { ProvinceList = provinceList });
        }

        /// <summary>
        /// Metodo para guardar la foto del empleado en el sistema
        /// </summary>
        /// <returns></returns>
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public async Task<JsonResult> SaveUploadedPicture(IFormFile files)
        {
            try
            {
                var session = securityService.GetSession();
                string guid = Guid.NewGuid().ToString();
                string url = string.Empty;

                TblTempFile file = new TblTempFile();
                file.FileName = guid + files.FileName;
                file.TypeFile = (byte)TypeFiles.ProfilePhoto;
                file.UserId = session.UserId;

                MemoryStream ms = new MemoryStream();
                files.CopyTo(ms);
                await globalService.AddTempFile(file, ms);

                var task = await globalService.Savechanges();

                if (task.ExecutedSuccesfully)
                    url = await globalService.GetTempUrlObjet(file.FileName);

                return Json(new
                {
                    message = ((task.ExecutedSuccesfully) ? "Foto cargada correctamente" : task.MessageList[0]),
                    File = url,
                    fileName = file.FileName,
                    status = true
                });

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "SaveUploadedPicture", "accountController", "Problema al guardar la foto.");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }

        /// <summary>
        /// Metodo para obtener las imagenes de fondo para el login
        /// </summary>
        /// <returns></returns>
        private async Task<string> getCoverLogin()
        {
            var FilesUrl = await globalService.GetSettingValue("Amazon", "ConverLogin");
            var urls = FilesUrl.ToArray();
            Random d = new Random();

            int randon = d.Next(1, urls.Count());
            return urls[randon].SettingValue;
        }

        /// <summary>
        /// Metodo para cargar las imagenes que usa el formulario
        /// </summary>
        /// <param name="model"></param>
        private async Task LoadPictures(ProfileViewModel model)
        {
            if (model.User != null && !string.IsNullOrEmpty(model.User.Picture))
            {
                if (!string.IsNullOrEmpty(model.User.Picture))
                    model.UrlPhotoProfile = await amazonService.GetUrlObjet(model.User.Picture, QuickSalEnum.TypeFiles.ProfilePhoto);
            }
            else
            {
                var productPicture = await globalService.GetSettingValue("amazon", "ProfileDefault");
                model.UrlPhotoProfile = productPicture.FirstOrDefault().SettingValue;
            }
        }
    }
}