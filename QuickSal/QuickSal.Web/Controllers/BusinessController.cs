﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using QuickSal.DataAcces;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using QuickSal.Services;
using QuickSal.Web.Api;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.Business;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static QuickSal.DataAcces.QuickSalEnum;

namespace QuickSal.Web.Controllers
{
    public class BusinessController : Controller
    {
        private IBusinessService businessService;
        private IGlobalService globalService;
        private IGenericMethod _genericMethod;
        private IServiceError serviceError;
        private IConfiguration _configuration;
        private IHostingEnvironment _env;
        private IAccountingService accountingService;
        private ISecurityService SecurityService;
        private IPlanService planService;
        private IAmazonService amazonService;

        public BusinessController(IBusinessService businessService, IPlanService planService,
                                  IGlobalService globalService, IAmazonService amazonService,
                                  IGenericMethod GenericMethod,
                                  IServiceError serviceError,
                                  IConfiguration Configuration,
                                  IHostingEnvironment Env,
                                  IAccountingService accountingService,
                                  ISecurityService SecurityService)
        {
            this.businessService = businessService;
            this.globalService = globalService;
            this._genericMethod = GenericMethod;
            this.serviceError = serviceError;
            this._configuration = Configuration;
            this._env = Env;
            this.accountingService = accountingService;
            this.SecurityService = SecurityService;
            this.planService = planService;
            this.amazonService = amazonService;
        }

        #region Business
        /// <summary>
        /// Metodo para cargar la vista para crear la empresa
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public async Task<IActionResult> CreateBusiness()
        {
            try
            {
                BusinessViewModel model = new BusinessViewModel();
                model.BusinessTypeList = businessService.GetAllBusinessType();
                model.DocumentIdentificationList = globalService.GetAllDocumentIdentification();
                model.Countries = globalService.GetAllCountries();

                await LoadPictures(model);
                return View(model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateBusiness", "BusinessController", "Error intentando carga registro de empresa.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para cargar la vista para editar una empresa
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<IActionResult> EditBusiness()
        {
            try
            {
                var sesion = SecurityService.GetSession();
                BusinessViewModel model = new BusinessViewModel();
                model.BusinessTypeList = businessService.GetAllBusinessType();
                model.DocumentIdentificationList = globalService.GetAllDocumentIdentification();
                model.Countries = globalService.GetAllCountries();

                model.Business = businessService.GetBusinessById(sesion.BusinessId);
                model.BranchOffice = businessService.GetPrincipalBranchOffice(sesion.BusinessId);

                await LoadPictures(model);

                if (model.Business.CountryId.HasValue)
                    model.Provinces = globalService.GetAllProvince(model.Business.CountryId.Value);


                return View(model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EditBusiness", "BusinessController", "Tuvimos un problema al cargar los datos de tu empresa.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para actualizar los datos de una empresa
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public async Task<JsonResult> UpdateBusiness(BusinessViewModel model)
        {
            try
            {
                var sesion = SecurityService.GetSession();
                TaskResult task = await businessService.UpdateBusiness(model.Business);

                model.BranchOffice.IdBusiness = model.Business.Id;
                task = businessService.UpdateBranchOffice(model.BranchOffice);

                if (task.ExecutedSuccesfully)
                    await businessService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Empresa actualizada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateBusiness", "BusinessController", "Problemas al actualizar los datos.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        /// <summary>
        /// Metodo para la vista para crear una empresa
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public async Task<JsonResult> CreateBusiness(BusinessViewModel model)
        {
            try
            {
                model.Business.TblBranchOffice.Add(model.BranchOffice);
                await businessService.CreateBusiness(model.Business, model.PlanId);

                var task = businessService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Empresa registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("DashboardPanel", "Dashboard")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateBusiness", "BusinessController", "Tuvimos un problema al registrar tu empresa.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        /// <summary>
        /// Metodo para cargar el logo de la empresa
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public async Task<JsonResult> UploadLogo(IFormFile files)
        {
            try
            {
                var session = SecurityService.GetSession();
                string guid = Guid.NewGuid().ToString();
                string url = string.Empty;

                TblTempFile file = new TblTempFile();
                file.FileName = guid + files.FileName;
                file.TypeFile = (byte)TypeFiles.BusinessLogo;
                file.UserId = session.UserId;

                MemoryStream ms = new MemoryStream();
                files.CopyTo(ms);
                await globalService.AddTempFile(file, ms);

                var task = await globalService.Savechanges();

                if (task.ExecutedSuccesfully)
                    url = await globalService.GetTempUrlObjet(file.FileName);


                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Logo cargado correctamente." : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    FilePath = url,
                    FileName = file.FileName,
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UploadLogo", "BusinessController", "Error intentando subir el logo de la empresa.");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }



        /// <summary>
        /// Metodo para cargar las imagenes que usa el formulario
        /// </summary>
        /// <param name="model"></param>
        private async Task LoadPictures(BusinessViewModel model)
        {
            if (model.Business != null && !string.IsNullOrEmpty(model.Business.Logo))
            {
                if (!string.IsNullOrEmpty(model.Business.Logo))
                    model.UrlLogo = await amazonService.GetUrlObjet(model.Business.Logo, TypeFiles.BusinessLogo);
            }
            else
            {
                var productPicture = await globalService.GetSettingValue("amazon", "BusinessLogoDefault");
                model.UrlLogo = productPicture.FirstOrDefault().SettingValue;
            }
        }

        #endregion     
    }
}