﻿using Microsoft.AspNetCore.Mvc;
using QuickSal.Services;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.Warehouse;
using System;
using System.Threading.Tasks;

namespace QuickSal.Web.Controllers
{
    public class WarehouseController : Controller
    {
        private IServiceError serviceError;
        private ISecurityService SecurityService;
        private IInventoryService inventoryService;

        public WarehouseController(IServiceError serviceError,
                                  ISecurityService SecurityService,
                                  IInventoryService inventoryService)
        {
            this.serviceError = serviceError;
            this.SecurityService = SecurityService;
            this.inventoryService = inventoryService;
        }

        /// <summary>
        /// Metodo para cargar el listado de almacenes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<IActionResult> WarehouseList()
        {
            try
            {
                WarehouseViewModel Model = new WarehouseViewModel();
                var sesion = SecurityService.GetSession();
                Model.Warehouses = await inventoryService.GetWarehouses(sesion.BranchOfficeId);
                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "WarehouseList", "WarehouseController", "Error intentando carga el listado de almacenes.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para cargar la vista que registra el almacén
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult WarehouseIndex()
        {
            return View();
        }

        /// <summary>
        /// Metodo para registrar un almacén
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> CreateWarehouse(WarehouseViewModel Model)
        {
            try
            {
                await inventoryService.CreateWarehouse(Model.Warehouse);
                inventoryService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Almacén registrado correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("WarehouseList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateWarehouse", "Warehouse", "Tuvimos un problema al registrar el almacén.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Almacén registrado correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }


        /// <summary>
        /// Cargando la vista para el actualizar el almacen
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult EditWarehouse(int Id)
        {
            try
            {
                WarehouseViewModel Model = new WarehouseViewModel();
                Model.Warehouse = inventoryService.GetWarehouse(Id);
                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EditWarehouse", "WarehouseController", "Error intentando carga los datos del almacén.");
                return null;
            }
        }


        /// <summary>
        /// metodo para actualizar un almacén
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult UpdateWarehouse(WarehouseViewModel Model)
        {
            try
            {
                inventoryService.UpdateWarehouse(Model.Warehouse);
                inventoryService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Almacén actualizado correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("WarehouseList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EditWarehouse", "Warehouse", "Tuvimos un problema al actualizar el almacén.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Almacén actualizado correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        /// <summary>
        /// Metodo para cambiar de estatus del almacen
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult StatusWarehouse(int Id, bool status)
        {
            try
            {
                inventoryService.SetWarehouseStatus(Id, status);
                inventoryService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Almacén " + ((status) ? "habilitado" : "desactivado") + " correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("WarehouseList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "StatusWarehouse", "warehouse", "Tuvimos un problema al " + ((status) ? "activada" : "Desactivada") + " tu almacén.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

    }
}