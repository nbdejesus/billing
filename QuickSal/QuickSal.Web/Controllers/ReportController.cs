﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using QuickSal.Services;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.Reportes;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace QuickSal.Web.Controllers
{
    public class ReportController : Controller
    {
        private IConfiguration Configuration;
        private ISecurityService securityService;
        private IServiceError serviceError;
        private ICustomerService customerService;
        private IEmployeeService employeeService;
        private IBusinessService businessService;
        private ISupplierService supplierService;
        private IAccountingService accountingService;
        private IInventoryService inventoryService;
        private IGlobalService globalService;


        public ReportController(IConfiguration Configuration,
                                ISecurityService securityService,
                                IServiceError serviceError,
                                ICustomerService customerService,
                                IEmployeeService employeeService,
                                IBusinessService businessService,
                                ISupplierService supplierService,
                                IAccountingService accountingService,
                                IInventoryService inventoryService,
                                IGlobalService globalService
            )
        {
            this.Configuration = Configuration;
            this.securityService = securityService;
            this.serviceError = serviceError;
            this.customerService = customerService;
            this.employeeService = employeeService;
            this.businessService = businessService;
            this.supplierService = supplierService;
            this.accountingService = accountingService;
            this.inventoryService = inventoryService;
            this.globalService = globalService;
        }


        #region ReporteIndex
        [HttpGet]
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public IActionResult ReportIndex(string name)
        {
            var session = securityService.GetSession();

            ViewBag.BusinessId = session.BusinessId;
            return View(name);
        }


        /// <summary>
        /// Metodo para exportar el reporte de resoluciones
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> CrearRPCustomer(ReportViewModel model, string NombreReporte)
        {
            Guid guid = Guid.NewGuid();

            ReportModel reportModel = new ReportModel();

            reportModel.fechaIni = model.fechaIni != null ? model.fechaIni.Value.ToString("yyyy-MM-dd") : null;
            reportModel.fechaFin = model.fechaFin != null ? model.fechaFin.Value.ToString("yyyy-MM-dd") : null;
            reportModel.nameId = model.nameId;
            reportModel.privileges = model.privileges;
            reportModel.guid = guid;
            reportModel.brachOffice = model.brachOffice;
            reportModel.businessId = model.businessId;
            reportModel.codigo = model.codigo;
            reportModel.disponible  = model.disponible;
            reportModel.servicio = model.servicio;

            return Json(new
            {
                nombreDocumento = string.IsNullOrEmpty(NombreReporte) ? "Resumen de decisiones por dependencia" : NombreReporte,
                status = true,
                archivo = await ObtenerReporte(ObtenerUrl(reportModel, NombreReporte, "PDF")),
                formato = "pdf"
            });
        }

        /// <summary>
        /// Metodo para exportar el reporte de resoluciones
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<JsonResult> GetRPTReceiptSalePoint(long InvoiceId)
        {
            ReportModel reportModel = new ReportModel();
            reportModel.InvoiceId = InvoiceId;

            return Json(new
            {
                nombreDocumento = "Factura de venta",
                status = true,
                archivo = await ObtenerReporte(ObtenerUrl(reportModel, "SalesPointReceipt", "PDF")),
                formato = "pdf"
            });
        }

        /// <summary>
        /// Metodo para convertir un reporte a un archivo especificado
        /// </summary>
        /// <param name="url"></param>
        /// <param name="format"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public async Task<byte[]> ObtenerReporte(string url)
        {
            try
            {
                var parametros = await globalService.GetSettingValue("Reportes");


                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(ReplaceAllSpaces(url));
                req.UseDefaultCredentials = true;
                req.Credentials = CredentialCache.DefaultCredentials;

                if (parametros.Where(x => x.ValueName == "Usuario").Any() && parametros.Where(x => x.ValueName == "Clave").Any())
                {
                    string usuario = parametros.Where(x => x.ValueName == "Usuario").FirstOrDefault().SettingValue;
                    string clave = parametros.Where(x => x.ValueName == "Clave").FirstOrDefault().SettingValue;
                    //string dominio = parametros.Where(x => x.ValueName == "Dominio").FirstOrDefault().SettingValue;//no se está funcionando pero se dejó en caso de
                    req.Credentials = new NetworkCredential(usuario, clave);
                }

                HttpWebResponse HttpWResp = (HttpWebResponse)req.GetResponse();
                Stream fStream = HttpWResp.GetResponseStream();

                byte[] fileBytes = ReadFully(fStream);

                HttpWResp.Close();
                return fileBytes;
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "ObtenerReporte", "ReportController", "Ocurrieron inconvenientes al acceder al recurso.");
                return null;
            }
        }

        public static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Metodo para generar la url apartir de los valores que tienen la propiedades del modelo
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model">Modelo con la información</param>
        /// <param name="NombreReporte">Nombre del reporte en el web.config</param>
        /// <returns></returns>
        private string ObtenerUrl<T>(T model, string NombreReporte, string tipo)
        {
            string stringValue = tipo.ToString();

            string UrlReporte = globalService.GetSettingValue("Reportes", "UrlReporte").Result.FirstOrDefault().SettingValue;
            string UrlreportesActivos = UrlReporte + NombreReporte;
            string comandosReporte = "&rs:Command=Render";
            string formato = "&rs:Format=" + tipo.ToString();
            string Parametros = string.Empty;

            //Construye el link del reporte.
            Type type = typeof(T); // Get type pointer
            PropertyInfo[] properties = typeof(T).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                //así obtenemos el nombre del atributo
                string NombreAtributo = property.Name;

                object valor = type.GetProperty(property.Name).GetValue(model);

                if (valor != null)
                    Parametros = Parametros + NombreAtributo + "=" + valor + "&";
            }
            string url = string.Empty;
            if (!string.IsNullOrWhiteSpace(Parametros))
                url = string.Format("{0}{1}&{2}&{3}", UrlreportesActivos, comandosReporte, formato.Replace("&", ""), Parametros.Remove(Parametros.Length - 1, 1));
            else
                url = string.Format("{0}{1}", UrlreportesActivos, formato);

            return url;
        }

        private static string ReplaceAllSpaces(string str)
        {
            return Regex.Replace(str, @"\s+", "%20");
        }

        #endregion

        #region ModalReport

        [HttpGet]
        public IActionResult CustomerModal(long businessId)
        {
            ReportViewModel model = new ReportViewModel();

            var customerList = customerService.GetCustomers(businessId);
            model.businessId = businessId;

            model.customerList = customerList;
            return PartialView("~/views/Report/ModalReport/CustomerModal.cshtml", model);
        }
        [HttpGet]
        public IActionResult CrediNoteModal(long businessId)
        {
            ReportViewModel model = new ReportViewModel();

            var customerList = customerService.GetCustomers(businessId);
            model.businessId = businessId;

            model.customerList = customerList;
            return PartialView("~/views/Report/ModalReport/CrediNoteModal.cshtml", model);
        }

        [HttpGet]
        public IActionResult CustomerInvoicesModal(long businessId)
        {
            ReportViewModel model = new ReportViewModel();

            model.customerList = customerService.GetCustomers(businessId);
            model.businessId = businessId;           

            return PartialView("~/views/Report/ModalReport/CustomerInvoicesModal.cshtml", model);
        }

        [HttpGet]
        public IActionResult CustomerCreditNoteModal(long businessId)
        {
            ReportViewModel model = new ReportViewModel();

            model.customerList = customerService.GetCustomers(businessId);
            model.businessId = businessId;

            return PartialView("~/views/Report/ModalReport/CustomerCreditNoteModal.cshtml", model);
        }

        [HttpGet]
        public IActionResult EmployeeModal(long businessId)
        {
            ReportViewModel model = new ReportViewModel();

            model.employeeBusinessesList = employeeService.GetEmployees(businessId);
            //model.privilegesList = businessService.GetPrivileges(businessId);
            model.businessId = businessId;

            return PartialView("~/views/Report/ModalReport/EmployeeModal.cshtml", model);
        }

        [HttpGet]
        public IActionResult EmployeePurchaseModal(long businessId)
        {
            ReportViewModel model = new ReportViewModel();

            model.customerList = customerService.GetCustomers(businessId);
            model.businessId = businessId;
            return PartialView("~/views/Report/ModalReport/EmployeePurchaseModal.cshtml", model);
        }

        [HttpGet]
        public IActionResult SuplierModal(long businessId)
        {
            ReportViewModel model = new ReportViewModel();

            model.suppliersList = supplierService.GetSuppliers(businessId);
            model.businessId = businessId;

            return PartialView("~/views/Report/ModalReport/SuplierModal.cshtml", model);
        }
        [HttpGet]
        public IActionResult SuplierPurchaseModal(long businessId)
        {
            ReportViewModel model = new ReportViewModel();

            model.suppliersList = supplierService.GetSuppliers(businessId);
            model.branchOfficesList = businessService.getBranchOffices(businessId);
            model.businessId = businessId;

            return PartialView("~/views/Report/ModalReport/SuplierPurchaseModal.cshtml", model);
        }

        [HttpGet]
        public IActionResult InvoiceModal(long businessId)
        {
            ReportViewModel model = new ReportViewModel();

            model.branchOfficesList = businessService.getBranchOffices(businessId);
            model.businessId = businessId;
            return PartialView("~/views/Report/ModalReport/InvoiceModal.cshtml", model);
        }

        [HttpGet]
        public IActionResult InvoicePendindModal(long businessId)
        {
            ReportViewModel model = new ReportViewModel();

            model.customerList = customerService.GetCustomers(businessId);
            model.branchOfficesList = businessService.getBranchOffices(businessId);
            model.businessId = businessId;

            return PartialView("~/views/Report/ModalReport/InvoicePendindModal.cshtml", model);
        }

        [HttpGet]
        public IActionResult NCFModal(long businessId)
        {
            ReportViewModel model = new ReportViewModel();

            model.ncfPrefixesList = accountingService.GettypeNcf(businessId);
            model.businessId = businessId;

            return PartialView("~/views/Report/ModalReport/NCFModal.cshtml", model);
        }

        [HttpGet]
        public IActionResult NCFAvailableModal(long businessId)
        {
            ReportViewModel model = new ReportViewModel();

            model.ncfPrefixesList = accountingService.GettypeNcf(businessId);
            model.businessId = businessId;         
            return PartialView("~/views/Report/ModalReport/NCFAvailableModal.cshtml", model);
        }

        [HttpGet]
        public IActionResult SuplierPaymentModal(long businessId)
        {
            ReportViewModel model = new ReportViewModel();

            model.suppliersList = supplierService.GetSuppliers(businessId);
            model.branchOfficesList = businessService.getBranchOffices(businessId);
            model.businessId = businessId;

            return PartialView("~/views/Report/ModalReport/SuplierPaymentModal.cshtml", model);
        }

        [HttpGet]
        public IActionResult ProductModal(long businessId)
        {
            ReportViewModel model = new ReportViewModel();
            model.productsList = inventoryService.GetProducts(businessId);
            model.businessId = businessId;

            return PartialView("~/views/Report/ModalReport/ProductModal.cshtml", model);
        }

        [HttpGet]
        public IActionResult PurchasePendingModal(long businessId)
        {
            ReportViewModel model = new ReportViewModel();
            model.suppliersList = supplierService.GetSuppliers(businessId);
            model.branchOfficesList = businessService.getBranchOffices(businessId);
            model.businessId = businessId;

            return PartialView("~/views/Report/ModalReport/PurchasePendingModal.cshtml", model);
        }


        [HttpGet]
        public IActionResult DebitNoteModal(long businessId)
        {
            ReportViewModel model = new ReportViewModel();
            model.branchOfficesList = businessService.getBranchOffices(businessId);
            model.businessId = businessId;

            return PartialView("~/views/Report/ModalReport/DebitNoteModal.cshtml", model);
        }

        [HttpGet]
        public IActionResult DebitNoteAvailableModal(long businessId)
        {
            ReportViewModel model = new ReportViewModel();
            model.branchOfficesList = businessService.getBranchOffices(businessId);
            model.businessId = businessId;

            return PartialView("~/views/Report/ModalReport/DebitNoteAvailableModal.cshtml", model);
        }

        [HttpGet]
        public IActionResult WareHouseMovingOfModal(long businessId)
        {
            ReportViewModel model = new ReportViewModel();
            model.branchOfficesList = businessService.getBranchOffices(businessId);
            model.businessId = businessId;

            return PartialView("~/views/Report/ModalReport/WareHouseMovingOfModal.cshtml", model);
        }

        [HttpGet]
        public IActionResult WareHouseMovingToModal(long businessId)
        {
            ReportViewModel model = new ReportViewModel();
            model.branchOfficesList = businessService.getBranchOffices(businessId);
            model.businessId = businessId;

            return PartialView("~/views/Report/ModalReport/WareHouseMovingToModal.cshtml", model);
        }

      
        [HttpGet]
        public IActionResult ProveedoresModal(long businessId)
        {
            ReportViewModel model = new ReportViewModel();
            model.branchOfficesList = businessService.getBranchOffices(businessId);
            model.businessId = businessId;
            model.suppliersList = supplierService.GetSuppliers(businessId);

            return PartialView("~/views/Report/ModalReport/ProveedoresModal.cshtml", model);
        }
   
    }

    
        #endregion
    }
