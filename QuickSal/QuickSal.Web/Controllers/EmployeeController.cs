﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using QuickSal.Services;
using QuickSal.Services.Services;
using QuickSal.Services.Support;
using QuickSal.Web.Api;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.Employee;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static QuickSal.DataAcces.QuickSalEnum;

namespace QuickSal.Web.Controllers
{
    public class EmployeeController : Controller
    {
        private IBusinessService businessService;
        private IEmployeeService employeeService;
        private IGlobalService globalService;
        private IGenericMethod _genericMethod;
        private IServiceError serviceError;
        private IConfiguration configurationService;
        private IHostingEnvironment _env;
        private IAccountService accountingService;
        private INotificationsService notificationService;
        private ISecurityService securityService;
        private IAmazonService amazonService;

        public EmployeeController(IBusinessService businessService, IGlobalService globalService, IAmazonService amazonService,
                                  IGenericMethod GenericMethod, IServiceError serviceError,
                                  IConfiguration configurationService, IHostingEnvironment Env,
                                  IAccountService accountingService, IEmployeeService employeeService,
                                  INotificationsService notificationService, ISecurityService securityService)
        {
            this.businessService = businessService;
            this.globalService = globalService;
            this._genericMethod = GenericMethod;
            this.serviceError = serviceError;
            this.configurationService = configurationService;
            this._env = Env;
            this.accountingService = accountingService;
            this.employeeService = employeeService;
            this.notificationService = notificationService;
            this.securityService = securityService;
            this.amazonService = amazonService;
        }

        #region privilegio


        /// <summary>
        /// Metodo para cargar el listado de privilegios
        /// </summary>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult PrivilegesList()
        {
            try
            {

                EmployeeViewModel model = new EmployeeViewModel();
                var session = securityService.GetSession();
                model.Privileges = businessService.GetPrivileges(session.BusinessId);

                return View("~/Views/Employee/Privileges/PrivilegesList.cshtml", model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "PrivilegesList", "EmployeeController", "Tuvimos un problema al cargar los datos de los privilegios.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para cargar la vista para registrar un privilegio
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult PrivilegeIndex()
        {
            try
            {
                var session = securityService.GetSession();
                EmployeeViewModel model = new EmployeeViewModel();
                model.Objects = businessService.GetObjects(session.BusinessId, Principal: true);

                return View("~/Views/Employee/Privileges/CreatePrivilege.cshtml", model);

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "PrivilegesList", "EmployeeController", "Tuvimos un problema al cargar los datos de los privilegios.");
                return null;
            }
        }

        /// <summary>
        /// Proceso para cargar la vista que actualiza un privilegio
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult PrivilegeEdit(int PrivilegeId)
        {
            try
            {
                var session = securityService.GetSession();

                EmployeeViewModel model = new EmployeeViewModel();
                //Buscamos el privilegio super usuario que contiene todos los accesos
                model.Objects = businessService.GetObjects(session.BusinessId, Principal: true);

                //Buscamos la seguridad con los accesos
                model.SegurityAccess = businessService.GetSegurityAccess(PrivilegeId);

                //Buscamos el nombre del privilegio
                model.Privilege = businessService.GetPrivilege(PrivilegeId, session.BusinessId);

                return View("~/Views/Employee/Privileges/UpdatePrivilege.cshtml", model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "PrivilegeEdit", "EmployeeController", "Tuvimos un problema al cargar los datos de los privilegios.");
                return null;
            }
        }


        /// <summary>
        /// Metodo para registrar un privilegio en el sistema
        /// </summary>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult CreatePrivilege(EmployeeViewModel model)
        {
            try
            {
                businessService.CreatePrivilege(model.Privilege, ConvertAccesToSegurity(model.security));
                businessService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Privilegio registrado correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("PrivilegesList")
                });

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreatePrivilege", "EmployeeController", "Tuvimos un problema al registrar el privilegio.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        //private List<SegurityAccess> ConvertSegurityToAcces(List<>)
        private List<TblSegurity> ConvertAccesToSegurity(List<SegurityAccess> model)
        {
            List<TblSegurity> segurities = new List<TblSegurity>();

            foreach (var item in model)
            {
                if (item.CanRead)
                {
                    TblSegurity segurity = new TblSegurity();
                    segurity.SubObjectId = item.SubObjectId;
                    segurity.TypeMethodId = 1;
                    segurities.Add(segurity);
                }

                if (item.CanWrite)
                {
                    TblSegurity segurity = new TblSegurity();
                    segurity.SubObjectId = item.SubObjectId;
                    segurity.TypeMethodId = 2;
                    segurities.Add(segurity);
                }

                if (item.CanEdit)
                {
                    TblSegurity segurity = new TblSegurity();
                    segurity.SubObjectId = item.SubObjectId;
                    segurity.TypeMethodId = 3;
                    segurities.Add(segurity);
                }

                if (item.CanDelete)
                {
                    TblSegurity segurity = new TblSegurity();
                    segurity.SubObjectId = item.SubObjectId;
                    segurity.TypeMethodId = 4;
                    segurities.Add(segurity);
                }
            }

            return segurities;
        }

        /// <summary>
        /// Metodo para actualizar un privilegio
        /// </summary>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult UpdatePrivilege(EmployeeViewModel model)
        {
            try
            {
                businessService.UpdatePrivilege(model.Privilege, ConvertAccesToSegurity(model.security));
                businessService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Privilegio actualizado correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("PrivilegesList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdatePrivilege", "EmployeeController", "Tuvimos un problema al actualizar el privilegio.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }


        /// <summary>
        /// Metodo para actualizar el estatus del privilegio
        /// </summary>
        /// <param name="model">Modelo que contiene la informacion del empleado</param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> StatusPrivileges(bool status, int privilege)
        {
            try
            {
                var session = securityService.GetSession();
                businessService.StatusPrivilege(status, privilege, session.BusinessId);

                var task = await employeeService.Savechanges();

                return Json(new
                {
                    message = ((task.ExecutedSuccesfully) ? "Privilegio actualizado correctamente" : task.MessageList[0]),
                    status = task.ExecutedSuccesfully,
                    redirect = Url.Action("PrivilegesList")
                });

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "StatusPrivileges", "EmployeeController", "se presento un problema al actualizar la información del privilegio.");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }

        #endregion

        #region empleados
        /// <summary>
        /// Metodo para cargar el listado de empleados de una empresa
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult EmployeesList()
        {
            try
            {
                EmployeeViewModel model = new EmployeeViewModel();
                var session = securityService.GetSession();

                model.EmployeeBusiness = employeeService.GetEmployees(session.BusinessId);
                return View(model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EmployeesList", "EmployeeController", "Tuvimos un problema al cargar los datos de los empleados.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para cargar la vista de registro del empleado
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<IActionResult> EmployeeIndex()
        {
            try
            {
                EmployeeViewModel model = new EmployeeViewModel();
                var session = securityService.GetSession();

                //Obtenemo un nuevo codigo para el empleado
                string code = employeeService.GetLastEmployeeCode(session.BusinessId);
                ViewBag.EmployeeCode = globalService.GenerateCode(Services.Support.EnumTypes.CodeEntities.Employee, code);

                model.Privileges = businessService.GetPrivileges(session.BusinessId);
                model.BranchOffices = businessService.getBranchOffices(session.BusinessId);

                //Cargamos las opciones desplegables
                model.Countries = globalService.GetAllCountries();
                model.DocumentIdentifications = globalService.GetAllDocumentIdentification();

                await LoadPictures(model);

                return View(model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EmployeeIndex", "EmployeeController", "Tuvimos un problema al cargar los datos de los empleados.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para validar si un correo esta asociado a un empleado
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public JsonResult ValidateEmployee(string email)
        {
            try
            {
                TaskResult task = new TaskResult();
                var session = securityService.GetSession();

                var user = accountingService.GetUser(email);
                if (user != null)
                {
                    var employee = employeeService.GetEmployeeInBusiness(user.Id, session.BusinessId);

                    if (employee != null)
                        task = serviceError.WarningMessage("El correo se encuentra asociado a un empleado");

                }

                return Json(new
                {
                    message = ((task.ExecutedSuccesfully) ? "" : task.MessageList[0]),
                    status = task.ExecutedSuccesfully
                });

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "ValidateEmployee", "Employee", "Se presento un problema al validar el empleado");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }


        /// <summary>
        /// Metodo para guardar los datos de un empleado
        /// </summary>
        /// <param name="model">Modelo con la informacion</param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> CreateEmployee(EmployeeViewModel model)
        {
            try
            {
                var session = securityService.GetSession();
                var statusMail = false;

                await employeeService.CreateEmployee(model.Employee);
                var task = await employeeService.Savechanges();

                //if (task.ExecutedSuccesfully)
                //{
                //    try
                //    {
                //        var user = accountingService.GetUser(model.Employee.User.Email);

                //        Mail mail = new Mail();
                //        mail.To = model.Employee.User.Email;
                //        mail.Subject = "Contraseña temporal para Quicksal";
                //        var business = businessService.GetBusinessById(session.BusinessId);

                //        //send temp password
                //        mail.Body = notificationService.WelcomeToQuicksalEmployee(user.UserPassword, business.Name, Url.Action("UserAccount", "account"));
                //        var taskMail = await notificationService.SendMailConfirmation(mail);
                //        statusMail = taskMail.ExecutedSuccesfully;
                //    }
                //    catch (Exception ex)
                //    {
                //        serviceError.LogError(ex.Message, "SendMailConfirmation", "AccountController");
                //    }
                //}

                return Json(new
                {
                    message = ((task.ExecutedSuccesfully) ? "Empleado registrado correctamente" : task.MessageList[0]),
                    status = task.ExecutedSuccesfully,
                    redirect = Url.Action("EmployeesList"),
                    StatusMail = statusMail
                });

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateEmployee", "employee", "se presento un problema grabar la información del empleado");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }

        /// <summary>
        /// Metodo para cargar la vista de registro del empleado
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<IActionResult> EditEmployee(long Id)
        {
            try
            {

                EmployeeViewModel model = new EmployeeViewModel();
                var session = securityService.GetSession();

                model.Employee = employeeService.GetEmployee(Id, session.BusinessId);

                //Cargamos las opciones desplegables
                model.Countries = globalService.GetAllCountries();
                model.DocumentIdentifications = globalService.GetAllDocumentIdentification();
                model.Privileges = businessService.GetPrivileges(session.BusinessId);
                model.BranchOffices = businessService.getBranchOffices(session.BusinessId);
                model.UserEmail = accountingService.GetEmailUser(model.Employee.UserId);

                if (model.Employee.CountryId.HasValue && model.Employee.CountryId.HasValue)
                    model.Provinces = globalService.GetAllProvince(model.Employee.CountryId.Value);

                await LoadPictures(model);

                return View(model);

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EditEmployee", "EmployeeController", "Tuvimos un problema al cargar los datos de los empleados.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para actualizar los datos del empleado
        /// </summary>
        /// <param name="model">Modelo que contiene la informacion del empleado</param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> UpdateEmployee(EmployeeViewModel model)
        {
            try
            {
                await employeeService.EditEmployee(model.Employee);

                var task = await employeeService.Savechanges();

                return Json(new
                {
                    message = ((task.ExecutedSuccesfully) ? "Empleado actualizado correctamente" : task.MessageList[0]),
                    status = task.ExecutedSuccesfully,
                    redirect = Url.Action("EmployeesList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateEmployee", "employee", "se presento un problema al actualizar la información del empleado");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }


        /// <summary>
        /// Metodo para actualizar el estatus del empleado
        /// </summary>
        /// <param name="model">Modelo que contiene la informacion del empleado</param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> StatusEmployee(bool status, long employee)
        {
            try
            {
                var session = securityService.GetSession();
                employeeService.StatusEmployee(status, employee, session.BusinessId);

                var task = await employeeService.Savechanges();

                return Json(new
                {
                    message = ((task.ExecutedSuccesfully) ? "Empleado actualizado correctamente" : task.MessageList[0]),
                    status = task.ExecutedSuccesfully,
                    redirect = Url.Action("EmployeesList")
                });

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "StatusEmployee", "EmployeeController", "se presento un problema al actualizar la información del empleado");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }


        /// <summary>
        /// Proceso para guardar la foto del producto en el sistema
        /// </summary>
        /// <returns></returns>
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public async Task<JsonResult> SaveUploadedPicture(IFormFile files)
        {
            try
            {
                var session = securityService.GetSession();
                string guid = Guid.NewGuid().ToString();
                string url = string.Empty;

                TblTempFile file = new TblTempFile();
                file.FileName = guid + files.FileName;
                file.TypeFile = (byte)TypeFiles.EmployeePhoto;
                file.UserId = session.UserId;

                MemoryStream ms = new MemoryStream();
                files.CopyTo(ms);
                await globalService.AddTempFile(file, ms);

                var task = await globalService.Savechanges();

                if (task.ExecutedSuccesfully)
                    url = await globalService.GetTempUrlObjet(file.FileName);


                return Json(new
                {
                    message = ((task.ExecutedSuccesfully) ? "Foto cargada correctamente" : task.MessageList[0]),
                    File = url,
                    fileName = file.FileName,
                    status = task.ExecutedSuccesfully
                });

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "SaveUploadedPicture", "Employee", "Se presento un problema al cargar la foto");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }


        /// <summary>
        /// Metodo para cargar las imagenes que usa el formulario
        /// </summary>
        /// <param name="model"></param>
        private async Task LoadPictures(EmployeeViewModel model)
        {
            if (model.Employee != null && !string.IsNullOrEmpty(model.Employee.Picture))
            {
                if (!string.IsNullOrEmpty(model.Employee.Picture))
                    model.UrlPhoto = await amazonService.GetUrlObjet(model.Employee.Picture, TypeFiles.EmployeePhoto);
            }
            else
            {
                var productPicture = await globalService.GetSettingValue("amazon", "ProfileDefault");
                model.UrlPhoto = productPicture.FirstOrDefault().SettingValue;
            }
        }

        #endregion
    }
}