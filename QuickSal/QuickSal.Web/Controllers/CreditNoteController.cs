﻿using Microsoft.AspNetCore.Mvc;
using QuickSal.Services;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.Customer.CreditNote;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuickSal.Web.Controllers
{
    public class CreditNoteController : Controller
    {
        private ICustomerService customerService;
        private ISecurityService securityService;
        private IServiceError serviceError;
        private IBusinessService businessService;
        private IInventoryService inventoryService;
        private IGlobalService globalService;
        private IAccountingService accountingService;

        public CreditNoteController(ICustomerService customerService, ISecurityService securityService, IServiceError serviceError,
                                IBusinessService businessService, IInventoryService inventoryService, IGlobalService globalService, IAccountingService accountingService)
        {
            this.customerService = customerService;
            this.securityService = securityService;
            this.serviceError = serviceError;
            this.businessService = businessService;
            this.inventoryService = inventoryService;
            this.globalService = globalService;
            this.accountingService = accountingService;
        }

        /// <summary>
        /// Metodo para cargar la vista de registro de producto
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult CreditNoteIndex()
        {
            try
            {
                List<CreditNoteViewModel> prueba = new List<CreditNoteViewModel>();

                ViewBag.fy = prueba;
                CreditNoteViewModel Model = new CreditNoteViewModel();
                var session = securityService.GetSession();

                Model.CreditNotes = customerService.GetAllCreditNotes(session.BranchOfficeId);
                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreditNoteIndex", "CreditNoteController", "Tuvimos un problema cargar la nota de crédito.");
                return null;
            }
        }


        /// <summary>
        /// Metodo para cargar las facturas que estan en estado procesada
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public PartialViewResult _Invoices()
        {
            CreditNoteViewModel Model = new CreditNoteViewModel();
            var session = securityService.GetSession();
            Model.Invoices = customerService.GetInvoiceWithOutCreditNote(session.BranchOfficeId);
            return PartialView(Model);
        }


        /// <summary>
        /// Metodo para cargar las facturas que estan en estado procesada
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public PartialViewResult _InvoiceItems(long invoice)
        {
            CreditNoteViewModel Model = new CreditNoteViewModel();
            Model.InvoiceRowItems = customerService.GetInvoiceDetail(invoice);
            return PartialView(Model);
        }


        /// <summary>
        /// Metodo para cargar la vista de registro de producto
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult CreateCreditNote(long invoiceId)
        {
            try
            {
                CreditNoteViewModel Model = new CreditNoteViewModel();
                var session = securityService.GetSession();
                ViewBag.DocumentNumber = globalService.GenerateCode(Services.Support.EnumTypes.CodeEntities.CreditNote);

                Model.InvoiceHeader = customerService.GetInvoiceHeader(invoiceId);
                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateCreditNote", "CreditNoteController", "Tuvimos un problema cargar la nota de crédito.");
                return null;
            }
        }



        /// <summary>
        /// Metodo para cargar la vista de registro de producto
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> CreateCreditNote(CreditNoteViewModel model)
        {
            try
            {
                var sesion = securityService.GetSession();
                customerService.CreateCreditNote(model.CreditNote);
                var task = await customerService.Savechanges();

                if (task.ExecutedSuccesfully && model.CreditNote.DocumentEstatusId == (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada)
                    task.ExecutedSuccesfully = customerService.ProcessCreditNote(sesion.BranchOfficeId, model.CreditNote.Id);

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Nota de crédito registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateCreditNote", "CreditNoteController", "Problemas al registrar la nota de crédito");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        /// <summary>
        /// Metodo para cargar la vista para editar una nota de credito
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult EditCreditNote(long creditNoteId)
        {
            try
            {
                CreditNoteViewModel Model = new CreditNoteViewModel();
                var session = securityService.GetSession();

                Model.CreditNote = customerService.GetCreditNoteHeader(creditNoteId);
                Model.CreditNoteRow = customerService.GetCreditNoteDetails(creditNoteId);
                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EditCreditNote", "EditCreditNote", "Tuvimos un problema cargar la nota de crédito.");
                return null;
            }
        }


        /// <summary>
        /// Metodo para cargar la vista de registro de producto
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> UpdateCreditNote(CreditNoteViewModel model)
        {
            try
            {
                var sesion = securityService.GetSession();
                customerService.UpdateCreditNote(model.CreditNote);
                var task = await customerService.Savechanges();

                if (task.ExecutedSuccesfully && model.CreditNote.DocumentEstatusId == (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada)
                    task.ExecutedSuccesfully = customerService.ProcessCreditNote(sesion.BranchOfficeId, model.CreditNote.Id);

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Nota de crédito actualizada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateCreditNote", "CreditNoteController", "Problemas al actualizar la nota de crédito");
                return null;
            }
        }


        /// <summary>
        /// Metodo para cargar los articulos que tiene una factura
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public JsonResult InvoiceItems(long invoice)
        {
            CreditNoteViewModel Model = new CreditNoteViewModel();
            Model.InvoiceRowItems = customerService.GetInvoiceDetail(invoice);

            return Json(new
            {
                invoiceItems = Model.InvoiceRowItems
            });
        }


        /// Metodo para cambiar el estado de la nota de credito
        /// </summary>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult StatusCreditNote(int Id, byte DocumentStatusID)
        {
            try
            {
                var session = securityService.GetSession();
                customerService.SetCreditNoteStatus(Id, DocumentStatusID);
                customerService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" + "Nota de crédito actualizada correctamente" : "Desactivada") + "",
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("CreditNoteIndex")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "StatusCreditNote", "CreditNoteController", "Tuvimos un problema al actualizar la nota de crédito.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }
    }
}