﻿using Microsoft.AspNetCore.Mvc;
using QuickSal.Services;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.Customer.Order;
using System;
using System.Threading.Tasks;

namespace QuickSal.Web.Controllers
{
    public class OrderController : Controller
    {
        private ICustomerService customerService;
        private ISecurityService securityService;
        private IServiceError serviceError;

        public OrderController(ICustomerService customerService, ISecurityService securityService, IServiceError serviceError)
        {
            this.customerService = customerService;
            this.securityService = securityService;
            this.serviceError = serviceError;
        }


        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult ListOrders()
        {
            return View();
        }


        [ServiceFilter(typeof(LoginSecurityFilter))]
        public async Task<PartialViewResult> _Orders()
        {
            try
            {
                OrderViewModel Model = new OrderViewModel();
                var session = securityService.GetSession();

                Model.Orders = await customerService.GetLastTwoDaysPendingOrders(session.BranchOfficeId);
                return PartialView(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "ListOrders", "OrderController", "Tuvimos un problema cargar listado de ordenes.");
                return null;
            }
        }

        [ServiceFilter(typeof(LoginSecurityFilter))]
        public async Task<JsonResult> UpdateStatusOrder(int orderId, byte status)
        {
            try
            {
                var task = await customerService.SetOrdenStatus(orderId, status);
                task = await customerService.Savechanges();

                return Json(new
                {
                    message = ((task.ExecutedSuccesfully) ? "Orden actualizada correctamente" : task.MessageList[0]),
                    status = task.ExecutedSuccesfully,
                    redirect = Url.Action("ListOrders"),
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateStatusOrder", "Order", "se presentó un problema actualizar la orden.");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }
    }
}
