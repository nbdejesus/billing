﻿using Microsoft.AspNetCore.Mvc;
using QuickSal.Services;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.Inventory;
using System;

namespace QuickSal.Web.Controllers
{
    public class SubCategoryController : Controller
    {
        private IServiceError serviceError;
        private ISecurityService SecurityService;
        private IInventoryService inventoryService;

        public SubCategoryController(IServiceError serviceError,
                                  ISecurityService SecurityService,
                                  IInventoryService inventoryService)
        {
            this.serviceError = serviceError;
            this.SecurityService = SecurityService;
            this.inventoryService = inventoryService;
        }


        /// <summary>
        /// Metodo para mostrar el listado de subcategorias
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult SubCategoryList()
        {
            try
            {
                SubCategoryViewModel Model = new SubCategoryViewModel();
                var session = SecurityService.GetSession();

                Model.SubCategories = inventoryService.GetBusinesSubCategories(session.BusinessId);
                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "SubCategoryList", "SubCategoryController", "Tuvimos un problema cargar listado de Subcategoría.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para cargar la vista de registro de SubCateorias
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public PartialViewResult _SubCategoryIndex()
        {
            SubCategoryViewModel model = new SubCategoryViewModel();
            var session = SecurityService.GetSession();

            model.Categories = inventoryService.GetCategories(session.BusinessId);
            return PartialView(model);
        }

        /// <summary>
        /// Metodo para registrar una SubCategoria
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult CreateSubCategory(SubCategoryViewModel Model)
        {
            try
            {
                inventoryService.CreateSubCateogry(Model.SubCategory);
                inventoryService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "SubCategoría registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateSubCategory", "SubCategoryController", "Tuvimos un problema al registrar la SubCategoría.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Subcategoría registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }


        /// <summary>
        /// Metodo para cargar la vista para el actualizar una Subcategoria
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult _EditSubCategory(int Id)
        {
            try
            {
                SubCategoryViewModel Model = new SubCategoryViewModel();
                var session = SecurityService.GetSession();

                Model.SubCategory = inventoryService.GetSubCategory(Id);
                Model.Categories = inventoryService.GetCategories(session.BusinessId);
                return PartialView(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "_EditSubCategory", "SubCategoryController", "Error intentando carga los datos de la Subcategoría.");
                return null;
            }
        }


        /// <summary>
        /// metodo para actualizar una subcategoria
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult UpdateSubCategory(SubCategoryViewModel Model)
        {
            try
            {
                inventoryService.UpdateSubCategory(Model.SubCategory);
                inventoryService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Subcategoría actualizada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateSubCategory", "SubCategoryController", "Tuvimos un problema al actualizar la subcategoría.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Subcategoría actualizada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        /// <summary>
        /// Metodo para cambiar de estatus de la subcategoría 
        /// </summary>
        /// <param name="Id">Primary key de la subcategoría </param>
        /// <param name="status">Estatus de la subcategoría </param>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult StatusSubCategory(int Id, bool status)
        {
            try
            {
                inventoryService.SetSubCategoryStatus(Id, status);
                inventoryService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Subcategoría " + ((status) ? "habilitada" : "desactivada") + " correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("SubCategoryList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "StatusSubCategory", "SubCategoryController", "Tuvimos un problema al " + ((status) ? "activar" : "Desactivar") + " la subcategoría.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }
    }
}