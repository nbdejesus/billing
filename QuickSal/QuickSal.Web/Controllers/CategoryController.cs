﻿using Microsoft.AspNetCore.Mvc;
using QuickSal.Services;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.Inventory;
using System;

namespace QuickSal.Web.Controllers
{
    public class CategoryController : Controller
    {
        private IServiceError serviceError;
        private ISecurityService SecurityService;
        private IInventoryService inventoryService;

        public CategoryController(IServiceError serviceError,
                                  ISecurityService SecurityService,
                                  IInventoryService inventoryService)
        {
            this.serviceError = serviceError;
            this.SecurityService = SecurityService;
            this.inventoryService = inventoryService;
        }


        /// <summary>
        /// Metodo para mostrar el listado de categorias
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult CategoryList()
        {
            try
            {
                CategoryViewModel Model = new CategoryViewModel();            
                var session = SecurityService.GetSession();

                Model.Categories = inventoryService.GetCategories(session.BusinessId);
                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CategoryList", "CategoryController", "Tuvimos un problema cargar listado de categoría.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para cargar la vista de registro de categorias
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public PartialViewResult _CategoryIndex()
        {
            return PartialView();
        }

        /// <summary>
        /// Metodo para registrar una categoria
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult CreateCategory(CategoryViewModel Model)
        {
            try
            {
                inventoryService.CreateCategory(Model.Category);
         
                inventoryService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Categoría registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateCategory", "CategoryController", "Tuvimos un problema al registrar la categoría.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Categoría registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }


        /// <summary>
        /// Metodo para cargar la vista para el actualizar ua categoria
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult _EditCategory(int Id)
        {
            try
            {
                CategoryViewModel Model = new CategoryViewModel();
                Model.Category = inventoryService.GetCategory(Id);
                return PartialView(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "_EditCategory", "CategoryController", "Error intentando carga los datos de la categoría.");
                return null;
            }
        }


        /// <summary>
        /// metodo para actualizar una categoria
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult UpdateCategory(CategoryViewModel Model)
        {
            try
            {
                inventoryService.UpdateCategory(Model.Category);
                inventoryService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Categoría actualizada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateCategory", "CategoryController", "Tuvimos un problema al actualizar la categoría.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Categoría actualizada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        /// <summary>
        /// Metodo para cambiar de estatus de la categoría 
        /// </summary>
        /// <param name="Id">Primary key de la categoría </param>
        /// <param name="status">Estatus de la categoría </param>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult StatusCategory(int Id, bool status)
        {
            try
            {
                inventoryService.SetCategoryStatus(Id, status);
                inventoryService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Categoría " + ((status) ? "habilitada" : "desactivada") + " correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("CategoryList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "StatusCategory", "CategoryController", "Tuvimos un problema al " + ((status) ? "activar" : "Desactivar") + " la categoría.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }
    }
}