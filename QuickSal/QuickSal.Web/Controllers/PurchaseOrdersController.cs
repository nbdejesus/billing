﻿using Microsoft.AspNetCore.Mvc;
using QuickSal.DataAcces.Models;
using QuickSal.Services;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.DebitNote;
using QuickSal.Web.ViewModels.Purchase;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.Web.Controllers
{
    public class PurchaseOrdersController : Controller
    {
        private IServiceError serviceError;
        private ISecurityService SecurityService;
        private IAccountingService accountingService;
        private ISupplierService supplierService;
        private IInventoryService inventoryService;
        private IGlobalService globalService;

        public PurchaseOrdersController(IServiceError serviceError, ISecurityService SecurityService, IAccountingService accountingService, ISupplierService supplierService, IInventoryService inventoryService, IGlobalService globalService)
        {
            this.serviceError = serviceError;
            this.SecurityService = SecurityService;
            this.accountingService = accountingService;
            this.supplierService = supplierService;
            this.inventoryService = inventoryService;
            this.globalService = globalService;
        }

        /// <summary>
        /// Metodo para mostrar el listado de PurchaseOrder
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult PurchaseOrdersList()
        {
            try
            {
                PurchaseViewModel Model = new PurchaseViewModel();
                var session = SecurityService.GetSession().BusinessId;
                var bussined = Convert.ToInt64(session);
                Model.PurchaseOrders = supplierService.PurchaseOrders(bussined);
                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "PurchaseOrdersList", "PurchaseOrdersController", "Tuvimos un problema cargar listado de ordenes.");
                return null;
            }
        }

        [HttpGet]
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public PartialViewResult _Suppliers()
        {
            PurchaseViewModel Model = new PurchaseViewModel();
            var session = SecurityService.GetSession();

            Model.Suppliers = supplierService.GetSuppliers(session.BusinessId);
            return PartialView("~/views/PurchaseOrders/Partial/_Suppliers.cshtml", Model);
        }


        [HttpGet]
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public async Task<PartialViewResult> _Products()
        {
            PurchaseViewModel Model = new PurchaseViewModel();
            var session = SecurityService.GetSession();

            Model.products = inventoryService.GetProducts(session.BusinessId);
            Model.Warehouse = await inventoryService.GetWarehouses(session.BranchOfficeId);
            return PartialView("~/views/PurchaseOrders/Partial/_Products.cshtml", Model);
        }

        /// <summary>
        /// Metodo para mostrar el listado de PurchaseOrder
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public PartialViewResult _DebitNoteList(long suplierId)
        {
            try
            {
                DebitNote model = new DebitNote();
                var session = SecurityService.GetSession();
                model.DebitNoteStatu = supplierService.ListDebitNoteState(session.BranchOfficeId, suplierId);
                return PartialView("~/views/PurchaseOrders/Partial/DebitNote/_DebitNoteList.cshtml", model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "DebitNoteList", "PurchaseOrdersController", "Tuvimos un problema cargar listado de ordenes.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para cargar  el formulario de  Compras
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult PurchaseOrderCreate(long supplierId)
        {
            try
            {
                PurchaseViewModel Model = new PurchaseViewModel();
                var session = SecurityService.GetSession();

                //Model.Taxes = accountingService.GetTaxes(session.BusinessId);
                Model.Supplier = supplierService.GetSupplier(supplierId, session.BusinessId);
                ////Obtenemos un nuevo codigo para el cliente
                string code = supplierService.GetLastPuschaseCode(Convert.ToInt64(session.BranchOfficeId));
                ViewBag.purchase = globalService.GenerateCode(Services.Support.EnumTypes.CodeEntities.PurshOrder, code);
                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "PurchaseOrdersList", "PurchaseOrdersController", "Tuvimos un problema cargar listado de ordenes.");
                return null;
            }
        }

        /// Metodo para registrar el PurchaseOrder
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> CreatePurchaseOrder(PurchaseViewModel Model)
        {
            try
            {
                await supplierService.CreatePurchase(Model.PurchaseOrder, Model.debitNote);

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Orden de compras registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("PurchaseOrdersList")

                });

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreatePurchaseOrder", "PurchaseOrdersController", "Tuvimos un problema al registrar la orden.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Orden de compras registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        /// Metodo para cambiar el estado del PurchaseOrder
        /// </summary>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> StatusOrden(int Id, long DocumentStatusID)
        {
            try
            {
                var session = SecurityService.GetSession();
                await supplierService.SetPurchaseOrdertatus(Id, session.BranchOfficeId, DocumentStatusID);

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" + "Orden de compras actualizada correctamente" : "desactivada") + "",
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("PurchaseOrdersList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "StatusPurchaseOrder", "PurchaseOrderController", "Problemas al ejecutar al cambiar de estado la factura de compras.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        /// <summary>
        /// Metodo para cargar la vista para el actualizar los purchase
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult EditPurchaseOrder(int Id)
        {
            try
            {
                var session = SecurityService.GetSession();
                PurchaseViewModel Model = new PurchaseViewModel();

                Model.PurchaseOrder = supplierService.GetPurchase(Id, session.BusinessId);
                Model.PurchaseRow = supplierService.GetPurchaseOrderRow(Id);
                Model.Supplier = supplierService.GetSupplier(Model.PurchaseOrder.SuppliersId, session.BusinessId);

                //Obtenemos el balance pendiente
                if (Model.PurchaseOrder.TblDebitNote != null && Model.PurchaseOrder.TblDebitNote.Count > 0)
                    Model.PendingBalance = supplierService.GetPendingBalance(Model.PurchaseOrder.TblDebitNote.Select(x => x.Id).ToList());

                //Cargamos las opciones desplegables
                return View(Model);

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EditPurchaseOrder", "PurchaseOrderController", "Tuvimos un problema al cargar los datos de la orden.");
                return null;
            }
        }

        /// Metodo para edita una ordem
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> UpdatePurchaseOrder(PurchaseViewModel Model)
        {
            var tipoProceso = Model.tipoProceso;
            try
            {
                await supplierService.UpdatePurchase(Model.PurchaseOrder);

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Orden de compras registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("PurchaseOrdersList")

                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdatePurchaseOrder", "PurchaseOrdersController", "Tuvimos un problema al registrar la orden.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Orden de compras registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        /// Metodo para cargar el detalle de la ordem
        /// </summary>
        /// <returns></returns>
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public JsonResult GetPurchaseOrder(long PurchaseId = 0)
        {
            var session = SecurityService.GetSession().BusinessId;
            var bussineId = Convert.ToInt64(session);
            //Luego verifica los campos que retornar que te estan dando error
            var PurchaseList = supplierService.GetPurchOrderRow(PurchaseId).Select(x => new PurchaseViewModel
            {
                Id = x.Id,
                Articulos = x.Produc_Description,
                MeasureId = x.MeasureId,
                Almacen = x.Almacen_Description,
                PurchaseOrderId = x.PurchaseOrderId,
                AlmacenId = x.WarehouseId,
                ITBIS = x.Tax,
                ArticulosId = x.ProductId,
                cantidad = x.Quantity,
                compra = x.Cost,
                descuento = x.Discount,
                Impuesto = x.ITBIS,
                subTotal = (x.Quantity * x.Cost),
                AmountTax = x.AmountTax,
                AmountDiscount = x.AmountDiscount,
                Total = x.Total,
                Delete = "<button type=\"button\" class=\"btn btn-default btn-icon deleteRow\"><i class=\"icon-trash\"></i></button>",//boton de eliminar

            });

            //
            return Json(new
            {
                prices = globalService.ConvertToArray(PurchaseList,
                new string[] { "Delete", "Id", "Articulos", "Almacen", "cantidad", "compra", "descuento", "Impuesto", "subTotal", "ArticulosId", "AlmacenId", "ITBIS", "AmountTax", "MeasureId" })
            });
        }


    }
}