﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuickSal.DataAcces.Contexts;
using QuickSal.Services;
using QuickSal.Web.Api;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.Customer;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static QuickSal.DataAcces.QuickSalEnum;

namespace QuickSal.Web.Controllers
{
    public class CustomerController : Controller
    {
        private ICustomerService customerService;
        private IServiceError serviceError;
        private IGenericMethod genericMethod;
        private IGlobalService globalService;
        private ISecurityService securityService;
        private IAmazonService amazonService;

        public CustomerController(ICustomerService customerService, IServiceError serviceError, ISecurityService securityService,
                                  IGenericMethod genericMethod, IGlobalService globalService, IAmazonService amazonService)
        {
            this.customerService = customerService;
            this.serviceError = serviceError;
            this.genericMethod = genericMethod;
            this.globalService = globalService;
            this.securityService = securityService;
            this.amazonService = amazonService;
        }


        /// <summary>
        /// Metodo para cargar la vista con el listado de clientes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult CustomersList()
        {
            try
            {
                var session = securityService.GetSession();
                CustomerViewModel model = new CustomerViewModel();
                model.Customers = customerService.GetCustomers(session.BusinessId);
                return View(model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CustomersList", "CustomerController", "Error intentando carga la vista de clientes.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para cargar la vista para crear clientes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<IActionResult> CustomerIndex()
        {
            try
            {
                CustomerViewModel model = new CustomerViewModel();
                var session = securityService.GetSession();

                ////Obtenemos un nuevo codigo para el cliente
                string code = customerService.GetLastCustomerCode(session.BusinessId);

                ViewBag.CustomerCode = globalService.GenerateCode(Services.Support.EnumTypes.CodeEntities.Customer, code);
               
                //Cargamos las opciones desplegables
                model.Countries = globalService.GetAllCountries();
                model.DocumentIdentifications = globalService.GetAllDocumentIdentification();
                model.Ncfprefixes = globalService.GetAllPrefixNCF();

                await LoadPictures(model);

                return View(model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateCustomer", "CustomerController", "Tuvimos un problema al cargar la vista para el registro.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para registrar los datos del cliente
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> CreateCustomer(CustomerViewModel model)
        {
            try
            {
                var session = securityService.GetSession();
                var response = customerService.ValidarCode(model.Customer.Code, session.BusinessId);
                if (response)
                {
                    serviceError.LogError("Error", "CreateCustomer", "Customer", "El código del cliente ya existe");
                    return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
                }
                await customerService.CreateCustomer(model.Customer);
                var task = await customerService.Savechanges();

                return Json(new
                {
                    message = ((task.ExecutedSuccesfully) ? "Cliente registrado correctamente" : task.MessageList[0]),
                    status = task.ExecutedSuccesfully,
                    redirect = Url.Action("CustomersList"),
                });

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateCustomer", "Customer", "se presento un problema grabar la información del cliente");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }

        /// <summary>
        /// Metodo para cargar la vista de edición del cliente
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<IActionResult> EditCustomer(long Id)
        {
            try
            {
                var session = securityService.GetSession();
                CustomerViewModel model = new CustomerViewModel();
                long businessID = session.BusinessId;

                model.Customer = customerService.GetCustomer(Id, businessID);

                //Cargamos las opciones desplegables
                model.Countries = globalService.GetAllCountries();
                model.DocumentIdentifications = globalService.GetAllDocumentIdentification();
                model.Ncfprefixes = globalService.GetAllPrefixNCF();

                if (model.Customer.CountryId.HasValue)
                    model.Provinces = globalService.GetAllProvince(model.Customer.CountryId.Value);


                await LoadPictures(model);
                return View(model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EditCustomer", "CustomerController", "Tuvimos un problema al cargar los datos del cliente.");
                return null;
            }
        }


        /// <summary>
        /// Metodo para actualizar los datos del cliente
        /// </summary>
        /// <param name="model">Modelo que contiene la informacion del empleado</param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> UpdateCustomer(CustomerViewModel model)
        {
            try
            {
                var task = await customerService.EditCustomer(model.Customer);
                task = await customerService.Savechanges();

                return Json(new
                {
                    message = ((task.ExecutedSuccesfully) ? "Cliente actualizado correctamente" : task.MessageList[0]),
                    status = task.ExecutedSuccesfully,
                    redirect = Url.Action("CustomersList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EditCustomer", "CustomerController", "se presento un problema al actualizar la información del cliente");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }


        /// <summary>
        /// Metodo para actualizar el estatus del cliente
        /// </summary>
        /// <param name="status">Estado al que pasará el cliente</param>
        /// <param name="Id">Primary key del cliente</param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> StatusCustomer(bool status, long Id)
        {
            try
            {
                var session = securityService.GetSession();
                customerService.StatusCustomer(status, Id, session.BusinessId);

                var task = await customerService.Savechanges();

                return Json(new
                {
                    message = ((task.ExecutedSuccesfully) ? "Cliente actualizado correctamente" : task.MessageList[0]),
                    status = task.ExecutedSuccesfully,
                    redirect = Url.Action("CustomersList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "StatusCustomer", "CustomerController", "se presento un problema al actualizar la información del cliente");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }



        /// <summary>
        /// Proceso para guardar la foto del producto en el sistema
        /// </summary>
        /// <returns></returns>
        [ServiceFilter(typeof(LoginSecurityFilter))]
        public async Task<JsonResult> SaveUploadedPicture(IFormFile files)
        {
            try
            {
                var session = securityService.GetSession();
                string guid = Guid.NewGuid().ToString();
                string url = string.Empty;

                TblTempFile file = new TblTempFile();
                file.FileName = guid + files.FileName;
                file.TypeFile = (byte)TypeFiles.CustomerPhoto;
                file.UserId = session.UserId;

                MemoryStream ms = new MemoryStream();
                files.CopyTo(ms);
                await globalService.AddTempFile(file, ms);

                var task = await globalService.Savechanges();

                if (task.ExecutedSuccesfully)
                    url = await globalService.GetTempUrlObjet(file.FileName);


                return Json(new
                {
                    message = ((task.ExecutedSuccesfully) ? "Foto cargada correctamente" : task.MessageList[0]),
                    File = url,
                    fileName = file.FileName,
                    status = task.ExecutedSuccesfully
                });

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "SaveUploadedPicture", "Customer", "Se presento un problema al cargar la foto");
                return Json(new { message = serviceError.TaskStatus().MessageList[0], status = false });
            }
        }


        /// <summary>
        /// Metodo para cargar las imagenes que usa el formulario
        /// </summary>
        /// <param name="model"></param>
        private async Task LoadPictures(CustomerViewModel model)
        {
            if (model.Customer != null && !string.IsNullOrEmpty(model.Customer.Picture))
            {
                if (!string.IsNullOrEmpty(model.Customer.Picture))
                    model.UrlPhoto = await amazonService.GetUrlObjet(model.Customer.Picture, TypeFiles.CustomerPhoto);
            }
            else
            {
                var productPicture = await globalService.GetSettingValue("amazon", "ProfileDefault");
                model.UrlPhoto = productPicture.FirstOrDefault().SettingValue;
            }
        }
    }
}