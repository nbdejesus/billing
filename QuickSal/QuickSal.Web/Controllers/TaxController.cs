﻿using Microsoft.AspNetCore.Mvc;
using QuickSal.Services;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.Accounting;
using System;

namespace QuickSal.Web.Controllers
{
    public class TaxController : Controller
    {
        private IServiceError serviceError;
        private ISecurityService SecurityService;
        private IAccountingService accountingService;

        public TaxController(IServiceError serviceError,
                                  ISecurityService SecurityService,
                                  IAccountingService accountingService)
        {
            this.serviceError = serviceError;
            this.SecurityService = SecurityService;
            this.accountingService = accountingService;
        }


        /// <summary>
        /// Metodo para mostrar el listado de impuestos
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult TaxList()
        {
            try
            {
                TaxViewModel Model = new TaxViewModel();
                var session = SecurityService.GetSession();

                Model.Taxes = accountingService.GetTaxes(session.BusinessId);
                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "TaxList", "TaxController", "Tuvimos un problema cargar listado de impuestos.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para cargar la vista de registro de impuesto
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public PartialViewResult _TaxIndex()
        {
            return PartialView();
        }

        /// <summary>
        /// Metodo para registrar un impuesto
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult CreateTax(TaxViewModel Model)
        {
            try
            {
                accountingService.CreateTax(Model.Tax);
                accountingService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Impuesto registrado correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateTax", "TaxController", "Tuvimos un problema al registrar el impuesto.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Impuesto registrado correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }


        /// <summary>
        /// Metodo para cargar la vista para actualizar el impuesto
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult _EditTax(int Id)
        {
            try
            {
                TaxViewModel Model = new TaxViewModel();
                Model.Tax = accountingService.GetTax(Id);
                return PartialView(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "_EditTax", "TaxController", "Error intentando carga los datos del impuesto.");
                return null;
            }
        }


        /// <summary>
        /// metodo para actualizar un impuesto
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult UpdateTax(TaxViewModel Model)
        {
            try
            {
                accountingService.UpdateTax(Model.Tax);
                accountingService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Impuesto actualizado correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateTax", "TaxController", "Tuvimos un problema al actualizar el impuesto.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Impuesto actualizado correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        /// <summary>
        /// Metodo para cambiar de estatus de un impuesto
        /// </summary>
        /// <param name="Id">Primary key de la categoría </param>
        /// <param name="status">Estatus de la categoría </param>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult StatusTax(int Id, bool status)
        {
            try
            {
                accountingService.SetTaxStatus(Id, status);
                accountingService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Impuesto " + ((status) ? "habilitado" : "desactivado") + " correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("TaxList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "StatusTax", "TaxController", "Tuvimos un problema al " + ((status) ? "activar" : "Desactivar") + " el impuesto.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }
    }
}