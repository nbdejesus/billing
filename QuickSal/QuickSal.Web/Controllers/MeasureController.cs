﻿using Microsoft.AspNetCore.Mvc;
using QuickSal.Services;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.Inventory;
using System;
using System.Linq;

namespace QuickSal.Web.Controllers
{
    public class MeasureController : Controller
    {
        private IServiceError serviceError;
        private ISecurityService SecurityService;
        private IInventoryService inventoryService;

        public MeasureController(IServiceError serviceError,
                                  ISecurityService SecurityService,
                                  IInventoryService inventoryService)
        {
            this.serviceError = serviceError;
            this.SecurityService = SecurityService;
            this.inventoryService = inventoryService;
        }


        /// <summary>
        /// Metodo para mostrar el listado de medidas
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult MeasureList()
        {
            try
            {
                MeasureViewModel Model = new MeasureViewModel();
                var session = SecurityService.GetSession();

                Model.Measurements = inventoryService.GetMeasurements(session.BusinessId);
                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "MeasureList", "CategoryController", "Tuvimos un problema cargar listado de medidas.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para cargar la vista de registro de medidas
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public PartialViewResult _MeasureIndex()
        {
            return PartialView();
        }

        /// <summary>
        /// Metodo para registrar una medida
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult CreateMeasure(MeasureViewModel Model)
        {
            try
            {
                inventoryService.CreateMeasure(Model.Measure);
                inventoryService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Medida registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateMeasure", "Measure", "Tuvimos un problema al registrar la medida.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "medida registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }


        /// <summary>
        /// Metodo para cargar la vista para el actualizar ua medida
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult _EditMeasure(int Id)
        {
            try
            {
                MeasureViewModel Model = new MeasureViewModel();
                Model.Measure = inventoryService.GetMeasure(Id);
                return PartialView(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EditMeasure", "MeasureController", "Error intentando carga los datos de la medida.");
                return null;
            }
        }


        /// <summary>
        /// metodo para actualizar una medida
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult UpdateMeasure(MeasureViewModel Model)
        {
            try
            {
                inventoryService.UpdateMeasure(Model.Measure);
                inventoryService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Medida actualizada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateMeasure", "measure", "Tuvimos un problema al actualizar la medida.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Medida actualizada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        /// <summary>
        /// Metodo para cambiar de estatus de la medida
        /// </summary>
        /// <param name="Id">Primary key de la medida</param>
        /// <param name="status">Estatus de la medida</param>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult StatusMeasure(int Id, bool status)
        {
            try
            {
                inventoryService.SetMeasureStatus(Id, status);
                inventoryService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Medida " + ((status) ? "habilitada" : "desactivada") + " correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("MeasureList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "StatusMeasure", "measure", "Tuvimos un problema al " + ((status) ? "activar" : "Desactivar") + " la medida.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }
    }
}