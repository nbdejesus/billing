﻿ using Microsoft.AspNetCore.Mvc;
using QuickSal.DataAcces.Models;
using QuickSal.Services;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.Purchase;
using QuickSal.Web.ViewModels.VendorPayment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.Web.Controllers
{
    public class VendorPaymentController : Controller
    {
        private readonly IServiceError serviceError;
        private readonly ISecurityService SecurityService;
        private readonly IAccountingService accountingService;
        private readonly ISupplierService supplierService;
        private readonly IInventoryService inventoryService;
        private readonly IGlobalService globalService;
        public VendorPaymentController(IServiceError serviceError, ISecurityService SecurityService, IAccountingService accountingService, ISupplierService supplierService, IInventoryService inventoryService, IGlobalService globalService)
        {
            this.serviceError = serviceError;
            this.SecurityService = SecurityService;
            this.accountingService = accountingService;
            this.supplierService = supplierService;
            this.inventoryService = inventoryService;
            this.globalService = globalService;
        }

        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult VendorPaymentList()
        {
            try
            {
                VendorPaymentViewModel Model = new VendorPaymentViewModel();
                //long session = SecurityService.GetSession().BusinessId;
                //long bussined = Convert.ToInt64(session);
                var session = SecurityService.GetSession();
                Model.ListVendorPayment = supplierService.GetVendorPayment(session.BranchOfficeId);
                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "VendorPaymentList", "VendorPaymentController", "Tuvimos un problema cargar listado de Pagos.");
                return null;
            }
        }

        /// <summary>
        /// Metodo para cargar la vista de registro de ncf
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult _OrdenCompraList()
        {
            try
            {
                PurchaseViewModel Model = new PurchaseViewModel();
                var session = SecurityService.GetSession();
                Model.PurchaseOrders = supplierService.VendorPurchaseOrders(session.BranchOfficeId);
                ////Obtenemos un nuevo codigo para el cliente
                //string code = supplierService.GetLastVendorPayment(bussined);
                //ViewBag.VendorPayment = globalService.GenerateCode(Services.Support.EnumTypes.CodeEntities.VendorPayment, code);

                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "PurchaseOrdersList", "PurchaseOrdersController", "Tuvimos un problema cargar listado de ordenes.");
                return null;
            }
        }

        // GET: VendorPayment
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult VendorPaymentCreate(string data)
        {

            List<string> listado = data.Split(',').ToList();
            IEnumerable<long> datos = listado.Select(long.Parse);

            VendorPaymentRowVM vendorPayment = new VendorPaymentRowVM();
            long session = SecurityService.GetSession().BusinessId;
            long bussined = Convert.ToInt64(session);
            vendorPayment.TblVendorPaymentRow = supplierService.PurchaseRow(bussined, datos);

            try
            {
                ViewBag.Fecha = DateTime.Now;
                return View(vendorPayment);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "VendorPaymentCreate", "VendorPaymentController", "Tuvimos un problema cargar listado de Pagos.");
                return null;
            }
        }

        // GET: VendorPayment
        /// Metodo para cambiar el estado del PurchaseOrder
        /// </summary>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult StatusPayment(int Id, long DocumentStatusID)
        {
            try
            {
                long bussineId = SecurityService.GetSession().BusinessId;
                DataAcces.Models.TaskResult task = supplierService.SetVendorPaymentstatus(Id, bussineId, DocumentStatusID);
                if (task.ExecutedSuccesfully)
                {
                    supplierService.Savechanges();

                }
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Pagos de orden guardada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("VendorPaymentList")
                });

            }
            catch (Exception)
            {
                //serviceError.LogError(ex.Message, "StatusPurchaseOrder", "PurchaseOrderController", "Tuvimos un problema al " + ((DocumentStatusID) ? "activar" : "Desactivar") + " el Ncf.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        [ServiceFilter(typeof(LoginSecurityFilter))]
        public JsonResult GetVendorPayment(long PurchaseId = 0)
        {
            long session = SecurityService.GetSession().BusinessId;
            long bussineId = Convert.ToInt64(session);
            //Luego verifica los campos que retornar que te estan dando error
            IEnumerable<VendorPaymentViewModel> PurchaseList = supplierService.GetVendorPaymentRow(PurchaseId).Select(x => new VendorPaymentViewModel
            {
                Id = x.Id,
                VendorPaymentId = x.VendorPaymentId,
                SuppliersId = x.SuppliersId,
                PurchaseOrderId = x.PurchaseOrderId,
                PurchaseOrderDocument = x.PurchaseOrderDocument,
                SupplierName = x.SupplierName,
                Concept = x.Concept,
                Owed = x.Owed.ToString("N2"),
                PaidOut =x.PaidOut.ToString("N2"),
                Payment = x.Payment.ToString("N2"),
                Pending = x.Pending.ToString("N2"),
                Delete = "<ul class=\"icons-list\"><li class=\"dropdown\"><a href = \"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"icon-menu9\"></i></a><ul class=\"dropdown-menu dropdown-menu-right\"><li><a type=\"button\" class=\"btn btn-default btn-icon modificarProducto\" data-id=\"" + x.Id + "\" ><i class=\"icon-pencil6\"></i>Editar</a><a type=\"button\" class=\"btn btn-default btn-icon deleteRow\"><i class=\"icon-trash\"></i>Eliminar</a></ul></li></ul>",
            });

            //
            return Json(new
            {
                prices = globalService.ConvertToArray(PurchaseList,
                new string[] { "Id", "VendorPaymentId", "PurchaseOrderId", "SuppliersId", "PurchaseOrderDocument", "SupplierName", "Concept", "Owed", "PaidOut", "Payment", "Pending", "Delete" })
            });
        }

        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult _EditVendorPayment(int Id)
        {
            try
            {
                long session = SecurityService.GetSession().BusinessId;
                long bussineId = Convert.ToInt64(session);
                VendorPaymentViewModel Model = new VendorPaymentViewModel
                {
                    VendorPayment = supplierService.GetVendorPaymentId(Id, bussineId)
                };
                //Cargamos las opciones desplegables
                return View(Model);

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EditPurchaseOrder", "PurchaseOrderController", "Tuvimos un problema al cargar los datos de la orden.");
                return null;
            }
        }

        // GET: VendorPayment/Details/5
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> CreateVendorPayment(VendorPaymentViewModel Model)
        {
            long session = SecurityService.GetSession().BusinessId;
            long bussined = Convert.ToInt64(session);
            string code = supplierService.GetLastVendorPayment(bussined);
            Model.VendorPayment.DocumentNumber = globalService.GenerateCode(Services.Support.EnumTypes.CodeEntities.VendorPayment, code);

            try
            {
                supplierService.VendorPaymentCreate(Model.VendorPayment);

                var task = await supplierService.Savechanges();
                if (task.ExecutedSuccesfully)
                {
                    if (Model.VendorPayment.DocumentEstatusId == 1)
                    {
                        supplierService.MovimientoPurchase(Model.VendorPayment.Id);
                    }
                }
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Pagos de Proveedores registrado correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("VendorPaymentList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "VendorPaymentCreate", "VendorPaymentController", "Tuvimos un problema al registrar la orden.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Orden de compras registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult UpdateVendorPayment(VendorPaymentViewModel Model)
        {
            long session = SecurityService.GetSession().BusinessId;
            long bussineId = Convert.ToInt64(session);

            try
            {
                TaskResult task = supplierService.VendorPaymentUpdate(Model.VendorPayment);
                if (task.ExecutedSuccesfully)
                {
                    supplierService.Savechanges();


                    //if (Model.VendorPayment.DocumentEstatusId == 1)
                    //{
                    //    supplierService.MovimientoPurchase(Model.VendorPayment.Id);
                    //}
                }
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Pagos de orden guardada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("VendorPaymentList")

                });


            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdatePurchaseOrder", "PurchaseOrdersController", "Tuvimos un problema al registrar la orden.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Orden de compras registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult EditVendorPaymentRow(long Id)
        {
            try
            {
                PurchaseViewModel Model = new PurchaseViewModel();
                long session = SecurityService.GetSession().BusinessId;
                long bussined = Convert.ToInt64(session);
                Model.PurchaseOrders = supplierService.VendorPurchaseOrders(bussined).Where(x => x.Id == Id);
                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "PurchaseOrdersList", "PurchaseOrdersController", "Tuvimos un problema cargar listado de ordenes.");
                return null;
            }

        }

    }
}