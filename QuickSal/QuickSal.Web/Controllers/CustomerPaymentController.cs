﻿using Microsoft.AspNetCore.Mvc;
using QuickSal.Services;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.CustomerPayment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.Web.Controllers
{
    public class CustomerPaymentController : Controller
    {
        private readonly IServiceError serviceError;
        private readonly ISecurityService SecurityService;
        private readonly IAccountingService accountingService;
        private readonly ICustomerService  _customerService;
        private readonly IInventoryService inventoryService;
        private readonly IGlobalService globalService;

        public CustomerPaymentController(IServiceError serviceError, ISecurityService SecurityService, IAccountingService accountingService, ICustomerService  customerService, IInventoryService inventoryService, IGlobalService globalService)
        {
            this.serviceError = serviceError;
            this.SecurityService = SecurityService;
            this.accountingService = accountingService;
            _customerService =  customerService;
            this.inventoryService = inventoryService;
            this.globalService = globalService;
        }

        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult CustomerPaymentList()
        {
            try
            {
                CustomerPaymentViewModel Model = new CustomerPaymentViewModel();
                long session = SecurityService.GetSession().BusinessId;
                long bussined = Convert.ToInt64(session);
                Model.List_Customer = _customerService.GetCustomerPayments(bussined);
                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CustomerPaymentList", "CustomerPaymentController", "Tuvimos un problema cargar listado de Pagos.");
                return null;
            }
        }

        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult _InvoiceList()
        {
            try
            {
                CustomerPaymentViewModel Model = new CustomerPaymentViewModel();
                long session = SecurityService.GetSession().BusinessId;
                long bussined = Convert.ToInt64(session);
                Model.List_Invoice = _customerService.GetInvoice(bussined);
                ////Obtenemos un nuevo codigo para el cliente
                //string code = supplierService.GetLastVendorPayment(bussined);
                //ViewBag.VendorPayment = globalService.GenerateCode(Services.Support.EnumTypes.CodeEntities.VendorPayment, code);

                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "_InvoiceList", "CustomerPaymentController", "Tuvimos un problema cargar listado de ordenes.");
                return null;
            }
        }

        // GET: VendorPayment
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult CustomerPaymentCreate(string cate )
        {
            List<string> listado = cate.Split(',').ToList();
            IEnumerable<long> datos = listado.Select(long.Parse);

            CustomerPaymentViewModel customerPayment = new CustomerPaymentViewModel();
            long session = SecurityService.GetSession().BusinessId;
            long bussined = Convert.ToInt64(session);
            customerPayment.List_CustomerPaymentsRow = _customerService.InvoiceRow(bussined, datos);

            try
            {
                ViewBag.Fecha = DateTime.Now;
                //return RedirectToAction("CustomerPaymentCreate", customerPayment);

                return View(customerPayment);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CustomerPaymentCreate", "VendorPaymentController", "Tuvimos un problema cargar listado de Pagos.");
                return null;
            }

        }

         
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult StatusPayment(int Id, long DocumentStatusID)
        {
            try
            {
                long bussineId = SecurityService.GetSession().BusinessId;
                DataAcces.Models.TaskResult task = _customerService.SetCustomerPaystatus(Id, bussineId, DocumentStatusID);
                if (task.ExecutedSuccesfully)
                {
                    _customerService.Savechanges();
                }
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Pagos de cliente guardada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("CustomerPaymentList")
                });

            }
            catch (Exception)
            {
                //serviceError.LogError(ex.Message, "StatusPurchaseOrder", "PurchaseOrderController", "Tuvimos un problema al " + ((DocumentStatusID) ? "activar" : "Desactivar") + " el Ncf.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        [ServiceFilter(typeof(LoginSecurityFilter))]
        public JsonResult GetCustomerPayment(long CustomerId = 0)
        {
            long session = SecurityService.GetSession().BusinessId;
            long bussineId = Convert.ToInt64(session);
            //Luego verifica los campos que retornar que te estan dando error
            IEnumerable<CustomerPaymentViewModel> PurchaseList = _customerService.GetCustomerPaymentsRow(CustomerId).Select(x => new CustomerPaymentViewModel
            {
                Id = x.Id,
                CustomerPaymentId = x.CustomerPaymentId,
                CustomerId = x.CustomerId,
                InvoiceId = x.InvoiceId,                
                InvoiceDocument = x.InvoiceDocument,
                CustomerName = x.CustomerName,
                Concept = x.Concept,
                Owed = x.Owed,
                PaidOut = x.PaidOut,
                Payment = x.Payment,
                Pending = x.Pending,
                Delete = "<ul class=\"icons-list\"><li class=\"dropdown\"><a href = \"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"icon-menu9\"></i></a><ul class=\"dropdown-menu dropdown-menu-right\"><li><a type=\"button\" class=\"btn btn-default btn-icon modificarProducto\" data-id=\"" + x.Id + "\" ><i class=\"icon-pencil6\"></i>Editar</a><a type=\"button\" class=\"btn btn-default btn-icon deleteRow\"><i class=\"icon-trash\"></i>Eliminar</a></ul></li></ul>",
            });
            return Json(new
            {
                prices = globalService.ConvertToArray(PurchaseList,
                new string[] { "Id", "CustomerPaymentId", "CustomerId", "InvoiceId",  "InvoiceDocument", "CustomerName", "Concept", "Owed", "PaidOut", "Payment", "Pending", "Delete" })
            });
        }

        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult _EditCustomerPayment(int Id)
        {
            try
            {
                long session = SecurityService.GetSession().BusinessId;
                long bussineId = Convert.ToInt64(session);
                CustomerPaymentViewModel Model = new CustomerPaymentViewModel
                {
                    customerPayment = _customerService.GetCustomerPaymentId(Id, bussineId)
                };
                //Cargamos las opciones desplegables
                return View(Model);

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EditCustomer", "CustomerController", "Tuvimos un problema al cargar los datos de la orden.");
                return null;
            }
        }
   
        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> SaveCustomerPayment(CustomerPaymentViewModel Model)
        {
            
             try
            {
                _customerService.CustomerPaymentCreate(Model.customerPayment);
                DataAcces.Models.TaskResult task = await _customerService.Savechanges();

                if (task.ExecutedSuccesfully)
                {
                    if (Model.customerPayment.DocumentEstatusId == 1)
                    {
                    //    supplierService.MovimientoPurchase(Model.VendorPayment.Id);
                    }
                }
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Pagos de Cliente registrado correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("CustomerPaymentList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "SaveCustomerPayment", "CustomerPaymentController", "Tuvimos un problema al registrar la orden.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Orden de compras registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        [HttpPost]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult UpdateCustomerPayment(CustomerPaymentViewModel Model)
        {
            long session = SecurityService.GetSession().BusinessId;
            long bussineId = Convert.ToInt64(session);
            try
            {
                DataAcces.Models.TaskResult task = _customerService.CustomerPaymentUpdate(Model.customerPayment);
                if (task.ExecutedSuccesfully)
                {
                    _customerService.Savechanges();
                }
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Pagos del cliente guardada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("CustomerPaymentList")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdatePurchaseOrder", "PurchaseOrdersController", "Tuvimos un problema al registrar el pago.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Orden de compras registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult EditCustomerPaymentRow(long Id)
        {
            try
            {
                CustomerPaymentViewModel Model = new CustomerPaymentViewModel();
                long session = SecurityService.GetSession().BusinessId;
                long bussined = Convert.ToInt64(session);
                Model.List_Invoice = _customerService.GetInvoice(bussined).Where(x => x.Id == Id);
                return View(Model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "InvoiceList", "CustomerPaymentController", "Tuvimos un problema cargar listado de ordenes.");
                return null;
            }

        }


    }

}