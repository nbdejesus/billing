﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuickSal.Services;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.LoadData;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static QuickSal.DataAcces.QuickSalEnum;

namespace QuickSal.Web.Controllers
{
    public class LoadDataController : Controller
    {
        private readonly IServiceError serviceError;
        private readonly ISecurityService SecurityService;
        private IFileHandleService FileHandleService;

        public LoadDataController(IServiceError serviceError, ISecurityService SecurityService, IFileHandleService FileHandleService)
        {
            this.serviceError = serviceError;
            this.SecurityService = SecurityService;
            this.FileHandleService = FileHandleService;
        }

        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult UploadList()
        {
            return View();
        }

        /// <summary>
        /// Metodo para obtener el emplate elegido
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<JsonResult> DownloadTemplate(TemplateVM model)
        {
            try
            {
                if (model.DocumentTemplateId == (int)TypeFiles.TemplateCustomer)
                {
                    var excel = await FileHandleService.GetCustomerTemplate();
                    byte[] bytes = excel.ToArray();

                    return Json(new
                    {
                        name = "Plantilla Clientes",
                        status = true,
                        file = bytes,
                        format = "xlsx"
                    });
                }
                else if (model.DocumentTemplateId == (int)TypeFiles.TemplateSupplier)
                {
                    var excel = await FileHandleService.GetSuplierTemplate();
                    byte[] bytes = excel.ToArray();

                    return Json(new
                    {
                        name = "Plantilla Proveedores",
                        status = true,
                        file = bytes,
                        format = "xlsx"
                    });
                }
                else if (model.DocumentTemplateId == (int)TypeFiles.TemplateProduct)
                {
                    var excel = await FileHandleService.GetProductTemplate();
                    byte[] bytes = excel.ToArray();

                    return Json(new
                    {
                        name = "Plantilla Productos",
                        status = true,
                        file = bytes,
                        format = "xlsx"
                    });
                }
                else
                {
                    return Json(new
                    {
                        name = "Plantilla Proveedores",
                        status = true,
                        format = "xlsx"
                    });
                }
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "DownloadTemplate", "LoadData", "Se presentó un problema al intentar descargar la plantilla.");
                return Json(new { status = false });
            }
        }

        /// <summary>
        /// Metodo para obtener el emplate elegido
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public async Task<PartialViewResult> _ProcessTemplate(TemplateVM model)
        {
            MemoryStream ms = new MemoryStream();
            model.Template.CopyTo(ms);

            var modelResult = await FileHandleService.ProcessTemplate(ms);

            return PartialView(modelResult);
        }
    }
}