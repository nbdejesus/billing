﻿using Microsoft.AspNetCore.Mvc;
using QuickSal.Services;
using QuickSal.Web.Api;
using QuickSal.Web.Infraestructure.Security;
using QuickSal.Web.ViewModels.BranchOffice;
using System;
using System.Linq;

namespace QuickSal.Web.Controllers
{
    public class BranchOfficeController : Controller
    {
        private IGlobalService globalService;
        private IAccountingService accountingService;
        private IBusinessService businessService;
        private IGenericMethod _genericMethod;
        private IServiceError serviceError;
        private ISecurityService SecurityService;

        public BranchOfficeController(IGlobalService globalService,
                                      IGenericMethod genericMethod,
                                      IServiceError serviceError,
                                      IBusinessService businessService,
                                      IAccountingService accountingService,
                                      ISecurityService SecurityService)
        {
            this.globalService = globalService;
            this._genericMethod = genericMethod;
            this.serviceError = serviceError;
            this.businessService = businessService;
            this.accountingService = accountingService;
            this.SecurityService = SecurityService;
        }

        #region Sucursal
        /// <summary>
        /// Metodo para cargar el listado de sucursales
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult BranchOfficeList()
        {
            try
            {
                BranchOfficeViewModel model = new BranchOfficeViewModel();
                var session = SecurityService.GetSession();
                model.BranchOffices = businessService.getBranchOffices(session.BusinessId);
                return View(model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "BranchOfficeList", "BranchOffice", "Error intentando carga el listado de sucursales.");
                return null;
            }
        }

        /// <summary>
        /// Proceso para cargar la vista para crear una sucursal
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult BranchOfficeIndex()
        {
            try
            {
                BranchOfficeViewModel model = new BranchOfficeViewModel();
                var session = SecurityService.GetSession();
                var business = businessService.GetBusinessById(session.BusinessId);
                
                model.BranchOffices = businessService.getBranchOffices(session.BusinessId);
                model.cant = model.BranchOffices.Count();

                if (business.CountryId.HasValue)
                    model.Provinces = globalService.GetAllProvince(business.CountryId.Value);

                return View(model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateBusiness", "BranchOffice", "Error intentando carga registro de la sucursal.");
                return null;
            }
        }

        /// <summary>
        /// metodo para registrar una sucursal en el sistema
        /// </summary>
        /// <param name="model">Modelo que contiene la información de la sucursal</param>
        /// <returns></returns>    
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult CreateBranchOffice(BranchOfficeViewModel model)
        {
            try
            {
                businessService.CreateBranchOffice(model.BranchOffice);
                var task = businessService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Sucursal registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("BranchOfficeList", "branchOffice")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateBranchOffice", "BranchOffice", "Tuvimos un problema al registrar tu sucursal.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Sucursal registrada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        /// <summary>
        /// Metodo para cargar la vista que actualiza la sucursal
        /// </summary>
        /// <param name="branchOfficeId">Primary key del a sucursal</param>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public IActionResult EditBranchOffice(long branchOfficeId)
        {
            try
            {
                BranchOfficeViewModel model = new BranchOfficeViewModel();
                var session = SecurityService.GetSession();
                var business = businessService.GetBusinessById(session.BusinessId);

                model.BranchOffices = businessService.getBranchOffices(session.BusinessId);
                model.cant = model.BranchOffices.Count();

                if (business.CountryId.HasValue)
                    model.Provinces = globalService.GetAllProvince(business.CountryId.Value);

                model.BranchOffice = businessService.GetBranchOffice(branchOfficeId);

                return View(model);
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "EditBranchOffice", "BranchOffice", "Error intentando cargar los datos.");
                return null;
            }
        }


        /// <summary>
        /// Proceso para actualizar una sucursal en el sistema
        /// </summary>
        /// <param name="model">Modelo que contiene la información de la sucursal</param>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult UpdateBranchOffice(BranchOfficeViewModel model)
        {
            try
            {
                businessService.UpdateBranchOffice(model.BranchOffice);

                var task = businessService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Sucursal actualizada correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("BranchOfficeList", "branchOffice")
                });

            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateBranchOffice", "BranchOffice", "Tuvimos un problema al registrar tu sucursal.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        /// <summary>
        /// Metodo para cambiar de estatus a la sucursal
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ServiceFilter(typeof(HandledSecurityFilter))]
        public JsonResult StatusBranchOffice(long branchOfficeId, bool status)
        {
            try
            {
                businessService.SetBranchOfficeStatus(branchOfficeId, status);
                var task = businessService.Savechanges();

                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "Sucursal " + ((status) ? "activada" : "Desactivada") + " correctamente" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully,
                    redirect = Url.Action("BranchOfficeList", "branchOffice")
                });
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "UpdateBranchOffice", "BranchOffice", "Tuvimos un problema al " + ((status) ? "activada" : "Desactivada") + " tu sucursal.");
                return Json(new
                {
                    message = ((serviceError.TaskStatus().ExecutedSuccesfully) ? "" : serviceError.TaskStatus().MessageList[0]),
                    status = serviceError.TaskStatus().ExecutedSuccesfully
                });
            }
        }

        #endregion

    }
}