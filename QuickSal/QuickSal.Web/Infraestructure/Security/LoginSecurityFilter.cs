﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Newtonsoft.Json;
using QuickSal.Services;
using QuickSal.Services.Support;
using System.Text;

namespace QuickSal.Web.Infraestructure.Security
{
    public class LoginSecurityFilter : IActionFilter
    {
        private IHttpContextAccessor httpContextAccessor;
        private ISecurityService securityService;
        private IGlobalService globalService;

        public LoginSecurityFilter(IHttpContextAccessor httpContextAccessor, ISecurityService securityService, IGlobalService globalService)
        {
            this.httpContextAccessor = httpContextAccessor;
            this.securityService = securityService;
            this.globalService = globalService;
        }

        public void OnActionExecuted(ActionExecutedContext context)
        { }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            string sessionData = httpContextAccessor.HttpContext.Request.Cookies["Quicksal"];

            if (!string.IsNullOrEmpty(sessionData))
            {
                string ressultado = globalService.TripleDesDescrypt(sessionData);
                securityService.LoadSesion(JsonConvert.DeserializeObject<SessionViewModel>(globalService.TripleDesDescrypt(sessionData)));
            }
            else
            {
                if (globalService.IsAjaxRequest(context.HttpContext.Request))
                {
                    var message = Encoding.UTF8.GetBytes("FinishedSession");          
                    context.HttpContext.Response.OnStarting(async () =>
                    {
                        context.HttpContext.Response.StatusCode = 429;
                        await context.HttpContext.Response.Body.WriteAsync(message, 0, message.Length);
                    });

                    context.Result = new BadRequestObjectResult("Object is null");
                    return;
                }
                else
                    context.Result = new RedirectToRouteResult(new RouteValueDictionary { { "Controller", "account" }, { "Action", "AccessDenied" } });

            }
        }
    }
}
