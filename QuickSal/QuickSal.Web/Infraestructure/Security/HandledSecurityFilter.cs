﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Newtonsoft.Json;
using QuickSal.Services;
using QuickSal.Services.Support;
using System.Text;

namespace QuickSal.Web.Infraestructure.Security
{
    public class HandledSecurityFilter : IActionFilter
    {
        private IHttpContextAccessor httpContextAccessor;
        private ISecurityService securityService;
        private IGlobalService globalService;
        private IEmployeeService employeeService;

        public HandledSecurityFilter(IHttpContextAccessor httpContextAccessor, ISecurityService securityService, IGlobalService globalService,
                                   IEmployeeService employeeService)
        {
            this.httpContextAccessor = httpContextAccessor;
            this.securityService = securityService;
            this.globalService = globalService;
            this.employeeService = employeeService;
        }

        public void OnActionExecuted(ActionExecutedContext context)
        { }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            string sessionData = httpContextAccessor.HttpContext.Request.Cookies["Quicksal"];

            if (!string.IsNullOrEmpty(sessionData))
            {
                //Acción actual que esta ejecutando el usuario                
                string controller = context.RouteData.Values["controller"].ToString();
                string Action = context.RouteData.Values["action"].ToString();

                string ressultado = globalService.TripleDesDescrypt(sessionData);
                var session = JsonConvert.DeserializeObject<SessionViewModel>(globalService.TripleDesDescrypt(sessionData));

                //Refrescamos la sesión
                securityService.LoadSesion(session);

                //Validamos si esta logueado como empleado
                if (session.EmployeeId != 0)
                {
                    //var subObject = securityService.GetSubObject(Action, controller);
                    var dataEmployee = employeeService.GetEmployee(session.EmployeeId, session.BusinessId);

                    //validamos si tiene acceso algun objeto
                    //if (subObject != null)
                    //{
                    var security = securityService.GetSecurity(dataEmployee.PrivilegesId, Action, controller);

                    if (security != null)
                    {
                        if (!security.AplicationMetod)
                        {
                            if (security != null)
                            { }
                            else
                                AccessDenied(context, EnumTypes.StatusCode.AccessDenied);
                        }
                    }
                    else
                        AccessDenied(context, EnumTypes.StatusCode.AccessDenied);

                }
            }
            else
            {
                AccessDenied(context, EnumTypes.StatusCode.FinishedSession);
            }
        }

        /// <summary>
        /// Metodo para devolver acceso denegado
        /// </summary>
        /// <param name="context"></param>
        private void AccessDenied(ActionExecutingContext context, EnumTypes.StatusCode Code)
        {
            if (globalService.IsAjaxRequest(context.HttpContext.Request))
            {
                var message = Encoding.UTF8.GetBytes("FinishedSession");
                context.HttpContext.Response.OnStarting(async () =>
                {
                    context.HttpContext.Response.StatusCode = (int)Code;
                    await context.HttpContext.Response.Body.WriteAsync(message, 0, message.Length);
                });

                context.Result = new BadRequestObjectResult("Object is null");
                return;
            }
            else
                context.Result = new RedirectToRouteResult(new RouteValueDictionary { { "Controller", "account" }, { "Action", "AccessDenied" } });
        }
    }
}
