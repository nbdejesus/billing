﻿using QuickSal.DataAcces.Contexts;
using System.Collections.Generic;
using System.Linq;

namespace QuickSal.Web.ViewModels.Supplier
{
    public class SupplierViewModel
    {
        public TblSuppliers Supplier { get; set; }
        public IQueryable<TblSuppliers> Suppliers { get; set; }

        public IEnumerable<TblProvince> Provinces { get; set; }
        public IEnumerable<TblCountry> Countries { get; set; }
        public IEnumerable<TblDocumentIdentification> DocumentIdentifications { get; set; }
        public IEnumerable<TblNcfprefix> Ncfprefixes { get; set; }

        /// <summary>
        /// Retorna la url de la foto
        /// </summary>
        public string UrlPhoto { get; set; }
    }
}
