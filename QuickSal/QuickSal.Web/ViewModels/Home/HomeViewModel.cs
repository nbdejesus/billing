﻿using QuickSal.DataAcces.Contexts;
using QuickSal.Web.ViewModels.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.Web.ViewModels.Home
{
    public class HomeViewModel
    {
        public SessionViewModel SessionDashboard { get; set; }
    }
}
