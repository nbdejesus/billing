﻿using QuickSal.DataAcces.Contexts;
using System.Linq;

namespace QuickSal.Web.ViewModels.Accounting
{
    public class TaxViewModel
    {
        /// <summary>
        /// Obtiene el listado de impuestos
        /// </summary>
        public IQueryable<TblTax> Taxes { get; set; }

        /// <summary>
        /// Obtiene o estable los datos de un impuesto
        /// </summary>
        public TblTax Tax { get; set; }
    }
}
