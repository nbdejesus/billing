﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.Web.ViewModels.CustomerPayment
{
    public class CustomerPaymentViewModel
    {
         public long Id { get; set; }
        public long BranchOfficeId { get; set; }
        public byte DocumentEstatusId { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Observation { get; set; }
        public decimal Total { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }
        public long CustomerPaymentId { get; set; }
        public long CustomerId { get; set; }
        public long InvoiceId { get; set; }
        public string InvoiceDocument { get; set; }
        public string CustomerName { get; set; }
        public string Concept { get; set; }
        public decimal Owed { get; set; }
        public decimal PaidOut { get; set; }
        public decimal Payment { get; set; }
        public decimal Pending { get; set; }
        public decimal Balance { get; set; }
        public string Delete { get; set; }
        public TblBranchOffice BranchOffice { get; set; }
        public TblEmployeeBusiness EmployeeCreate { get; set; }
        public TblEmployeeBusiness EmployeeModify { get; set; }
        public TblCustomerPayments  customerPayment { get; set; }
        public ICollection<TblCustomerPaymentsRow> TblCustomerPaymentsRow { get; set; }
        public IQueryable<TblCustomerPayments>  List_Customer { get; set; }
        public IQueryable<Invoice> List_Invoice { get; set; }
        public IQueryable<TblCustomerPaymentsRow> List_CustomerPaymentsRow { get; set; }

    }
}
