﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.CustomModels.Procedure.Customer;
using QuickSal.DataAcces.Procedure;
using QuickSal.DataAcces.Procedure.Customer;
using System.Linq;

namespace QuickSal.Web.ViewModels.Customer.CreditNote
{
    public class CreditNoteViewModel
    {
        /// <summary>
        /// Obtiene todas las notas de credito de una sucursal
        /// </summary>
        public IQueryable<GetAllCreditNote> CreditNotes { get; set; }

        /// <summary>
        /// Obtiene el listado de facturas sin notas de credito y aprobadas
        /// </summary>
        public IQueryable<prc_InvoiceWithOutCreditNote> Invoices { get; set; }

        /// <summary>
        /// Obtiene o establece la factura
        /// </summary>
        public TblInvoice InvoiceHeader { get; set; }

        /// <summary>
        /// Obtiene el detalle de una factura
        /// </summary>
        public IQueryable<PrcInvoceRowDetail> RowDetail { get; set; }

        /// <summary>
        /// Obtiene los articulos de una factura
        /// </summary>
        public IQueryable <GetInvoiceDetail> InvoiceRowItems { get; set; }

        /// <summary>
        /// Obtiene o establece la cabecera de la nota de credito
        /// </summary>
        public TblCreditNote CreditNote { get; set; }

        /// <summary>
        /// Obtiene o establece el detalle de la nota de credito
        /// </summary>
        public IQueryable<TblCreditNoteRow> CreditNoteRow { get; set; }
    }
}
