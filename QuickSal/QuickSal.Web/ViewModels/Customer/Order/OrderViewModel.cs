﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.CustomModels.Procedure.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.Web.ViewModels.Customer.Order
{
    public class OrderViewModel
    {
        public IEnumerable<GetOrders> Orders { get; set; }
    }
}
