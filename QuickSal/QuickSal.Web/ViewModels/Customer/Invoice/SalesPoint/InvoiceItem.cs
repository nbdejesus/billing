﻿namespace QuickSal.Web.ViewModels.Customer.Invoice.SalesPoint
{
    public class InvoiceItem
    {
        /// <summary>
        /// Obtiene el primary key del item
        /// </summary>
        public long InvoiceRowId { get; set; }

        /// <summary>
        /// Obtiene el primary key del producto
        /// </summary>
        public long? ProductId { get; set; }

        /// <summary>
        /// Obtiene el nombre del producto
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// Obtiene el monto del impuesto
        /// </summary>
        public decimal? Tax { get; set; }

        /// <summary>
        /// Obtiene la abreviatura de la medida
        /// </summary>
        public string MeaseureAbbreviation { get; set; }

        /// <summary>
        /// Obtiene el precio del articulo
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Obtiene la abreviatura del impuesto
        /// </summary>
        public string TaxAbbreviation { get; set; }

        /// <summary>
        /// Obtiene la cantidad del articulo
        /// </summary>
        public decimal ItemQuantity { get; set; }

        /// <summary>
        /// Obtiene el monto de descuento
        /// </summary>
        public decimal ItemDiscount { get; set; }

        /// <summary>
        /// Obtiene sub total del articulo
        /// </summary>
        public decimal Subtotal { get; set; }

        /// <summary>
        /// Obtiene el monto de descuento a un articulo
        /// </summary>
        public decimal AmountDiscount { get; set; }

        /// <summary>
        /// Obtiene el monto de impuesto a un articulo
        /// </summary>
        public decimal AmountTax { get; set; }

        /// <summary>
        /// Obtiene el almacen al que esá asociado un artículo
        /// </summary>
        public int WarehouseId { get; set; }
    }
}
