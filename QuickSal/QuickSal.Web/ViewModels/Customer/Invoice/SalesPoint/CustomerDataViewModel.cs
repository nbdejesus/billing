﻿namespace QuickSal.Web.ViewModels.Customer.Invoice.SalesPoint
{
    public class CustomerDataViewModel
    {
        /// <summary>
        /// Obtiene o establece el tipo de credito de un cliente
        /// </summary>
        public short? creditType { get; set; }

        /// <summary>
        /// Obtiene o establece el tipo de comprobante fiscal
        /// </summary>
        public short? NcfPrefixId { get; set; }
    }
}
