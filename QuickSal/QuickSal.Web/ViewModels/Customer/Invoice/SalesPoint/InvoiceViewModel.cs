﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.CustomModels.Procedure.Customer;
using QuickSal.DataAcces.Procedure;
using System.Collections.Generic;
using System.Linq;

namespace QuickSal.Web.ViewModels.Customer.Invoice.SalesPoint
{
    public class InvoiceViewModel
    {
        /// <summary>
        /// Obtiene todas las factuas de una sucursal
        /// </summary>
        public IQueryable<PrcGetAllInvoce> Invoices { get; set; }

        /// <summary>
        /// Obtiene el listado de clientes
        /// </summary>
        public IQueryable<TblCustomers> customers { get; set; }

        /// <summary>
        /// Obtiene y estable la informacion de un cliente
        /// </summary>
        public TblCustomers Customer { get; set; }

        /// <summary>
        /// Obtiene el primary key del tipo de empresa
        /// </summary>
        public short BusinesType { get; set; }

        /// <summary>
        /// Obtiene el listado de productos de una sucursal para el punto de ventas
        /// </summary>
        public IQueryable<GetProductsOnSale> GetProductsOnSale { get; set; }

        ///// <summary>
        ///// Obtiene el producto seleccionado para la factura
        ///// </summary>
        public InvoiceItem InvoiceItem { get; set; }

        /// <summary>
        /// Obtiene el listado de articulos almacenados en una factura
        /// </summary>
        public IEnumerable<InvoiceItem> invoiceItems { get; set; }

        /// <summary>
        /// Obtiene el listado de almacenes
        /// </summary>
        public IQueryable<TblWarehouse> Warehouses { get; set; }

        /// <summary>
        /// Obtiene o establece la información de la cabecera de la factura
        /// </summary>
        public TblInvoice InvoiceHeader { get; set; }

        /// <summary>
        /// Obtiene o establece un pago realizado a la factura
        /// </summary>
        public TblCustomerPayments Payment { get; set; }

        /// <summary>
        /// Obtiene o establece los tipos de comprobantes fiscales
        /// </summary>
        public IQueryable<TblNcfprefix> Ncfprefixes{ get; set; }

        /// <summary>
        /// Obtiene o establece un pendido
        /// </summary>
        public TblOrders Order { get; set; }
    }
}
