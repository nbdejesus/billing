﻿using QuickSal.DataAcces.Models.Customer;
using System.Collections.Generic;

namespace QuickSal.Web.ViewModels.Customer.Invoice
{
    public class GenericVM
    {
        public IEnumerable<CreditNoteVM> ListCreditNoteVM { get; set; }

        public Dictionary<long, decimal> PendingBalance { get; set; }
    }
}
