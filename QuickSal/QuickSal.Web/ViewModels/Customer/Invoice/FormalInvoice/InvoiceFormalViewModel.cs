﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.CustomModels.Procedure.Customer;
using QuickSal.DataAcces.Procedure;
using QuickSal.DataAcces.Procedure.Customer;
using System.Collections.Generic;
using System.Linq;

namespace QuickSal.Web.ViewModels.Customer.Invoice.FormalInvoice
{
    public class InvoiceFormalViewModel
    {
        /// <summary>
        /// Obtiene todas las factuas de una sucursal
        /// </summary>
        public IQueryable<PrcGetAllInvoce> Invoices { get; set; }

        /// <summary>
        /// Obtiene el listado de clientes
        /// </summary>
        public IQueryable<TblCustomers> customers { get; set; }

        /// <summary>
        /// Obtiene y estable la informacion de un cliente
        /// </summary>
        public TblCustomers Customer { get; set; }


        /// <summary>
        /// Obtiene el listado de productos de un almacen para su sucursal
        /// </summary>
        public IQueryable<GetProductsOnSale> ProductsWareHouse { get; set; }


        /// <summary>
        /// Obtiene el listado de almacenes
        /// </summary>
        public IQueryable<TblWarehouse> Warehouses { get; set; }

        /// <summary>
        /// Obtiene o establece la información de la cabecera de la factura
        /// </summary>
        public TblInvoice InvoiceHeader { get; set; }


        /// <summary>
        /// Obtiene los productos de una factura
        /// </summary>
        public IQueryable<GetInvoiceDetail> InvoiceRows { get; set; }

        /// <summary>
        /// Modelo con las clases genericas
        /// </summary>
        public GenericVM GenericVm { get; set; }
    }
}
