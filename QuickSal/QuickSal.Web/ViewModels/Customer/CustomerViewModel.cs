﻿using QuickSal.DataAcces.Contexts;
using System.Collections.Generic;
using System.Linq;

namespace QuickSal.Web.ViewModels.Customer
{
    public class CustomerViewModel
    {
        public TblCustomers Customer { get; set; }
        public IQueryable<TblCustomers> Customers { get; set; }

        public IEnumerable<TblProvince> Provinces { get; set; }
        public IEnumerable<TblCountry> Countries { get; set; }
        public IEnumerable<TblDocumentIdentification> DocumentIdentifications { get; set; }
        public IEnumerable<TblNcfprefix> Ncfprefixes { get; set; }

        /// <summary>
        /// Obtiene foto del cliente
        /// </summary>
        public string UrlPhoto { get; set; }
    }
}
