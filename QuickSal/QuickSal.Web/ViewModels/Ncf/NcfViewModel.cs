﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System.Collections.Generic;
using System.Linq;

namespace QuickSal.Web.ViewModels.Nfc
{
    public class NcfViewModel
    {
        /// <summary>
        /// Obtiene el listado de ncf
        /// </summary>
       
        public IQueryable<TblNcf>  ncfs { get; set; }

        /// <summary>
        /// Obtiene o estable los datos de un ncf
        /// </summary>
        ///  /// 
        //tu ves el nombre de ese objeto que se llama ncfs, si
        //si yo le doy ncfs.     el me llevara a su relacion
        //Este si por que el otro es un iquerable
        //este es un objeto
        public TblNcf  ncf { get; set; }

        public IQueryable<Ncfs> ListNcfs { get; set; }

        public IQueryable<TblNcfprefix> Prefix { get; set; }
        public ICollection<TblNcfrow> TblNcfrow { get; set; }
    }
}
