﻿using QuickSal.DataAcces.Contexts;
using System.Linq;

namespace QuickSal.Web.ViewModels.Inventory
{
    public class SubCategoryViewModel
    {
        /// <summary>
        /// Obtiene el listado de Subcatetorias
        /// </summary>
        public IQueryable<TblSubCategory> SubCategories { get; set; }

        /// <summary>
        /// Obtiene un listado de subcategorias
        /// </summary>
        public IQueryable<TblCategory> Categories { get; set; }

        /// <summary>
        /// Obtiene o estable los datos de una Subcategoria
        /// </summary>
        public TblSubCategory SubCategory { get; set; }
    }
}
