﻿using QuickSal.DataAcces.Contexts;
using System.Linq;

namespace QuickSal.Web.ViewModels.Inventory
{
    public class CategoryViewModel
    {
        /// <summary>
        /// Obtiene el listado de catetorias
        /// </summary>
        public IQueryable<TblCategory> Categories { get; set; }

        /// <summary>
        /// Obtiene o estable los datos de una categoria
        /// </summary>
        public TblCategory Category { get; set; }
    }
}
