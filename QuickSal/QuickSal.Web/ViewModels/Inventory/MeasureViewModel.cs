﻿using QuickSal.DataAcces.Contexts;
using System.Linq;

namespace QuickSal.Web.ViewModels.Inventory
{
    public class MeasureViewModel
    {
        /// <summary>
        /// Obtiene el listado de medidas
        /// </summary>
        public IQueryable<TblMeasure> Measurements { get; set; }

        /// <summary>
        /// Obtiene o estable los datos de una medida
        /// </summary>
        public TblMeasure Measure { get; set; }
    }
}
