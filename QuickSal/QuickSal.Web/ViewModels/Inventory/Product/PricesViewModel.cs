﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.Web.ViewModels.Inventory.Product
{
    public class PricesViewModel
    {
        public long Id { get; set; }
        public string TaxName { get; set; }
        public string PriceName { get; set; }
        public decimal Utility { get; set; }
        public string Price { get; set; }
        public string TotalPrice { get; set; }
        public string Principal { get; set; }
        public string Delete { get; set; }
        public int TaxId { get; set; }
        public bool IsPrincipal { get; set; }
    }
}
