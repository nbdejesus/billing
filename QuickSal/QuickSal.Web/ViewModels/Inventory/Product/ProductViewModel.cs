﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System.Collections.Generic;
using System.Linq;

namespace QuickSal.Web.ViewModels.Inventory.Product
{
    public class ProductViewModel
    {
        /// <summary>
        /// Obtiene o establece un listado de productos
        /// </summary>
        public IQueryable<PrcGetAllProducts> Products { get; set; }

        /// <summary>
        /// Obtiene
        /// </summary>
        public string UrlProduct { get; set; }


        /// <summary>
        /// Obtiene o establece un producto
        /// </summary>
        public TblProducts Product { get; set; }

        /// <summary>
        /// Obtiene un listado de precios para un producto
        /// </summary>
        public List<TblProductsPrices> Prices { get; set; }

        /// <summary>
        /// Obtiene un listado de proveedores
        /// </summary>
        public IQueryable<TblSuppliers> Suppliers { get; set; }

        /// <summary>
        /// Obtiene un listado de categorias
        /// </summary>
        public IQueryable<TblCategory> Categories { get; set; }

        /// <summary>
        /// Obtiene un listado de subcategorias
        /// </summary>
        public IQueryable<TblSubCategory> SubCategories { get; set; }

        /// <summary>
        /// Obtiene un listado de medidas
        /// </summary>
        public IQueryable<TblMeasure> Measures { get; set; }

        /// <summary>
        /// Obtiene un listado de impuestos
        /// </summary>
        public IQueryable<TblTax> Taxes { get; set; }

        /// <summary>
        /// Obtiene un listado de almacenes
        /// </summary>
        public IQueryable<TblWarehouse> Warehouses { get; set; }
    }
}
