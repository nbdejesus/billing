﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.CustomModels.Models.Plan;
using System.Collections.Generic;
using System.Linq;

namespace QuickSal.Web.ViewModels.Plan
{
    public class PlanModule
    {
        public IQueryable<TblObject> ObjectList { get; set; }

        public IEnumerable<TemplateStatus> ModuleList { get; set; }

        public IEnumerable<TblPlans> plans { get; set; }
    }
}
