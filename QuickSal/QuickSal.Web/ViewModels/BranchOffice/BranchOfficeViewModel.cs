﻿using QuickSal.DataAcces.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.Web.ViewModels.BranchOffice
{
    public class BranchOfficeViewModel
    {
        public IQueryable<TblBranchOffice> BranchOffices { get; set; }
        public TblBranchOffice BranchOffice { get; set; }
        public IQueryable<TblProvince> Provinces { get; set; }

        public int cant { get; set; }
    }
}
