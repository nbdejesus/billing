﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.Web.ViewModels.Employee
{
    public class SegurityAccess
    {
        public long Id { get; set; }
        public int PrivilegesId { get; set; }
        public int SubObjectId { get; set; }
        public bool CanRead { get; set; }
        public bool CanWrite { get; set; }
        public bool CanEdit { get; set; }
        public bool CanDelete { get; set; }
    }
}
