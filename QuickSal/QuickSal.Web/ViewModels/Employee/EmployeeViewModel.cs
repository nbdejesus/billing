﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Views;
using System.Collections.Generic;
using System.Linq;

namespace QuickSal.Web.ViewModels.Employee
{
    public class EmployeeViewModel
    {
        public IQueryable<TblPrivileges> Privileges { get; set; }
        public IQueryable<TblEmployeeBusiness> EmployeeBusiness { get; set; }
        public IQueryable<vwObjectsBusiness> Objects { get; set; }
        public IQueryable<vwSegurityAccess> SegurityAccess { get; set; }
        public IQueryable<TblBranchOffice> BranchOffices { get; set; }

        public List<SegurityAccess> security { get; set; }

        public IEnumerable<TblProvince> Provinces { get; set; }
        public IEnumerable<TblCountry> Countries { get; set; }
        public IEnumerable<TblDocumentIdentification> DocumentIdentifications { get; set; }
   

        public TblEmployeeBusiness Employee { get; set; }
        public TblPrivileges Privilege { get; set; }
        public TblUsers User { get; set; }
        public string PrivilegeName { get; set; }
        public string UserEmail { get; set; }

        /// <summary>
        /// Obtiene la url de la foto
        /// </summary>
        public string UrlPhoto { get; set; }
    }
}
