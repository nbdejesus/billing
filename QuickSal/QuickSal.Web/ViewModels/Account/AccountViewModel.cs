﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuickSal.DataAcces.Contexts;

namespace QuickSal.Web.ViewModels.Account
{
    public class AccountViewModel
    {
        public TblUsers User { get; set; }
        public TblUserConfirmation Confirmation { get; set; }
        public bool RecoveryPassword { get; set; }
        public string KeyConfirmation { get; set; }
    }
}
