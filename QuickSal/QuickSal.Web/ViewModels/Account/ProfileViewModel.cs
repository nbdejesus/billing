﻿using QuickSal.DataAcces.Contexts;
using System.Collections.Generic;
using static QuickSal.Web.Api.GenericController;

namespace QuickSal.Web.ViewModels.Account
{
    public class ProfileViewModel
    {
        public TblUsers User { get; set; }
        public List<SelectedGenders> ListGenderSelected { get; set; }
        public IEnumerable<TblCountry> Countries { get; set; }
        public IEnumerable<TblDocumentIdentification> DocumentIdentifications { get; set; }
        public IEnumerable<TblProvince> Provinces { get; set; }

        public string UrlPhotoProfile { get; set; }
    }
}
