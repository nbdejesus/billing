﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.Web.ViewModels.Account
{
    public class ReCaptchaSettings
    {
        public string site_Key { get; set; }
        public string secret_Key { get; set; }
    }
}
