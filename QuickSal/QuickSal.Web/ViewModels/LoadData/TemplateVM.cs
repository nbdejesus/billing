﻿
using Microsoft.AspNetCore.Http;

namespace QuickSal.Web.ViewModels.LoadData
{
    public class TemplateVM
    {
        public int DocumentTemplateId { get; set; }
        public IFormFile Template { get; set; }
    }
}
