﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Views;
using System.Collections.Generic;
using System.Linq;

namespace QuickSal.Web.ViewModels.Business
{
    public class BusinessViewModel
    {
        public IQueryable<TblBusinessType> BusinessTypeList { get; set; }
        public IEnumerable<TblDocumentIdentification> DocumentIdentificationList { get; set; }
        public IEnumerable<TblCountry> Countries { get; set; }
       
        public IEnumerable<vwPlansDetail> Objects { get; set; }
        public IEnumerable<TblProvince> Provinces { get; set; }

        public TblBusiness Business { get; set; }
        public TblBranchOffice BranchOffice { get; set; }
        public short PlanId { get; set; }

        /// <summary>
        /// Obtiene la url del logo
        /// </summary>
        public string UrlLogo { get; set; }
    }
}
