﻿using QuickSal.DataAcces.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.Web.ViewModels.DebitNote
{
    public class DebitNoteVM : TblDebitNote
    {
        public IEnumerable<TblDebitNote> tblDebitNotes { get; set; }
        public IEnumerable<TblDebitNoteRow> tblDebitNoteRows { get; set; }
        public IQueryable<TblSuppliers> Supplier { get; set; }
        public TblPurchaseOrder PurchaseOrders { get; set; }
        public TblDebitNote DebitNotes { get; set; }
        public TblSuppliers Supplierer { get; set; }

        public TblBusiness TblBusiness { get; set; }
    }
}
