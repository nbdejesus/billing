﻿
using QuickSal.DataAcces.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QuickSal.Web.ViewModels.Reportes
{
    public class ReportModel
    {
        public long? nameId { get; set; }
        public string fechaIni{ get; set; }
        public string fechaFin { get; set; }
        public long businessId { get; set; }
        public long? brachOffice { get; set; }
        public int? privileges { get; set; }
        public string documentNumber { get; set; }
        public string codigo { get; set; }
        public bool? disponible { get; set; }
        public bool? servicio { get; set; }
        public long? InvoiceId { get; set; }

        public Guid guid { get; set; }  

      
    }
        
}
