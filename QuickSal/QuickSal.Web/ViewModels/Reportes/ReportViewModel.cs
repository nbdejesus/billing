﻿
using QuickSal.DataAcces.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QuickSal.Web.ViewModels.Reportes
{
    public class ReportViewModel
    {
        public long? nameId { get; set; }
        public DateTime? fechaIni{ get; set; }
        public DateTime? fechaFin { get; set; }
        public long businessId { get; set; }
        public long? brachOffice { get; set; }
        public int? privileges { get; set; }
        public string document { get; set; }
        public string codigo { get; set; }
        public bool? disponible { get; set; }
        public bool? servicio { get; set; }
      
        public Guid guid { get; set; }
        public IQueryable<TblCustomers> customerList { get; set; }
        public IQueryable<TblEmployeeBusiness> employeeBusinessesList { get; set; }
        public IQueryable<TblSuppliers> suppliersList { get; set; }
        public IQueryable<TblBranchOffice> branchOfficesList { get; set; }

        public IQueryable<TblPrivileges> privilegesList { get; set; }
        public IQueryable<TblNcfprefix> ncfPrefixesList { get; set; }
        public IQueryable<TblProducts> productsList { get; set; }

    }    
    
}
