﻿using QuickSal.DataAcces.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.Web.ViewModels.Warehouse
{
    public class WarehouseViewModel
    {
        /// <summary>
        /// Litado de almacenes de la sucursal
        /// </summary>
        public IQueryable<TblWarehouse> Warehouses { get; set; }

        /// <summary>
        /// Obtiene o establece los datos de un almacen
        /// </summary>
        public TblWarehouse Warehouse { get; set; }
    }
}
