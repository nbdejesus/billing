﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Procedure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.Web.ViewModels.Warehouse
{
    public class MovingWareVM
    {
        public IQueryable<PrcGetAllMovingWare> tblMovingWarehouses { get; set; }
        public string tipoProceso { get; set; }
        public IQueryable<PrcGetAllMovingWare> MovingWareSender { get; set; }
        public IQueryable<PrcGetAllMovingWare> MovingWareReciver { get; set; }
        public IQueryable<TblWarehouse> Warehouses { get; set; }
        public IQueryable<TblMovingWarehouseRow> tblMovingWarehouseRows { get; set; }
        public TblMovingWarehouse MovingWarehouse { get; set; }
        public IQueryable<ProductInventory> TblProducts { get; set; }

    }
}
