﻿using QuickSal.DataAcces.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.Web.ViewModels.VendorPayment
{
    public class VendorPaymentViewModel
    {
        public long Id { get; set; }
        public long BranchOfficeId { get; set; }
        public long EmployeeBusinessId { get; set; }
        public byte DocumentEstatusId { get; set; }
        public string DocumentNumber { get; set; }
        public string fecha { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Observation { get; set; }
        public decimal Total { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }
        public long VendorPaymentId { get; set; }
        public long SuppliersId { get; set; }
        public long PurchaseOrderId { get; set; }
        public string PurchaseOrderDocument { get; set; }
        public string SupplierName { get; set; }
        public string Concept { get; set; }
        public string Owed { get; set; }
        public string PaidOut { get; set; }
        public string Payment { get; set; }
        public string Pending { get; set; }
        public string Delete { get; set; }
        public TblBranchOffice BranchOffice { get; set; }
        public TblEmployeeBusiness EmployeeBusiness { get; set; }
        public TblVendorPayment  VendorPayment { get; set; }
        public IQueryable<TblVendorPayment> ListVendorPayment { get; set; }
        public ICollection<TblVendorPaymentRow> TblVendorPaymentRow { get; set; }
    }
}
