﻿using QuickSal.DataAcces.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.Web.ViewModels.VendorPayment
{
    public class VendorPaymentRowVM : TblVendorPaymentRow
    {
        public IQueryable<TblVendorPaymentRow> TblVendorPaymentRow { get; set; }
    }
}
