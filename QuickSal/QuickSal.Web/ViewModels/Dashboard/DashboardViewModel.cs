﻿using QuickSal.DataAcces.Procedure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.Web.ViewModels.Dashboard
{
    public class DashboardViewModel
    {
        public long Id { get; set; }
        public string ventas { get; set; }

        public List<prc_Sales> sales { get; set; }
        public List<prc_Shopping> Shopping { get; set; }
        public List<prc_Profits> Profits { get; set; }
        public List<prc_expenses>  expenses { get; set; }
        public List<sp_Sales_for_Month>   sp_Sales_For_Months { get; set; }
        public List<sp_Sales_for_Month_products> Sales_For_Month_Products { get; set; }
        public List<prc_sp_Sales_for_Month_seller> prc_Sp_Sales_For_Month_Sellers { get; set; }
        public List<prc_Sales_for_Month_branchOffice>  Sales_For_Month_BranchOffices { get; set; }
        public List<pr_sp_Sales_for_Month_Customer> Sales_for_Month_Customer { get; set; }
    }
}
