﻿namespace QuickSal.Web.ViewModels.Dashboard
{
    public class SessionViewModel
    {
        public long UserId { get; set; }
        public long EmployeeId { get; set; }
        public long BusinessId { get; set; }
        public long BranchOfficeId { get; set; }
        public string UserName { get; set; }
        public string UserPicture { get; set; }
        public string UserEmail { get; set; }
    }
}
