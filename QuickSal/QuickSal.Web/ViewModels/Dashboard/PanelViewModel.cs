﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Views;
using System.Collections.Generic;
using System.Linq;

namespace QuickSal.Web.ViewModels.Dashboard
{
    public class PanelViewModel
    {
        public IEnumerable<TblBusiness> BusinessList { get; set; }
        public IEnumerable<vwObjectsBusiness> Objects { get; set; }
        public IQueryable<TblBranchOffice> branchOffices { get; set; }
        public TblBranchOffice branchOffice { get; set; }
        /// <summary>
        /// Almacena la informacion de la empresa
        /// </summary>
        public TblBusiness Business { get; set; }
        /// <summary>
        /// Almacena la informacion del plan
        /// </summary>
        public string PlanName { get; set; }
        public TblUsers User { get; set; }

        /// <summary>
        /// Obtiene la url con el logo de la empresa
        /// </summary>
        public string UrlBusinessLogo { get; set; }

        /// <summary>
        /// Obtiene la url con el logo del usuario
        /// </summary>
        public string UrlPhotoProfile { get; set; }        
    }
}
