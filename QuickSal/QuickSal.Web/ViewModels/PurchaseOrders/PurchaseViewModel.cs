﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System.Collections.Generic;
using System.Linq;

namespace QuickSal.Web.ViewModels.Purchase
{
    public class PurchaseViewModel
    {

        /// <summary>
        /// Obtiene el listado de ncf
        /// </summary>
        public IQueryable<TblSuppliers> ncfs { get; set; }

        /// <summary>
        /// Obtiene o estable los datos de un ncf
        /// </summary>
        public TblNcf ncf { get; set; }
        public TblPurchaseOrder PurchaseOrder { get; set; }
        public IQueryable<TblWarehouse> Warehouse { get; set; }
        
        public IQueryable<TblSuppliers> Suppliers { get; set; }
        public TblSuppliers Supplier { get; set; }
        public IQueryable<TblPurchaseOrderRow> PurchaseRow { get; set; }
        public ICollection<TblNcfrow> TblNcfrow { get; set; }
        public object Warehouses { get; internal set; }
        public IQueryable<TblProducts> products { get; set; }
        public IQueryable<TblTax> Taxes { get; set; }
        public IQueryable<PurchaseOrders> PurchaseOrders { get; internal set; }

        public ICollection<TblDebitNote> debitNote { get; set; }
        public long Id { get; set; }
        public long PurchaseOrderId { get; set; }
        public long ProductId { get; set; }
        public long MeasureId { get; set; }
        public string Measure { get; set; }
        public long ArticulosId { get; set; }
        public long AlmacenId { get; set; }
        public int RowNumber { get; set; }
        public decimal Quantity { get; set; }
        public decimal cantidad { get; set; }
        public string Articulos { get; set; }
        public string Almacen { get; set; }
        public decimal Cost { get; set; }
        public decimal compra { get; set; }
        public decimal Tax { get; set; }
        public string Impuesto { get; set; }
        public decimal AmountTax { get; set; }
        public decimal Discount { get; set; }
        public decimal descuento { get; set; }
        public decimal subTotal { get; set; }
        public decimal AmountDiscount { get; set; }
        public decimal Total { get; set; }
        public string Principal { get; set; }
        public string Accion { get; set; }
        public string Delete { get; set; }
        public string Edit { get; set; }
        public string Produc_Description { get; set; }
        public string Almacen_Description { get; set; }
        public decimal ITBIS { get; set; }
        public string tipoProceso { get; set; }
        public int estado { get; set; }

        public Dictionary<long,decimal> PendingBalance { get; set; }
    }
}
