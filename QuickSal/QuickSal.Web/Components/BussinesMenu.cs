﻿using Microsoft.AspNetCore.Mvc;
using QuickSal.Services;
using QuickSal.Web.Api;
using QuickSal.Web.ViewModels.Dashboard;

namespace QuickSal.Web.Components
{
    public class BussinesMenu : ViewComponent
    {
        private IGenericMethod _genericMethod;
        private IBusinessService businessService;
        private IEmployeeService employeeService;
        private ISecurityService securityService;

        public BussinesMenu(IGenericMethod GenericMethod, IBusinessService businessService,
                            IEmployeeService employeeService, ISecurityService securityService)
        {
            this._genericMethod = GenericMethod;
            this.businessService = businessService;
            this.employeeService = employeeService;
            this.securityService = securityService;
        }

        public IViewComponentResult Invoke()
        {
            PanelViewModel model = new PanelViewModel();
            var session = securityService.GetSession();

            //Obenemos los datos del empleado
            var employee = employeeService.GetEmployeeInBusiness(session.UserId, session.BusinessId);

            //Obtenemos los objetos que tiene permitido el empleado
            if (employee != null)
                model.Objects = employeeService.GetObjects(session.BusinessId, employee.PrivilegesId);

            //Obtenemos la sucursal o las sucursales que puede acceder el empleado
            if (employee != null && !employee.BranchOfficeId.HasValue)
                model.branchOffices = businessService.getBranchOffices(session.BusinessId);
            else if (employee != null && employee.BranchOfficeId.HasValue)
                model.branchOffice = businessService.GetBranchOffice(employee.BranchOfficeId.Value);


            return View(model);
        }
    }
}
