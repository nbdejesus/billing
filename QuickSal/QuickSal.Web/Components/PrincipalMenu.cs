﻿using Microsoft.AspNetCore.Mvc;
using QuickSal.DataAcces;
using QuickSal.DataAcces.CustomModels.Models;
using QuickSal.Services;
using QuickSal.Web.Api;
using QuickSal.Web.ViewModels.Dashboard;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.Web.Components
{
    public class PrincipalMenu : ViewComponent
    {
        private IBusinessService businessService;
        private IAccountService accountingService;
        private ISecurityService securityService;
        private IAmazonService amazonService;
        private IGlobalService globalService;

        public PrincipalMenu(IBusinessService businessService, IAccountService accountingService,
                             ISecurityService securityService, IAmazonService amazonService, IGlobalService globalService)
        {
            this.businessService = businessService;
            this.accountingService = accountingService;
            this.securityService = securityService;
            this.amazonService = amazonService;
            this.globalService = globalService;
        }


        public IViewComponentResult Invoke()
        {
            PanelViewModel model = new PanelViewModel();
            var sesion = securityService.GetSession();

            if (sesion != null && sesion.BusinessId != 0)
            {
                model.Business = businessService.GetBusinessById(sesion.BusinessId);
                model.PlanName = businessService.GetPlanName(sesion.BusinessId);
            }
            model.User = accountingService.GetUser(sesion.UserId);

            LoadPictures(model);

            return View(model);
        }

        private void LoadPictures(PanelViewModel model)
        {
            if (model.Business != null && !string.IsNullOrEmpty(model.Business.Logo))
            {
                Task<string> sCode = Task.Run(async () =>
                {
                    return await amazonService.GetUrlObjet(model.Business.Logo, QuickSalEnum.TypeFiles.BusinessLogo);
                });
                model.UrlBusinessLogo = sCode.Result;
            }
            else
            {
                Task<IQueryable<Setting>> sCode = Task.Run(async () =>
                {
                    return await globalService.GetSettingValue("amazon", "BusinessLogoDefault");
                });

                var productPicture = sCode.Result;
                model.UrlBusinessLogo = productPicture.FirstOrDefault().SettingValue;
            }



            if (model.User != null && !string.IsNullOrEmpty(model.User.Picture))
            {
                Task<string> sCode = Task.Run(async () =>
                {
                    return await amazonService.GetUrlObjet(model.User.Picture, QuickSalEnum.TypeFiles.ProfilePhoto);
                });
                model.UrlPhotoProfile = sCode.Result;
            }
            else
            {
                Task<IQueryable<Setting>> sCode = Task.Run(async () =>
                {
                    return await globalService.GetSettingValue("amazon", "ProfileDefault");
                });

                var productPicture = sCode.Result;
                model.UrlPhotoProfile = productPicture.FirstOrDefault().SettingValue;
            }
        }
    }
}