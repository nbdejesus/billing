﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using QuickSal.DataAcces;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using QuickSal.Services;
using QuickSal.Web.ViewModels.Dashboard;
using System;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static QuickSal.Web.Api.GenericController;

namespace QuickSal.Web.Api
{
    public class GenericMethod : Controller, IGenericMethod
    {
        private IAccountService accountingService;
        private IBusinessService businessService;
        private IConfiguration _configuration;
        private IHostingEnvironment _env;
        private IGlobalService globalService;
        private IServiceError serviceError;
        private ISecurityService securityService;
        public SessionViewModel SessionDashboard { get; set; }

        public GenericMethod(IAccountService accountingService, IConfiguration configuration,
                             IHostingEnvironment env, IGlobalService globalService, IServiceError serviceError,
                             IBusinessService businessService, ISecurityService securityService)
        {
            this.businessService = businessService;
            this.accountingService = accountingService;
            this._configuration = configuration;
            this._env = env;
            this.globalService = globalService;
            this.serviceError = serviceError;
            this.securityService = securityService;
        }

        public bool IsSessionActive(HttpRequest CurrentRequest)
        {
            string sessionData = CurrentRequest.Cookies["Quicksal"];

            if (string.IsNullOrEmpty(sessionData))
                return false;
            else
            {
                string ressultado = globalService.TripleDesDescrypt(sessionData);
                SessionDashboard = JsonConvert.DeserializeObject<SessionViewModel>(globalService.TripleDesDescrypt(sessionData));
                return true;
            }
        }

        /// <summary>
        /// Proceso para crear una session de usuario en el sistema
        /// </summary>
        /// <param name="CurrentRequest"></param>
        /// <param name="UserModel"></param>
        /// <returns></returns>
        public async Task<TaskResult> CreateSession(Controller CurrentRequest, long userID, long businessId = 0, long branchOffieId = 0, long EmployeeId = 0)
        {
            try
            {
                Services.Support.SessionViewModel dashboard = new Services.Support.SessionViewModel();
                var user = accountingService.GetUser(userID);

                //Variables que se utilizarán en la session
                dashboard.UserId = user.Id;
                dashboard.UserName = user.Name;
                dashboard.UserPicture = user.Picture;
                dashboard.UserEmail = user.Email;
                dashboard.EmployeeId = EmployeeId;
                dashboard.BusinessId = businessId;
                dashboard.BranchOfficeId = branchOffieId;


                securityService.LoadSesion(dashboard);

                var site_Key = await globalService.GetSettingValue("GeneralSetting", "CookieSessionDays");

                CookieOptions option = new CookieOptions();
                option.Expires = DateTime.Now.AddHours(Convert.ToInt16(site_Key.FirstOrDefault().SettingValue));
                CurrentRequest.Response.Cookies.Delete("Quicksal");
                CurrentRequest.Response.Cookies.Append("Quicksal", globalService.TripleDesEncrypt(JsonConvert.SerializeObject(dashboard, Formatting.None,
                                                                                                       new JsonSerializerSettings()
                                                                                                       {
                                                                                                           ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                                                                                                       })), option);
                return serviceError.TaskStatus();
            }
            catch (Exception ex)
            {
                serviceError.LogError(ex.Message, "CreateSession", "GenericMethod", "Error intentando levantar sesion del usuario");
                return serviceError.TaskStatus();
            }

        }

        public void RemoveSession(Controller CurrentRequest)
        {
            CurrentRequest.Response.Cookies.Delete("Quicksal");
        }

        public SessionViewModel GetActive(Controller CurrentRequest)
        {
            var sessionData = CurrentRequest.HttpContext.Session.GetString("Quicksal");

            if (string.IsNullOrEmpty(sessionData))
                return null;
            else
            {

                return JsonConvert.DeserializeObject<SessionViewModel>(sessionData);
            }
        }


        /// <summary>
        /// Elimina un archivo desde su carpeta principal
        /// </summary>
        /// <param name="name"></param>
        /// <param name="typeFiles"></param>
        /// <returns></returns>
        public bool RemoveFile(string name, TypeFiles typeFiles)
        {
            try
            {
                if (typeFiles == TypeFiles.ProfilePhoto)
                {
                    string PictureProfile = _env.WebRootPath + _configuration.GetSection("Recursos")["PictureProfile"] + name;

                    // Ensure that the target does not exist.
                    if (System.IO.File.Exists(PictureProfile))
                        System.IO.File.Delete(PictureProfile);

                }
                else if (typeFiles == TypeFiles.BusinessLogo)
                {
                    string CoverBook = _env.WebRootPath + _configuration.GetSection("Recursos")["BusinessLogo"] + name;

                    // Ensure that the target does not exist.
                    if (System.IO.File.Exists(CoverBook))
                        System.IO.File.Delete(CoverBook);

                }
                //else if (typeFiles == TypeFiles.Book)
                //{
                //    string Book = _env.WebRootPath + _configuration.GetSection("Recursos")["Books"] + name;

                //    // Ensure that the target does not exist.
                //    if (System.IO.File.Exists(Book))
                //        System.IO.File.Delete(Book);
                //}

                //else if (typeFiles == TypeFiles.CoverReview)
                //{
                //    string Review = _env.WebRootPath + _configuration.GetSection("Recursos")["ReviewCovers"] + name;

                //    // Ensure that the target does not exist.
                //    if (System.IO.File.Exists(Review))
                //        System.IO.File.Delete(Review);
                //}

                return true;

            }
            catch (Exception ex)
            {
                //_ApiService.logError(ex.Message, "MoveFile", "ApiController", "Uno de los archivos cargados no se ha encontrado intenta cargarlo nuevamente.");
                return false;
            }
        }

        /// <summary>
        /// Proceso para obtener la url del archivo del servidor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="typeFiles"></param>
        /// <returns></returns>
        public string GetUrlImage(string name, TypeFiles typeFiles)
        {
            try
            {
                string pattern = "[\\]";
                Regex rgx = new Regex(pattern);
                if (string.IsNullOrEmpty(name))
                    return null;

                if (typeFiles == TypeFiles.ProfilePhoto)
                {
                    string Url = rgx.Replace(_configuration.GetSection("Recursos")["PictureProfile"], "/") + name;
                    return Url;
                }
                else if (typeFiles == TypeFiles.BusinessLogo)
                {
                    string Url = rgx.Replace(_configuration.GetSection("Recursos")["BusinessLogo"], "/") + name;
                    return Url;
                }
                //else if (typeFiles == TypeFiles.Book)
                //{
                //    string Url = rgx.Replace(_configuration.GetSection("Recursos")["Books"], "/") + name;
                //    return Url;
                //}
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                //_ApiService.logError(ex.Message, "GetUrlImage", "GenericMethod", "Problema intentando obtener archivo.");
                return "";
            }
        }

        public string GetListDesc(Type tipoEnum)
        {
            string key = "";
            var query = tipoEnum.GetType().
                    GetField(tipoEnum.ToString()).GetCustomAttributes(true).
                    Where(a => a.GetType().Equals(typeof(System.ComponentModel.DescriptionAttribute)));

            key = (tipoEnum.GetType().
                GetField(tipoEnum.ToString()).GetCustomAttributes(true).
                Where(a => a.GetType().Equals(typeof(System.ComponentModel.DescriptionAttribute))).
                FirstOrDefault() as System.ComponentModel.DescriptionAttribute).Description;

            return key;
        }

        public RedirectToActionResult RedirecNotSession()
        {
            return RedirectToAction("UserAccount", "Account");
        }

        /// <summary>
        /// Metodo para obtener el tipo de identificación
        /// </summary>
        /// <param name="Id">Primary key del tipo</param>
        /// <returns></returns>
        public async Task<TblDocumentIdentification> GetDocumentIdentification(int Id)
        {
            return await globalService.GetDocumentIdentification(Id);
        }
    }

    public interface IGenericMethod

    {
        string GetListDesc(Type tipoEnum);

        bool IsSessionActive(HttpRequest CurrentRequest);

        /// <summary>
        /// Proceso para crear una session de usuario en el sistema
        /// </summary>
        /// <param name="CurrentRequest"></param>
        /// <param name="UserModel"></param>
        /// <returns></returns> 
        Task<TaskResult> CreateSession(Controller CurrentRequest, long userID, long businessId = 0, long branchOffie = 0, long EmployeeId = 0);

        void RemoveSession(Controller CurrentRequest);
        SessionViewModel SessionDashboard { get; set; }
        SessionViewModel GetActive(Controller CurrentRequest);
        string GetUrlImage(string name, TypeFiles typeFiles);
        bool RemoveFile(string name, TypeFiles typeFiles);
        RedirectToActionResult RedirecNotSession();
    }
}
