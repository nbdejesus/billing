﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel;
using System.Linq;

namespace QuickSal.Web.Api
{
    public class GenericController : Controller
    {
        public enum TypeFiles
        {
            ProfilePhoto = 1,
            BusinessLogo = 2
        }     


        public string GetListDesc(Enum tipoEnum)
        {
            var query = tipoEnum.GetType().
                    GetField(tipoEnum.ToString()).GetCustomAttributes(true).
                    Where(a => a.GetType().Equals(typeof(System.ComponentModel.DescriptionAttribute)));

            return (tipoEnum.GetType().
                    GetField(tipoEnum.ToString()).GetCustomAttributes(true).
                    Where(a => a.GetType().Equals(typeof(System.ComponentModel.DescriptionAttribute))).
                    FirstOrDefault() as System.ComponentModel.DescriptionAttribute).Description;
        }
    }
}