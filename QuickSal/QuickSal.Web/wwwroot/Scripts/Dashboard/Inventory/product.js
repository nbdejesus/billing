﻿var $priceTable = undefined;
$(document).ready(function myfunction() {

    CallPriceTable();
    var $warehouseTable = CallDinamicTable('#WareHouseTable', 10, false);

    $('form').validate({
        rules: {
            'Product.Code': { required: true },
            'Product.Name': { required: true }
        },
        messages: {
            'Product.Code': { required: 'Este campo es obligatorio' },
            'Product.Name': { required: 'Este campo es obligatorio' }
        },
        debug: true
    });

    //Calculamos el precio desde el campo costo
    $(document).on('keyup', '[name="Product.Cost"]', function (e) {
        $('#CostPreview').val($(this).val());

        CalculatePrice();
    });

    //Calculamos el precios desde el costo view
    $(document).on('keyup', '#CostPreview', function myfunction() {
        $('[name="Product.Cost"]').val($(this).val());
        CalculatePrice();
    });

    //calculo del precio
    $(document).on('keyup', '#gain', function myfunction() {
        CalculatePrice();
    });

    //Calculando la utilidad con el precio y el costo
    $(document).on('keyup', '#price', function myfunction() {
        var Cost = parseFloat($('[name="Product.Cost"]').val());
        var price = parseFloat($(this).val());

        if (price !== "" && price !== 0) {
            if (Cost !== "" && Cost !== 0) {
                var Utility = ((price - Cost) / Cost) * 100;
                $('#gain').val(Utility.toFixed(2));
            }
            else {
                $('#gain').val(0.00);
            }
        }
    });


    $('#BtnAceptar').click(function myfunction() {
        var $form = $('form');

        if ($form.valid()) {

            var model = $form.serialize() + '&' + $.param({ "Product.TblProductsPrices": ConvertArraryToObjectPrice($priceTable.data().toArray()) })
                + '&' + $.param({ "Product.TblInventoryWarehouse": ConvertArraryToObjectWareHouse($warehouseTable.data().toArray()) });

            $.ajax({
                url: (($('#edit-product').length > 0) ? ProcessManager.getUrl('product/UpdateProduct') : ProcessManager.getUrl('product/CreateProduct')),
                type: 'POST',
                data: model,
                beforeSend: function myfunction() {
                    ProcessManager.Loading(true, 'body');
                },
                success: function myfunction(data) {
                    if (data.status) {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.Success('Notificación', data.message);
                        ProcessManager.Redirect(data.redirect);
                    }
                    else {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.Warning('Alerta', data.message);
                    }
                },
                error: function myfunction(xhr, status) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.validateStatusCode(xhr.status);
                }
            });
        }
        else {
            ProcessManager.Warning('Alerta', 'Faltan campos por llenar.');
        }
    });


    //Metodo para agregar precios a la tabla
    $('#BtnAddPrice').click(function myfunction() {
        if (PriceValidations()) {
            var $gain = $('#gain');
            var price = parseFloat($('#price').val());
            var $name = $('#priceName');
            var tax = $('#priceTax :selected').val();       
            var taxPercent = parseFloat($('#priceTax :selected').attr('data-percent'));
            var taxName = $('#priceTax :selected').text();
            var checked = $('#PrincipalPrice').find('input').val();

            //validamos si ya existe un precio principal en la tabla
            if (!ProcessManager.ValidateTableColum($priceTable, 8, 'true') || checked != 'true') {
                $priceTable.row.add([
                    taxName,
                    $name.val(),
                    $gain.val(),
                    price,
                    ((price * (taxPercent / 100)) + price),
                    '<span class="col-md-2 label label-' + ((checked ==="true") ? 'success' : 'info') + '">' + ((checked ==="true") ? 'Si' : 'No') + '</span>',
                    '<button type="button" class="btn btn-default btn-icon deleteRow"><i class="icon-trash"></i></button>',
                    tax, //tax value
                    checked,//principal value
                    0 //PriceId
                ]).draw(false);


                //Limpiamos los campos
                $('#gain,#price,#priceName, #priceTax').val('');
            }
            else {
                ProcessManager.Warning('Alerta', 'Ya existe un precio principal en la tabla');
            }
        }
    });

    //Metodo para agregar cantidad al almacen
    $(document).on('click', '#BtnAddWarehouse', function myfunction() {
        if (WareHouseValidations()) {
            var $Quantity = $('#Quantity');
            var Warehouse = $('#Warehouse :selected').val();
            var WarehouseName = $('#Warehouse :selected').text();

            //Validamos que para el mismo almacen no se reptia
            if (!ProcessManager.ValidateTableColum($warehouseTable, 3, Warehouse)) {
                $warehouseTable.row.add([
                    WarehouseName,
                    $Quantity.val(),
                    '<button type="button" class="btn btn-default btn-icon deleteRowWarehouse"><i class="icon-trash"></i></button>',
                    Warehouse //warehouse value
                ]).draw(false);

                //Limpiamos los campos
                $('#Quantity,#Warehouse').val('');
            }
            else {
                ProcessManager.Warning('Alerta', 'El almacén que intenta ingresar ya tiene una cantidad ingresada.');
            }
        }
    });

    //Metodo para eliminar un precio
    $(document).on('click', '.deleteRow', function myfunction() {
        $priceTable.row($(this).parents('tr'))
            .remove()
            .draw();
    });

    //Metodo para eliminar una cantidad del almacen
    $(document).on('click', '.deleteRowWarehouse', function myfunction() {
        $warehouseTable.row($(this).parents('tr'))
            .remove()
            .draw();
    });


    $(document).on('click', '.status', function myfunction() {
        $option = $(this);

        var question = ProcessManager.Question('Alerta', 'Está seguro de que desea ' + (($option.attr('data-status') ==='True') ? 'activar' : 'desactivar') + ' este producto?.');

        question.then((answer) => {
            if (answer) {

                $.ajax({
                    url: ProcessManager.getUrl('product/StatusProduct'),
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        status: $option.attr('data-status'),
                        Id: $option.attr('data-product')
                    },
                    beforeSend: function myfunction() {
                        ProcessManager.Loading(true, 'body');
                    },
                    success: function (data) {
                        if (data.status ===true) {
                            ProcessManager.Success('Notificación', data.message);
                            ProcessManager.Redirect(data.redirect);
                        }
                        else {
                            ProcessManager.Warning('Alerta', data.message);
                            ProcessManager.Loading(false, 'body');
                        }
                    },
                    error: function myfunction(xhr, status) {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.validateStatusCode(xhr.status);
                    }
                });
            }
        });
    });

    $(document).on('click', '#btnPhotoProfile', function myfunction() {
        $('#PhotosUpload').trigger('click');
    });

    function uploadProgressHandler(event) {
        $("#loaded_n_total").html("Uploaded " + event.loaded + " bytes of " + event.total);
        var percent = (event.loaded / event.total) * 100;
        var progress = Math.round(percent);
        $("#btnPhotoProfile").attr('disabled', true);
        $("#btnPhotoProfile").html("<i class=\"icon-file-upload position-left\"></i> Porcentage cargado: " + progress + "%");
    }

    function loadHandler(event) {
        $("#status").html(event.target.responseText);
        $("#btnPhotoProfile").html("<i class=\"icon-camera position-left\"></i> Cargar imagen");
        $("#btnPhotoProfile").attr('disabled', false);
    }

    function errorHandler(event) {
        $("#status").html("Upload Failed");
    }

    function abortHandler(event) {
        $("#status").html("Upload Aborted");
    }

    $(document).on('change', '#PhotosUpload', function myfunction() {
        var srcActual = $('#Picture-Account').attr("src");

        if (ProcessManager.ValidateImage($('#PhotosUpload'))) {
            event.preventDefault();

            var formData = new FormData(document.getElementById("Picture-Profile"));

            $.ajax({
                url: (($('#edit-product').length > 0) ? ProcessManager.getUrl('product/SaveUploadedPicture') : ProcessManager.getUrl('product/SaveUploadedPicture')),
                method: 'POST',
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                beforeSend: function myfunction() {
                    $('#Picture-Account').attr("src", ProcessManager.GIF);
                },
                success: function (data) {
                    if (data.status ===true) {
                        $('#Picture-Account').attr("src", data.file);
                        $('#picture').val(data.fileName);
                    }
                    else {
                        ProcessManager.Warning('Alerta', data.message);
                        $('#Picture-Account').attr("src", srcActual);
                        $('#picture').val("");
                    }
                },
                error: function (datos) {
                    ProcessManager.Warning('Alerta', datos.message);
                    $('#Picture-Account').attr("src", srcActual);
                    $('#picture').val("");
                },
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress",
                        uploadProgressHandler,
                        false
                    );
                    xhr.addEventListener("load", loadHandler, false);
                    xhr.addEventListener("error", errorHandler, false);
                    xhr.addEventListener("abort", abortHandler, false);
                    return xhr;
                }
            });
        }
    });
});



function CalculatePrice() {
    var $gain = $('#gain');

    if ($gain.val() != "" && $gain.val() != undefined) {
        cost = parseFloat(ProcessManager.RemoveNumberFormat($('[name="Product.Cost"]').val()));

        var percent = parseFloat($('#gain').val()) / 100;
        var price = (percent * cost) + cost;

        $('#price').val(price.toFixed(2));
    }
    else {
        $('#price').val("0.00");
    }
}

function PriceValidations() {
    var $gain = $('#gain');
    var $price = $('#price');
    var $name = $('#priceName');
    var tax = $('#priceTax :selected').val();

    if (parseFloat($gain.val()) < 0) {
        ProcessManager.Warning('Alerta', "Para agregar un precio la ganancia no puede ser negativa.");
        return false;
    }
    else if (parseFloat($price.val()) <= 0 || $price.val() ===undefined) {
        ProcessManager.Warning('Alerta', "El precio del producto no puede ser menor o igual a 0 o estar en blanco.");
        return false;
    }
    else if ($name.val() ===undefined || $name.val() ==="") {
        ProcessManager.Warning('Alerta', "Debe agregar un nombre al precio.");
        return false;
    }
    else if (tax ===undefined || tax ==="") {
        ProcessManager.Warning('Alerta', "Debe seleccionar el impuesto que se le aplicará al precio.");
        return false;
    }
    else {
        return true;
    }
}


function WareHouseValidations() {
    var $Quantity = $('#Quantity');
    var Warehouse = $('#Warehouse :selected').val();

    if ($Quantity.val() ==="" || $Quantity.val() ===undefined || parseFloat($Quantity.val()) <= 0) {
        ProcessManager.Warning('Alerta', "Debe ingresar la cantidad del producto.");
        return false;
    }
    else if (Warehouse ===undefined || Warehouse ==="") {
        ProcessManager.Warning('Alerta', "Debe seleccionar un almacén.");
        return false;
    }
    else {
        return true;
    }
}

function ConvertArraryToObjectPrice(array) {
    var PriceMemory = {
        Memory: [],
        add: function (TaxId, Name, Utility, Price, Principal, Id) {
            var price = {
                TaxId: TaxId,
                Name: Name,
                Utility: Utility,
                Price: Price,
                IsDefault: Principal,
                Id: Id
            };
            PriceMemory.Memory.push(price);
        }
    };

    for (var i = 0; i < array.length; i++) {
        PriceMemory.add(array[i][7], array[i][1], array[i][2], array[i][3], array[i][8], array[i][9]);
    }

    return PriceMemory.Memory;
}

function ConvertArraryToObjectWareHouse(array) {
    var WarehouseMemory = {
        Memory: [],
        add: function (WarehouseId, ProductAmount) {
            var price = {
                WarehouseId: WarehouseId,
                ProductAmount: ProductAmount
            };
            WarehouseMemory.Memory.push(price);
        }
    };

    for (var i = 0; i < array.length; i++) {
        WarehouseMemory.add(array[i][3], array[i][1]);
    }

    return WarehouseMemory.Memory;
}



///Cargamos los datos a la tabla
function CallPriceTable() {

    var columnsTitle = [
        { title: "Impuesto" },
        { title: "Nombre" },
        { title: "Ganancia" },
        { title: "Precio" },
        { title: "Precio total" },
        { title: "Principal" },
        { title: "Eliminar" },
        { title: "TaxId" },
        { title: "IsPrincipal" },
        { title: "Id" }
    ];

    var columnVisible = [
        { "visible": false, "targets": 7 },
        { "visible": false, "targets": 8 },
        { "visible": false, "targets": 9 }
    ];

    if ($('#edit-product').length > 0) {
        var form = ProcessManager.LoadForm(ProcessManager.getUrl('product/GetProductPrice'), { ProductId: $('[name = "Product.Id"]').val() });

        form.then((data) => {
            $priceTable = $('#PriceTable').DataTable({
                bLengthChange: false,
                data: data.prices,
                columnDefs: columnVisible,
                columns: columnsTitle,
                language: {
                    processing: "Procesando",
                    search: "Buscar:",
                    lengthMenu: "Ver _MENU_ Filas",
                    info: "_START_ - _END_ de _TOTAL_ elementos",
                    infoEmpty: "0 - 0 de 0 elementos",
                    infoFiltered: "(Filtro de _MAX_ entradas en total)",
                    loadingRecords: "Cargando datos.",
                    zeroRecords: "No se encontraron datos",
                    emptyTable: "No hay datos disponibles",
                    paginate: {
                        first: "Primero",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Ultimo"
                    },
                    aria: {
                        sortAscending: ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    }
                }
            });
        });
    }
    else {
        $priceTable = $('#PriceTable').DataTable({
            bLengthChange: false,
            data: null,
            columns: columnsTitle,
            columnDefs: columnVisible,
            language: {
                processing: "Procesando",
                search: "Buscar:",
                lengthMenu: "Ver _MENU_ Filas",
                info: "_START_ - _END_ de _TOTAL_ elementos",
                infoEmpty: "0 - 0 de 0 elementos",
                infoFiltered: "(Filtro de _MAX_ entradas en total)",
                loadingRecords: "Cargando datos.",
                zeroRecords: "No se encontraron datos",
                emptyTable: "No hay datos disponibles",
                paginate: {
                    first: "Primero",
                    previous: "Anterior",
                    next: "Siguiente",
                    last: "Ultimo"
                },
                aria: {
                    sortAscending: ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre décroissant"
                }
            }
        });
    }
}