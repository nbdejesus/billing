﻿$(document).ready(function myfunction() {

    $(document).on('click', '#BtnCreate', function myfunction() {
        var $option = $(this);
 
        var form = ProcessManager.LoadForm(ProcessManager.getUrl('SubCategory/_SubCategoryIndex'), null);

        form.then((data) => {
            ProcessManager.ShowModal("Agregar Subcategoría", 'icon-plus2', data, true, true);
 
            ConfingForm();
        });
   
    });

    $(document).on('click', '.edit', function myfunction() {
        var $option = $(this);
        var form = ProcessManager.LoadForm(ProcessManager.getUrl('SubCategory/_EditSubCategory'), { Id: $option.attr('data-id') });

        form.then((data) => {
            ProcessManager.ShowModal("Editar Subcategoría ", 'icon-plus2', data, true, true);

            ConfingForm();
        });
    });

    $(document).on('click', '#MdAceptar', function myfunction() {
        var $form = ((($('#form-SubCategory').length > 0) ? $('#form-SubCategory') : $('#form-SubCategory-edit')));

        if ($form.valid()) {
            $.ajax({
                url: ((($('#form-SubCategory').length > 0) ? ProcessManager.getUrl('SubCategory/CreateSubCategory') : ProcessManager.getUrl('SubCategory/UpdateSubCategory'))),
                type: 'POST',
                data: $form.serialize(),
                beforeSend: function myfunction() {
                    ProcessManager.Loading(true, '.panel');
                },
                success: function myfunction(data) {
                    if (data.status) {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.Success('Notificación', data.message);
                        ProcessManager.Redirect(data.redirect);
                    }
                    else {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.Warning('Alerta', data.message);
                    }
                },
                error: function myfunction(xhr, status) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.validateStatusCode(xhr.status);
                }
            });
        }
    });


    $(document).on('click', '.status', function myfunction() {
        $option = $(this);

        var question = ProcessManager.Question('Alerta', 'Está seguro de que desea ' + (($option.attr('data-status') ==='True') ? 'activar' : 'desactivar') + ' esta subcategoría?.');

        question.then((answer) => {
            if (answer) {

                $.ajax({
                    url: ProcessManager.getUrl('SubCategory/StatusSubCategory'),
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        status: $option.attr('data-status'),
                        Id: $option.attr('data-id')
                    },
                    beforeSend: function myfunction() {
                        ProcessManager.Loading(true, 'body');
                    },
                    success: function (data) {
                        if (data.status ===true) {
                            ProcessManager.Success('Notificación', data.message);
                            ProcessManager.Redirect(data.redirect);
                        }
                        else {
                            ProcessManager.Warning('Alerta', data.message);
                            ProcessManager.Loading(false, 'body');
                        }
                    },
                    error: function (xhr, status) {
                        ProcessManager.validateStatusCode(xhr.status);
                        ProcessManager.Loading(false, 'body');
                    }
                });
            }
        });
    });
});


function ConfingForm() {
    $('form').validate({
        rules: {
            'SubCategory.Name': { required: true },
            'SubCategory.CategoryId': { required: true }
        },
        messages: {
            'SubCategory.Name': { required: 'Campo requerido' },
            'SubCategory.CategoryId': { required: 'Campo requerido'  }
        },
        debug: true
    });
    $('.select').selectize({
        create: true,
        sortField: {
            field: 'text',
            direction: 'asc'
        }
    });
}