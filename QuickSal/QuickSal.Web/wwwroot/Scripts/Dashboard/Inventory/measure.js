﻿$(document).ready(function myfunction() {

    $(document).on('click', '#BtnCreate', function myfunction() {
        var $option = $(this);
        var form = ProcessManager.LoadForm(ProcessManager.getUrl('Measure/_MeasureIndex'), null);

        form.then((data) => {
            ProcessManager.ShowModal("Agregar medida", 'icon-plus2', data, true, true);

            ConfingForm();
        });
    });

    $(document).on('click', '.edit', function myfunction() {
        var $option = $(this);
        var form = ProcessManager.LoadForm(ProcessManager.getUrl('Measure/_EditMeasure'), { Id: $option.attr('data-id') });

        form.then((data) => {
            ProcessManager.ShowModal("Editar medida", 'icon-plus2', data, true, true);

            ConfingForm();
        });
    });

    $(document).on('click', '#MdAceptar', function myfunction() {
        var $form = ((($('#form-measure').length > 0) ? $('#form-measure') : $('#form-measure-edit')));

        if ($form.valid()) {
            $.ajax({
                url: ((($('#form-measure').length > 0) ? ProcessManager.getUrl('Measure/CreateMeasure') : ProcessManager.getUrl('Measure/UpdateMeasure'))),
                type: 'POST',
                data: $form.serialize(),
                beforeSend: function myfunction() {
                    ProcessManager.Loading(true, '.panel');
                },
                success: function myfunction(data) {
                    if (data.status) {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.Success('Notificación', data.message);
                        ProcessManager.Redirect(data.redirect);
                    }
                    else {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.Warning('Alerta', data.message);
                    }
                },
                error: function myfunction(xhr, status) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.validateStatusCode(xhr.status);
                }
            });
        }
    });


    $(document).on('click', '.status', function myfunction() {
        $option = $(this);

        var question = ProcessManager.Question('Alerta', 'Está seguro de que desea ' + (($option.attr('data-status') ==='True') ? 'activar' : 'desactivar') + ' esta medida?.');

        question.then((answer) => {
            if (answer) {

                $.ajax({
                    url: ProcessManager.getUrl('Measure/StatusMeasure'),
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        status: $option.attr('data-status'),
                        Id: $option.attr('data-id')
                    },
                    beforeSend: function myfunction() {
                        ProcessManager.Loading(true, 'body');
                    },
                    success: function (data) {
                        if (data.status ===true) {
                            ProcessManager.Success('Notificación', data.message);
                            ProcessManager.Redirect(data.redirect);
                        }
                        else {
                            ProcessManager.Warning('Alerta', data.message);
                            ProcessManager.Loading(false, 'body');
                        }
                    },
                    error: function (xhr, status) {
                        ProcessManager.validateStatusCode(xhr.status);
                        ProcessManager.Loading(false, 'body');
                    }
                });
            }
        });
    });
});


function ConfingForm() {
    $('form').validate({
        rules: {
            'Measure.Name': { required: true },
            'Measure.Abbreviation': { required: true, maxlength: 4 },
            'Measure.Quantity': { required: true}
        },
        messages: {
            'Measure.Name': { required: 'Campo requerido' },
            'Measure.Abbreviation': { required: 'Campo requerido', maxlength: 'La abreviatura debe tener solo 4 letras.' },
            'Measure.Quantity': { required: 'Campo requerido'}
        },
        debug: true
    });
}