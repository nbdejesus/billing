﻿$(document).ready(function myfunction() {
    $('#BtnDownload').click(function myfunction(e) {
        var $form = $('#frmTemplate');

        if ($('[name="DocumentTemplateId"]').val() == "") {
            ProcessManager.Warning('Alerta', 'Debe seleccionar una plantilla');
        }
        else {
            $.ajax({
                url: ProcessManager.getUrl('LoadData/DownloadTemplate'),
                method: "GET",
                dataType: "json",
                data: $form.serialize(),
                beforeSend: function myfunction() {
                    ProcessManager.Loading(true, 'body');
                },
                success: function (data) {
                    if (data.status) {
                        ProcessManager.saveByteArray(data.name, ProcessManager.base64ToArrayBuffer(data.file), data.format);
                        ProcessManager.Loading(false, 'body');
                    }
                    else {
                        ProcessManager.Warning('Alerta', data.mensaje);
                        ProcessManager.Loading(false, 'body');
                    }
                },
                error: function myfunction(xhr, status) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.validateStatusCode(xhr.status);
                }
            });
        }
    });






    //Subir libro ==================================================================================================
    $(document).on('click', '#btnTemplate', function myfunction() {
        $('#TemplateFile').trigger('click');
    });

    $(document).on('change', '#TemplateFile', function myfunction() {
        var srcActual = $('#Picture-template').attr("src");
        extensionesPermitidas = new Array(".xlsx");
        
        if (ProcessManager.ValidateTypeFile($('#TemplateFile'), extensionesPermitidas)) {

            event.preventDefault();

            var formData = new FormData(document.getElementById("TemplateUpload"));

            $.ajax({
                url: ProcessManager.getUrl('LoadData/_ProcessTemplate'),
                method: 'POST',
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                beforeSend: function myfunction() {
                    $('#TemplateResult').html('');      
                    ProcessManager.Loading(true, 'body');
                },
                success: function (data) {
                    $('#TemplateResult').html(data);
                    CallDinamicTable('.datatable', 10);
                    ProcessManager.Loading(false, 'body');
                    $('#TemplateFile').val('');
                },
                error: function (datos) {
                    ProcessManager.Warning('Alerta', datos.message);
                    $('#TemplateFile').val('');
                }
            });
        }
    });
});