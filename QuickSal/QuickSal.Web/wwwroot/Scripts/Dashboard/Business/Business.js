﻿$(document).ready(function myfunction() {

    if ($('[name="Business.DocumentTypeId"]').length > 0) {
        ProcessManager.getDocumentTypeConfiguration();
    }

    // Show form
    var form = $(".steps-validation").show();

    // Initialize wizard
    $(".steps-validation").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        titleTemplate: '<span class="number">#index#</span> #title#',
        autoFocus: true,
        labels: {
            finish: 'Aceptar',
            next: 'Siguiente',
            previous: "Anterior",
            loading: 'Procesando...'
        },
        onStepChanging: function (event, currentIndex, newIndex) {

            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex) {
                return true;
            }

            // Forbid next action on "Warning" step if the user is to young
            //if (newIndex === 3 && Number($("#age-2").val()) < 18) {
            //    return false;
            //}

            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex) {

                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }

            form.validate({
                rules: {
                    'Business.Name': { required: true, minlength: 3 },
                    'Business.DocumentTypeId': 'required',
                    'Business.DocumentNumber': 'required',
                    'Business.IdBusinessType': 'required',
                    'BranchOffice.AddressBranchOffice': 'required',
                    'BranchOffice.Email': { email: true }
                },
                messages: {
                    'Business.Name': { required: 'Debe ingresar el nombre', minlength: 'El nombre de la empresa debe ser mas largo de 3 caracteres' },
                    'Business.DocumentTypeId': 'El tipo de identificación es requerido',
                    'Business.DocumentNumber': 'Debe ingresar el número de documento',
                    'Business.IdBusinessType': 'Debe ingresar el tipo de empresa',
                    'BranchOffice.AddressBranchOffice': 'Debe ingresar la dirección de la empresa',
                    'BranchOffice.Email': { email: 'Debe ingresar el correo electrónico con el formato correcto. Por ejemplo: u@localhost.com' }
                },
                debug: true
            });
            //form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },

        onStepChanged: function (event, currentIndex, priorIndex) {

            //// Used to skip the "Warning" step if the user is old enough.
            //if (currentIndex === 2 && Number($("#age-2").val()) >= 18) {
            //    form.steps("next");
            //}

            //// Used to skip the "Warning" step if the user is old enough and wants to the previous step.
            //if (currentIndex === 2 && priorIndex === 3) {
            //    form.steps("previous");
            //}
        },

        onFinishing: function (event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            if ($('#PlanId').val() == "") {
                ProcessManager.Warning('Alerta', 'Debe seleccionar un plan para poder continuar con el registro de la empresa.');
                return false;
            }
            else {
                if (form.valid()) {
                    $.ajax({
                        url: ProcessManager.getUrl('Business/CreateBusiness'),
                        type: 'POST',
                        data: form.serialize(),
                        beforeSend: function myfunction() {
                            ProcessManager.Loading(true, 'body');
                        },
                        success: function myfunction(data) {
                            if (data.status) {
                                ProcessManager.Loading(false, 'body');
                                ProcessManager.Success('Notificación', data.message);
                                ProcessManager.Redirect(data.redirect);
                            }
                            else {
                                ProcessManager.Loading(false, 'body');
                                ProcessManager.Warning('Alerta', data.message);
                            }
                        },
                        error: function myfunction(xhr, status) {
                            ProcessManager.Loading(false, 'body');
                            ProcessManager.validateStatusCode(xhr.status);
                        }
                    });
                }
            }          
        }
    });



    $(document).on('click', '#btnUploaLogo', function myfunction() {
        $('#LogoFile').trigger('click');
    });

    function uploadProgressHandler(event) {
        $("#loaded_n_total").html("Uploaded " + event.loaded + " bytes of " + event.total);
        var percent = (event.loaded / event.total) * 100;
        var progress = Math.round(percent);
        $("#btnUploaLogo").attr('disabled', true);
        $("#btnUploaLogo").html("<i class=\"icon-file-upload position-left\"></i> Porcentage cargado: " + progress + "%");
    }

    function loadHandler(event) {
        $("#status").html(event.target.responseText);
        $("#btnUploaLogo").html("<i class=\"icon-upload position-left\"></i> Subir logo");
        $("#btnUploaLogo").attr('disabled', false);
    }

    function errorHandler(event) {
        $("#status").html("Upload Failed");
    }

    function abortHandler(event) {
        $("#status").html("Upload Aborted");
    }

    $(document).on('change', '#LogoFile', function myfunction() {
        var srcActual = $('#UploadFileLogo').attr("src");
        var $form = $("#BookUpload");

        if (ProcessManager.ValidateImage($('#LogoFile'))) {

            event.preventDefault();

            var formData = new FormData(document.getElementById("Picture-logo"));

            $.ajax({
                url: ProcessManager.getUrl('Business/UploadLogo'),
                method: 'POST',
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                beforeSend: function myfunction() {
                    $('#UploadFileLogo').attr("src", ProcessManager.GIF);
                },
                success: function (data) {
                    if (data.status === true) {
                        $('#UploadFileLogo').attr("src", data.filePath);
                        $('#Logo').val(data.fileName);
                    }
                    else {
                        ProcessManager.Warning('Alerta', data.message);
                        $('#UploadFileLogo').attr("src", srcActual);
                        $('#Logo').val("");
                    }
                },
                error: function (datos) {
                    ProcessManager.Warning('Alerta', datos.message);
                    $('#UploadFileLogo').attr("src", srcActual);
                    $('#Logo').val("");
                },
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress",
                        uploadProgressHandler,
                        false
                    );
                    xhr.addEventListener("load", loadHandler, false);
                    xhr.addEventListener("error", errorHandler, false);
                    xhr.addEventListener("abort", abortHandler, false);
                    return xhr;
                }
            });
        }
    });

    $(document).on('change', '#BusinessType', function myfunction() {
        var option = $(this).children(':selected');

        if (option.val() != "") {
            var form = ProcessManager.LoadForm(ProcessManager.getUrl('plan/_BusinessPlan'), { businessTypeId: option.val() }, '#BusinessPlan');

            form.then((data) => {
                $('#BusinessPlan').html('');
                $('#BusinessPlan').html(data);
            });
        }
    });

    //Cargamos validaciones si estamos en la vista de editar
    if ($('#Edit-business').length > 0) {
        $('form').validate({
            rules: {
                'Business.Name': { required: true, minlength: 3 },
                'Business.DocumentTypeId': 'required',
                'Business.DocumentNumber': 'required',
                'BranchOffice.AddressBranchOffice': 'required',
                'BranchOffice.Email': { email: true }
            },
            messages: {
                'Business.Name': { required: 'Debe ingresar el nombre', minlength: 'El nombre de la empresa debe ser mas largo de 3 caracteres' },
                'Business.DocumentTypeId': 'El tipo de identificación es requerido',
                'Business.DocumentNumber': 'Debe ingresar el número de documento',
                'BranchOffice.AddressBranchOffice': 'Debe ingresar la dirección de la empresa',
                'BranchOffice.Email': { email: 'Debe ingresar el correo electrónico con el formato correcto. Por ejemplo: u@localhost.com' }
            },
            debug: true
        });
    }

    $(document).on('click', '.btn-plan', function myfunction() {
        $('.btn-plan').removeClass('bg-danger').addClass('bg-blue');
        $('.btn-plan').html('<i class="icon-cart position-left"></i> Seleccionar');

        if ($(this).hasClass('bg-blue')) {
            $(this).removeClass('bg-blue').addClass('bg-danger');
            $(this).html('<i class="icon-check position-left"></i> Seleccionado');
        }

        $('#PlanId').val($(this).attr('data-plan'));
    });


    //Metodo para actualizar y crear una empresa
    $(document).on('click', '#btnAceptar', function myfunction() {
        $('#PlanId').val($(this).attr('data-plan'));

        var sendform = $('#Edit-business');
        if (sendform.valid()) {
            $.ajax({
                url: ProcessManager.getUrl('Business/UpdateBusiness'),
                type: 'POST',
                data: sendform.serialize(),
                beforeSend: function myfunction() {
                    ProcessManager.Loading(true, 'body');
                },
                success: function myfunction(data) {
                    if (data.status) {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.Success('Notificación', data.message);
                        ProcessManager.Redirect(data.redirect);
                    }
                    else {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.Warning('Alerta', data.message);
                    }
                },
                error: function myfunction(xhr, status) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.validateStatusCode(xhr.status);
                }
            });
        }
    });


});


