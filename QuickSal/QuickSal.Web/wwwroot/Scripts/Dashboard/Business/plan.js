﻿$(document).ready(function myfunction() {

    LoadPlan();

    $(document).on('click', '.btn-plan', function myfunction() {
        var question = ProcessManager.Question('Alerta', 'Está seguro de que desea cambiar de plan?.');

        question.then((answer) => {
            if (answer) {

                $('.btn-plan').removeClass('bg-danger').addClass('bg-blue');
                $('.btn-plan').html('<i class="icon-cart position-left"></i> Seleccionar');

                if ($(this).hasClass('bg-blue')) {
                    $(this).removeClass('bg-blue').addClass('bg-danger');
                    $(this).html('<i class="icon-check position-left"></i> Seleccionado');
                }      

                $.ajax({
                    url: ProcessManager.getUrl('plan/UpdateBusinessPlan'),
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        Id: $(this).attr('data-plan')
                    },
                    beforeSend: function myfunction() {
                        ProcessManager.Loading(true, 'body');
                    },
                    success: function (data) {
                        if (data.status == true) {
                            ProcessManager.Success('Notificación', data.message);
                            ProcessManager.Redirect(data.redirect);
                        }
                        else {
                            
                            ProcessManager.Warning('Alerta', data.message);
                            ProcessManager.Loading(false, 'body');

                            $('.btn-plan').removeClass('bg-danger').addClass('bg-blue');
                            $('.btn-plan').html('<i class="icon-cart position-left"></i> Seleccionar');
                        }
                    },
                    error: function myfunction(xhr, status) {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.validateStatusCode(xhr.status);

                        $('.btn-plan').removeClass('bg-danger').addClass('bg-blue');
                        $('.btn-plan').html('<i class="icon-cart position-left"></i> Seleccionar');
                    }
                });
            }
        });

    });
});


function LoadPlan() {
    var BusinessType = $('#BusinessType').val();

    var items = ProcessManager.LoadForm(ProcessManager.getUrl('plan/_BusinessPlan'), {
        businessTypeId: BusinessType
    });

    items.then((data) => {
        $('#plans').html(data);

        $('.btn-plan').each(function myfunction() {
            if ($(this).attr('data-plan') == $('#plan').val()) {
                $(this).removeClass('bg-blue').addClass('bg-danger');
                $(this).html('<i class="icon-check position-left"></i> Seleccionado');
                $(this).attr('disabled', true);
            }
        });
    });
} 