﻿$(document).ready(function myfunction() {
    // funcionalidad de los checkbox
    $(document).on('click', '#allChecked', function myfunction(event) {
        if (this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function () {
                this.checked = true;
            });
        }
        else {
            $(':checkbox').each(function () {
                this.checked = false;
            });
        }
    });

    $(document).on('click', '#BtnCreate', function myfunction() {

        var $option = $(this);
        var form = ProcessManager.LoadForm(ProcessManager.getUrl('VendorPayment/_OrdenCompraList'), null);
        form.then((data) => {
            ProcessManager.ShowModal("Facturas de compras", 'icon-plus2', data, true, true,2);

            CallDinamicTable('#OrdenList .datatable', 10);
            $('.modal-footer #btnCancel').show();
            $('.modal-footer #MdAceptar').show();
            $('#agregar').remove();
            $('#cerrarM').remove();
            $('.modal-footer #Cancelar').remove();
            $('.modal-footer #btActualizar').remove();
        });
    });

    $(document).on('click', '.status', function myfunction() {
        $option = $(this);

        var question = ProcessManager.Question('Alerta', 'Está seguro de que desea ' + (($option.attr('data-status') == 'True') ? 'activar' : 'cambiar el') + ' estado a del pago?.');

        question.then((answer) => {
            if (answer) {

                $.ajax({
                    url: ProcessManager.getUrl('VendorPayment/StatusPayment'),
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        DocumentStatusID: $option.attr('data-estado'),
                        Id: $option.attr('data-id')
                    },
                    beforeSend: function myfunction() {
                        ProcessManager.Loading(true, 'body');
                    },
                    success: function (data) {
                        if (data.status == true) {
                            ProcessManager.Success('Notificación', data.message);
                            ProcessManager.Redirect(data.redirect);
                        }
                        else {
                            ProcessManager.Warning('Alerta', data.message);
                            ProcessManager.Loading(false, 'body');
                        }
                    },
                    error: function myfunction(xhr, status) {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.validateStatusCode(xhr.status);
                    }
                });
            }
        });
    });

    //Esta parte elimina una fila de la tabla //Si te fijas usa la clase que se declaro en el metodo de arriba  //Este metodo lo borra de memoria y del html
    $(document).on('click', '.deleteRow', function myfunction() {
        $VendorPaymentTable.row($(this).parents('tr'))
            .remove()
            .draw();

        CalcularMontos($VendorPaymentTable.data().toArray());
    });

    ///////////////envia los datos hacia el controlador para crear el Purchase/////////////
    $("#Enviar").click(function (e) {
        e.preventDefault();
        var validar = ValidarRow();

        if (validar.okay) {
        var documentStatus = parseInt($(this).attr('data-document'));
            EditVendorPayment(documentStatus);
        } else {
            ProcessManager.Warning('Notificación', validar.mensaje);
        } 
    });

    $("#Procesar").click(function (e) {
      var estatus =  $(this).attr('data-document')
        e.preventDefault();
        var validar = ValidarRow();

        if (validar.okay) {
        var documentStatus = estatus;
        EditVendorPayment(documentStatus);
    } else {
            ProcessManager.Warning('Notificación', validar.mensaje);
        } 
    });
    VendorPaymentTable();
    
});

function ValidarRow() {
    var objecto = {
        mensaje: "",
        okay: false
    }
    var $EditVendorPayment = $('#EditVendorPayment').DataTable();

    var cells = $EditVendorPayment.cells().nodes();
    var cellsOfRow = cells.data();
    var compareWith = "";
    if (0 < cellsOfRow.length) {

        for (var i = 0; i < cellsOfRow.length; i++) {
            compareWith = cellsOfRow[i][0];
            // Buscamos el texto en el contenido de la celda

            if (parseFloat(cellsOfRow[i][7]) <= 0) {
                objecto.mensaje = "Debe ingresar el monto de la orden";
                objecto.okay = false;
                return objecto;
            }
        }
        objecto.okay = true;
        return objecto;
    }
    else {
        objecto.mensaje = "No hay pagos seleccionados...";
        objecto.okay = false;
        return objecto;

    }

}


//Este metodo agrega o envia la informacion al controller
function EditVendorPayment(documentStatus) {
    var $form = $('form');

    if ($form.valid()) {

        var model = $form.serialize() + '&' + $.param({ "VendorPayment.DocumentEstatusId": documentStatus }) + '&' + $.param({ "VendorPayment.TblVendorPaymentRow": ConvertArraryToObject($VendorPaymentTable.data().toArray()) });

        ///Pondras el nombre del create  y lo reemplazas en la url entonces
        $.ajax({
            url: (($('#Edit-PurchaseOrder').length > 0) ? ProcessManager.getUrl('VendorPayment/UpdateVendorPayment') : ProcessManager.getUrl('VendorPayment/CreateVendorPayment')),
            type: 'POST',
            data: model,
            cache: false,
            beforeSend: function myfunction() {
                ProcessManager.Loading(true, 'body');
            },
            success: function myfunction(data) {
                if (data.status) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.Success('Notificación', data.message);
                    ProcessManager.Redirect(data.redirect);
                }
                else {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.Warning('Alerta', data.message);
                }
            },
            error: function myfunction(xhr, status) {
                ProcessManager.Loading(false, 'body');
                ProcessManager.validateStatusCode(xhr.status);
            }
        });
    }
    else {
        ProcessManager.Warning('Alerta', 'Faltan campos por llenar.');
    }
}

async function VendorPaymentTable() {
    //Estas con las columnas que apareceran en la tabla
    var columnsTitle = [
        { title: "Id" },
        { title: "VendorPaymentId" },
        { title: "PurchaseOrderId" },
        { title: "SuppliersId" },
        { title: "No.Orden" },
        { title: "Proveedor" },
        { title: "Concepto" },//0
        { title: "Balance" },//0
        { title: "Pagado" },//1
        { title: "A Pagar" },//2    
        { title: "Pendiente" },//2 
        { title: "Opciones" },

        //{ title: "Opciones" }, { title: "Opciones" },

    ];
    if ($("#estado").val() == "2") {
        //Con este tu le dices al datable cuales columnas ocultar
        var columnVisible = [
            //esa columna la tenias 
            { "visible": false, "targets": 0 },
            { "visible": false, "targets": 1 },
            { "visible": false, "targets": 2 },
            { "visible": false, "targets": 3 },
            //{ "visible": false, "targets": 10},
            //{ "visible": false, "targets": 11 },

        ];
    }
    else {
        //Con este tu le dices al datable cuales columnas ocultar
        var columnVisible = [
            //esa columna la tenias 
            { "visible": false, "targets": 0 },
            { "visible": false, "targets": 1 },
            { "visible": false, "targets": 2 },
            { "visible": false, "targets": 3 },
            //{ "visible": false, "targets": 10},
            { "visible": false, "targets": 11 },

        ];

    }


    //esto valida si estas en el formulario de edit o crear
    const data = await ProcessManager.LoadForm(ProcessManager.getUrl('VendorPayment/GetVendorPayment'), { PurchaseId: $('[name="VendorPayment.Id"]').val() });
    //este codigo te carga solamente el detalle de la orden de compra
    //form.then((data) => {
    
    $VendorPaymentTable = $('#EditVendorPayment').DataTable({
        bLengthChange: false,
        data: data.prices,//aqui es donde se le pasa el objeto con sus datos<---
        columnDefs: columnVisible,
        columns: columnsTitle,
        bSort: false,
        language: {
            processing: "Procesando",
            search: "Buscar:",
            lengthMenu: "Ver _MENU_ Filas",
            info: "_START_ - _END_ de _TOTAL_ elementos",
            infoEmpty: "0 - 0 de 0 elementos",
            infoFiltered: "(Filtro de _MAX_ entradas en total)",
            loadingRecords: "Cargando datos.",
            zeroRecords: "No se encontraron datos",
            emptyTable: "No hay datos disponibles",
            paginate: {
                first: "Primero",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultimo"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }

    });
    
    CalcularMontos($VendorPaymentTable.data().toArray());
    //});
}

$(document).on('click', '.modificarProducto', function myfunction() {
    var parent = $(this).parent().parent().parent().parent().parent().parent();
    var form = ProcessManager.LoadForm(ProcessManager.getUrl('VendorPayment/EditVendorPaymentRow'), { Id: $(this).attr('data-id') }, '.panel');
    form.then((data) => {
        ProcessManager.ShowModal("Editar Orden ", 'icon-plus2', data, true, true,2);

        CallDinamicTable('#tableList', 10, false);

        $('.modal-footer #btnCancel').hide();
        $('.modal-footer #MdAceptar').hide();


        $(".modal-footer").append('<div class=\"panel-footer\">' +
            '<button id=\"Cancelar\" type=\"button\" class=\"btn btn-link\" data-dismiss=\"modal\">Cerrar</button>' +
            '<button id=\"btActualizar\" type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Aceptar</button></div>');

        valorModal(parent);
    });
    $('.modal-footer .panel-footer').remove();

});

function valorModal(parent) {

    var cells = $VendorPaymentTable.cells().nodes();
    var array = cells.data();
    var index = parent.index();
    var l = array[index][0];
    Order.translateOrder(
        array[index][0],
        array[index][1],
        array[index][2],
        array[index][3],
        array[index][4],
        array[index][5],
        array[index][6],
        array[index][7],
        array[index][8],
        array[index][9],
        array[index][10],
        array[index][11],

        index);

    console.log(5)
    let response = parseFloat(Order.apagar.replace(/,/g, ""));
    $("#Id").val(Order.Id);
    $("#VendorPaymentId").val(Order.VendorPaymentId);
    $("#PurchaseOrderId").val(Order.PurchaseOrderId);
    $("#SuppliersId").val(Order.SuppliersId);
    $("#NoDocument").val(Order.noOrden);
    $("#provedor").val(Order.supplierName);
    $("#concepto").val(Order.concepto);
    $("#balance").val(Order.balance);
    $("#pagado").val(Order.pagado);
    $("#aPagar").val(response);
    $("#pendiente").val(Order.pendiente);
    $("#opcion").val(Order.opciones);
    $("#index").val(Order.rowIndex);
}

var Order = {
    Id: 0,
    VendorPaymentId:0,
    PurchaseOrderId: 0,
    SuppliersId: 0,
    noOrden: 0,
    supplierName: '',
    concepto: "",
    balance: 0,
    pagado: 0,
    apagar: 0,
    pendiente: 0,
    opciones: "",
    rowIndex: 0,

    translateOrder: function (Id, VendorPaymentId, PurchaseOrderId, SuppliersId, NoOrden, SupplierName, Concepto, Balance, Pagado, APagar, Pendiente, Opciones, RowIndex) {
        this.Id = Id
        this.VendorPaymentId = VendorPaymentId
        this.PurchaseOrderId = PurchaseOrderId
        this.SuppliersId = SuppliersId
        this.noOrden = NoOrden
        this.concepto = Concepto
        this.supplierName = SupplierName
        this.balance = Balance
        this.pagado = Pagado
        this.apagar = APagar
        this.pendiente = Pendiente
        this.opciones = Opciones
        this.rowIndex = RowIndex
    },
    updateOrder: function (Id, VendorPaymentId, PurchaseOrderId, SuppliersId, NoOrden, SupplierName, Concepto, Balance, Pagado, APagar, Pendiente, Opciones, RowIndex) {

        this.Id = Id,
        this.VendorPaymentId = VendorPaymentId,
        this.PurchaseOrderId = PurchaseOrderId,
        this.SuppliersId = SuppliersId,
        this.noOrden = NoOrden
        this.concepto = Concepto
        this.supplierName = SupplierName
        this.balance = Balance
        this.pagado = Pagado
        this.apagar = formatMoney(APagar, decimalCount = 2, decimal = ".", thousands = ","); 
        this.pendiente = Pendiente
        this.opciones = Opciones
        this.rowIndex = RowIndex
    },
    updateTable: function () {
        let array = [this.Id, this.VendorPaymentId, this.PurchaseOrderId, this.SuppliersId, this.noOrden, this.supplierName, this.concepto, this.balance, this.pagado, this.apagar, this.pendiente, this.opciones];
        $VendorPaymentTable.row(this.rowIndex).data(array).draw();

    }
};

$(document).on('click', '#btActualizar', function myfunction() {

    Order.updateOrder(
        $("#Id").val(),
        $("#VendorPaymentId").val(),
        $("#PurchaseOrderId").val(),
        $("#SuppliersId").val(),
        $("#NoDocument").val(),
        $("#provedor").val(),
        $("#concepto").val(),
        $("#balance").val(),
        $("#pagado").val(),
        $("#aPagar").val(),
        $("#pendiente").val(),
        $("#opcion").val(),
        $("#index").val());
    Order.updateTable();
    CalcularMontos($VendorPaymentTable.data().toArray());
});

function CalcularMontos(tabla) {
    console.log(5)
    //var TotalDevengado = $("#balance").val();
    //var balance = parseFloat(TotalDevengado.replace(/,/g, ""));
    valor = 0;
    for (var i = 0; i < tabla.length; i++) {

        var response1= parseFloat(tabla[i][9]);
        if (response1>0) {
            var response2 = parseFloat(tabla[i][9].replace(/,/g, ""));
             valor +=response2;
        }
    }
    var response = formatMoney(valor, decimalCount = 2, decimal = ".", thousands = ",");
    $("#mensaje").text(response);
    $("#mensajesubtotal").val(valor.toFixed(2));
}

$(document).on('change', '#aPagar', function () {

    var TotalDevengado = $("#balance").val();
    var balance = parseFloat(TotalDevengado.replace(/,/g, ""));
    var pagado = $("#pagado").val();
    var apagar = $("#aPagar").val();
    var resultados = balance - apagar;
    if (resultados < 0) {
        $("#resultados").text("El valor a pagar es mayor al pendiente");
        $('.modal-footer #btActualizar').hide();
        $("#resultados").show();

    } else {
        $("#resultados").hide();
        $('.modal-footer #btActualizar').show();

    }

    let response = formatMoney(resultados, decimalCount = 2, decimal = ".", thousands = ",");
    $("#pendiente").val(response);
});
console.log(5)
$(document).on('click', '#MdAceptar', function myfunction() {

    var cells = $('#myDatatable').DataTable().cells().nodes();

    $(cells).find('input:checked').each(function () {
        var parent = $(this).parent().parent().parent();
        var validacion = ValidarProducto($(parent).find('td:nth-child(1)').text());
        if (validacion) {
            InsertOrder(0, 0,
                $(parent).find('td:nth-child(1)').text(),
                $(parent).find('td:nth-child(2)').text(),
                $(parent).find('td:nth-child(3)').text(),
                $(parent).find('td:nth-child(4)').text(),
                $(parent).find('td:nth-child(7)').text(),
                $(parent).find('td:nth-child(8)').text(),
                0);
        }else {

        }
    })
    $('#Generic-modal').modal('toggle');
});

function InsertOrder(Id, VendorPaymentId, PurchaseOrderId, SuppliersId, PurchaseOrderDocument, SupplierName, Balance, Total, Payment) {

    var totals = (parseFloat(Total)) + (parseFloat(Balance));
    Payment = (parseFloat(Total)) - (parseFloat(Balance));

    $VendorPaymentTable.row.add([
        Id,
        VendorPaymentId,
        PurchaseOrderId,
        SuppliersId,
        PurchaseOrderDocument,
        SupplierName,
        "",
        Balance,
        Payment,
        0,
        0,
        '<ul class=\"icons-list\"><li class=\"dropdown\"><a href = \"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"icon-menu9\"></i></a><ul class=\"dropdown-menu dropdown-menu-right\"><li><a type=\"button\" class=\"btn btn-default btn-icon modificarProducto\"><i class=\"icon-pencil6\"></i> Editar</a><a type=\"button\" class=\"btn btn-default btn-icon deleteRow\" ><i class=\"icon-trash\"></i> Eliminar</a></ul></li></ul>'
    ]).draw(false);
    CalcularMontos($VendorPaymentTable.data().toArray());
}

function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
    try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
        console.log(e)
    }
};

function ConvertArraryToObject(array) {
    //Verifica bien como estan escrito estas propiedades y las que estan en el tblPushOrderRow que sean iguales
    var PriceMemory = {
        Memory: [],
        add: function (Id, VendorPaymentId, PurchaseOrderId, SuppliersId, PurchaseOrderDocument, SupplierName, Concepto, Balance, Pagado, APagar, Pendiente) {
            var OrderRow = {
                Id: Id,
                VendorPaymentId: VendorPaymentId,
                PurchaseOrderId: PurchaseOrderId,
                SuppliersId: SuppliersId,
                PurchaseOrderDocument: PurchaseOrderDocument,
                SupplierName: SupplierName,
                Concept: Concepto,
                Owed: Balance,
                PaidOut: Pagado,//es el mismo charlatan?
                Payment: APagar,
                Pending: Pendiente,
            };
            PriceMemory.Memory.push(OrderRow);
        }
    };
    for (var i = 0; i < array.length; i++) {
        PriceMemory.add(
            array[i][0],
            array[i][1],
            array[i][2],
            array[i][3],
            array[i][4],
            array[i][5],
            array[i][6],
            array[i][7],
            array[i][8],
            array[i][9],
            array[i][10],
        );
    }
    return PriceMemory.Memory;
}

function ValidarProducto(productoId) {

    var $VendorPaymentTable = $('#EditVendorPayment').DataTable();

    if (!$VendorPaymentTable.data().any()) {
        return true;
    }

    var cells = $VendorPaymentTable.cells().nodes();
    var cellsOfRow = cells.data();
    var compareWith = "";

    for (var i = 0; i < cellsOfRow.length; i++) {
        compareWith = cellsOfRow[i][2];
        // Buscamos el texto en el contenido de la celda
        if (productoId.length == 0 || (compareWith == productoId)) {
            return false;
        }
    }
    return true;
}