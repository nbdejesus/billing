﻿$(document).ready(function myfunction() {
    console.log(5);
    $(document).on('click', '#BtnCreate', function myfunction() {

        var $option = $(this);
        var form = ProcessManager.LoadForm(ProcessManager.getUrl('VendorPayment/_OrdenCompraList'), null);
        form.then((data) => {
            ProcessManager.ShowModal("Facturas de compras", 'icon-plus2', data, true, true,2);

            CallDinamicTable('#OrdenList .datatable', 10);
            $('.modal-footer #btnCancel').show();
            $('.modal-footer #MdAceptar').show();
            $('#agregar').hide();
            $('#cerrarM').hide();
            $('.modal-footer #Cancelar').hide();
            $('.modal-footer #btActualizar').hide();
        });
    });      

    $(document).on('click', '.status', function myfunction() {
        $option = $(this);

        var question = ProcessManager.Question('Alerta', 'Está seguro de que desea ' + (($option.attr('data-status') == 'True') ? 'activar' : 'cambiar el') + ' estado del pago?.');

        question.then((answer) => {
            if (answer) {

                $.ajax({
                    url: ProcessManager.getUrl('VendorPayment/StatusPayment'),
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        DocumentStatusID: $option.attr('data-estado'),
                        Id: $option.attr('data-id')
                    },
                    beforeSend: function myfunction() {
                        ProcessManager.Loading(true, 'body');
                    },
                    success: function (data) {
                        if (data.status == true) {
                            ProcessManager.Success('Notificación', data.message);
                            ProcessManager.Redirect(data.redirect);
                        }
                        else {
                            ProcessManager.Warning('Alerta', data.message);
                            ProcessManager.Loading(false, 'body');
                        }
                    },
                    error: function myfunction(xhr, status) {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.validateStatusCode(xhr.status);
                    }
                });
            }
        });
    });

    //Esta parte elimina una fila de la tabla //Si te fijas usa la clase que se declaro en el metodo de arriba  //Este metodo lo borra de memoria y del html
    $(document).on('click', '.deleteRow', function myfunction() {
        $PurchaseTable.row($(this).parents('tr'))
            .remove()
            .draw();

        CalcularMontos($PurchaseTable.data().toArray());
    });

    ///////////////envia los datos hacia el controlador para crear el Purchase/////////////
    $("#Enviar").click(function () {
        var validar = ValidarRow();

        if (validar.okay) {
            var documentStatus = parseInt($(this).attr('data-document'));
            CrearPurchase(documentStatus);
        } else {
            ProcessManager.Warning('Notificación', validar.mensaje);
        } 
                         
    });

    $("#Procesar").click(function () {
        var validar = ValidarRow();

        if (validar.okay) {
            var documentStatus = $(this).attr('data-document');
            CrearPurchase(documentStatus);
        } else {
            ProcessManager.Warning('Notificación', validar.mensaje);
        } 

    });

    //CallPriceTable();
    //VendorPaymentTable();
    // funcionalidad de los checkbox
    $(document).on('click', '#allChecked', function myfunction(event) {
        if (this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function () {
                this.checked = true;
            });
        }
        else {
            $(':checkbox').each(function () {
                this.checked = false;
            });
        }
    });

    
    VendorPaymentTable();
});

//Este metodo agrega o envia la informacion al controller
function CrearPurchase(documentStatus) {
    var $form = $('form');

    if ($form.valid()) {

        var model = $form.serialize() + '&' + $.param({ "VendorPayment.DocumentEstatusId": documentStatus }) + '&' + $.param({ "VendorPayment.TblVendorPaymentRow": ConvertArraryToObject($PurchaseTable.data().toArray()) });

        ///Pondras el nombre del create  y lo reemplazas en la url entonces
        $.ajax({
            url: (($('#Edit-PurchaseOrder').length > 0) ? ProcessManager.getUrl('VendorPayment/UpdateVendorPayment') : ProcessManager.getUrl('VendorPayment/CreateVendorPayment')),
            type: 'POST',
            data: model,
            cache: false,
            beforeSend: function myfunction() {
                ProcessManager.Loading(true, 'body');
            },
            success: function myfunction(data) {
                if (data.status) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.Success('Notificación', data.message);
                    ProcessManager.Redirect(data.redirect);
                }
                else {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.Warning('Alerta', data.message);
                }
            },
            error: function myfunction(xhr, status) {
                ProcessManager.Loading(false, 'body');
                ProcessManager.validateStatusCode(xhr.status);
            }
        });
    }
    else {
        ProcessManager.Warning('Alerta', 'Faltan campos por llenar.');
    }
}
//////Valida los producto agragados en la tabla///////////////////
function ValidarProducto(productoId) {

    var $PurchaseTables = $('#PurchaseTable').DataTable();

    if (!$PurchaseTables.data().any()) {
        return true;
    }
    var cells = $PurchaseTables.cells().nodes();
    var cellsOfRow = cells.data();
    var compareWith = "";

    for (var i = 0; i < cellsOfRow.length; i++) {
        compareWith = cellsOfRow[i][0];
        // Buscamos el texto en el contenido de la celda
        if (productoId.length == 0 || (compareWith == productoId)) {
            return false;
        }           
    }
    return true;
}

function ValidarRow() {
    var objecto = {
        mensaje : "",
        okay : false
    }
    var $PurchaseTables = $('#PurchaseTable').DataTable();

    //if (!$PurchaseTables.data().any()) {
    //    return true;
    //}

    var cells = $PurchaseTables.cells().nodes();
    var cellsOfRow = cells.data();
    var compareWith = "";
    if (0 < cellsOfRow.length) {

        for (var i = 0; i < cellsOfRow.length; i++) {
            compareWith = cellsOfRow[i][0];
            // Buscamos el texto en el contenido de la celda
           
                if (parseFloat(cellsOfRow[i][7]) <= 0) {
                    objecto.mensaje = "Debe ingresar el monto de la orden";
                    objecto.okay = false;
                    return objecto;
                }            
        }
        objecto.okay = true;
        return objecto;
    }
    else {
        objecto.mensaje = "No hay pagos seleccionados...";
        objecto.okay = false;
        return objecto;
    }  
}

$(document).on('change', '#aPagar', function () {
    console.log(5)
    
    var TotalDevengado = $("#balance").val();
    var balance = parseFloat(TotalDevengado.replace(/,/g, ""));
    var pagado = $("#pagado").val();
    var apagar = $("#aPagar").val();
    var resultados = balance - apagar;
    if (resultados < 0) {
        $("#resultados").text("El valor a pagar es mayor al pendiente");
        $('.modal-footer #btActualizar').hide();
        $("#resultados").show();

    } else {
        $("#resultados").hide();
        $('.modal-footer #btActualizar').show();

    }
   
    let response = formatMoney(resultados, decimalCount = 2, decimal = ".", thousands = ",");
    $("#pendiente").val(response);

});

        //este te configura la tabla cuando tu estas creando la orden de compra y no te carga ninguna informacion del a BD
async function VendorPaymentTable() {
    //Estas con las columnas que apareceran en la tabla
    var columnsTitle = [
        { title: "Id" },
        { title: "SuppliersId" },
        { title: "No.Orden" },
        { title: "Proveedor" },
        { title: "Concepto" },//0
        { title: "Balance" },//0
        { title: "Pagado" },//1
        { title: "A Pagar" },//2    
        { title: "Pendiente" },//2 
        { title: "Opciones" },

    ];

    //Con este tu le dices al datable cuales columnas ocultar
    var columnVisible = [
        //esa columna la tenias      
        { "visible": false, "targets": 0 },
        { "visible": false, "targets": 1 }

    ];

    //esto valida si estas en el formulario de edit o crear
    $PurchaseTable = $('#PurchaseTable').DataTable({
        bLengthChange: false,
        columnDefs: columnVisible,
        columns: columnsTitle,
        bSort: false,
        language: {
            processing: "Procesando",
            search: "Buscar:",
            lengthMenu: "Ver _MENU_ Filas",
            info: "_START_ - _END_ de _TOTAL_ elementos",
            infoEmpty: "0 - 0 de 0 elementos",
            infoFiltered: "(Filtro de _MAX_ entradas en total)",
            loadingRecords: "Cargando datos.",
            zeroRecords: "No se encontraron datos",
            emptyTable: "No hay datos disponibles",
            paginate: {
                first: "Primero",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultimo"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }

    });

    CalcularMontos($PurchaseTable.data().toArray());
}

///Esta es la funcion que llama a la tabla, tanto en el create como en el edit

$(document).on('click', '#MdAceptar', function myfunction() {
    debugger
    var cells = $('#myDatatable').DataTable().cells().nodes();

    $(cells).find('input:checked').each(function () {

        var parent = $(this).parent().parent().parent();
        var validacion = ValidarProducto($(parent).find('td:nth-child(1)').text());
        if (validacion) {
            InsertOrder($(parent).find('td:nth-child(1)').text(),
                $(parent).find('td:nth-child(2)').text(),
                $(parent).find('td:nth-child(3)').text(),
                $(parent).find('td:nth-child(4)').text(),
                $(parent).find('td:nth-child(7)').text(),
                $(parent).find('td:nth-child(8)').text(),
                $(parent).find('td:nth-child(9)').text(),
                0);
        }
        else {

        }
    })

    $('#Generic-modal').modal('toggle');
});

function InsertOrder(Id, SuppliersId, PurchaseOrderDocument, SupplierName, Balance, Total, Payment) {

    var totals = (parseFloat(Total)) + (parseFloat(Balance));
    Payment = (parseFloat(Total)) - (parseFloat(Balance));

    $PurchaseTable.row.add([
        Id,
        SuppliersId,
        PurchaseOrderDocument,
        SupplierName,
        "",
        Balance,
        Payment,
        0,
        0,
        '<ul class=\"icons-list\"><li class=\"dropdown\"><a href = \"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"icon-menu9\"></i></a><ul class=\"dropdown-menu dropdown-menu-right\"><li><a type=\"button\" class=\"btn btn-default btn-icon modificarProducto\" data-id=\"' + Id + '\" ><i class=\"icon-pencil6\"></i> Editar</a><a type=\"button\" class=\"btn btn-default btn-icon deleteRow\" ><i class=\"icon-trash\"></i> Eliminar</a></ul></li></ul>'
    ]).draw(false);

    CalcularMontos($PurchaseTable.data().toArray());
}

function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
    try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
        console.log(e)
    }
};

$(document).on('click', '.modificarProducto', function myfunction() {

    var form = ProcessManager.LoadForm(ProcessManager.getUrl('VendorPayment/EditVendorPaymentRow'), { Id: $(this).attr('data-id') }, '.panel');
    form.then((data) => {
        ProcessManager.ShowModal("Editar Orden ", 'icon-plus2', data, true, true);

        CallDinamicTable('#tableList', 10, false);

        var parent = $(this).parent().parent().parent().parent().parent().parent();

        $('.modal-footer #btnCancel').hide();
        $('.modal-footer #MdAceptar').hide();


        $(".modal-footer").append('<div class=\"panel-footer\">' +
            '<button id=\"Cancelar\" type=\"button\" class=\"btn btn-link\" data-dismiss=\"modal\">Cerrar</button>' +
            '<button id=\"btActualizar\" type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Aceptar</button></div>');

        valorModal(parent);
    });
    $('.modal-footer .panel-footer').remove();

});

function valorModal(parent) {

    var cells = $PurchaseTable.cells().nodes();
    var array = cells.data();
    var index = parent.index();

    Order.translateOrder(
        array[index][0],
        array[index][1],
        array[index][2],
        array[index][3],
        array[index][4],
        array[index][5],
        array[index][6],
        array[index][7],
        array[index][8],
        array[index][9],
        index);
   
    
    $("#PurchaseOrderId").val(Order.PurchaseOrderId);
    $("#SuppliersId").val(Order.SuppliersId);
    $("#NoDocument").val(Order.noOrden);
    $("#provedor").val(Order.supplierName);
    $("#concepto").val(Order.concepto);
    $("#balance").val(Order.balance);
    $("#pagado").val(Order.pagado);
    $("#aPagar").val(Order.apagar);
    $("#pendiente").val(Order.pendiente);
    $("#opcion").val(Order.opciones);
    $("#index").val(Order.rowIndex);
}

$(document).on('click', '#btActualizar', function myfunction() {

    Order.updateOrder(
        $("#PurchaseOrderId").val(),
        $("#SuppliersId").val(),
        $("#NoDocument").val(),
        $("#provedor").val(),
        $("#concepto").val(),
        $("#balance").val(),
        $("#pagado").val(),
        $("#aPagar").val(),
        $("#pendiente").val(),
        $("#opcion").val(),
        $("#index").val());
    Order.updateTable();
    CalcularMontos($PurchaseTable.data().toArray());
});

var Order = {
    PurchaseOrderId: 0,
    SuppliersId: 0,
    noOrden: 0,
    supplierName: '',
    concepto: "",
    balance: 0,
    pagado: 0,
    apagar: 0,
    pendiente: 0,
    opciones: "",
    rowIndex: 0,

    translateOrder: function (PurchaseOrderId, SuppliersId, NoOrden, SupplierName, Concepto, Balance, Pagado, APagar, Pendiente, Opciones, RowIndex) {
        this.PurchaseOrderId = PurchaseOrderId
        this.SuppliersId = SuppliersId
        this.noOrden = NoOrden
        this.concepto = Concepto
        this.supplierName = SupplierName
        this.balance = Balance
        this.pagado = Pagado
        this.apagar = APagar
        this.pendiente = Pendiente
        this.opciones = Opciones
        this.rowIndex = RowIndex


    },
    updateOrder: function (PurchaseOrderId, SuppliersId, NoOrden, SupplierName, Concepto, Balance, Pagado, APagar, Pendiente, Opciones, RowIndex) {
        this.PurchaseOrderId = PurchaseOrderId,
        this.SuppliersId = SuppliersId,
        this.noOrden = NoOrden
        this.concepto = Concepto
        this.supplierName = SupplierName
        this.balance = Balance
        this.pagado = Pagado
        this.apagar = formatMoney(APagar, decimalCount = 2, decimal = ".", thousands = ","); 
        this.pendiente = Pendiente
        this.opciones = Opciones
        this.rowIndex = RowIndex
    },
    updateTable: function () {
        let array = [this.PurchaseOrderId, this.SuppliersId, this.noOrden, this.supplierName, this.concepto, this.balance, this.pagado, this.apagar, this.pendiente, this.opciones];
        $('#PurchaseTable').DataTable().row(this.rowIndex).data(array).draw();

    }
};

function CalcularMontos(tabla) {
    console.log(5);
    valor = 0;
    for (var i = 0; i < tabla.length; i++) {

        var response1 = parseFloat(tabla[i][7]);

        if (response1 > 0) {
            var response2 = parseFloat(tabla[i][7].replace(/,/g, ""));
            valor += response2;
        }
    }

    let response = formatMoney(valor, decimalCount = 2, decimal = ".", thousands = ",");
    $("#mensaje").text(response);
    $("#mensajesubtotal").val(valor.toFixed(2));
}


function ConvertArraryToObject(array) {
    //Verifica bien como estan escrito estas propiedades y las que estan en el tblPushOrderRow que sean iguales
    var PriceMemory = {
        Memory: [],
        add: function (PurchaseOrderId, SuppliersId, NoOrden, SupplierName, Concepto, Balance, Pagado, APagar, Pendiente) {
            var OrderRow = {
                PurchaseOrderId: PurchaseOrderId,
                SuppliersId: SuppliersId,
                PurchaseOrderDocument: NoOrden,
                SupplierName: SupplierName,
                Concept: Concepto,
                Owed: Balance,
                PaidOut: Pagado,
                Payment: APagar,
                Pending: Pendiente,
            };
            PriceMemory.Memory.push(OrderRow);
        }
    };
    for (var i = 0; i < array.length; i++) {
        PriceMemory.add(
            array[i][0],
            array[i][1],
            array[i][2],
            array[i][3],
            array[i][4],
            array[i][5],
            array[i][6],
            array[i][7],
            array[i][8],
        );
    }
    return PriceMemory.Memory;
}

