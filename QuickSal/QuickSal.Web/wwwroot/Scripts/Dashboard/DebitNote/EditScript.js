﻿// #region Inicio

$(document).ready(function myfunction() {

    CallPriceTable();

    $(document).on('click', '#allChecked', function myfunction(event) {
        if (this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function () {
                this.checked = true;
            });
        }
        else {
            $(':checkbox').each(function () {
                this.checked = false;
            });
        }
    });
});


var estado = $(".estado").text();
///Esta es la funcion que llama a la tabla, tanto en el create como en el edit
async function CallPriceTable() {
    //Estas con las columnas que apareceran en la tabla
    var columnsTitle = [

        { title: "Id" },
        { title: "Articulos" },//1
        { title: "Almacen" },//2
        { title: "Cantidad" },//3
        { title: "Descuento %" },//4 
        { title: "Impuesto %" }, //5        
        { title: "ArticulosId" },//6
        { title: "AlmacenId" },//7
        { title: "MedidadId" },//8
        { title: "Medida" },//9
        { title: "Tipo Imp." },//10
        { title: "Costo" },//11
        { title: "Monto Imp." },//12
        { title: "Monto Descuento" },//13
        { title: "Total" },//14
        { title: "Acción" }
    ];

    var columnVisible = [];

    //////Aqui validamos si esta aprobada y quitamos la opcion de editar y eliminar
    if (estado === "Aprobada" || estado === "Anulada") {
        //Con este tu le dices al datable cuales columnas ocultar
        columnVisible = [
            //esa columna la tenias 
            { "visible": false, "targets": 0 },
            { "visible": false, "targets": 6 },
            { "visible": false, "targets": 7 },
            { "visible": false, "targets": 8 },
            { "visible": false, "targets": 15 },
            { "visible": false, "targets": 10 }
        ];

    }
    else {
        //Con este tu le dices al datable cuales columnas ocultar
        columnVisible = [
            //esa columna la tenias 
            { "visible": false, "targets": 0 },
            { "visible": false, "targets": 6 },
            { "visible": false, "targets": 7 },
            { "visible": false, "targets": 8 },
            { "visible": false, "targets": 10 }
        ];
    }


    //esto valida si estas en el formulario de edit o crear
    if ($('#Edit-DebitNote').length > 0) {

        const data = await ProcessManager.LoadForm(ProcessManager.getUrl('DebitNote/GetDebitNoteRow'), { id: $('[name="DebitNoteId"]').val() });
        //este codigo te carga solamente el detalle de la orden de compra
        //form.then((data) => {
        $EditTable = $('#DebitNoteTable').DataTable({
            responsive: true,
            bLengthChange: false,
            data: data.prices,//aqui es donde se le pasa el objeto con sus datos<---
            columnDefs: columnVisible,
            columns: columnsTitle,
            language: {
                processing: "Procesando",
                search: "Buscar:",
                lengthMenu: "Ver _MENU_ Filas",
                info: "_START_ - _END_ de _TOTAL_ elementos",
                infoEmpty: "0 - 0 de 0 elementos",
                infoFiltered: "(Filtro de _MAX_ entradas en total)",
                loadingRecords: "Cargando datos.",
                zeroRecords: "No se encontraron datos",
                emptyTable: "No hay datos disponibles",
                paginate: {
                    first: "Primero",
                    previous: "Anterior",
                    next: "Siguiente",
                    last: "Ultimo"
                },
                aria: {
                    sortAscending: ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre décroissant"
                }
            }
        });
        CalcularMontos($EditTable.data().toArray());
        //});

    }
    else {
        //este te configura la tabla cuando tu estas creando la orden de compra y no te carga ninguna informacion del a BD
        $EditTable = $('#PurchaseTable').DataTable({
            bLengthChange: false,
            data: null,//si te fijas aqui la data esta null<---
            columns: columnsTitle,
            columnDefs: columnVisible,
            language: {
                processing: "Procesando",
                search: "Buscar:",
                lengthMenu: "Ver _MENU_ Filas",
                info: "_START_ - _END_ de _TOTAL_ elementos",
                infoEmpty: "0 - 0 de 0 elementos",
                infoFiltered: "(Filtro de _MAX_ entradas en total)",
                loadingRecords: "Cargando datos.",
                zeroRecords: "No se encontraron datos",
                emptyTable: "No hay datos disponibles",
                paginate: {
                    first: "Primero",
                    previous: "Anterior",
                    next: "Siguiente",
                    last: "Ultimo"
                },
                aria: {
                    sortAscending: ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre décroissant"
                }
            }
        });
    }
}

// #endregion

//  #region Agregar datos a la tabla

$(document).on('click', '.callModal', function myfunction() {
    $(".modal-dialog").addClass("modal-lg");
    var form = ProcessManager.LoadForm('../ModalPurchOrder');

    form.then((data) => {
        ProcessManager.ShowModal("Listado de la orden", 'icon-plus2', data, false, false, 3);

        $('.modal-footer #btnCancel').hide();
        $('.modal-footer #MdAceptar').hide();

        $(".modal-footer").append('<div class=\"panel-footer\">' +
            '<button id=\"Cancelar\" type=\"button\" class=\"btn btn-link\" data-dismiss=\"modal\">Cerrar</button>' +
            '<button id=\"Agregar\" type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Aceptar</button></div>');

        AddPriceTable();
    });
    $('.modal-footer .panel-footer').remove();
});

///Esta es la funcion que llama a la tabla en el modal///////
function AddPriceTable() {
    //Estas con las columnas que apareceran en la tabla
    var columnsTitle = [

        { title: "Id" },
        { title: "Articulos" },//1
        { title: "Almacen" },//2
        { title: "Cantidad" },//3
        { title: "costo" },//4    
        { title: "Descuento %" },//5
        { title: "Tipo Imp." },//6
        { title: "Sub Total" },//7      
        { title: "ArticulosId" },//8
        { title: "AlmacenId" },//9
        { title: "Impuesto %" },//10
        { title: "Monto Imp." },//11
        { title: "MeasureId" },//12
        { title: "Measure" },//13
        { title: "Acción" },//14
        { title: "Seleccione" }//15

    ];

    //Con este tu le dices al datable cuales columnas ocultar
    var columnVisible = [
        //esa columna la tenias 
        { "visible": false, "targets": 0 },
        { "visible": false, "targets": 6 },
        { "visible": false, "targets": 8 },
        { "visible": false, "targets": 9 },
        { "visible": false, "targets": 10 },
        { "visible": false, "targets": 12 },
        { "visible": false, "targets": 13 },
        { "visible": false, "targets": 14 }

    ];

    //checkbox
    $(".styled, .multiselect-container input").uniform({
        radioClass: 'choice'
    });

    //Agregamos el evento render en memoria
    $('.ItemList').on('draw.dt', function () {
        //Volviendo a renderizar el diseno
        $(".styled, .multiselect-container input").uniform({
            radioClass: 'choice'
        });

        // $('.checkbox').prop('checked', false).uniform('refresh');
    });

    //esto valida si estas en el formulario de edit o crear 

    var form = ProcessManager.LoadForm(ProcessManager.getUrl('DebitNote/GetPurchaseOrder'), { PurchaseId: $('[name="PurchaseOrderId"]').val() });
    //este codigo te carga solamente el detalle de la orden de compra
    form.then((data) => {
        $ModalPurchaseTable = $('#ModalPurchaseTable').DataTable({
            bLengthChange: false,
            data: data.prices,//aqui es donde se le pasa el objeto con sus datos<---
            columnDefs: columnVisible,
            columns: columnsTitle,
            language: {
                processing: "Procesando",
                search: "Buscar:",
                lengthMenu: "Ver _MENU_ Filas",
                info: "_START_ - _END_ de _TOTAL_ elementos",
                infoEmpty: "0 - 0 de 0 elementos",
                infoFiltered: "(Filtro de _MAX_ entradas en total)",
                loadingRecords: "Cargando datos.",
                zeroRecords: "No se encontraron datos",
                emptyTable: "No hay datos disponibles",
                paginate: {
                    first: "Primero",
                    previous: "Anterior",
                    next: "Siguiente",
                    last: "Ultimo"
                },
                aria: {
                    sortAscending: ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre décroissant"
                }
            }
        });
    });
}

/////////////Obtiene los datos celecionados del modal de orden de compra//////////
$(document).on('click', '#Agregar', function myfunction() {

    var cells = $ModalPurchaseTable.cells().nodes();

    $(cells).find('input:checked').each(function () {
        var parent = $(this).closest('tr');
        var array = cells.data();
        var index = parent.index();
        var validacion = ValidarProducto(array[index][8]);

        if (validacion) {

            InsertOrder(
                0,
                array[index][1],
                array[index][2],
                array[index][3],
                array[index][4],
                array[index][5],
                array[index][6],
                array[index][7],
                array[index][8],
                array[index][9],
                array[index][10],
                array[index][11],
                array[index][12],
                array[index][13],
                array[index][14],
            );
            CalcularMontos($EditTable.data().toArray());
        }
    });
});

//////////////////Agrega las ordenes de compra a la tabla parcial de nota de debito/////
function InsertOrder(Id, ProductName, Warehouse, Quantity, Cost, Discount, TaxAbbreviation, SubTotal, ProductId, WarehouseId, Tax, AmountTax, MeasureId, Measure, Accion) {
    var montoDescuento = SubTotal * (Discount / 100);
    $('#DebitNoteTable').DataTable().row.add([
        Id,
        ProductName,
        Warehouse,
        formatMoney(Quantity),
        formatMoney(Discount),
        formatMoney(Tax),
        ProductId,
        WarehouseId,
        MeasureId,
        Measure,
        TaxAbbreviation,
        formatMoney(Cost),
        formatMoney(AmountTax),
        formatMoney(montoDescuento),
        SubTotal,
        Accion
    ]).draw(false);
    CalcularMontos($EditTable.data().toArray());
}

//////////////Metodo para dar formato a los numeros//////////////
function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
    try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
        console.log(e);
    }
}

//////Valida los producto agragados en la tabla///////////////////

function ValidarProducto(productoId) {

    var $PurchaseTables = $('#DebitNoteTable').DataTable();

    if (!$PurchaseTables.data().any()) {
        return true;
    }

    var cells = $PurchaseTables.cells().nodes();
    var cellsOfRow = cells.data();
    var compareWith = "";

    for (var i = 0; i < cellsOfRow.length; i++) {
        compareWith = cellsOfRow[i][6];
        // Buscamos el texto en el contenido de la celda
        if (productoId.length === 0 || (compareWith === productoId)) {
            return false;
        }

    }
    return true;
}

// #endregion

// #region Editar la Tabla

//Esta funcion asigna cada fila de la tabla a una fila de la tabla detalle
function ConvertArraryToObject(array) {
    //Verifica bien como estan escrito estas propiedades y las que estan en el tblPushOrderRow que sean iguales
    var PriceMemory = {
        Memory: [],
        add: function (ProductId, AlmacenId, MeasureId, TaxAbbreviation, Quantity, Cost, Tax, AmountTax, Discount, AmountDiscount, Total, Id) {
            var OrderRow = {
                Id: Id,
                ProductId: ProductId,
                MeasureId: MeasureId,
                WarehouseId: AlmacenId,//es el mismo charlatan?
                TaxAbbreviation: TaxAbbreviation,
                Quantity: Quantity,
                Cost: Cost,
                Tax: Tax,
                AmountTax: AmountTax,
                Discount: Discount,
                AmountDiscount: AmountDiscount,
                Total: Total
            };
            PriceMemory.Memory.push(OrderRow);
        }
    };

    for (var i = 0; i < array.length; i++) {
        PriceMemory.add(
            array[i][9],
            array[i][10],
            array[i][13],
            array[i][7],
            array[i][4],
            array[i][5],
            array[i][11],
            array[i][12],
            array[i][6],
            array[i][6],
            array[i][8],
            array[i][1]
        );
    }

    return PriceMemory.Memory;
}

function CalcularMontos(tabla) {

    valor = 0;
    tax = parseInt(0);
    desc = parseInt(0);
    impuestos = parseInt(0);
    imp = parseInt(0);

    for (var i = 0; i < tabla.length; i++) {
        //Multipliamos el subTotal con su impuesto correspondiente
        imp = parseFloat(tabla[i][8]);//<<la columna que era la 7 ya no es la columna 7 ahora es la columna 8 porque al yo agregar una nueva columna al inicio los Id se desplazan

        tax += (parseFloat(tabla[i][14]) * (parseFloat(tabla[i][5]) / 100));
        valor += parseFloat(tabla[i][14]);
        desc += (parseFloat(tabla[i][14]) / 100) * parseFloat(tabla[i][4]);
    }

    $("#mensaje").text(formatMoney(valor));
    $("#Subtotal").val(formatMoney(valor));
    $("#total").val(formatMoney((valor + tax) - desc));
    $("#Mensajetax").text(formatMoney(tax));
    $("#MensajeTotal").text(formatMoney((valor + tax) - desc));
    $("#descuentoTotal").text(formatMoney(desc.toFixed(2)));

}

///////Llama el modal de editar el producto////
$(document).on('click', '.modificarProducto', function myfunction() {

    var tr = $(this).closest('.child');
    //en caso de que la fila sobrepase el ancho de la pantalla
    if (tr.length === 0) {
        tr = $(this).closest('tr').index();
    }
    else {
        //tr = $(this).closest('.odd.parent').parent();
        tr = $(this).closest('tr').children().parent().index();
        tr = tr - 1;
    }
    var form = ProcessManager.LoadForm(ProcessManager.getUrl('DebitNote/EditDebitNoteRow'), { PurchaseOrderId: $('[name="PurchaseOrderId"]').val(), ProductId: $(this).attr('data-product') }, '.panel');
    //$('.panel-footer').remove();
    form.then((data) => {
        ProcessManager.ShowModal(" Editar Detalle Nota de Debito", 'icon-plus2', data, true, true);
        valorModal(tr);
    });
    $('.modal-footer #btnCancel').show();
    $('.modal-footer #MdAceptar').show();
    $('.modal-footer .panel-footer').remove();
});

function valorModal(parent) {
    var cells = $EditTable.cells().nodes();
    var array = cells.data();
    var index = parseInt(parent);
    Order.translateOrder(
        array[index][0],
        array[index][1],
        array[index][2],
        array[index][3],
        array[index][4],
        array[index][5],
        array[index][6],
        array[index][7],
        array[index][8],
        array[index][9],
        array[index][10],
        array[index][11],
        array[index][12],
        array[index][13],
        array[index][14],
        array[index][15],        
       index);


    $("#id").val(Order.id);
    $("#AmountDiscount").val(Order.amountDiscount);
    $("#AmountTax").val(Order.amountTax);
    $("#Cost").val(Order.cost);
    $("#Descuento").val(Order.discount);
    $("#MeasureId").val(Order.measureId);
    $("#Measure").val(Order.measure);
    $("#ProductId").val(Order.poroductId);
    $("#productName").val(Order.productName);
    $("#WarehouseId").val(Order.warehouseId);
    $("#Warehouse").val(Order.warehouse);
    $("#TaxAbbreviation").val(Order.taxAbbreviation);
    $("#Tax").val(Order.tax);
    $("#Cantidad").val(Order.quantity);
    $("#SubTotal").val(Order.subTotal);
    $("#Accion").val(Order.accion);
    $("#index").val(Order.index);
}

var Order = {
    id: 0,
    productName: "",
    warehouse: "",
    quantity: 0,
    cost: 0,
    discount: 0,
    taxAbbreviation: "",
    subTotal: 0,
    poroductId: 0,
    warehouseId: 0,
    tax: 0,
    amountTax: 0,
    measureId: 0,
    Measure: "",
    accion: "",
    index: 0,
    amountDiscount: 0,


    translateOrder: function (Id, ProductName, Warehouse, Quantity, Discount, Tax, ProductId, WarehouseId, MeasureId, Measure, TaxAbbreviation, Cost, AmountTax, AmountDiscount, SubTotal, Accion, Index) {
        this.id = Id,
            this.productName = ProductName,
            this.warehouse = Warehouse,
            this.quantity = Quantity,
            this.cost = Cost,
            this.discount = Discount,
            this.taxAbbreviation = TaxAbbreviation,
            this.subTotal = SubTotal,
            this.poroductId = ProductId,
            this.warehouseId = WarehouseId,
            this.tax = Tax,
            this.amountTax = AmountTax,
            this.measureId = MeasureId,
            this.measure = Measure,
            this.amountDiscount = AmountDiscount,
            this.index = Index,
            this.accion = Accion;
     

    },
    updateOrder: function (Id, ProductName, Warehouse, Quantity, Discount, Tax, ProductId, WarehouseId, MeasureId, Measure, TaxAbbreviation, Cost, AmountTax, AmountDiscount, SubTotal, Accion, Index) {
        this.id = Id,
            this.productName = ProductName,
            this.warehouse = Warehouse,
            this.quantity = Quantity,
            this.cost = Cost,
            this.discount = Discount,
            this.taxAbbreviation = TaxAbbreviation,
            this.subTotal = SubTotal,
            this.poroductId = ProductId,
            this.warehouseId = WarehouseId,
            this.tax = Tax,
            this.amountTax = AmountTax,
            this.measureId = MeasureId,
            this.measure = Measure,
            this.amountDiscount = AmountDiscount,
            this.accion = Accion,
            this.index = Index;
    },
    updateTable: function () {
        let array = [this.id, this.productName, this.warehouse, this.quantity, this.discount, this.tax, this.poroductId,
        this.warehouseId, this.measureId, this.measure, this.taxAbbreviation, this.cost, this.amountTax,
        this.amountDiscount, this.subTotal, this.accion];
        $EditTable.row(this.index).data(array).draw();

    }
};

///////////////Metodo para guardar las modificaciones de la tabla//////
$(document).on('click', '#MdAceptar', function myfunction() {
    var validacion = validarCantidad();
    if (validacion) {


        var total = ($("#Cost").val()) * ($("#Cantidad").val());
        var montoImpuesto = ($("#Tax").val() / 100) * total;
        var montodescuento = total * ($("#Descuento").val() / 100);
        Order.updateOrder
            (
                $("#id").val()
                , $("#productName").val()
                , $("#Warehouse").val()
                , $("#Cantidad").val()
                , $("#Descuento").val()
                , $("#Tax").val()
                , $("#ProductId").val()
                , $("#WarehouseId").val()
                , $("#MeasureId").val()
                , $("#Measure").val()
                , $("#TaxAbbreviation").val()
                , parseFloat($("#Cost").val())
                , parseFloat(montoImpuesto).toFixed(2)
                , montodescuento.toFixed(2)
                , parseFloat(total)
                , $("#Accion").val()
                , $("#index").val()
            );
        Order.updateTable();
        CalcularMontos($EditTable.data().toArray());
        $("#CerrarBtn").click();
    }
    else {
        ProcessManager.Warning('Alerta', "La cantidad no puede ser mayor a lo disponible");
    }
});


//Esta parte elimina una fila de la tabla
//Si te fijas usa la clase que se declaro en el metodo de arriba
//Este metodo lo borra de memoria y del html
$(document).on('click', '.deleteRow', function myfunction() {
    var tr = $(this).closest('.child');

    //en caso de que la fila sobrepase el ancho de la pantalla
    if (tr.length === 0) {
        tr = $(this).closest('tr');
    }
    else {
        //tr = $(this).closest('.odd.parent').parent();
        tr = $(this).closest('tr');
        var trs = tr.index() - 1;
    }

    $EditTable.row(trs)
        .remove()
        .draw();

    CalcularMontos($EditTable.data().toArray());
});

function validarCantidad() {
    var disponible = $("#cantidadProducto").val();
    var cantidad = $("#Cantidad").val();
    if (parseFloat(cantidad) <= disponible && parseFloat(cantidad) > 0) {
        return true;

    }
    else
        return false;
}


// #endregion

// #region Agregar

$("#Enviar").click(function () {
    var TabletRow = validarDatosEnLaTabla();
    if (TabletRow)
        UpdateDebitNote($(this).attr('data-document'));
    else
        ProcessManager.Warning('Alerta', "Debe selecionar un producto");
});

$("#Procesar").click(function () {
    var TabletRow = validarDatosEnLaTabla();
    if (TabletRow) {
        var question = ProcessManager.Question('Alerta', 'Está seguro de que desea procesar esta Orden?.');

        question.then((answer) => {
            if (answer) {
                UpdateDebitNote($(this).attr('data-document'));
            }
        });
    }

    else
        ProcessManager.Warning('Alerta', "Debe selecionar un producto");
});

function validarDatosEnLaTabla() {
    var row = $("#DebitNoteTable").DataTable().data().row().any();
    if (row > 0)
        return true;
    return false;
}


//Este metodo agrega o envia la informacion al controller
function UpdateDebitNote(tipo) {
    var $form = $('form');

    if ($form.valid()) {

        var model = $form.serialize() + '&' + $.param({ "tipoProceso": tipo }) + '&' + $.param({ "TblDebitNoteRow": ConvertArraryToObject($EditTable.data().toArray()) });

        ///Pondras el nombre del create  y lo reemplazas en la url entonces
        $.ajax({
            url: ProcessManager.getUrl('DebitNote/UpdateDebitNote'),
            type: 'POST',
            data: model,
            cache: false,
            beforeSend: function myfunction() {
                ProcessManager.Loading(true, 'body');
            },
            success: function myfunction(data) {
                if (data.status) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.Success('Notificación', data.message);
                    ProcessManager.Redirect(data.redirect);
                }
                else {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.Warning('Alerta', data.message);
                }
            },
            error: function myfunction(xhr, status) {
                ProcessManager.Loading(false, 'body');
                ProcessManager.validateStatusCode(xhr.status);
            }
        });
    }
    else {
        ProcessManager.Warning('Alerta', 'Faltan campos por llenar.');
    }
}



// #endregion