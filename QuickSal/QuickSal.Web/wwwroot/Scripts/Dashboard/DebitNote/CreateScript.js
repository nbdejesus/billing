﻿

// #region Agregar datos a la tabla

$(document).on('click', '.callModal', function myfunction() {

    var form = ProcessManager.LoadForm(ProcessManager.getUrl('DebitNote/ModalPurchOrder'));
    form.then((data) => {
        ProcessManager.ShowModal("Articulos de la factura de compra", 'icon-plus2', data, false, false, 3);

        $('.modal-footer #btnCancel').hide();
        $('.modal-footer #MdAceptar').hide();

        $(".modal-footer").append('<div class=\"panel-footer\">' +
            '<button id=\"Cancelar\" type=\"button\" class=\"btn btn-link\" data-dismiss=\"modal\">Cerrar</button>' +
            '<button id=\"Agregar\" type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Aceptar</button></div>');
        AddPriceTable();
    });
    $('.modal-footer .panel-footer').remove();
});

//Esta parte elimina una fila de la tabla
//Si te fijas usa la clase que se declaro en el metodo de arriba
//Este metodo lo borra de memoria y del html
$(document).on('click', '.deleteRow', function myfunction() {
    var tr = $(this).closest('.child');

    //en caso de que la fila sobrepase el ancho de la pantalla
    if (tr.length === 0) {
        tr = $(this).closest('tr');
    }
    else {
        //tr = $(this).closest('.odd.parent').parent();
        tr = $(this).closest('tr');
        var trs = tr.index() - 1;
    }

    $PurchaseTable.row(trs)
        .remove()
        .draw();

    CalcularMontosNuevos($('#PurchaseTable').DataTable().data().toArray());
});

///Esta es la funcion que llama a la tabla en el modal///////
async function AddPriceTable() {
    //Estas con las columnas que apareceran en la tabla
    var columnsTitle = [

        { title: "Id" },
        { title: "Articulos" },//1
        { title: "Almacen" },//2
        { title: "Cantidad" },//3
        { title: "costo" },//4    
        { title: "Descuento %" },//5
        { title: "Impuesto" },//6
        { title: "Sub Total" },//7      
        { title: "ArticulosId" },//8
        { title: "AlmacenId" },//9
        { title: "Monto Imp." },//10
        { title: "Impuesto" },//11
        { title: "MeasureId" },//12
        { title: "Measure" },//13
        { title: "Acción" },//14
        { title: "Seleccione" }//15

    ];

    //Con este tu le dices al datable cuales columnas ocultar
    var columnVisible = [
        //esa columna la tenias 
        { "visible": false, "targets": 0 },
        { "visible": false, "targets": 6 },
        { "visible": false, "targets": 8 },
        { "visible": false, "targets": 9 },
        { "visible": false, "targets": 10 },
        { "visible": false, "targets": 12 },
        { "visible": false, "targets": 13 },
        { "visible": false, "targets": 14 }
    ];


    //checkbox
    $(".styled, .multiselect-container input").uniform({
        radioClass: 'choice'
    });

    //Agregamos el evento render en memoria
    $('.ItemList').on('draw.dt', function () {
        //Volviendo a renderizar el diseno
        $(".styled, .multiselect-container input").uniform({
            radioClass: 'choice'
        });
    
    });
    //esto valida si estas en el formulario de edit o crear
    if ($('#Edit-PurchaseOrder').length > 0) {

        const data = await ProcessManager.LoadForm(ProcessManager.getUrl('DebitNote/GetPurchaseOrder'), { PurchaseId: $('[name="PurchaseOrderId"]').val() });
        //este codigo te carga solamente el detalle de la orden de compra
        //form.then((data) => {
        $ModalPurchaseTable = $('#ModalPurchaseTable').DataTable({
            bLengthChange: false,
            data: data.prices,//aqui es donde se le pasa el objeto con sus datos<---
            columnDefs: columnVisible,
            columns: columnsTitle,
            language: {
                processing: "Procesando",
                search: "Buscar:",
                lengthMenu: "Ver _MENU_ Filas",
                info: "_START_ - _END_ de _TOTAL_ elementos",
                infoEmpty: "0 - 0 de 0 elementos",
                infoFiltered: "(Filtro de _MAX_ entradas en total)",
                loadingRecords: "Cargando datos.",
                zeroRecords: "No se encontraron datos",
                emptyTable: "No hay datos disponibles",
                paginate: {
                    first: "Primero",
                    previous: "Anterior",
                    next: "Siguiente",
                    last: "Ultimo"
                },
                aria: {
                    sortAscending: ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre décroissant"
                }
            }
        });
        //});
    }

}

////////////Calcula los valores de la tabla/////
function CalcularMontosNuevos(tabla) {

    valor = parseFloat(0);
    tax = parseInt(0);
    desc = parseInt(0);
    impuestos = parseInt(0);
    imp = parseInt(0);
    total = parseFloat(0);
    valorTotal = parseFloat(0);

    for (var i = 0; i < tabla.length; i++) {
        //Multipliamos el subTotal con su impuesto correspondiente
        imp = parseFloat(tabla[i][8]);//<<la columna que era la 7 ya no es la columna 7 ahora es la columna 8 porque al yo agregar una nueva columna al inicio los Id se desplazan

        tax += (parseFloat(tabla[i][11])) * parseFloat((tabla[i][10] / 100));
        valor += parseFloat(tabla[i][11]);
        desc += ((parseFloat(tabla[i][11]) / 100) * parseFloat(tabla[i][5]));
     
        valorTotal = ((valor + parseFloat(tax)) - desc);
    }

    $("#mensaje").text(formatMoney(valor));
    $("#Subtotal").val(formatMoney(valor));
    $("#total").val(formatMoney((valor + tax) - desc));
    $("#Mensajetax").text(formatMoney(tax.toFixed(2)));
    $("#MensajeDescuento").text(formatMoney(desc));

    $("#MensajeTotal").text(formatMoney(valorTotal));
    $("#descuentoTotal").text(formatMoney(desc.toFixed(2)));


}


/////////////        /////////////////////
$(document).on('click', '#allChecked', function myfunction(event) {
    if (this.checked) {
        // Iterate each checkbox
        $(':checkbox').each(function () {
            this.checked = true;
        });
    }
    else {
        $(':checkbox').each(function () {
            this.checked = false;
        });
    }
});

/////////////Obtiene los datos celecionados del modal de orden de compra//////////
$(document).on('click', '#Agregar', function myfunction() {

    var cells = $('#ModalPurchaseTable').DataTable().cells().nodes();

    $(cells).find('input:checked').each(function () {
        var parent = $(this).closest('tr');
        var array = cells.data();
        var index = parent.index();
        var validacion = ValidarProducto(array[index][8]);

        if (!validacion) {

            InsertOrder(
                0,
                array[index][1],
                array[index][2],
                array[index][3],
                array[index][4],
                array[index][5],
                array[index][6],
                array[index][7],
                array[index][8],
                array[index][9],
                array[index][10],
                array[index][11],
                array[index][12],
                array[index][13],
                array[index][14],
            );               
                }

    });


});

//////////////////Agrega las ordenes de compra a la tabla parcial de nota de debito/////
function InsertOrder(Id, ProductName, Warehouse, Quantity, Cost, Discount, TaxAbbreviation, SubTotal, ProductId, WarehouseId, Tax, AmountTax, MeasureId, Measure, Accion) {

    $PurchaseTable.row.add([
        0,
        ProductName,
        Warehouse,
        formatMoney(Quantity),
        formatMoney(Cost),
        parseFloat(Discount).toFixed(2),
        TaxAbbreviation,
        parseFloat(AmountTax).toFixed(2),
        ProductId,
        WarehouseId,
        formatMoney(Tax.toFixed(2)),
        parseFloat(SubTotal).toFixed(2),
        MeasureId,
        Accion

    ]).draw(false);

    CalcularMontosNuevos($PurchaseTable.data());
}

//////////////Metodo para dar formato a los numeros//////////////
function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
    try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
        console.log(e);
    }
}

//////Valida los producto agragados en la tabla///////////////////

function ValidarProducto(productoId) {

    var $PurchaseTables = $('#PurchaseTable').DataTable();

    if (!$PurchaseTables.data().any()) {
        return false;
    }

    var cells = $PurchaseTables.cells().nodes();
    var cellsOfRow = cells.data();
    var compareWith = "";

    for (var i = 0; i < cellsOfRow.length; i++) {
        compareWith = cellsOfRow[i][8];
        // Buscamos el texto en el contenido de la celda
        if (productoId.length === 0 || (compareWith === productoId)) {
            return true;
        }
    }
}

// #endregion

// #region Editar Tabla


///////Llama el modal de editar el producto////
$(document).on('click', '.modificarProducto', function myfunction() {


    var form = ProcessManager.LoadForm(ProcessManager.getUrl('DebitNote/EditDebitNoteRow'), { PurchaseOrderId: $('[name="PurchaseOrderId"]').val(), ProductId: $(this).attr('data-product') }, '.panel');
    //$('.panel-footer').remove();
    form.then((data) => {
        ProcessManager.ShowModal("List Debit Note Row", 'icon-plus2', data, true, true);

        var tr = $(this).closest('.child');

        //en caso de que la fila sobrepase el ancho de la pantalla
        if (tr.length === 0) {
            tr = $(this).closest('tr').index();
        }
        else {
            //tr = $(this).closest('.odd.parent').parent();
            tr = $(this).closest('tr').children().parent().index();
            tr = tr - 1;
        }
        valorModal(tr);

    });
    $('.modal-footer #btnCancel').show();
    $('.modal-footer #MdAceptar').show();
    $('.modal-footer .panel-footer').remove();
});

function valorModal(parent) {
    var cells = $PurchaseTable.cells().nodes();
    var array = cells.data();
    var index = parseInt(parent);
    Order.translateOrder(
        0,
        array[index][1],
        array[index][2],
        array[index][3],
        array[index][4],
        array[index][5],
        array[index][6],
        array[index][7],
        array[index][8],
        array[index][9],
        array[index][10],
        array[index][11],
        array[index][12],
        array[index][13],
        index);


    $("#AmountDiscount").val(Order.AmountDiscount);
    $("#AmountTax").val(Order.amountTax);
    $("#Cost").val(Order.cost);
    $("#Descuento").val(Order.discount);
    $("#MeasureId").val(Order.measureId);
    $("#ProductId").val(Order.poroductId);
    $("#productName").val(Order.productName);
    $("#WarehouseId").val(Order.warehouseId);
    $("#Warehouse").val(Order.warehouse);
    $("#TaxAbbreviation").val(Order.taxAbbreviation);
    $("#Tax").val(Order.tax);
    $("#Cantidad").value = "";
    $("#Cantidad").val(Order.quantity);
    $("#SubTotal").val(Order.subTotal);
    $("#Accion").val(Order.accion);
    $("#index").val(Order.index);
}

var Order = {
    id: 0,
    productName: "",
    warehouse: "",
    quantity: 0,
    cost: 0,
    discount: 0,
    taxAbbreviation: "",
    subTotal: 0,
    poroductId: 0,
    warehouseId: 0,
    tax: 0,
    amountTax: 0,
    measureId: 0,
    accion: "",
    index: "",

    translateOrder: function (Id, ProductName, Warehouse, Quantity, Cost, Discount, TaxAbbreviation, SubTotal, ProductId, WarehouseId, Tax, AmountTax, MeasureId, Accion, Index) {
        this.productName = ProductName,
            this.warehouse = Warehouse,
            this.quantity = Quantity,
            this.cost = Cost,
            this.discount = Discount,
            this.taxAbbreviation = TaxAbbreviation,
            this.subTotal = SubTotal,
            this.poroductId = ProductId,
            this.warehouseId = WarehouseId,
            this.tax = Tax,
            this.amountTax = AmountTax,
            this.measureId = MeasureId,
            this.accion = Accion,
            this.index = Index;

    },
    updateOrder: function (Id, ProductName, Warehouse, Quantity, Cost, Discount, TaxAbbreviation, SubTotal, ProductId, WarehouseId, Tax, AmountTax, MeasureId, Accion, Index) {
        this.productName = ProductName,
            this.warehouse = Warehouse,
            this.quantity = Quantity,
            this.cost = Cost,
            this.discount = Discount,
            this.taxAbbreviation = TaxAbbreviation,
            this.subTotal = SubTotal,
            this.poroductId = ProductId,
            this.warehouseId = WarehouseId,
            this.tax = Tax,
            this.amountTax = AmountTax,
            this.measureId = MeasureId,
            this.accion = Accion,
            this.index = Index;
    },
    updateTable: function () {
        let array = [this.id, this.productName, this.warehouse, this.quantity, this.cost, this.discount, this.taxAbbreviation, parseFloat(this.amountTax) , this.poroductId,
            this.warehouseId, this.tax, parseFloat(this.subTotal), this.measureId, this.accion];

        $('#PurchaseTable').DataTable().row(this.index).data(array).draw();

    }
};

///////////////Metodo para guardar las modificaciones de la tabla//////
$(document).on('click', '#MdAceptar', function myfunction() {
    var disponible = $("#cantidadProducto").val();
    var d = $("#Cantidad").val();
    if (parseFloat($("#cantidadProducto").val()) >= parseFloat($("#Cantidad").val()) && $("#Cantidad").val() > 0) {

        var total = ($("#Cost").val()) * ($("#Cantidad").val());
        var montoImpuesto = ($("#Tax").val() / 100) * total;
        Order.updateOrder
            (
                0
                , $("#productName").val()
                , $("#Warehouse").val()
                , $("#Cantidad").val()
                , $("#Cost").val()
                , $("#Descuento").val()
                , $("#TaxAbbreviation").val()
                , parseFloat(total)
                , $("#ProductId").val()
                , $("#WarehouseId").val()
                , $("#Tax").val()
                , montoImpuesto.toFixed(2)
                , $("#MeasureId").val()
                , $("#Accion").val()
                , $("#index").val()
            );

        Order.updateTable();
        $("#CerrarBtn").click();
        CalcularMontosNuevos($('#PurchaseTable').DataTable().data().toArray());
    }
    else {
        $("#mensajeError").text("Esta cantidad no puede ser mayor a la disponible");
    }
});

// #endregion

// #region Agregar Nota de Debito

///////////////envia los datos hacia el controlador para crear el Purchase/////////////
$("#Enviar").click(function () {
    var TabletRow = validarDatosEnLaTabla();
    if (TabletRow) {
        var tipoProceso = "Guardar";
        var documentStatus = $(this).attr('data-document');
        CrearDebitNote(documentStatus, tipoProceso);
    }
    else ProcessManager.Warning('Alerta', "Debe selecionar un producto");
});

$("#Procesar").click(function () {
    var tipoProceso = "Procesar";
    var documentStatus = $(this).attr('data-document');
    CrearDebitNote(documentStatus, tipoProceso);

    if (TabletRow) {
        var question = ProcessManager.Question('Alerta', 'Está seguro de que desea procesar esta orden?.');

        question.then((answer) => {
            if (answer) {
                var tipoProceso = "Procesar";
                var documentStatus = $(this).attr('data-document');
                CrearDebitNote(documentStatus, tipoProceso);
            }
        });
    }
    else
        ProcessManager.Warning('Alerta', "Debe selecionar un producto");


});

function validarDatosEnLaTabla() {
    var row = $("#PurchaseTable").DataTable().data().row().any();
    if (row > 0)
        return true;
    return false;
}

//Este metodo agrega o envia la informacion al controller
function CrearDebitNote(documentStatus, tipoProceso) {
    var $form = $('form');

    if ($form.valid()) {

        var model = $form.serialize() + '&' + $.param({ "DocumentEstatusId": documentStatus }) + '&' + $.param({ "tipoProceso": tipoProceso }) + '&' +
            $.param({ "TblDebitNoteRow": ConvertArraryToObject($PurchaseTable.data().toArray()) });

        ///Pondras el nombre del create  y lo reemplazas en la url entonces

        $.ajax({
            url: (($('#Edit-DebitNote').length > 0) ? ProcessManager.getUrl('DebitNote/UpdateDebitNote') : ProcessManager.getUrl('DebitNote/CreateDebitNoteSave')),
            type: 'POST',
            data: model,
            cache: false,
            beforeSend: function myfunction() {
                ProcessManager.Loading(true, 'body');
            },
            success: function myfunction(data) {
                if (data.status) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.Success('Notificación', data.message);
                    ProcessManager.Redirect(data.redirect);
                }
                else {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.Warning('Alerta', data.message);
                }
            },
            error: function myfunction(xhr, status) {
                ProcessManager.Loading(false, 'body');
                ProcessManager.validateStatusCode(xhr.status);
            }
        });
    }
    else {
        ProcessManager.Warning('Alerta', 'Faltan campos por llenar.');
    }
}

//Esta funcion asigna cada fila de la tabla a una fila de la tabla detalle
function ConvertArraryToObject(array) {
    //Verifica bien como estan escrito estas propiedades y las que estan en el tblPushOrderRow que sean iguales
    var PriceMemory = {
        Memory: [],
        add: function (ProductId, AlmacenId, MeasureId, TaxAbbreviation, Quantity, Cost, Tax, AmountTax, Discount, Total) {
            var OrderRow = {
                DebitNoteId: 0,
                ProductId: ProductId,
                MeasureId: MeasureId,
                WarehouseId: AlmacenId,//es el mismo charlatan?
                TaxAbbreviation: TaxAbbreviation,
                Quantity: Quantity,
                Cost: Cost,
                Tax: Tax,
                AmountTax: AmountTax,
                Discount: Discount,
                AmountDiscount: (parseFloat(Total) * (parseFloat(Discount) / 100)),
                Total: Total
            };
            PriceMemory.Memory.push(OrderRow);
        }
    };

    for (var i = 0; i < array.length; i++) {
        PriceMemory.add(
            array[i][8],
            array[i][9],
            array[i][12],
            array[i][6],
            array[i][3],//5
            array[i][4],
            array[i][10],
            array[i][7],
            array[i][5],
            array[i][11]

        );
    }

    return PriceMemory.Memory;
}


// #endregion