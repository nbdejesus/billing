﻿$(document).ready(function myfunction() {

    CallPriceTable();
});


///Esta es la funcion que llama a la tabla, tanto en el create como en el edit
function CallPriceTable() {
    //Estas con las columnas que apareceran en la tabla
    var columnsTitle = [

        { title: "Id" },
        { title: "Articulos" },//1
        { title: "Almacen" },//2
        { title: "Cantidad" },//3
        { title: "costo" },//4    
        { title: "Descuento %" },//5
        { title: "Tipo Imp." },//6
        { title: "Monto Imp." },//7      
        { title: "ArticulosId" },//8
        { title: "AlmacenId" },//9
        { title: "Impuesto %" },//10
        { title: "Sub Total" },//11
        { title: "MeasureId" },//12
        { title: "Acción" },//13



    ];

    //Con este tu le dices al datable cuales columnas ocultar
    var columnVisible = [
        //esa columna la tenias 
        { "visible": false, "targets": 0 },
        { "visible": false, "targets": 8 },
        { "visible": false, "targets": 9 },
        { "visible": false, "targets": 12 }


    ];

    //esto valida si estas en el formulario de edit o crear

    //este te configura la tabla cuando tu estas creando la orden de compra y no te carga ninguna informacion del a BD
    $PurchaseTable = $('#PurchaseTable').DataTable({
        bLengthChange: false,
        data: null,//si te fijas aqui la data esta null<---
        columns: columnsTitle,
        columnDefs: columnVisible,
        language: {
            processing: "Procesando",
            search: "Buscar:",
            lengthMenu: "Ver _MENU_ Filas",
            info: "_START_ - _END_ de _TOTAL_ elementos",
            infoEmpty: "0 - 0 de 0 elementos",
            infoFiltered: "(Filtro de _MAX_ entradas en total)",
            loadingRecords: "Cargando datos.",
            zeroRecords: "No se encontraron datos",
            emptyTable: "No hay datos disponibles",
            paginate: {
                first: "Primero",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultimo"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });

}

//Esta funcion asigna cada fila de la tabla a una fila de la tabla detalle
function ConvertArraryToObject(array) {
    //Verifica bien como estan escrito estas propiedades y las que estan en el tblPushOrderRow que sean iguales
    var PriceMemory = {
        Memory: [],
        add: function (ProductId, AlmacenId, MeasureId, TaxAbbreviation, Quantity, Cost, Tax, AmountTax, Discount, AmountDiscount, Total, Id) {
            var OrderRow = {
                Id: Id,
                ProductId: ProductId,
                MeasureId: MeasureId,
                WarehouseId: AlmacenId,//es el mismo charlatan?
                TaxAbbreviation: TaxAbbreviation,
                Quantity: Quantity,
                Cost: Cost,
                Tax: Tax,
                AmountTax: AmountTax,
                Discount: Discount,
                AmountDiscount: AmountDiscount,
                Total: Total
            };
            PriceMemory.Memory.push(OrderRow);
        }
    };

    for (var i = 0; i < array.length; i++) {
        PriceMemory.add(
            array[i][9],
            array[i][10],
            array[i][13],
            array[i][7],
            array[i][4],
            array[i][5],
            array[i][11],
            array[i][12],
            array[i][6],
            array[i][6],
            array[i][8],
            array[i][1]
        );
    }

    return PriceMemory.Memory;
}


function CalcularMontos(tabla) {
    valor = 0;
    tax = parseInt(0);
    desc = parseInt(0);
    impuestos = parseInt(0);
    imp = parseInt(0);

    for (var i = 0; i < tabla.length; i++) {
        //Multipliamos el subTotal con su impuesto correspondiente
        imp = parseFloat(tabla[i][8]);//<<la columna que era la 7 ya no es la columna 7 ahora es la columna 8 porque al yo agregar una nueva columna al inicio los Id se desplazan

        tax += (parseFloat(tabla[i][7]) * (parseFloat(tabla[i][10]) / 100));
        valor += parseFloat(tabla[i][7]);
        desc += (parseFloat(tabla[i][7]) / 100) * parseFloat(tabla[i][5]);
    }

    $("#mensaje").text(valor.toFixed(2));

    $("#Mensajetax").text(tax.toFixed(2));

    $("#MensajeDescuento").text(desc.toFixed(2));

    $("#MensajeTotal").text(((valor + tax) - desc).toFixed(2));

    $("#descuentoTotal").val((valor + tax) - desc);

    $("#mensajesubtotal").val(valor.toFixed(2));

    $("#taxtotal").val(tax.toFixed(2));

    $("#Descuentototal").val(desc.toFixed(2));

    $("#Totaltotal").val(((valor + tax) - desc).toFixed(2));

}

/////////////Llama el modal para ver el listado de ordenes de compra/////
$(document).on('click', '.ordenCompra', function myfunction() {

    var form = ProcessManager.LoadForm(ProcessManager.getUrl('DebitNote/GetListPurchaseOrder'));
    form.then((data) => {
        ProcessManager.ShowModal("Factura de Compras ", ' icon-cart-add', data, false, false,3);

        CallDinamicTable('#tableList', 10, false);
    });

});

$(document).on('click', '.status', function myfunction() {
    $option = $(this);

    var question = ProcessManager.Question('Alerta', 'Está seguro de que desea ' + (($option.attr('data-status') === 'True') ? 'activar' : 'cambiar el') + ' estado a la Orden?.');

    question.then((answer) => {
        if (answer) {

            $.ajax({
                url: ProcessManager.getUrl('DebitNote/StatusDebitNote'),
                dataType: 'JSON',
                type: 'POST',
                data: {
                    DocumentStatusID: $option.attr('data-estado'),
                    Id: $option.attr('data-id')
                },
                beforeSend: function myfunction() {
                    ProcessManager.Loading(true, 'body');
                },
                success: function (data) {
                    if (data.status ===true) {
                        ProcessManager.Success('Notificación', data.message);
                        ProcessManager.Redirect(data.redirect);
                    }
                    else {
                        ProcessManager.Warning('Alerta', data.message);
                        ProcessManager.Loading(false, 'body');
                    }
                },
                error: function myfunction(xhr, status) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.validateStatusCode(xhr.status);
                }
            });
        }
    });
});

