﻿$(document).ready(function myfunction() {
    console.log(5);
    $(document).on('click', '#BtnCreate', function myfunction() {
        var $option = $(this);
        var form = ProcessManager.LoadForm(ProcessManager.getUrl('Warehouse/WarehouseIndex'), null);
        form.then((data) => {
            ProcessManager.ShowModal("Agregar almacén", 'icon-plus2', data, true, true);
            //ConfingForm();
        });
    });

    $(document).on('click', '.edit', function myfunction() {
        var $option = $(this);
        var form = ProcessManager.LoadForm(ProcessManager.getUrl('Warehouse/EditWarehouse'), { Id: $option.attr('data-id') });
        form.then((data) => {
            ProcessManager.ShowModal("Editar Comprobante ", 'icon-plus2', data, true, true);

            //ConfingForm();
        });
    });

    $('#form-warehouse').validate({
        rules: {
            'Warehouse.Name': { required: true, minlength: 3 },
            'Warehouse.description': 'required'
        },
        messages: {
            'Warehouse.Name': { required: 'Debe ingresar el nombre', minlength: 'El nombre del almacén debe ser mas largo de 3 caracteres' },
            'Warehouse.description': 'La descripción del almacén es requerida'
        },
        debug: true
    });

    $(document).on('click', '.status', function myfunction() {
        var status = $(this).attr('data-status');
        var id = $(this).attr('data-id');
        var question = ProcessManager.Question('Alerta', 'Está seguro de que desea ' + ((status === 'true') ? 'activar' : 'desactivar') + ' esta almacén?.');

        question.then((answer) => {
            if (answer) {
                $.ajax({
                    url: ProcessManager.getUrl('Warehouse/StatusWarehouse'),
                    type: 'POST',
                    data: { Id: id, status: status },
                    beforeSend: function myfunction() {
                        ProcessManager.Loading(true, '.panel');
                    },
                    success: function myfunction(data) {
                        if (data.status) {
                            ProcessManager.Loading(false, '.panel');
                            ProcessManager.Success('Notificación', data.message);
                            ProcessManager.Redirect(data.redirect);
                        }
                        else {
                            ProcessManager.Loading(false, '.panel');
                            ProcessManager.Warning('Alerta', data.message);
                        }
                    },
                    error: function myfunction(xhr, status) {
                        ProcessManager.validateStatusCode(xhr.status);  
                        ProcessManager.Warning('Alerta', 'Error al enviar los datos');
                    }
                });
            } else {
                HeavenBook.Info('La sucursal no se ha podido actualizar.');
            }
        });
    });


    $(document).on('click', '#MdAceptar', function myfunction() {
        var $form = $((($('#form-warehouse').length > 0) ? $('#form-warehouse') : $('#warehouse-update')));

        if ($form.valid()) {
            $.ajax({
                url: (($('#form-warehouse').length > 0) ? ProcessManager.getUrl('Warehouse/CreateWarehouse') : ProcessManager.getUrl('Warehouse/UpdateWarehouse')),
                type: 'POST',
                data: $form.serialize(),
                beforeSend: function myfunction() {
                    ProcessManager.Loading(true, '.panel');
                },
                success: function myfunction(data) {
                    if (data.status) {
                        ProcessManager.Loading(false, '.panel');
                        ProcessManager.Success('Notificación', data.message);
                        ProcessManager.Redirect(data.redirect);
                    }
                    else {
                        ProcessManager.Loading(false, '.panel');
                        ProcessManager.Warning('Alerta', data.message);
                    }
                },
                error: function myfunction(xhr, status) {
                    ProcessManager.Loading(false, '.panel');
                    ProcessManager.validateStatusCode(xhr.status);
                }
            });
        }
    });
});