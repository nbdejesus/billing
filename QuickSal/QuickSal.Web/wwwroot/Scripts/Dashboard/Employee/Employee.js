﻿$(document).ready(function myfunction() {

    if ($('[name="Employee.DocumentTypeId"]').length > 0) {
        ProcessManager.getDocumentTypeConfiguration();
    }


    $('form').validate({
        rules: {
            'Employee.EmployeeCode': { required: true },
            'User.Email': { required: true, email: true },
            'Employee.PrivilegesId': { required: true },
            'Employee.Name': { required: true },
            'Employee.DocumentTypeId': { required: true },
            'Employee.DocumentNumber': { required: true },
            'Employee.InstitutionalEmail': { email: true },
            'Employee.Salary': { number: true },
            'Employee.User.Email': { email: true, required: true }
        },
        messages: {
            'Employee.EmployeeCode': { required: 'Este campo es obligatorio' },
            'Employee.PrivilegesId': { required: 'Este campo es obligatorio' },
            'Employee.Name': { required: 'Este campo es obligatorio' },
            'User.Email': { required: 'Este campo es obligatorio' },
            'Employee.DocumentTypeId': { required: 'Este campo es obligatorio' },
            'Employee.DocumentNumber': { required: 'Este campo es obligatorio' },
            'Employee.InstitutionalEmail': { email: 'Debe ingresar el correo electrónico con el formato correcto. Por ejemplo: u@localhost.com' },
            'Employee.Salary': { number: 'El campo debe ser numérico' },
            'Employee.User.Email': { email: 'Debe ingresar el correo electrónico con el formato correcto. Por ejemplo: u@localhost.com', required: 'Este campo es obligatorio' }
        },
        debug: true
    });


    $('#BtnAceptar').click(function myfunction() {
        var $form = $('form');

        if ($form.valid()) {
            if ($('#frm-create-employee').length > 0) {
                var question = ProcessManager.Question('Alerta', 'Verificó  que todo los datos estan correctos?, se enviará una notificación con la contraseña al correo de acceso proporcionado del empleado.');

                question.then((answer) => {
                    if (answer) {
                        SaveEmployee();
                    }
                });
            }
            else {
                SaveEmployee();
            }   
        }
        else {
            ProcessManager.Warning('Alerta', 'Faltan campos por llenar.');
        }
    });

    // Single picker
    $('.date').daterangepicker({
        singleDatePicker: true
    });

    $(document).on('click', '#btnPhotoProfile', function myfunction() {
        $('#PhotosUpload').trigger('click');
    });

    function uploadProgressHandler(event) {
        $("#loaded_n_total").html("Uploaded " + event.loaded + " bytes of " + event.total);
        var percent = (event.loaded / event.total) * 100;
        var progress = Math.round(percent);
        $("#btnPhotoProfile").attr('disabled', true);
        $("#btnPhotoProfile").html("<i class=\"icon-file-upload position-left\"></i> Porcentage cargado: " + progress + "%");
    }

    function loadHandler(event) {
        $("#status").html(event.target.responseText);
        $("#btnPhotoProfile").html("<i class=\"icon-camera position-left\"></i> Cargar imagen");
        $("#btnPhotoProfile").attr('disabled', false);
    }

    function errorHandler(event) {
        $("#status").html("Upload Failed");
    }

    function abortHandler(event) {
        $("#status").html("Upload Aborted");
    }

    $(document).on('change', '#PhotosUpload', function myfunction() {
        var srcActual = $('#Picture-Account').attr("src");

        if (ProcessManager.ValidateImage($('#PhotosUpload'))) {
            event.preventDefault();

            var formData = new FormData(document.getElementById("Picture-Profile"));

            $.ajax({
                url: ProcessManager.getUrl('employee/SaveUploadedPicture'),
                method: 'POST',
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                beforeSend: function myfunction() {
                    $('#Picture-Account').attr("src", ProcessManager.GIF);
                },
                success: function (data) {
                    if (data.status === true) {
                        $('#Picture-Account').attr("src", data.file);
                        $('#picture').val(data.fileName);
                    }
                    else {
                        ProcessManager.Warning('Alerta', data.message);
                        $('#Picture-Account').attr("src", srcActual);
                        $('#picture').val("");
                    }
                },
                error: function (datos) {
                    ProcessManager.Warning('Alerta', datos.message);
                    $('#Picture-Account').attr("src", srcActual);
                    $('#picture').val("");
                },
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress",
                        uploadProgressHandler,
                        false
                    );
                    xhr.addEventListener("load", loadHandler, false);
                    xhr.addEventListener("error", errorHandler, false);
                    xhr.addEventListener("abort", abortHandler, false);
                    return xhr;
                }
            });
        }
    });


    $(document).on('click', '.status', function myfunction() {
        $option = $(this);

        var question = ProcessManager.Question('Alerta', 'Está seguro de que desea ' + (($option.attr('data-status') === 'True') ? 'activar' : 'desactivar') + ' este empleado?.');

        question.then((answer) => {
            if (answer) {

                $.ajax({
                    url: ProcessManager.getUrl('employee/StatusEmployee'),
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        status: $option.attr('data-status'),
                        employee: $option.attr('data-employee')
                    },
                    beforeSend: function myfunction() {
                        ProcessManager.Loading(true, 'body');
                    },
                    success: function (data) {
                        if (data.status === true) {
                            ProcessManager.Success('Notificación', data.message);
                            ProcessManager.Redirect(data.redirect);
                        }
                        else {
                            ProcessManager.Warning('Alerta', data.message);
                            ProcessManager.Loading(false, 'body');
                        }
                    },
                    error: function (datos) {
                        ProcessManager.Warning('Alerta', 'Error al contactar servidor');
                        ProcessManager.Loading(false, 'body');
                    }
                });
            }
        });
    });



});


function SaveEmployee() {
    var $form = $('form');

    $.ajax({
        url: (($('#frm-edit-employee').length > 0) ? ProcessManager.getUrl('Employee/UpdateEmployee') : ProcessManager.getUrl('Employee/CreateEmployee')),
        type: 'POST',
        data: $form.serialize(),
        beforeSend: function myfunction() {
            ProcessManager.Loading(true, 'body');
        },
        success: function myfunction(data) {
            if (data.status) {
                ProcessManager.Loading(false, 'body');
                ProcessManager.Success('Notificación', data.message);
                ProcessManager.Redirect(data.redirect);
            }
            else {
                ProcessManager.Loading(false, 'body');
                ProcessManager.Warning('Alerta', data.message);
            }
        },
        error: function myfunction(xhr, status) {
            ProcessManager.Loading(false, 'body');
            ProcessManager.validateStatusCode(xhr.status);
        }
    });
}