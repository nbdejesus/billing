﻿var PrivilegeMemory = {
    Memory: [],
    add: function (SubObject, CanRead, CanWrite, CanEdit, CanDelete, Privileges, security) {
        var Privilege = {
            SubObjectId: SubObject,
            CanRead: CanRead,
            CanWrite: CanWrite,
            CanEdit: CanEdit,
            CanDelete: CanDelete,
            PrivilegesId: Privileges,
            Id: ((security ===undefined) ? 0 : security)
        };
        PrivilegeMemory.Memory.push(Privilege);
    }
};


$(document).ready(function myfunction() {
    LoadMemory();

    if (screen.width >= 1024) {
        $('.table-responsive').addClass('pre-scrollable');
        $('#TableHeader thead').show();
        $('#TableHeaderMovil thead').hide();
    }
    else {
        $('#TableHeader thead').hide();
        $('#TableHeaderMovil thead').show();
    }

    $('form').validate({
        rules: {
            'Privilege.Name': { required: true }
        },
        messages: {
            'Privilege.Name': { required: 'Este campo es requerido' }
        },
        debug: true
    });

    //guardamos en memoria las materias retiradas o seleccionada
    $(document).on('click', 'table tbody .canRead :checkbox', function myfunction() {
        var $input = $(this);
        var SubObject = $input.closest('tr').attr('data-object');

        for (var i = 0; i < PrivilegeMemory.Memory.length; i++) {
            if (PrivilegeMemory.Memory[i].SubObjectId ===SubObject) {
                PrivilegeMemory.Memory[i].CanRead = $input.is(':checked');
            }
        }
    });

    //guardamos en memoria las materias retiradas o seleccionada
    $(document).on('click', 'table tbody .canWrite :checkbox', function myfunction() {
        var $input = $(this);
        var SubObject = $input.closest('tr').attr('data-object');

        for (var i = 0; i < PrivilegeMemory.Memory.length; i++) {
            if (PrivilegeMemory.Memory[i].SubObjectId ===SubObject) {
                PrivilegeMemory.Memory[i].CanWrite = $input.is(':checked');
            }
        }
    });

    $(document).on('click', 'table tbody .canEdit :checkbox', function myfunction() {
        var $input = $(this);
        var SubObject = $input.closest('tr').attr('data-object');

        for (var i = 0; i < PrivilegeMemory.Memory.length; i++) {
            if (PrivilegeMemory.Memory[i].SubObjectId ===SubObject) {
                PrivilegeMemory.Memory[i].CanEdit = $input.is(':checked');
            }
        }
    });

    $(document).on('click', 'table tbody .canDelete :checkbox', function myfunction() {
        var $input = $(this);
        var SubObject = $input.closest('tr').attr('data-object');

        for (var i = 0; i < PrivilegeMemory.Memory.length; i++) {
            if (PrivilegeMemory.Memory[i].SubObjectId ===SubObject) {
                PrivilegeMemory.Memory[i].CanDelete = $input.is(':checked');
            }
        }
    });

    //Metodo utilizado para guardar los cambios para correquisitos / Prerrequisitos / electiva
    $(document).on('click', '#BtnAceptar', function myfunction() {
        var $form = $('form');

        if ($form.valid()) {

            $.ajax({
                url: ProcessManager.getUrl('employee/CreatePrivilege'),
                type: 'POST',
                data: {
                    model: {
                        security: PrivilegeMemory.Memory,
                        Privilege: {
                            name: $('#PrivilegeName').val()
                        },
                    }
                },
                beforeSend: function myfunction() {
                    ProcessManager.Loading(true, '.panel-body');
                },
                success: function myfunction(data) {
                    if (data.status) {
                        ProcessManager.Loading(false, '.panel-body');
                        ProcessManager.Success('Notificación', data.message);
                        ProcessManager.Redirect(data.redirect);
                    }
                    else {
                        ProcessManager.Loading(false, '.panel-body');
                        ProcessManager.Warning('Alerta', data.message);
                    }
                },
                error: function myfunction(xhr, status) {
                    ProcessManager.Loading(false, '.panel-body');
                    ProcessManager.validateStatusCode(xhr.status);
                }
            });
        }

    });


    $(document).on('click', '#BtnUpdate', function myfunction() {
        var $form = $('form');

        if ($form.valid()) {

            $.ajax({
                url: ProcessManager.getUrl('employee/UpdatePrivilege'),
                type: 'POST',
                data: {
                    model: {
                        security: PrivilegeMemory.Memory,
                        Privilege: {
                            name: $('#PrivilegeName').val(),
                            Id: $('#Privilege').val()
                        }
                    }
                },
                beforeSend: function myfunction() {
                    ProcessManager.Loading(true, '.panel-body');
                },
                success: function myfunction(data) {
                    if (data.status) {
                        ProcessManager.Loading(false, '.panel-body');
                        ProcessManager.Success('Notificación', data.message);
                        ProcessManager.Redirect(data.redirect);
                    }
                    else {
                        ProcessManager.Loading(false, '.panel-body');
                        ProcessManager.Warning('Alerta', data.message);
                    }
                },
                error: function myfunction(xhr, status) {
                    ProcessManager.Loading(false, '.panel-body');
                    ProcessManager.validateStatusCode(xhr.status);
                }
            });
        }

    });


    $(document).on('click', '.status', function myfunction() {
        $option = $(this);

        var question = ProcessManager.Question('Alerta', 'Está seguro de que desea ' + (($option.attr('data-status') === 'True') ? 'activar' : 'desactivar') + ' este privilegio?.');

        question.then((answer) => {
            if (answer) {

                $.ajax({
                    url: ProcessManager.getUrl('Employee/StatusPrivileges'),
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        status: $option.attr('data-status'),
                        privilege: $option.attr('data-privilege')
                    },
                    beforeSend: function myfunction() {
                        ProcessManager.Loading(true, 'body');
                    },
                    success: function (data) {
                        if (data.status === true) {
                            ProcessManager.Success('Notificación', data.message);
                            ProcessManager.Redirect(data.redirect);
                        }
                        else {
                            ProcessManager.Warning('Alerta', data.message);
                            ProcessManager.Loading(false, 'body');
                        }
                    },
                    error: function (datos) {
                        ProcessManager.Warning('Alerta', 'Error al contactar servidor');
                        ProcessManager.Loading(false, 'body');
                    }
                });
            }
        });
    });
});


function LoadMemory() {
    var $tr = $('table tbody tr').not('.active');

    //.SubjectSelect: checked
    $tr.each(function myfunction(index) {
        var $canRead = $(this).find('.canRead').find('input');
        var $canWrite = $(this).find('.canWrite').find('input');
        var $canEdit = $(this).find('.canEdit').find('input');
        var $canDelete = $(this).find('.canDelete').find('input');
        var subObject = $(this).attr('data-object');
        var securityId = $(this).attr('data-object-privilege');
        var privilege = $('#Privilege').val();

        PrivilegeMemory.add(subObject, $canRead.is(":checked"), $canWrite.is(":checked"), $canEdit.is(":checked"), $canDelete.is(":checked"), privilege, securityId);
    });
}
