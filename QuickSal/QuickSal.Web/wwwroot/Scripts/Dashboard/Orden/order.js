﻿$(document).ready(function myfunction() {
    LoadOrder();

    $('.filterDate').click(function myfunction() {

    });


    $(document).on('click', '.status', function myfunction() {
        $option = $(this);

        var question = ProcessManager.Question('Alerta', 'Está seguro de que desea cambiar el estado de la orden?.');

        question.then((answer) => {
            if (answer) {

                $.ajax({
                    url: ProcessManager.getUrl('order/UpdateStatusOrder'),
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        status: $option.attr('data-status'),
                        orderId: $option.closest('.invoice-grid').attr('data-id')
                    },
                    success: function (data) {
                        if (data.status == true) {
                            LoadOrder();
                        }
                        else {
                            ProcessManager.Warning('Alerta', data.message);
                            ProcessManager.Loading(false, 'body');
                        }
                    },
                    error: function myfunction(xhr, status) {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.validateStatusCode(xhr.status);
                    }
                });
            }
        });
    });
});



function LoadOrder() {
    //var local = localStorage.getItem('status');

    //if (local == null) {
    //    localStorage.setItem('status', 'Tom');
    //    localStorage.setItem('date', 'Tom');
    //}   

    $.ajax({
        url: ProcessManager.getUrl('order/_Orders'),
        dataType: 'HTML',
        type: 'GET',
        beforeSend: function myfunction() {
            ProcessManager.Loading(true, '#dvOrders');
        },
        success: function (data) {
            ProcessManager.Loading(false, '#dvOrders');
            $('#dvOrders').html('');
            $('#dvOrders').html(data);     
        },
        error: function myfunction(xhr, status) {
            ProcessManager.Loading(false, '#dvOrders');
            ProcessManager.validateStatusCode(xhr.status);
        }
    });
}