﻿$(document).ready(function myfunction() {

    $('#form-Account').validate({
        rules: {
            'User.Names': { required: true, minlength: 3 },
            'User.DocumentTypeId': 'required',
            'User.DocumentNumber': 'required'
        },
        messages: {
            'User.Names': { required: 'Este campo es requerido', minlength: 'El nombre debe ser mas largo de 3 caracteres' },
            'User.DocumentTypeId': 'El documento de identificación es requerido',
            'User.DocumentNumber': 'Debe ingresar el número de identificación'
        },
        debug: true
    });

    $('#Country').change(function myfunction() {
        if ($(this).val() != "") {

            var form = ProcessManager.LoadForm(
                $(this).attr('data-url'),
                {
                    CountryId: $(this).children(':selected').val()
                }, '.panel');

            form.then((data) => {
                var option = '';
                $('#ProvinceId').html('');
                if (data.provinceList.length > 0) {
                    option = option + '<option value="" selected="selected">Selecionar...</option>';
                    for (var i = 0; i < data.provinceList.length; i++) {
                        option = option + '<option value=' + data.provinceList[i].id + '>' + data.provinceList[i].description + '</option>';
                    }
                }
                $('#ProvinceId').append(option);
            });
        }

    });

    $(document).on('click', '#BtnAceptar', function myfunction() {
        var $form = $('#form-Account');

        if ($form.valid()) {
            $.ajax({
                url: $form.attr('action'),
                type: 'POST',
                data: $form.serialize(),
                beforeSend: function myfunction() {
                    ProcessManager.Loading(true, 'body');
                },
                success: function myfunction(data) {
                    if (data.status) {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.Success('Notificación', data.message, true);
                        ProcessManager.Redirect(data.redirect);
                    }
                    else {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.Warning('Alerta', data.message);
                    }
                },
                error: function myfunction(xhr, status) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.validateStatusCode(xhr.status);  
                }
            });
        }
    });


    $(document).on('click', '#btnPhotoProfile', function myfunction() {
        $('#PhotosUpload').trigger('click');
    });

    function uploadProgressHandler(event) {
        $("#loaded_n_total").html("Uploaded " + event.loaded + " bytes of " + event.total);
        var percent = (event.loaded / event.total) * 100;
        var progress = Math.round(percent);
        $("#btnPhotoProfile").attr('disabled', true);
        $("#btnPhotoProfile").html("<i class=\"icon-file-upload position-left\"></i> Porcentage cargado: " + progress + "%");
    }

    function loadHandler(event) {
        $("#status").html(event.target.responseText);
        $("#btnPhotoProfile").html("<i class=\"icon-camera position-left\"></i> Cargar imagen");
        $("#btnPhotoProfile").attr('disabled', false);
    }

    function errorHandler(event) {
        $("#status").html("Upload Failed");
    }

    function abortHandler(event) {
        $("#status").html("Upload Aborted");
    }

    $(document).on('change', '#PhotosUpload', function myfunction() {
        var srcActual = $('#Picture-Account').attr("src");
        var $form = $("#Picture-Profile");

        if (ProcessManager.ValidateImage($('#PhotosUpload'))) {
            event.preventDefault();

            var formData = new FormData(document.getElementById("Picture-Profile"));

            $.ajax({
                url: $form.attr("action"),
                method: 'POST',
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                beforeSend: function myfunction() {
                    $('#Picture-Account').attr("src", ProcessManager.GIF);
                },
                success: function (data) {
                    if (data.status == true) {
                        $('#Picture-Account').attr("src", data.file);
                        $('#picture').val(data.fileName);
                    }
                    else {
                        ProcessManager.Warning('Alerta', data.message);
                        $('#Picture-Account').attr("src", srcActual);
                        $('#picture').val('');
                    }
                },
                error: function (datos) {
                    HeavenBook.Warning('Alerta', datos.message);
                    $('#Picture-Account').attr("src", srcActual);
                    $('#picture').val('');
                },
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress",
                        uploadProgressHandler,
                        false
                    );
                    xhr.addEventListener("load", loadHandler, false);
                    xhr.addEventListener("error", errorHandler, false);
                    xhr.addEventListener("abort", abortHandler, false);
                    return xhr;
                }
            });
        }
    });

});