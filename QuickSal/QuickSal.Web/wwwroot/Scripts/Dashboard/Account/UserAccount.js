﻿var nuIntentos = 0;

$(document).ready(function myfunction() {
    if (localStorage.getItem('nuIntentos') >= 2) {
        $("#loginRecaptcha").show();
    }
    if ($('#ConfirmationStatus').length > 0) {
        setTimeout(function () {
            location.href = $('#ConfirmationStatus').attr('data-url');
        }, 5000);
    }

    if ($('#form-Account').length > 0) {
        $('#form-Account').validate({
            rules: {
                'User.Name': { required: true, nameValidate: "" },
                'User.UserPassword': { required: true, minlength: 6 },
                'RepeatUserPassword': { required: true, equalTo: "#login-password" },
                'User.Email': { required: true, email: true },
                'User.LastName': { required: true, nameValidate: "" },
                'TermAndCondition': { required: true },
            },
            messages: {
                'User.Name': { required: 'Como te llamas?', nameValidate: "Solo puede ingresar números y letras" },
                'User.LastName': { required: 'Como te llamas?', nameValidate: "Solo puede ingresar números y letras" },
                'User.UserPassword': { required: 'La contraseña es requerida', minlength: 'La contraseña debe de ser mas larga de 6 caracteres' },
                'RepeatUserPassword': { required: 'Debe ingresar la contraseña de confirmacion', equalTo: 'La contraseña de confirmación es diferente' },
                'User.Email': { required: 'Debe ingresar un correo electrónico', email: 'Debe ingresar el correo electrónico con el formato correcto. Por ejemplo: u@localhost.com' },
                'TermAndCondition': { required: 'Debe aceptar los terminos y condiciones.' }
            },
            debug: true
        });
    }

    if ($('#form-User').length > 0) {
        $('#form-User').validate({
            rules: {
                'User.UserPassword': { required: true, minlength: 6 },
                'User.Email': { required: true, email: true }
            },
            messages: {
                'User.UserPassword': { required: 'La contraseña es requerida', minlength: 'La contraseña debe de ser mas larga de 6 caracteres' },
                'User.Email': { required: 'Debe ingresar un correo electrónico', email: 'Debe ingresar el correo electrónico con el formato correcto. Por ejemplo: u@localhost.com' }
            },
            debug: true
        });
    }


    if ($('#form-ForgoPassword').length > 0) {
        $('#form-ForgoPassword').validate({
            rules: {
                'email': { required: true, email: true }
            },
            messages: {
                'email': { required: 'Debe ingresar su correo electrónico', email: 'Debe ingresar el correo electrónico con el formato correcto. Por ejemplo: u@localhost.com' }
            },
            debug: true
        });
    }


    if ($('#form-ChangePassword').length > 0) {
        $('#form-ChangePassword').validate({
            rules: {
                'User.UserPassword': { required: true, minlength: 6 },
                'RepeatUserPassword': { required: true, equalTo: "#login-password" }
            },
            messages: {
                'User.UserPassword': { required: 'La contraseña es requerida', minlength: 'La contraseña debe de ser mas larga de 6 caracteres' },
                'RepeatUserPassword': { required: 'Debe ingresar la contraseña de confirmacion', equalTo: 'La contraseña de confirmación es diferente' }
            },
            debug: true
        });
    }

    $('#BtnAceptarLogin').click(function myfunction() {
        var $form = $("#form-User");

        if ($form.valid()) {
            var form = $form.serialize();
            if (localStorage.getItem('nuIntentos') >= 2) {
                $.ajax({
                    url: ProcessManager.getUrl('Account/RecapcahResponse'),
                    type: 'POST',
                    data: $form.serialize(),
                    beforeSend: function myfunction() {
                        ProcessManager.Loading(true, 'body');
                    },
                    success: function myfunction(data) {
                        if (data) {
                            LoginAcces(form);
                        }
                        else {                            
                            ProcessManager.Loading(false, 'body');
                            document.getElementById("mssRecaptchaLogin").innerHTML = "Debe dar click al cuadro de validacion";
                        }
                    },
                    error: function myfunction(xhr, data) {                       
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.validateStatusCode(xhr.data);
                    }
                });
            }
            else {
                LoginAcces(form);
            }
            // else {
            //     ProcessManager.Loading(false, 'body');
            //     ProcessManager.Warning('Alerta', data.message);
            // }
        }
    });



    function LoginAcces(form) {
        $.ajax({
            url: ProcessManager.getUrl('Account/UserLogin'),
            type: 'POST',
            data: form,
            beforeSend: function myfunction() {
                ProcessManager.Loading(true, 'body');
            },
            success: function myfunction(data) {
                if (data.status) {
                    localStorage.setItem('nuIntentos', 0);
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.Success('Notificación', data.message);
                    ProcessManager.Redirect(ProcessManager.getUrl('dashboard/DashboardPanel'));
                }
                else {
                    grecaptcha.reset();
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.Warning('Alerta', data.message);

                    nuIntentos = parseInt(localStorage.getItem('nuIntentos')) + 1;
                    localStorage.setItem('nuIntentos', nuIntentos);
                    if (nuIntentos >= 2) {
                        $("#loginRecaptcha").show();
                    }
                }
            },
            error: function myfunction(xhr, status) {
                grecaptcha.reset();
                ProcessManager.Loading(false, 'body');
                ProcessManager.validateStatusCode(xhr.status);
            }
        });
    }
    $('#BtnRecoveryPassword').click(function myfunction() {
        var $form = $("#form-ForgoPassword");

        if ($form.valid()) {
            $.ajax({
                url: ProcessManager.getUrl('Account/SendChangeMail'),
                type: 'POST',
                data: $form.serialize(),
                beforeSend: function myfunction() {
                    ProcessManager.Loading(true, 'body');
                },
                success: function myfunction(data) {
                    if (data.status) {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.Success('Notificación', data.message);
                        ProcessManager.Redirect(data.redirect);
                    }
                    else {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.Warning('Alerta', data.message);
                    }
                },
                error: function myfunction(xhr, status) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.validateStatusCode(xhr.status);
                }
            });
        }
    });


    $('#BtnChangePassword').click(function myfunction() {
        var $form = $("#form-ChangePassword");

        if ($form.valid()) {
            $.ajax({
                url: ProcessManager.getUrl('account/ChangePassword'),
                type: 'POST',
                data: $form.serialize(),
                beforeSend: function myfunction() {
                    ProcessManager.Loading(true, 'body');
                },
                success: function myfunction(data) {
                    if (data.status) {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.Success('Notificación', data.message);
                        ProcessManager.Redirect(data.redirect);
                    }
                    else {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.Warning('Alerta', data.message);

                    }
                },
                error: function myfunction(xhr, status) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.validateStatusCode(xhr.status);
                }
            });
        }
    });

    $('#BtnCreateUser').click(function myfunction() {
        var $form = $('#form-Account');
    
        if ($form.valid()) {
            $.ajax({
                url: ProcessManager.getUrl('Account/RecapcahResponse'),
                type: 'POST',
                data: $form.serialize(),
                beforeSend: function myfunction() {
                    ProcessManager.Loading(true, 'body');
                },
                success: function myfunction(data) {
                    if (data) {
                        $.ajax({
                            url: ProcessManager.getUrl('Account/UserCreate'),
                            type: 'POST',
                            data: $form.serialize(),
                            beforeSend: function myfunction() {
                                ProcessManager.Loading(true, 'body');
                            },
                            success: function myfunction(data) {
                                if (data.status) {
                                    ProcessManager.Loading(false, 'body');
                                    ProcessManager.Success('Notificación', data.message);
                                    ProcessManager.Redirect(ProcessManager.getUrl('dashboard/DashboardPanel'));
                                }
                                else {
                                    ProcessManager.Loading(false, 'body');
                                    ProcessManager.Warning('Alerta', data.message);
                                    $("#loginRecaptcha").remove();
                                    grecaptcha.reset();
                                }
                            },
                            error: function myfunction(xhr, status) {
                                ProcessManager.Loading(false, 'body');
                                ProcessManager.validateStatusCode(xhr.status);
                            }
                        });
                    }
                    else {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.validateStatusCode(data);
                        document.getElementById("mssRecaptcha").innerHTML = "Debe dar click al cuadro de validacion";
                    }
                },
                error: function myfunction(xhr, data) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.validateStatusCode(data);
                }
                //ProcessManager.validateStatusCode(xhr.status);
            });
        }
    });


    $("#ForgotPassword").click(function myfunction() {
        var $ForgotEmail = $("#Forgot-email");

        if (HeavenBook.ValidEmail($ForgotEmail.val())) {
            var url = $("#ForgotPassword").attr("data-url");

            $.ajax({
                url: url,
                type: 'POST',
                data: { email: $ForgotEmail.val() },
                beforeSend: function myfunction() {
                    HeavenBook.loading(true, 'ForgotPassword', "Enviando...");
                },
                success: function myfunction(data) {
                    if (data.status) {
                        HeavenBook.loading(false, 'ForgotPassword', "Recuperar mi cuenta");
                        HeavenBook.Success('Notificación', "Enviamos una confirmación a tu correo para que realices el cambio de contraseña.");
                        //this.Redirect($("#form-User").attr('data-url'));
                    }
                    else {
                        HeavenBook.loading(false, 'ForgotPassword', 'Recuperar mi cuenta');
                        HeavenBook.Warning('Alerta', data.menssaje[0]);
                    }
                },
                error: function myfunction(xhr, status) {
                    HeavenBook.loading(false);
                    HeavenBook.Warning('Alerta', 'Encontramos un problema al comunicarnos con HeavenBook');
                }
            });
        }
        else {
            HeavenBook.loading(false, 'ForgotPassword');
            HeavenBook.Warning('Alerta', "El correo ingresado tiene formato incorrecto");
        }
    });

    if ($('#FormForgot').length > 0) {
        $('#FormForgot').validate({
            rules: {
                'User.UserPassword': { required: true, minlength: 6 },
                'RepeatUserPassword': { required: true, equalTo: "#UserPassword" }
            },
            messages: {
                'User.UserPassword': { required: 'Debe ingresar la nueva contraseña', minlength: 'La contraseña contener mas de 6 caracteres' },
                'RepeatUserPassword': { required: 'Debe ingresar la contraseña de confirmacion', equalTo: 'La contraseña de confirmacion es diferente' }
            },
            debug: true
        });
    }

    $("#BtnAceptarChange").click(function myfunction() {
        var $form = $("#FormForgot");

        if ($form.valid()) {
            $.ajax({
                url: $form.attr('action'),
                type: 'POST',
                data: $form.serialize(),
                beforeSend: function myfunction() {
                    HeavenBook.loading(true, 'BtnAceptarChange', "Registrando cambio...");
                },
                success: function myfunction(data) {
                    if (data.status) {
                        HeavenBook.loading(false, 'BtnAceptarChange', "Cambiar contraseña");
                        HeavenBook.Success('Notificación', "Tu contraseña se ha cambiado correctamente, entrando a tu panel....");
                        //this.Redirect($("#form-User").attr('data-url'));
                    }
                    else {
                        HeavenBook.loading(false, 'BtnAceptarChange', "Cambiar contraseña");
                        HeavenBook.Warning('Alerta', data.menssaje[0]);
                    }
                },
                error: function myfunction(xhr, status) {
                    HeavenBook.loading(false, 'BtnAceptarChange');
                    HeavenBook.Warning('Alerta', data.Menssaje);
                }
            });
        }
    });

    $('#btnFacebook').click(function myfunction() {
        window.location.href = $(this).attr('data-url');
    });

    $('[Name="User.Name"]').keypress(function myfunction(e) {
        return ProcessManager.OnlyNumberAndCharter(e);
    });

    $('[Name="User.LastName"]').keypress(function myfunction(e) {
        return ProcessManager.OnlyNumberAndCharter(e);
    });

    $.validator.addMethod("nameValidate", function (value, element, arg) {
        return ProcessManager.OnlyNumberAndCharter(element);
    }, 'Solo puede ingresar números y letras.');

});