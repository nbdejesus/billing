﻿var $tableCreditNote = undefined;
var $tableItems = undefined;
var itemsTemp = undefined;


$(document).ready(function myfunction() {

    callTableCreditNote();
    $tableCreditNote = $('#ItemsCreditNote').DataTable();

    //Cargamos los articulos de la factura en memoria
    if ($('#frm-credit-note').length > 0 || $('#frm-edit-credit-note').length > 0) {
        invoiceItem = window.ProcessManager.LoadForm(
            ProcessManager.getUrl('CreditNote/InvoiceItems'),
            {
                invoice: $('[name="InvoiceHeader.Id"]').val()
            }, '.panel');

        invoiceItem.then((data) => {
            itemsTemp = data;
        });
    }


    $(document).on('click', '.status', function myfunction() {
        $option = $(this);

        var question = ProcessManager.Question('Alerta', 'Está seguro de que desea ' + (($option.attr('data-status') == 'True') ? 'activar' : 'cambiar el') + ' estado a la nota de crédito?.');

        question.then((answer) => {
            if (answer) {

                $.ajax({
                    url: ProcessManager.getUrl('CreditNote/StatusCreditNote'),
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        DocumentStatusID: $option.attr('data-estado'),
                        Id: $option.attr('data-id')
                    },
                    beforeSend: function myfunction() {
                        ProcessManager.Loading(true, 'body');
                    },
                    success: function (data) {
                        if (data.status == true) {
                            ProcessManager.Success('Notificación', data.message);
                            ProcessManager.Redirect(data.redirect);
                        }
                        else {
                            ProcessManager.Warning('Alerta', data.message);
                            ProcessManager.Loading(false, 'body');
                        }
                    },
                    error: function myfunction(xhr, status) {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.validateStatusCode(xhr.status);
                    }
                });
            }
        });
    });


    /*carga el modal con las facturas*/
    $('#BtnNueva').click(function myfunction() {
        var items = ProcessManager.LoadForm(ProcessManager.getUrl('CreditNote/_Invoices'), null);

        items.then((data) => {
            ProcessManager.ShowModal("Facturas procesadas", 'icon-stack', data, false, false, 2);

            CallDinamicTable("#InvoiceList", 10);
        });
    });


    if ($('#frm-credit-note').length > 0) {
        /*agregamos el boton dinamico*/
        $('.dataTables_wrapper').prepend('<button id="btn-add-item" type="button" class="btn border-slate text-slate-800 btn-flat pull-left">' +
            ' <i class="icon-folder-plus"></i>' +
            ' </button>');
    }


    $(document).on('click', '#btn-add-item', function myfunction() {
        var items = ProcessManager.LoadForm(ProcessManager.getUrl('CreditNote/_InvoiceItems'), {
            invoice: $('[name="InvoiceHeader.Id"]').val()
        });

        items.then((data) => {
            ProcessManager.ShowModal("Artículos de la factura", 'icon-cube3', data, false, true, 3);

            //Inicializamos los componentes que utiliza el modal           
            callTableItems();
            $tableItems = $('#ItemList').DataTable();

            //checkbox
            $(".styled, .multiselect-container input").uniform({
                radioClass: 'choice'
            });

            //Agregamos el evento render en memoria
            $('#ItemList').on('draw.dt', function () {
                //Volviendo a renderizar el diseno
                $(".styled, .multiselect-container input").uniform({
                    radioClass: 'choice'
                });

                $('.checkbox').prop('checked', false).uniform('refresh');
            });
        });
    });

    $(document).on('click', '#ItemList tbody td', function () {
        //Almacenamos los productos que vamos a utilizar
        var indexColum = $tableItems.column('seleccionar:name').index();
        var indexSelectedColum = $tableItems.column('selected:name').index();

        if (this._DT_CellIndex.column == indexColum) {
            if (!ExistProduct($tableItems.column('codigo:name').data()[this._DT_CellIndex.row])) {
                $tableItems.cell(this._DT_CellIndex.row, indexSelectedColum).data($(this).find('input').hasClass('checked')).draw();
            }
            else {
                ProcessManager.Warning('Alerta', 'El producto que intenta seleccionar ya se encuentra en la nota de crédito', true);
                $(this).find('input').removeClass('checked').val(false);
                $(this).find('.checked').removeClass('checked');
            }
        }
    });

    //agregamos el o los productos a la nota de credito
    $(document).on('click', '#MdAceptar', function myfunction() {
        ProcessManager.HideModal();

        var itemsTotal = $tableItems.data().length;

        for (var index = 0; index < itemsTotal; index++) {
            if ($tableItems.cell(index, 'selected:name').data() === true) {
                $tableCreditNote.row.add([
                    $tableItems.cell(index, 'codigo:name').data(),//productCode
                    $tableItems.cell(index, 'productoId:name').data(),//productId
                    $tableItems.cell(index, 'producto:name').data(),//productName
                    $tableItems.cell(index, 'med:name').data(),//mesure
                    '<input type="text" class="form-control number inputQuantity" value="' + $tableItems.cell(index, 'quantity:name').data() + '">',//quantity                   
                    $tableItems.cell(index, 'price:name').data(),//price
                    $tableItems.cell(index, 'imp:name').data(),//tax
                    $tableItems.cell(index, 'amountTax:name').data(),//AmountTax
                    $tableItems.cell(index, 'disc:name').data(),//discount
                    $tableItems.cell(index, 'amountDisc:name').data(),//amountDiscount
                    $tableItems.cell(index, 'subTotal:name').data(),//subtotal
                    $tableItems.cell(index, 'measureId:name').data(),//measureId
                    $tableItems.cell(index, 'warehouseId:name').data(),//warehouseId
                    $tableItems.cell(index, 'taxQuantity:name').data(),//TaxQuantity
                    0,
                    '<span  class="itemRemove"> <i class="icon-trash" ></i></span >'
                ]).draw(false);
            }
        }

        calculateTotal();
    });

    ///Removiendo articulos de la nota de credito
    $(document).on('click', '.itemRemove', function eliminar() {
        var tr = $(this).parents('tr').prev('tr');

        //en caso de que la fila sobrepase el ancho de la pantalla
        if (tr.length == 0) {
            tr = $(this).parents('tr');
        }

        $tableCreditNote.row(tr).remove().draw(false);

        calculateTotal();
    });

    //campo cantidad calculo
    $(document).on('keyup', '.inputQuantity', function myfunction() {
        var quantity = $(this).val();
        var tr = $(this).parents('tr');

        var productCode = $tableCreditNote.cell(tr, 'codigo:name').data();

        updateQuantity(productCode, quantity);

        calculateTotal();
        $(this).focus();
    });


    //Metodo para regsitrar la nota de credito
    $('.save').click(function myfunction() {
        var documentStatus = $(this).attr('data-document');

        if (documentStatus == 1) {
            var question = ProcessManager.Question('Alerta', 'Desea procesar la nota de crédito? una vez procesada la nota de crédito no se podrá editar, desea continuar?');

            question.then((answer) => {
                if (answer) {
                    SaveDocument(documentStatus);
                }
            });
        }
        else {
            SaveDocument(documentStatus);
        }
    });
});



function ExistProduct(productCode) {
    //removemos el elimento de la memoria
    for (var i = 0; i < $tableItems.data().length; i++) {
        if ($tableCreditNote.cell(i, 'codigo:name').data() == productCode) {
            return true;
        }
    }
}


function calculateTotal() {
    var total = 0;
    var totalDiscunt = 0;
    var totalTax = 0;

    var itemsTotal = $tableCreditNote.data().length;

    for (var i = 0; i < itemsTotal; i++) {
        total = total + parseFloat(ProcessManager.ValueExtract($tableCreditNote.column('subTotal:name').data()[i]));
        totalDiscunt = totalDiscunt + parseFloat(ProcessManager.ValueExtract($tableCreditNote.column('amountDisc:name').data()[i]));
        totalTax = totalTax + parseFloat(ProcessManager.ValueExtract($tableCreditNote.column('amountTax:name').data()[i]));
    }

    var subTotal = total - totalDiscunt - totalTax;

    $('#subTotal').text(ProcessManager.addCommas(subTotal.toFixed(2)));
    $('#impTotal').text(ProcessManager.addCommas(totalTax.toFixed(2)));
    $('#desTotal').text(totalDiscunt.toFixed(2));
    $('#total').text(ProcessManager.addCommas((total + totalTax).toFixed(2)));
}


function GetElement(productCode, objectMemory) {
    //Obtenemos el elimento de la memoria
    for (var i = 0; i < objectMemory.length; i++) {
        if (objectMemory[i].productCode == productCode) {
            return objectMemory[i];
        }
    }
}


function SaveDocument(documentStatus) {
    if (ValidateDocument()) {
        var model = ProcessManager.FormatUrl($.param({ "CreditNote.DocumentEstatusId": documentStatus }) + '&' + $.param({ "CreditNote.": GetHeader() })) + '&' + $.param({ "CreditNote.TblCreditNoteRow": GetBody() });

        $.ajax({
            url: (($('#frm-edit-note').length > 0) ? ProcessManager.getUrl('CreditNote/UpdateCreditNote') : ProcessManager.getUrl('CreditNote/CreateCreditNote')),
            type: 'POST',
            data: model,
            cache: false,
            beforeSend: function myfunction() {
                ProcessManager.Loading(true, 'body');
            },
            success: function myfunction(data) {
                if (data.status) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.Success('Notificación', data.message);
                    ProcessManager.Redirect(data.redirect);
                }
                else {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.Warning('Alerta', data.message);
                }
            },
            error: function myfunction(xhr, status) {
                ProcessManager.Loading(false, 'body');
                ProcessManager.validateStatusCode(xhr.status);
            }
        });
    }
}


function ValidateDocument() {
    var customer = $('#CustomerId').val();
    var statusItem = true;

    for (var i = 0; i < $tableCreditNote.data().length; i++) {
        if (parseFloat(ValueExtract($tableCreditNote.column('quantity:name').data()[i])) == 0) {
            ProcessManager.Warning('Alerta', 'Los artículos no pueden tener valores en 0');
            statusItem = false;
            break;
        }
    }

    if (customer == undefined || customer == "") {
        ProcessManager.Warning('Alerta', 'Debe elegir un cliente para la nota de crédito.');
        return false;
    }
    else if ($tableCreditNote.data().length <= 0) {
        ProcessManager.Warning('Alerta', 'La nota de crédito debe tener artículos.');
        return false;
    }
    else if (!statusItem) {
        return false;
    }
    else {
        return true;
    }
}


function GetHeader() {
    var headerMemory = {
        Memory: [],
        add: function (customerId, invoiceId, documentDate, observation, subTotal, discount, totalTax, total, rowId, ncfModified) {
            var header = {
                CustomerId: customerId,
                InvoiceId: invoiceId,
                DocumentDate: documentDate,
                Observation: observation,
                SubTotal: subTotal,
                TotalDiscount: discount,
                TotalTax: totalTax,
                Total: total,
                Id: rowId,
                NCFModified: ncfModified
            };
            headerMemory.Memory.push(header);
        }
    };

    var customer = $('#CustomerId').val();
    var invoiceId = $('#InvoiceHeaderId').val();
    var creditNoteId = $('#CreditNoteId').val();
    var DocumentDate = $('#DocumentDate').val();

    var observacion = $('#Observation').text();
    var SubTotal = $('#subTotal').text();
    var Discount = $('#desTotal').text();
    var TotalTax = $('#impTotal').text();
    var Total = $('#total').text();
    var NCFModified = $('#ncfModified').text();

    //var rowId = (($('#InvoiceHeader').length > 0) ? $('#InvoiceHeader').val() : null); Pendiente:

    headerMemory.add(customer, invoiceId, DocumentDate, observacion, SubTotal, Discount, TotalTax, Total, creditNoteId, NCFModified);
    return headerMemory.Memory[0];
}


function GetBody() {
    var CreditMemory = {
        memory: [],
        add: function (productId, productCode, productName, mesure, quantity, price, tax, amountTax, discount, amountDiscount, subtotal, warehouseId, measureId, taxQuantity, index) {
            var item = {
                ProductId: productId,
                productCode: productCode,
                productName: productName,
                mesure: mesure,
                Quantity: quantity,
                Price: price,
                TaxAbbreviation: tax,
                AmountTax: amountTax,
                Discount: discount,
                AmountDiscount: amountDiscount,
                Total: subtotal,
                WarehouseId: warehouseId,
                MeasureId: measureId,
                Tax: taxQuantity,
                Id: index
            };
            CreditMemory.memory.push(item);
        }
    };

    for (var index = 0; index < $tableCreditNote.data().length; index++) {
        CreditMemory.add(
            $tableCreditNote.cell(index, 'productoId:name').data(),
            $tableCreditNote.cell(index, 'codigo:name').data(),
            $tableCreditNote.cell(index, 'producto:name').data(),
            $tableCreditNote.cell(index, 'med:name').data(),
            ValueExtract($tableCreditNote.cell(index, 'quantity:name').data()),
            $tableCreditNote.cell(index, 'price:name').data(),
            $tableCreditNote.cell(index, 'imp:name').data(),
            $tableCreditNote.cell(index, 'amountTax:name').data(),
            $tableCreditNote.cell(index, 'disc:name').data(),
            $tableCreditNote.cell(index, 'amountDisc:name').data(),
            $tableCreditNote.cell(index, 'subTotal:name').data(),
            $tableCreditNote.cell(index, 'warehouseId:name').data(),
            $tableCreditNote.cell(index, 'measureId:name').data(),
            $tableCreditNote.cell(index, 'taxQuantity:name').data(),
            $tableCreditNote.cell(index, 'index:name').data(),
        );
    }
    return CreditMemory.memory;
}

function ValueExtract(stringValue) {
    var result = stringValue.match(/\d+(\.\d{1,2})?/g);

    if (result.length > 1) {
        var val = undefined;
        for (var i = 0; i < result.length; i++) {
            val = val + result[i];
        }
        return val;
    }
    else {
        return result[0];
    }
}


function updateQuantity(productCode, quantity) {
    var itemsCreditNoteTotal = $tableCreditNote.data().length;

    var oldQuantity = 0;//buscamos la cantidad que tiene el producto en la factura
    for (var i = 0; i < itemsTemp.invoiceItems.length; i++) {
        if (itemsTemp.invoiceItems[i].code == productCode) {
            oldQuantity = parseFloat(itemsTemp.invoiceItems[i].itemQuantity);
        }
    }

    //Actualizamos los valores
    for (var i = 0; i < itemsCreditNoteTotal; i++) {
        if ($tableCreditNote.cell(i, 'codigo:name').data() == productCode) {

            //validamos que la cantidad no exeda la de la nota de credito
            if (parseFloat(quantity) > oldQuantity) {
                ProcessManager.Warning('Alerta', 'La cantidad que intenta ingresar excede la cantidad de ' + oldQuantity + ' ' + ((oldQuantity > 1) ? 'artículos' : "artículo") + ' ingresada del artículo en la factura.', true);

                //update table quantity
                $tableCreditNote.cell(i, $tableCreditNote.column('quantity:name').index()).data('<input type="text" class="form-control number inputQuantity" value="' + oldQuantity + '">').draw(true);
                $tableCreditNote.row().invalidate();
                quantity = oldQuantity;
            }

            //update memory
            $tableCreditNote.cell(i, $tableCreditNote.column('quantity:name').index()).data('<input type="text" class="form-control number inputQuantity" value="' + quantity + '">').draw();

            var amountDiscount = ProcessManager.CalculateDiscount(quantity, $tableCreditNote.column('price:name').data()[i], $tableCreditNote.column('disc:name').data()[i]);
            $tableCreditNote.cell(i, $tableCreditNote.column('amountDisc:name').index()).data(amountDiscount).draw();


            var amountTax = ProcessManager.CalculateTax(quantity, $tableCreditNote.column('price:name').data()[i], $tableCreditNote.column('disc:name').data()[i], $tableCreditNote.column('taxQuantity:name').data()[i]);
            $tableCreditNote.cell(i, $tableCreditNote.column('amountTax:name').index()).data(amountTax).draw();


            var total = ProcessManager.CalculateSubTotal(quantity, $tableCreditNote.column('price:name').data()[i]);
            $tableCreditNote.cell(i, $tableCreditNote.column('subTotal:name').index()).data(total).draw();
            $tableCreditNote.row().invalidate();
        }
    }
}


function callTableItems() {

    //Estas con las columnas que apareceran en la tabla
    var columnsTitle = [
        { title: "Código", name: "codigo" },//0
        { title: "ProductoId", name: "productoId", visible: false },//1
        { title: "Producto", name: "producto" },//2
        { title: "Med.", name: "med" },//3
        { title: "Cantidad", name: "quantity" },//4
        { title: "Precio", name: "price" },//5
        { title: "Imp.", name: "imp" },//6
        { title: "AmountTax", name: "amountTax", visible: false },//7
        { title: "Desc.", name: "disc" },//8     
        { title: "AmountDisc", name: "amountDisc", visible: false },//9
        { title: "Sub.Total", name: "subTotal" },//10
        { title: "MeasureId", name: "measureId", visible: false },//11
        { title: "WarehouseId", name: "warehouseId", visible: false },//12
        { title: "TaxQuantity", name: "taxQuantity", visible: false },//13
        { title: "Seleccionar", name: "seleccionar" },//14
        { title: "Selected", name: "selected", visible: false }//15
    ];

    CallSyncTable("#ItemList", 10, null, columnsTitle);
}



function callTableCreditNote() {

    //Estas con las columnas que apareceran en la tabla
    var columnsTitle = [
        { title: "Código", name: "codigo" },//0
        { title: "ProductoId", name: "productoId", visible: false },//1
        { title: "Producto", name: "producto" },//2
        { title: "Med.", name: "med" },//3
        { title: "Cantidad", name: "quantity" },//4
        { title: "Precio", name: "price" },//5
        { title: "Imp.", name: "imp" },//6
        { title: "AmountTax", name: "amountTax", visible: false },//7
        { title: "Desc.", name: "disc" },//8     
        { title: "AmountDisc", name: "amountDisc", visible: false },//9
        { title: "Sub.Total", name: "subTotal" },//10
        { title: "MeasureId", name: "measureId", visible: false },//11
        { title: "WarehouseId", name: "warehouseId", visible: false },//12
        { title: "TaxQuantity", name: "taxQuantity", visible: false },//13     
        { title: "IndexRow", name: "index", visible: false },//14
        { title: "Eliminar", name: "eliminar" }//15
    ];

    CallSyncTable("#ItemsCreditNote", 10, null, columnsTitle);
}