﻿var $InvoiceTable = undefined;
var $tableItems = undefined;
var $tablePendigBalance = undefined;
var PendingBalanceMemory = undefined;

$(document).ready(function myfunction() {
    //ValidationsDocument();
    $InvoiceTable = CallInvoiceTable();

    /*carga el modal con los proveedores*/
    $('#BtnNueva').click(function myfunction() {
        var items = ProcessManager.LoadForm(ProcessManager.getUrl('Invoice/_CustomersList'), null);

        items.then((data) => {
            ProcessManager.ShowModal("Clientes", 'icon-users4', data, false, false, 3);

            CallDinamicTable("#CustomerList", 10);
        });
    });

    $(document).on('click', '.status', function myfunction() {
        $option = $(this);

        var question = ProcessManager.Question('Alerta', 'Está seguro de que desea ' + (($option.attr('data-status') == 'True') ? 'activar' : 'cambiar el') + ' estado a la factura de venta?.');

        question.then((answer) => {
            if (answer) {

                $.ajax({
                    url: ProcessManager.getUrl('Invoice/SalesPointStatusInvoice'),
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        DocumentStatusID: $option.attr('data-estado'),
                        Id: $option.attr('data-id')
                    },
                    beforeSend: function myfunction() {
                        ProcessManager.Loading(true, 'body');
                    },
                    success: function (data) {
                        if (data.status == true) {
                            ProcessManager.Success('Notificación', data.message);
                            ProcessManager.Redirect(data.redirect);
                        }
                        else {
                            ProcessManager.Warning('Alerta', data.message);
                            ProcessManager.Loading(false, 'body');
                        }
                    },
                    error: function myfunction(xhr, status) {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.validateStatusCode(xhr.status);
                    }
                });
            }
        });
    });


    if ($('#frm-InvoiceCreate').length > 0 || $('#frm-InvoiceEdit').length > 0) {
        /*agregamos el boton dinamico*/
        $('.dataTables_wrapper').prepend('<button id="btn-add-item" type="button" class="btn border-slate text-slate-800 btn-flat pull-left">' +
            ' <i class="icon-folder-plus"></i>' +
            ' </button>');
    }


    $(document).on('click', '#btn-add-item', function myfunction() {
        var items = ProcessManager.LoadForm(ProcessManager.getUrl('Invoice/_ProductsList'), null);

        items.then((data) => {
            ProcessManager.ShowModal("Listado de productos", 'icon-cube3', data, false, true, 3);

            //Inicializamos los componentes que utiliza el modal           
            callTableItems();
            $tableItems = $('#ItemList').DataTable();

            //checkbox
            $(".styled, .multiselect-container input").uniform({
                radioClass: 'choice'
            });

            //Agregamos el evento render en memoria
            $('#ItemList').on('draw.dt', function () {
                //Volviendo a renderizar el diseno
                $(".styled, .multiselect-container input").uniform({
                    radioClass: 'choice'
                });

                $('.checkbox').prop('checked', false).uniform('refresh');
            });
        });
    });

    ///Removiendo articulos de la nota de credito
    $(document).on('click', '.itemRemove', function eliminar() {
        var tr = $(this).parents('tr').prev('tr');

        //en caso de que la fila sobrepase el ancho de la pantalla
        if (tr.length == 0) {
            tr = $(this).parents('tr');
        }

        $InvoiceTable.row(tr).remove().draw(false);

        CalcularMontos();
    });


    $(document).on('click', '#ItemList tbody td', function () {
        //Almacenamos los productos que vamos a utilizar
        var indexColum = $tableItems.column('seleccionar:name').index();
        var indexSelectedColum = $tableItems.column('selected:name').index();

        if (this._DT_CellIndex.column == indexColum) {
            if (!ExistProduct($tableItems.column('codigo:name').data()[this._DT_CellIndex.row])) {
                $tableItems.cell(this._DT_CellIndex.row, indexSelectedColum).data($(this).find('input').hasClass('checked')).draw();
            }
            else {
                ProcessManager.Warning('Alerta', 'El producto que intenta seleccionar ya se encuentra en la factura', true);
                $(this).find('input').removeClass('checked').val(false);
                $(this).find('.checked').removeClass('checked');
            }
        }
    });


    //Agregamos el o los productos a la factura
    $(document).on('click', '#MdAceptar', function myfunction() {
        ProcessManager.HideModal();
        var itemsTotal = $tableItems.data().length;

        for (var index = 0; index < itemsTotal; index++) {
            if ($tableItems.cell(index, 'selected:name').data() === true) {

                $InvoiceTable.row.add([
                    $tableItems.cell(index, 'codigo:name').data(),
                    $tableItems.cell(index, 'producto:name').data(),
                    $tableItems.cell(index, 'med:name').data(),
                    '<input name="inputQuantity" type="text" class="form-control number inputQuantity" value="0">',
                    '<input name="inputPrice" type="text" class="form-control number inputPrice" value="' + $tableItems.cell(index, 'price:name').data() + '">',
                    '<input name="inputDesc" type="text" class="form-control number inputDesc" value="0">',
                    $tableItems.cell(index, 'imp:name').data(),
                    0,
                    $('#Almacen :selected').text(),
                    0,
                    0,
                    $tableItems.cell(index, 'productoId:name').data(),
                    $('#Almacen :selected').val(),
                    $tableItems.cell(index, 'taxQuantity:name').data(),
                    0,
                    '<span class="itemRemove"><i class="icon-trash cursor" ></i></span>'
                ]).draw(false);
            }
        }

        CalcularMontos();
    });


    //campo price calculo
    $(document).on('keyup', '.inputDesc', function myfunction() {
        var discount = $(this).val();
        var tr = $(this).parents('tr');

        var productCode = $InvoiceTable.cell(tr, 'codigo:name').data();
        var quantity = ProcessManager.ValueExtract($InvoiceTable.cell(tr, 'quantity:name').data());
        var price = ProcessManager.ValueExtract($InvoiceTable.cell(tr, 'price:name').data());

        updateTable(productCode, quantity, price, discount);

        CalcularMontos();
        ProcessManager.focusTextToEnd(tr.find('.inputDesc'));
    });



    $(document).on('keyup', '.inputPrice', function myfunction() {
        var price = $(this).val();
        var tr = $(this).parents('tr');

        var productCode = $InvoiceTable.cell(tr, 'codigo:name').data();
        var quantity = ProcessManager.ValueExtract($InvoiceTable.cell(tr, 'quantity:name').data());
        var discount = ProcessManager.ValueExtract($InvoiceTable.cell(tr, 'discount:name').data());

        updateTable(productCode, quantity, price, discount);


        CalcularMontos();
        ProcessManager.focusTextToEnd(tr.find('.inputPrice'));
    });


    //campo cantidad calculo
    $(document).on('keyup', '.inputQuantity', function myfunction() {
        var quantity = ProcessManager.ValueExtract($(this).val());
        var tr = $(this).parents('tr');

        var productCode = $InvoiceTable.cell(tr, 'codigo:name').data();
        var price = ProcessManager.ValueExtract($InvoiceTable.cell(tr, 'price:name').data());
        var discount = ProcessManager.ValueExtract($InvoiceTable.cell(tr, 'discount:name').data());

        updateTable(productCode, quantity, price, discount);

        CalcularMontos();
        ProcessManager.focusTextToEnd(tr.find('.inputQuantity'));
    });


    $('.save').click(function myfunction() {
        var documentStatus = $(this).attr('data-document');

        if (documentStatus == 1) {
            var question = ProcessManager.Question('Alerta', 'Desea procesar la factura? una vez procesada la factura no se podrá editar, desea continuar?');

            question.then((answer) => {
                if (answer) {
                    saveInvoice(documentStatus);
                }
            });
        }
        else {
            saveInvoice(documentStatus);
        }
    });


    //////////////////////Balance pendiente/////////////////////
    $(document).on('click', '#PendigBalance', function myfunction() {

        var form = ProcessManager.LoadForm(ProcessManager.getUrl('Invoice/_BalanceList'), { customerId: $('#CustomerId').val() });

        form.then((data) => {
            ProcessManager.ShowModal("Balances por consumir", 'icon-cash2', data, true, true, 1, 'btnPendiBalance');
            callTablePendingBalance();
            $tablePendigBalance = $('#TablePendingBalance').DataTable();

            //checkbox
            $(".styled, .multiselect-container input").uniform({
                radioClass: 'choice'
            });

            //Agregamos el evento render en memoria
            $('#TablePendingBalance').on('draw.dt', function () {
                //Volviendo a renderizar el diseno
                $(".styled, .multiselect-container input").uniform({
                    radioClass: 'choice'
                });

                $('.checkbox').prop('checked', false).uniform('refresh');
            });

            CallPendingBalanceMemory();
        });
    });


    $(document).on('click', '#TablePendingBalance tbody td', function () {
        //Almacenamos los productos que vamos a utilizar
        var indexColum = $tablePendigBalance.column('seleccionar:name').index();
        var indexSelectedColum = $tablePendigBalance.column('selected:name').index();

        if (this._DT_CellIndex.column == indexColum) {
            if (!ExistPendingBalance($tablePendigBalance.column('Id:name').data()[this._DT_CellIndex.row])) {
                $tablePendigBalance.cell(this._DT_CellIndex.row, indexSelectedColum).data($(this).find('input').hasClass('checked')).draw();
            }
            else {
                ProcessManager.Warning('Alerta', 'El balance ya se encuentra en la factura de ventas', true);
                $(this).find('input').removeClass('checked').val(false);
                $(this).find('.checked').removeClass('checked');
            }
        }
    });

    //Agregamos el o los balances al total de la factura de compras
    $(document).on('click', '#btnPendiBalance', function myfunction() {
        ProcessManager.HideModal();
        var itemsTotal = $tablePendigBalance.data().length;

        for (var index = 0; index < itemsTotal; index++) {
            if ($tablePendigBalance.cell(index, 'selected:name').data() === true) {
                PendingBalanceMemory.add(
                    $tablePendigBalance.cell(index, 'Id:name').data(),
                    $tablePendigBalance.cell(index, 'total:name').data()
                );
            }
        }

        CalcularMontos();
    });

    ///Remueve el balance de la memoria
    $('#removePendigBalance').click(function myfunction() {
        PendingBalanceMemory = undefined;
        CalcularMontos();
    })

    //En caso de tener balance asignado lo cargamos a la memoria
    if ($('#frm-InvoiceEdit').length > 0 && $('#TableMemoryPendigBalance').length > 0) {
        CallPendingBalanceMemory();
        $('#TableMemoryPendigBalance tr').each(function () {
            PendingBalanceMemory.add(
                $(this).children('td').eq(0).text(),//Id
                $(this).children('td').eq(1).text() //balance
            );
        });
    }
});



function updateTable(productCode, quantity, price, discount) {
    var itemsPurchaseTotal = $InvoiceTable.data().length;

    //Actualizamos los valores
    for (var i = 0; i < itemsPurchaseTotal; i++) {
        if ($InvoiceTable.cell(i, 'codigo:name').data() == productCode) {

            //update campo
            $InvoiceTable.cell(i, $InvoiceTable.column('discount:name').index()).data('<input name="inputDesc" type="text" class="form-control number inputDesc" value="' + (discount == undefined ? 0 : discount) + '">').draw();

            //update campo
            $InvoiceTable.cell(i, $InvoiceTable.column('quantity:name').index()).data('<input name="inputQuantity" type="text" class="form-control number inputQuantity" value="' + (quantity == undefined ? 0 : quantity) + '">').draw();

            //update campo
            $InvoiceTable.cell(i, $InvoiceTable.column('price:name').index()).data('<input name="inputPrice" type="text" class="form-control number inputPrice" value="' + (price == undefined ? 0 : price) + '">').draw();

            var amountDiscount = ProcessManager.CalculateDiscount(quantity, price, discount);
            $InvoiceTable.cell(i, $InvoiceTable.column('amountDiscount:name').index()).data(isNaN(amountDiscount) ? 0 : amountDiscount).draw();

            var amountTax = ProcessManager.CalculateTaxApplied(quantity, price, discount, $InvoiceTable.cell(i, 'taxQuantity:name').data());
            $InvoiceTable.cell(i, $InvoiceTable.column('amountTax:name').index()).data(isNaN(amountTax) ? 0 : amountTax).draw();


            var total = ProcessManager.CalculateSubTotal(quantity, price);
            $InvoiceTable.cell(i, $InvoiceTable.column('total:name').index()).data(isNaN(total) ? 0 : total.toFixed(2)).draw();
            $InvoiceTable.row().invalidate();
        }
    }
}


function ExistProduct(productCode) {
    //removemos el elimento de la memoria
    for (var i = 0; i < $tableItems.data().length; i++) {
        if ($InvoiceTable.cell(i, 'codigo:name').data() == productCode) {
            return true;
        }
    }
}


function CalcularMontos() {

    valor = 0;
    tax = parseInt(0);
    desc = parseInt(0);
    impuestos = parseInt(0);
    imp = parseInt(0);
    var amountDiscount = 0;

    for (var index = 0; index < $InvoiceTable.data().length; index++) {
        tax += parseFloat($InvoiceTable.cell(index, 'amountTax:name').data());
        amountDiscount += parseFloat($InvoiceTable.cell(index, 'amountDiscount:name').data());
        valor += parseFloat($InvoiceTable.cell(index, 'total:name').data());
        desc += parseFloat(ProcessManager.ValueExtract($InvoiceTable.cell(index, 'discount:name').data()));
    }

    /*Verificamos si tiene balance pendiente*/
    CalcularPendingBalance();

    $('#subTotal').text(ProcessManager.addCommas((valor - tax).toFixed(2)));

    $('#impTotal').text(ProcessManager.addCommas(tax.toFixed(2)));

    $('#desTotal').text(ProcessManager.addCommas(amountDiscount.toFixed(2)));

    $('#total').text(ProcessManager.addCommas(((valor - amountDiscount).toFixed(2))));
}



//Este metodo agrega o envia la informacion al controller
function saveInvoice(documentStatus) {

    var model = ProcessManager.FormatUrl($.param({ "InvoiceHeader.DocumentEstatusId": documentStatus }) + '&' + $.param({ "InvoiceHeader.": GetHeader() })) + '&' + $.param({ "InvoiceHeader.TblInvoiceRow": GetBody() })
        + '&' + $.param({ "InvoiceHeader.TblCretitNoteUsed": ((PendingBalanceMemory != undefined && PendingBalanceMemory.Memory.length > 0) ? PendingBalanceMemory.Memory : null) });

    var url = (($('#frm-InvoiceEdit').length > 0) ? ProcessManager.getUrl('Invoice/UpdateInvoice') : ProcessManager.getUrl('Invoice/CreateInvoice'));


    $.ajax({
        url: url,
        type: 'POST',
        data: model,
        cache: false,
        beforeSend: function myfunction() {
            ProcessManager.Loading(true, 'body');
        },
        success: function myfunction(data) {
            if (data.status) {
                ProcessManager.Loading(false, 'body');
                ProcessManager.Success('Notificación', data.message);
                ProcessManager.Redirect(data.redirect);
            }
            else {
                ProcessManager.Loading(false, 'body');
                ProcessManager.Warning('Alerta', data.message);
            }
        },
        error: function myfunction(xhr, status) {
            ProcessManager.Loading(false, 'body');
            ProcessManager.validateStatusCode(xhr.status);
        }
    });
}


function GetHeader() {
    var headerMemory = {
        Memory: [],
        add: function (customerId, documentDate, documentNumber, expirationDate, creditTypeId, observation, subTotal, discount, totalTax, total, rowId, ncf) {
            var header = {
                CustomerId: customerId,
                DocumentDate: documentDate,
                DocumentNumber: documentNumber,
                ExpirationDate: expirationDate,
                CreditTypeId: creditTypeId,
                Observation: observation,
                Discount: discount,
                SubTotal: subTotal,
                TotalTax: totalTax,
                Total: total,
                Id: rowId,
                Ncf: ncf
            };
            headerMemory.Memory.push(header);
        }
    };

    var customerId = $('#CustomerId').val();
    var DocumentDate = $('#DocumentDate').val();
    var ExpirationDate = $('#ExpirationDate').val();
    var Id = $('#InvoiceId').val();

    var observacion = $('#Observation').text();
    var SubTotal = $('#subTotal').text();
    var Discount = $('#desTotal').text();
    var TotalTax = $('#impTotal').text();
    var Total = $('#total').text();
    var documentNumber = $('#documentNumber').text();
    var ncf = $('#ncf').val();
    var creditType = $('#CreditType :selected').val();

    headerMemory.add(customerId, DocumentDate, documentNumber, ExpirationDate, creditType, observacion, SubTotal, Discount, TotalTax, Total, Id, ncf);
    return headerMemory.Memory[0];
}



//Esta funcion asigna cada fila de la tabla a una fila de la tabla detalle
function GetBody() {

    var InvoiceMemory = {
        Memory: [],
        add: function (productId, almacenId, measure, taxAbbreviation, quantity, price, tax, amountTax, discount, amountDiscount, total, index) {
            var Row = {
                ProductId: productId,
                Measure: measure,
                WarehouseId: almacenId,
                TaxAbbreviation: taxAbbreviation,
                Quantity: quantity,
                Price: price,
                Tax: tax,
                AmountTax: amountTax,
                Discount: discount,
                AmountDiscount: amountDiscount,
                Total: total,
                Id: index
            };
            InvoiceMemory.Memory.push(Row);
        }
    };

    for (var index = 0; index < $InvoiceTable.data().length; index++) {
        InvoiceMemory.add(
            $InvoiceTable.cell(index, 'productId:name').data(),
            $InvoiceTable.cell(index, 'warehouseId:name').data(),
            $InvoiceTable.cell(index, 'medida:name').data(),
            $InvoiceTable.cell(index, 'taxAbbreviation:name').data(),
            ProcessManager.ValueExtract($InvoiceTable.cell(index, 'quantity:name').data()),
            ProcessManager.ValueExtract($InvoiceTable.cell(index, 'price:name').data()),
            $InvoiceTable.cell(index, 'taxQuantity:name').data(),
            parseFloat($InvoiceTable.cell(index, 'amountTax:name').data()).toFixed(2),
            ProcessManager.ValueExtract($InvoiceTable.cell(index, 'discount:name').data()),
            $InvoiceTable.cell(index, 'amountDiscount:name').data(),
            $InvoiceTable.cell(index, 'total:name').data(),
            $InvoiceTable.cell(index, 'index:name').data()
        );
    }
    return InvoiceMemory.Memory;
}



function CallInvoiceTable() {
    var columnsTitle = [
        { title: "Código", name: "codigo" },
        { title: "Producto", name: "articulo" },
        { title: "Med", name: "medida" },
        { title: "Cantidad", name: "quantity" },
        { title: "Precio", name: "price" },
        { title: "% Desc.", name: "discount" },
        { title: "Imp.", name: "taxAbbreviation" },
        { title: "Sub Total", name: "total" },
        { title: "Almacén", name: "almacen" },
        { title: "Taxamount", name: "amountTax", visible: false },
        { title: "AmountDisc", name: "amountDiscount", visible: false },
        { title: "ArticulosId", name: "productId", visible: false },
        { title: "AlmacenId", name: "warehouseId", visible: false },
        { title: "tax", name: "taxQuantity", visible: false },
        { title: "IndexRow", name: "index", visible: false },
        { title: "Eliminar", name: "eliminar" }
    ];

    return CallSyncTable("#InvoiceTable", 10, null, columnsTitle);
}


function callTableItems() {

    //Estas con las columnas que apareceran en la tabla
    var columnsTitle = [
        { title: "Código", name: "codigo" },
        { title: "ProductoId", name: "productoId", visible: false },
        { title: "Producto", name: "producto" },
        { title: "Servicio", name: "isService" },
        { title: "Med.", name: "med" },
        { title: "Imp.", name: "imp" },
        { title: "Precio", name: "price" },
        { title: "TaxQuantity", name: "taxQuantity", visible: false },
        { title: "Seleccionar", name: "seleccionar" },
        { title: "Selected", name: "selected", visible: false }
    ];

    CallSyncTable("#ItemList", 5, null, columnsTitle);
}




//////////////Balance pendiente///////////////////////////////////////////

function ExistPendingBalance(Id) {
    //removemos el elimento de la memoria
    if (PendingBalanceMemory.Memory.length > 0) {
        for (var i = 0; i < PendingBalanceMemory.Memory.length; i++) {
            if (PendingBalanceMemory.Memory[i].CreditNoteId == Id) {
                return true;
            }
        }
    }
}

function callTablePendingBalance() {

    //Estas con las columnas que apareceran en la tabla
    var columnsTitle = [
        { title: "No. Documento", name: "documento" },
        { title: "Fecha", name: "fecha" },
        { title: "Total", name: "total" },
        { title: "Id", name: "Id", visible: false },
        { title: "Seleccionar", name: "seleccionar" },
        { title: "Selected", name: "selected", visible: false }
    ];

    CallSyncTable("#TablePendingBalance", 5, null, columnsTitle);
}

function CalcularPendingBalance() {
    var PendingBalance = parseFloat(0);
    if (PendingBalanceMemory != undefined && PendingBalanceMemory.Memory.length > 0) {
        $('#PendingBalanceTotal').closest('tr').show();
        for (var i = 0; i < PendingBalanceMemory.Memory.length; i++) {
            PendingBalance += parseFloat(ProcessManager.ValueExtract(PendingBalanceMemory.Memory[i].Total));
        }
        $('#PendingBalanceTotal').text(PendingBalance.toFixed(2));
        return PendingBalance;
    }
    else {
        $('#PendingBalanceTotal').closest('tr').hide();
        return 0;
    }
}

function CallPendingBalanceMemory() {

    if (PendingBalanceMemory == undefined) {
        PendingBalanceMemory = {
            Memory: [],
            add: function (creditNoteId, total) {
                var balanceRow = {
                    CreditNoteId: creditNoteId,
                    Total: total
                };
                PendingBalanceMemory.Memory.push(balanceRow);
            }
        };
    }
}