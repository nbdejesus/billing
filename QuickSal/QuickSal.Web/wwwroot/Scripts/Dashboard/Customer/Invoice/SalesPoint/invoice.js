﻿var Items = {
    memory: [],
    add: function (productId, warehouseId, tax, measure, quantity, price, discount, amountDiscount, TaxAmount, subtotal, taxAbbreviation, rowId) {
        var item = {
            productId: productId,
            warehouseId: warehouseId,
            tax: tax,
            measure: measure,
            quantity: quantity,
            price: price,
            discount: discount,
            amountDiscount: amountDiscount,
            taxAmount: TaxAmount,
            subtotal: subtotal,
            taxAbbreviation: taxAbbreviation,
            rowId: rowId
        };
        Items.memory.push(item);
    }
};

$(document).ready(function myfunction() {
    $('.page-container').removeClass('page-container');
    $('.page-header').hide();
    $('.footer').hide();
    $('.page-content').addClass('page-content-full').removeClass('page-content');

    //Obtenemos los items que vamos a utilizar deacuerdo al negocio
    GetItems();

    //$('#btn-recibo').click(function myfunction() {
    //    $.ajax({
    //        url: url,
    //        method: "Post",
    //        dataType: "json",
    //        cache: false,
    //        data: $('#form-CustomerModal').serialize(),
    //        beforeSend: function myfunction() {
    //            ProcessManager.Loading(true, 'body');
    //        },
    //        success: function (data) {
    //            ProcessManager.Loading(false, 'body');
    //            if (data.status) {
    //                ProcessManager.saveByteArray(data.nombreDocumento, ProcessManager.base64ToArrayBuffer(data.archivo), data.formato);
    //            }
    //            else {
    //                ProcessManager.Warning('Alerta', "Error intentando buscar reporte");
    //            }
    //        },
    //        error: function myfunction(xhr, status) {
    //            ProcessManager.loading(false, 'body');
    //        }
    //    });
    //});

    $('#reset').click(function myfunction() {
        window.location.href = $(this).attr('data-url');
    });

    //Metodo para expandir la pantalla
    $('#btn-expand').click(function myfunction() {
        if (window.innerWidth == screen.width && window.innerHeight == screen.height) {
            //$('.page-header').show();
            //$('.footer').show();

            //$('.page-content-full').addClass('page-content').removeClass('page-content-full');
            $(this).children().addClass('icon-enlarge7').removeClass('icon-shrink7');

            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            }

        } else {
            var elem = document.documentElement;
            if (elem.requestFullscreen) {
                elem.requestFullscreen();
            } else if (elem.mozRequestFullScreen) { /* Firefox */
                elem.mozRequestFullScreen();
            } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
                elem.webkitRequestFullscreen();
            } else if (elem.msRequestFullscreen) { /* IE/Edge */
                elem.msRequestFullscreen();
            }

            $('.page-header').hide();
            $('.footer').hide();
            $('.page-content').addClass('page-content-full').removeClass('page-content');
            $(this).children().addClass('icon-shrink7').removeClass('icon-enlarge7');
        }
    });

    //Metodo para obtener el listado de productos de un almacén
    $('#warehouses').change(function myfunction() {
        var warehouseId = $('#warehouses :selected').val();

        var items = ProcessManager.LoadForm('_Products', { warehouse: warehouseId }, '.pos-list-product');

        items.then((data) => {
            $('#ProductItem').html('');
            $('#ProductItem').html(data);
        });
    });

    //Metodo para realizar busqueda de productos
    $("#searchProd").keyup(function () {
        // Retrieve the input field text
        var filter = $(this).val();

        /* if (isAjax == 'false') {*/
        // Loop through the list
        $(".product").find('.title').each(function () {
            // If the list item does not contain the text phrase fade it out
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).closest('.product').hide();
                // Show the list item if the phrase matches
            } else {
                $(this).closest('.product').show();
            }
        });
        //}
        //else {
        //    if (filter != "") {
        //        if (filter.length > 2) {
        //            $.ajax({
        //                url: ProcessManager.getUrl('Invoice/SearchProducts'),
        //                type: 'GET',
        //                data: {
        //                    warehouseId: warehouseId,
        //                    text: filter
        //                },
        //                async: true,
        //                beforeSend: function myfunction() {
        //                    ProcessManager.Loading(true, '#productList2');
        //                },
        //                success: function myfunction(data) {
        //                    ProcessManager.Loading(false, '#productList2');

        //                    var product = '';
        //                    $('#productList2').html('');
        //                    if (data.products.length > 0) {
        //                        for (var i = 0; i < data.products.length; i++) {
        //                            $('#productList2').append('<div class="col-sm-2 col-xs-4" style="display: block;">' +
        //                                '<a href="#" class="addPct" id="' + data.products[i].id + '">' +
        //                                '<div class="product color05 flat-box waves-effect waves-block">' +
        //                                '<h3 id="proname">' + data.products[i].productName + '</h3>' +
        //                                '<div class="mask">' +
        //                                '<h3>' + data.products[i].price + '</h3>' +
        //                                '<p></p>' +
        //                                '</div>' +
        //                                '</div>' +
        //                                '</a>' +
        //                                '</div>');
        //                        }
        //                    }
        //                    else {
        //                        GetItems();
        //                    }
        //                },
        //                error: function myfunction(xhr, status) {
        //                    ProcessManager.Loading(false, '#productList2');
        //                    ProcessManager.validateStatusCode(xhr.status, status);
        //                }
        //            });
        //        }
        //    }
        //    else {
        //        GetItems();
        //    }
        //}
    });

    ///Metodo para agregar un articulo
    $(document).on('click', '.product', function myfunction() {
        var productId = $(this).attr('data-id');
        var warehouseId = $('#warehouses :selected').val();


        if (!ExistProduct(productId)) {
            var item = ProcessManager.LoadForm(ProcessManager.getUrl('Invoice/_ItemInvoice'), {
                ProductId: productId,
                WarehouseId: warehouseId
            }, '#itemsTable tbody');

            item.then((data) => {
                $('#itemsTable tbody').prepend(data);
                var panelBody = $('[data-id-product="' + productId + '"]');

                var tax = panelBody.attr('data-tax');
                var measureId = panelBody.attr('data-measure');
                var price = panelBody.attr('data-price');
                var taxAbbreviation = panelBody.attr('data-tax-abbreviation');

                Items.add(productId, warehouseId, tax, measureId, 1, price, 0, 0, CalculateTax(1, price, 0, tax), price, taxAbbreviation);
                panelBody.find('.itemQuantity').val(1);

                calculateTotal();
            });
        }
        else {
            var element = GetElement(productId);

            UpdateMemory(productId,
                element.warehouseId,
                element.tax,
                element.measure,
                element.quantity + 1,
                element.discount,
                element.price);


            var panelBody = $('[data-id-product="' + productId + '"]');
            panelBody.find('.itemQuantity').val((element.quantity).toFixed(2));
        }
    });

    //Actualizamos memoria cuando se cambia el valor
    $(document).on('keyup', '.itemQuantity', function myfunction() {
        var quantity = (($(this).val() == "" ? 0 : parseFloat($(this).val()).toFixed(2)));
        var ItemPanel = $(this).closest('[data-id-product]');

        var element = GetElement(ItemPanel.attr('data-id-product'));
        UpdateMemory(element.productId,
            element.warehouseId,
            element.tax,
            element.measure,
            quantity,
            element.discount,
            element.price);
    });

    //Actualizamos memoria cuando se cambia el valor
    $(document).on('keyup', '.ItemDiscount', function myfunction() {
        var discount = (($(this).val() == "" ? 0 : parseFloat($(this).val())));
        var ItemPanel = $(this).closest('[data-id-product]');

        var element = GetElement(ItemPanel.attr('data-id-product'));
        UpdateMemory(element.productId,
            element.warehouseId,
            element.tax,
            element.measure,
            element.quantity,
            discount,
            element.price);
    });

    //Remueve un articulo de la factura
    $(document).on('click', '.icon-trash', function myfunction() {
        var ItemPanel = $(this).closest('[data-id-product]');
        RemoveMemory(ItemPanel.attr('data-id-product'));
    });

    //Cargando el tipo de crédito para el cliente
    //$('#customerSelect').change(function myfunction() {
    //    var customerId = $('#customerSelect').find('option:selected').val();

    //    if (customerId != "") {
    //        $.ajax({
    //            url: ProcessManager.getUrl('Invoice/GetCustomerData'),
    //            type: 'GET',
    //            data: {
    //                CustomerId: customerId
    //            },
    //            success: function myfunction(data) {
    //                if (data.dataCustomer.creditType != null) {
    //                    $("#CreditType").val("" + data.dataCustomer.creditType + "");
    //                }
    //                else {
    //                    $("#CreditType").val("");
    //                }

    //                if (data.dataCustomer.ncfPrefixId != null) {
    //                    $("#NcfId").val("" + data.dataCustomer.ncfPrefixId + "");
    //                }
    //                else {
    //                    $("#NcfId").val("");
    //                }
    //            },
    //            error: function myfunction(xhr, status) {
    //                ProcessManager.validateStatusCode(xhr.status, status);
    //            }
    //        });
    //    }
    //    else {
    //        $("#CreditType").val("");
    //        $("#NcfId").val("");
    //    }
    //});

    $('#CreditType').change(function myfunction() {
        var creditTypeValue = $('#CreditType :selected').val();

        if (creditTypeValue == 2) {
            $('#ExpirationDate').closest('.input-group').show();
            $(this).closest('div').addClass('col-md-6');
            $(this).closest('div').removeClass('col-md-12');
        }
        else {
            $('#ExpirationDate').closest('.input-group').hide();
            $(this).closest('div').addClass('col-md-12');
            $(this).closest('div').removeClass('col-md-6');
        }
    });

    //Metodo para registrar un cliente
    $('#AddCustomer').click(function myfunction() {
        var items = ProcessManager.LoadForm(ProcessManager.getUrl('Invoice/_Customers'), null);

        items.then((data) => {
            ProcessManager.ShowModal("Agregar cliente", 'icon-plus2', data, true, true, true);

            $('#frmCustomer').validate({
                rules: {
                    'Customer.Code': { required: true },
                    'Customer.Names': { required: true },
                    'Customer.DocumentTypeId': { required: true },
                    'Customer.DocumentNumber': { required: true },
                    'Customer.Email': { email: true }
                },
                messages: {
                    'Customer.Code': { required: 'Este campo es obligatorio' },
                    'Customer.Names': { required: 'Este campo es obligatorio' },
                    'Customer.DocumentTypeId': { required: 'Este campo es obligatorio' },
                    'Customer.DocumentNumber': { required: 'Este campo es obligatorio' },
                    'Customer.Email': { email: 'Debe ingresar el correo electrónico con el formato correcto. Por ejemplo: u@localhost.com' }
                },
                debug: true
            });
        });
    });

    //Metodo para registrar el cliente desde el modal
    $(document).on('click', '#MdAceptar', function myfunction() {
        var $form = $('#frmCustomer');

        if ($form.valid()) {
            $.ajax({
                url: ProcessManager.getUrl('Customer/CreateCustomer'),
                type: 'POST',
                data: $form.serialize(),
                beforeSend: function myfunction() {
                    ProcessManager.Loading(true, '.modal-body');
                },
                success: function myfunction(data) {
                    if (data.status) {
                        ProcessManager.Loading(false, '.modal-body');
                        ProcessManager.Success('Notificación', data.message);

                        var form = ProcessManager.LoadForm(ProcessManager.getUrl('invoice/GetCustomersName'), null, '.modal-body');

                        form.then((data) => {
                            var option = '';
                            $('#customerSelect').html('');
                            if (data.customers.length > 0) {
                                option = option + '<option value="" selected="selected">Seleccionar cliente</option>';
                                for (var i = 0; i < data.customers.length; i++) {
                                    option = option + '<option value=' + data.customers[i].id + '>' + data.customers[i].names + '</option>';
                                }
                            }
                            $('#customerSelect').append(option);  //cargamos el listado al combo
                            $('#customerSelect').val($('#customerSelect option').last().val());//Pre seleccionamos el cliente
                            $('#customerSelect').trigger("change");//Disparamos el change del select
                            ProcessManager.HideModal();
                        });
                    }
                    else {
                        ProcessManager.Loading(false, '.modal-body');
                        ProcessManager.Warning('Alerta', data.message);
                    }
                },
                error: function myfunction(xhr, status) {
                    ProcessManager.Loading(false, '.modal-body');
                    ProcessManager.validateStatusCode(xhr.status);
                }
            });
        }
        else {
            ProcessManager.Warning('Alerta', 'Faltan campos por llenar.');
        }
    });

    //Metodo para regsitrar una factura
    $('.save').click(function myfunction() {
        var documentStatus = $(this).attr('data-document');

        if (documentStatus == 1) {

            var items = ProcessManager.LoadForm(ProcessManager.getUrl('Invoice/_Payments'), null);

            items.then((data) => {
                ProcessManager.ShowModal("Pagar factura", 'icon-cash2', data, true, true, 1, "MPayment");

                //Pasamos los valores al modal
                $('#item_count').text($('#total-items').text());
                $('#twt').text($('#Total').text());
                $('#balance').text($('#Total').text());
            });

        }
        else {
            RegisterInvoice(documentStatus);
        }
    });

    //Metodo para regsitrar una factura
    $('#btn-Order').click(function myfunction() {
        var items = ProcessManager.LoadForm(ProcessManager.getUrl('Invoice/_NoteOrder'), null);

        items.then((data) => {
            ProcessManager.ShowModal("Crear Pedido", 'icon-list-unordered', data, true, true, 1, "MOrder");
            /*       global_switch_sound = new Switchery(document.querySelector('#check-order'));*/
        });
    });

    $(document).on('click', '#MOrder', function myfunction() {
        var documentStatus = $('#btn-Order').attr('data-document');
        RegisterInvoice(documentStatus);
    });


    $(document).on('click', '#MPayment', function myfunction() {
        var question = ProcessManager.Question('Alerta', 'Desea procesar la factura? una vez procesada la factura no se podrá editar, desea continuar?');

        question.then((answer) => {
            if (answer) {
                var documentStatus = $('#payment').attr('data-document');
                RegisterInvoice(documentStatus);
            }
        });
    });


    function RegisterInvoice(documentStatus) {
        /*  if (ValidateInvoice()) {*/
        var customerID = $('#customerSelect').find('option:selected').val();
        var customerName = $('#customerSelect').find('option:selected').text();

        var model = formatUrl($.param({ "InvoiceHeader.DocumentEstatusId": documentStatus }) + '&' +
            $.param({ "InvoiceHeader.": GetHeader() })) + '&' +
            $.param({ "InvoiceHeader.TblInvoiceRow": getInvoiceDetail() }) + '&' +
            formatUrl($.param({ "Payment.": PaymentManager.GetPaymentHeader() })) + '&' +
            $.param({ "Payment.TblCustomerPaymentsRow[0]": PaymentManager.GetPaymentRow(customerID, customerName) }) + '&' +
            formatUrl($.param({ "Order.": GetHeaderOrder() })) + '&' +
            $.param({ "Order.TblOrdersRow": getOrderDetail() });

        $.ajax({
            url: (($('#edit-Invoice').length > 0) ? ProcessManager.getUrl('Invoice/UpdateInvoice') : ProcessManager.getUrl('Invoice/CreateInvoice')),
            type: 'POST',
            data: model,
            cache: false,
            beforeSend: function myfunction() {
                ProcessManager.Loading(true, 'body');
            },
            success: function myfunction(data) {
                if (data.status) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.Success('Notificación', data.message);
                    ProcessManager.Redirect(data.redirect);
                }
                else {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.Warning('Alerta', data.message);
                }
            },
            error: function myfunction(xhr, status) {
                ProcessManager.Loading(false, 'body');
                ProcessManager.validateStatusCode(xhr.status);
            }
        });
        /*  }*/
    }

    //Metodo para cargar el detalle de la factura
    if ($('#edit-Invoice').length > 0) {
        var invoiceId = $('#InvoiceHeader').val();


        var item = ProcessManager.LoadForm(ProcessManager.getUrl('Invoice/_DetailInvoice'), {
            InvoiceId: invoiceId
        }, '#itemsTable tbody');

        item.then((data) => {
            $('#itemsTable tbody').append(data);
            var panelBodies = $('[data-id-product]');

            panelBodies.each(function myfunction() {
                var panelBody = $(this);

                var productId = panelBody.attr('data-id-product');
                var tax = panelBody.attr('data-tax');
                var measure = panelBody.attr('data-measure');
                var price = panelBody.attr('data-price');
                var warehouse = panelBody.attr('data-warehouse');
                var taxAbbreviation = panelBody.attr('data-tax-abbreviation');
                var quantity = parseFloat(panelBody.find('.itemQuantity').val());
                var discount = parseFloat(panelBody.attr('data-discount'));
                var rowId = panelBody.attr('data-id');

                Items.add(productId,
                    warehouse,
                    tax,
                    measure,
                    quantity,
                    price,
                    discount,
                    CalculateDiscount(quantity, price, discount),
                    CalculateTax(quantity, price, discount, tax),
                    CalculateSubTotal(quantity, price),
                    taxAbbreviation,
                    rowId);

                calculateTotal();
            });
        });
    }

    $(document).on('click', '.status', function myfunction() {
        $option = $(this);

        var question = ProcessManager.Question('Alerta', 'Está seguro de que desea ' + (($option.attr('data-status') == 'True') ? 'activar' : 'cambiar el') + ' estado a la factura de venta?.');

        question.then((answer) => {
            if (answer) {

                $.ajax({
                    url: ProcessManager.getUrl('Invoice/FormalStatusInvoice'),
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        DocumentStatusID: $option.attr('data-estado'),
                        Id: $option.attr('data-id')
                    },
                    beforeSend: function myfunction() {
                        ProcessManager.Loading(true, 'body');
                    },
                    success: function (data) {
                        if (data.status == true) {
                            ProcessManager.Success('Notificación', data.message);
                            ProcessManager.Redirect(data.redirect);
                        }
                        else {
                            ProcessManager.Warning('Alerta', data.message);
                            ProcessManager.Loading(false, 'body');
                        }
                    },
                    error: function myfunction(xhr, status) {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.validateStatusCode(xhr.status);
                    }
                });
            }
        });
    });

});

//function ValidateInvoice() {
//    var customer = $('#customerSelect').find('option:selected').val();
//    var creditType = $("#CreditType").find('option:selected').val();
//    var ExpirationDate = $('#ExpirationDate').val();
//    var DocumentDate = $('#DocumentDate').val();

//    //validamos fechas
//    var fechaActual = moment(DocumentDate, 'DD/MM/YYYY hh:mm:ss A');
//    var fechaexpiracion = moment(ExpirationDate, 'DD/MM/YYYY hh:mm:ss A');
//    var statusItem = true;
//    var mensaje = '';

//    //validamos los articulos 
//    $('.panel-body').each(function myfunction() {
//        var quantity = $(this).find('.itemQuantity');

//        if (quantity.val() == "0") {
//            statusItem = true;
//            mensaje = 'Los artículos no pueden tener valores en 0';
//            return statusItem;
//        }
//        else {
//            statusItem = false;
//            return statusItem;
//        }
//    });

//    if (customer == undefined || customer == "") {
//        ProcessManager.Warning('Alerta', 'Debe elegir un cliente para la factura.');
//        return false;
//    }
//    else if (creditType == undefined || creditType == "") {
//        ProcessManager.Warning('Alerta', 'Debe elegir un tipo de pago para factura.');
//        return false;
//    }
//    else if (fechaActual.diff(fechaexpiracion) > 0) {
//        ProcessManager.Warning('Alerta', 'La fecha de vencimiento no puede ser menor a la fecha de la factura.');
//        return false;
//    }
//    else if ($('.panel-body').length <= 0) {
//        ProcessManager.Warning('Alerta', 'La factura debe tener artículos o servicios registrados.');
//        return false;
//    }
//    else if (statusItem) {
//        ProcessManager.Warning('Alerta', mensaje);
//        return false;
//    }
//    else {
//        return true;
//    }
//}

function closeFullscreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
    }
}



function GetItems() {
    var items = ProcessManager.LoadForm('_Products', null, '.pos-list-product');

    items.then((data) => {
        $('#ProductItem').html('');
        $('#ProductItem').html(data);
    });
}


function RemoveMemory(ProducId) {
    //removemos el elimento de la memoria
    for (var i = 0; i < Items.memory.length; i++) {
        if (Items.memory[i].productId == ProducId) {
            Items.memory.splice(i, 1);
            $('[data-id-product="' + ProducId + '"]').remove();
            calculateTotal();
        }
    }
}

function UpdateMemory(ProducId, warehouseId, tax, measure, quantity, discount, price) {
    //actualizamos el elemento de la memoria
    for (var i = 0; i < Items.memory.length; i++) {
        if (Items.memory[i].productId == ProducId) {
            Items.memory[i].warehouseId = warehouseId;
            Items.memory[i].tax = tax;
            Items.memory[i].measure = measure;
            Items.memory[i].quantity = quantity
            Items.memory[i].price = price;
            Items.memory[i].discount = discount;
            Items.memory[i].amountDiscount = CalculateDiscount(Items.memory[i].quantity, Items.memory[i].price, discount);
            Items.memory[i].taxAmount = CalculateTax(Items.memory[i].quantity, Items.memory[i].price, discount, tax);
            Items.memory[i].subtotal = CalculateSubTotal(Items.memory[i].quantity, Items.memory[i].price, discount);

            //Actualizamos el panel
            var PanelItem = $('[data-id-product="' + ProducId + '"]');
            //PanelItem.find('.itemQuantity').val(quantity);
            //PanelItem.find('.ItemDiscount').val(discount.toFixed(2));
            PanelItem.find('.subtotal').text(addCommas(Items.memory[i].subtotal.toFixed(2)));

            calculateTotal();
        }
    }
}

function GetElement(ProducId) {
    //Obtenemos el elimento de la memoria
    for (var i = 0; i < Items.memory.length; i++) {
        if (Items.memory[i].productId == ProducId) {
            return Items.memory[i];
        }
    }
}

function ExistProduct(ProducId) {
    //removemos el elimento de la memoria
    for (var i = 0; i < Items.memory.length; i++) {
        if (Items.memory[i].productId == ProducId) {
            return true;
        }
    }
}

function CalculateSubTotal(quantity, price, discount) {
    var Price = parseFloat(price);
    var Quantity = parseFloat(quantity);

    return (Quantity * Price);
}

function CalculateTax(quantity, price, discount, tax) {
    var Price = parseFloat(price);
    var Quantity = parseFloat(quantity);
    var Desc = parseFloat(discount);
    var Tax = parseFloat(((tax == "") ? 0 : tax));
    var subtotal = CalculateSubTotal(Quantity, Price, Desc) - CalculateDiscount(quantity, price, discount);

    return subtotal - (subtotal / ((Tax / 100) + 1));
}

function CalculateDiscount(quantity, price, discount) {
    var Price = parseFloat(price);
    var Quantity = parseFloat(quantity);
    var Desc = parseFloat(discount);

    return (Quantity * Price) * (Desc / 100);
}


function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function calculateTotal() {
    var total = 0;
    var totalDiscunt = 0;
    var totalTax = 0;
    var totalQuantity = 0;

    for (var i = 0; i < Items.memory.length; i++) {
        total = total + parseFloat(Items.memory[i].subtotal);
        totalDiscunt = totalDiscunt + parseFloat(Items.memory[i].amountDiscount);
        totalTax = totalTax + parseFloat(Items.memory[i].taxAmount);
        totalQuantity = totalQuantity + parseFloat(Items.memory[i].quantity);
    }

    var subTotal = total - totalDiscunt - totalTax;

    $('#total-items').text(Items.memory.length + " (" + totalQuantity + ")");
    $('#Subtot').text(addCommas(subTotal.toFixed(2)));
    $('#TotalDiscount').text(addCommas(totalDiscunt.toFixed(2)));
    $('#TotalTax').text(addCommas(totalTax.toFixed(2)));
    $('#ItemsNum').text($('.panel-body').length);
    $('#Total').text(addCommas(total.toFixed(2)));
}


function formatUrl(stringUrl) {
    return stringUrl.replace(/%5B/g, '').replace(/%5D/g, '').replace(/%2F/g, '/');
}

function GetHeader() {
    var headerMemory = {
        Memory: [],
        add: function (customerId, creditTypeId, documentEstatusId, documentDate, observation, subTotal, discount, totalTax, total, expirationDate, reference, rowId, ncfId) {
            var header = {
                CustomerId: customerId,
                CreditTypeId: creditTypeId,
                DocumentEstatusId: documentEstatusId,
                DocumentDate: documentDate,
                Observation: observation,
                SubTotal: subTotal,
                Discount: discount,
                TotalTax: totalTax,
                Total: total,
                ExpirationDate: expirationDate,
                Reference: reference,
                Id: rowId,
                NcfId: ncfId
            };
            headerMemory.Memory.push(header);
        }
    };

    var customer = $('#customerSelect').find('option:selected').val();
    var creditType = $("#CreditType").find('option:selected').val();
    var ExpirationDate = $('#ExpirationDate').val();
    var DocumentDate = $('#DocumentDate').val();
    var observacion = $('#observacion').val();
    var SubTotal = $('#Subtot').text();
    var Discount = $('#TotalDiscount').text();
    var TotalTax = $('#TotalTax').text();
    var Total = $('#Total').text();
    var Reference = $('#Reference').val();
    var rowId = (($('#InvoiceHeader').length > 0) ? $('#InvoiceHeader').val() : null);
    var NcfId = $('#NcfId :selected').val();

    headerMemory.add(customer, creditType, 1, DocumentDate, observacion, SubTotal, Discount, TotalTax, Total, ExpirationDate, Reference, rowId, NcfId);
    return headerMemory.Memory[0];
}


function getInvoiceDetail() {
    var rowMemory = {
        Memory: [],
        add: function (warehouseId, productId, measure, quantity, price, tax, amountTax, discount, amountDiscount, taxAbbreviation, total, rowId, invoiceId) {
            var row = {
                Id: rowId,
                InvoiceId: invoiceId,
                WarehouseId: warehouseId,
                ProductId: productId,
                Measure: measure,
                Quantity: quantity,
                Price: price,
                Tax: tax,
                AmountTax: amountTax,
                Discount: discount,
                AmountDiscount: amountDiscount,
                TaxAbbreviation: taxAbbreviation,
                Total: total,
            };
            rowMemory.Memory.push(row);
        }
    };

    var headerId = (($('#InvoiceHeader').length > 0) ? $('#InvoiceHeader').val() : null);
    var items = Items.memory;
    for (var i = 0; i < items.length; i++) {
        rowMemory.add(
            items[i].warehouseId,
            items[i].productId,
            items[i].measure,
            items[i].quantity,
            items[i].price,
            items[i].tax,
            items[i].taxAmount,
            items[i].discount,
            items[i].amountDiscount,
            items[i].taxAbbreviation,
            items[i].subtotal,
            items[i].rowId,
            headerId
        );
    }
    return rowMemory.Memory;
}

function GetHeaderOrder() {
    var headerMemory = {
        Memory: [],
        add: function (customerId, observation, rowId) {
            var header = {
                CustomerId: customerId,
                Observation: observation,
                Id: rowId
            };
            headerMemory.Memory.push(header);
        }
    };

    var customer = $('#customerSelect').find('option:selected').val();
    var observacion = $('#noteOrder').val();
    var rowId = (($('#InvoiceHeader').length > 0) ? $('#InvoiceHeader').val() : null);

    headerMemory.add(customer, observacion, rowId);
    return headerMemory.Memory[0];
}

function getOrderDetail() {
    var rowMemory = {
        Memory: [],
        add: function (productId, measure, quantity, total, rowId) {
            var row = {
                Id: rowId,
                ProductId: productId,
                Measure: measure,
                Quantity: quantity,
                Total: total,
            };
            rowMemory.Memory.push(row);
        }
    };

    var items = Items.memory;
    for (var i = 0; i < items.length; i++) {
        rowMemory.add(
            items[i].productId,
            items[i].measure,
            items[i].quantity,
            items[i].rowId
        );
    }
    return rowMemory.Memory;
}

