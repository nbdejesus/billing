﻿(function (window) {
    var PaymentManager = function () {


        $(document).on('keyup', '#amount', function myfunction() {
            var totalPay = parseFloat(ProcessManager.ValueExtract($('#twt').text()));
            var amount = parseFloat(ProcessManager.ValueExtract($('#amount').val() == "" ? "0" : $('#amount').val()));
            var TotalInvoice = parseFloat(ProcessManager.ValueExtract($('#twt').text()));


            var totalAmount = (totalPay - amount).toFixed(2);
            if (amount > TotalInvoice) {
                $('#balance').text(0);
            }
            else {
                $('#balance').text(totalAmount);
            }

            $('#total_paying').text($('#amount').val());
        });


        this.GetPaymentHeader = function () {
            var PaymentMemory = {
                Memory: [],
                add: function (Observation, Total) {
                    var header = {
                        Observation: Observation,
                        Total: Total
                    };
                    PaymentMemory.Memory.push(header);
                }
            };

            var observacion = $('#note').val();
            var Total = parseFloat(ProcessManager.ValueExtract($('#amount').val()));

            PaymentMemory.add(observacion, Total);
            return PaymentMemory.Memory[0];
        }


        this.GetPaymentRow = function (CustomerId, CustomerName) {
            var PaymentRowMemory = {
                Memory: [],
                add: function (CustomerId, CustomerName, Concept, Owed, PaidOut, Payment, Pending) {
                    var header = {
                        CustomerId: CustomerId,
                        CustomerName: CustomerName,
                        Concept: Concept,
                        Owed: Owed,
                        Payment: Payment,
                        Pending: Pending,
                        PaidOut: PaidOut
                    };
                    PaymentRowMemory.Memory.push(header);
                }
            };

            var Concept = $('#note').val();
            var Pending = parseFloat(ProcessManager.ValueExtract($('#balance').text()));
            var Payment = parseFloat(ProcessManager.ValueExtract($('#amount').val()));
            var PaidOut = parseFloat(ProcessManager.ValueExtract($('#total_paying').text()));
            var Owed = parseFloat(ProcessManager.ValueExtract($('#balance').text()));

            PaymentRowMemory.add(CustomerId, CustomerName, Concept, Owed, PaidOut, Payment, Pending);
            return PaymentRowMemory.Memory[0];
        }

    };
    window.PaymentManager = new PaymentManager();
})(window);