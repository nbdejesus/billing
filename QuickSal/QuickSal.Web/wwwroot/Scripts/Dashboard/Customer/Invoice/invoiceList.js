﻿$(document).ready(function myfunction() {

    $(document).on('click', '.rpt-invoice', function myfunction() {

        $.ajax({
            url: ProcessManager.getUrl('Report/GetRPTReceiptSalePoint'),
            method: "GET",
            dataType: "json",
            cache: false,
            data: {
                InvoiceId: $(this).attr('data-id')
            },
            beforeSend: function myfunction() {
                ProcessManager.Loading(true, 'body');
            },
            success: function (data) {
                ProcessManager.Loading(false, 'body');
                if (data.status) {
                    printJS({ printable: data.archivo, type: 'pdf', base64: true });
                }
                else {
                    ProcessManager.Warning('Alerta', "Error intentando buscar reporte");
                }
            },
            error: function myfunction(xhr, status) {
                ProcessManager.Loading(false, 'body');
            }
        });
    });


    $(document).on('click', '.status', function myfunction() {
        $option = $(this);

        var question = ProcessManager.Question('Alerta', 'Está seguro de que desea ' + (($option.attr('data-status') == 'True') ? 'activar' : 'cambiar el') + ' estado a la factura de venta?.');

        question.then((answer) => {
            if (answer) {

                $.ajax({
                    url: ProcessManager.getUrl('Invoice/StatusInvoice'),
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        DocumentStatusID: $option.attr('data-estado'),
                        Id: $option.attr('data-id')
                    },
                    beforeSend: function myfunction() {
                        ProcessManager.Loading(true, 'body');
                    },
                    success: function (data) {
                        if (data.status == true) {
                            ProcessManager.Success('Notificación', data.message);
                            ProcessManager.Redirect(data.redirect);
                        }
                        else {
                            ProcessManager.Warning('Alerta', data.message);
                            ProcessManager.Loading(false, 'body');
                        }
                    },
                    error: function myfunction(xhr, status) {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.validateStatusCode(xhr.status);
                    }
                });
            }
        });
    });
});