﻿$(document).ready(() => {
    const action = ProcessManager.getUrl('Dashboard/GetCompras');

    buscarCompras(action);
});

buscarCompras = (action) => {
    const dashboard = new Dashboard(action);
    dashboard.buscarCompras();   
}

class Dashboard {

    constructor(action) {
        this.action = action
    }
    formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
        try {
            decimalCount = Math.abs(decimalCount);
            decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

            const negativeSign = amount < 0 ? "-" : "";

            let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
            let j = (i.length > 3) ? i.length % 3 : 0;

            return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
        } catch (e) {
            console.log(e)
        }
    }
    buscarCompras() {
        $.ajax({           
            type: "Get",
            url: this.action,
            success: (response) => {              
                let compras = this.formatMoney(response.shopping[0].compras);
                let sales = this.formatMoney(response.sales[0].sales);
                let Profits = this.formatMoney(response.profits[0].profit);
                let expenses = this.formatMoney(response.expenses[0].gasto);
                $("#PriceVendor").html('$' + compras);
                $("#PriceSales").html('$' + sales);
                $("#GetProfits").html('$' + Profits);
                $("#Getexpenses").html('$' + expenses);
                charts(response);
            }
        });
    }       
};

function charts(response) {    
    let Sales_for_moth = response.sp_Sales_For_Months;
    let Sales_for_moth_products = response.sales_For_Month_Products;
    let Sales_for_moth_sellers = response.prc_Sp_Sales_For_Month_Sellers;
    let Sales_For_Month_BranchOffices = response.sales_For_Month_BranchOffices;
    let Sales_for_Month_Customer = response.sales_for_Month_Customer;

    let mes=['x'];
    let ventas = ['Ventas'];

    let venta = ['Ventas'];
    let meses = ['x'];
    let customer=[]

    let sale_products = new Array();
    let sale_Seller = new Array();
    let BranchOffices = new Array();
    //let customer = ['x'];
      

    for (let i = 0; i < Sales_for_moth.length; i++) {

        mes.push(Sales_for_moth[i].mes);
        ventas.push(Sales_for_moth[i].ventas); 
    }    
    for (let i = 0; i < Sales_for_moth_products.length; i++) {

        let temp = new Array()
        temp.push(Sales_for_moth_products[i].name, Sales_for_moth_products[i].sales)
        sale_products.push(temp)
    }    
    for (let i = 0; i < Sales_for_moth_sellers.length; i++) {

        let temp = new Array()
        temp.push(Sales_for_moth_sellers[i].name, Sales_for_moth_sellers[i].sales)
        sale_Seller.push(temp)
    }
    for (let i = 0; i < Sales_For_Month_BranchOffices.length; i++) {

        let temp = new Array()
        temp.push(Sales_For_Month_BranchOffices[i].name, Sales_For_Month_BranchOffices[i].sales)
        BranchOffices.push(temp)
    }  
    for (let i = 0; i < Sales_for_Month_Customer.length; i++) {
        debugger
        let temp = new Array()
        let customer = ['x'];
        console.log(Sales_for_Month_Customer[i].months, Sales_for_Month_Customer[i].names, Sales_for_Month_Customer[i].sales);
        meses.push(Sales_for_Month_Customer[i].names)
        venta.push(Sales_for_Month_Customer[i].sales)
        customer.push(Sales_for_Month_Customer[i].names/*, Sales_for_Month_Customer[i].sales*/)
        //Customer.push(temp)
    }  
    // Ventas chart
    var transform = c3.generate({
        bindto: '#c3-transform',
        size: { height: 400 },
        data: {
            x: 'x',
            columns: [
                mes,
                ventas,
            ],
            type: 'bar'
        },
        color: {
            pattern: ['#00BCD4']
        },
        axis: {
            x: {
                type: 'category',
                tick: {
                    rotate: -90
                },
                height: 80
            }
        },
        grid: {
            x: {
                show: true
            }
        }
    });

    // Update data
    function update() {
        transform.transform('donut');
        setTimeout(function () {
            transform.transform('area');
        }, 1500);
        setTimeout(function () {
            transform.transform('bar', 'data1');
        }, 3000);
        setTimeout(function () {
            transform.transform('scatter');
        }, 4500);
        setTimeout(function () {
            transform.transform('bar');
        }, 6000);
        setTimeout(function () {
            transform.transform('step');
        }, 7500);
        setTimeout(function () {
            transform.transform('line');
            $('#btn-transform').removeClass('disabled');
        }, 11500);
    }

    // Run update on click
    $('#btn-transform').click(function () {
        $(this).addClass('disabled');
        update();
    });

    // Resize chart on sidebar width change
    $(".sidebar-control").on('click', function() {
        transform.resize();
       
    });
    // Generate chart
    var zoomable_chart = c3.generate({
        
        bindto: '#c3-chart-zooma',
        size: { height: 400 },
        data: {
            columns: sale_products,
            type: 'pie'
        },
        color: {
            pattern: ['#3F51B5', '#FF9800', '#4CAF50', '#00BCD4', '#F44336', '#7986CB']
        },
        zoom: {
            enabled: true
        },
        grid: {
            x: {
                show: true
            },
            y: {
                show: true
            }
        }
    });

    var zoomable_chart = c3.generate({
        bindto: '#c4-chart-zoomable',
        size: { height: 400 },
        data: {
            columns: sale_Seller,
            type: 'pie'
        },
        color: {
            pattern: ['#3F51B5', '#FF9800', '#4CAF50', '#00BCD4', '#F44336', '#7986CB']
        },
        zoom: {
            enabled: true
        },
        grid: {
            x: {
                show: true
            },
            y: {
                show: true
            }
        }
    });

    //var axis_additional = c3.generate({
    //    bindto: '#c3-axis-additional',
    //    size: { height: 400 },
    //    data: {
    //        columns: [
    //            ['data1', 30, 200, 100, 400, 150, 250],
    //            ['data2', 50, 20, 10, 40, 15, 25]
   
    //        ],
    //        axes: {
    //            data1: 'y',
    //            data2: 'y2'
    //        }
    //    },
    //    color: {
    //        pattern: ['#4CAF50', '#607D8B']
    //    },
    //    axis: {
    //        y2: {
    //            show: true
    //        }
    //    },
    //    grid: {
    //        y: {
    //            show: true
    //        }
    //    }
    //});

    var transform = c3.generate({
        bindto: '#c3-transforme',
        size: { height: 400 },
        data: {
            columns: BranchOffices,
        },
        grid: {
            y: {
                show: true
            }
        }
    });

    var transform = c3.generate({
        bindto: '#c4-transforme',
        size: { height: 400 },
        data: {
            x: 'x',
            columns: [
                meses,
                venta,
            ],
            type: 'bar'
        },
        color: {
            pattern: ['#3F51B5', '#FF9800', '#4CAF50', '#00BCD4', '#F44336', '#7986CB'],
            
        },
        axis: {
            x: {
                type: 'category',
                tick: {
                    rotate: -90
                },
                height: 80
            }
        },
        grid: {
            x: {
                show: true
            }
        }
    });
       
}

