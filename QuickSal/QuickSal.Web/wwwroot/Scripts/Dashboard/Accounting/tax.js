﻿$(document).ready(function myfunction() {

    $(document).on('click', '#BtnCreate', function myfunction() {
        var $option = $(this);
        var form = ProcessManager.LoadForm(ProcessManager.getUrl('Tax/_TaxIndex'), null);

        form.then((data) => {
            ProcessManager.ShowModal("Agregar impuesto", 'icon-plus2', data, true, true);

            ConfingForm();
        });
    });

    $(document).on('click', '.edit', function myfunction() {
        var $option = $(this);
        var form = ProcessManager.LoadForm(ProcessManager.getUrl('Tax/_EditTax'), { Id: $option.attr('data-id') });

        form.then((data) => {
            ProcessManager.ShowModal("Editar impuesto", 'icon-plus2', data, true, true);

            ConfingForm();
        });
    });

    $(document).on('click', '#MdAceptar', function myfunction() {
        var $form = ((($('#form-tax').length > 0) ? $('#form-tax') : $('#form-tax-edit')));

        if ($form.valid()) {
            $.ajax({
                url: ((($('#form-tax').length > 0) ? ProcessManager.getUrl('Tax/CreateTax') : ProcessManager.getUrl('Tax/UpdateTax'))),
                type: 'POST',
                data: $form.serialize(),
                beforeSend: function myfunction() {
                    ProcessManager.Loading(true, '.body');
                },
                success: function myfunction(data) {
                    if (data.status) {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.Success('Notificación', data.message);
                        ProcessManager.Redirect(data.redirect);
                    }
                    else {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.Warning('Alerta', data.message);
                    }
                },
                error: function myfunction(xhr, status) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.validateStatusCode(xhr.status);
                }
            });
        }
    });


    $(document).on('click', '.status', function myfunction() {
        $option = $(this);

        var question = ProcessManager.Question('Alerta', 'Está seguro de que desea ' + (($option.attr('data-status') == 'True') ? 'activar' : 'desactivar') + ' este impuesto?.');

        question.then((answer) => {
            if (answer) {

                $.ajax({
                    url: ProcessManager.getUrl('Tax/StatusTax'),
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        status: $option.attr('data-status'),
                        Id: $option.attr('data-id')
                    },
                    beforeSend: function myfunction() {
                        ProcessManager.Loading(true, 'body');
                    },
                    success: function (data) {
                        if (data.status == true) {
                            ProcessManager.Success('Notificación', data.message);
                            ProcessManager.Redirect(data.redirect);
                        }
                        else {
                            ProcessManager.Warning('Alerta', data.message);
                            ProcessManager.Loading(false, 'body');
                        }
                    },
                    error: function (xhr, status) {
                        ProcessManager.validateStatusCode(xhr.status);
                        ProcessManager.Loading(false, 'body');
                    }
                });
            }
        });
    });
});


function ConfingForm() {
    $('form').validate({
        rules: {
            'Tax.Name': { required: true },
            'Tax.Abbreviation': { required: true },
            'Tax.Quantity': { required: true }
        },
        messages: {
            'Tax.Name': { required: 'Campo requerido' },
            'Tax.Abbreviation': { required: 'Campo requerido' },
            'Tax.Quantity': { required: 'Campo requerido' }
        },
        debug: true
    });
}