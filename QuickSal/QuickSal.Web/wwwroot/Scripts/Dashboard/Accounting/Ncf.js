﻿$(document).ready(function myfunction() {

    $(document).on('click', '#BtnCreate', function myfunction() {
        var $option = $(this);

        var form = ProcessManager.LoadForm(ProcessManager.getUrl('Ncf/_NcfCreate'),null);
        form.then((data) => {       
            ProcessManager.ShowModal("Agregar Comprobante", 'icon-plus2', data, true, true);
            ConfingForm();   
        });
   
    });

    $(document).on('click', '.edit', function myfunction() {
        var $option = $(this);
        var form = ProcessManager.LoadForm(ProcessManager.getUrl('Ncf/_EditNcf'), { Id: $option.attr('data-id') });
        form.then((data) => {
            ProcessManager.ShowModal("Editar Comprobante ", 'icon-plus2', data, true, true);

            ConfingForm();
        });
    });

    $(document).on('click', '.detalles', function myfunction() {
        var $option = $(this);
        var form = ProcessManager.LoadForm(ProcessManager.getUrl('Ncf/NcfDetails'), { Id: $option.attr('data-id') });
        form.then((data) => {
            ProcessManager.ShowModal("Listado de Comprobante Generados", 'icon-plus2', data, true, true);

            CallDinamicTable('#Detail-ncf .datatable', 10);
        });
    });

    $(document).on('click', '#MdAceptar', function myfunction() {
        var $form = ((($('#form-Ncf').length > 0) ? $('#form-Ncf') : $('#form-Ncf-edit')));

        if ($form.valid()) {
            var promesa = undefined;
            if ($('#form-Ncf').length > 0) {
                promesa =  validar();
            } else {
                promesa = validarEdit();
            }
            promesa.then((data) => {
                if (!data.estatu) {
                    $.ajax({
                        url: ((($('#form-Ncf').length > 0) ? ProcessManager.getUrl('Ncf/CreateNcf') : ProcessManager.getUrl('Ncf/UpdateNcf'))),
                        type: 'POST',
                        data: $form.serialize(),
                        beforeSend: function myfunction() {
                            ProcessManager.Loading(true, '.panel');
                        },
                        success: function myfunction(data) {
                            if (data.status) {
                                ProcessManager.Loading(false, 'body');
                                ProcessManager.Success('Notificación', data.message);
                                ProcessManager.Redirect(data.redirect);
                            }
                            else {
                                ProcessManager.Loading(false, 'body');
                                ProcessManager.Warning('Alerta', data.message);
                            }
                        },
                        error: function myfunction(xhr, status) {
                            ProcessManager.Loading(false, 'body');
                            ProcessManager.validateStatusCode(xhr.status);
                        }
                    });
                }
                else {
                    if ($('#form-Ncf').length > 0) {
                        ProcessManager.Warning('Alerta', 'Los números de comprobante ya existen');
                    }
                    else {
                        ProcessManager.Warning('Alerta', 'Existen comprobantes utilizados en este rango.');
                    }
                    ProcessManager.Loading(false, 'body');
                }
            });
        }
    });
    
    $(document).on('click', '.status', function myfunction() {
        $option = $(this);

        var question = ProcessManager.Question('Alerta', 'Está seguro de que desea ' + (($option.attr('data-status') == 'True') ? 'activar' : 'desactivar') + ' este NCF?.');

        question.then((answer) => {
            if (answer) {

                $.ajax({
                    url: ProcessManager.getUrl('Ncf/StatusNcf'),
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        status: $option.attr('data-status'),
                        Id: $option.attr('data-id')
                    },
                    beforeSend: function myfunction() {
                        ProcessManager.Loading(true, 'body');
                    },
                    success: function (data) {
                        if (data.status === true) {
                            ProcessManager.Success('Notificación', data.message);
                            ProcessManager.Redirect(data.redirect);
                        }
                        else {
                            ProcessManager.Warning('Alerta', data.message);
                            ProcessManager.Loading(false, 'body');
                        }
                    },
                    error: function (xhr, status) {
                        ProcessManager.validateStatusCode(xhr.status);
                        ProcessManager.Loading(false, 'body');
                    }
                });
            }
        });
    });


});


function validar() {
    var Desde = $('#Desde').val();
    var Hasta = $('#Hasta').val();
    var Prefix = $("#Prefix option:selected").val();
    var data = { CounterFrom: Desde, CounterTo: Hasta, PrefixId: Prefix }

    return new Promise(resolve => {
        $.ajax({
            url: ProcessManager.getUrl('Ncf/ValidarForm'),
            dataType: 'JSON',
            type: 'GET',
            data: data,
            success: function (data) {
                resolve(data);
                //if (data.estatu == true) {
                //    ProcessManager.Warning('Alerta', 'Los números de comprobante ya existen');
                //    ProcessManager.Loading(false, 'body');
                //} 
                //return data.status;
            },
            error: function (xhr, status) {
                ProcessManager.validateStatusCode(xhr.status);
                ProcessManager.Loading(false, 'body');
            }
        });
    });
}


function validarEdit() {
    var Desde = $('#Desde').val();
    var Hasta = $('#Hasta').val();
    var Prefix = $("#Prefix option:selected").val();
    var data = { CounterFrom: Desde, CounterTo: Hasta, PrefixId: Prefix }

    return new Promise(resolve => {
        $.ajax({
            url: ProcessManager.getUrl('Ncf/ValidarFormEdit'),
            dataType: 'JSON',
            type: 'GET',
            data: data,
            success: function (data) {
                resolve(data);
                //if (data.estatu == true) {
                //    ProcessManager.Warning('Alerta', 'Los números de comprobante ya existen');
                //    ProcessManager.Loading(false, 'body');
                //} 
                //return data.status;
            },
            error: function (xhr, status) {
                ProcessManager.validateStatusCode(xhr.status);
                ProcessManager.Loading(false, 'body');
            }
        });
    });
}


function ConfingForm() {
  
    $('form').validate({
        rules: {
            'ncf.PrefixId': { required: true, number: true },
            'ncf.CounterFrom': { required: true, number: true },
            'ncf.CounterTo': { required: true, number: true }
        },
        messages: {
            'ncf.PrefixId': { required: 'Debe selecionar un item de la lista' },
            'ncf.CounterFrom': { required: 'El Campo es requerido', number: 'El campo solo acepta numeros' },
            'ncf.CounterTo': { required: 'El Campo es requerido', number: 'El campo solo acepta numeros' }
        },
        debug: true
    });
    $('.select').selectize({
        create:false
    });
}


