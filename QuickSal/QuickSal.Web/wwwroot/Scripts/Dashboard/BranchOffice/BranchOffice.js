﻿$(document).ready(function myfunction() {

  
    $((($('#form-BranchOffice').length > 0) ? $('#form-BranchOffice') : $('#Update-BranchOffice'))).validate({
        rules: {
            'BranchOffice.Name': { required: true },
            'BranchOffice.AddressBranchOffice': { required: true }
        },
        messages: {
            'BranchOffice.Name': { required: 'Este campo es requerido' },
            'BranchOffice.AddressBranchOffice': { required: 'Este campo es requerido' }
        },
        debug: true
    });


    $(document).on('click', '.checkbox :checkbox', function myfunction() {
        var $check = $(this);
        $('#Principal').val($check.is(':checked'));

        if ($('#Update-BranchOffice').length > 0 && !$check.is(':checked')) {
            ProcessManager.Warning('Alerta', 'El sistema necesita una sucursal principal, si remueve esta sucursal como principal el sistema elegirá una aleatoriamente.');
        }
    });

    $(document).on('click', '.status', function myfunction() {
        var status = $(this).attr('data-status');
        var id = $(this).attr('data-id');
        var question = ProcessManager.Question('Alerta', 'Está seguro de que desea ' + ((status == 'true') ? 'activar' : 'desactivar') + ' esta sucursal?.');

        question.then((answer) => {
            if (answer) {
                $.ajax({
                    url: ProcessManager.getUrl('BranchOffice/StatusBranchOffice'),
                    type: 'POST',
                    data: { branchOfficeId: id, status: status },
                    beforeSend: function myfunction() {
                        ProcessManager.Loading(true, '.panel');
                    },
                    success: function myfunction(data) {
                        if (data.status) {
                            ProcessManager.Loading(false, '.panel');
                            ProcessManager.Success('Notificación', data.message);
                            ProcessManager.Redirect(data.redirect);
                        }
                        else {
                            ProcessManager.Loading(false, '.panel');
                            ProcessManager.Warning('Alerta', data.message);
                        }
                    },
                    error: function myfunction(xhr, status) {
                        ProcessManager.Loading(false, '.panel');
                        ProcessManager.Warning('Alerta', 'Error al enviar los datos');
                    }
                });
            } else {
                HeavenBook.Info('La sucursal no se ha podido actualizar.');
            }
        });
    });

    $(document).on('click', '#BtnAceptar', function myfunction() {
        var $form = $((($('#form-BranchOffice').length > 0) ? $('#form-BranchOffice') : $('#Update-BranchOffice')));

        if ($form.valid()) {
            $.ajax({
                url: (($('#form-BranchOffice').length > 0) ? 'CreateBranchOffice' : 'UpdateBranchOffice'),
                type: 'POST',
                data: $form.serialize(),
                beforeSend: function myfunction() {
                    ProcessManager.Loading(true, '.panel');
                },
                success: function myfunction(data) {
                    if (data.status) {
                        ProcessManager.Loading(false, '.panel');
                        ProcessManager.Success('Notificación', data.message);
                        ProcessManager.Redirect(data.redirect);
                    }
                    else {
                        ProcessManager.Loading(false, '.panel');
                        ProcessManager.Warning('Alerta', data.message);
                    }
                },
                error: function myfunction(xhr, status) {
                    ProcessManager.Loading(false, '.panel');
                    ProcessManager.validateStatusCode(xhr.status);
                }
            });
        }
    });

});