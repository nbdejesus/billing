﻿var $warehouseTable = undefined;
$(document).ready(function () {
    CallPriceTable();
    DesabilitarCampos();



    ///Este método cargar un datatable para registrar información y para cargar información registrada
    function CallPriceTable() {

        //Columnas que tiendrá el datatable
        var columnsTitle = [
            { title: "Id" },
            { title: "ProductId" },
            { title: "Nombre Producto" },
            { title: "Cantidad" },
            { title: "Estado" },
            { title: "Eliminar" },
            { title: "Editar" }

        ];

        var columnVisible = [
            { "visible": false, "targets": 0 },
            { "visible": false, "targets": 1 }
        ];

        //Valida si el formulario es de editar o de crear
        if ($("#movingId").val() != "") {
            var form = ProcessManager.LoadForm(ProcessManager.getUrl('MovingWare/DetailsRows'), { MovingId: $("#movingId").val() });//Url que hace la llamada

            form.then((data) => {
                $warehouseTable = $('#PriceTable').DataTable({
                    data: data.prices,//Array que viene desde el controller
                    columnDefs: columnVisible,
                    columns: columnsTitle,
                    language: {
                        processing: "Procesando",
                        search: "Buscar:",
                        lengthMenu: "Ver _MENU_ Filas",
                        info: "_START_ - _END_ de _TOTAL_ elementos",
                        infoEmpty: "0 - 0 de 0 elementos",
                        infoFiltered: "(Filtro de _MAX_ entradas en total)",
                        loadingRecords: "Cargando datos.",
                        zeroRecords: "No se encontraron datos",
                        emptyTable: "No hay datos disponibles",
                        paginate: {
                            first: "Primero",
                            previous: "Anterior",
                            next: "Siguiente",
                            last: "Ultimo"
                        },
                        aria: {
                            sortAscending: ": activer pour trier la colonne par ordre croissant",
                            sortDescending: ": activer pour trier la colonne par ordre décroissant"
                        }
                    }
                });
            });
        }
        else {
            $warehouseTable = $('#PriceTable').DataTable({
                data: null,
                columnDefs: columnVisible,
                columns: columnsTitle,
                language: {
                    processing: "Procesando",
                    search: "Buscar:",
                    lengthMenu: "Ver _MENU_ Filas",
                    info: "_START_ - _END_ de _TOTAL_ elementos",
                    infoEmpty: "0 - 0 de 0 elementos",
                    infoFiltered: "(Filtro de _MAX_ entradas en total)",
                    loadingRecords: "Cargando datos.",
                    zeroRecords: "No se encontraron datos",
                    emptyTable: "No hay datos disponibles",
                    paginate: {
                        first: "Primero",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Ultimo"
                    },
                    aria: {
                        sortAscending: ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    }
                }
            });
        }
    }

    // Single picker
    $('.date').daterangepicker({
        singleDatePicker: true
    });

    $(document).on('click', '.Editar', function myfunction() {
        var $option = $(this);
        var wareId = $('[data-wareHouses]').children(':selected').val();
        var nameProduct = $(this).attr('data-product');


        var form = ProcessManager.LoadForm(ProcessManager.getUrl('MovingWare/_EditProduct'), { MovingId: $(this).attr("data-id"), ProductId: $(this).attr("data-product") });


        form.then((data) => {
            ProcessManager.ShowModal("Modificar Producto", 'icon-plus2', data, true, false);
            $('.modal-footer .dropdown-toggle').remove();
            var estado = $("#stateMoving").val();
            var sender = $("#senderValue").val();

            if (estado ===2 && sender ==="") {

                $(".modal-footer").append('<div class=\"btn-group\">' +
                    '<button type =\"button\" class=\"btn bg-teal-400 btn-labeled dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">' +
                    '<b><i class=\"icon-menu7\"></i></b> Accion <span class=\"caret\"></span></button>' +
                    '<ul class=\"dropdown-menu dropdown-menu-right\">' +
                    '<li><a id=\"retornar\" data-id=\"@(Convert.ToInt16(QuickSal.DataAcces.QuickSalEnum.MovingStatus.Returned))\"><i class=\"icon-plus2\" id=\"Enviar\"></i></i> Devolver</a ></li </ul></div> ');
            }
            else if (estado ===1) {
                $(".modal-footer").append('<div class=\"btn-group\">' +
                    '<button type =\"button\" class=\"btn bg-teal-400 btn-labeled dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">' +
                    '<b><i class=\"icon-menu7\"></i></b> Accion <span class=\"caret\"></span></button>' +
                    '<ul class=\"dropdown-menu dropdown-menu-right\">' +
                    '<li><a id="MdAceptar"><i class=\"icon-plus2\"></i> Modificar</a></li >');

            }

     
            ConfingForm();
            AllProducts(wareId, nameProduct);
        });

    });

    function ConfingForm() {
        $('form').validate({
            rules: {
                'Product.Name': { required: true }
            },
            messages: {
                'Product.Name': { required: 'Campo requerido' }
            },
            debug: true
        });
    }


    function AllProducts(WareHouseId, ProductId) {
        if (WareHouseId > 0) {


            var form = window.ProcessManager.LoadForm(
                ProcessManager.getUrl('MovingWare/GetProducts'),
                {
                    WareHouseId: WareHouseId
                }, '.panel');

            form.then((data) => {
                var option = '';
                $('[data-productId]').html('');
                if (data.length > 0) {
                    option = option + '<option value="">Seleccionar....</option>';
                    for (var i = 0; i < data.length; i++) {
                        option = option + '<option value=' + data[i].productId + '>' + data[i].product + '</option>';
                    }
                    DesabilitarCamposRow();
                }
                $('[data-productId]').append(option);

                $('[data-productId]').val(ProductId);
            });

        }

    }

    //Metodo para eliminar un precio
    $(document).on('click', '.deleteRow', function myfunction() {
        //Código para levanter instancia en memoria de la tabla.
        //var $warehouseTable = CallDinamicTable('#PriceTable', 10, false);

        ///Código para eliminar los rows de la tabla////////
        var tes = $(this).closest('tr');
        $warehouseTable.row($(this).closest('tr'))
            .remove()
            .draw();
        /////////Elimina los rows en la base de datos////////////
        $.ajax({
            url: ProcessManager.getUrl('MovingWare/DeleteDetailsRow'),
            type: 'POST',
            data: { MovingRowId: $(this).attr('data-id') },
            beforeSend: function myfunction() {
                ProcessManager.Loading(true, 'body');
            },
            success: function myfunction(data) {
                ProcessManager.Loading(false, 'body');

            }
        });
    });

    $(document).on('change', '[data-productId]', function myfunction() {
        if ($(this).val() != "") {

            var wareHouseId = $(this).val();
            var form = window.ProcessManager.LoadForm(
                ProcessManager.getUrl('MovingWare/GetProducts'),
                {
                    ProductId: $(this).children(':selected').val()
                }, '.panel');

            form.then((data) => {
                var option = '';
                document.getElementById('disponible').value = "";
                if (data.length > 0) {
                    document.getElementById('disponible').value = data[0].amount;
                    $('#nameProduct').val(data[0].product);



                }
            });
        }
    });

    $(document).on('click', '#MdAceptar', function myfunction() {
        var $form = $('#form-EditProduct');

        var model = $form.serialize();

        $.ajax({
            url: ProcessManager.getUrl('MovingWare/UpdateDetailsRow'),
            type: 'POST',
            data: model,
            beforeSend: function myfunction() {
                ProcessManager.Loading(true, 'body');
            },
            success: function myfunction(data) {
                if (data.status) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.Success('Notificación', data.message);
                    ProcessManager.Redirect(data.redirect);
                    Limpiar();
                }
                else {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.Warning('Alerta', data.message);
                }
            },
            error: function myfunction(xhr, status) {
                ProcessManager.Loading(false, 'body');
                ProcessManager.validateStatusCode(xhr.status);
            }
        });

    });

    function DesabilitarCampos() {
        var estado = $("#stateMoving").val();

        if (estado ===1) {
            document.getElementById('comentario').removeAttribute('readonly');
            document.getElementById('sender').removeAttribute('disabled');
            document.getElementById('receiver').removeAttribute('disabled');
        }
    }


    function DesabilitarCamposRow() {
        var estado = $("#stateMoving").val();
        if (estado ===1) {
            document.getElementById('amount').removeAttribute('readonly');
            document.getElementById('description').removeAttribute('readonly');
            document.getElementById('producto').removeAttribute('disabled');
        }
    }

    $(document).on('click', "#retornar", function () {
        document.getElementById('producto').removeAttribute('disabled');

        var estado = $(this).attr('data-id');

        var $form = $('form');

        var model = $form.serialize() + '&' + $.param({ "estado": estado });

        $.ajax({
            url: ProcessManager.getUrl('MovingWare/ReturnProduct'),
            type: 'POST',
            data: model,
            beforeSend: function myfunction() {
                ProcessManager.Loading(true, 'body');
            },
            success: function myfunction(data) {
                if (data.status) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.Success('Notificación', data.message);
                    ProcessManager.Redirect(data.redirect);

                }
                else {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.Warning('Alerta', data.message);
                }
            },
            error: function myfunction(xhr, status) {
                ProcessManager.Loading(false, 'body');
                ProcessManager.validateStatusCode(xhr.status);
            }
        });

    });

});

