﻿$(document).ready(function () {
  
    warehouse();
    var $warehouseTable = CallDinamicTable('#TableMoving', 10, false);

    $(document).on('change', '[data-wareHouse]', function myfunction() {
        if ($(this).val() !== "") {         
            var wareHouseId = $(this).val();
            var form = window.ProcessManager.LoadForm(
                ProcessManager.getUrl('MovingWare/GetProducts'),
                {
                    WareHouseId: $(this).children(':selected').val()
                }, '.panel');

            form.then((data) => {
                var option = '';
                $('[data-product]').html('');
                if (data.length > 0) {
                    option = option + '<option value="" selected="selected">Selecionar...</option>';
                    for (var i = 0; i < data.length; i++) {
                        option = option + '<option value=' + data[i].productId + '>' + data[i].product + '</option>';
                    }
                }
              
                $('[data-product]').append(option);

                $('[data-product]').select2({
                    create: false,
                    sortField: {
                        field: 'text',
                        direction: 'asc'
                    }
                   
                });
              
            });
        }
    });


    $(document).on('change', '[data-product]', function myfunction() {
        if ($(this).val() !== "") {
          
            var wareHouseId = $(this).val();
            var form = window.ProcessManager.LoadForm(
                ProcessManager.getUrl('MovingWare/GetProducts'),
                {
                    ProductId: $(this).children(':selected').val(),
                    WareHouseId: $('[data-wareHouse]').children(':selected').val()
                }, '.panel');

            form.then((data) => {
                var option = '';
                document.getElementById('disponible').value = "";
                if (data.length > 0) {
                    document.getElementById('disponible').value = data[0].amount;
                }
            });
        }
    });

    /////////////////////////Agrega los productos parcial a la tabla en la vista///////////////
    $('#agregarProducto').click(function myfunction() {
        //Código para validar y agregar filas a una tabla
        var states = MovingWareValidations();
        if (states[1]) {
            var Quantity = $('#cantidad').val();
            var ProductId = $('#producto :selected').val();
            var ProductName = $('#producto :selected').text();


            $warehouseTable.row.add([
                ProductName,
                Quantity,
                '<button type="button" class="btn btn-default btn-icon deleteRowWarehouse"><i class="icon-trash"></i></button>',
                ProductId //ProductId value
            ]).draw(false);

            //Limpiamos los campos
            $('#producto,#cantidad,#disponible').val('');
        }

        else {
            ProcessManager.Warning('Alerta', states[0]);
        }
    });

    ///////////////envia los datos hacia el controlador para crear el movimiento/////////////
    $("#Enviar").click(function () {
        var tipo = $(this).attr('data-id');
        CrearMovimiento(tipo);
    });

    $("#Procesar").click(function () {
        var tipo = $(this).attr('data-id');
        CrearMovimiento(tipo);
    });

    function CrearMovimiento(tipoProcesos) {
        var $form = $('form');

        var model = $form.serialize() + '&' + $.param({ "tipoProceso": tipoProcesos }) + '&' + $.param({ "MovingWarehouse.TblMovingWarehouseRow": ConvertArraryToObjectWareHouse($warehouseTable.data().toArray()) });

        $.ajax({
            url: (($('#Edit-moving').length > 0) ? ProcessManager.getUrl('MovingWare/UpdateMovingWare') : ProcessManager.getUrl('MovingWare/CreateMovingWareSave')),
            type: 'POST',
            data: model,
            beforeSend: function myfunction() {
                ProcessManager.Loading(true, 'body');
            },
            success: function myfunction(data) {
                if (data.status) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.Success('Notificación', data.message);
                    ProcessManager.Redirect(data.redirect);
                    Limpiar();
                }
                else {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.Warning('Alerta', data.message);
                }
            },
            error: function myfunction(xhr, status) {
                ProcessManager.Loading(false, 'body');
                ProcessManager.validateStatusCode(xhr.status);
            }
        });
    };


    //Metodo para eliminar un precio
    $(document).on('click', '.deleteRowWarehouse', function myfunction() {
        //Código para levanter instancia en memoria de la tabla.


        $warehouseTable.row($(this).parents('tr'))
            .remove()
            .draw();

    });
    function warehouse() {
      
        ///////////LLena el Select de donde saldran los porductos///////
        var form = window.ProcessManager.LoadForm(
            ProcessManager.getUrl('MovingWare/GetAllWarehouse'));

        form.then((data) => {
            var option = '';
            $('[data-wareHouse]').html('');
            if (data.data.length > 0) {
                option = option + '<option value="" selected="selected"> Selecionar...</option>';
                for (var i = 0; i < data.data.length; i++) {
                    option = option + '<option value=' + data.data[i].id + '>' + data.data[i].name + '</option>';
                }
            }
            $('[data-wareHouse]').append(option);
            $('#warehouse').selectize({
              
            });
            console.log("hola");
         
        });      
    }

});

function Limpiar() {
    $('#TableMoving tbody').children().remove();
    $("#warehouse").val('').trigger('change');
    $("#allWarehouse").val('').trigger('change');
    $("#producto").val('').trigger('change');
    document.getElementById("disponible").innerHTML = "";
    document.getElementById("cantidad").innerHTML = "";
    document.getElementById("descripcion").innerHTML = "";


};
function MovingWareValidations() {
    var disponible = $("#disponible").val();
    var descripcion = $("#descripcion").val();
    var cantidad = $("#cantidad").val();
    var desde = $("#warehouse").children(':selected').val();
    var hacia = $("#allWarehouse").children(':selected').val();
    var producto = $('#producto :selected').val();
    var validateProduct = ValidarProducto();

    var state = true;
    var message = "";
  
    if (parseInt(cantidad) > parseInt(disponible)) {
         message = "La cantida es mayor a la disponible";
        state = false;

        return [message, state];
    }

   else if (cantidad ==="") {
         message = "Debe agregar la cantidad";
        state = false;

        return [message, state];
    }
    else if (desde === hacia) {
       
         message = "Debe elegir otro Almacen de destino";
        state = false;

        return [message, state];

    }
    else if (hacia === "") {

        message = "Debe elegir otro Almacen de destino";
        state = false;

        return [message, state];

    }
    else if (producto ==="") {
        message = "Debe elegir un Producto";
        state = false;

        return [message, state];
    }


    else if (!validateProduct) {
        message = "Este producto ya esta agregado";
        state = false;
        return [message, state];
    }

    else if (descripcion ==="" ) {
        message = "Debe Agregar Una Descripcion";
        state = false;
        return [message, state];
    }

    else {

        return [message, state];
    }
       
}

//Convirtiendo el array
function ConvertArraryToObjectWareHouse(array) {
    var WarehouseMemory = {
        Memory: [],
        add: function (ProductName, ProductId, Amount) {
            var product = {
                ProductName: ProductName,
                ProductId: ProductId,
                Amount: Amount
            };
            WarehouseMemory.Memory.push(product);
        }
    };

    for (var i = 0; i < array.length; i++) {
        WarehouseMemory.add(array[i][0], array[i][3], array[i][1]);
    }

    return WarehouseMemory.Memory;
}

//////Valida los producto agragados en la tabla///////////////////

function ValidarProducto() {

    var tableReg    = document.getElementById('TableMoving');
    var searchText  = $('#producto :selected').text();
    var cellsOfRow  = "";
    var compareWith = "";

    for (var i = 1; i < tableReg.rows.length; i++) {
        cellsOfRow = tableReg.rows[i].getElementsByTagName('td');


        for (var j = 0; j < cellsOfRow.length; j++) {
            compareWith = cellsOfRow[j].innerHTML;
            // Buscamos el texto en el contenido de la celda
            if (searchText.length ===0 || (compareWith.indexOf(searchText) > -1)) {
                return false;
            }

            else
                return true;
        }
    }
}

$('.changeStatu').click(function myfunction() {

    var form = window.ProcessManager.LoadForm(
        ProcessManager.getUrl('MovingWare/ChangeStatu'),
        {
            MovingWarehouseId: $(this).attr('data-id'),
            Estado: $(this).attr('data-estado')
        }, '.panel');

    form.then((data) => {
        location.reload();
    });
});





