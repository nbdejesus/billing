﻿$(document).on('click', '.callModal', function myfunction() {
    var BusinessId = document.getElementById("businessId").innerHTML;
    var form = ProcessManager.LoadForm($(this).attr('data-url'),
        {
            businessId: BusinessId
        }, '.panel');
    form.then((data) => {
        ProcessManager.ShowModal($(this).attr('data-name'), 'icon-plus2', data, false, false,2);
        
        $('.modal-footer #btnCancel').hide();
        $('.modal-footer #MdAceptar').hide();

        //$(".modal-footer").append('<div class="panel-foote">' +
        //    '<button type=\"button\" id="Cancelar" data-dismiss=\"modal\" class="\btn border-slate text-slate-800 btn-flat back\">Cancelar</button>' +
        //    //'<button id=\"Agregar\" type=\"submit\" class=\"btn btn-primary btnGenerar\">Generar</button></div>'
        //'<div class=\"btn-group\">' +
        //    '<button type=\"button\" class=\"btn bg-teal-400 btn-labeled dropdown-toggle\"  ><b><i class=\"icon-menu7\"></i></b> Generar</button>' +' </div>');

        // Single picker
        $('.date').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
    });
    $('.modal-footer .panel-foote').remove();
   
});

$(document).on('click', '.Aceptar', function myfunction() {
    var url = $('#form-CustomerModal')[0].action;
    $("#tipoDocumento").val($(this).attr('data-id'));
    $("#Generar").click();

    $.ajax({
        url: url,
        method: "Post",
        dataType: "json",
        cache: false,
        data: $('#form-CustomerModal').serialize(),
        beforeSend: function myfunction() {
            ProcessManager.Loading(true, '.modal-content');
        },
        success: function (data) {
            ProcessManager.Loading(false, '.modal-content');
            if (data.status) {
                //ProcessManager.saveByteArray(data.nombreDocumento, ProcessManager.base64ToArrayBuffer(data.archivo), data.formato);
                printJS({ printable: data.archivo, type: 'pdf', base64: true });
            }
            else {
                ProcessManager.Warning('Alerta', "Error intentando buscar reporte");
            }
        },
        error: function myfunction(xhr, status) {
            ProcessManager.loading(false, 'body');
        }
    });
});
