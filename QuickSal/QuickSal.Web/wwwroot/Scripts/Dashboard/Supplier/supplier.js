﻿$(document).ready(function myfunction() {

    if ($('[name="Supplier.DocumentTypeId"]').length > 0) {
        ProcessManager.getDocumentTypeConfiguration();
    }

    $('form').validate({
        rules: {
            'Supplier.Code': { required: true },
            'Supplier.Names': { required: true },
            'Supplier.DocumentTypeId': { required: true },
            'Supplier.DocumentNumber': { required: true },
            'Supplier.Email': { email: true }
        },
        messages: {
            'Supplier.Code': { required: 'Este campo es obligatorio' },
            'Supplier.Names': { required: 'Este campo es obligatorio' },
            'Supplier.DocumentTypeId': { required: 'Este campo es obligatorio' },
            'Supplier.DocumentNumber': { required: 'Este campo es obligatorio' },
            'Supplier.Email': { email: 'Debe ingresar el correo electrónico con el formato correcto. Por ejemplo: u@localhost.com' }
        },
        debug: true
    });


    $('#BtnAceptar').click(function myfunction() {
        var $form = $('form');

        if ($form.valid()) {
            $.ajax({
                url: $('#Edit-supplier').length > 0 ? ProcessManager.getUrl('supplier/UpdateSupplier') : ProcessManager.getUrl('supplier/CreateSupplier'),
                type: 'POST',
                data: $form.serialize(),
                beforeSend: function myfunction() {
                    ProcessManager.Loading(true, 'body');
                },
                success: function myfunction(data) {
                    if (data.status) {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.Success('Notificación', data.message);
                        ProcessManager.Redirect(data.redirect);
                    }
                    else {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.Warning('Alerta', data.message);
                    }
                },
                error: function myfunction(xhr, status) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.validateStatusCode(xhr.status);
                }
            });
        }
        else {
            ProcessManager.Warning('Alerta', 'Faltan campos por llenar.');
        }
    });


    $(document).on('click', '.status', function myfunction() {
        $option = $(this);

        var question = ProcessManager.Question('Alerta', 'Está seguro de que desea ' + (($option.attr('data-status') ==='True') ? 'activar' : 'desactivar') + ' este proveedor?.');

        question.then((answer) => {
            if (answer) {

                $.ajax({
                    url: ProcessManager.getUrl('supplier/StatusSupplier'),
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        status: $option.attr('data-status'),
                        Id: $option.attr('data-supplier')
                    },
                    beforeSend: function myfunction() {
                        ProcessManager.Loading(true, 'body');
                    },
                    success: function (data) {
                        if (data.status ===true) {
                            ProcessManager.Success('Notificación', data.message);
                            ProcessManager.Redirect(data.redirect);
                        }
                        else {
                            ProcessManager.Warning('Alerta', data.message);
                            ProcessManager.Loading(false, 'body');
                        }
                    },
                    error: function myfunction(xhr, status) {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.validateStatusCode(xhr.status);
                    }
                });
            }
        });
    });


    $(document).on('click', '#btnPhotoProfile', function myfunction() {
        $('#PhotosUpload').trigger('click');
    });

    function uploadProgressHandler(event) {
        $("#loaded_n_total").html("Uploaded " + event.loaded + " bytes of " + event.total);
        var percent = (event.loaded / event.total) * 100;
        var progress = Math.round(percent);
        $("#btnPhotoProfile").attr('disabled', true);
        $("#btnPhotoProfile").html("<i class=\"icon-file-upload position-left\"></i> Porcentage cargado: " + progress + "%");
    }

    function loadHandler(event) {
        $("#status").html(event.target.responseText);
        $("#btnPhotoProfile").html("<i class=\"icon-camera position-left\"></i> Cargar imagen");
        $("#btnPhotoProfile").attr('disabled', false);
    }

    function errorHandler(event) {
        $("#status").html("Upload Failed");
    }

    function abortHandler(event) {
        $("#status").html("Upload Aborted");
    }

    $(document).on('change', '#PhotosUpload', function myfunction() {
        var srcActual = $('#Picture-Account').attr("src");
        var $form = $("#Picture-Profile");

        if (ProcessManager.ValidateImage($('#PhotosUpload'))) {
            event.preventDefault();

            var formData = new FormData(document.getElementById("Picture-Profile"));

            $.ajax({
                url: ProcessManager.getUrl('supplier/SaveUploadedPicture'),
                method: 'POST',
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                beforeSend: function myfunction() {
                    $('#Picture-Account').attr("src", ProcessManager.GIF);
                },
                success: function (data) {
                    if (data.status ===true) {
                        $('#Picture-Account').attr("src", data.file);
                        $('#picture').val(data.fileName);
                    }
                    else {
                        ProcessManager.Warning('Alerta', data.message);
                        $('#Picture-Account').attr("src", srcActual);
                        $('#picture').val('');
                    }
                },
                error: function (xhr, status) {
                    ProcessManager.Loading(false, 'body');
                    ProcessManager.validateStatusCode(xhr.status);
                    $('#Picture-Account').attr("src", srcActual);
                    $('#picture').val('');
                },
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress",
                        uploadProgressHandler,
                        false
                    );
                    xhr.addEventListener("load", loadHandler, false);
                    xhr.addEventListener("error", errorHandler, false);
                    xhr.addEventListener("abort", abortHandler, false);
                    return xhr;
                }
            });
        }
    });


});

function soloNumeros(e) {
    var key = window.Event ? e.which : e.keyCode;
    return ((key >= 48 && key <= 57) || (key === 8));
}
