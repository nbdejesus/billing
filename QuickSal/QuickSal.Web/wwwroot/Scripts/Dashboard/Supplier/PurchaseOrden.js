﻿var $PurchaseTable = undefined;
var $tableItems = undefined;
var $tablePendigBalance = undefined;
var PendingBalanceMemory = undefined;

$(document).ready(function myfunction() {

    ///////////////////////////////////Orden de compras/////////////////////////////////////////////////
    $PurchaseTable = CallPurchaseTable();

    /*carga el modal con los proveedores*/
    $('#BtnNueva').click(function myfunction() {
        var items = ProcessManager.LoadForm(ProcessManager.getUrl('PurchaseOrders/_Suppliers'), null);

        items.then((data) => {
            ProcessManager.ShowModal("Suplidores", 'icon-users4', data, false, false, 3);

            CallDinamicTable("#SuppliersList", 10);
        });
    });

    $(document).on('click', '.status', function myfunction() {
        $option = $(this);

        var question = ProcessManager.Question('Alerta', 'Está seguro de que desea ' + (($option.attr('data-status') ==='True') ? 'activar' : 'cambiar el') + ' estado a la Orden?.');

        question.then((answer) => {
            if (answer) {

                $.ajax({
                    url: ProcessManager.getUrl('PurchaseOrders/StatusOrden'),
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        DocumentStatusID: $option.attr('data-estado'),
                        Id: $option.attr('data-id')
                    },
                    beforeSend: function myfunction() {
                        ProcessManager.Loading(true, 'body');
                    },
                    success: function (data) {
                        if (data.status ===true) {
                            ProcessManager.Success('Notificación', data.message);
                            ProcessManager.Redirect(data.redirect);
                        }
                        else {
                            ProcessManager.Warning('Alerta', data.message);
                            ProcessManager.Loading(false, 'body');
                        }
                    },
                    error: function myfunction(xhr, status) {
                        ProcessManager.Loading(false, 'body');
                        ProcessManager.validateStatusCode(xhr.status);
                    }
                });
            }
        });
    });


    //Este metodo agrega las filas a la tabla
    $('#BtnAddPrice').click(function myfunction() {
        if (PurchaseValidations()) {
            //Campos desde donde pasaras los valores
            var canti = $('#cantidad').val();
            cantidad = parseFloat(canti).toFixed(2);
            var comp = $('#compra').val();
            compra = parseFloat(comp).toFixed(2);
            var descuen = $('#descuento').val();
            if (descuen ==="") {
                descuento = parseInt(0);
            } else {
                descuento = parseFloat(descuen).toFixed(2);
            }
            var Impuesto = $('#Impuesto :selected').text();
            var ImpuestoV = $('#Impuesto :selected').attr('data-quantity');
            ImpuestoValue = parseFloat(ImpuestoV).toFixed(2);
            var Articulos = $('#ArticulosId :selected').text();
            var ArticulosId = $('#ArticulosId :selected').val();
            var MeasureId = $('#ArticulosId :selected').attr('data-quantity');
            var Almacen = $('#AlmacenId :selected').text();
            var AlmacenId = $('#AlmacenId :selected').val();
            var subTotal = (parseFloat(cantidad) * parseInt(compra)).toFixed(2);

            var AmountTax = (((parseFloat(cantidad) * parseInt(compra)).toFixed(2)) * ImpuestoValue) / 100;

            if (!ProcessManager.ValidateTableColum($PurchaseTable, 8, ArticulosId)) {

                //Aqui agregamos la fila
                $PurchaseTable.row.add([
                    0,
                    Articulos,
                    Almacen,
                    cantidad,
                    compra,
                    descuento,
                    Impuesto,
                    subTotal,
                    ArticulosId,
                    AlmacenId,
                    ImpuestoValue,
                    AmountTax,
                    MeasureId,
                    '<button type="button" class="btn btn-default btn-icon deleteRow"><i class="icon-trash"></i></button>'
                ]).draw(false);

                CalcularMontos();

                //Limpiamos los campos
                $('#cantidad,#compra,#venta, #Articulos,#AlmacenId,#descuento,#ArticulosId,#Impuesto').val('');
            }
            else {
                ProcessManager.Warning('Alerta', 'Ya existe un articulo en la orden de compras');
            }
        }
    });

    ///Removiendo articulos de la nota de credito
    $(document).on('click', '.itemRemove', function eliminar() {
        var tr = $(this).parents('tr').prev('tr');

        //en caso de que la fila sobrepase el ancho de la pantalla
        if (tr.length === 0) {
            tr = $(this).parents('tr');
        }

        $PurchaseTable.row(tr).remove().draw(false);

        CalcularMontos();
    });

    ///////////////envia los datos hacia el controlador para crear el Purchase/////////////
    $(".save").click(function () {
        var documentStatus = $(this).attr('data-document');

        if (documentStatus === 1) {
            var question = ProcessManager.Question('Alerta', 'Desea procesar la factura de compras? una vez procesada la factura de compras no se podrá editar, desea continuar?');

            question.then((answer) => {
                if (answer) {
                    savePurchase(documentStatus);
                }
            });
        }
        else {
            savePurchase(documentStatus);
        }
    });

    $("#Procesar").click(function () {
        var documentStatus = $(this).attr('data-document');
        CrearPurchase(documentStatus);
    });

    if ($('#frm-purchaseOrder').length > 0 || $('#Edit-PurchaseOrder').length > 0) {
        /*agregamos el boton dinamico*/
        $('.dataTables_wrapper').prepend('<button id="btn-add-item" type="button" class="btn border-slate text-slate-800 btn-flat pull-left">' +
            ' <i class="icon-folder-plus"></i>' +
            ' </button>');
    }

    $(document).on('click', '#btn-add-item', function myfunction() {
        var items = ProcessManager.LoadForm(ProcessManager.getUrl('PurchaseOrders/_Products'), null);

        items.then((data) => {
            ProcessManager.ShowModal("Listado de productos", 'icon-cube3', data, false, true, 3);

            //Inicializamos los componentes que utiliza el modal           
            callTableItems();
            $tableItems = $('#ItemList').DataTable();

            //checkbox
            $(".styled, .multiselect-container input").uniform({
                radioClass: 'choice'
            });

            //Agregamos el evento render en memoria
            $('#ItemList').on('draw.dt', function () {
                //Volviendo a renderizar el diseno
                $(".styled, .multiselect-container input").uniform({
                    radioClass: 'choice'
                });

                $('.checkbox').prop('checked', false).uniform('refresh');
            });
        });
    });


    $(document).on('click', '#ItemList tbody td', function () {
        //Almacenamos los productos que vamos a utilizar
        var indexColum = $tableItems.column('seleccionar:name').index();
        var indexSelectedColum = $tableItems.column('selected:name').index();

        if (this._DT_CellIndex.column == indexColum) {
            if (!ExistProduct($tableItems.column('codigo:name').data()[this._DT_CellIndex.row])) {
                $tableItems.cell(this._DT_CellIndex.row, indexSelectedColum).data($(this).find('input').hasClass('checked')).draw();
            }
            else {
                ProcessManager.Warning('Alerta', 'El producto que intenta seleccionar ya se encuentra en la factura de compras', true);
                $(this).find('input').removeClass('checked').val(false);
                $(this).find('.checked').removeClass('checked');
            }
        }
    });


    //agregamos el o los productos a la factura de compra
    $(document).on('click', '#MdAceptar', function myfunction() {
        var almacen = $("#Almacen").val();
        if (almacen !== "") {
            ProcessManager.HideModal();
            var itemsTotal = $tableItems.data().length;

            for (var index = 0; index < itemsTotal; index++) {
                if ($tableItems.cell(index, 'selected:name').data() === true) {

                    $PurchaseTable.row.add([
                        $tableItems.cell(index, 'codigo:name').data(),
                        $tableItems.cell(index, 'producto:name').data(),
                        $tableItems.cell(index, 'med:name').data(),
                        '<input name="inputQuantity" type="text" class="form-control number inputQuantity" value="0">',
                        '<input name="inputCost" type="text" class="form-control number inputCost" value="' + $tableItems.cell(index, 'cost:name').data() + '">',
                        '<input name="inputDesc" type="text" class="form-control number inputDesc" value="0">',
                        $tableItems.cell(index, 'imp:name').data(),
                        0,
                        $('#Almacen :selected').text(),
                        0,
                        0,
                        $tableItems.cell(index, 'productoId:name').data(),
                        $('#Almacen :selected').val(),
                        $tableItems.cell(index, 'measureId:name').data(),
                        $tableItems.cell(index, 'taxQuantity:name').data(),
                        0,
                        '<span class="itemRemove"> <i class="icon-trash cursor" ></i></span>'
                    ]).draw(false);
                }
            }
            CalcularMontos();
        }
        else {
            ProcessManager.Warning('Alerta', 'Debe selecionar un Almacen');
        }
    });


    //campo costo calculo
    $(document).on('keyup', '.inputDesc', function myfunction() {
        var discount = $(this).val();
        var tr = $(this).parents('tr');

        var productCode = $PurchaseTable.cell(tr, 'codigo:name').data();
        var quantity = ProcessManager.ValueExtract($PurchaseTable.cell(tr, 'quantity:name').data());
        var cost = ProcessManager.ValueExtract($PurchaseTable.cell(tr, 'cost:name').data());

        updateTable(productCode, quantity, cost, discount);

        CalcularMontos();
        ProcessManager.focusTextToEnd(tr.find('.inputDesc'));
    });


    //campo costo calculo
    $(document).on('keyup', '.inputCost', function myfunction() {
        var cost = $(this).val();
        var tr = $(this).parents('tr');

        var productCode = $PurchaseTable.cell(tr, 'codigo:name').data();
        var quantity = ProcessManager.ValueExtract($PurchaseTable.cell(tr, 'quantity:name').data());
        var discount = ProcessManager.ValueExtract($PurchaseTable.cell(tr, 'discount:name').data());

        updateTable(productCode, quantity, cost, discount);


        CalcularMontos();
        ProcessManager.focusTextToEnd(tr.find('.inputCost'));
    });


    //campo cantidad calculo
    $(document).on('keyup', '.inputQuantity', function myfunction() {
        var quantity = $(this).val();
        var tr = $(this).parents('tr');

        var productCode = $PurchaseTable.cell(tr, 'codigo:name').data();
        var cost = ProcessManager.ValueExtract($PurchaseTable.cell(tr, 'cost:name').data());
        var discount = ProcessManager.ValueExtract($PurchaseTable.cell(tr, 'discount:name').data());

        updateTable(productCode, quantity, cost, discount);

        CalcularMontos();
        ProcessManager.focusTextToEnd(tr.find('.inputQuantity'));
    });



    //////////////////////Balance pendiente/////////////////////
    $(document).on('click', '#PendigBalance', function myfunction() {

        var form = ProcessManager.LoadForm(ProcessManager.getUrl('PurchaseOrders/_DebitNoteList'), { suplierId: $('#SuppliersId').val() });

        form.then((data) => {
            ProcessManager.ShowModal("Balances por consumir", 'icon-cash2', data, true, true, 1, 'btnPendiBalance');
            callTablePendingBalance();
            $tablePendigBalance = $('#TablePendingBalance').DataTable();

            //checkbox
            $(".styled, .multiselect-container input").uniform({
                radioClass: 'choice'
            });

            //Agregamos el evento render en memoria
            $('#TablePendingBalance').on('draw.dt', function () {
                //Volviendo a renderizar el diseno
                $(".styled, .multiselect-container input").uniform({
                    radioClass: 'choice'
                });

                $('.checkbox').prop('checked', false).uniform('refresh');
            });

            CallPendingBalanceMemory();
        });
    });


    $(document).on('click', '#TablePendingBalance tbody td', function () {
        //Almacenamos los productos que vamos a utilizar
        var indexColum = $tablePendigBalance.column('seleccionar:name').index();
        var indexSelectedColum = $tablePendigBalance.column('selected:name').index();

        if (this._DT_CellIndex.column == indexColum) {
            if (!ExistPendingBalance($tablePendigBalance.column('Id:name').data()[this._DT_CellIndex.row])) {
                $tablePendigBalance.cell(this._DT_CellIndex.row, indexSelectedColum).data($(this).find('input').hasClass('checked')).draw();
            }
            else {
                ProcessManager.Warning('Alerta', 'El balance ya se encuentra en la factura de compras', true);
                $(this).find('input').removeClass('checked').val(false);
                $(this).find('.checked').removeClass('checked');
            }
        }
    });

    //Agregamos el o los balances al total de la factura de compras
    $(document).on('click', '#btnPendiBalance', function myfunction() {
        ProcessManager.HideModal();
        var itemsTotal = $tablePendigBalance.data().length;

        for (var index = 0; index < itemsTotal; index++) {
            if ($tablePendigBalance.cell(index, 'selected:name').data() === true) {
                PendingBalanceMemory.add(
                    $tablePendigBalance.cell(index, 'Id:name').data()
                );
            }
        }

        CalcularMontos();
    });

    ///Remueve el balance de la memoria
    $('#removePendigBalance').click(function myfunction() {
        PendingBalanceMemory = undefined;
        CalcularMontos();
    })

    //En caso de tener balance asignado lo cargamos a la memoria
    if ($('#Edit-PurchaseOrder').length > 0 && $('#TableMemoryPendigBalance').length > 0) {
        CallPendingBalanceMemory();
        $('#TableMemoryPendigBalance tr').each(function () {
            PendingBalanceMemory.add(
                $(this).children('td').eq(0).text(),//Id
                $(this).children('td').eq(1).text() //balance
            );
        });
    }
});

//////////////////////////Factura de compras//////////////////////////////////
function ExistProduct(productCode) {
    //removemos el elimento de la memoria
    for (var i = 0; i < $tableItems.data().length; i++) {
        if ($PurchaseTable.cell(i, 'codigo:name').data() == productCode) {
            return true;
        }
    }
}

//Este metodo agrega o envia la informacion al controller
function savePurchase(documentStatus) {

    var model = ProcessManager.FormatUrl($.param({ "PurchaseOrder.DocumentEstatusId": documentStatus }) + '&' + $.param({ "PurchaseOrder.": GetHeader() })) + '&' + $.param({ "PurchaseOrder.TblPurchaseOrderRow": GetBody() })
        + '&' + $.param({ "debitNote": ((PendingBalanceMemory !== undefined && PendingBalanceMemory.Memory.length > 0) ? PendingBalanceMemory.Memory : null) });


    var url = (($('#Edit-PurchaseOrder').length > 0) ? ProcessManager.getUrl('PurchaseOrders/UpdatePurchaseOrder') : ProcessManager.getUrl('PurchaseOrders/CreatePurchaseOrder'));


    $.ajax({
        url: url,
        type: 'POST',
        data: model,
        cache: false,
        beforeSend: function myfunction() {
            ProcessManager.Loading(true, 'body');
        },
        success: function myfunction(data) {
            if (data.status) {
                ProcessManager.Loading(false, 'body');
                ProcessManager.Success('Notificación', data.message);
                ProcessManager.Redirect(data.redirect);
            }
            else {
                ProcessManager.Loading(false, 'body');
                ProcessManager.Warning('Alerta', data.message);
            }
        },
        error: function myfunction(xhr, status) {
            ProcessManager.Loading(false, 'body');
            ProcessManager.validateStatusCode(xhr.status);
        }
    });

}

function PurchaseValidations() {
    var Articulos = $("#ArticulosId option:selected").val();
    var Almacen = $("#AlmacenId option:selected").val();
    var Impuesto = $("#Impuesto option:selected").val();
    var cantidad = $('#cantidad').val();
    var Precio_compra = $('#compra').val();

    if (Articulos ===null || Articulos.length ===0) {
        ProcessManager.Warning('Alerta', 'Debe selecionar un articulos');
        ProcessManager.Loading(false, 'body');
        return false;
    } else if (Almacen ===null || Almacen.length ===0) {
        ProcessManager.Warning('Alerta', 'Debe selecionar el almacen');
        ProcessManager.Loading(false, 'body');
        return false;
    } else if (cantidad.length ==="" || cantidad ===null) {
        ProcessManager.Warning('Alerta', 'El campo cantidad es obligatorio');
        ProcessManager.Loading(false, 'body');
        return false;
    } else if (Precio_compra ==="" || Precio_compra ===null) {
        ProcessManager.Warning('Alerta', 'El campo precio compra es obligatorio');
        ProcessManager.Loading(false, 'body');
        return false;
    } else if (Impuesto ===null || Impuesto.length ===0) {
        ProcessManager.Warning('Alerta', 'Debe selecionar un articulos');
        ProcessManager.Loading(false, 'body');
        return false;
    }
    else {
        return true;
    }
}

function GetHeader() {
    var headerMemory = {
        Memory: [],
        add: function (suppliersId, documentDate, documentNumber, expirationDate, observation, subTotal, discount, totalTax, total, rowId, ncf) {
            var header = {
                SuppliersId: suppliersId,
                DocumentDate: documentDate,
                DocumentNumber: documentNumber,
                ExpirationDate: expirationDate,
                Observation: observation,
                Discount: discount,
                SubTotal: subTotal,
                TotalTax: totalTax,
                Total: total,
                Id: rowId,
                Ncf: ncf
            };
            headerMemory.Memory.push(header);
        }
    };

    var suppliersId = $('#SuppliersId').val();
    var DocumentDate = $('#DocumentDate').val();
    var ExpirationDate = $('#ExpirationDate').val();
    var pushOrderId = $('#PurchaseOrderId').val();

    var observacion = $('#Observation').text();
    var SubTotal = $('#subTotal').text();
    var Discount = $('#desTotal').text();
    var TotalTax = $('#impTotal').text();
    var Total = $('#total').text();
    var documentNumber = $('#documentNumber').text();
    var ncf = $('#ncf').val();


    headerMemory.add(suppliersId, DocumentDate, documentNumber, ExpirationDate, observacion, SubTotal, Discount, TotalTax, Total, pushOrderId, ncf);
    return headerMemory.Memory[0];
}

//Esta funcion asigna cada fila de la tabla a una fila de la tabla detalle
function GetBody() {

    var PurchaseMemory = {
        Memory: [],
        add: function (productId, almacenId, measureId, taxAbbreviation, quantity, cost, tax, amountTax, discount, amountDiscount, total, index) {
            var OrderRow = {
                ProductId: productId,
                MeasureId: measureId,
                WarehouseId: almacenId,
                TaxAbbreviation: taxAbbreviation,
                Quantity: quantity,
                Cost: cost,
                Tax: tax,
                AmountTax: amountTax,
                Discount: discount,
                AmountDiscount: amountDiscount,
                Total: total
            };
            PurchaseMemory.Memory.push(OrderRow);
        }
    };

    for (var index = 0; index < $PurchaseTable.data().length; index++) {
        PurchaseMemory.add(
            $PurchaseTable.cell(index, 'productId:name').data(),
            $PurchaseTable.cell(index, 'warehouseId:name').data(),
            $PurchaseTable.cell(index, 'measureId:name').data(),
            $PurchaseTable.cell(index, 'taxAbbreviation:name').data(),
            ProcessManager.ValueExtract($PurchaseTable.cell(index, 'quantity:name').data()),
            ProcessManager.ValueExtract($PurchaseTable.cell(index, 'cost:name').data()),
            $PurchaseTable.cell(index, 'taxQuantity:name').data(),
            parseFloat($PurchaseTable.cell(index, 'amountTax:name').data()).toFixed(2),
            ProcessManager.ValueExtract($PurchaseTable.cell(index, 'discount:name').data()),
            $PurchaseTable.cell(index, 'amountDiscount:name').data(),
            $PurchaseTable.cell(index, 'total:name').data(),
            $PurchaseTable.cell(index, 'index:name').data()
        );
    }
    return PurchaseMemory.Memory;
}

function CalcularMontos() {

    valor = 0;
    tax = parseInt(0);
    desc = parseInt(0);
    impuestos = parseInt(0);
    imp = parseInt(0);
    var amountDiscount = 0;

    for (var index = 0; index < $PurchaseTable.data().length; index++) {
        tax += parseFloat($PurchaseTable.cell(index, 'amountTax:name').data());
        amountDiscount += parseFloat($PurchaseTable.cell(index, 'amountDiscount:name').data());
        valor += parseFloat($PurchaseTable.cell(index, 'total:name').data());
        desc += parseFloat(ProcessManager.ValueExtract($PurchaseTable.cell(index, 'discount:name').data()));
    }

    /*Verificamos si tiene balance pendiente*/
    CalcularPendingBalance();

    $('#subTotal').text(formatMoney(valor));

    $('#impTotal').text(formatMoney(tax));

    $('#desTotal').text(formatMoney(amountDiscount));

    $('#total').text(formatMoney((valor + tax) - amountDiscount));
}

function updateTable(productCode, quantity, cost, discount) {
    var itemsPurchaseTotal = $PurchaseTable.data().length;

    //Actualizamos los valores
    for (var i = 0; i < itemsPurchaseTotal; i++) {
        if ($PurchaseTable.cell(i, 'codigo:name').data() == productCode) {

            //update campo
            $PurchaseTable.cell(i, $PurchaseTable.column('discount:name').index()).data('<input name="inputDesc" type="text" class="form-control number inputDesc" value="' + discount + '">').draw();

            //update campo
            $PurchaseTable.cell(i, $PurchaseTable.column('quantity:name').index()).data('<input name="inputQuantity" type="text" class="form-control number inputQuantity" value="' + quantity + '">').draw();

            //update campo
            $PurchaseTable.cell(i, $PurchaseTable.column('cost:name').index()).data('<input name="inputCost" type="text" class="form-control number inputCost" value="' + cost + '">').draw();

            var amountDiscount = ProcessManager.CalculateDiscount(quantity, cost, discount);
            $PurchaseTable.cell(i, $PurchaseTable.column('amountDiscount:name').index()).data(amountDiscount).draw();

            var amountTax = ProcessManager.CalculateTaxApplied(quantity, cost, discount, $PurchaseTable.cell(i, 'taxQuantity:name').data());
            $PurchaseTable.cell(i, $PurchaseTable.column('amountTax:name').index()).data(amountTax).draw();


            var total = ProcessManager.CalculateSubTotal(quantity, cost) - amountTax;
            $PurchaseTable.cell(i, $PurchaseTable.column('total:name').index()).data(total.toFixed(2)).draw();
            $PurchaseTable.row().invalidate();
        }
    }
}

function CallPurchaseTable() {
    var columnsTitle = [
        { title: "Código", name: "codigo" },
        { title: "Producto", name: "articulo" },
        { title: "Med", name: "medida" },
        { title: "Cantidad", name: "quantity" },
        { title: "Costo", name: "cost" },
        { title: "Desc.", name: "discount" },
        { title: "Imp.", name: "taxAbbreviation" },
        { title: "Sub Total", name: "total" },
        { title: "Almacén", name: "almacen" },
        { title: "Taxamount", name: "amountTax", visible: false },
        { title: "AmountDisc", name: "amountDiscount", visible: false },
        { title: "ArticulosId", name: "productId", visible: false },
        { title: "AlmacenId", name: "warehouseId", visible: false },
        { title: "MedidaId", name: "measureId", visible: false },
        { title: "tax", name: "taxQuantity", visible: false },
        { title: "IndexRow", name: "index", visible: false },
        { title: "Eliminar", name: "eliminar" }
    ];

    return CallSyncTable("#PurchaseTable", 10, null, columnsTitle);
}

function callTableItems() {

    //Estas con las columnas que apareceran en la tabla
    var columnsTitle = [
        { title: "Código", name: "codigo" },
        { title: "ProductoId", name: "productoId", visible: false },
        { title: "Producto", name: "producto" },
        { title: "Servicio", name: "isService" },
        { title: "Med.", name: "med" },
        { title: "Imp.", name: "imp" },
        { title: "Costo", name: "cost" },
        { title: "MeasureId", name: "measureId", visible: false },
        { title: "TaxQuantity", name: "taxQuantity", visible: false },
        { title: "Seleccionar", name: "seleccionar" },
        { title: "Selected", name: "selected", visible: false }
    ];

    CallSyncTable("#ItemList", 5, null, columnsTitle);
}

//////////////Balance pendiente///////////////////////////////////////////

function ExistPendingBalance(Id) {
    //removemos el elimento de la memoria
    if (PendingBalanceMemory.Memory.length > 0) {
        for (var i = 0; i < PendingBalanceMemory.Memory.length; i++) {
            if (PendingBalanceMemory.Memory[i].DebitNoteId === Id) {
                return true;
            }
        }
    }
}

function callTablePendingBalance() {

    //Estas con las columnas que apareceran en la tabla
    var columnsTitle = [
        { title: "No. Documento", name: "documento" },
        { title: "Fecha", name: "fecha" },
        { title: "Total", name: "total" },
        { title: "Id", name: "Id", visible: false },
        { title: "Seleccionar", name: "seleccionar" },
        { title: "Selected", name: "selected", visible: false }
    ];

    CallSyncTable("#TablePendingBalance", 5, null, columnsTitle);
}

function CalcularPendingBalance() {
    var PendingBalance = parseFloat(0);
    if (PendingBalanceMemory != undefined && PendingBalanceMemory.Memory.length > 0) {
        $('#PendingBalanceTotal').closest('tr').show();
        for (var i = 0; i < PendingBalanceMemory.Memory.length; i++) {
            PendingBalance += parseFloat(ProcessManager.ValueExtract(PendingBalanceMemory.Memory[i].Total));
        }
        $('#PendingBalanceTotal').text(PendingBalance.toFixed(2));
        return PendingBalance;
    }
    else {
        $('#PendingBalanceTotal').closest('tr').hide();
        return 0;
    }
}

function CallPendingBalanceMemory() {

    if (PendingBalanceMemory == undefined) {
        PendingBalanceMemory = {
            Memory: [],
            add: function (debitNoteId) {
                var balanceRow = {
                    Id: debitNoteId
                };
                PendingBalanceMemory.Memory.push(balanceRow);
            }
        };
    }
}

//////////////Metodo para dar formato a los numeros//////////////
function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
    try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
        console.log(e);
    }
}