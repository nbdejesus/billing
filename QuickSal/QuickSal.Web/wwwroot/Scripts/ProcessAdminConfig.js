﻿$(document).ready(function myfunction() {
    CallDinamicTable('.datatable', 10);

    $(document).on('click', '.businessPanelId', function myfunction() {
        localStorage.setItem('businessId', $(this).attr('data-id')); 
        console.log(localStorage.getItem('businessId'));

    });

    $('.select').selectize({
        create: true,
        sortField: {
            field: 'text',
            direction: 'asc'
        },
        dropdownParent: 'body'
    });

    // Single picker
    $('.date').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD/MM/YYYY'
        }
    });

    $(document).on('click', '.back', function myfunction() {
        window.history.back();
    });

    $(document).on('click', '.backed', function myfunction() {
        window.location.replace(ProcessManager.getUrl('dashboard/DashboardBusinessPrincipal?BusinessId=' + localStorage.getItem('businessId') + ''));
       
    });

    $('.phone').mask('000-000-0000');

    $('.number').mask('000,000,000,000,000.00', { reverse: true });

    var elems = "";
    if (Array.prototype.forEach) {
        elems = Array.prototype.slice.call(document.querySelectorAll('.checkbox-switchery'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html, { color: '#64BD63' });
        });
    }
    else {
         elems = document.querySelectorAll('.checkbox-switchery');
        for (var i = 0; i < elems.length; i++) {
            var switchery = new Switchery(elems[i], { color: '#64BD63' });
        }
    }
});

function CallDinamicTable(object, RowTotal) {
    var order = $(object).attr('table-order');

    //Data table configuration
    return $(object).DataTable({
        "bLengthChange": false,
        "lengthMenu": [RowTotal],
        "order": ((order === undefined) ? order : []),
        language: {
            processing: "Procesando...",
            search: "Buscar:",
            lengthMenu: "Ver _MENU_ Filas",
            info: "_START_ - _END_ de _TOTAL_ elementos",
            infoEmpty: "0 - 0 de 0 elementos",
            infoFiltered: "(Filtro de _MAX_ entradas en total)",
            infoPostFix: "",
            loadingRecords: "Cargando datos.",
            zeroRecords: "No se encontraron datos",
            emptyTable: "No hay datos disponibles",
            paginate: {
                first: "Primero",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultimo"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            },
        }
    });
}


function CargarTablaAjax(object, url, columns, orderby) {

    return $(object).DataTable({
        "bLengthChange": false,
        responsive: true,
        language: {
            processing: "Procesando",
            search: "Buscar:",
            lengthMenu: "Ver _MENU_ Filas",
            info: "_START_ - _END_ de _TOTAL_ elementos",
            infoEmpty: "0 - 0 de 0 elementos",
            infoFiltered: "(Filtro de _MAX_ entradas en total)",
            infoPostFix: "",
            loadingRecords: "Cargando datos.",
            zeroRecords: "No se encontraron datos",
            emptyTable: "No hay datos disponibles",
            paginate: {
                first: "Primero",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultimo"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        "ajax": {
            "url": url,
            "type": "get",
            "datatype": "json"
        },
        "columns": columns
    });
}


function CallSyncTable(object, RowTotal, orderBy, columnsProperties) {   

    //Data table configuration
    return $(object).DataTable({
        "bLengthChange": false,
        "lengthMenu": [RowTotal],
        "ordering": ((orderBy === undefined || orderBy === true) ? true : false),
        columns: columnsProperties,
        language: {
            processing: "Procesando...",
            search: "Buscar:",
            lengthMenu: "Ver _MENU_ Filas",
            info: "_START_ - _END_ de _TOTAL_ elementos",
            infoEmpty: "0 - 0 de 0 elementos",
            infoFiltered: "(Filtro de _MAX_ entradas en total)",
            infoPostFix: "",
            loadingRecords: "Cargando datos.",
            zeroRecords: "No se encontraron datos",
            emptyTable: "No hay datos disponibles",
            paginate: {
                first: "Primero",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultimo"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            },
        }
    });
}
