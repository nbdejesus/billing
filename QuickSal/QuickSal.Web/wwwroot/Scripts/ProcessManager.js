﻿(function (window) {
    var ProcessManager = function () {

        $(document).on('change', '[data-country]', function myfunction() {
            if ($(this).val() !== "") {

                var form = window.ProcessManager.LoadForm(
                    window.ProcessManager.getUrl('Account/GetAllProvince'),
                    {
                        CountryId: $(this).children(':selected').val()
                    }, '.panel');

                form.then((data) => {
                    var option = '';
                    $('[data-province]').html('');


                    if (data.provinceList.length > 0) {
                        option = option + '<option value="" selected="selected">Selecionar...</option>';
                        for (var i = 0; i < data.provinceList.length; i++) {
                            option = option + '<option value=' + data.provinceList[i].id + '>' + data.provinceList[i].description + '</option>';
                        }
                    }
                    $('[data-province]').append(option);
                    $('[data-province]').select2();
                });
            }
        });

        $(document).on('change', '[data-type-document]', function myfunction() {
            window.ProcessManager.getDocumentTypeConfiguration($(this));
        });

        this.getDocumentTypeConfiguration = function () {

            if ($('[data-type-document]').val() !== "") {

                var form = window.ProcessManager.LoadForm(
                    window.ProcessManager.getUrl('genericMethod/GetDocumentIdentification'),
                    {
                        Id: $('[data-type-document]').children(':selected').val()
                    }, '.panel');

                form.then((data) => {
                    if (data.mask != null) {
                        $('[data-document]').mask(data.mask);
                    }
                    else {
                        $("[data-document]").unmask();
                    }
                    $('[data-document]').attr('maxlength', data.maxlengthChar);
                });
            }
        }

        //guardamos en memoria las materias retiradas o seleccionada
        $(document).on('click', '[type="checkbox"]', function myfunction() {
            var $check = $(this);

            if ($check.hasClass('checked')) {
                $check.removeClass('checked').val(false);
            }
            else {
                $check.addClass('checked').val(true);
            }
        });


        $(document).on('change', '[data-categories]', function myfunction() {
            if ($(this).val() !== "") {

                var form = window.ProcessManager.LoadForm(
                    window.ProcessManager.getUrl('Product/GetAllSubCategories'),
                    {
                        CategoryId: $(this).children(':selected').val()
                    }, '.panel');

                form.then((data) => {
                    var option = '';
                    $('[data-subcategories]').html('');
                    if (data.subCategoryList.length > 0) {
                        option = option + '<option value="" selected="selected">Selecionar...</option>';
                        for (var i = 0; i < data.subCategoryList.length; i++) {
                            option = option + '<option value="' + data.subCategoryList[i].id + '">' + data.subCategoryList[i].name + '</option>';

                        }
                    }
                    $('[data-subcategories]').append(option).select2({
                        selectOnClose: true,

                        placeholder: 'Seleccionar la Subcategoria... ',
                        width: '100%',
                        allowClear: true,
                        insertTag: function (data, tag) {
                            // Insert the tag at the end of the results
                            data.push(tag);
                        }
                    }
                    );
                });

            }
        });


        $(document).on('click', '.number', function myfunction() {
            var valor = $(this).val();
            if (valor === "" || valor == 0.00) {
                $(this).val("");
            }
        }).on('blur', '.number', function myfunction() {
            var valor = $(this).val();
            if (valor === "") {
                $(this).val("0.00");
            }
        });


        this.ValidateTableColum = function (ObjecTable, ColumNumber, value) {
            var array = ObjecTable.data().toArray();
            for (var i = 0; i < array.length; i++) {
                if (array[i][ColumNumber] === value) {
                    return true;
                }
            }
        };

        this.getUrl = function (url) {
            var path = $('body').attr('data-root');
            return window.location.origin + path + '/' + url;
        };

        this.ValueExtract = function (stringValue) {
            if (stringValue != undefined && stringValue != "") {
                var result = stringValue.match(/\d+(\.\d{1,2})?/g);

                if (result.length > 1) {
                    var val = 0;
                    for (var i = 0; i < result.length; i++) {
                        val = val + result[i];
                    }
                    return parseFloat(val).toFixed(2);
                }
                else {
                    return result[0];
                }
            }
        };

        this.OnlyNumberAndCharter = function (e) {
            teclaEvento = (document.all) ? e.keyCode : e.which;
            // Patron de entrada, en este caso solo acepta numeros y letras
            patron = /^[A-Za-z9ñçáéíóú\d\s]+$/;
            var result = false;

            if (teclaEvento !== undefined) {
                //Tecla de retroceso para borrar, siempre la permite
                if (teclaEvento === 8) {
                    return true;
                }

                tecla_final = String.fromCharCode(teclaEvento);
                result = patron.test(tecla_final);
            }
            else {
                result = patron.test(e.value);
            }

            return result;
        };

        this.Loading = function (Mostrar, Object) {
            var block = ((Object === undefined || Object === null) ? $('.panel') : Object);
            if (Mostrar) {
                $(block).block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait',
                        'box-shadow': '0 0 0 1px #ddd'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            }
            else {
                $(block).unblock();
            }
        };

        this.Redirect = function (url) {
            setTimeout(function () {
                if (url === undefined) {
                    location.reload();
                }
                else {
                    location.href = url;
                }
            }, 1700);
        };

        this.ShowModal = function (Title, icon = '', body, ShowCancel = true, ShowAccept = true, ModalType, ButtonId = '') {
            $('#Generic-modal').modal('show');
            $('#Generic-modal #Title').text(Title);
            $('#Modal-icon').addClass(icon);
            $('#Generic-modal .modal-body').html(body);

            if (ShowCancel === false) {
                $('#Generic-modal #btnCancel').css({ "display": "none" });
            }
            else {
                $('#Generic-modal #btnCancel').css({ "display": "initial" });
            }


            if (ShowAccept === false) {
                $('#Generic-modal #MdAceptar').css({ "display": "none" });
            }
            else {
                $('#Generic-modal #MdAceptar').css({ "display": "initial" });
            }


            if (ModalType === 2) {
                $('.modal-dialog').addClass('modal-lg');
            }
            else if (ModalType === 3) {
                $('.modal-dialog').addClass('modal-xl');
                $('.modal-dialog').removeClass('modal-lg');
            }
            else {
                $('.modal-dialog').removeClass('modal-xl');
                $('.modal-dialog').removeClass('modal-lg');
            }

            if (ButtonId !== '') {
                $('.modal-footer .btn-primary').attr('id', ButtonId);
            }
            else {
                $('.modal-footer .btn-primary').attr('id', 'MdAceptar');
            }
        };

        this.HideModal = function () {
            $('#Generic-modal').modal('hide');
        };

        this.FormatUrl = function (stringUrl) {
            return stringUrl.replace(/%5B/g, '').replace(/%5D/g, '').replace(/%2F/g, '/');
        };


        this.GIF = function myfunction() {
            return '/Img/Loader.gif';
        };

        this.LoadForm = function (Url, Data, LoginUbication) {

            return new Promise(resolve => {
                $.ajax({
                    url: Url,
                    type: 'GET',
                    data: Data,
                    async: true,
                    beforeSend: function myfunction() {
                        window.ProcessManager.Loading(true, ((LoginUbication === null || LoginUbication === undefined) ? 'body' : LoginUbication));
                    },
                    success: function myfunction(data) {
                        resolve(data);
                        window.ProcessManager.Loading(false, ((LoginUbication === null || LoginUbication === undefined) ? 'body' : LoginUbication));
                    },
                    error: function myfunction(xhr, status) {
                        window.ProcessManager.Loading(false, ((LoginUbication === null || LoginUbication === undefined) ? 'body' : LoginUbication));
                        var jerror = $($.parseHTML(xhr.responseText));
                        var error = undefined;

                        for (var i = 0; i < jerror.length; i++) {
                            if (jerror[i].className === 'titleerror') {
                                error = jerror[i].outerText;
                            }
                        }

                        //var test = jerror.find('body');
                        window.ProcessManager.validateStatusCode(xhr.status, error);
                    }
                });
            });
        };

        this.RemoveNumberFormat = function (amount) {
            return amount.replace(/,/g, "");
        };


        this.base64ToArrayBuffer = function (base64) {
            var binaryString = window.atob(base64);
            var binaryLen = binaryString.length;
            var bytes = new Uint8Array(binaryLen);
            for (var i = 0; i < binaryLen; i++) {
                var ascii = binaryString.charCodeAt(i);
                bytes[i] = ascii;
            }
            return bytes;
        };

        this.saveByteArray = function (FileName, base64byte, FileType) {
            var blob = new Blob([base64byte]);
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            var fileName = FileName + "." + FileType;
            link.download = fileName;
            link.click();
        };



        this.ValidateTypeFile = function ($inputFile, extensionesPermitidas) {
            var archivo = $inputFile.val();
            miError = "";
            if (!archivo) {
                window.ProcessManager.Warning('Alerta', "Debe seleccionar un archivo.", true);
                return false;
            }
            else {
                extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
                permitida = false;
                for (var i = 0; i < extensionesPermitidas.length; i++) {
                    if (extensionesPermitidas[i] === extension) {
                        permitida = true;
                        break;
                    }
                }
                if (!permitida) {
                    window.ProcessManager.Warning("Alerta", "Comprueba la extensión de los archivos a subir. \nSólo se pueden subir archivos con extensión: " + extensionesPermitidas.join(), true);
                    $inputFile.val('');
                }

                return permitida;
            }
        }


        this.Error = function (Titulo, Mensaje, buttons) {
            new PNotify({
                title: Titulo,
                text: Mensaje,
                icon: 'icon-blocked',
                type: 'error'
            });
        }

        this.Info = function (Titulo, Mensaje, buttons) {
            new PNotify({
                title: Titulo,
                text: Mensaje,
                icon: 'icon-info22',
                type: 'info'
            });
        }

        this.Question = function (Titulo, Mensaje) {
            return swal({
                title: Titulo,
                text: Mensaje,
                icon: "warning",
                buttons: true,
                dangerMode: true
            });
        };


        this.Warning = function (Titulo, Mensaje, buttons) {
            new PNotify({
                title: Titulo,
                text: Mensaje,
                icon: 'icon-warning22',
                type: 'warning'
            });
        };

        this.Success = function (Titulo, Mensaje, buttons) {
            new PNotify({
                title: Titulo,
                text: Mensaje,
                icon: 'icon-checkmark3',
                type: 'success'
            });
        };


        this.ValidEmail = function (email) {
            emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
            //Se muestra un texto a modo de ejemplo, luego va a ser un icono
            if (emailRegex.test(email)) {
                return true;
            } else {
                return false;
            }
        };

        this.mask = function (mask) {
            if (mask === 'phone') {
                $('.phone').mask('(000) 000-0000');
            }
        };

        this.PDF = function myfunction() {
            return '/Img/pdf.png';
        };

        this.EPUB = function myfunction() {
            return '/Img/epub.png';
        };

        this.validateStatusCode = function (StatusCode, error) {
            if (StatusCode === 429) {
                window.ProcessManager.Warning('Alerta', "El tiempo de sesión se ha agotado.");
                setTimeout(function () {
                    location.href = window.location.origin;
                }, 1500);
            }
            else if (StatusCode === 401) {
                window.ProcessManager.Warning('Alerta', "No tiene privilegios para entrar a esta opción.");
            }
            else if (StatusCode === 404) {
                window.ProcessManager.Warning('Alerta', "Accion no encontrada.");
            }
            else if (StatusCode === 500) {
                window.ProcessManager.Warning('Alerta', error);
            }
        };

        this.ValidateImage = function ($inputFile) {
            var archivo = $inputFile.val();
            extensionesPermitidas = new Array(".jpg", ".png", ".jpeg");
            miError = "";
            if (!archivo) {
                window.ProcessManager.Warning('Alerta', "Debe seleccionar una imagen.");
                return false;
            }
            else {
                extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
                permitida = false;
                for (var i = 0; i < extensionesPermitidas.length; i++) {
                    if (extensionesPermitidas[i] === extension) {
                        permitida = true;
                        break;
                    }
                }
                if (!permitida) {
                    window.ProcessManager.Warning('Alerta', "Comprueba la extensión de los archivos a subir. \nSólo se pueden subir archivos con extensión: " + extensionesPermitidas.join());
                    $inputFile.val('');
                }

                return permitida;
            }
        };



        this.CalculateTax = function (quantity, value, discount, tax) {
            var Value = parseFloat(value);
            var Quantity = parseFloat(quantity);
            var Desc = parseFloat(discount);
            var Tax = parseFloat(((tax === "") ? 0 : tax));
            var subtotal = window.ProcessManager.CalculateSubTotal(Quantity, Value) - window.ProcessManager.CalculateDiscount(quantity, value, discount);

            return subtotal - (subtotal / ((Tax / 100) + 1));
        };

        this.focusTextToEnd = function ($object) {
            $object.focus();
            var $thisVal = $object.val();
            $object.val('').val($thisVal);
            return $object;
        };

        this.CalculateTaxApplied = function (quantity, value, discount, tax) {
            var Value = parseFloat(value);
            var Quantity = parseFloat(quantity);
            var Desc = parseFloat(discount);
            var Tax = parseFloat(((tax === "") ? 0 : tax));
            var subtotal = window.ProcessManager.CalculateSubTotal(Quantity, Value);

            return subtotal - (subtotal / ((Tax / 100) + 1));
        };

        this.CalculateDiscount = function (quantity, value, discount) {
            var Value = parseFloat(value);
            var Quantity = parseFloat(quantity);
            var Desc = parseFloat(discount);

            return ((Quantity * Value) * (Desc / 100));
        };

        this.CalculateSubTotal = function (quantity, value) {
            var Value = parseFloat(value);
            var Quantity = parseFloat(quantity);

            return (Quantity * Value);
        };

        this.addCommas = function (nStr) {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        };

        this.isNumeric = function (obj) {
            var realStringObj = obj && obj.toString();
            return !jQuery.isArray(obj) && (realStringObj - parseFloat(realStringObj) + 1) >= 0;
        };

        this.saveByteArray = function (FileName, base64byte, FileType) {
            var blob = new Blob([base64byte]);
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            var fileName = FileName + "." + FileType;
            document.body.appendChild(link);
            link.download = fileName;
            link.click();

            window.setTimeout(function () {
                URL.revokeObjectURL(blob);
                document.body.removeChild(link);
            }, 0);
        };

        this.base64ToArrayBuffer = function (base64) {
            var binaryString = window.atob(base64);
            var binaryLen = binaryString.length;
            var bytes = new Uint8Array(binaryLen);
            for (var i = 0; i < binaryLen; i++) {
                var ascii = binaryString.charCodeAt(i);
                bytes[i] = ascii;
            }
            return bytes;
        };

        this.base64ToArray = function (buf) {
            var binstr = Array.prototype.map.call(buf, function (ch) {
                return String.fromCharCode(ch);
            }).join('');
            return btoa(binstr);
        };

    };
    window.ProcessManager = new ProcessManager();
})(window);