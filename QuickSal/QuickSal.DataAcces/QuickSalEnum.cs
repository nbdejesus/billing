﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace QuickSal.DataAcces
{
    public class QuickSalEnum
    {
        public enum DocumentEstatus
        {
            [Description("Aprobada")]
            Aprobada = 1,
            [Description("En Proceso")]
            Proceso = 2,
            [Description("Anulada")]
            Anulada = 3
        }


        public enum OrderEstatus
        {
            [Description("Pendiente")]
            Pendiente = 1,
            [Description("En Proceso")]
            Proceso = 2,
            [Description("Cancelada")]
            Cancelada = 3,
            [Description("Finalizada")]
            Finalizada = 4
        }

        public enum TypeFiles
        {
            ProfilePhoto = 1,
            BusinessLogo = 2,
            EmployeePhoto = 3,
            CustomerPhoto = 4,
            SupplierPhoto = 5,
            Product = 6,
            TemplateCustomer = 7,
            TemplateSupplier = 8,
            TemplateEmployee = 9,
            TemplateProduct = 10
        }

        public enum TypeOfKey
        {
            CreacionContraseña = 1,
            RecuperacionContraseña = 2,
            CambioContraseña = 3

        }

        public enum Currency
        {
            [Description("RD$")]
            RD = 1,
            [Description("USD$")]
            USD = 2,
        }

        public enum MovingStatus
        {
            [Description("En Proceso")]
            In_Procces = 1,
            [Description("En Transito")]
            In_Transit = 2,
            [Description("Recibido")]
            Received = 3,
            [Description("Cancelado")]
            Canceled = 4,
            [Description("Devuelto")]
            Returned = 5,
        }

        public static string GetListDesc(Enum tipoEnum)
        {
            var query = tipoEnum.GetType().
                    GetField(tipoEnum.ToString()).GetCustomAttributes(true).
                    Where(a => a.GetType().Equals(typeof(System.ComponentModel.DescriptionAttribute)));

            return (tipoEnum.GetType().
                    GetField(tipoEnum.ToString()).GetCustomAttributes(true).
                    Where(a => a.GetType().Equals(typeof(System.ComponentModel.DescriptionAttribute))).
                    FirstOrDefault() as System.ComponentModel.DescriptionAttribute).Description;
        }
    }
}
