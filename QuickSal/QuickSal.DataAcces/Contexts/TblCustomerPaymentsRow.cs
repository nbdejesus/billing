﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblCustomerPaymentsRow
    {
        public long Id { get; set; }
        public long CustomerPaymentId { get; set; }
        public long CustomerId { get; set; }
        public long InvoiceId { get; set; }
        public string InvoiceDocument { get; set; }
        public string CustomerName { get; set; }
        public string Concept { get; set; }
        public decimal Owed { get; set; }
        public decimal PaidOut { get; set; }
        public decimal Payment { get; set; }
        public decimal Pending { get; set; }

        public TblCustomers Customer { get; set; }
        public TblCustomerPayments CustomerPayment { get; set; }
        public TblInvoice Invoice { get; set; }
    }
}
