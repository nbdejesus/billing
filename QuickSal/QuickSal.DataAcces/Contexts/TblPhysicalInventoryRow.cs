﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblPhysicalInventoryRow
    {
        public long Id { get; set; }
        public int PhysicalInventoryId { get; set; }
        public long ProductId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Measure { get; set; }
        public decimal CountedAmount { get; set; }
        public decimal? StockQuantity { get; set; }
        public decimal? StockDifference { get; set; }
        public decimal Cost { get; set; }
        public decimal TotalCost { get; set; }
        public decimal TotalCostCounted { get; set; }
        public decimal? TotalCostDifference { get; set; }

        public TblPhysicalInventory PhysicalInventory { get; set; }
        public TblProducts Product { get; set; }
    }
}
