﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblBusineesModules
    {
        public long Id { get; set; }
        public int AdaSubObjectId { get; set; }
        public long BusinessId { get; set; }

        public TblBusiness Business { get; set; }
    }
}
