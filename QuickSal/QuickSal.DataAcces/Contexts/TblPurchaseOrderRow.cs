﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblPurchaseOrderRow
    {
        public long Id { get; set; }
        public long PurchaseOrderId { get; set; }
        public int WarehouseId { get; set; }
        public int MeasureId { get; set; }
        public long ProductId { get; set; }
        public int RowNumber { get; set; }
        public string TaxAbbreviation { get; set; }
        public decimal Quantity { get; set; }
        public decimal Cost { get; set; }
        public decimal Tax { get; set; }
        public decimal AmountTax { get; set; }
        public decimal Discount { get; set; }
        public decimal AmountDiscount { get; set; }
        public decimal Total { get; set; }

        public TblMeasure Measure { get; set; }
        public TblProducts Product { get; set; }
        public TblPurchaseOrder PurchaseOrder { get; set; }
        public TblWarehouse Warehouse { get; set; }
    }
}
