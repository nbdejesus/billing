﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblPlanSubObject
    {
        public int Id { get; set; }
        public int SubObjectId { get; set; }
        public short PlanId { get; set; }

        public TblPlans Plan { get; set; }
        public TblSubObject SubObject { get; set; }
    }
}
