﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblBranchOfficeSchedule
    {
        public long Id { get; set; }
        public long BranchOfficeId { get; set; }
        public byte DayOfWeek { get; set; }
        public TimeSpan FromTime { get; set; }
        public TimeSpan ToTime { get; set; }
        public bool Estatus { get; set; }

        public TblBranchOffice BranchOffice { get; set; }
    }
}
