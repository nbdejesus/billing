﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblCreditNoteRow
    {
        public long Id { get; set; }
        public long CreditNoteId { get; set; }
        public int WarehouseId { get; set; }
        public int MeasureId { get; set; }
        public long ProductId { get; set; }
        public string TaxAbbreviation { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Tax { get; set; }
        public decimal AmountTax { get; set; }
        public decimal Discount { get; set; }
        public decimal AmountDiscount { get; set; }
        public decimal Total { get; set; }

        public TblCreditNote CreditNote { get; set; }
        public TblMeasure Measure { get; set; }
        public TblProducts Product { get; set; }
    }
}
