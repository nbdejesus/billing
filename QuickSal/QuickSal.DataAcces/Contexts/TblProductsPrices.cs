﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblProductsPrices
    {
        public long Id { get; set; }
        public long ProductsId { get; set; }
        public int TaxId { get; set; }
        public string Name { get; set; }
        public decimal Utility { get; set; }
        public decimal Price { get; set; }
        public bool IsDefault { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }

        public TblProducts Products { get; set; }
        public TblTax Tax { get; set; }
    }
}
