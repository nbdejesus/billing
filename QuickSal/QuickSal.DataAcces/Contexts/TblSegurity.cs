﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblSegurity
    {
        public long Id { get; set; }
        public int PrivilegesId { get; set; }
        public int SubObjectId { get; set; }
        public byte? TypeMethodId { get; set; }

        public TblPrivileges Privileges { get; set; }
        public TblTypeMethod TypeMethod { get; set; }
    }
}
