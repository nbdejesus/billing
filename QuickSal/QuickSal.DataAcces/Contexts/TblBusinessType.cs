﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblBusinessType
    {
        public TblBusinessType()
        {
            TblPlans = new HashSet<TblPlans>();
        }

        public short Id { get; set; }
        public string Name { get; set; }
        public bool Estatus { get; set; }
        public string Description { get; set; }
        public string CoverPage { get; set; }

        public ICollection<TblPlans> TblPlans { get; set; }
    }
}
