﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblAppSettingValue
    {
        public int Id { get; set; }
        public short IdAppSetting { get; set; }
        public string ValueName { get; set; }
        public string SettingValue { get; set; }

        public TblAppSettings IdAppSettingNavigation { get; set; }
    }
}
