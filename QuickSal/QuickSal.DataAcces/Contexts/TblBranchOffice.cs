﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblBranchOffice
    {
        public TblBranchOffice()
        {
            TblBranchOfficeSchedule = new HashSet<TblBranchOfficeSchedule>();
            TblCreditNote = new HashSet<TblCreditNote>();
            TblCustomerPayments = new HashSet<TblCustomerPayments>();
            TblDebitNote = new HashSet<TblDebitNote>();
            TblEmployeeBusiness = new HashSet<TblEmployeeBusiness>();
            TblInvoice = new HashSet<TblInvoice>();
            TblOrders = new HashSet<TblOrders>();
            TblPurchaseOrder = new HashSet<TblPurchaseOrder>();
            TblVendorPayment = new HashSet<TblVendorPayment>();
            TblWarehouse = new HashSet<TblWarehouse>();
        }

        public long Id { get; set; }
        public long IdBusiness { get; set; }
        public bool Principal { get; set; }
        public string Name { get; set; }
        public byte BranchOfficeNumber { get; set; }
        public int? ProvinceId { get; set; }
        public string AddressBranchOffice { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneNumber2 { get; set; }
        public DateTime ModifyDate { get; set; }
        public DateTime CreateDate { get; set; }
        public long? EmployeeCreateId { get; set; }
        public long? EmployeeModifyId { get; set; }
        public bool Estatus { get; set; }
        public string Email { get; set; }

        public TblBusiness IdBusinessNavigation { get; set; }
        public TblProvince Province { get; set; }
        public ICollection<TblBranchOfficeSchedule> TblBranchOfficeSchedule { get; set; }
        public ICollection<TblCreditNote> TblCreditNote { get; set; }
        public ICollection<TblCustomerPayments> TblCustomerPayments { get; set; }
        public ICollection<TblDebitNote> TblDebitNote { get; set; }
        public ICollection<TblEmployeeBusiness> TblEmployeeBusiness { get; set; }
        public ICollection<TblInvoice> TblInvoice { get; set; }
        public ICollection<TblOrders> TblOrders { get; set; }
        public ICollection<TblPurchaseOrder> TblPurchaseOrder { get; set; }
        public ICollection<TblVendorPayment> TblVendorPayment { get; set; }
        public ICollection<TblWarehouse> TblWarehouse { get; set; }
    }
}
