﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblPhysicalInventory
    {
        public TblPhysicalInventory()
        {
            TblPhysicalInventoryRow = new HashSet<TblPhysicalInventoryRow>();
        }

        public int Id { get; set; }
        public long? UserId { get; set; }
        public long BusinessId { get; set; }
        public long? EmployeeBusinessId { get; set; }
        public long? CustomerId { get; set; }
        public int? WarehouseId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
        public decimal TotalProducts { get; set; }
        public decimal TotalCost { get; set; }
        public decimal TotalCostCounted { get; set; }
        public decimal? TotalCostDifference { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }

        public TblBusiness Business { get; set; }
        public TblEmployeeBusiness EmployeeBusiness { get; set; }
        public TblUsers User { get; set; }
        public TblWarehouse Warehouse { get; set; }
        public ICollection<TblPhysicalInventoryRow> TblPhysicalInventoryRow { get; set; }
    }
}
