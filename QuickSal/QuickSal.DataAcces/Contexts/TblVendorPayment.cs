﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblVendorPayment
    {
        public TblVendorPayment()
        {
            TblVendorPaymentRow = new HashSet<TblVendorPaymentRow>();
        }

        public long Id { get; set; }
        public long BranchOfficeId { get; set; }
        public byte DocumentEstatusId { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Observation { get; set; }
        public decimal Total { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }
        public bool? Used { get; set; }

        public TblBranchOffice BranchOffice { get; set; }
        public TblEmployeeBusiness EmployeeCreate { get; set; }
        public TblEmployeeBusiness EmployeeModify { get; set; }
        public ICollection<TblVendorPaymentRow> TblVendorPaymentRow { get; set; }
    }
}
