﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using QuickSal.DataAcces.Configuration;

namespace QuickSal.DataAcces.Contexts
{
    public partial class QuickSalContext : DbContext
    {
        public QuickSalContext()
        {
        }

        public QuickSalContext(DbContextOptions<QuickSalContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TblAppSettings> TblAppSettings { get; set; }
        public virtual DbSet<TblAppSettingValue> TblAppSettingValue { get; set; }
        public virtual DbSet<TblBanks> TblBanks { get; set; }
        public virtual DbSet<TblBranchOffice> TblBranchOffice { get; set; }
        public virtual DbSet<TblBranchOfficeSchedule> TblBranchOfficeSchedule { get; set; }
        public virtual DbSet<TblBrand> TblBrand { get; set; }
        public virtual DbSet<TblBusineesModules> TblBusineesModules { get; set; }
        public virtual DbSet<TblBusiness> TblBusiness { get; set; }
        public virtual DbSet<TblBusinessType> TblBusinessType { get; set; }
        public virtual DbSet<TblCategory> TblCategory { get; set; }
        public virtual DbSet<TblCountry> TblCountry { get; set; }
        public virtual DbSet<TblCreditNote> TblCreditNote { get; set; }
        public virtual DbSet<TblCreditNoteRow> TblCreditNoteRow { get; set; }
        public virtual DbSet<TblCretitNoteUsed> TblCretitNoteUsed { get; set; }
        public virtual DbSet<TblCustomerPayments> TblCustomerPayments { get; set; }
        public virtual DbSet<TblCustomerPaymentsRow> TblCustomerPaymentsRow { get; set; }
        public virtual DbSet<TblCustomers> TblCustomers { get; set; }
        public virtual DbSet<TblDebitNote> TblDebitNote { get; set; }
        public virtual DbSet<TblDebitNoteRow> TblDebitNoteRow { get; set; }
        public virtual DbSet<TblDocumentIdentification> TblDocumentIdentification { get; set; }
        public virtual DbSet<TblEmployeeBusiness> TblEmployeeBusiness { get; set; }
        public virtual DbSet<TblForms> TblForms { get; set; }
        public virtual DbSet<TblInventoryWarehouse> TblInventoryWarehouse { get; set; }
        public virtual DbSet<TblInvoice> TblInvoice { get; set; }
        public virtual DbSet<TblInvoiceRow> TblInvoiceRow { get; set; }
        public virtual DbSet<TblLogError> TblLogError { get; set; }
        public virtual DbSet<TblMeasure> TblMeasure { get; set; }
        public virtual DbSet<TblMovingWarehouse> TblMovingWarehouse { get; set; }
        public virtual DbSet<TblMovingWarehouseRow> TblMovingWarehouseRow { get; set; }
        public virtual DbSet<TblNcf> TblNcf { get; set; }
        public virtual DbSet<TblNcfprefix> TblNcfprefix { get; set; }
        public virtual DbSet<TblNcfrow> TblNcfrow { get; set; }
        public virtual DbSet<TblObject> TblObject { get; set; }
        public virtual DbSet<TblOrders> TblOrders { get; set; }
        public virtual DbSet<TblOrdersRow> TblOrdersRow { get; set; }
        public virtual DbSet<TblPaymentGateway> TblPaymentGateway { get; set; }
        public virtual DbSet<TblPhysicalInventory> TblPhysicalInventory { get; set; }
        public virtual DbSet<TblPhysicalInventoryRow> TblPhysicalInventoryRow { get; set; }
        public virtual DbSet<TblPlanDetails> TblPlanDetails { get; set; }
        public virtual DbSet<TblPlans> TblPlans { get; set; }
        public virtual DbSet<TblPlanSubObject> TblPlanSubObject { get; set; }
        public virtual DbSet<TblPrivileges> TblPrivileges { get; set; }
        public virtual DbSet<TblProducts> TblProducts { get; set; }
        public virtual DbSet<TblProductsPrices> TblProductsPrices { get; set; }
        public virtual DbSet<TblProvince> TblProvince { get; set; }
        public virtual DbSet<TblPurchaseOrder> TblPurchaseOrder { get; set; }
        public virtual DbSet<TblPurchaseOrderRow> TblPurchaseOrderRow { get; set; }
        public virtual DbSet<TblSegurity> TblSegurity { get; set; }
        public virtual DbSet<TblSubCategory> TblSubCategory { get; set; }
        public virtual DbSet<TblSubObject> TblSubObject { get; set; }
        public virtual DbSet<Tblsubscriptions> Tblsubscriptions { get; set; }
        public virtual DbSet<TblSuppliers> TblSuppliers { get; set; }
        public virtual DbSet<TblTax> TblTax { get; set; }
        public virtual DbSet<TblTempFile> TblTempFile { get; set; }
        public virtual DbSet<TblTypeMethod> TblTypeMethod { get; set; }
        public virtual DbSet<TblUserConfirmation> TblUserConfirmation { get; set; }
        public virtual DbSet<TblUsers> TblUsers { get; set; }
        public virtual DbSet<TblVendorPayment> TblVendorPayment { get; set; }
        public virtual DbSet<TblVendorPaymentRow> TblVendorPaymentRow { get; set; }
        public virtual DbSet<TblWarehouse> TblWarehouse { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            EntityModelConfiguration.ContinueModelCreating(modelBuilder);

            modelBuilder.Entity<TblAppSettings>(entity =>
            {
                entity.Property(e => e.SettingName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblAppSettingValue>(entity =>
            {
                entity.Property(e => e.SettingValue)
                    .IsRequired()
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.ValueName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAppSettingNavigation)
                    .WithMany(p => p.TblAppSettingValue)
                    .HasForeignKey(d => d.IdAppSetting)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblAppSettingValue_TblAppSettings");
            });

            modelBuilder.Entity<TblBanks>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblBranchOffice>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddressBranchOffice)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdBusiness).HasColumnName("idBusiness");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(18)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber2)
                    .HasMaxLength(18)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdBusinessNavigation)
                    .WithMany(p => p.TblBranchOffice)
                    .HasForeignKey(d => d.IdBusiness)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblBranchOffice_TblBusiness");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.TblBranchOffice)
                    .HasForeignKey(d => d.ProvinceId)
                    .HasConstraintName("FK_TblBranchOffice_TblProvince");
            });

            modelBuilder.Entity<TblBranchOfficeSchedule>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Estatus).HasColumnName("estatus");

                entity.Property(e => e.ToTime).HasColumnName("toTime");

                entity.HasOne(d => d.BranchOffice)
                    .WithMany(p => p.TblBranchOfficeSchedule)
                    .HasForeignKey(d => d.BranchOfficeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblBranchOfficeSchedule_TblBranchOffice");
            });

            modelBuilder.Entity<TblBrand>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.TblBrand)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_TblBrand_TblBusiness");
            });

            modelBuilder.Entity<TblBusineesModules>(entity =>
            {
                entity.HasOne(d => d.Business)
                    .WithMany(p => p.TblBusineesModules)
                    .HasForeignKey(d => d.BusinessId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblBusineesModules_TblBusiness");
            });

            modelBuilder.Entity<TblBusiness>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DateConstitution).HasColumnType("date");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(500);

                entity.Property(e => e.DocumentNumber)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Logo).IsUnicode(false);

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.WebPage).HasMaxLength(100);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.TblBusiness)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_TblBusiness_TblCountry");

                entity.HasOne(d => d.DocumentType)
                    .WithMany(p => p.TblBusiness)
                    .HasForeignKey(d => d.DocumentTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblBusiness_TblDocumentIdentification");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TblBusiness)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblBusiness_TblUsers");
            });

            modelBuilder.Entity<TblBusinessType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CoverPage).IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblCategory>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.TblCategory)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_TblCategory_TblBusiness");
            });

            modelBuilder.Entity<TblCountry>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<TblCreditNote>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.DocumentDate).HasColumnType("date");

                entity.Property(e => e.DocumentNumber)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Ncf)
                    .HasColumnName("NCF")
                    .HasMaxLength(22)
                    .IsUnicode(false);

                entity.Property(e => e.Ncfmodified)
                    .HasColumnName("NCFModified")
                    .HasMaxLength(22)
                    .IsUnicode(false);

                entity.Property(e => e.Observation)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.BranchOffice)
                    .WithMany(p => p.TblCreditNote)
                    .HasForeignKey(d => d.BranchOfficeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblCreditNote_TblBranchOffice");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.TblCreditNote)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblCreditNote_TblCustomers");

                entity.HasOne(d => d.EmployeeCreate)
                    .WithMany(p => p.TblCreditNote)
                    .HasForeignKey(d => d.EmployeeCreateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblCreditNote_TblEmployeeBusiness");

                entity.HasOne(d => d.Invoice)
                    .WithMany(p => p.TblCreditNote)
                    .HasForeignKey(d => d.InvoiceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblCreditNote_TblInvoice");
            });

            modelBuilder.Entity<TblCreditNoteRow>(entity =>
            {
                entity.Property(e => e.Discount).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.Tax).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.TaxAbbreviation)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreditNote)
                    .WithMany(p => p.TblCreditNoteRow)
                    .HasForeignKey(d => d.CreditNoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblCreditNoteRow_TblCreditNote");

                entity.HasOne(d => d.Measure)
                    .WithMany(p => p.TblCreditNoteRow)
                    .HasForeignKey(d => d.MeasureId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblCreditNoteRow_TblMeasure");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.TblCreditNoteRow)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblCreditNoteRow_TblProducts");
            });

            modelBuilder.Entity<TblCretitNoteUsed>(entity =>
            {
                entity.Property(e => e.InvoiceId).HasColumnName("InvoiceID");

                entity.HasOne(d => d.CreditNote)
                    .WithMany(p => p.TblCretitNoteUsed)
                    .HasForeignKey(d => d.CreditNoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblCretitNoteUsed_TblCreditNote");

                entity.HasOne(d => d.Invoice)
                    .WithMany(p => p.TblCretitNoteUsed)
                    .HasForeignKey(d => d.InvoiceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblCretitNoteUsed_TblInvoice");
            });

            modelBuilder.Entity<TblCustomerPayments>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.DocumentNumber)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Observation)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.BranchOffice)
                    .WithMany(p => p.TblCustomerPayments)
                    .HasForeignKey(d => d.BranchOfficeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblCustomerPayments_TblBranchOffice");

                entity.HasOne(d => d.EmployeeCreate)
                    .WithMany(p => p.TblCustomerPaymentsEmployeeCreate)
                    .HasForeignKey(d => d.EmployeeCreateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblCustomerPayments_TblEmployeeBusiness");

                entity.HasOne(d => d.EmployeeModify)
                    .WithMany(p => p.TblCustomerPaymentsEmployeeModify)
                    .HasForeignKey(d => d.EmployeeModifyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblCustomerPayments_TblEmployeeBusiness2");
            });

            modelBuilder.Entity<TblCustomerPaymentsRow>(entity =>
            {
                entity.Property(e => e.Concept)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceDocument)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.TblCustomerPaymentsRow)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblCustomerPaymentsRow_TblCustomers");

                entity.HasOne(d => d.CustomerPayment)
                    .WithMany(p => p.TblCustomerPaymentsRow)
                    .HasForeignKey(d => d.CustomerPaymentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblCustomerPaymentsRow_TblVendorPaymentRow");

                entity.HasOne(d => d.Invoice)
                    .WithMany(p => p.TblCustomerPaymentsRow)
                    .HasForeignKey(d => d.InvoiceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblCustomerPaymentsRow_TblInvoice");
            });

            modelBuilder.Entity<TblCustomers>(entity =>
            {
                entity.Property(e => e.AddressCustomers).HasMaxLength(200);

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.CreditLimit).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.Discount).HasColumnType("numeric(5, 2)");

                entity.Property(e => e.DocumentNumber)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EditDate).HasColumnType("date");

                entity.Property(e => e.Email)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Movil).HasMaxLength(15);

                entity.Property(e => e.Names)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Ncfid).HasColumnName("NCFId");

                entity.Property(e => e.Obervation)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.PendingBalance).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.PhoneNumber).HasMaxLength(18);

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.TblCustomers)
                    .HasForeignKey(d => d.BusinessId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblCustomers_TblBusiness");

                entity.HasOne(d => d.DocumentType)
                    .WithMany(p => p.TblCustomers)
                    .HasForeignKey(d => d.DocumentTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblCustomers_TblDocumentIdentification");

                entity.HasOne(d => d.Ncf)
                    .WithMany(p => p.TblCustomers)
                    .HasForeignKey(d => d.Ncfid)
                    .HasConstraintName("FK_TblCustomers_TblNCFPrefix");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.TblCustomers)
                    .HasForeignKey(d => d.ProvinceId)
                    .HasConstraintName("FK_TblCustomers_TblProvince");
            });

            modelBuilder.Entity<TblDebitNote>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.DocumentNumber)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Ncf)
                    .HasColumnName("NCF")
                    .HasMaxLength(22)
                    .IsUnicode(false);

                entity.Property(e => e.Ncfmodified)
                    .HasColumnName("NCFModified")
                    .HasMaxLength(22)
                    .IsUnicode(false);

                entity.Property(e => e.Observation)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.BranchOffice)
                    .WithMany(p => p.TblDebitNote)
                    .HasForeignKey(d => d.BranchOfficeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblDebitNote_TblBranchOffice");

                entity.HasOne(d => d.EmployeeCreate)
                    .WithMany(p => p.TblDebitNoteEmployeeCreate)
                    .HasForeignKey(d => d.EmployeeCreateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblDebitNote_TblEmployeeBusiness");

                entity.HasOne(d => d.EmployeeModify)
                    .WithMany(p => p.TblDebitNoteEmployeeModify)
                    .HasForeignKey(d => d.EmployeeModifyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblDebitNote_TblEmployeeBusiness2");

                entity.HasOne(d => d.PurchaseOrder)
                    .WithMany(p => p.TblDebitNote)
                    .HasForeignKey(d => d.PurchaseOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblDebitNote_TblPurchaseOrder");

                entity.HasOne(d => d.Suppliers)
                    .WithMany(p => p.TblDebitNote)
                    .HasForeignKey(d => d.SuppliersId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblDebitNote_TblSuppliers");
            });

            modelBuilder.Entity<TblDebitNoteRow>(entity =>
            {
                entity.Property(e => e.Discount).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.Tax).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.TaxAbbreviation)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.DebitNote)
                    .WithMany(p => p.TblDebitNoteRow)
                    .HasForeignKey(d => d.DebitNoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblDebitNoteRow_TblPurchaseOrder");

                entity.HasOne(d => d.Measure)
                    .WithMany(p => p.TblDebitNoteRow)
                    .HasForeignKey(d => d.MeasureId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblDebitNoteRow_TblMeasure");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.TblDebitNoteRow)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblDebitNoteRow_TblProducts");
            });

            modelBuilder.Entity<TblDocumentIdentification>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Mask)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblEmployeeBusiness>(entity =>
            {
                entity.Property(e => e.BirthDate).HasColumnType("date");

                entity.Property(e => e.CellPhone)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Direction)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EditDate).HasColumnType("date");

                entity.Property(e => e.EmployeeCode)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.InstitutionalEmail)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IntoDate).HasColumnType("date");

                entity.Property(e => e.LastName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.OutDate).HasColumnType("date");

                entity.Property(e => e.Phone)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Picture).IsUnicode(false);

                entity.Property(e => e.Position)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Salary).HasColumnType("numeric(18, 2)");

                entity.HasOne(d => d.BranchOffice)
                    .WithMany(p => p.TblEmployeeBusiness)
                    .HasForeignKey(d => d.BranchOfficeId)
                    .HasConstraintName("FK_TblEmployeeBusiness_TblBranchOffice");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.TblEmployeeBusiness)
                    .HasForeignKey(d => d.BusinessId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblEmployeeBusiness_TblBusiness");

                entity.HasOne(d => d.DocumentType)
                    .WithMany(p => p.TblEmployeeBusiness)
                    .HasForeignKey(d => d.DocumentTypeId)
                    .HasConstraintName("FK_TblEmployeeBusiness_TblDocumentIdentification");

                entity.HasOne(d => d.Privileges)
                    .WithMany(p => p.TblEmployeeBusiness)
                    .HasForeignKey(d => d.PrivilegesId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblEmployeeBusiness_TblPrivileges");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.TblEmployeeBusiness)
                    .HasForeignKey(d => d.ProvinceId)
                    .HasConstraintName("FK_TblEmployeeBusiness_TblProvince");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TblEmployeeBusiness)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblEmployeeBusiness_TblUsers");
            });

            modelBuilder.Entity<TblForms>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Description).HasMaxLength(256);
            });

            modelBuilder.Entity<TblInventoryWarehouse>(entity =>
            {
                entity.HasOne(d => d.Products)
                    .WithMany(p => p.TblInventoryWarehouse)
                    .HasForeignKey(d => d.ProductsId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblInventoryWarehouse_TblProducts");

                entity.HasOne(d => d.Warehouse)
                    .WithMany(p => p.TblInventoryWarehouse)
                    .HasForeignKey(d => d.WarehouseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblInventoryWarehouse_TblWarehouse");
            });

            modelBuilder.Entity<TblInvoice>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.DocumentDate).HasColumnType("date");

                entity.Property(e => e.DocumentNumber)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ExpirationDate).HasColumnType("date");

                entity.Property(e => e.Ncf)
                    .HasColumnName("NCF")
                    .HasMaxLength(22)
                    .IsUnicode(false);

                entity.Property(e => e.Observation)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Reference)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.BranchOffice)
                    .WithMany(p => p.TblInvoice)
                    .HasForeignKey(d => d.BranchOfficeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblInvoice_TblBranchOffice");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.TblInvoice)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK_TblInvoice_TblCustomers");
            });

            modelBuilder.Entity<TblInvoiceRow>(entity =>
            {
                entity.Property(e => e.AmountDiscount).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.AmountTax).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.Discount).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.Measure)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Price).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.Tax).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.TaxAbbreviation)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Total).HasColumnType("decimal(18, 4)");

                entity.HasOne(d => d.Invoice)
                    .WithMany(p => p.TblInvoiceRow)
                    .HasForeignKey(d => d.InvoiceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblInvoiceRow_TblPurchaseOrder");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.TblInvoiceRow)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblInvoiceRow_TblProducts");

                entity.HasOne(d => d.Warehouse)
                    .WithMany(p => p.TblInvoiceRow)
                    .HasForeignKey(d => d.WarehouseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblInvoiceRow_TblWarehouse");
            });

            modelBuilder.Entity<TblLogError>(entity =>
            {
                entity.ToTable("tblLogError");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.ErrorMessage)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Location)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Method)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SystemProccess)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblMeasure>(entity =>
            {
                entity.Property(e => e.Abbreviation)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.TblMeasure)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_TblMeasure_TblBusiness");
            });

            modelBuilder.Entity<TblMovingWarehouse>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Description).IsRequired();

                entity.Property(e => e.DocumentNo)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.HasOne(d => d.EmployeeCreate)
                    .WithMany(p => p.TblMovingWarehouseEmployeeCreate)
                    .HasForeignKey(d => d.EmployeeCreateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblMovingWarehouse_TblEmployeeBusiness1");

                entity.HasOne(d => d.EmployeeModify)
                    .WithMany(p => p.TblMovingWarehouseEmployeeModify)
                    .HasForeignKey(d => d.EmployeeModifyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblMovingWarehouse_TblEmployeeBusiness2");

                entity.HasOne(d => d.ReceiverWarehouse)
                    .WithMany(p => p.TblMovingWarehouse)
                    .HasForeignKey(d => d.ReceiverWarehouseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblMovingWarehouse_TblWarehouse2");
            });

            modelBuilder.Entity<TblMovingWarehouseRow>(entity =>
            {
                entity.Property(e => e.ProductName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Moving)
                    .WithMany(p => p.TblMovingWarehouseRow)
                    .HasForeignKey(d => d.MovingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblMovingWarehouseRow_TblMovingWarehouse");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.TblMovingWarehouseRow)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblMovingWarehouseRow_TblProducts");
            });

            modelBuilder.Entity<TblNcf>(entity =>
            {
                entity.ToTable("TblNCF");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.TblNcf)
                    .HasForeignKey(d => d.BusinessId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblNCF_TblBusiness");

                entity.HasOne(d => d.Prefix)
                    .WithMany(p => p.TblNcf)
                    .HasForeignKey(d => d.PrefixId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblNCF_TblNCFPrefix");
            });

            modelBuilder.Entity<TblNcfprefix>(entity =>
            {
                entity.ToTable("TblNCFPrefix");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Prefix)
                    .IsRequired()
                    .HasMaxLength(11)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblNcfrow>(entity =>
            {
                entity.ToTable("TblNCFRow");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ncf)
                    .IsRequired()
                    .HasColumnName("NCF")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Ncfid).HasColumnName("NCFId");

                entity.HasOne(d => d.NcfNavigation)
                    .WithMany(p => p.TblNcfrow)
                    .HasForeignKey(d => d.Ncfid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblNCFRow_TblNCF");
            });

            modelBuilder.Entity<TblObject>(entity =>
            {
                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Ico)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TblOrders>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.DocumentNumber)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Observation)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.BranchOffice)
                    .WithMany(p => p.TblOrders)
                    .HasForeignKey(d => d.BranchOfficeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblOrders_TblBranchOffice");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.TblOrders)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK_TblOrders_TblCustomers");

                entity.HasOne(d => d.Invoice)
                    .WithMany(p => p.TblOrders)
                    .HasForeignKey(d => d.InvoiceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblOrders_TblInvoice");
            });

            modelBuilder.Entity<TblOrdersRow>(entity =>
            {
                entity.Property(e => e.Measure)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Note)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.TblOrdersRow)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblOrdersRow_TblOrders");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.TblOrdersRow)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblOrdersRow_TblProducts");
            });

            modelBuilder.Entity<TblPaymentGateway>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblPhysicalInventory>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EndDate).HasColumnType("date");

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.TblPhysicalInventory)
                    .HasForeignKey(d => d.BusinessId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblPhysicalInventory_TblBusiness");

                entity.HasOne(d => d.EmployeeBusiness)
                    .WithMany(p => p.TblPhysicalInventory)
                    .HasForeignKey(d => d.EmployeeBusinessId)
                    .HasConstraintName("FK_TblPhysicalInventory_TblEmployeeBusiness");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TblPhysicalInventory)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_TblPhysicalInventory_TblUsers");

                entity.HasOne(d => d.Warehouse)
                    .WithMany(p => p.TblPhysicalInventory)
                    .HasForeignKey(d => d.WarehouseId)
                    .HasConstraintName("FK_TblPhysicalInventory_TblWarehouse");
            });

            modelBuilder.Entity<TblPhysicalInventoryRow>(entity =>
            {
                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Measure)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.PhysicalInventory)
                    .WithMany(p => p.TblPhysicalInventoryRow)
                    .HasForeignKey(d => d.PhysicalInventoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblPhysicalInventoryRow_TblPhysicalInventory");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.TblPhysicalInventoryRow)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblPhysicalInventoryRow_TblProducts");
            });

            modelBuilder.Entity<TblPlanDetails>(entity =>
            {
                entity.Property(e => e.BenefitDetail)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.Plan)
                    .WithMany(p => p.TblPlanDetails)
                    .HasForeignKey(d => d.PlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblPlanDetails_tblplans");
            });

            modelBuilder.Entity<TblPlans>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdBusinessTypeNavigation)
                    .WithMany(p => p.TblPlans)
                    .HasForeignKey(d => d.IdBusinessType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblPlans_TblBusinessType");
            });

            modelBuilder.Entity<TblPlanSubObject>(entity =>
            {
                entity.HasOne(d => d.Plan)
                    .WithMany(p => p.TblPlanSubObject)
                    .HasForeignKey(d => d.PlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblPlanSubObject_TblPlans");

                entity.HasOne(d => d.SubObject)
                    .WithMany(p => p.TblPlanSubObject)
                    .HasForeignKey(d => d.SubObjectId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblPlanSubObject_TblSubObject");
            });

            modelBuilder.Entity<TblPrivileges>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.EditDate).HasColumnType("date");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.TblPrivileges)
                    .HasForeignKey(d => d.BusinessId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblPrivileges_TblBusiness");
            });

            modelBuilder.Entity<TblProducts>(entity =>
            {
                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Observation)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Picture).IsUnicode(false);

                entity.Property(e => e.Reference)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.Brand)
                    .WithMany(p => p.TblProducts)
                    .HasForeignKey(d => d.BrandId)
                    .HasConstraintName("FK_TblProducts_TblBrand");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.TblProducts)
                    .HasForeignKey(d => d.BusinessId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblProducts_TblBusiness");

                entity.HasOne(d => d.Measure)
                    .WithMany(p => p.TblProducts)
                    .HasForeignKey(d => d.MeasureId)
                    .HasConstraintName("FK_TblProducts_TblMeasure");

                entity.HasOne(d => d.SubCategory)
                    .WithMany(p => p.TblProducts)
                    .HasForeignKey(d => d.SubCategoryId)
                    .HasConstraintName("FK_TblProducts_TblSubCategory");

                entity.HasOne(d => d.Supplier)
                    .WithMany(p => p.TblProducts)
                    .HasForeignKey(d => d.SupplierId)
                    .HasConstraintName("FK_TblProducts_TblSuppliers");

                entity.HasOne(d => d.Tax)
                    .WithMany(p => p.TblProducts)
                    .HasForeignKey(d => d.TaxId)
                    .HasConstraintName("FK_TblProducts_TblTax");
            });

            modelBuilder.Entity<TblProductsPrices>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Utility).HasColumnType("decimal(6, 2)");

                entity.HasOne(d => d.Products)
                    .WithMany(p => p.TblProductsPrices)
                    .HasForeignKey(d => d.ProductsId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblProductsPrices_TblProducts");

                entity.HasOne(d => d.Tax)
                    .WithMany(p => p.TblProductsPrices)
                    .HasForeignKey(d => d.TaxId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblProductsPrices_TblTax");
            });

            modelBuilder.Entity<TblProvince>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.TblProvince)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblProvince_TblCountry");
            });

            modelBuilder.Entity<TblPurchaseOrder>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.DocumentDate).HasColumnType("date");

                entity.Property(e => e.DocumentNumber)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ExpirationDate).HasColumnType("date");

                entity.Property(e => e.Ncf)
                    .HasColumnName("NCF")
                    .HasMaxLength(22)
                    .IsUnicode(false);

                entity.Property(e => e.Observation)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Reference)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.BranchOffice)
                    .WithMany(p => p.TblPurchaseOrder)
                    .HasForeignKey(d => d.BranchOfficeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblPurchaseOrder_TblBranchOffice");

                entity.HasOne(d => d.EmployeeBusiness)
                    .WithMany(p => p.TblPurchaseOrder)
                    .HasForeignKey(d => d.EmployeeBusinessId)
                    .HasConstraintName("FK_TblPurchaseOrder_TblEmployeeBusiness");

                entity.HasOne(d => d.Suppliers)
                    .WithMany(p => p.TblPurchaseOrder)
                    .HasForeignKey(d => d.SuppliersId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblPurchaseOrder_TblSuppliers");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TblPurchaseOrder)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_TblPurchaseOrder_TblUsers");
            });

            modelBuilder.Entity<TblPurchaseOrderRow>(entity =>
            {
                entity.Property(e => e.Discount).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.Tax).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.TaxAbbreviation)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.Measure)
                    .WithMany(p => p.TblPurchaseOrderRow)
                    .HasForeignKey(d => d.MeasureId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblPurchaseOrderRow_TblMeasure");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.TblPurchaseOrderRow)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblPurchaseOrderRow_TblProducts");

                entity.HasOne(d => d.PurchaseOrder)
                    .WithMany(p => p.TblPurchaseOrderRow)
                    .HasForeignKey(d => d.PurchaseOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblPurchaseOrderRow_TblPurchaseOrder");

                entity.HasOne(d => d.Warehouse)
                    .WithMany(p => p.TblPurchaseOrderRow)
                    .HasForeignKey(d => d.WarehouseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblPurchaseOrderRow_TblWarehouse");
            });

            modelBuilder.Entity<TblSegurity>(entity =>
            {
                entity.HasOne(d => d.Privileges)
                    .WithMany(p => p.TblSegurity)
                    .HasForeignKey(d => d.PrivilegesId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblSegurity_TblPrivileges");

                entity.HasOne(d => d.TypeMethod)
                    .WithMany(p => p.TblSegurity)
                    .HasForeignKey(d => d.TypeMethodId)
                    .HasConstraintName("FK_TblSegurity_TblTypeMethod");
            });

            modelBuilder.Entity<TblSubCategory>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.TblSubCategory)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_TblSubCategory_TblBusiness");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.TblSubCategory)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblSubCategory_TblSubCategory");
            });

            modelBuilder.Entity<TblSubObject>(entity =>
            {
                entity.Property(e => e.AplicationDescription)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Controller)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.Ico).HasMaxLength(50);

                entity.Property(e => e.Method)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.UrlModules)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.WebDescription)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.Object)
                    .WithMany(p => p.TblSubObject)
                    .HasForeignKey(d => d.ObjectId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblSubObject_TblObject");

                entity.HasOne(d => d.TypeMethod)
                    .WithMany(p => p.TblSubObject)
                    .HasForeignKey(d => d.TypeMethodId)
                    .HasConstraintName("FK_TblSubObject_TblTypeMethod");
            });

            modelBuilder.Entity<Tblsubscriptions>(entity =>
            {
                entity.ToTable("TBLSubscriptions");

                entity.Property(e => e.DateEnd).HasColumnType("datetime");

                entity.Property(e => e.DateStart)
                    .HasColumnName("dateStart")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdPlan).HasColumnName("idPlan");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.Tblsubscriptions)
                    .HasForeignKey(d => d.BusinessId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TBLSubscriptions_TblBusiness");

                entity.HasOne(d => d.IdPlanNavigation)
                    .WithMany(p => p.Tblsubscriptions)
                    .HasForeignKey(d => d.IdPlan)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TBLSubscriptions_TblPlans");
            });

            modelBuilder.Entity<TblSuppliers>(entity =>
            {
                entity.Property(e => e.AddressSuppliers).HasMaxLength(200);

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.ContactPerson).HasMaxLength(200);

                entity.Property(e => e.CreationDate).HasColumnType("date");

                entity.Property(e => e.DocumentNumber)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Movil).HasMaxLength(15);

                entity.Property(e => e.Names)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Ncfid).HasColumnName("NCFId");

                entity.Property(e => e.Obervation)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Observation).HasMaxLength(200);

                entity.Property(e => e.PendingBalance).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.PhoneNumber).HasMaxLength(18);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.TblSuppliers)
                    .HasForeignKey(d => d.BusinessId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblSuppliers_TblBusiness");

                entity.HasOne(d => d.DocumentType)
                    .WithMany(p => p.TblSuppliers)
                    .HasForeignKey(d => d.DocumentTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblSuppliers_TblDocumentIdentification");

                entity.HasOne(d => d.Ncf)
                    .WithMany(p => p.TblSuppliers)
                    .HasForeignKey(d => d.Ncfid)
                    .HasConstraintName("FK_TblSuppliers_TblNCFPrefix");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.TblSuppliers)
                    .HasForeignKey(d => d.ProvinceId)
                    .HasConstraintName("FK_TblSuppliers_TblProvince");
            });

            modelBuilder.Entity<TblTax>(entity =>
            {
                entity.Property(e => e.Abbreviation)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Quantity).HasColumnType("numeric(5, 2)");

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.TblTax)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_TblTax_TblBusiness");
            });

            modelBuilder.Entity<TblTempFile>(entity =>
            {
                entity.Property(e => e.FileName)
                    .IsRequired()
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TblTempFile)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblTempFile_TblUsers");
            });

            modelBuilder.Entity<TblTypeMethod>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblUserConfirmation>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ConfirmationKey)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.ExpirationKey).HasColumnType("date");

                entity.Property(e => e.IdForms).HasColumnName("idForms");

                entity.HasOne(d => d.IdFormsNavigation)
                    .WithMany(p => p.TblUserConfirmation)
                    .HasForeignKey(d => d.IdForms)
                    .HasConstraintName("FK__TblUserCo__idFor__793DFFAF");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TblUserConfirmation)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblUserConfirmation_TblUsers");
            });

            modelBuilder.Entity<TblUsers>(entity =>
            {
                entity.Property(e => e.BankAccount).HasMaxLength(30);

                entity.Property(e => e.BirthDate).HasColumnType("date");

                entity.Property(e => e.CellPhone)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Direction)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmailPaymentGateway)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FacebookId)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Picture).IsUnicode(false);

                entity.Property(e => e.Telephone)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.Property(e => e.UserPassword)
                    .IsRequired()
                    .IsUnicode(false);

                entity.HasOne(d => d.Bank)
                    .WithMany(p => p.TblUsers)
                    .HasForeignKey(d => d.BankId)
                    .HasConstraintName("FK_TblUsers_TblBanks");

                entity.HasOne(d => d.DocumentType)
                    .WithMany(p => p.TblUsers)
                    .HasForeignKey(d => d.DocumentTypeId)
                    .HasConstraintName("FK_TblUsers_TblDocumentIdentification");

                entity.HasOne(d => d.PaymentGateway)
                    .WithMany(p => p.TblUsers)
                    .HasForeignKey(d => d.PaymentGatewayId)
                    .HasConstraintName("FK_tblusers_TblPaymentGateway");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.TblUsers)
                    .HasForeignKey(d => d.ProvinceId)
                    .HasConstraintName("FK_TblUsers_TblProvince");
            });

            modelBuilder.Entity<TblVendorPayment>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.DocumentNumber)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Observation)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.BranchOffice)
                    .WithMany(p => p.TblVendorPayment)
                    .HasForeignKey(d => d.BranchOfficeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblVendorPayment_TblBranchOffice");

                entity.HasOne(d => d.EmployeeCreate)
                    .WithMany(p => p.TblVendorPaymentEmployeeCreate)
                    .HasForeignKey(d => d.EmployeeCreateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblVendorPayment_TblEmployeeBusiness");

                entity.HasOne(d => d.EmployeeModify)
                    .WithMany(p => p.TblVendorPaymentEmployeeModify)
                    .HasForeignKey(d => d.EmployeeModifyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblVendorPayment_TblEmployeeBusiness2");
            });

            modelBuilder.Entity<TblVendorPaymentRow>(entity =>
            {
                entity.Property(e => e.Concept)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PurchaseOrderDocument)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SupplierName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.PurchaseOrder)
                    .WithMany(p => p.TblVendorPaymentRow)
                    .HasForeignKey(d => d.PurchaseOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblVendorPaymentRow_TblPurchaseOrder");

                entity.HasOne(d => d.Suppliers)
                    .WithMany(p => p.TblVendorPaymentRow)
                    .HasForeignKey(d => d.SuppliersId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblVendorPaymentRow_TblSuppliers");

                entity.HasOne(d => d.VendorPayment)
                    .WithMany(p => p.TblVendorPaymentRow)
                    .HasForeignKey(d => d.VendorPaymentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblVendorPayment_TblVendorPaymentRow");
            });

            modelBuilder.Entity<TblWarehouse>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.BranchOffice)
                    .WithMany(p => p.TblWarehouse)
                    .HasForeignKey(d => d.BranchOfficeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TblWarehouse_TblBranchOffice");
            });
        }
    }
}
