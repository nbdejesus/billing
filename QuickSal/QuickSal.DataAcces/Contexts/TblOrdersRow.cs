﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblOrdersRow
    {
        public long Id { get; set; }
        public long OrderId { get; set; }
        public long ProductId { get; set; }
        public string Measure { get; set; }
        public decimal Quantity { get; set; }
        public string Note { get; set; }

        public TblOrders Order { get; set; }
        public TblProducts Product { get; set; }
    }
}
