﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblVendorPaymentRow
    {
        public long Id { get; set; }
        public long VendorPaymentId { get; set; }
        public long SuppliersId { get; set; }
        public long PurchaseOrderId { get; set; }
        public string PurchaseOrderDocument { get; set; }
        public string SupplierName { get; set; }
        public string Concept { get; set; }
        public decimal Owed { get; set; }
        public decimal PaidOut { get; set; }
        public decimal Payment { get; set; }
        public decimal Pending { get; set; }

        public TblPurchaseOrder PurchaseOrder { get; set; }
        public TblSuppliers Suppliers { get; set; }
        public TblVendorPayment VendorPayment { get; set; }
    }
}
