﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblBusiness
    {
        public TblBusiness()
        {
            TblBranchOffice = new HashSet<TblBranchOffice>();
            TblBrand = new HashSet<TblBrand>();
            TblBusineesModules = new HashSet<TblBusineesModules>();
            TblCategory = new HashSet<TblCategory>();
            TblCustomers = new HashSet<TblCustomers>();
            TblEmployeeBusiness = new HashSet<TblEmployeeBusiness>();
            TblMeasure = new HashSet<TblMeasure>();
            TblNcf = new HashSet<TblNcf>();
            TblPhysicalInventory = new HashSet<TblPhysicalInventory>();
            TblPrivileges = new HashSet<TblPrivileges>();
            TblProducts = new HashSet<TblProducts>();
            TblSubCategory = new HashSet<TblSubCategory>();
            TblSuppliers = new HashSet<TblSuppliers>();
            TblTax = new HashSet<TblTax>();
            Tblsubscriptions = new HashSet<Tblsubscriptions>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public long UserId { get; set; }
        public short? CountryId { get; set; }
        public byte DocumentTypeId { get; set; }
        public string DocumentNumber { get; set; }
        public short IdBusinessType { get; set; }
        public string WebPage { get; set; }
        public string Description { get; set; }
        public DateTime ModifyDate { get; set; }
        public DateTime CreateDate { get; set; }
        public string Logo { get; set; }
        public bool Estatus { get; set; }
        public DateTime? DateConstitution { get; set; }

        public TblCountry Country { get; set; }
        public TblDocumentIdentification DocumentType { get; set; }
        public TblUsers User { get; set; }
        public ICollection<TblBranchOffice> TblBranchOffice { get; set; }
        public ICollection<TblBrand> TblBrand { get; set; }
        public ICollection<TblBusineesModules> TblBusineesModules { get; set; }
        public ICollection<TblCategory> TblCategory { get; set; }
        public ICollection<TblCustomers> TblCustomers { get; set; }
        public ICollection<TblEmployeeBusiness> TblEmployeeBusiness { get; set; }
        public ICollection<TblMeasure> TblMeasure { get; set; }
        public ICollection<TblNcf> TblNcf { get; set; }
        public ICollection<TblPhysicalInventory> TblPhysicalInventory { get; set; }
        public ICollection<TblPrivileges> TblPrivileges { get; set; }
        public ICollection<TblProducts> TblProducts { get; set; }
        public ICollection<TblSubCategory> TblSubCategory { get; set; }
        public ICollection<TblSuppliers> TblSuppliers { get; set; }
        public ICollection<TblTax> TblTax { get; set; }
        public ICollection<Tblsubscriptions> Tblsubscriptions { get; set; }
    }
}
