﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblTypeMethod
    {
        public TblTypeMethod()
        {
            TblSegurity = new HashSet<TblSegurity>();
            TblSubObject = new HashSet<TblSubObject>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }

        public ICollection<TblSegurity> TblSegurity { get; set; }
        public ICollection<TblSubObject> TblSubObject { get; set; }
    }
}
