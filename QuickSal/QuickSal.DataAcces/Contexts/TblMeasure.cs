﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblMeasure
    {
        public TblMeasure()
        {
            TblCreditNoteRow = new HashSet<TblCreditNoteRow>();
            TblDebitNoteRow = new HashSet<TblDebitNoteRow>();
            TblProducts = new HashSet<TblProducts>();
            TblPurchaseOrderRow = new HashSet<TblPurchaseOrderRow>();
        }

        public int Id { get; set; }
        public long? BusinessId { get; set; }
        public string Abbreviation { get; set; }
        public string Name { get; set; }
        public decimal Quantity { get; set; }
        public bool Estatus { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool IsCustom { get; set; }
        public long? EmployeeCreateId { get; set; }
        public long? EmployeeModifyId { get; set; }

        public TblBusiness Business { get; set; }
        public ICollection<TblCreditNoteRow> TblCreditNoteRow { get; set; }
        public ICollection<TblDebitNoteRow> TblDebitNoteRow { get; set; }
        public ICollection<TblProducts> TblProducts { get; set; }
        public ICollection<TblPurchaseOrderRow> TblPurchaseOrderRow { get; set; }
    }
}
