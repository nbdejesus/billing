﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblCustomers
    {
        public TblCustomers()
        {
            TblCreditNote = new HashSet<TblCreditNote>();
            TblCustomerPaymentsRow = new HashSet<TblCustomerPaymentsRow>();
            TblInvoice = new HashSet<TblInvoice>();
            TblOrders = new HashSet<TblOrders>();
        }

        public long Id { get; set; }
        public long BusinessId { get; set; }
        public byte DocumentTypeId { get; set; }
        public string DocumentNumber { get; set; }
        public string Code { get; set; }
        public string Names { get; set; }
        public string PhoneNumber { get; set; }
        public string Movil { get; set; }
        public string AddressCustomers { get; set; }
        public string Picture { get; set; }
        public int? CountryId { get; set; }
        public int? ProvinceId { get; set; }
        public bool Estatus { get; set; }
        public string Email { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public string Obervation { get; set; }
        public decimal? CreditLimit { get; set; }
        public decimal? PendingBalance { get; set; }
        public short? Ncfid { get; set; }
        public decimal? Discount { get; set; }
        public short? CreditTypeId { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }

        public TblBusiness Business { get; set; }
        public TblDocumentIdentification DocumentType { get; set; }
        public TblNcfprefix Ncf { get; set; }
        public TblProvince Province { get; set; }
        public ICollection<TblCreditNote> TblCreditNote { get; set; }
        public ICollection<TblCustomerPaymentsRow> TblCustomerPaymentsRow { get; set; }
        public ICollection<TblInvoice> TblInvoice { get; set; }
        public ICollection<TblOrders> TblOrders { get; set; }
    }
}
