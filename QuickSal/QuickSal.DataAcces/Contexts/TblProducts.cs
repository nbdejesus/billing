﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblProducts
    {
        public TblProducts()
        {
            TblCreditNoteRow = new HashSet<TblCreditNoteRow>();
            TblDebitNoteRow = new HashSet<TblDebitNoteRow>();
            TblInventoryWarehouse = new HashSet<TblInventoryWarehouse>();
            TblInvoiceRow = new HashSet<TblInvoiceRow>();
            TblMovingWarehouseRow = new HashSet<TblMovingWarehouseRow>();
            TblOrdersRow = new HashSet<TblOrdersRow>();
            TblPhysicalInventoryRow = new HashSet<TblPhysicalInventoryRow>();
            TblProductsPrices = new HashSet<TblProductsPrices>();
            TblPurchaseOrderRow = new HashSet<TblPurchaseOrderRow>();
        }

        public long Id { get; set; }
        public long BusinessId { get; set; }
        public int? BrandId { get; set; }
        public long? SupplierId { get; set; }
        public int? SubCategoryId { get; set; }
        public int? MeasureId { get; set; }
        public int? TaxId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsService { get; set; }
        public bool InvoiceWithoutExistence { get; set; }
        public decimal MinimumAmount { get; set; }
        public decimal MaximumQuantity { get; set; }
        public decimal Cost { get; set; }
        public bool Estatus { get; set; }
        public string Reference { get; set; }
        public string Picture { get; set; }
        public string Observation { get; set; }
        public decimal LimitAmountReport { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool SellLowCost { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }

        public TblBrand Brand { get; set; }
        public TblBusiness Business { get; set; }
        public TblMeasure Measure { get; set; }
        public TblSubCategory SubCategory { get; set; }
        public TblSuppliers Supplier { get; set; }
        public TblTax Tax { get; set; }
        public ICollection<TblCreditNoteRow> TblCreditNoteRow { get; set; }
        public ICollection<TblDebitNoteRow> TblDebitNoteRow { get; set; }
        public ICollection<TblInventoryWarehouse> TblInventoryWarehouse { get; set; }
        public ICollection<TblInvoiceRow> TblInvoiceRow { get; set; }
        public ICollection<TblMovingWarehouseRow> TblMovingWarehouseRow { get; set; }
        public ICollection<TblOrdersRow> TblOrdersRow { get; set; }
        public ICollection<TblPhysicalInventoryRow> TblPhysicalInventoryRow { get; set; }
        public ICollection<TblProductsPrices> TblProductsPrices { get; set; }
        public ICollection<TblPurchaseOrderRow> TblPurchaseOrderRow { get; set; }
    }
}
