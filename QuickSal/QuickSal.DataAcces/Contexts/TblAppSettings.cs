﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblAppSettings
    {
        public TblAppSettings()
        {
            TblAppSettingValue = new HashSet<TblAppSettingValue>();
        }

        public short Id { get; set; }
        public string SettingName { get; set; }

        public ICollection<TblAppSettingValue> TblAppSettingValue { get; set; }
    }
}
