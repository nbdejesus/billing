﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblBrand
    {
        public TblBrand()
        {
            TblProducts = new HashSet<TblProducts>();
        }

        public int Id { get; set; }
        public long? BusinessId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool Estatus { get; set; }
        public bool IsCustom { get; set; }
        public long? EmployeeCreateId { get; set; }
        public long? EmployeeModifyId { get; set; }

        public TblBusiness Business { get; set; }
        public ICollection<TblProducts> TblProducts { get; set; }
    }
}
