﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblPlans
    {
        public TblPlans()
        {
            TblPlanDetails = new HashSet<TblPlanDetails>();
            TblPlanSubObject = new HashSet<TblPlanSubObject>();
            Tblsubscriptions = new HashSet<Tblsubscriptions>();
        }

        public short Id { get; set; }
        public short IdBusinessType { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public bool Estatus { get; set; }
        public byte DaysOfPlan { get; set; }
        public byte Currency { get; set; }
        public decimal Price { get; set; }

        public TblBusinessType IdBusinessTypeNavigation { get; set; }
        public ICollection<TblPlanDetails> TblPlanDetails { get; set; }
        public ICollection<TblPlanSubObject> TblPlanSubObject { get; set; }
        public ICollection<Tblsubscriptions> Tblsubscriptions { get; set; }
    }
}
