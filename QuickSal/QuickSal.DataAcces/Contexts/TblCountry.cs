﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblCountry
    {
        public TblCountry()
        {
            TblBusiness = new HashSet<TblBusiness>();
            TblProvince = new HashSet<TblProvince>();
        }

        public short Id { get; set; }
        public string Description { get; set; }

        public ICollection<TblBusiness> TblBusiness { get; set; }
        public ICollection<TblProvince> TblProvince { get; set; }
    }
}
