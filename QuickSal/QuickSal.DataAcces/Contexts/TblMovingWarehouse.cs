﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblMovingWarehouse
    {
        public TblMovingWarehouse()
        {
            TblMovingWarehouseRow = new HashSet<TblMovingWarehouseRow>();
        }

        public long Id { get; set; }
        public string DocumentNo { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }
        public int SenderWarehouseId { get; set; }
        public int ReceiverWarehouseId { get; set; }
        public string Description { get; set; }
        public int State { get; set; }

        public TblEmployeeBusiness EmployeeCreate { get; set; }
        public TblEmployeeBusiness EmployeeModify { get; set; }
        public TblWarehouse ReceiverWarehouse { get; set; }
        public ICollection<TblMovingWarehouseRow> TblMovingWarehouseRow { get; set; }
    }
}
