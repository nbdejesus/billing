﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblForms
    {
        public TblForms()
        {
            TblUserConfirmation = new HashSet<TblUserConfirmation>();
        }

        public byte Id { get; set; }
        public string Description { get; set; }

        public ICollection<TblUserConfirmation> TblUserConfirmation { get; set; }
    }
}
