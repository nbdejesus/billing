﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblUsers
    {
        public TblUsers()
        {
            TblBusiness = new HashSet<TblBusiness>();
            TblEmployeeBusiness = new HashSet<TblEmployeeBusiness>();
            TblPhysicalInventory = new HashSet<TblPhysicalInventory>();
            TblPurchaseOrder = new HashSet<TblPurchaseOrder>();
            TblTempFile = new HashSet<TblTempFile>();
            TblUserConfirmation = new HashSet<TblUserConfirmation>();
        }

        public long Id { get; set; }
        public int? CountryId { get; set; }
        public int? ProvinceId { get; set; }
        public byte? DocumentTypeId { get; set; }
        public short? BankId { get; set; }
        public byte? PaymentGatewayId { get; set; }
        public string FacebookId { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Direction { get; set; }
        public string UserPassword { get; set; }
        public string DocumentNumber { get; set; }
        public string BankAccount { get; set; }
        public string Picture { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime? BirthDate { get; set; }
        public string CellPhone { get; set; }
        public string Telephone { get; set; }
        public bool? EmailConfirm { get; set; }
        public string EmailPaymentGateway { get; set; }
        public bool Estatus { get; set; }
        public bool? HaveFacebookEmail { get; set; }
        public byte? Gender { get; set; }
        public byte? CivilStatus { get; set; }

        public TblBanks Bank { get; set; }
        public TblDocumentIdentification DocumentType { get; set; }
        public TblPaymentGateway PaymentGateway { get; set; }
        public TblProvince Province { get; set; }
        public ICollection<TblBusiness> TblBusiness { get; set; }
        public ICollection<TblEmployeeBusiness> TblEmployeeBusiness { get; set; }
        public ICollection<TblPhysicalInventory> TblPhysicalInventory { get; set; }
        public ICollection<TblPurchaseOrder> TblPurchaseOrder { get; set; }
        public ICollection<TblTempFile> TblTempFile { get; set; }
        public ICollection<TblUserConfirmation> TblUserConfirmation { get; set; }
    }
}
