﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblPlanDetails
    {
        public int Id { get; set; }
        public short PlanId { get; set; }
        public string BenefitDetail { get; set; }
        public short Orden { get; set; }
        public bool Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public TblPlans Plan { get; set; }
    }
}
