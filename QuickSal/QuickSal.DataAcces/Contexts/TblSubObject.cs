﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblSubObject
    {
        public TblSubObject()
        {
            TblPlanSubObject = new HashSet<TblPlanSubObject>();
        }

        public int Id { get; set; }
        public int ObjectId { get; set; }
        public string Ico { get; set; }
        public string Controller { get; set; }
        public string Method { get; set; }
        public string Description { get; set; }
        public int? Parent { get; set; }
        public bool Estatus { get; set; }
        public bool UseGeneral { get; set; }
        public bool ShowOnWeb { get; set; }
        public string WebDescription { get; set; }
        public bool AplicationMethod { get; set; }
        public string AplicationDescription { get; set; }
        public string UrlModules { get; set; }
        public byte? TypeMethodId { get; set; }

        public TblObject Object { get; set; }
        public TblTypeMethod TypeMethod { get; set; }
        public ICollection<TblPlanSubObject> TblPlanSubObject { get; set; }
    }
}
