﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblInventoryWarehouse
    {
        public long Id { get; set; }
        public int WarehouseId { get; set; }
        public long ProductsId { get; set; }
        public decimal ProductAmount { get; set; }

        public TblProducts Products { get; set; }
        public TblWarehouse Warehouse { get; set; }
    }
}
