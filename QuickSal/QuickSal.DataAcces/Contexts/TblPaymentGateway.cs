﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblPaymentGateway
    {
        public TblPaymentGateway()
        {
            TblUsers = new HashSet<TblUsers>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public bool Estatus { get; set; }

        public ICollection<TblUsers> TblUsers { get; set; }
    }
}
