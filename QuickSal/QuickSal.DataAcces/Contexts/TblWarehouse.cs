﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblWarehouse
    {
        public TblWarehouse()
        {
            TblInventoryWarehouse = new HashSet<TblInventoryWarehouse>();
            TblInvoiceRow = new HashSet<TblInvoiceRow>();
            TblMovingWarehouse = new HashSet<TblMovingWarehouse>();
            TblPhysicalInventory = new HashSet<TblPhysicalInventory>();
            TblPurchaseOrderRow = new HashSet<TblPurchaseOrderRow>();
        }

        public int Id { get; set; }
        public long BranchOfficeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Estatus { get; set; }
        public bool Principal { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }

        public TblBranchOffice BranchOffice { get; set; }
        public ICollection<TblInventoryWarehouse> TblInventoryWarehouse { get; set; }
        public ICollection<TblInvoiceRow> TblInvoiceRow { get; set; }
        public ICollection<TblMovingWarehouse> TblMovingWarehouse { get; set; }
        public ICollection<TblPhysicalInventory> TblPhysicalInventory { get; set; }
        public ICollection<TblPurchaseOrderRow> TblPurchaseOrderRow { get; set; }
    }
}
