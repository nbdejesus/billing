﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblOrders
    {
        public TblOrders()
        {
            TblOrdersRow = new HashSet<TblOrdersRow>();
        }

        public long Id { get; set; }
        public long BranchOfficeId { get; set; }
        public long InvoiceId { get; set; }
        public long? CustomerId { get; set; }
        public byte DocumentEstatusId { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Observation { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }

        public TblBranchOffice BranchOffice { get; set; }
        public TblCustomers Customer { get; set; }
        public TblInvoice Invoice { get; set; }
        public ICollection<TblOrdersRow> TblOrdersRow { get; set; }
    }
}
