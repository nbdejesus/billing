﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblNcf
    {
        public TblNcf()
        {
            TblNcfrow = new HashSet<TblNcfrow>();
        }

        public int Id { get; set; }
        public long BusinessId { get; set; }
        public short PrefixId { get; set; }
        public long CounterFrom { get; set; }
        public long CounterTo { get; set; }
        public DateTime CreateDate { get; set; }
        public bool Estatus { get; set; }
        public bool IsExhausted { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }

        public TblBusiness Business { get; set; }
        public TblNcfprefix Prefix { get; set; }
        public ICollection<TblNcfrow> TblNcfrow { get; set; }
    }
}
