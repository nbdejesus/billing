﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblPrivileges
    {
        public TblPrivileges()
        {
            TblEmployeeBusiness = new HashSet<TblEmployeeBusiness>();
            TblSegurity = new HashSet<TblSegurity>();
        }

        public int Id { get; set; }
        public long UserId { get; set; }
        public long BusinessId { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public bool Principal { get; set; }
        public long? UserCreateId { get; set; }
        public long? EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }
        public bool? Estatus { get; set; }

        public TblBusiness Business { get; set; }
        public ICollection<TblEmployeeBusiness> TblEmployeeBusiness { get; set; }
        public ICollection<TblSegurity> TblSegurity { get; set; }
    }
}
