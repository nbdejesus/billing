﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblUserConfirmation
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string ConfirmationKey { get; set; }
        public DateTime ExpirationKey { get; set; }
        public byte? IdForms { get; set; }

        public TblForms IdFormsNavigation { get; set; }
        public TblUsers User { get; set; }
    }
}
