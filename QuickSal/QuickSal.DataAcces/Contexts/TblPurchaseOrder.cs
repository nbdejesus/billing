﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblPurchaseOrder
    {
        public TblPurchaseOrder()
        {
            TblDebitNote = new HashSet<TblDebitNote>();
            TblPurchaseOrderRow = new HashSet<TblPurchaseOrderRow>();
            TblVendorPaymentRow = new HashSet<TblVendorPaymentRow>();
        }

        public long Id { get; set; }
        public long BranchOfficeId { get; set; }
        public long SuppliersId { get; set; }
        public long? UserId { get; set; }
        public long? EmployeeBusinessId { get; set; }
        public byte DocumentEstatusId { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime DocumentDate { get; set; }
        public string Observation { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Discount { get; set; }
        public decimal TotalTax { get; set; }
        public decimal Total { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string Reference { get; set; }
        public string Ncf { get; set; }
        public decimal Balance { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }

        public TblBranchOffice BranchOffice { get; set; }
        public TblEmployeeBusiness EmployeeBusiness { get; set; }
        public TblSuppliers Suppliers { get; set; }
        public TblUsers User { get; set; }
        public ICollection<TblDebitNote> TblDebitNote { get; set; }
        public ICollection<TblPurchaseOrderRow> TblPurchaseOrderRow { get; set; }
        public ICollection<TblVendorPaymentRow> TblVendorPaymentRow { get; set; }
    }
}
