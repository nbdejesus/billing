﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblDebitNote
    {
        public TblDebitNote()
        {
            TblDebitNoteRow = new HashSet<TblDebitNoteRow>();
        }

        public long Id { get; set; }
        public long BranchOfficeId { get; set; }
        public long SuppliersId { get; set; }
        public long PurchaseOrderId { get; set; }
        public byte DocumentEstatusId { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Observation { get; set; }
        public decimal SubTotal { get; set; }
        public decimal TotalTax { get; set; }
        public decimal Total { get; set; }
        public string Ncf { get; set; }
        public string Ncfmodified { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }
        public bool? Used { get; set; }

        public TblBranchOffice BranchOffice { get; set; }
        public TblEmployeeBusiness EmployeeCreate { get; set; }
        public TblEmployeeBusiness EmployeeModify { get; set; }
        public TblPurchaseOrder PurchaseOrder { get; set; }
        public TblSuppliers Suppliers { get; set; }
        public ICollection<TblDebitNoteRow> TblDebitNoteRow { get; set; }
    }
}
