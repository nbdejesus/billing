﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblBanks
    {
        public TblBanks()
        {
            TblUsers = new HashSet<TblUsers>();
        }

        public short Id { get; set; }
        public string Name { get; set; }
        public bool Estatus { get; set; }

        public ICollection<TblUsers> TblUsers { get; set; }
    }
}
