﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblObject
    {
        public TblObject()
        {
            TblSubObject = new HashSet<TblSubObject>();
        }

        public int Id { get; set; }
        public string Ico { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Estatus { get; set; }

        public ICollection<TblSubObject> TblSubObject { get; set; }
    }
}
