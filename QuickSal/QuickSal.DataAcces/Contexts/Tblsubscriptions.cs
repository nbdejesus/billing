﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class Tblsubscriptions
    {
        public long Id { get; set; }
        public long BusinessId { get; set; }
        public short IdPlan { get; set; }
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public bool Estatus { get; set; }

        public TblBusiness Business { get; set; }
        public TblPlans IdPlanNavigation { get; set; }
    }
}
