﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblCretitNoteUsed
    {
        public int Id { get; set; }
        public long InvoiceId { get; set; }
        public long CreditNoteId { get; set; }

        public TblCreditNote CreditNote { get; set; }
        public TblInvoice Invoice { get; set; }
    }
}
