﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblLogError
    {
        public long Id { get; set; }
        public DateTime CreateDate { get; set; }
        public string ErrorMessage { get; set; }
        public string Method { get; set; }
        public string Location { get; set; }
        public string SystemProccess { get; set; }
    }
}
