﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblSuppliers
    {
        public TblSuppliers()
        {
            TblDebitNote = new HashSet<TblDebitNote>();
            TblProducts = new HashSet<TblProducts>();
            TblPurchaseOrder = new HashSet<TblPurchaseOrder>();
            TblVendorPaymentRow = new HashSet<TblVendorPaymentRow>();
        }

        public long Id { get; set; }
        public long BusinessId { get; set; }
        public byte DocumentTypeId { get; set; }
        public string DocumentNumber { get; set; }
        public string Code { get; set; }
        public string Names { get; set; }
        public string PhoneNumber { get; set; }
        public string Movil { get; set; }
        public string AddressSuppliers { get; set; }
        public string ContactPerson { get; set; }
        public string Picture { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Observation { get; set; }
        public int? ProvinceId { get; set; }
        public int? CountryId { get; set; }
        public bool Estatus { get; set; }
        public string Email { get; set; }
        public string Obervation { get; set; }
        public short? Ncfid { get; set; }
        public decimal? PendingBalance { get; set; }
        public short? CreditTypeId { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }

        public TblBusiness Business { get; set; }
        public TblDocumentIdentification DocumentType { get; set; }
        public TblNcfprefix Ncf { get; set; }
        public TblProvince Province { get; set; }
        public ICollection<TblDebitNote> TblDebitNote { get; set; }
        public ICollection<TblProducts> TblProducts { get; set; }
        public ICollection<TblPurchaseOrder> TblPurchaseOrder { get; set; }
        public ICollection<TblVendorPaymentRow> TblVendorPaymentRow { get; set; }
    }
}
