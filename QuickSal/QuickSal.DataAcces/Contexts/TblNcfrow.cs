﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblNcfrow
    {
        public long Id { get; set; }
        public int Ncfid { get; set; }
        public string Ncf { get; set; }
        public bool Used { get; set; }

        public TblNcf NcfNavigation { get; set; }
    }
}
