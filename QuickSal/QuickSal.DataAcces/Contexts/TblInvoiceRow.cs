﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblInvoiceRow
    {
        public long Id { get; set; }
        public long InvoiceId { get; set; }
        public int WarehouseId { get; set; }
        public long ProductId { get; set; }
        public int RowNumber { get; set; }
        public string Measure { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Tax { get; set; }
        public string TaxAbbreviation { get; set; }
        public decimal AmountTax { get; set; }
        public decimal Discount { get; set; }
        public decimal AmountDiscount { get; set; }
        public decimal Total { get; set; }

        public TblInvoice Invoice { get; set; }
        public TblProducts Product { get; set; }
        public TblWarehouse Warehouse { get; set; }
    }
}
