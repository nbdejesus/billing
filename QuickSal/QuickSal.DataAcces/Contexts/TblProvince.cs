﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblProvince
    {
        public TblProvince()
        {
            TblBranchOffice = new HashSet<TblBranchOffice>();
            TblCustomers = new HashSet<TblCustomers>();
            TblEmployeeBusiness = new HashSet<TblEmployeeBusiness>();
            TblSuppliers = new HashSet<TblSuppliers>();
            TblUsers = new HashSet<TblUsers>();
        }

        public int Id { get; set; }
        public short CountryId { get; set; }
        public string Description { get; set; }

        public TblCountry Country { get; set; }
        public ICollection<TblBranchOffice> TblBranchOffice { get; set; }
        public ICollection<TblCustomers> TblCustomers { get; set; }
        public ICollection<TblEmployeeBusiness> TblEmployeeBusiness { get; set; }
        public ICollection<TblSuppliers> TblSuppliers { get; set; }
        public ICollection<TblUsers> TblUsers { get; set; }
    }
}
