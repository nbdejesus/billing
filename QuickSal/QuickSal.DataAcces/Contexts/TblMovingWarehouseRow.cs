﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblMovingWarehouseRow
    {
        public long Id { get; set; }
        public long MovingId { get; set; }
        public decimal Amount { get; set; }
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public int State { get; set; }
        public string Description { get; set; }

        public TblMovingWarehouse Moving { get; set; }
        public TblProducts Product { get; set; }
    }
}
