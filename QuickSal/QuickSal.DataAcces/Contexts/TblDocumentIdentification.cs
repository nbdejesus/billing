﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblDocumentIdentification
    {
        public TblDocumentIdentification()
        {
            TblBusiness = new HashSet<TblBusiness>();
            TblCustomers = new HashSet<TblCustomers>();
            TblEmployeeBusiness = new HashSet<TblEmployeeBusiness>();
            TblSuppliers = new HashSet<TblSuppliers>();
            TblUsers = new HashSet<TblUsers>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public byte MaxlengthChar { get; set; }
        public bool Estatus { get; set; }
        public string Mask { get; set; }

        public ICollection<TblBusiness> TblBusiness { get; set; }
        public ICollection<TblCustomers> TblCustomers { get; set; }
        public ICollection<TblEmployeeBusiness> TblEmployeeBusiness { get; set; }
        public ICollection<TblSuppliers> TblSuppliers { get; set; }
        public ICollection<TblUsers> TblUsers { get; set; }
    }
}
