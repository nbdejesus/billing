﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblTempFile
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string FileName { get; set; }
        public byte TypeFile { get; set; }

        public TblUsers User { get; set; }
    }
}
