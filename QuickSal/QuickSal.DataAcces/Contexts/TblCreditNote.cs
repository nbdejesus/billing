﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblCreditNote
    {
        public TblCreditNote()
        {
            TblCreditNoteRow = new HashSet<TblCreditNoteRow>();
            TblCretitNoteUsed = new HashSet<TblCretitNoteUsed>();
        }

        public long Id { get; set; }
        public long BranchOfficeId { get; set; }
        public long CustomerId { get; set; }
        public long InvoiceId { get; set; }
        public byte DocumentEstatusId { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime DocumentDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Observation { get; set; }
        public decimal SubTotal { get; set; }
        public decimal TotalDiscount { get; set; }
        public decimal TotalTax { get; set; }
        public decimal Total { get; set; }
        public string Ncf { get; set; }
        public string Ncfmodified { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }
        public bool Used { get; set; }

        public TblBranchOffice BranchOffice { get; set; }
        public TblCustomers Customer { get; set; }
        public TblEmployeeBusiness EmployeeCreate { get; set; }
        public TblInvoice Invoice { get; set; }
        public ICollection<TblCreditNoteRow> TblCreditNoteRow { get; set; }
        public ICollection<TblCretitNoteUsed> TblCretitNoteUsed { get; set; }
    }
}
