﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblInvoice
    {
        public TblInvoice()
        {
            TblCreditNote = new HashSet<TblCreditNote>();
            TblCretitNoteUsed = new HashSet<TblCretitNoteUsed>();
            TblCustomerPaymentsRow = new HashSet<TblCustomerPaymentsRow>();
            TblInvoiceRow = new HashSet<TblInvoiceRow>();
            TblOrders = new HashSet<TblOrders>();
        }

        public long Id { get; set; }
        public long BranchOfficeId { get; set; }
        public long? CustomerId { get; set; }
        public short? CreditTypeId { get; set; }
        public short? NcfId { get; set; }
        public byte DocumentEstatusId { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime DocumentDate { get; set; }
        public string Observation { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Discount { get; set; }
        public decimal TotalTax { get; set; }
        public decimal Total { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string Reference { get; set; }
        public string Ncf { get; set; }
        public decimal Balance { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }

        public TblBranchOffice BranchOffice { get; set; }
        public TblCustomers Customer { get; set; }
        public ICollection<TblCreditNote> TblCreditNote { get; set; }
        public ICollection<TblCretitNoteUsed> TblCretitNoteUsed { get; set; }
        public ICollection<TblCustomerPaymentsRow> TblCustomerPaymentsRow { get; set; }
        public ICollection<TblInvoiceRow> TblInvoiceRow { get; set; }
        public ICollection<TblOrders> TblOrders { get; set; }
    }
}
