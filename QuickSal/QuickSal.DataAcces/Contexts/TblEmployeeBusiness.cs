﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblEmployeeBusiness
    {
        public TblEmployeeBusiness()
        {
            TblCreditNote = new HashSet<TblCreditNote>();
            TblCustomerPaymentsEmployeeCreate = new HashSet<TblCustomerPayments>();
            TblCustomerPaymentsEmployeeModify = new HashSet<TblCustomerPayments>();
            TblDebitNoteEmployeeCreate = new HashSet<TblDebitNote>();
            TblDebitNoteEmployeeModify = new HashSet<TblDebitNote>();
            TblMovingWarehouseEmployeeCreate = new HashSet<TblMovingWarehouse>();
            TblMovingWarehouseEmployeeModify = new HashSet<TblMovingWarehouse>();
            TblPhysicalInventory = new HashSet<TblPhysicalInventory>();
            TblPurchaseOrder = new HashSet<TblPurchaseOrder>();
            TblVendorPaymentEmployeeCreate = new HashSet<TblVendorPayment>();
            TblVendorPaymentEmployeeModify = new HashSet<TblVendorPayment>();
        }

        public long Id { get; set; }
        public string EmployeeCode { get; set; }
        public int? CountryId { get; set; }
        public int? ProvinceId { get; set; }
        public byte? DocumentTypeId { get; set; }
        public string DocumentNumber { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public long UserId { get; set; }
        public int PrivilegesId { get; set; }
        public long? BranchOfficeId { get; set; }
        public long BusinessId { get; set; }
        public bool Estatus { get; set; }
        public DateTime IntoDate { get; set; }
        public DateTime? OutDate { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Direction { get; set; }
        public string Phone { get; set; }
        public string CellPhone { get; set; }
        public string InstitutionalEmail { get; set; }
        public byte? Gender { get; set; }
        public byte? CivilStatus { get; set; }
        public bool BusinessOwner { get; set; }
        public decimal? Salary { get; set; }
        public string Position { get; set; }
        public string Picture { get; set; }
        public long? UserCreateId { get; set; }
        public long? EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }

        public TblBranchOffice BranchOffice { get; set; }
        public TblBusiness Business { get; set; }
        public TblDocumentIdentification DocumentType { get; set; }
        public TblPrivileges Privileges { get; set; }
        public TblProvince Province { get; set; }
        public TblUsers User { get; set; }
        public ICollection<TblCreditNote> TblCreditNote { get; set; }
        public ICollection<TblCustomerPayments> TblCustomerPaymentsEmployeeCreate { get; set; }
        public ICollection<TblCustomerPayments> TblCustomerPaymentsEmployeeModify { get; set; }
        public ICollection<TblDebitNote> TblDebitNoteEmployeeCreate { get; set; }
        public ICollection<TblDebitNote> TblDebitNoteEmployeeModify { get; set; }
        public ICollection<TblMovingWarehouse> TblMovingWarehouseEmployeeCreate { get; set; }
        public ICollection<TblMovingWarehouse> TblMovingWarehouseEmployeeModify { get; set; }
        public ICollection<TblPhysicalInventory> TblPhysicalInventory { get; set; }
        public ICollection<TblPurchaseOrder> TblPurchaseOrder { get; set; }
        public ICollection<TblVendorPayment> TblVendorPaymentEmployeeCreate { get; set; }
        public ICollection<TblVendorPayment> TblVendorPaymentEmployeeModify { get; set; }
    }
}
