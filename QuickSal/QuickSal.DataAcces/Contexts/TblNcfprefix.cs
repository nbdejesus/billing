﻿using System;
using System.Collections.Generic;

namespace QuickSal.DataAcces.Contexts
{
    public partial class TblNcfprefix
    {
        public TblNcfprefix()
        {
            TblCustomers = new HashSet<TblCustomers>();
            TblNcf = new HashSet<TblNcf>();
            TblSuppliers = new HashSet<TblSuppliers>();
        }

        public short Id { get; set; }
        public string Prefix { get; set; }
        public string Description { get; set; }
        public bool Estatus { get; set; }

        public ICollection<TblCustomers> TblCustomers { get; set; }
        public ICollection<TblNcf> TblNcf { get; set; }
        public ICollection<TblSuppliers> TblSuppliers { get; set; }
    }
}
