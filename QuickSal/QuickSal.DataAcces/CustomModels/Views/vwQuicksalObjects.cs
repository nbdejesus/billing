﻿namespace QuickSal.DataAcces.Views
{
    public class vwQuicksalObjects
    {
        public long Id { get; set; }
        public int SubObjectId { get; set; }
        public string SubObjectDescription { get; set; }
        public string ObjectDescription { get; set; }
        public string Ico { get; set; }
        public string Controller { get; set; }
        public string Method { get; set; }
        public string Name { get; set; }
        public bool UseGeneral { get; set; }
        public string UrlModule { get; set; }
        public short PlanId { get; set; }
        public bool ShowOnWeb { get; set; }
        public string TextDescription { get; set; }
    }
}
