﻿namespace QuickSal.DataAcces.Views
{
    public class vwObjectChildren
    {
        public long Id { get; set; }
        public int SubObjectChildren { get; set; }
        public int SubObjectParent { get; set; }
        public string ChildrenController { get; set; }
        public string ChildrenMethod { get; set; }
        public string ParentController { get; set; }
        public string ParentMethod { get; set; }
        public byte? TypeMethodId { get; set; }
    }
}
