﻿namespace QuickSal.DataAcces.Views
{
    public class vwEmployeeSecurity
    {
        public int Id { get; set; }
        public string Controller { get; set; }
        public string Method { get; set; }
        public bool Status { get; set; }
        public int? PrivilegesId { get; set; }
 
        public string UrlModules { get; set; }
        public bool AplicationMetod { get; set; }
    }
}
