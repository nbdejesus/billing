﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickSal.DataAcces.Views
{
    public class vwSegurityAccess
    {
        public long Id { get; set; }
        public bool? Leer { get; set; }
        public bool? Escribir { get; set; }
        public bool? Editar { get; set; }
        public bool? Eliminar { get; set; }
        public int SubObjectId{ get; set; }
        public int? Parent { get; set; }
        public int PrivilegesId { get; set; }
    }
}
