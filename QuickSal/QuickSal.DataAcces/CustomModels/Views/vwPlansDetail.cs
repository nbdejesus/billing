﻿using System;
using System.Linq;

namespace QuickSal.DataAcces.Views
{
    public class vwPlansDetail
    {
        public long Id { get; set; }
        public Int16 PlanId { get; set; }
        public string Ico { get; set; }
        public string WebDescription { get; set; }
        public string ObjectDescription { get; set; }
        public Int16 BusinessTypeId { get; set; }
        public string PlanName { get; set; }
        public decimal Price { get; set; }
        public byte CurrencyID { get; set; }

    }
}
