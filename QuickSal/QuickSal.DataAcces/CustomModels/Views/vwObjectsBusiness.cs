﻿namespace QuickSal.DataAcces.Views
{
    public class vwObjectsBusiness
    {
        public long Id { get; set; }
        public int SubObjectId { get; set; }
        public string SubObjectDescription { get; set; }
        public string ObjectDescription { get; set; }
        public string Ico { get; set; }
        public string Controller { get; set; }
        public string Method { get; set; }
        public string UrlModule { get; set; }
        public short? PlanId { get; set; }
        public string AplicationDescription { get; set; }
        public long BusinessId { get; set; }
        public int? PrivilegesId { get; set; }
        public bool? Principal { get; set; }
        public long? SegurityId { get; set; }
    }
}
