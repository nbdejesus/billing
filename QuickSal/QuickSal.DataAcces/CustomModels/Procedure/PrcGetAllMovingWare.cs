﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickSal.DataAcces.Procedure
{
    public class PrcGetAllMovingWare
    {
        public long Id { get; set; }
        public string DocumentNo { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string EmployeeCreateId { get; set; }
        public string EmployeeModifyId { get; set; }
        public string SenderWarehouseId { get; set; }
        public string ReceiverWarehouseId { get; set; }
        public string Description { get; set; }
        public int State { get; set; }
       
    }
}
