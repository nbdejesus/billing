﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickSal.DataAcces.Procedure
{
    public class prc_PurchaseOrderRow
    {

        public long PurchaseOrderRowId { get; set; }   
 
        public decimal Disponible { get; set; }  
        public decimal Discount { get; set; }  
        public decimal Total { get; set; }
        public string Produc_Description { get; set; }
        public string WareHouse { get; set; }
        public int WarehouseId { get; set; }    
        public string TaxAbbreviation { get; set; } 

        public long ProductId { get; set; }
        public int MeasureId { get; set; }
        public string Measure { get; set; }
        public decimal Quantity { get; set; }
        public decimal Cost { get; set; }
        public decimal Tax { get; set; }
        public decimal AmountTax { get; set; }
        public long PurchaseOrderId { get; set; }
        public decimal AmountDiscount { get; set; }
   
    }
}
