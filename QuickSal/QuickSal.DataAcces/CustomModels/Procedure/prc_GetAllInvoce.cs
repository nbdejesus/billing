﻿using System;

namespace QuickSal.DataAcces.Procedure
{
    public class PrcGetAllInvoce
    {
        public long Id { get; set; }
        public string DocumentNumber { get; set; }
        public string CustomerName { get; set; }
        public DateTime DocumentDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public byte DocumentStatus { get; set; }
        public string NCF { get; set; }
        public decimal Balance { get; set; }
        public decimal Total { get; set; }
        public string EmployeeCreate { get; set; }
    }
}
