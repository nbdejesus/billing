﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickSal.DataAcces.CustomModels.Procedure.Customer
{
    public class GetOrders
    {
        public long Id { get; set; }
        public string OrderNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public string CustomerName { get; set; }
        public DateTime CreateDate { get; set; }
        public byte DocumentEstatusId { get; set; }
        public string ProductName { get; set; }
        public decimal Quantity { get; set; }
        public string ProductPicture { get; set; }
    }
}
