﻿namespace QuickSal.DataAcces.Procedure.Customer
{
    public class GetProductSalesPoint
    {
        public long Id { get; set; }
        public string ProductName { get; set; }
        public string Code { get; set; }
        public decimal Price { get; set; }
        public string Picture { get; set; }
        public decimal TotalProduct { get; set; }
        public string Abbreviation { get; set; }
        public int? MeasureId { get; set; }
        public bool Selllowcost { get; set; }
        public bool InvoiceWithoutExistence { get; set; }
        public string TaxAbbreviation { get; set; }
        public decimal? Tax { get; set; }
        public bool IsService { get; set; }
    }
}
