﻿namespace QuickSal.DataAcces.Procedure.Customer
{
    public class GetInvoiceDetail
    {
    
        public string ProductName { get; set; }
        public string Code { get; set; }
        public decimal Price { get; set; }
        public string Picture { get; set; }
        public decimal TotalProduct { get; set; }
        public bool Selllowcost { get; set; }
        public bool InvoiceWithoutExistence { get; set; }
        public string TaxAbbreviation { get; set; }
        public decimal? Tax { get; set; }


        public long Id { get; set; }
        public long ProductId { get; set; }
        public decimal ItemQuantity { get; set; }
        public decimal ItemDiscount { get; set; }
        public string MeaseureAbbreviation { get; set; }
        public decimal SubTotal { get; set; }
        public decimal AmountDiscount { get; set; }
        public decimal AmountTax { get; set; }
        public int WarehouseId { get; set; }
        public string WareHouseName { get; set; }

        public int? MeasureId { get; set; }
    }
}
