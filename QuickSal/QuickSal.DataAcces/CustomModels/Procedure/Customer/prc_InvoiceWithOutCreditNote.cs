﻿using System;

namespace QuickSal.DataAcces.CustomModels.Procedure.Customer
{
    public class prc_InvoiceWithOutCreditNote
    {
        public long Id { get; set; }
        public string CustomerName { get; set; }

        public string DocumentNumber { get; set; }

        public string NCF { get; set; }

        public int? NcfId { get; set; }

        public DateTime CreateDate { get; set; }

        public decimal Total { get; set; }

        public byte DocumentStatusId { get; set; }
    }
}
