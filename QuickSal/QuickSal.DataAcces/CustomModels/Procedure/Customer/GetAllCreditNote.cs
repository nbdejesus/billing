﻿using System;

namespace QuickSal.DataAcces.Procedure.Customer
{
    public class GetAllCreditNote
    {
        public long Id { get; set; }
        public string DocumentNumber { get; set; }
        public string CustomerName { get; set; }
        public string InvoiceNumber { get; set; }
        public string NCF { get; set; }
        public string NCFModified { get; set; }
        public DateTime CreateDate { get; set; }
        public decimal Total { get; set; }
        public byte DocumentEstatusId { get; set; }
    }
}
