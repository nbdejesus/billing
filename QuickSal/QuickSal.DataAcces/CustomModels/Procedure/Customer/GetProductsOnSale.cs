﻿namespace QuickSal.DataAcces.CustomModels.Procedure.Customer
{
    public class GetProductsOnSale
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Picture { get; set; }
        public decimal ProductAmount { get; set; }
        public decimal Price { get; set; }
        public bool InvoiceWithoutExistence { get; set; }
        public string TaxAbbreviation { get; set; }
        public decimal? Tax { get; set; }
        public bool IsService { get; set; }
        public string abbreviation { get; set; }
    }
}
