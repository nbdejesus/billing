﻿namespace QuickSal.DataAcces.Procedure
{
    public class PrcInvoceRowDetail
    {
        public long Id { get; set; }
        public int WarehouseId { get; set; }
        public string WareHouseName { get; set; }
        public long ProductId { get; set; }

        public string PorductName { get; set; }

        public string Measure { get; set; }

        public decimal Quantity { get; set; }

        public decimal Price { get; set; }

        public decimal Tax { get; set; }

        public string TaxAbbreviation { get; set; }

        public decimal AmountTax { get; set; }

        public decimal Discount { get; set; }

        public decimal AmountDiscount { get; set; }

        public decimal Total { get; set; }
    }
}
