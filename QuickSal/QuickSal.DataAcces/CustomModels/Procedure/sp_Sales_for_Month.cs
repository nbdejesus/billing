﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickSal.DataAcces.Procedure
{
    public class sp_Sales_for_Month
    {
        public int año { get; set; }
        public string mes { get; set; }
        public decimal Ventas { get; set; }
    }
}
