﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickSal.DataAcces.Procedure
{
    public class prc_Profits
    {
        public int month { get; set; }

        public int year { get; set; }

        public decimal Profit { get; set; }
    }
}
