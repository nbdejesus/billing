﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickSal.DataAcces.Procedure
{
    public class sp_Sales_for_Month_products
    {
        public decimal Sales { get; set; }

        public string months { get; set; }
        public int years { get; set; }

        public string Name { get; set; }
    }
}
