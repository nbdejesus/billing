﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickSal.DataAcces.Procedure
{
    public class prc_Sales_for_Month_branchOffice
    {
        public decimal Sales { get; set; }

        public string months { get; set; }
        public int years { get; set; }

        public string Name { get; set; }
    }
}
