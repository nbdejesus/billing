﻿namespace QuickSal.DataAcces.Models
{
    public class PrcGetAllProducts
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool IsService { get; set; }
        public decimal Cost { get; set; }
        public string Measure { get; set; }
        public string SubCategory { get; set; }
        public string Supplier { get; set; }
        public decimal? QuantityTotal { get; set; }
        public bool Status { get; set; }
    }
}
