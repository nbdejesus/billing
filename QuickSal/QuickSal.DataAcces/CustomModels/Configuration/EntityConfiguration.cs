﻿using Microsoft.EntityFrameworkCore;
using QuickSal.DataAcces.CustomModels.Models;
using QuickSal.DataAcces.CustomModels.Procedure.Customer;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Procedure;
using QuickSal.DataAcces.Procedure.Customer;
using QuickSal.DataAcces.Views;

namespace QuickSal.DataAcces.Configuration
{
    public static class EntityModelConfiguration
    {
        public static void ContinueModelCreating(ModelBuilder modelBuilder)
        {
            #region vistas
            modelBuilder.Entity<vwQuicksalObjects>(entity =>
            {
                entity.ToTable("vwQuicksalObjects");
                entity.HasKey(x => x.Id);

                entity.Property(x => x.Id).HasColumnName("Id");
                entity.Property(x => x.SubObjectId).HasColumnName("SubObjectId");
                entity.Property(x => x.SubObjectDescription).HasColumnName("SubObjectDescription");
                entity.Property(x => x.Ico).HasColumnName("Ico");
                entity.Property(x => x.ObjectDescription).HasColumnName("ObjectDescription");
                entity.Property(x => x.Controller).HasColumnName("Controller");
                entity.Property(x => x.Method).HasColumnName("Method");
                entity.Property(x => x.Name).HasColumnName("Name");
                entity.Property(x => x.UseGeneral).HasColumnName("UseGeneral");
                entity.Property(x => x.UrlModule).HasColumnName("UrlModules");
                entity.Property(x => x.PlanId).HasColumnName("PlanId");
            });

            modelBuilder.Entity<vwPlansDetail>(entity =>
            {
                entity.ToTable("vwPlansDetail");
                entity.HasKey(x => x.Id);

                entity.Property(x => x.Id).HasColumnName("Id");
                entity.Property(x => x.PlanId).HasColumnName("PlanId");
                entity.Property(x => x.Ico).HasColumnName("Ico");
                entity.Property(x => x.WebDescription).HasColumnName("WebDescription");
                entity.Property(x => x.ObjectDescription).HasColumnName("ObjectDescription");
                entity.Property(x => x.BusinessTypeId).HasColumnName("BusinessTypeId");
                entity.Property(x => x.PlanName).HasColumnName("Name");
                entity.Property(x => x.Price).HasColumnName("Price");
                entity.Property(x => x.CurrencyID).HasColumnName("Currency");
            });

            modelBuilder.Entity<vwObjectsBusiness>(entity =>
            {
                entity.ToTable("vwObjectsBusiness");
                entity.HasKey(x => x.Id);

                entity.Property(x => x.Id).HasColumnName("Id");
                entity.Property(x => x.SubObjectId).HasColumnName("SubObjectId");
                entity.Property(x => x.SubObjectDescription).HasColumnName("SubObjectDescription");
                entity.Property(x => x.Ico).HasColumnName("Ico");
                entity.Property(x => x.ObjectDescription).HasColumnName("ObjectDescription");
                entity.Property(x => x.Controller).HasColumnName("Controller");
                entity.Property(x => x.Method).HasColumnName("Method");
                entity.Property(x => x.UrlModule).HasColumnName("UrlModules");
                entity.Property(x => x.AplicationDescription).HasColumnName("AplicationDescription");
                entity.Property(x => x.PlanId).HasColumnName("PlanId");
                entity.Property(x => x.BusinessId).HasColumnName("BusinessId");
                entity.Property(x => x.PrivilegesId).HasColumnName("PrivilegesId");
                entity.Property(x => x.Principal).HasColumnName("principal");
                entity.Property(x => x.SegurityId).HasColumnName("SegurityId");
            });



            modelBuilder.Entity<vwEmployeeSecurity>(entity =>
            {
                entity.ToTable("vwEmployeeSecurity");
                entity.HasKey(x => x.Id);

                entity.Property(x => x.Id).HasColumnName("Id");
                entity.Property(x => x.Controller).HasColumnName("Controller");
                entity.Property(x => x.Method).HasColumnName("Method");
                entity.Property(x => x.Status).HasColumnName("Estatus");
                entity.Property(x => x.PrivilegesId).HasColumnName("PrivilegesId");
                entity.Property(x => x.UrlModules).HasColumnName("UrlModules");
                entity.Property(x => x.AplicationMetod).HasColumnName("AplicationMethod");

            });

            modelBuilder.Entity<vwObjectChildren>(entity =>
            {
                entity.ToTable("vwObjectChildren");
                entity.HasKey(x => x.Id);

                entity.Property(x => x.Id).HasColumnName("Id");
                entity.Property(x => x.SubObjectChildren).HasColumnName("SubObjectChildren");
                entity.Property(x => x.SubObjectParent).HasColumnName("SubObjectParent");
                entity.Property(x => x.ChildrenController).HasColumnName("ChildrenController");
                entity.Property(x => x.ChildrenMethod).HasColumnName("ChildrenMethod");
                entity.Property(x => x.ParentController).HasColumnName("ParentController");
                entity.Property(x => x.ParentMethod).HasColumnName("ParentMethod");
                entity.Property(x => x.TypeMethodId).HasColumnName("TypeMethodId");
            });


            modelBuilder.Entity<vwSegurityAccess>(entity =>
            {
                entity.ToTable("vwSegurityAccess");
                entity.HasKey(x => x.Id);

                entity.Property(x => x.Id).HasColumnName("Id");
                entity.Property(x => x.Leer).HasColumnName("Leer");
                entity.Property(x => x.Escribir).HasColumnName("Escribir");
                entity.Property(x => x.Editar).HasColumnName("Editar");
                entity.Property(x => x.Eliminar).HasColumnName("Eliminar");
                entity.Property(x => x.SubObjectId).HasColumnName("SubObjectId");
                entity.Property(x => x.Parent).HasColumnName("Parent");
                entity.Property(x => x.PrivilegesId).HasColumnName("PrivilegesId");
            });


            #endregion

            #region Procedure
            modelBuilder.Entity<PrcGetAllProducts>(entity =>
            {
                entity.ToTable("prc_GetAllProducts");
                entity.HasKey(x => x.Id);

                entity.Property(x => x.Id).HasColumnName("Id");
                entity.Property(x => x.Code).HasColumnName("Code");
                entity.Property(x => x.Name).HasColumnName("Name");
                entity.Property(x => x.IsService).HasColumnName("IsService");
                entity.Property(x => x.Cost).HasColumnName("Cost");
                entity.Property(x => x.Measure).HasColumnName("Measure");
                entity.Property(x => x.SubCategory).HasColumnName("SubCategory");
                entity.Property(x => x.Supplier).HasColumnName("Supplier");
                entity.Property(x => x.QuantityTotal).HasColumnName("QuantityTotal");
                entity.Property(x => x.Status).HasColumnName("Estatus");
            });

            modelBuilder.Entity<PrcGetAllMovingWare>(entity =>
            {
                entity.ToTable("WarehouseMoving");
                entity.HasKey(x => x.Id);

                entity.Property(x => x.Id).HasColumnName("Id");
                entity.Property(x => x.DocumentNo).HasColumnName("DocumentNo");
                entity.Property(x => x.CreateDate).HasColumnName("CreateDate");
                entity.Property(x => x.Description).HasColumnName("Description");
                entity.Property(x => x.ModifyDate).HasColumnName("ModifyDate");
                entity.Property(x => x.EmployeeCreateId).HasColumnName("EmployeeCreateId");
                entity.Property(x => x.EmployeeModifyId).HasColumnName("EmployeeModifyId");
                entity.Property(x => x.ReceiverWarehouseId).HasColumnName("Receiver");
                entity.Property(x => x.SenderWarehouseId).HasColumnName("Sender");
                entity.Property(x => x.State).HasColumnName("State");
            });

            modelBuilder.Entity<PrGetMovingWareDetails>(entity =>
            {
                entity.ToTable("SP_WareMovingDatials");
                entity.HasKey(x => x.Id);

                entity.Property(x => x.Id).HasColumnName("Id");
                entity.Property(x => x.DocumentNo).HasColumnName("DocumentNo");
                entity.Property(x => x.CreateDate).HasColumnName("CreateDate");
                entity.Property(x => x.DescriptionHouse).HasColumnName("Description");
                entity.Property(x => x.ModifyDate).HasColumnName("ModifyDate");
                entity.Property(x => x.EmployeeCreateId).HasColumnName("EmployeeCreateId");
                entity.Property(x => x.EmployeeModifyId).HasColumnName("EmployeeModifyId");
                entity.Property(x => x.ReceiverWarehouseId).HasColumnName("Receiver");
                entity.Property(x => x.SenderWarehouseId).HasColumnName("Sender");
                entity.Property(x => x.StateHouse).HasColumnName("State");
                entity.Property(x => x.Product).HasColumnName("Product");
                entity.Property(x => x.Amount).HasColumnName("Amount");
                entity.Property(x => x.Description).HasColumnName("DescriptionDetails");
                entity.Property(x => x.State).HasColumnName("StateDedatils");
            });

            modelBuilder.Entity<prc_Shopping>(entity =>
            {
                entity.ToTable("PS_Compras");
                entity.HasKey(x => x.businessId);

                entity.Property(x => x.businessId).HasColumnName("Numero_Mes");
                entity.Property(x => x.compras).HasColumnName("Compras");
            });

            modelBuilder.Entity<prc_Sales>(entity =>
            {
                entity.ToTable("SP_Sales");
                entity.HasKey(x => x.Numero_Mes);

                entity.Property(x => x.Numero_Mes).HasColumnName("Numero_Mes");
                entity.Property(x => x.Sales).HasColumnName("Ventas");
            });

            modelBuilder.Entity<prc_Profits>(entity =>
            {
                entity.ToTable("sp_profit", "Dashboard");
                entity.HasKey(x => x.month);

                entity.Property(x => x.month).HasColumnName("month");
                entity.Property(x => x.year).HasColumnName("year");
                entity.Property(x => x.Profit).HasColumnName("Profit");
            });

            modelBuilder.Entity<prc_expenses>(entity =>
            {
                entity.ToTable("sp_expenses", "Dashboard");
                entity.HasKey(x => x.Numero_Mes);

                entity.Property(x => x.Numero_Mes).HasColumnName("Numero_Mes");
                entity.Property(x => x.Gasto).HasColumnName("Gasto");
            });

            modelBuilder.Entity<sp_Sales_for_Month>(entity =>
            {
                entity.ToTable("sp_Sales_for_Month", "Dashboard");
                entity.HasKey(x => x.mes);

                entity.Property(x => x.año).HasColumnName("año");
                entity.Property(x => x.Ventas).HasColumnName("Ventas");
            });

            modelBuilder.Entity<sp_Sales_for_Month_products>(entity =>
            {
                entity.ToTable("sp_Sales_for_Month_products", "Dashboard");
                entity.HasKey(x => x.months);
                entity.Property(x => x.months).HasColumnName("months");
                entity.Property(x => x.Sales).HasColumnName("Sales");
                entity.Property(x => x.years).HasColumnName("years");
                entity.Property(x => x.Name).HasColumnName("Name");
            });

            modelBuilder.Entity<prc_Sales_for_Month_branchOffice>(entity =>
            {
                entity.ToTable("sp_Sales_for_Month_branchOffice", "Dashboard");
                entity.HasKey(x => x.months);
                entity.Property(x => x.months).HasColumnName("months");
                entity.Property(x => x.Sales).HasColumnName("Sales");
                entity.Property(x => x.years).HasColumnName("years");
                entity.Property(x => x.Name).HasColumnName("Name");
            });

            modelBuilder.Entity<pr_sp_Sales_for_Month_Customer>(entity =>
            {
                entity.ToTable("sp_Sales_for_Month_Customer", "Dashboard");
                entity.HasKey(x => x.months);
                entity.Property(x => x.months).HasColumnName("months");
                entity.Property(x => x.Sales).HasColumnName("Sales");
                entity.Property(x => x.years).HasColumnName("years");
                entity.Property(x => x.Names).HasColumnName("Names");
            });

            modelBuilder.Entity<prc_sp_Sales_for_Month_seller>(entity =>
            {
                entity.ToTable("prc_sp_Sales_for_Month_seller", "Dashboard");
                entity.HasKey(x => x.months);
                entity.Property(x => x.months).HasColumnName("months");
                entity.Property(x => x.Sales).HasColumnName("Sales");
                entity.Property(x => x.years).HasColumnName("years");
                entity.Property(x => x.Name).HasColumnName("Name");
            });


            modelBuilder.Entity<GetProductSalesPoint>(entity =>
            {
                entity.ToTable("GetProductSalesPoint", "Customer");
                entity.HasKey(x => x.Id);

                entity.Property(x => x.Id).HasColumnName("Id");
                entity.Property(x => x.ProductName).HasColumnName("Name");
                entity.Property(x => x.Code).HasColumnName("Code");
                entity.Property(x => x.Price).HasColumnName("Price");
                entity.Property(x => x.Picture).HasColumnName("Picture");
                entity.Property(x => x.TotalProduct).HasColumnName("TotalProduct");
                entity.Property(x => x.Abbreviation).HasColumnName("Abbreviation");
                entity.Property(x => x.Selllowcost).HasColumnName("Selllowcost");
                entity.Property(x => x.InvoiceWithoutExistence).HasColumnName("InvoiceWithoutExistence");
                entity.Property(x => x.TaxAbbreviation).HasColumnName("taxAbbreviation");
                entity.Property(x => x.MeasureId).HasColumnName("MeasureId");
                entity.Property(x => x.Tax).HasColumnName("TaxQuantity");
            });

            modelBuilder.Entity<PrcSearchProducts>(entity =>
            {
                entity.ToTable("Customer.prc_SearchProducts", "Customer");
                entity.HasKey(x => x.Id);

                entity.Property(x => x.Id).HasColumnName("Id");
                entity.Property(x => x.ProductName).HasColumnName("Name");
                entity.Property(x => x.Code).HasColumnName("Code");
                entity.Property(x => x.Price).HasColumnName("Price");
                entity.Property(x => x.Picture).HasColumnName("Picture");
                entity.Property(x => x.TotalProduct).HasColumnName("TotalProduct");
                entity.Property(x => x.Abbreviation).HasColumnName("Abbreviation");
                entity.Property(x => x.Selllowcost).HasColumnName("Selllowcost");
                entity.Property(x => x.InvoiceWithoutExistence).HasColumnName("InvoiceWithoutExistence");
                entity.Property(x => x.TaxAbbreviation).HasColumnName("taxAbbreviation");
                entity.Property(x => x.MeasureId).HasColumnName("MeasureId");
                entity.Property(x => x.Tax).HasColumnName("TaxQuantity");
            });

            modelBuilder.Entity<GetInvoiceDetail>(entity =>
            {
                entity.ToTable("Customer.GetInvoiceDetail", "Customer");
                entity.HasKey(x => x.Id);

                entity.Property(x => x.Id).HasColumnName("Id");
                entity.Property(x => x.ProductId).HasColumnName("ProductId");
                entity.Property(x => x.ProductName).HasColumnName("Name");
                entity.Property(x => x.Code).HasColumnName("Code");
                entity.Property(x => x.Price).HasColumnName("Price");
                entity.Property(x => x.Picture).HasColumnName("Picture");
                entity.Property(x => x.TotalProduct).HasColumnName("TotalProduct");
                entity.Property(x => x.MeaseureAbbreviation).HasColumnName("MeaseureAbbreviation");
                entity.Property(x => x.Selllowcost).HasColumnName("Selllowcost");
                entity.Property(x => x.InvoiceWithoutExistence).HasColumnName("InvoiceWithoutExistence");
                entity.Property(x => x.TaxAbbreviation).HasColumnName("taxAbbreviation");
                entity.Property(x => x.Tax).HasColumnName("TaxQuantity");
                entity.Property(x => x.ItemQuantity).HasColumnName("ItemQuantity");
                entity.Property(x => x.ItemDiscount).HasColumnName("Discount");
                entity.Property(x => x.AmountDiscount).HasColumnName("AmountDiscount");
                entity.Property(x => x.AmountTax).HasColumnName("AmountTax");
                entity.Property(x => x.SubTotal).HasColumnName("Total");
                entity.Property(x => x.WarehouseId).HasColumnName("WarehouseId");
                entity.Property(x => x.MeasureId).HasColumnName("MeasureId");
                entity.Property(x => x.WareHouseName).HasColumnName("WareHouseName");
            });

            modelBuilder.Entity<prc_PurchaseOrderRow>(entity =>
            {
                entity.ToTable("prc_PurchaseOrderAvailable");
                entity.HasKey(x => x.PurchaseOrderId);

                entity.Property(x => x.ProductId).HasColumnName("ProductId");
                entity.Property(x => x.PurchaseOrderId).HasColumnName("PurchaseOrderId");
                entity.Property(x => x.MeasureId).HasColumnName("MeasureId");
                entity.Property(x => x.WareHouse).HasColumnName("WareHouse");
                entity.Property(x => x.WarehouseId).HasColumnName("WareHouseId");
                entity.Property(x => x.TaxAbbreviation).HasColumnName("TaxAbbreviation");
                entity.Property(x => x.PurchaseOrderRowId).HasColumnName("PurchaseOrderRowId");
                entity.Property(x => x.Quantity).HasColumnName("Quantity");
                entity.Property(x => x.Disponible).HasColumnName("Disponible");
                entity.Property(x => x.Cost).HasColumnName("Cost");
                entity.Property(x => x.Discount).HasColumnName("Discount");
                entity.Property(x => x.Tax).HasColumnName("Tax");
                entity.Property(x => x.AmountTax).HasColumnName("AmountTax");
                entity.Property(x => x.Total).HasColumnName("Total");
                entity.Property(x => x.Produc_Description).HasColumnName("Produc_Description");
                entity.Property(x => x.AmountDiscount).HasColumnName("AmountDiscount");
            });

            modelBuilder.Entity<Setting>(entity =>
            {
                entity.ToTable("General.GetSetting");

                entity.Property(e => e.Id).HasColumnName("Id");
                entity.Property(e => e.SettingName).HasColumnName("SettingName");
                entity.Property(e => e.ValueName).HasColumnName("ValueName");
                entity.Property(e => e.SettingValue).HasColumnName("SettingValue");
            });




            #region Customer

            modelBuilder.Entity<GetAllCreditNote>(entity =>
            {
                entity.ToTable("Customer.prc_GetAllCreditNote", "Customer");
                entity.HasKey(x => x.Id);

                entity.Property(x => x.DocumentNumber).HasColumnName("DocumentNumber");
                entity.Property(x => x.CustomerName).HasColumnName("Names");
                entity.Property(x => x.InvoiceNumber).HasColumnName("InvoiceNumber");
                entity.Property(x => x.NCF).HasColumnName("NCF");
                entity.Property(x => x.NCFModified).HasColumnName("NCFModified");
                entity.Property(x => x.CreateDate).HasColumnName("CreateDate");
                entity.Property(x => x.Total).HasColumnName("Total");
                entity.Property(x => x.DocumentEstatusId).HasColumnName("DocumentEstatusId");
            });


            modelBuilder.Entity<prc_InvoiceWithOutCreditNote>(entity =>
            {
                entity.ToTable("Customer.prc_InvoiceWithOutCreditNote", "Customer");
                entity.HasKey(x => x.Id);

                entity.Property(x => x.DocumentNumber).HasColumnName("DocumentNumber");
                entity.Property(x => x.CustomerName).HasColumnName("Customer");
                entity.Property(x => x.NCF).HasColumnName("NCF");
                entity.Property(x => x.NcfId).HasColumnName("NcfId");
                entity.Property(x => x.CreateDate).HasColumnName("CreateDate");
                entity.Property(x => x.Total).HasColumnName("Total");
                entity.Property(x => x.DocumentStatusId).HasColumnName("DocumentEstatusId");
            });


            modelBuilder.Entity<PrcGetAllInvoce>(entity =>
            {
                entity.ToTable("prc_GetAllInvoce", "Customer");
                entity.HasKey(x => x.Id);

                entity.Property(x => x.Id).HasColumnName("Id");
                entity.Property(x => x.DocumentNumber).HasColumnName("DocumentNumber");
                entity.Property(x => x.CustomerName).HasColumnName("CustomerName");
                entity.Property(x => x.DocumentDate).HasColumnName("DocumentDate");
                entity.Property(x => x.ExpirationDate).HasColumnName("ExpirationDate");
                entity.Property(x => x.DocumentStatus).HasColumnName("DocumentEstatusId");
                entity.Property(x => x.NCF).HasColumnName("NCF");
                entity.Property(x => x.Balance).HasColumnName("Balance");
                entity.Property(x => x.Total).HasColumnName("Total");
                entity.Property(x => x.EmployeeCreate).HasColumnName("EmployeeCreate");
            });


            modelBuilder.Entity<GetProductsOnSale>(entity =>
            {
                entity.ToTable("GetProductsOnSale", "Customer");
                entity.HasKey(x => x.Id);

                entity.Property(x => x.Id).HasColumnName("Id");
                entity.Property(x => x.Code).HasColumnName("Code");
                entity.Property(x => x.Name).HasColumnName("NAME");
                entity.Property(x => x.Picture).HasColumnName("Picture");
                entity.Property(x => x.ProductAmount).HasColumnName("ProductAmount");
                entity.Property(x => x.InvoiceWithoutExistence).HasColumnName("InvoiceWithoutExistence");
                entity.Property(x => x.Price).HasColumnName("Price");
                entity.Property(x => x.TaxAbbreviation).HasColumnName("taxAbbreviation");
                entity.Property(x => x.Tax).HasColumnName("TaxQuantity");
                entity.Property(x => x.IsService).HasColumnName("IsService");
                entity.Property(x => x.abbreviation).HasColumnName("abbreviation");
            });

            //Mapea la entidad con el procedimiento que obtiene todas los pedidos
            modelBuilder.Entity<GetOrders>(entity =>
            {
                entity.ToTable("GetOrders", "Customer");
                entity.HasKey(x => x.Id);
                entity.Property(x => x.Id).HasColumnName("OrderId");
                entity.Property(x => x.OrderNumber).HasColumnName("OrderNumber");
                entity.Property(x => x.InvoiceNumber).HasColumnName("InvoiceNumber");
                entity.Property(x => x.CustomerName).HasColumnName("CustomerName");
                entity.Property(x => x.CreateDate).HasColumnName("CreateDate");
                entity.Property(x => x.DocumentEstatusId).HasColumnName("DocumentEstatusId");
                entity.Property(x => x.ProductName).HasColumnName("ProductName");
                entity.Property(x => x.Quantity).HasColumnName("Quantity");
                entity.Property(x => x.ProductPicture).HasColumnName("ProductPicture");
            });


            #endregion


            #endregion
        }
    }
}
