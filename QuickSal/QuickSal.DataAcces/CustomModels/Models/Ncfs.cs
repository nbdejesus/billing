﻿using QuickSal.DataAcces.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuickSal.DataAcces.Models
{
    public class Ncfs
    {
        public IQueryable<TblNcf> ncfs { get; set; }
    
        public int Id { get; set; }
        public int ID { get; set; }
        public long BusinessId { get; set; }
        public short PrefixId { get; set; }
        public long? UserId { get; set; }
        public long? EmployeeBusinessId { get; set; }
        public string Description { get; set; }
        public string Prefi { get; set; }
        public long CounterFrom { get; set; }
        public long CounterTo { get; set; }
        public DateTime CreateDate { get; set; }
        public bool Estatus { get; set; }
        public bool IsExhausted { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }
        public string NCF { get; set; }
        public long RowId { get; set; }
        public TblNcfrow TblNcfrow { get; set; }
        public TblBusiness Business { get; set; }
        public TblEmployeeBusiness EmployeeBusiness { get; set; }
        public TblNcfprefix Prefix { get; set; }
        public TblUsers User { get; set; }
        public bool Used { get; set; }
    }
}
