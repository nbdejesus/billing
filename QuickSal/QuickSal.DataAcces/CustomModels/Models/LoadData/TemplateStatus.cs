﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickSal.DataAcces.CustomModels.Models.LoadData
{
    public class TemplateStatus
    {
        public int RowNumber { get; set; }
        public string Object { get; set; }

        public string Error { get; set; }
    }
}
