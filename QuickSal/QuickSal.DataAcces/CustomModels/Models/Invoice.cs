﻿using QuickSal.DataAcces.Contexts;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuickSal.DataAcces.Models
{
   public class Invoice
    {
        public long Id { get; set; }
        public long BranchOfficeId { get; set; }
        public long? CustomerId { get; set; }
        public short? CreditTypeId { get; set; }
        public short? NcfId { get; set; }
        public byte DocumentEstatusId { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime DocumentDate { get; set; }
        public string CustomerName { get; set; }
        public string Observation { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Discount { get; set; }
        public decimal TotalTax { get; set; }
        public decimal Total { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string Reference { get; set; }
        public string Ncf { get; set; }
        public decimal Balance { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }
        /// <summary>
     
        public long BusinessId { get; set; }
        public byte DocumentTypeId { get; set; }
      
        public string Code { get; set; }
        public string Names { get; set; }
        public string PhoneNumber { get; set; }
        public string Movil { get; set; }
        public string AddressCustomers { get; set; }
        public string Picture { get; set; }
        public int? CountryId { get; set; }
        public int? ProvinceId { get; set; }
        public bool Estatus { get; set; }
        public string Email { get; set; }
 
        /// </summary>
        public TblBranchOffice BranchOffice { get; set; }
        public TblCustomers Customer { get; set; }
        public ICollection<TblCustomerPaymentsRow> TblCustomerPaymentsRow { get; set; }
        public ICollection<TblInvoiceRow> TblInvoiceRow { get; set; }

    }
}
