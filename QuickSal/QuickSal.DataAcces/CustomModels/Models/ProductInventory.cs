﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickSal.DataAcces.Models
{
    public class ProductInventory
    {
        public long ProductId { get; set; }
        public decimal Amount { get; set; }
        public string Product { get; set; }
        public string Descripcion { get; set; }
    }
}
