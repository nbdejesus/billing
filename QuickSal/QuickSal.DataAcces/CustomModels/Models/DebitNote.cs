﻿using QuickSal.DataAcces.Contexts;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuickSal.DataAcces.Models
{
   public class DebitNote
    {
        public long Id { get; set; }
        public long BranchOfficeId { get; set; }
        public bool Used { get; set; }
        public long SuppliersId { get; set; }
        public long PurchaseOrderId { get; set; }
        public string PurchaseOrder { get; set; }
        public byte DocumentEstatusId { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Observation { get; set; }
        public decimal SubTotal { get; set; }
        public decimal TotalTax { get; set; }
        public decimal Total { get; set; }
        public string Ncf { get; set; }
        public string Ncfmodified { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }
        public string SupplierName { get; set; }
        public IEnumerable<TblDebitNote> tblDebitNotes { get; set; }
        public IEnumerable<DebitNote> DebitNotes { get; set; }
        public IEnumerable<DebitNote> DebitNoteStatu { get; set; }
    }
}
