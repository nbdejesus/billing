﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Procedure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuickSal.DataAcces.Models
{
    public class MovingWareHouseCls
    {
        public int Id { get; set; }
        public string DocumentNo { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public long EmployeeModifyId { get; set; }
        public long EmployeeCreateId { get; set; }
        public long SenderWarehouseId { get; set; }
        public long ReceiverWarehouseId { get; set; }
        public string DescriptionHouse { get; set; }
        public int StateHouse { get; set; }

        public IEnumerable<TblMovingWarehouseRow> warehouseRows { get; set; }
        public IQueryable<PrGetMovingWareDetails> prGetMovingWareDetails { get; set; }


    }
}
