﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickSal.DataAcces.Models
{
    public class DebitNoteRowVM
    {

        public long Id { get; set; }
        public int Index { get; set; }
        public long DebitNoteId { get; set; }
        public int WarehouseId { get; set; }
        public string Warehouse { get; set; }
        public long MeasureId { get; set; }
        public string Measure { get; set; }
        public long ProductId { get; set; }
        public string Product { get; set; }
        public string TaxAbbreviation { get; set; }
        public decimal Quantity { get; set; }
        public decimal Cost { get; set; }
        public decimal Tax { get; set; }
        public decimal AmountTax { get; set; }
        public decimal Discount { get; set; }
        public decimal AmountDiscount { get; set; }
        public decimal Total { get; set; }
        public string Accion { get; set; }
        public string Delete { get; set; }
        public string Edit { get; set; }
        public long PurchaseOrderId { get; set; }

        public decimal CantidadProducto { get; set; }

    }
}
