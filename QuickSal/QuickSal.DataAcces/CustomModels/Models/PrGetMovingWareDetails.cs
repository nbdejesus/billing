﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickSal.DataAcces.Procedure
{
    public class PrGetMovingWareDetails
    {
        public long Id { get; set; }
        public string DocumentNo { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string EmployeeModifyId { get; set; }
        public string EmployeeCreateId { get; set; }
        public string SenderWarehouse { get; set; }
        public string ReceiverWarehouse { get; set; }
        public int SenderWarehouseId { get; set; }      
        public int ReceiverWarehouseId { get; set; }
        public string DescriptionHouse { get; set; }
        public int StateHouse { get; set; }
        public decimal Amount { get; set; }
        public decimal Disponible { get; set; }
        public string Delete { get; set; }
        public string Editar { get; set; }
        //public long ProductId { get; set; }
        public string Product { get; set; }
        public long ProductId { get; set; }
        public int State { get; set; }
        public string StateDescripcion { get; set; }
        public string Description { get; set; }
    }
}
