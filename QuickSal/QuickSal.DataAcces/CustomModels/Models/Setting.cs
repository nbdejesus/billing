﻿namespace QuickSal.DataAcces.CustomModels.Models
{
    public class Setting
    {
        public int Id { get; set; }
        public string SettingName { get; set; }
        public string ValueName { get; set; }
        public string SettingValue { get; set; }
    }
}
