﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickSal.DataAcces.CustomModels.Models.Plan
{
    public class TemplateStatus
    {
        public int ObjectId { get; set; }
        public string ObjectName { get; set; }
        public string ObjectIcon { get; set; }

        public int SubObjecId { get; set; }
        public string SubObjectName { get; set; }
        public string SubObjectIcon { get; set; }
    }
}
