﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace QuickSal.DataAcces.Models
{
    public class TaskResult
    {
        /// <summary>
        /// Determine if a Task has been executed Succesfully
        /// </summary>
        public bool ExecutedSuccesfully { get; set; }

        /// <summary>
        /// List of all messages that were recorded while performing the task
        /// </summary>
        public IList<string> MessageList { get; private set; } = new List<string>();

        /// <summary>
        /// In case we have an exception performing our task
        /// </summary>
        public Exception Exception { get; set; }

        public TaskResult()
        {
            ExecutedSuccesfully = true;
        }

        /// <summary>
        /// Summary of messages. This can include error messages (if ExecutedSuccesfully = false) or
        /// a result message (if ExecutedSuccesfully = true)
        /// </summary>
        public string Messages()
        {
            if (Exception != null)
            {
                AddErrorMessage($"Exception: {Exception.ToString()}");
            }
            return String.Join("\n \n", MessageList);
        }

        public void AddErrorMessage(string errorMessage)
        {
            ExecutedSuccesfully = false;
            MessageList.Add(errorMessage);
        }

        public void AddMessage(string message)
        {
            MessageList.Add(message);
        }

        public void AddAllMessages(IList<string> messages)
        {
            MessageList = MessageList.Concat(messages).ToList();
        }

        public void AppendTaskResultData(TaskResult taskResult)
        {
            Exception = taskResult.Exception;

            AddAllMessages(taskResult.MessageList);

            if (ExecutedSuccesfully != false)
            {
                ExecutedSuccesfully = taskResult.ExecutedSuccesfully;
            }
        }
    }
}
