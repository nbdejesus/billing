﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickSal.DataAcces.Models
{
    public class SubObjectPlanDetail
    {
        public int SubObjectId { get; set; }
        public int  PlanId { get; set; }
        public byte? TypeMethodId { get; set; }
        public string Method { get; set; }
    }
}
