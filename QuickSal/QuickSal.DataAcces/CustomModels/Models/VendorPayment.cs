﻿using QuickSal.DataAcces.Contexts;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuickSal.DataAcces.Models
{
    public class VendorPayment
    {

        public long Id { get; set; }
        public long VendorPaymentId { get; set; }
        public long SuppliersId { get; set; }
        public long PurchaseOrderId { get; set; }
        public string PurchaseOrderDocument { get; set; }
        public string SupplierName { get; set; }
        public string Concept { get; set; }
        public decimal Owed { get; set; }
        public decimal PaidOut { get; set; }
        public decimal Payment { get; set; }
        public decimal Pending { get; set; }

        public TblBranchOffice BranchOffice { get; set; }
        public TblEmployeeBusiness EmployeeBusiness { get; set; }
        public ICollection<TblVendorPaymentRow> TblVendorPaymentRow { get; set; }
    }
}
