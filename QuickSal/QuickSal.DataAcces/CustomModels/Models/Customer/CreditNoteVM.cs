﻿using System;

namespace QuickSal.DataAcces.Models.Customer
{
    public class CreditNoteVM
    {
        public long Id { get; set; }
        public long BranchOfficeId { get; set; }
        public long CustomerId { get; set; }
        public long InvoiceId { get; set; }
        public string Invoice { get; set; }
        public byte DocumentEstatusId { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Observation { get; set; }
        public decimal SubTotal { get; set; }
        public decimal TotalTax { get; set; }
        public decimal Total { get; set; }
        public string Ncf { get; set; }
        public string Ncfmodified { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }
        public string SupplierName { get; set; }
        public string Used { get; set; }
    }
}
