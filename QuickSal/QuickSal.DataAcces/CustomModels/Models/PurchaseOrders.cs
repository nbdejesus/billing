﻿using QuickSal.DataAcces.Contexts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace QuickSal.DataAcces.Models
{
   public class PurchaseOrders
    {       

        public long Id { get; set; }
        public long BranchOfficeId { get; set; }
        public int WarehouseId { get; set; }
        public long SuppliersId { get; set; }
        public long? UserId { get; set; }
        public long? EmployeeBusinessId { get; set; }
        public byte DocumentEstatusId { get; set; }
        public string DocumentNumber { get; set; }
        public string Suplidor { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime DocumentDate { get; set; }
        public string Observation { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Discount { get; set; }
        public decimal TotalTax { get; set; }
        public decimal Total { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string Reference { get; set; }
        public string Ncf { get; set; }
        public string Produc_Description { get; set; }
        public string Almacen_Description { get; set; }
        public string ITBIS { get; set; }
        public decimal Balance { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }
        public long ProductId { get; set; }
        public long MeasureId { get; set; }
        public decimal Quantity { get; set; }
        public decimal Cost { get; set; }
        public decimal Tax { get; set; }
        public decimal AmountTax { get; set; }
        public long PurchaseOrderId { get; set; }
        public decimal AmountDiscount { get; set; }
   

        public TblBranchOffice BranchOffice { get; set; }
        public TblEmployeeBusiness EmployeeBusiness { get; set; }
        public TblSuppliers Suppliers { get; set; }
        public TblUsers User { get; set; }
        public TblWarehouse Warehouse { get; set; }
        public ICollection<TblPurchaseOrderRow> TblPurchaseOrderRow { get; set; }
    }
}
