﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickSal.DataAcces.Models
{
    public class CustomerPayments
    {
        public long Id { get; set; }
        public long CustomerId { get; set; }
        public long CustomerPaymentId { get; set; }
        public long InvoiceId { get; set; }
        public string InvoiceDocument { get; set; }
        public string CustomerName { get; set; }
        public string Concept { get; set; }
        public decimal Owed { get; set; }
        public decimal PaidOut { get; set; }
        public decimal Payment { get; set; }
        public decimal Pending { get; set; }

        public long BranchOfficeId { get; set; }
        public byte DocumentEstatusId { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Observation { get; set; }
        public decimal Total { get; set; }
        public long EmployeeCreateId { get; set; }
        public long EmployeeModifyId { get; set; }
    }
}
