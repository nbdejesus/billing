﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickSal.DataAcces.Models
{
   public class ValidacionProducto
    {
        public decimal Quantity { get; set; }
        public string Name { get; set; }
    }
}
