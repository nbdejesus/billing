﻿using Microsoft.EntityFrameworkCore;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.CustomModels.Models;
using QuickSal.DataAcces.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class GlobalRepository : QuicksalRepository<TblTempFile>, IGlobalRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public GlobalRepository(QuickSalContext context) : base(context)
        { }


        /// <summary>
        /// Proceso para obtener el listado de paises registrados
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TblCountry> GetAllCountries()
        {

            return context.TblCountry.OrderBy(x => x.Description);
        }


        /// <summary>
        /// Meotodo para obtener todos los prefijos de los NCF
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TblNcfprefix> GetAllPrefix()
        {

            return context.TblNcfprefix.ToList();
        }

        /// <summary>
        /// Proceso para obtener el listado de documentos
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TblDocumentIdentification> GetAllDocumentIdentification()
        {
            return context.TblDocumentIdentification;
        }


        /// <summary>
        /// Metodo para obtener un tipo de identificacion
        /// </summary>
        /// <param name="Id">Primary key del tipo</param>
        /// <returns></returns>
        public async Task<TblDocumentIdentification> GetDocumentIdentification(int Id)
        {
            return await context.TblDocumentIdentification.Where(x => x.Id == Id).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Proceso para agregar la ruta de un archivo temporal
        /// </summary>
        /// <param name="entity"></param>
        public TaskResult AddTempFileEntity(TblTempFile entity)
        {
            return AddEntity(entity);
        }

        /// <summary>
        /// Metodo para eliminar un archivo temporal
        /// </summary>
        /// <param name="entity">Modelo con la informacion</param>
        /// <returns></returns>
        public void DeleteTempFile(TblTempFile entity)
        {
            Delete(entity);
        }


        /// <summary>
        /// Metodo para eliminar archivo temporales de la tabal temp files
        /// </summary>
        /// <param name="entity">Modelo con la informacion</param>
        /// <returns></returns>
        public void DeleteRangeTempFile(IEnumerable<TblTempFile> entity)
        {
            DeleteRange(entity);
        }

        /// <summary>
        /// Proceso para obtener los objetos temporales almacenados en la DB
        /// </summary>
        /// <param name="UserId">Primary key del usuario que grabo el archivo</param>
        /// <param name="FileType">Tipo de archivo que se registro en la tabla temporal</param>
        /// <returns></returns>
        public IQueryable<TblTempFile> GetTempFiles(long UserId, byte FileType)
        {
            return context.TblTempFile.Where(x => x.UserId == UserId && x.TypeFile == FileType);
        }

        /// <summary>
        /// Proceso para obtener todas las provincias de un pais
        /// </summary>
        /// <param name="CountryId">Primary key del pais</param>
        /// <returns></returns>
        public IQueryable<TblProvince> GetAllProvince(int CountryId)
        {
            return context.TblProvince.Where(x => x.CountryId == CountryId);
        }

        /// <summary>
        /// Metodo para obtener el o los valores de un parametro
        /// </summary>
        /// <param name="nameSetting">Parametros</param>
        /// <param name="valueName">Nombre del valor</param>
        /// <returns></returns>
        public async Task<IQueryable<Setting>> GetSettingValue(string nameSetting, string valueName = null)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("@NameSetting", nameSetting);
            parameters.Add("@ValueName", valueName);
            return ExecuteQuerys(new Setting(), "General.GetSetting", parameters);
        }

        /// <summary>
        /// Proceso para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }
    }

    public interface IGlobalRepository
    {
        /// <summary>
        /// Proceso para obtener todas las provincias de un pais
        /// </summary>
        /// <param name="CountryId">Primary key del pais</param>
        /// <returns></returns>
        IQueryable<TblProvince> GetAllProvince(int CountryId);

        /// <summary>
        /// Proceso para obtener los objetos temporales almacenados en la DB
        /// </summary>
        /// <param name="UserId">Primary key del usuario que grabo el archivo</param>
        /// <param name="FileType">Tipo de archivo que se registro en la tabla temporal</param>
        /// <returns></returns>
        IQueryable<TblTempFile> GetTempFiles(long UserId, byte FileType);

        /// <summary>
        /// Proceso para agregar la ruta de un archivo temporal
        /// </summary>
        /// <param name="entity"></param>
        TaskResult AddTempFileEntity(TblTempFile entity);

        /// <summary>
        /// Proceso para obtener el listado de documentos
        /// </summary>
        /// <returns></returns>
        IEnumerable<TblDocumentIdentification> GetAllDocumentIdentification();


        /// <summary>
        /// Metodo para obtener un tipo de identificacion
        /// </summary>
        /// <param name="Id">Primary key del tipo</param>
        /// <returns></returns>
        Task<TblDocumentIdentification> GetDocumentIdentification(int Id);

        /// <summary>
        /// Proceso para obtener el listado de paises registrados
        /// </summary>
        /// <returns></returns>
        IEnumerable<TblCountry> GetAllCountries();

        /// <summary>
        /// Meotodo para obtener todos los prefijos de los NCF
        /// </summary>
        /// <returns></returns>
        IEnumerable<TblNcfprefix> GetAllPrefix();

        /// <summary>
        /// Metodo para eliminar un archivo temporal
        /// </summary>
        /// <param name="entity">Modelo con la informacion</param>
        /// <returns></returns>
        void DeleteTempFile(TblTempFile entity);

        /// <summary>
        /// Metodo para eliminar archivo temporales de la tabal temp files
        /// </summary>
        /// <param name="entity">Modelo con la informacion</param>
        /// <returns></returns>
        void DeleteRangeTempFile(IEnumerable<TblTempFile> entity);

        /// <summary>
        /// Metodo para obtener el o los valores de un parametro
        /// </summary>
        /// <param name="nameSetting">Parametros</param>
        /// <param name="valueName">Nombre del valor</param>
        /// <returns></returns>
        Task<IQueryable<Setting>> GetSettingValue(string nameSetting, string valueName = null);

        /// <summary>
        /// Proceso para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
