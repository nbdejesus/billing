﻿using Microsoft.EntityFrameworkCore;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Procedure;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class ProductRepository : QuicksalRepository<TblProducts>, IProductRepository
    {

        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public ProductRepository(QuickSalContext context) : base(context)
        { }

        /// <summary>
        /// Metodo para obtener todos los productos de una empresa
        /// </summary>
        /// <param name="businessId"></param>
        /// <returns></returns>
        public IQueryable<PrcGetAllProducts> GetAllProduct(long businessId)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("@BusinessId", businessId);
            return ExecuteQuerys(new PrcGetAllProducts(), "prc_GetAllProducts", parameters);
        }

        /// <summary>
        /// Metodo para obtener la información de un producto
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public TblProducts GetProduct(long Id)
        {
            var query = context.TblProducts.Where(x => x.Id == Id)
                                .Include(x => x.SubCategory)
                                .Include(x => x.TblProductsPrices)
                                .Include(x => x.TblInventoryWarehouse).FirstOrDefault();

            return query;
        }

        /// <summary>
        /// Metodo para obtener el ultimo código generado
        /// </summary>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        public string LastProductCode(long businessId)
        {
            var code = (from c in context.TblProducts
                        where c.BusinessId == businessId
                        orderby c.CreateDate descending
                        select c.Code).FirstOrDefault();

            return code;
        }
        public IQueryable<TblProducts> Products(long Id)
        {
            var response = context.TblProducts.OrderByDescending(x => x.Description)
                .Include(m => m.Measure)
                .Include(x => x.Tax).Where(b => b.BusinessId == Id && b.Estatus == true);
            return response;
        }

        /// <summary>
        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }
    }

    public interface IProductRepository : IQuicksalBase<TblProducts>
    {
        /// <summary>
        /// Metodo para obtener todos los productos de una empresa
        /// </summary>
        /// <param name="businessId"></param>
        /// <returns></returns>
        IQueryable<PrcGetAllProducts> GetAllProduct(long businessId);

        IQueryable<TblProducts> Products(long Id);
        /// <summary>
        /// Metodo para obtener la información de un producto
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        TblProducts GetProduct(long Id);

        /// <summary>
        /// Metodo para obtener el ultimo código generado
        /// </summary>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        string LastProductCode(long businessId);

        /// <summary>
        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
