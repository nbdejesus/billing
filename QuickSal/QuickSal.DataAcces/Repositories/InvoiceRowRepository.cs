﻿using Microsoft.EntityFrameworkCore;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Procedure.Customer;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class InvoiceRowRepository : QuicksalRepository<TblInvoiceRow>, IInvoiceRowRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public InvoiceRowRepository(QuickSalContext context) : base(context)
        {

        }

        /// <summary>
        /// Metodo para obtener el detalle de una factura
        /// </summary>
        /// <param name="InvoiceId">Primary key de la factura</param>
        /// <returns></returns>
        public IQueryable<GetInvoiceDetail> GetInvoiceDetail(long InvoiceId)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("@InvoiceId", InvoiceId);
            return ExecuteQuerys(new GetInvoiceDetail(), "Customer.GetInvoiceDetail", parameters);
        }


        /// <summary>
        /// Metodo para obtener el detalle de una factura de ventas
        /// </summary>
        /// <param name="InvoiceId">Primary key de la factura</param>
        /// <returns></returns>
        public IQueryable<TblInvoiceRow> GetInvoiceRows(long InvoiceId)
        {
            return context.TblInvoiceRow.Where(x => x.InvoiceId == InvoiceId).Include(x => x.Warehouse).Include(x => x.Product);
        }


        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }
    }

    public interface IInvoiceRowRepository : IQuicksalBase<TblInvoiceRow>
    {     
        /// <summary>
        /// Metodo para obtener el detalle de una factura
        /// </summary>
        /// <param name="InvoiceId">Primary key de la factura</param>
        /// <returns></returns>
        IQueryable<GetInvoiceDetail> GetInvoiceDetail(long InvoiceId);

        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
