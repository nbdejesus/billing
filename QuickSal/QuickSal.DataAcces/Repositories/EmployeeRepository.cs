﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Views;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class EmployeeRepository : QuicksalRepository<TblEmployeeBusiness>, IEmployeeRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public EmployeeRepository(QuickSalContext context) : base(context)
        { }

        /// <summary>
        /// Proceso para obtener la informacion de un usuario que no es empleado
        /// </summary>
        /// <param name="email">Email del usuario</param>
        /// <returns></returns>
        public TblUsers GetUserNotEmployee(string email, long businessID)
        {
            var queryUser = (from c in context.TblUsers
                             where c.Email == email
                             select c).FirstOrDefault();

            bool exist = true;
            if (queryUser != null)
                exist = context.TblEmployeeBusiness.Where(x => x.BusinessId == businessID && x.UserId == queryUser.Id).Any();

            if (exist)
                return null;
            else
                return queryUser;
        }


        /// <summary>
        /// Proceso para obtener los objetos permitidos para un empleado
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <param name="privilegeId">Primary key del privilegio (opcional)</param>
        /// <returns></returns>
        public IQueryable<vwObjectsBusiness> GetObjectsEmployee(long BusinessId, int privilegeId = 0)
        {
            return context.Set<vwObjectsBusiness>().Where(x => x.BusinessId == BusinessId &&
                                                         x.PrivilegesId == ((privilegeId == 0) ? x.PrivilegesId : privilegeId));
        }

        /// <summary>
        /// Proceso para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }
    }

    public interface IEmployeeRepository : IQuicksalBase<TblEmployeeBusiness>
    {
        /// <summary>
        /// Proceso para obtener la informacion de un usuario que no es empleado
        /// </summary>
        /// <param name="email">Email del usuario</param>
        /// <returns></returns>
        TblUsers GetUserNotEmployee(string email, long businessID);

        /// <summary>
        /// Proceso para obtener los objetos permitidos para un empleado
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <param name="privilegeId">Primary key del privilegio (opcional)</param>
        /// <returns></returns>
        IQueryable<vwObjectsBusiness> GetObjectsEmployee(long BusinessId, int privilegeId = 0);

        /// <summary>
        /// Proceso para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
