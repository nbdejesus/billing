﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class TaxRepository : QuicksalRepository<TblTax>, ITaxRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public TaxRepository(QuickSalContext context) : base(context)
        { }

        /// <summary>
        /// Metodo para obtener todos los impuestos del sistema
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        public IQueryable<TblTax> GetTaxes(long BusinessId)
        {
            return context.TblTax.Where(x => x.BusinessId == BusinessId || !x.BusinessId.HasValue).OrderByDescending(x => x.CreateDate);
        }

        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }
    }

    public interface ITaxRepository : IQuicksalBase<TblTax>
    {
        /// <summary>
        /// Metodo para obtener todos los impuestos del sistema
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        IQueryable<TblTax> GetTaxes(long BusinessId);

        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
