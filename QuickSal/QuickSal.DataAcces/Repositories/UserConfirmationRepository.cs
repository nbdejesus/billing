﻿using QuickSal.DataAcces.Contexts;
using System.Linq;

namespace QuickSal.DataAcces.Repositories
{
    public class UserConfirmationRepository : QuicksalRepository<TblUserConfirmation>, IUserConfirmationRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public UserConfirmationRepository(QuickSalContext context) : base(context)
        { }

        /// <summary>
        /// Proceso para obtener la ultima confirmacion del usuario
        /// </summary>
        /// <param name="UserId">Primary key del usuario</param>
        /// <returns></returns>
        public string GetUserConfirmation(long UserId)
        {
            return context.TblUserConfirmation.Where(x => x.UserId == UserId).LastOrDefault().ConfirmationKey;
        }
    }

    public interface IUserConfirmationRepository : IQuicksalBase<TblUserConfirmation>
    {
        string GetUserConfirmation(long UserId);
    }
}
