﻿using Microsoft.EntityFrameworkCore;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class PurchaseOrderRepositoy : QuicksalRepository<TblPurchaseOrder>, IPurchaseOrderRepositoy
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }
        public PurchaseOrderRepositoy(QuickSalContext context) : base(context)
        {

        }
        /// Metodo que trae el listado de compras
        /// </summary>
        /// <returns></returns>
        public IQueryable<PurchaseOrders> Purchase(long branchOfficeId)
        {
            var queryPurchas = (from Pur in context.TblPurchaseOrder
                                join Sup in context.TblSuppliers on Pur.SuppliersId equals Sup.Id
                                where Pur.BranchOfficeId == branchOfficeId
                                select new PurchaseOrders
                                {
                                    Suplidor = Sup.Names,
                                    CreateDate = Pur.CreateDate,
                                    Id = Pur.Id,
                                    SuppliersId = Pur.SuppliersId,
                                    Balance = Pur.Balance,
                                    Ncf = Pur.Ncf,
                                    DocumentNumber = Pur.DocumentNumber,
                                    TotalTax = Pur.TotalTax,
                                    Total = Pur.Total,
                                    DocumentEstatusId = Pur.DocumentEstatusId
                                });

            return queryPurchas;
        }

        /// Metodo que trae el listado de detalle correspondiente
        /// </summary>
        /// <returns></returns>
        public List<PurchaseOrders> GetPurchOrderRow(long Id)
        {
            var queryPurchas = (from Pur in context.TblPurchaseOrderRow
                                join Row in context.TblProducts on Pur.ProductId equals Row.Id
                                join Alm in context.TblWarehouse on Pur.WarehouseId equals Alm.Id
                                join mea in context.TblMeasure on Pur.MeasureId equals mea.Id
                                where Pur.PurchaseOrderId == Id
                                select new PurchaseOrders
                                {
                                    ProductId = Pur.ProductId,
                                    PurchaseOrderId = Pur.PurchaseOrderId,
                                    MeasureId = Pur.MeasureId,
                                    Almacen_Description = Alm.Name,
                                    WarehouseId = Alm.Id,
                                    ITBIS = Pur.TaxAbbreviation,
                                    Id = Pur.Id,
                                    Quantity = Pur.Quantity,
                                    Cost = Pur.Cost,
                                    Discount = Pur.Discount,
                                    Tax = Pur.Tax,
                                    AmountTax = Pur.AmountTax,
                                    Total = Pur.Total,
                                    Produc_Description = mea.Name + " " + Row.Name
                                }).ToList();

            return queryPurchas;
        }

        /// Metodo que trae el listado de compras
        /// </summary>
        /// <returns></returns>
        public IQueryable<PurchaseOrders> PurchaseId(long bussineId, long Id)
        {
            var queryPurchas = (from Pur in context.TblPurchaseOrder
                                join Row in context.TblPurchaseOrderRow on Pur.Id equals Row.PurchaseOrderId
                                join Sup in context.TblSuppliers on Pur.SuppliersId equals Sup.Id
                                where Pur.EmployeeBusinessId == bussineId && Pur.Id == Id
                                select new PurchaseOrders
                                {
                                    Suplidor = Sup.Names,
                                    CreateDate = Pur.CreateDate,
                                    Id = Pur.Id,
                                    SuppliersId = Pur.SuppliersId,
                                    Ncf = Pur.Ncf,
                                    DocumentNumber = Pur.DocumentNumber,
                                    Total = Pur.Total,
                                    TotalTax = Pur.TotalTax,
                                    DocumentEstatusId = Pur.DocumentEstatusId
                                });

            return queryPurchas;
        }

        /// <summary>
        /// Metodo para validar un Ncf
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion del Ncf</param>
        /// <returns></returns>
        public IQueryable<TblPurchaseOrder> ValidarNcf(string ncf)
        {
            return context.TblPurchaseOrder.Where(x => x.Ncf == ncf);
        }


        /// <summary>
        /// Metodo para obtener una orden de compras completa
        /// </summary>
        /// <param name="Id">Primary key de la orden de compras</param>
        /// <param name="branchOfficeId">Sucursal donde se realizo la orden de compras</param>
        /// <returns></returns>
        public TblPurchaseOrder GetPurchaseOrder(long Id, long branchOfficeId)
        {
            return context.TblPurchaseOrder.Where(x => x.Id == Id && x.BranchOfficeId == branchOfficeId).Include(x => x.TblDebitNote)
                .Include(x => x.TblPurchaseOrderRow).FirstOrDefault();
        }

        /// <summary>
        /// Metodo para obtener el detalle de la orden de compras
        /// </summary>
        /// <param name="PurchaseOrderId">Primary key de la orden de compras</param>
        /// <returns></returns>
        public IQueryable<TblPurchaseOrderRow> GetPurchaseOrderRow(long PurchaseOrderId)
        {
            return context.TblPurchaseOrderRow.Where(x => x.PurchaseOrderId == PurchaseOrderId)
                .Include(x => x.Measure).Include(x => x.Warehouse).Include(x => x.Product);
        }

        /// <summary>
        /// Metodo para validar si existe un comprobante registrado en alguna compra que no sea la actual
        /// </summary>
        /// <param name="ncf">Numero de Comprobante fiscal</param>
        /// <param name="BussinedId">Primary ke y de la sucursal</param>
        /// <param name="PushOrderId">Primary key de la compra (opcional) </param>
        /// <returns></returns>
        public IQueryable<TblPurchaseOrder> ValidarNcfUpdate(string ncf, long? BranchOfficeId, long? PurchaseOrderId = 0)
        {
            return context.TblPurchaseOrder.Where(x => x.Ncf == ncf && x.BranchOfficeId == BranchOfficeId && x.Id != PurchaseOrderId);
        }

        public bool MovimientoPurchase(long PurchaseId)
        {
            bool status = false;
            Dictionary<string, object> parametres = new Dictionary<string, object>();
            parametres.Add("@PurchaseId", PurchaseId);
            parametres.Add("@SystemProcess ", "Quicksal App");
            parametres.Add("@Status", status);
            return ExecuteProcedure("Purchase.prc_MovimientoPurchase", parametres);
        }

        public void DeleteRange(IEnumerable<TblPurchaseOrderRow> deleteRows)
        {
            this.context.Set<TblPurchaseOrderRow>().RemoveRange(deleteRows);
        }

        /// Metodo para relacionar las ordenes de compra con las notas de debito
        /// </summary>
        /// <returns></returns>
       /* public TaskResult DebitNoteUsed(TblDebitNoteUsed tblDebitNoteUsed)
        {
            TaskResult task = new TaskResult();

            try
            {
                context.TblDebitNoteUsed.Add(tblDebitNoteUsed);
                task.ExecutedSuccesfully = true;

            }
            catch (System.Exception)
            {

                throw;
            }

            return task;

        }*/

        /// Metodo para tarer el listado de nota de debito relacionada a una orden
        /// </summary>
        /// <returns></returns>
        public IQueryable<TblDebitNote> ListDebitNoteUsed(long purchaseOrderId)
        {
            return context.TblDebitNote.Where(x => x.PurchaseOrderId == purchaseOrderId && x.Used == false);
        }



        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }
    }
    public interface IPurchaseOrderRepositoy : IQuicksalBase<TblPurchaseOrder>
    {
        // Metodo que trae el listado de detalle correspondiente
        /// </summary>
        /// <returns></returns>
        List<PurchaseOrders> GetPurchOrderRow(long Id);

        // Metodo que valida  el listado ncf
        /// </summary>
        /// <returns></returns>

        IQueryable<TblPurchaseOrder> ValidarNcf(string ncf);
        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        IQueryable<PurchaseOrders> Purchase(long branchOfficeId);

        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        IQueryable<PurchaseOrders> PurchaseId(long bussineId, long Id);

        IQueryable<TblPurchaseOrder> ValidarNcfUpdate(string ncf, long? BranchOfficeId, long? PurchaseOrderId = 0);

        bool MovimientoPurchase(long PurchaseId);

        Task<TaskResult> Savechanges();
        void DeleteRange(IEnumerable<TblPurchaseOrderRow> deleteRows);


        /// Metodo para relacionar las ordenes de compra con las notas de debito
        /// </summary>
        /// <returns></returns>
        //TaskResult DebitNoteUsed(TblDebitNoteUsed tblDebitNoteUsed);

        /// Metodo para tarer el listado de nota de debito relacionada a una orden
        /// </summary>
        /// <returns></returns>
        IQueryable<TblDebitNote> ListDebitNoteUsed(long purchaseOrderId);



        /// <summary>
        /// Metodo para obtener una orden de compras completa
        /// </summary>
        /// <param name="Id">Primary key de la orden de compras</param>
        /// <param name="branchOfficeId">Sucursal donde se realizo la orden de compras</param>
        /// <returns></returns>
        TblPurchaseOrder GetPurchaseOrder(long Id, long branchOfficeId);


        /// <summary>
        /// Metodo para obtener el detalle de la orden de compras
        /// </summary>
        /// <param name="PurchaseOrderId">Primary key de la orden de compras</param>
        /// <returns></returns>
        IQueryable<TblPurchaseOrderRow> GetPurchaseOrderRow(long PurchaseOrderId);

    }
}
