﻿using Microsoft.EntityFrameworkCore;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class CustomerRepository : QuicksalRepository<TblCustomers>, ICustomerRepository
    {

        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public CustomerRepository(QuickSalContext context) : base(context)
        { }

        /// <summary>
        /// Metodo para obtener los clientes registrados para una empresa
        /// </summary>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        public IQueryable<TblCustomers> GetCustomers(long businessId)
        {
            return context.TblCustomers.Where(x => x.BusinessId == businessId).Include(x => x.DocumentType);
        }

        public bool Validationdocument(string document, long bussineId)
        {
            bool isValid = false;
            IQueryable<TblCustomers> documentNumber = context.TblCustomers.Where(x => x.DocumentNumber == document && x.BusinessId == bussineId);
            if (documentNumber.Count() > 0)
            {
                isValid = true;
            }

            return isValid;
        }

        public bool ValidationEditdocument(string document, long bussineId)
        {
            bool isValid = false;
            IQueryable<TblCustomers> documentNumber = context.TblCustomers.Where(x => x.DocumentNumber == document && x.BusinessId == bussineId);
            if (documentNumber.Count() >= 1)
            {
                isValid = true;
            }

            return isValid;
        }
        public bool ValidationCodeCustomer(string code, long bussineId)
        {
            bool isValid = false;
            IQueryable<TblCustomers> codeNumber = context.TblCustomers.Where(x => x.Code == code && x.BusinessId == bussineId);
            if (codeNumber.Count() > 0)
            {
                isValid = true;
            }

            return isValid;
        }
        /// <summary>
        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }

    }

    public interface ICustomerRepository : IQuicksalBase<TblCustomers>
    {
        bool ValidationCodeCustomer(string code, long bussineId);
       

        bool Validationdocument(string document, long bussineId);

        bool ValidationEditdocument(string document, long bussineId);
        /// <summary>
        /// Metodo para obtener los clientes registrados para una empresa
        /// </summary>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        IQueryable<TblCustomers> GetCustomers(long businessId);

        /// <summary>
        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }

}
