﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class NcfRepository : QuicksalRepository<TblNcf>, INcfRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public NcfRepository(QuickSalContext context) : base(context)
        {

        }

        public IQueryable<Ncfs> NCF(long businessID)
        {
            var queryNCF = (from NF in context.TblNcf
                            join NFP in context.TblNcfprefix on NF.PrefixId equals NFP.Id
                            where NF.BusinessId == businessID
                            select new Ncfs
                            {
                                BusinessId = NF.BusinessId,
                                Id = NF.Id,
                                Prefi = NFP.Description,
                                CounterFrom = NF.CounterFrom,
                                CounterTo = NF.CounterTo,
                                Estatus = NF.Estatus
                            });

            return queryNCF;
        }

        /// <summary>
        /// Metodo para obtener todas los tipos de comprobantes
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        public IQueryable<TblNcfprefix> TypeNcf(long businessId)
        {
            return context.TblNcfprefix.OrderByDescending(x => x.Description);
        }

        /// <summary>
        /// Metodo para validar un Ncf
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion del Ncf</param>
        /// <returns></returns>
        public bool ValidarForm(TblNcf Ncf)
        {
            var query = from n in context.TblNcf
                        join r in context.TblNcfrow on n.Id equals r.Ncfid
                        where n.BusinessId == Ncf.BusinessId && n.PrefixId == Ncf.PrefixId &&
                               ((n.CounterFrom >= Ncf.CounterFrom && n.CounterTo <= Ncf.CounterTo) || (n.CounterTo >= Ncf.CounterFrom && n.CounterFrom <= Ncf.CounterTo)) &&
                               r.Used == false
                        select n;

            return query.Any();
        }

        /// <summary>
        /// Metodo para validar un Ncf
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion del Ncf</param>
        /// <returns></returns>
        public bool ValidarFormEdit(Ncfs Ncf)
        {
            var query = from n in context.TblNcf
                        join r in context.TblNcfrow on n.Id equals r.Ncfid
                        where n.BusinessId == Ncf.BusinessId && n.PrefixId == Ncf.PrefixId && r.Used==true &&
                               (n.CounterFrom >= Ncf.CounterFrom && n.CounterTo <= Ncf.CounterTo) 
                        select n;

            return query.Any();
        }

        /////Metodo para traer el datos del nfc
        ///// </summary>
        ///// <returns></returns>
        public IQueryable<Ncfs> NCFs(int businessID, int Id)
        {
            var queryNCF = (from NF in context.TblNcf
                            join NFP in context.TblNcfprefix on NF.PrefixId equals NFP.Id
                            where NF.BusinessId == businessID && NF.Id == Id
                            select new Ncfs
                            {
                                BusinessId = NF.BusinessId,
                                Id = NFP.Id,
                                Prefi = NFP.Description,
                                CounterFrom = NF.CounterFrom,
                                CounterTo = NF.CounterTo

                            });

            return queryNCF;
        }

        /////Metodo para traer el datos del nfccompleto
        ///// </summary> 
        ///// <returns></returns>
        public IQueryable<Ncfs> ListVoucher(long businessID, int Id)
        {
            var queryNCF = (from NF in context.TblNcf
                            join NFP in context.TblNcfprefix on NF.PrefixId equals NFP.Id
                            join Row in context.TblNcfrow on NF.Id equals Row.Ncfid
                            where NF.BusinessId == businessID && NF.Id == Id
                            select new Ncfs
                            {
                                RowId = Row.Id,
                                Prefi = NFP.Prefix,
                                Description = NFP.Description,
                                NCF = NFP.Prefix + Row.Ncf,
                                Estatus = Row.Used
                            });

            return queryNCF;
        }

        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }
    }
    public interface INcfRepository : IQuicksalBase<TblNcf>
    {

        /// <summary>
        /// Metodo para validar un Ncf
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion del Ncf</param>
        /// <returns></returns>
        bool ValidarFormEdit(Ncfs Ncf);
        /// <summary>
        /// Metodo para validar un Ncf
        /// </summary>
        /// <param name="entity">Modelo que contiene la informacion del Ncf</param>
        /// <returns></returns>
        bool ValidarForm(TblNcf Ncf);
        /////Metodo para traer el datos del nfccompleto
        ///// </summary> 
        ///// <returns></returns>
        IQueryable<Ncfs> ListVoucher(long businessID, int Id);
        /// <summary>
        /// Metodo para obtener todas los tipos de comprobantes
        /// </summary>
        /// <returns></returns>
        IQueryable<TblNcfprefix> TypeNcf(long businessId);
        /// <summary>
        /// Metodo para traer el datos del nfc
        /// </summary>
        /// <returns></returns>
        IQueryable<Ncfs> NCFs(int businessID, int Id);
        /// <summary>
        /// Metodo para traer los datos de los ncf
        /// </summary>
        /// <returns></returns>
        IQueryable<Ncfs> NCF(long businessID);
        /// <summary>
        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }

}
