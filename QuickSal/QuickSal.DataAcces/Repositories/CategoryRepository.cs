﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class CategoryRepository : QuicksalRepository<TblCategory>, ICategoryRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public CategoryRepository(QuickSalContext context) : base(context)
        { }

        /// <summary>
        /// Metodo para obtener todas las categorias del sistema
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        public IQueryable<TblCategory> GetCategories(long BusinessId)
        {
            return context.TblCategory.Where(x => x.BusinessId == BusinessId || !x.BusinessId.HasValue).OrderByDescending(x => x.CreateDate);
        }

        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }
    }

    public interface ICategoryRepository : IQuicksalBase<TblCategory>
    {
        /// <summary>
        /// Metodo para obtener todas las categorias del sistema
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        IQueryable<TblCategory> GetCategories(long BusinessId);

        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
