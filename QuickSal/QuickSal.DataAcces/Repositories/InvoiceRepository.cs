﻿using Microsoft.EntityFrameworkCore;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.CustomModels.Procedure.Customer;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Procedure;
using QuickSal.DataAcces.Procedure.Customer;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class InvoiceRepository : QuicksalRepository<TblInvoice>, IInvoiceRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public InvoiceRepository(QuickSalContext context) : base(context)
        {

        }

        /// <summary>
        /// Metodo para obtener todas las facturas de una sucursal
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        public IQueryable<PrcGetAllInvoce> GetAllInvoices(long BranchOfficeId)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("@BranchOfficeId", BranchOfficeId);
            return ExecuteQuerys(new PrcGetAllInvoce(), "Customer.prc_GetAllInvoce", parameters);
        }

        /// <summary>
        /// Metodo para buscar productos mediante un criterio de busqueda
        /// </summary>
        /// <param name="branchOfficeId">Primary key de la sucursal</param>
        /// <param name="WarehouseId">Primary key del almacén</param>
        /// <param name="text">Texto de busqueda</param>
        /// <returns></returns>
        public IQueryable<PrcSearchProducts> SeacrhProducts(long branchOfficeId, int WarehouseId, string text)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("@BranchOfficeId", branchOfficeId);
            parameters.Add("@WarehouseId", WarehouseId);
            parameters.Add("@ProducName", text);
            return ExecuteQuerys(new PrcSearchProducts(), "Customer.prc_SearchProducts", parameters);
        }

        /// <summary>
        /// Metodo para obtener el producto para una factura
        /// </summary>
        /// <param name="ProductId">Primary key del producto</param>
        /// <returns></returns>
        public GetProductSalesPoint GetProductSalePoint(long ProductId)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("@ProducId", ProductId);

            return ExecuteQuerys(new GetProductSalesPoint(), "Customer.GetProductSalesPoint", parameters).FirstOrDefault();
        }
      

        /// <summary>
        /// Metodo para obtener una factura
        /// </summary>
        /// <param name="InvoiceId">Primary key de la factura</param>
        /// <param name="BanchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        public TblInvoice GetInvoiceHeader(long InvoiceId, long BanchOfficeId)
        {
            return context.TblInvoice.Where(x => x.Id == InvoiceId && x.BranchOfficeId == BanchOfficeId).Include(x => x.Customer).Include(x => x.TblCretitNoteUsed)
                .Include(x => x.TblInvoiceRow).FirstOrDefault();
        }


        /// <summary>
        /// Metodo para obtener factuas que no tengan notas de credito
        /// </summary>
        /// <param name="BanchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        public IQueryable<prc_InvoiceWithOutCreditNote> GetInvoiceWithOutCreditNote(long BanchOfficeId)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("@BranchOfficeId", BanchOfficeId);
            return ExecuteQuerys(new prc_InvoiceWithOutCreditNote(), "Customer.prc_InvoiceWithOutCreditNote", parameters);
        }


        /// <summary>
        /// Metodo para obtener todos los productos disponibles para la venta
        /// </summary>
        /// <param name="WarehouseId">Primary key del almacen</param>
        /// <returns></returns>
        public IQueryable<GetProductsOnSale> GetProductsOnSale(int WarehouseId)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("@WareHouseId", WarehouseId);
            return ExecuteQuerys(new GetProductsOnSale(), "Customer.GetProductsOnSale", parameters);
        }

        /// <summary>
        /// Metodo para obtener el ultimo número de documento generado
        /// </summary>
        /// <returns></returns>
        public string GetLastCode()
        {
            var data = context.TblInvoice.LastOrDefault();
            if (data == null)
                return null;
            else
                return data.DocumentNumber;

        }


        /// <summary>
        /// Metodo para procesar los datos de una factura
        /// </summary>
        /// <param name="branchOfficeId">Primary key de la sucursal</param>
        /// <param name="InvoiceId">Primary key de la cabecera de la factura</param>
        /// <returns></returns>
        public bool ProcessInvoice(long branchOfficeId, long InvoiceId)
        {
            Dictionary<string, object> parametres = new Dictionary<string, object>();
            bool status = false;
            parametres.Add("@BranchOfficeId", branchOfficeId);
            parametres.Add("@invoiceId", InvoiceId);
            parametres.Add("@SystemProcess", "QuickSal APP");
            parametres.Add("@Status", status);
            return ExecuteProcedure("Customer.prc_ProcessInvoice", parametres);
        }



        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }
    }

    public interface IInvoiceRepository : IQuicksalBase<TblInvoice>
    {
        /// <summary>
        /// Metodo para obtener todas las facturas de una sucursal
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        IQueryable<PrcGetAllInvoce> GetAllInvoices(long BranchOfficeId);

        /// <summary>
        /// Metodo para obtener el producto para una factura
        /// </summary>
        /// <param name="ProductId">Primary key del producto</param>
        /// <returns></returns>
        GetProductSalesPoint GetProductSalePoint(long ProductId);

        /// <summary>
        /// Metodo para buscar productos mediante un criterio de busqueda
        /// </summary>
        /// <param name="branchOfficeId">Primary key de la sucursal</param>
        /// <param name="WarehouseId">Primary key del almacén</param>
        /// <param name="text">Texto de busqueda</param>
        /// <returns></returns>
        IQueryable<PrcSearchProducts> SeacrhProducts(long branchOfficeId, int WarehouseId, string text);

        /// <summary>
        /// Metodo para obtener una factura
        /// </summary>
        /// <param name="InvoiceId">Primary key de la factura</param>
        /// <param name="BanchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        TblInvoice GetInvoiceHeader(long InvoiceId, long BanchOfficeId);


        /// <summary>
        /// Metodo para procesar los datos de una factura
        /// </summary>
        /// <param name="branchOfficeId">Primary key de la sucursal</param>
        /// <param name="InvoiceId">Primary key de la cabecera de la factura</param>
        /// <returns></returns>
        bool ProcessInvoice(long branchOfficeId, long InvoiceId);


        /// <summary>
        /// Metodo para obtener factuas que no tengan notas de credito
        /// </summary>
        /// <param name="BanchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        IQueryable<prc_InvoiceWithOutCreditNote> GetInvoiceWithOutCreditNote(long BanchOfficeId);

        /// <summary>
        /// Metodo para obtener todos los productos disponibles para la venta
        /// </summary>
        /// <param name="WarehouseId">Primary key del almacen</param>
        /// <returns></returns>
        IQueryable<GetProductsOnSale> GetProductsOnSale(int WarehouseId);

        /// <summary>
        /// Metodo para obtener el ultimo número de documento generado
        /// </summary>
        /// <returns></returns>
        string GetLastCode();
    }
}
