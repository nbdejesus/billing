﻿using Microsoft.EntityFrameworkCore;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class WarehouseRepository : QuicksalRepository<TblWarehouse>, IWarehouseRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public WarehouseRepository(QuickSalContext context) : base(context)
        { }

        /// <summary>
        /// Metodo para obtener los almacenes de una sucursal
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <param name="active">Estatus del almacen (opcional)</param>
        /// <returns></returns>
        public async Task<IQueryable<TblWarehouse>> GetWarehouses(long BranchOfficeId, bool? active = null)
        {
            if (active.HasValue)
                return context.TblWarehouse.Where(x => x.BranchOfficeId == BranchOfficeId && x.Estatus == active.Value);
            else
                return context.TblWarehouse.Where(x => x.BranchOfficeId == BranchOfficeId);

        }

        /// <summary>
        /// Metodo para obtener los almacenes de una empresa
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        public IQueryable<TblWarehouse> GetBusinessWarehouses(long BusinessId)
        {
            return context.TblWarehouse.Include(x => x.BranchOffice).Where(x => x.BranchOffice.IdBusiness == BusinessId);
        }

        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }
    }

    public interface IWarehouseRepository : IQuicksalBase<TblWarehouse>
    {
        /// <summary>
        /// Metodo para obtener los almacenes de una sucursal
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <param name="active">Estatus del almacen (opcional)</param>
        /// <returns></returns>
        Task<IQueryable<TblWarehouse>> GetWarehouses(long BranchOfficeId, bool? active = null);


        /// <summary>
        /// Metodo para obtener los almacenes de una empresa
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        IQueryable<TblWarehouse> GetBusinessWarehouses(long BusinessId);


        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
