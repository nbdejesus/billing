﻿using Microsoft.EntityFrameworkCore.Storage.Internal;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Views;
using System.Collections.Generic;
//using System.Data.Entity;
using System.Linq;


namespace QuickSal.DataAcces.Repositories
{
    public class SecurityRepository : QuicksalRepository<TblSegurity>, ISecurityRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public SecurityRepository(QuickSalContext context) : base(context)
        { }

        /// <summary>
        /// Metodo para obtener un sub objeto de quicksal
        /// </summary>
        /// <param name="method">Metodo del controller</param>
        /// <param name="controller">Controller</param>
        /// <returns></returns>
        public TblSubObject GetSubObject(string method, string controller)
        {
            return context.TblSubObject.Where(x => x.Method == method && x.Controller == controller).FirstOrDefault();
        }

        /// <summary>
        /// Metodo para obtener un sub objeto de quicksal
        /// </summary>
        /// <param name="id">Primary key del subObjeto</param>
        /// <returns></returns>
        public TblSubObject GetSubObject(int id)
        {
            return context.TblSubObject.Where(x => x.Id==id && !x.Parent.HasValue).FirstOrDefault();
        }

        /// <summary>
        /// Metodo para obtener la seguridad asignada al empleado
        /// </summary>
        /// <param name="privilegeId">Primary key del privilegio</param>
        /// <param name="method">Nombre del metodo</param>
        /// <param name="controller">Nombre del controller</param>
        /// <returns></returns>
        public vwEmployeeSecurity GetSecurity(int privilegeId, string method, string controller)
        {
            return context.Set<vwEmployeeSecurity>().Where(x => x.PrivilegesId == privilegeId &&
                                                            x.Controller == controller &&
                                                            x.Method == method &&
                                                            x.Status).FirstOrDefault();
        }

        /// <summary>
        /// Metodo para obtener los hijos de los objetos padres
        /// </summary>
        /// <param name="ObjectsId">Primary key de los objetos</param>
        /// <param name="TypeMethodId">Primary key del tipo de metodos</param>
        /// <returns></returns>
        public IQueryable<vwObjectChildren> GetObjectChildrens(int ObjectsId, List<byte?> TypeMethodId)
        {
            return context.Set<vwObjectChildren>().Where(x => x.SubObjectParent == ObjectsId && TypeMethodId.Contains(x.TypeMethodId));
        }

        /// <summary>
        /// Metodo para obtener los hijos de los objetos padres
        /// </summary>
        /// <param name="ObjectsId">Listado de primary key de los objetos padres</param>
        /// <returns></returns>
        public IQueryable<vwObjectChildren> GetObjectChildrens(List<int> ObjectsId)
        {
            return context.Set<vwObjectChildren>().Where(x => ObjectsId.Contains(x.SubObjectParent));
        }

        /// <summary>
        /// Metodo para obtener las agrupaciones de objetos
        /// </summary>
        /// <returns></returns>
        public IQueryable<TblObject> GetObject()
        {
            return context.TblObject;
        }
    }

    public interface ISecurityRepository : IQuicksalBase<TblSegurity>
    {
        /// <summary>
        /// Metodo para obtener un sub objeto de quicksal
        /// </summary>
        /// <param name="method">Metodo del controller</param>
        /// <param name="controller">Controller</param>
        /// <returns></returns>
        TblSubObject GetSubObject(string method, string controller);

        /// <summary>
        /// Metodo para obtener los hijos de los objetos padres
        /// </summary>
        /// <param name="ObjectsId">Listado de primary key de los objetos padres</param>
        /// <returns></returns>
        IQueryable<vwObjectChildren> GetObjectChildrens(List<int> ObjectsId);

        /// <summary>
        /// Metodo para obtener un sub objeto de quicksal
        /// </summary>
        /// <param name="id">Primary key del subObjeto</param>
        /// <returns></returns>
        TblSubObject GetSubObject(int id);

        /// <summary>
        /// Metodo para obtener los hijos de los objetos padres
        /// </summary>
        /// <param name="ObjectsId">Primary key de los objetos</param>
        /// <param name="TypeMethodId">Primary key del tipo de metodos</param>
        /// <returns></returns>
        IQueryable<vwObjectChildren> GetObjectChildrens(int ObjectsId, List<byte?> TypeMethodId);


        /// <summary>
        /// Metodo para obtener la seguridad asignada al empleado
        /// </summary>
        /// <param name="privilegeId">Primary key del privilegio</param>
        /// <param name="method">Nombre del metodo</param>
        /// <param name="controller">Nombre del controller</param>
        /// <returns></returns>
        vwEmployeeSecurity GetSecurity(int privilegeId, string method, string controller);


        /// <summary>
        /// Metodo para obtener las agrupaciones de objetos
        /// </summary>
        /// <returns></returns>
        IQueryable<TblObject> GetObject();

    }
}
