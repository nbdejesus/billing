﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Procedure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class MovingWareRepository : QuicksalRepository<TblMovingWarehouse>, IMovingWareRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }
        public MovingWareRepository(QuickSalContext context) : base(context)
        { }


        /// <summary>
        /// Metodo para obtener los movimientos de almacen de una sucursal
        /// </summary>
        /// <param name="WarehouseId">Primary key de la sucursal</param>
        /// <returns></returns>
        public IQueryable<PrcGetAllMovingWare> GetMovingWarehouse(long WarehouseId)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("@BranchOfficeId", WarehouseId);

            return ExecuteQuery(new PrcGetAllMovingWare(), "WarehouseMoving", parameters);
        }


        /// <summary>
        /// Metodo para obtener los movimientos de almacen de una sucursal
        /// </summary>
        /// <param name="MovingId">Primary key del movimiento del almacen</param>
        /// <returns></returns>
        public TblMovingWarehouse GetMovingDetails(long MovingId)
        {


            var datos = context.TblMovingWarehouse.Where(x => x.Id == MovingId).FirstOrDefault();

            return datos;
        }


        /// <summary>
        /// Metodo que resta la cantidad disponible del inventario
        /// </summary>
        /// <param name="WareHouseId">Parametro que contiene el Id del amacen</param>
        /// <param name="ProductId">Parametro que contiene el Id del producto</param>
        /// <param name="cantidad">Parametro que contiene la cantidad enviada</param>
        /// <param name="Agregar">Parametro que define si se le agregara al inventario o se le restara</param>
        /// <returns></returns>
        public TaskResult UpdateInventoryWareHouse(int WareHouseId, long ProductId, decimal cantidad, bool Agregar)
        {

            TblInventoryWarehouse TblInventoryWarehouse = new TblInventoryWarehouse();

            var dato = context.TblInventoryWarehouse.Where(x => x.WarehouseId == WareHouseId && x.ProductsId == ProductId).FirstOrDefault();
            try
            {
                if (dato == null)
                {
                    TblInventoryWarehouse.WarehouseId = WareHouseId;
                    TblInventoryWarehouse.ProductsId = ProductId;
                    TblInventoryWarehouse.ProductAmount = cantidad;
                    context.TblInventoryWarehouse.Add(TblInventoryWarehouse);

                }

                else
                {
                    TblInventoryWarehouse = dato;
                    decimal disponible = 0;
                    decimal resultado = 0;


                    if (Agregar)
                    {
                        disponible = TblInventoryWarehouse.ProductAmount;
                        resultado = disponible + cantidad;
                    }

                    else
                    {
                        disponible = TblInventoryWarehouse.ProductAmount;
                        resultado = disponible - cantidad;
                    }


                    TblInventoryWarehouse.ProductAmount = resultado;
                    context.TblInventoryWarehouse.Update(TblInventoryWarehouse);

                }
                return TaskResult;
            }


            catch (Exception)
            {

                return TaskResult;


            }


        }

        /// <summary>
        /// Metodo para obtener todos los almacenes de una empresa
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        public IQueryable<TblWarehouse> GetAllWarehouses(long BranchOfficeId)
        {
            var BusinessId = context.TblBranchOffice.Where(x => x.Id == BranchOfficeId).Select(x => x.IdBusiness).FirstOrDefault();

            var allWareHouse = (from w in context.TblWarehouse
                                join o in context.TblBranchOffice on w.BranchOfficeId equals o.Id
                                join b in context.TblBusiness on o.IdBusiness equals b.Id
                                where b.Id == BusinessId
                                select new TblWarehouse()
                                {
                                    Id = w.Id,
                                    Name = w.Name + " " + "( " + o.Name + " )",

                                });
            return allWareHouse;
        }


        /// <summary>
        /// Metodo para obtener el ultimo movimiento de almacen
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        public string GetLastMoving(long BusinessId)
        {

            var code = (from M in context.TblMovingWarehouse
                        join W in context.TblWarehouse on M.SenderWarehouseId equals W.Id
                        join B in context.TblBranchOffice on W.BranchOfficeId equals B.Id
                        join Bu in context.TblBusiness on B.IdBusiness equals Bu.Id
                        where Bu.Id == BusinessId
                        select
                            M.DocumentNo
            ).LastOrDefault();

            return code;
        }

        /// <summary>
        /// Metodo para modificar el inventario
        /// </summary>
        /// <param name="movingWareHouse">Modelo que contiene el movimiento de almacen</param>
        /// <returns></returns>
        public TaskResult ModifyInventory(TblMovingWarehouse movingWareHouse)
        {

            TblInventoryWarehouse tblInventoryWarehouse = new TblInventoryWarehouse();
            foreach (var item in movingWareHouse.TblMovingWarehouseRow)
            {

                var realAmount = context.TblInventoryWarehouse.Where(x => x.ProductsId == item.ProductId && x.WarehouseId == movingWareHouse.SenderWarehouseId).Select(x => x.Id);


            }

            return null;
        }

        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }


    }

    public interface IMovingWareRepository : IQuicksalBase<TblMovingWarehouse>
    {

        /// <summary>
        /// Metodo para obtener los movimientos de almacen de una sucursal
        /// </summary>
        /// <param name="WarehouseId">Primary key de la sucursal</param>
        /// <returns></returns>
        IQueryable<PrcGetAllMovingWare> GetMovingWarehouse(long WarehouseId);

        /// <summary>
        /// Metodo para obtener los movimientos de almacen de una sucursal
        /// </summary>
        /// <param name="MovingId">Primary key de la sucursal</param>
        /// <returns></returns>
        TblMovingWarehouse GetMovingDetails(long MovingId);

        /// <summary>
        /// Metodo que resta la cantidad disponible del inventario
        /// </summary>
        /// <param name="WareHouseId">Parametro que contiene el Id del amacen</param>
        /// <param name="ProductId">Parametro que contiene el Id del producto</param>
        /// <param name="cantidad">Parametro que contiene la cantidad enviada</param>
        /// <returns></returns>
        TaskResult UpdateInventoryWareHouse(int WareHouseId, long ProductId, decimal cantidad, bool Cancelar);


        /// <summary>
        /// Metodo para obtener todos los almacenes de una empresa
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        IQueryable<TblWarehouse> GetAllWarehouses(long BranchOfficeId);


        /// <summary>
        /// Metodo para obtener el ultimo movimiento de almacen
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        string GetLastMoving(long BusinessId);


        Task<TaskResult> Savechanges();
    }
}
