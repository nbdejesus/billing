﻿using Microsoft.EntityFrameworkCore;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Procedure;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class MovingWareRowRepository : QuicksalRepository<TblMovingWarehouseRow>, IMovingWareRowRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }
        public MovingWareRowRepository(QuickSalContext context) : base(context)
        { }


        /// <summary>
        /// Metodo el detalle del movimiento
        /// </summary>
        /// <param name="MovingId">Parametro que contiene el Id del movimiento</param>    
        /// <returns></returns>
        public IQueryable<TblMovingWarehouseRow> DatetailsRows(long MovingId)
        {

            return context.TblMovingWarehouseRow.Where(x => x.MovingId == MovingId).Include(x => x.Moving); 

        }


        /// <summary>
        /// Metodo para llamar el producto del movimineto
        /// </summary>
        /// <param name="MovingId">Parametro que contiene el Id del movimiento</param>    
        /// <param name="ProductId">Parametro que contiene el Id del producto</param>    
        /// <returns></returns>
        public PrGetMovingWareDetails ProductRow(long MovingId, long ProductId)
        {
            
         var warehouse = context.TblMovingWarehouse.Where(x => x.Id == MovingId).FirstOrDefault();

            var datos = context.TblMovingWarehouseRow.Where(x => x.MovingId == MovingId && x.ProductId == ProductId).Select(x => new PrGetMovingWareDetails
            {
                Id          = x.Id,
                Amount      = x.Amount,
                ProductId   = x.ProductId,
                State       = x.State,
                Product     = x.ProductName,
                Description = x.Description

            }).FirstOrDefault();

            datos.Disponible        = GetProducts(warehouse.SenderWarehouseId, ProductId).FirstOrDefault().Amount;
            datos.StateHouse        = warehouse.State;
            datos.SenderWarehouseId = warehouse.SenderWarehouseId;

            return datos;

        }

        /// <summary>
        /// Metodo que trae todos los producto del inventario del almacen
        /// </summary>
        /// <param name="WarehouseId">Parametro que contiene el Id del almacen</param>
        /// <param name="ProductId">Parametro que contiene el Id del producto</param>
        /// <returns></returns>
        public IQueryable<ProductInventory> GetProducts(long WarehouseId, long ProductId)
        {
            if (ProductId != 0)
            {
                var producto = (from i in context.TblInventoryWarehouse
                                join p in context.TblProducts on i.ProductsId equals p.Id
                                where p.Id == ProductId && i.WarehouseId == WarehouseId
                                select new ProductInventory()
                                {

                                    ProductId = p.Id,
                                    Product = p.Name,
                                    Amount = i.ProductAmount,
                                    Descripcion = p.Description
                                });

                return producto;

            }

            else
            {


                var producto = (from i in context.TblInventoryWarehouse
                                join p in context.TblProducts on i.ProductsId equals p.Id
                                where i.WarehouseId == WarehouseId
                                select new ProductInventory()
                                {

                                    ProductId = p.Id,
                                    Product = p.Name,
                                    Amount = i.ProductAmount,
                                    Descripcion = p.Description
                                });
                return producto;
            }


        }


        /// <summary>
        /// Metodo que elimina los productos de los moviminetos
        /// </summary>
        /// <param name="MovingIdRow">Parametro que contiene el Id del detalle del movimiento</param>
        /// <returns></returns>
        public TaskResult DeleteMovingIdRow(long MovingIdRow)
        {
            var movingWareRow = new TblMovingWarehouseRow();
            movingWareRow = context.TblMovingWarehouseRow.Where(x => x.Id == MovingIdRow).FirstOrDefault();

            context.TblMovingWarehouseRow.Remove(movingWareRow);
            var resp = context.SaveChanges();
            var response="";

            if (resp > 1)
                response = "true";
            else
                response = "false";

            return TaskResult;
        }


        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }


    }

    public interface IMovingWareRowRepository : IQuicksalBase<TblMovingWarehouseRow>
    {



        /// <summary>
        /// Metodo el detalle del movimiento
        /// </summary>
        /// <param name="MovingId">Parametro que contiene el Id del movimiento</param>    
        /// <returns></returns>
        IQueryable<TblMovingWarehouseRow> DatetailsRows(long MovingId);


        /// <summary>
        /// Metodo para llamar el producto del movimineto
        /// </summary>
        /// <param name="MovingId">Parametro que contiene el Id del movimiento</param>    
        /// <param name="ProductId">Parametro que contiene el Id del producto</param>    
        /// <returns></returns>
        PrGetMovingWareDetails ProductRow(long MovingId, long ProductId);

        /// <summary>
        /// Metodo que trae todos los producto del inventario del almacen
        /// </summary>
        /// <param name="WarehouseId">Parametro que contiene el Id del almacen</param>
        /// <param name="ProductId">Parametro que contiene el Id del producto</param>
        /// <returns></returns>
        IQueryable<ProductInventory> GetProducts(long WarehouseId, long ProductId);

       

        /// <summary>
        /// Metodo que elimina los productos de los moviminetos
        /// </summary>
        /// <param name="MovingIdRow">Parametro que contiene el Id del detalle del movimiento</param>
        /// <returns></returns>
        TaskResult DeleteMovingIdRow(long MovingIdRow);

        Task<TaskResult> Savechanges();
    }
}
