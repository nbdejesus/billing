﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Procedure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using static QuickSal.DataAcces.Repositories.DashboardRepository;

namespace QuickSal.DataAcces.Repositories
{
    public class DashboardRepository : QuicksalRepository<TblBusiness>, IDashboardRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public DashboardRepository(QuickSalContext context) : base(context)
        {

        }

        /// <summary>
        /// Metodo para obtener todas las facturas de una sucursal
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        public IQueryable<prc_Shopping> GetShopping(long businessId)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("@businessId", businessId);
            return ExecuteQuerys(new prc_Shopping(), "PS_Compras", parameters);
        }
        public IQueryable<prc_Sales> GetSSales(long businessId)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("@businessId", businessId);
            return ExecuteQuerys(new prc_Sales(), "SP_Sales", parameters);
        }
        public IQueryable<prc_Profits> GetProfits(long businessId, int month,int year )
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("@businessId", businessId);
            parameters.Add("@month", month);
            parameters.Add("@year", year);
            return ExecuteQuerys(new prc_Profits(), "Dashboard.sp_profit", parameters);
        }
        public IQueryable<prc_expenses> GetExpense(long businessId, int month, int year)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("@businessId", businessId);
            parameters.Add("@month", month);
            parameters.Add("@year", year);
            return ExecuteQuerys(new prc_expenses(), "Dashboard.sp_expenses", parameters);
        }
        public IQueryable<sp_Sales_for_Month> Getsp_Sales_for_Month(long businessId, int year)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("@businessId", businessId);
            parameters.Add("@year", year);
            return ExecuteQuerys(new sp_Sales_for_Month(), "Dashboard.sp_Sales_for_Month", parameters);
        }
        public IQueryable<sp_Sales_for_Month_products> Getsp_Sales_for_Month_products(long businessId, int year,int month)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("@businessId", businessId);
            parameters.Add("@year", year);
            parameters.Add("@month", month);
            return ExecuteQuerys(new sp_Sales_for_Month_products(), "Dashboard.sp_Sales_for_Month_products", parameters);
        }
        public IQueryable<prc_sp_Sales_for_Month_seller> Getsp_Sales_for_Month_seller(long businessId, int year, int month)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("@businessId", businessId);
            parameters.Add("@year", year);
            parameters.Add("@month", month);
            return ExecuteQuerys(new prc_sp_Sales_for_Month_seller(), "Dashboard.sp_Sales_for_Month_seller", parameters);
        }
        public IQueryable<prc_Sales_for_Month_branchOffice> GetSales_for_Month_branchOffice(long businessId, int year, int month)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("@businessId", businessId);
            parameters.Add("@year", year);
            parameters.Add("@month", month);
            return ExecuteQuerys(new prc_Sales_for_Month_branchOffice(), "Dashboard.sp_Sales_for_Month_branchOffice", parameters);
        }
        public IQueryable<pr_sp_Sales_for_Month_Customer> GetSales_for_Month_Customer(long businessId, int year, int month)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("@businessId", businessId);
            parameters.Add("@year", year);
            parameters.Add("@month", month);
            return ExecuteQuerys(new pr_sp_Sales_for_Month_Customer(), "Dashboard.sp_Sales_for_Month_Customer", parameters);
        }
    }
    public interface IDashboardRepository : IQuicksalBase<TblBusiness>
    {
        /// <summary>
        /// Metodo para obtener todas las facturas de una sucursal
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        IQueryable<prc_Shopping> GetShopping(long businessId);

        IQueryable<prc_Sales> GetSSales(long businessId);

        IQueryable<prc_Profits> GetProfits(long businessId, int month, int year);

        IQueryable<prc_expenses> GetExpense(long businessId, int month, int year);
        IQueryable<sp_Sales_for_Month> Getsp_Sales_for_Month(long businessId, int year);
        IQueryable<sp_Sales_for_Month_products> Getsp_Sales_for_Month_products(long businessId, int year, int month);
        IQueryable<prc_sp_Sales_for_Month_seller> Getsp_Sales_for_Month_seller(long businessId, int year, int month);
        IQueryable<prc_Sales_for_Month_branchOffice> GetSales_for_Month_branchOffice(long businessId, int year, int month);
        IQueryable<pr_sp_Sales_for_Month_Customer> GetSales_for_Month_Customer(long businessId, int year, int month);
    }
}
