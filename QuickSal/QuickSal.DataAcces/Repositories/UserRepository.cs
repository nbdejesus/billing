﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class UserRepository : QuicksalRepository<TblUsers>, IUserRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public UserRepository(QuickSalContext context) : base(context)
        { }

        /// <summary>
        /// Proceso para obtener el usuario 
        /// </summary>
        /// <param name="Email">Email registrado del usuario</param>
        /// <returns></returns>
        public TblUsers GetUser(string Email)
        {     
            return context.TblUsers.Where(x => x.Email == Email).FirstOrDefault();
        }

        /// <summary>
        /// Metodo para obtener el usuario
        /// </summary>
        /// <param name="Email">Email del usuario</param>
        /// <param name="password">Contrasena del usuario</param>
        /// <returns></returns>
        public TblUsers GetUser(string Email, string password)
        {          
            return context.TblUsers.Where(x => x.Email == Email && x.UserPassword == password).FirstOrDefault();
        }

        /// <summary>
        /// Proceso para obtener el usuario mediante su llave primaria
        /// </summary>
        /// <param name="Id">Id registrado del usuario</param>
        /// <returns></returns>
        public TblUsers GetUser(long Id)
        {
            return Get(Id);
        }

        
        /// <summary>
        /// Proceso para agregar un usuario al modelo
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        public TaskResult AddUser(TblUsers User)
        {
            return AddEntity(User);
        }

        public bool Validationdocument(string document, long bussineId)
        {
            bool isValid = false;
            IQueryable<TblUsers> documentNumber = context.TblUsers.Where(x => x.DocumentNumber == document);
            if (documentNumber.Count() > 0)
            {
                isValid = true;
            }

            return isValid;
        }
        public bool ValidationEditdocument(string document)
        {
            bool isValid = false;
            IQueryable<TblUsers> documentNumber = context.TblUsers.Where(x => x.DocumentNumber == document);
            if (documentNumber.Count() >= 1)
            {
                isValid = true;
            }

            return isValid;
        }

        /// <summary>
        /// Proceso para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }
    }

    public interface IUserRepository : IQuicksalBase<TblUsers>
    {
        /// <summary>
        /// Proceso para obtener el usuario mediante su email
        /// </summary>
        /// <param name="Email">Email registrado del usuario</param>
        /// <returns></returns>
        TblUsers GetUser(string Email);


        /// <summary>
        /// Proceso para obtener el usuario mediante su llave primaria
        /// </summary>
        /// <param name="Id">Id registrado del usuario</param>
        /// <returns></returns>
        TblUsers GetUser(long Id);


        /// <summary>
        /// Proceso para agregar un usuario al modelo
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        TaskResult AddUser(TblUsers User);

        /// <summary>
        /// Proceso para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();

        bool ValidationEditdocument(string document);
    }
}
