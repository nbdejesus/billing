﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class VendorPaymentRepository : QuicksalRepository<TblVendorPayment>, IVendorPaymentRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public VendorPaymentRepository(QuickSalContext context) : base(context)
        {

        }
        /// <summary>
        /// Metodo para obtener todas las categorias del sistema
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        public IQueryable<TblVendorPayment> GetVendorPayment(long BusinessId)
        {
            return context.TblVendorPayment.Where(x => x.BranchOfficeId == BusinessId /*|| !x.BusinessId.HasValue*/).OrderByDescending(x => x.CreateDate);
        }

        public IQueryable<PurchaseOrders> VendorPurchase(long BranchOfficeId)
        {
            var queryPurchas = (from Pur in context.TblPurchaseOrder
                                join Sup in context.TblSuppliers on Pur.SuppliersId equals Sup.Id
                                where Pur.EmployeeBusinessId == BranchOfficeId && Pur.DocumentEstatusId == 1 && Pur.Balance > 0
                                select new PurchaseOrders
                                {
                                    Suplidor = Sup.Names,
                                    CreateDate = Pur.CreateDate,
                                    Id = Pur.Id,
                                    SuppliersId = Pur.SuppliersId,
                                    Ncf = Pur.Ncf,
                                    DocumentNumber = Pur.DocumentNumber,
                                    TotalTax = Pur.TotalTax,
                                    Total = Pur.Total,
                                    Balance = Pur.Balance,
                                    DocumentEstatusId = Pur.DocumentEstatusId
                                });

            return queryPurchas;
        }
        public List<VendorPayment> GetVendorPaymentRow(long Id)
        {
            var queryPurchas = (from Pur in context.TblVendorPaymentRow
                                where Pur.VendorPaymentId == Id

                                select new VendorPayment
                                {
                                    VendorPaymentId = Pur.VendorPaymentId,
                                    PurchaseOrderId = Pur.PurchaseOrderId,
                                    SuppliersId = Pur.SuppliersId,
                                    PurchaseOrderDocument = Pur.PurchaseOrderDocument,
                                    SupplierName = Pur.SupplierName,
                                    Id = Pur.Id,
                                    Concept = Pur.Concept,
                                    Owed = Pur.Owed,
                                    PaidOut = Pur.PaidOut,
                                    Payment = Pur.Payment,
                                    Pending = Pur.Pending
                                }).ToList();

            return queryPurchas;
        }

        public decimal ValidarPagado(long Id)
        {
            var queryPurchas = (from Pur in context.TblVendorPaymentRow
                                join Pu in context.TblVendorPayment on Pur.VendorPaymentId equals Pu.Id
                                where Pur.PurchaseOrderId == Id && Pu.DocumentEstatusId == (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada
                                select new
                                {
                                    Pur.Payment
                                }).ToList();
            var pagado = queryPurchas.Select(x => x.Payment).Sum();

            return pagado;
        }

        public void DeleteRange(IEnumerable<TblVendorPaymentRow> deleteRows)
        {
            this.context.Set<TblVendorPaymentRow>().RemoveRange(deleteRows);
        }

        /// Metodo que trae el listado de compras
        /// </summary>
        /// <returns></returns>
        public IQueryable<TblVendorPaymentRow> PurchaseRow(long businessID, IEnumerable<long> datos)
        {
            var queryPurchas = (from Pur in context.TblPurchaseOrder
                                join Sup in context.TblSuppliers on Pur.SuppliersId equals Sup.Id
                                where Pur.BranchOfficeId == businessID && datos.Contains(Pur.Id)
                                select new TblVendorPaymentRow
                                {

                                    PurchaseOrderId = Pur.Id,
                                    SupplierName = Sup.Names,
                                    SuppliersId = Pur.SuppliersId,
                                    PurchaseOrderDocument = Pur.DocumentNumber,
                                    Concept = "",
                                    Pending = Pur.Balance,
                                    Owed = 0,
                                    PaidOut = 0,
                                    Payment = 0
                                });
            return queryPurchas;
        }

        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }
    }
    public interface IVendorPaymentRepository : IQuicksalBase<TblVendorPayment>
    {

        List<VendorPayment> GetVendorPaymentRow(long Id);
        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        IQueryable<PurchaseOrders> VendorPurchase(long BranchOfficeId);
        /// <summary>
        /// Metodo para obtener todas las categorias del sistema
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        IQueryable<TblVendorPayment> GetVendorPayment(long BusinessId);
        /// <summary>
        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        void DeleteRange(IEnumerable<TblVendorPaymentRow> deleteRows);


        /// Metodo que trae el listado de compras
        /// </summary>
        /// <returns></returns>
        IQueryable<TblVendorPaymentRow> PurchaseRow(long businessID, IEnumerable<long> datos);

        decimal ValidarPagado(long Id);
        Task<TaskResult> Savechanges();
    }
}
