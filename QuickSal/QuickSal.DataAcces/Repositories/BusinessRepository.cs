﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Views;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class BusinessRepository : QuicksalRepository<TblBusiness>, IBusinessRepository
    {

        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public BusinessRepository(QuickSalContext context) : base(context)
        { }

        public bool Validationdocument(string document)
        {
            bool isValid = false;
            IQueryable<TblBusiness> documentNumber = context.TblBusiness.Where(x => x.DocumentNumber == document);
            if (documentNumber.Count() > 0)
            {
                isValid = true;
            }

            return isValid;
        }

        /// <summary>
        /// Proceso para obtener todas las empresas de un usuario
        /// </summary>
        /// <param name="UserId">Llave primaria del usuario</param>
        /// <returns></returns>
        public IQueryable<TblBusiness> GetBusinessByUserId(long UserId)
        {
            try
            {
                return context.TblBusiness.Where(x => x.UserId == UserId);
            }
            catch (Exception ex)
            {
                throw new System.ArgumentException("Error procesando peticion " + ex.Message);
            }
        }

        /// <summary>
        /// Metodo para validar si una empresa tiene el mismo tipo de documento registrado
        /// </summary>
        /// <param name="document">No documento</param>
        /// <param name="businesId">Primary key de la empresa</param>
        /// <returns></returns>
        public bool ValidationEditdocument(string document, long businesId)
        {
            return context.TblBusiness.Where(x => x.DocumentNumber == document && x.Id != businesId).Any();
        }

        /// <summary>
        /// Proceso para obtener todos los tipos de empresa
        /// </summary>
        /// <returns></returns>
        public IQueryable<TblBusinessType> GetAllBusinessType()
        {
            try
            {
                return context.TblBusinessType.Where(x => x.Estatus);
            }
            catch (Exception ex)
            {
                throw new System.ArgumentException("Error en proceso para obtener todos los tipos de empresa " + ex.Message);
            }
        }

        /// <summary>
        /// Proceso para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }


        /// <summary>
        /// Proceso para obtener los objetos permitidos para la empresa
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <param name="privilegeId">Primary key del privilegio (opcional)</param>
        /// <returns></returns>
        public IQueryable<vwObjectsBusiness> GetObjects(long BusinessId, int privilegeId = 0, bool Principal = false)
        {
            return context.Set<vwObjectsBusiness>().Where(x => x.BusinessId == BusinessId &&
                                                         x.PrivilegesId == ((privilegeId == 0) ? x.PrivilegesId : privilegeId) && x.Principal == Principal);

        }

        /// <summary>
        /// Metodo para obtener la seguridad con sus accesos
        /// </summary>
        /// <param name="privilegeId"></param>
        /// <returns></returns>
        public IQueryable<vwSegurityAccess> GetSegurityAccess(int privilegeId)
        {
            return context.Set<vwSegurityAccess>().Where(x => x.PrivilegesId == privilegeId);
        }
    }

    public interface IBusinessRepository : IQuicksalBase<TblBusiness>
    {
        /// <summary>
        /// Metodo para validar si una empresa tiene el mismo tipo de documento registrado
        /// </summary>
        /// <param name="document">No documento</param>
        /// <param name="businesId">Primary key de la empresa</param>
        /// <returns></returns>
        bool ValidationEditdocument(string document, long businesId);

        bool Validationdocument(string document);
        IQueryable<TblBusinessType> GetAllBusinessType();
        IQueryable<TblBusiness> GetBusinessByUserId(long UserId);

        /// <summary>
        /// Proceso para obtener los objetos permitidos para la empresa
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <param name="privilegeId">Primary key del privilegio</param>
        /// <param name="Principal">Para obtener el privilegio super usuario</param>
        /// <returns></returns>
        IQueryable<vwObjectsBusiness> GetObjects(long BusinessId, int privilegeId = 0, bool Principal = false);

        /// <summary>
        /// Metodo para obtener la seguridad con sus accesos
        /// </summary>
        /// <param name="privilegeId"></param>
        /// <returns></returns>
        IQueryable<vwSegurityAccess> GetSegurityAccess(int privilegeId);

        /// <summary>
        /// Proceso para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
