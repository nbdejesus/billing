﻿using Microsoft.EntityFrameworkCore;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class OrderRowRepository : QuicksalRepository<TblOrdersRow>, IOrderRowRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public OrderRowRepository(QuickSalContext context) : base(context)
        {

        }   


        /// <summary>
        /// Metodo para obtener el detalle de una order de ventas
        /// </summary>
        /// <param name="OrderId">Primary key de la ordern</param>
        /// <returns></returns>
        public IQueryable<TblOrdersRow> GetOrderRows(long OrderId)
        {
            return context.TblOrdersRow.Where(x => x.OrderId == OrderId).Include(x => x.Product);
        }


        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }
    }

    public interface IOrderRowRepository : IQuicksalBase<TblOrdersRow>
    {
        /// <summary>
        /// Metodo para obtener el detalle de una order de ventas
        /// </summary>
        /// <param name="OrderId">Primary key de la ordern</param>
        /// <returns></returns>
        IQueryable<TblOrdersRow> GetOrderRows(long OrderId);

        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
