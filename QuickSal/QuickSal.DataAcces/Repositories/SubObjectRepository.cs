﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class SubObjectRepository : QuicksalRepository<TblSubObject>, ISubObjectRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public SubObjectRepository(QuickSalContext context) : base(context)
        { }

        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }

    }

    public interface ISubObjectRepository : IQuicksalBase<TblSubObject>
    {
        Task<TaskResult> Savechanges();
    }
}
