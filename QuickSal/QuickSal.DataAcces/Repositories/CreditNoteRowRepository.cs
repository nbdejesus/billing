﻿using Microsoft.EntityFrameworkCore;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class CreditNoteRowRepository : QuicksalRepository<TblCreditNoteRow>, ICreditNoteRowRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public CreditNoteRowRepository(QuickSalContext context) : base(context)
        { }

        /// <summary>
        /// Metodo para obtener el detalle de una nota de credito
        /// </summary>
        /// <param name="CreditNoteId">Primary key de la nota de credito</param>
        /// <returns></returns>
        public IQueryable<TblCreditNoteRow> GetCreditNoteDetails(long CreditNoteId)
        {
            return context.TblCreditNoteRow.Where(x => x.CreditNoteId == CreditNoteId)
                .Include(x => x.Product)
                .Include(x => x.Measure);
        }


        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }
    }

    public interface ICreditNoteRowRepository : IQuicksalBase<TblCreditNoteRow>
    {

        /// <summary>
        /// Metodo para obtener el detalle de una nota de credito
        /// </summary>
        /// <param name="CreditNoteId">Primary key de la nota de credito</param>
        /// <returns></returns>
        IQueryable<TblCreditNoteRow> GetCreditNoteDetails(long CreditNoteId);

        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
