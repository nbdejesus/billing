﻿using Microsoft.EntityFrameworkCore;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Views;
using System.Collections.Generic;
using System.Linq;

namespace QuickSal.DataAcces.Repositories
{
    public class SubscriptionsRepository : QuicksalRepository<Tblsubscriptions>, ISubscriptionsRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public SubscriptionsRepository(QuickSalContext context) : base(context)
        { }

     

        /// <summary>
        /// Proceso para obtener los objetos que tiene un plan
        /// </summary>
        /// <param name="PlanId">Primary key del plan</param>
        /// <returns></returns>
        public IEnumerable<SubObjectPlanDetail> GetSubObject(int PlanId)
        {
            var query = (from c in context.TblPlanSubObject
                         join t in context.TblSubObject on c.SubObjectId equals t.Id
                         where c.PlanId == PlanId
                         select new SubObjectPlanDetail
                         {
                             SubObjectId = c.SubObjectId,
                             PlanId = c.PlanId,
                             TypeMethodId = t.TypeMethodId,
                             Method = t.Method
                         }).ToList();

            return query;
        }

        /// <summary>
        /// Proceso para obtener los objetos mediante un listado de ID
        /// </summary>
        /// <param name="SubObjectsId"></param>
        /// <returns></returns>
        public IQueryable<vwQuicksalObjects> GetObjects(List<short> PlansId)
        {
            return context.Set<vwQuicksalObjects>().Where(x => PlansId.Contains(x.PlanId) && x.ShowOnWeb);
        }

       

        /// <summary>
        /// Metodo para obtener los planes y objetos para un tipo de negocio
        /// </summary>
        /// <param name="businessTypeId">primary key del bussines type</param>
        /// <returns></returns>
        public IQueryable<vwPlansDetail> GetPlansDetails(int businessTypeId)
        {
            return context.Set<vwPlansDetail>().Where(x => x.BusinessTypeId == businessTypeId);
        }



        /// <summary>
        /// Metodo para actualizar la suscripcion de una empresa
        /// </summary>
        /// <param name="businessId"></param>
        /// <param name="PlanId"></param>
        /// <returns></returns>
        public bool UpdateBusinessPlan(long businessId, int PlanId)
        {
            /*
             1-Tu creas tu metodo que devolvera true o false si el procedimiento se ejecuto correctamente
             2-Pasas el nombre del procedimiento almacenado "prc_UpdateBusinessPlan" utilizando el metodo "ExecuteProcedure"
             3-Pasas el o los parametros que utilizas en tu procedimiento almacenado "@businessId"
              ya eso es todo asi lo llamas
             */

            Dictionary<string, object> parametres = new Dictionary<string, object>();
            var status = 0;

            parametres.Add("@businessId", businessId);
            parametres.Add("@PlanId", PlanId);
            parametres.Add("@SystemProcess", "QuickSal APP");
            parametres.Add("@Status", status);
           

            return ExecuteProcedure("prc_UpdateBusinessPlan", parametres);
        }

        /// <summary>
        /// Metodo para obtener a que tipo de empresa pertenece un negocio
        /// </summary>
        /// <param name="BusinessId">Primary key del negocio</param>
        /// <returns></returns>
        public short GetBusinessTypeId(long BusinessId)
        {
            var subscription = context.Tblsubscriptions.Where(x => x.BusinessId == BusinessId)
                                       .Include(x => x.IdPlanNavigation)
                                       .Select(x => x.IdPlanNavigation).FirstOrDefault();

            return subscription.IdBusinessType;
        }
    }

    public interface ISubscriptionsRepository : IQuicksalBase<Tblsubscriptions>
    {      

        /// <summary>
        /// Proceso para obtener los objetos que tiene un plan
        /// </summary>
        /// <param name="PlanId">Primary key del plan</param>
        /// <returns></returns>
        IEnumerable<SubObjectPlanDetail> GetSubObject(int PlanId);

        /// <summary>
        /// Metodo para obtener los planes y objetos para un tipo de negocio
        /// </summary>
        /// <param name="businessTypeId">primary key del bussines type</param>
        /// <returns></returns>
        IQueryable<vwPlansDetail> GetPlansDetails(int businessTypeId);

        /// <summary>
        /// Proceso para obtener los objetos mediante un listado de ID
        /// </summary>
        /// <param name="SubObjectsId"></param>
        /// <returns></returns>
        IQueryable<vwQuicksalObjects> GetObjects(List<short> PlansId);

      

        /// Metodo para actualizar la suscripcion de una empresa
        /// </summary>
        /// <param name="businessId"></param>
        /// <param name="PlanId"></param>
        /// <returns></returns>
        bool UpdateBusinessPlan(long businessId, int PlanId);



        /// <summary>
        /// Metodo para obtener a que tipo de empresa pertenece un negocio
        /// </summary>
        /// <param name="BusinessId">Primary key del negocio</param>
        /// <returns></returns>
        short GetBusinessTypeId(long BusinessId);
    }
}
