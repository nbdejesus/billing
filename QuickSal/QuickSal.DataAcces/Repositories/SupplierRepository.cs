﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class SupplierRepository : QuicksalRepository<TblSuppliers>, ISupplierRepository
    {

        private new QuickSalContext context => base.context as QuickSalContext;

        public SupplierRepository(QuickSalContext context) : base(context)
        { }


        /// <summary>
        /// Metodo para consultar si existe un documento registrado para un suplidor
        /// </summary>
        /// <param name="documentTypeId">Tipo de documento</param>
        /// <param name="documentNumber">Numero de documento</param>
        /// <param name="bussineId">Primary key de la empresa</param>
        /// <param name="supplierId">Primary key del suplidor</param>
        /// <returns></returns>
        public bool ValidateDocument(int documentTypeId, string documentNumber, long bussineId, long supplierId)
        {
            var supplier = context.TblSuppliers.Where(x => x.DocumentTypeId == documentTypeId && x.DocumentNumber == documentNumber && x.BusinessId == bussineId)
                .Select(x => new { x.Id, x.DocumentNumber }).FirstOrDefault();

            if (supplier == null)
                return false;
            else
            {
                if (supplier != null && supplier.DocumentNumber == documentNumber && supplierId == supplier.Id)
                    return false;
                else
                    return true;
            }
        }

        /// <summary>
        /// Metodo para validar si el codigo de un suplidor se encuentra registrado
        /// </summary>
        /// <param name="code">Codigo del suplidor</param>
        /// <param name="bussineId">Primary key del negocio</param>
        /// <returns></returns>
        public bool ValidationCodeSuplier(string code, long bussineId)
        {
            return context.TblSuppliers.Where(x => x.Code == code && x.BusinessId == bussineId).Any();
        }

        /// <summary>
        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }
    }

    public interface ISupplierRepository : IQuicksalBase<TblSuppliers>
    {
        /// <summary>
        /// Metodo para consultar si existe un documento registrado para un suplidor
        /// </summary>
        /// <param name="documentTypeId">Tipo de documento</param>
        /// <param name="documentNumber">Numero de documento</param>
        /// <param name="bussineId">Primary key de la empresa</param>
        /// <param name="supplierId">Primary key del suplidor</param>
        /// <returns></returns>
        bool ValidateDocument(int documentTypeId, string documentNumber, long bussineId, long supplierId);

        /// <summary>
        /// Metodo para validar si el codigo de un suplidor se encuentra registrado
        /// </summary>
        /// <param name="code">Codigo del suplidor</param>
        /// <param name="bussineId">Primary key del negocio</param>
        /// <returns></returns>
        bool ValidationCodeSuplier(string code, long bussineId);

        /// <summary>
        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
