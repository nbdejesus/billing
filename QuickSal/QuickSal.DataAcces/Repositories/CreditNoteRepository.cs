﻿using Microsoft.EntityFrameworkCore;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Models.Customer;
using QuickSal.DataAcces.Procedure.Customer;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class CreditNoteRepository : QuicksalRepository<TblCreditNote>, ICreditNoteRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public CreditNoteRepository(QuickSalContext context) : base(context)
        { }

        /// <summary>
        /// Metodo para obtener todas las notas de credito de una sucursal
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        public IQueryable<GetAllCreditNote> GetAllCreditNotes(long BranchOfficeId)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("@BranchOfficeId", BranchOfficeId);
            return ExecuteQuerys(new GetAllCreditNote(), "Customer.prc_GetAllCreditNote", parameters);
        }


        /// <summary>
        /// Metodo para obtener el ultimo número de documento generado
        /// </summary>
        /// <returns></returns>
        public string GetLastCode()
        {
            var data = context.TblCreditNote.LastOrDefault();
            if (data == null)
                return null;
            else
                return data.DocumentNumber;
        }


        /// <summary>
        /// Metodo para procesar los datos de una nota de credito
        /// </summary>
        /// <param name="branchOfficeId">Primary key de la sucursal</param>
        /// <param name="creditNoteId">Primary key de la cabecera de la nota de credito</param>
        /// <returns></returns>
        public bool ProcessCreditNote(long branchOfficeId, long creditNoteId)
        {
            Dictionary<string, object> parametres = new Dictionary<string, object>();
            bool status = false;
            parametres.Add("@BranchOfficeId", branchOfficeId);
            parametres.Add("@CreditNoteId", creditNoteId);
            parametres.Add("@SystemProcess", "QuickSal APP");
            parametres.Add("@Status", status);

            return ExecuteProcedure("Customer.prc_ProcessCreditNote", parametres);
        }


        /// <summary>
        /// Metodo para obtener la cabecera de una nota de credito
        /// </summary>
        /// <param name="CreditNoteId">Primary key de la nota de credito</param>
        /// <param name="BanchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        public TblCreditNote GetCreditNoteHeader(long CreditNoteId, long BanchOfficeId)
        {
            return context.TblCreditNote.Where(x => x.Id == CreditNoteId && x.BranchOfficeId == BanchOfficeId)
                .Include(x => x.Customer)
                .Include(x => x.Invoice).FirstOrDefault();
        }

        /// <summary>
        /// Metodo para obtener el balacen pendiente de un cliente
        /// </summary>
        /// <param name="branchOfficeId">Primary key de la sucursal</param>
        /// <param name="customerId">Primary key del cliente</param>
        /// <returns></returns>
        public IQueryable<CreditNoteVM> GetPendingBalance(long branchOfficeId, long customerId)
        {
            var queryCredit = (from d in context.TblCreditNote
                               join s in context.TblCustomers on d.CustomerId equals s.Id
                               where d.BranchOfficeId == branchOfficeId && d.Used == false && d.CustomerId == customerId
                               && d.DocumentEstatusId == (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada
                               select new CreditNoteVM
                               {
                                   Id = d.Id,
                                   DocumentNumber = d.DocumentNumber,
                                   SupplierName = s.Names,
                                   InvoiceId = d.InvoiceId,
                                   Ncf = d.Ncf,
                                   Ncfmodified = d.Ncfmodified,
                                   CreateDate = d.CreateDate,
                                   Total = d.Total,
                                   DocumentEstatusId = d.DocumentEstatusId,
                               });

            var query = context.TblCretitNoteUsed.Where(x => queryCredit.Select(d => d.Id).Contains(x.CreditNoteId));

            return queryCredit.Where(x => !query.Select(d => d.CreditNoteId).Contains(x.Id));
        }

        /// <summary>
        /// Obtiene los totales de las notas de credito seleccionadas
        /// </summary>
        /// <param name="creditNoteId">Listados con los primary key de las notas de credito</param>
        /// <returns></returns>
        public Dictionary<long, decimal> GetPendingBalance(List<long> creditNoteId)
        {
            return (from c in context.TblCreditNote
                    where creditNoteId.Contains(c.Id)
                    select c).ToDictionary(x => x.Id, x => x.Total);
        }


        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }
    }

    public interface ICreditNoteRepository : IQuicksalBase<TblCreditNote>
    {
        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();

        /// <summary>
        /// Metodo para obtener todas las notas de credito de una sucursal
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        IQueryable<GetAllCreditNote> GetAllCreditNotes(long BranchOfficeId);


        /// <summary>
        /// Metodo para obtener la cabecera de una nota de credito
        /// </summary>
        /// <param name="CreditNoteId">Primary key de la nota de credito</param>
        /// <param name="BanchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        TblCreditNote GetCreditNoteHeader(long CreditNoteId, long BanchOfficeId);

        /// <summary>
        /// Metodo para procesar los datos de una nota de credito
        /// </summary>
        /// <param name="branchOfficeId">Primary key de la sucursal</param>
        /// <param name="creditNoteId">Primary key de la cabecera de la nota de credito</param>
        /// <returns></returns>
        bool ProcessCreditNote(long branchOfficeId, long creditNoteId);

        /// <summary>
        /// Metodo para obtener el balacen pendiente de un cliente
        /// </summary>
        /// <param name="branchOfficeId">Primary key de la sucursal</param>
        /// <param name="customerId">Primary key del cliente</param>
        /// <returns></returns>
        IQueryable<CreditNoteVM> GetPendingBalance(long branchOfficeId, long customerId);

        /// <summary>
        /// Obtiene los totales de las notas de credito seleccionadas
        /// </summary>
        /// <param name="creditNoteId">Listados con los primary key de las notas de credito</param>
        /// <returns></returns>
        Dictionary<long, decimal> GetPendingBalance(List<long> creditNoteId);


        /// <summary>
        /// Metodo para obtener el ultimo número de documento generado
        /// </summary>
        /// <returns></returns>
        string GetLastCode();
    }
}
