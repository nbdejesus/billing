﻿using Microsoft.EntityFrameworkCore;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class SubCategoryRepository : QuicksalRepository<TblSubCategory>, ISubCategoryRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public SubCategoryRepository(QuickSalContext context) : base(context)
        { }

        /// <summary>
        /// Metodo para obtener todas las subcategorias del sistema
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        public IQueryable<TblSubCategory> GetSubCategories(long BusinessId)
        {
           
            return context.TblSubCategory.Where(x => x.BusinessId == BusinessId || !x.BusinessId.HasValue)
                                               .Include(x => x.Category).OrderByDescending(x => x.CreateDate);
        }

        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }
    }

    public interface ISubCategoryRepository : IQuicksalBase<TblSubCategory>
    {
        /// <summary>
        /// Metodo para obtener todas las subcategorias del sistema
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        IQueryable<TblSubCategory> GetSubCategories(long BusinessId);

        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
