﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class LogErrorRepository : QuicksalRepository<TblLogError>, ILogErrorRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public LogErrorRepository(QuickSalContext context) : base(context)
        { }

        /// <summary>
        /// Proceso para registrar un error en la tabla de errores
        /// </summary>
        /// <param name="Error">Error prensentado</param>
        /// <param name="MetodoEjecutado">Metodo donde se presento el error</param>
        /// <param name="UbicacionCode">Clase o controller donde se encuentra el metodo</param>
        /// <returns></returns>
        public async Task<TaskResult> AddError(string Error, string MetodoEjecutado, string UbicacionCode)
        {
            TblLogError LogError = new TblLogError();
            LogError.CreateDate = DateTime.Now;
            LogError.ErrorMessage = Error;
            LogError.Method = MetodoEjecutado;
            LogError.Location = UbicacionCode;
            LogError.SystemProccess = "Quicksal";
            AddEntity(LogError);
            return await Save();
        }
    }

    public interface ILogErrorRepository : IQuicksalBase<TblLogError>
    {
        /// <summary>
        /// Proceso para registrar un error en la tabla de errores
        /// </summary>
        /// <param name="Error">Error prensentado</param>
        /// <param name="MetodoEjecutado">Metodo donde se presento el error</param>
        /// <param name="UbicacionCode">Clase o controller donde se encuentra el metodo</param>
        /// <returns></returns>
        Task<TaskResult> AddError(string Error, string MetodoEjecutado, string UbicacionCode);
    }
}
