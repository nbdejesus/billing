﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions.Internal;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.CustomModels.Models.Plan;
using QuickSal.DataAcces.Models;
using Remotion.Linq.Utilities;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class PlanRepository : QuicksalRepository<TblPlans>, IPlanRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public PlanRepository(QuickSalContext context) : base(context)
        { }

        /// <summary>
        /// Metodo para obtener el nombre de un plan asociado a una empresa
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        public string GetPlanName(long BusinessId)
        {
            return (from s in context.Tblsubscriptions
                    join p in context.TblPlans on s.IdPlan equals p.Id
                    where s.BusinessId == BusinessId
                    select p.Name).FirstOrDefault();
        }

        /// <summary>
        /// Proceso para obtener los planes activos para el tipo de negocio
        /// </summary>
        /// <param name="IdBusinessType"></param>
        /// <returns></returns>
        public IEnumerable<TblPlans> GetPlans(int IdBusinessType)
        {
            return context.TblPlans.Where(x => x.IdBusinessType == IdBusinessType && x.Estatus)
                .Include(x => x.TblPlanDetails)
                .Include(x => x.TblPlanSubObject);
        }

        /// <summary>
        /// Proceso para obtener un plan mediante su llave primaria
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public TblPlans GetPlan(short Id)
        {
            return context.TblPlans.Find(Id);
        }

        /// <summary>
        /// Metodo para obtener el id del plan que tiene una empresa
        /// </summary>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        public int GetPlanId(long businessId)
        {
            return context.Tblsubscriptions.Where(x => x.BusinessId == businessId).Select(x => x.IdPlan).FirstOrDefault();
        }

        /// <summary>
        /// Metodo para obtener los Modulos de un plan junto con su agrupacion
        /// </summary>
        /// <param name="planId">Primary key del plan</param>
        /// <returns></returns>
        public IQueryable<TemplateStatus> GetAllModule(int planId)
        {

            return from PO in context.TblPlanSubObject
                   join SO in context.TblSubObject on PO.SubObjectId equals SO.Id
                   join O in context.TblObject on SO.ObjectId equals O.Id
                   where PO.PlanId == planId && SO.Parent == null
                   select new TemplateStatus
                   {
                       SubObjectName = SO.WebDescription,
                       SubObjecId = SO.Id,
                       ObjectId = SO.ObjectId,
                       ObjectName = O.Description,
                       ObjectIcon = O.Ico,
                       SubObjectIcon = SO.Ico
                   };
        }


        /// <summary>
        /// Metodo para obtener los Modulos
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<TemplateStatus>> GetAllModule()
        {
            return await (from SO in context.TblSubObject
                          join O in context.TblObject on SO.ObjectId equals O.Id
                          where SO.Parent == null
                          select new TemplateStatus
                          {
                              SubObjectName = SO.WebDescription,
                              SubObjecId = SO.Id,
                              ObjectId = SO.ObjectId,
                              ObjectName = O.Description,
                              ObjectIcon = O.Ico,
                              SubObjectIcon = SO.Ico
                          }).ToListAsync();
        }


        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }

    }

    public interface IPlanRepository : IQuicksalBase<TblPlans>
    {

        /// <summary>
        /// Proceso para obtener los planes activos para el tipo de negocio
        /// </summary>
        /// <param name="IdBusinessType"></param>
        /// <returns></returns>
        IEnumerable<TblPlans> GetPlans(int IdBusinessType);

        /// <summary>
        /// Proceso para obtener un plan mediante su llave primaria
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        TblPlans GetPlan(short Id);

        /// <summary>
        /// Metodo para obtener el id del plan que tiene una empresa
        /// </summary>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        int GetPlanId(long businessId);


        /// <summary>
        /// Metodo para obtener los Modulos de un plan
        /// </summary>
        /// <param name="planId">Primary key del plan</param>
        /// <returns></returns>
        IQueryable<TemplateStatus> GetAllModule(int planId);

        /// <summary>
        /// Metodo para obtener los Modulos
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<TemplateStatus>> GetAllModule();


        /// <summary>
        /// Metodo para obtener el nombre de un plan asociado a una empresa
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        string GetPlanName(long BusinessId);

        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
