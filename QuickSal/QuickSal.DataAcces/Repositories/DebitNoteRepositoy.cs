﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using QuickSal.DataAcces.Procedure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class DebitNoteRepositoy : QuicksalRepository<TblDebitNote>, IDebitNoteRepositoy
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }
        public DebitNoteRepositoy(QuickSalContext context) : base(context)
        {

        }

        /// Metodo que trae el listado de nota de Debito
        /// </summary>
        /// <returns></returns>
        public IQueryable<DebitNote> ListDebitNote(long branchOfficeId)
        {
            var queryDebit = (from d in context.TblDebitNote
                              join s in context.TblSuppliers on d.SuppliersId equals s.Id
                              where d.BranchOfficeId == branchOfficeId
                              select new DebitNote
                              {
                                  Id = d.Id,
                                  DocumentNumber = d.DocumentNumber,
                                  SupplierName = s.Names,
                                  PurchaseOrderId = d.PurchaseOrderId,
                                  Ncf = d.Ncf,
                                  Ncfmodified = d.Ncfmodified,
                                  CreateDate = d.CreateDate,
                                  Total = d.Total,
                                  DocumentEstatusId = d.DocumentEstatusId,

                              });

            return queryDebit;
        }

        public IQueryable<DebitNote> ListDebitNoteStatus(long branchOfficeId, long suplierId)
        {
            var queryDebit = (from d in context.TblDebitNote
                              join s in context.TblSuppliers on d.SuppliersId equals s.Id
                              where d.BranchOfficeId == branchOfficeId && d.SuppliersId == suplierId
                              && d.DocumentEstatusId == (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada
                              && d.Used == false
                              select new DebitNote
                              {
                                  Id = d.Id,
                                  DocumentNumber = d.DocumentNumber,
                                  SupplierName = s.Names,
                                  PurchaseOrderId = d.PurchaseOrderId,
                                  Ncf = d.Ncf,
                                  Ncfmodified = d.Ncfmodified,
                                  CreateDate = d.CreateDate,
                                  Total = d.Total,
                                  DocumentEstatusId = d.DocumentEstatusId,
                              });

            // var query = context.TblDebitNoteUsed.Where(x => queryDebit.Select(d => d.Id).Contains(x.DebitNoteId));

            //return queryDebit.Where(x => !query.Select(d => d.DebitNoteId).Contains(x.Id));
            return queryDebit;
        }

        /// Metodo que trae el detalle  de nota de Debito
        /// </summary>
        /// <returns></returns>
        public TblDebitNote DebitNoteDetails(long id)
        {
            var queryDebit = context.TblDebitNote.Where(x => x.Id == id).FirstOrDefault();

            return queryDebit;
        }


        /// Metodo que trae el listado de detalle correspondiente
        /// </summary>
        /// <returns></returns>
        public List<DebitNoteRowVM> GetDebitNoteRow(long DebitNoteId)
        {
            var row = (from r in context.TblDebitNoteRow
                       join w in context.TblWarehouse on r.WarehouseId equals w.Id
                       join p in context.TblProducts on r.ProductId equals p.Id
                       join m in context.TblMeasure on r.MeasureId equals m.Id
                       where r.DebitNoteId == DebitNoteId
                       select  new DebitNoteRowVM
                       {
                         
                         
                           Id = r.Id,
                           Product = p.Name,
                           MeasureId = r.MeasureId,
                           Warehouse = w.Name,
                           WarehouseId = r.WarehouseId,
                           Tax = r.Tax,
                           ProductId = r.ProductId,
                           Quantity = r.Quantity,
                           Cost = r.Cost,
                           Discount = r.Discount,
                           AmountTax = r.AmountTax,
                           AmountDiscount = r.AmountDiscount,
                           Total = r.Total,
                           Measure = m.Name,
                           TaxAbbreviation = r.TaxAbbreviation,                          
                           Accion = "<ul class=\"icons-list\"><li class=\"dropdown\"><a href =\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"icon-menu9\"></i></a><ul class=\"dropdown-menu dropdown-menu-right\">" +
                           "<li><a type=\"button\" class=\"btn btn-default btn-icon modificarProducto\" data-product=\"" + r.ProductId + "\" data-id=\"" + r.Id + "\"  ><i class=\"icon-eye\"></i> Editar</a></li>" +
                           "<li><a type =\"button\" class=\"btn btn-default btn-icon deleteRow\" ><i class=\"icon-trash\" ></i> Eliminar</a></ul></li></ul>"//boton de eliminar         

                       }).ToList();

            return row;
        }

        /// <summary>
        /// Metodo para obtener la ultima nota de debito
        /// </summary>
        /// <param name="branchOfficeId">Primary key de la oficina</param>
        /// <returns></returns>
        public string GetLastMoving(long branchOfficeId)
        {

            var code = (from d in context.TblDebitNote
                        where d.BranchOfficeId == branchOfficeId
                        select
                             d.DocumentNumber).LastOrDefault();
            if (code == null)
                code = "ND0000000";


            return code;
        }


        /// <summary>
        /// Metodo que Modifica la cantidad disponible del inventario
        /// </summary>
        /// <param name="WareHouseId">Parametro que contiene el Id del amacen</param>
        /// <param name="ProductId">Parametro que contiene el Id del producto</param>
        /// <param name="cantidad">Parametro que contiene la cantidad enviada</param>
        /// <returns></returns>
        public TaskResult UpdateInventoryWareHouse(long WareHouseId, long ProductId, decimal cantidad)
        {

            TblInventoryWarehouse TblInventoryWarehouse = new TblInventoryWarehouse();

            var dato = context.TblInventoryWarehouse.Where(x => x.WarehouseId == WareHouseId && x.ProductsId == ProductId).FirstOrDefault();
            try
            {
                TblInventoryWarehouse = dato;
                decimal disponible = 0;
                decimal resultado = 0;

                disponible = TblInventoryWarehouse.ProductAmount;
                resultado = disponible - cantidad;

                TblInventoryWarehouse.ProductAmount = resultado;
                context.TblInventoryWarehouse.Update(TblInventoryWarehouse);

                return TaskResult;
            }


            catch (Exception)
            {

                return TaskResult;

            }


        }


        /// Metodo que trae el listado de ordenes de compra disponible
        /// </summary>
        /// <returns></returns>
        public IEnumerable<prc_PurchaseOrderRow> GetPurchOrderRow(long Id)
        {

            var parameters = new Dictionary<string, object>();
            parameters.Add("@PurchaseOrderId", Id);

            var purchaseOrder = ExecuteQuery(new prc_PurchaseOrderRow(), "prc_PurchaseOrderAvailable", parameters);
            return purchaseOrder;

        }

        /// <summary>
        /// Metodo validar la cantidad del productos debitados de una orden de compra
        /// </summary>
        /// <param name="PurchaseId">Parametro que contiene el Id del amacen</param>
        /// <param name="ProductId">Parametro que contiene el Id del producto</param>   
        /// <returns></returns>
        public ValidacionProducto ValidarCantidadProducto(long PurchaseId, long ProductId)
        {
            decimal totalDebitado = 0;
            ValidacionProducto resultado = new ValidacionProducto();

            var totalRow = context.TblPurchaseOrderRow.Where(x => x.PurchaseOrderId == PurchaseId && x.ProductId == ProductId).FirstOrDefault();

            var cantidadDebitada = (from d in context.TblDebitNote
                                    join dr in context.TblDebitNoteRow on d.Id equals dr.DebitNoteId
                                    join pr in context.TblProducts on dr.ProductId equals pr.Id
                                    where d.PurchaseOrderId == PurchaseId && dr.ProductId == ProductId
                                    select new
                                    {
                                        dr.Quantity,
                                        pr.Name,
                                        d.DocumentEstatusId

                                    }).Where(x => x.DocumentEstatusId == (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada);
            if (cantidadDebitada.Count() != 0)
            {
                totalDebitado = cantidadDebitada.Select(c => c.Quantity).Sum();
                resultado.Quantity = totalRow.Quantity - totalDebitado;
                resultado.Name = cantidadDebitada.FirstOrDefault().Name;

            }
            else
            {
                resultado.Quantity = totalRow.Quantity;

            }



            return resultado;

        }


        /// <summary>
        /// Metodo para agregar el NCF a la nota de debito
        /// </summary>
        /// <param name="businessId">Primary key de la sucursal</param>
        /// <param name="DebitNoteId">Primary key de la cabecera de la nota de debito</param>
        /// <returns></returns>
        public bool AddNcfDebitNote(long businessId, long DebitNoteId)
        {
            Dictionary<string, object> parametres = new Dictionary<string, object>();
            bool status = false;
            parametres.Add("@BusinessId", businessId);
            parametres.Add("@DebitNoteId", DebitNoteId);
            parametres.Add("@SystemProcess", "QuickSal APP");
            parametres.Add("@Status", status);

            return ExecuteProcedure("Customer.prc_AddNcfDebitNote", parametres);
        }


        /// <summary>
        /// Metodo para eliminar el row del detalle de nota de debito
        /// </summary>
        /// <param name="deleteRows">Modelo que contiene la nota de debito</param>
        /// <returns></returns>

        public void DeleteRange(IEnumerable<TblDebitNoteRow> deleteRows)
        {
            this.context.Set<TblDebitNoteRow>().RemoveRange(deleteRows);
        }

        /// <summary>
        /// Obtiene los totales de las notas de debitos seleccionadas
        /// </summary>
        /// <param name="debitsNoteId">Listados con los primary key de las notas de debito</param>
        /// <returns></returns>
        public Dictionary<long, decimal> GetPendingBalance(List<long> debitsNoteId)
        {
            return (from c in context.TblDebitNote
                    where debitsNoteId.Contains(c.Id)
                    select c).ToDictionary(x => x.Id, x => x.Total);
        }

        /// <summary>
        /// Metodo para obtener el listado de nota de debito que contiene esa orden de compra
        /// </summary>
        /// <param name="purchaseOrdeId">Id que contiene el la orden de compra</param>
        /// <returns></returns>

        public ICollection<TblDebitNote> DebitNotePurchaseOder(long purchaseOrdeId)
        {
            var queryDebit = (from d in context.TblDebitNote
                              where d.PurchaseOrderId == purchaseOrdeId
                              && d.DocumentEstatusId == (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada
                              && d.Used == false
                              select new TblDebitNote
                              {
                                  Id = d.Id,
                                  BranchOfficeId   = d.BranchOfficeId,
                                  SuppliersId      = d.SuppliersId,
                                  UpdateDate       = d.UpdateDate,
                                  Observation      = d.Observation,
                                  DocumentNumber   = d.DocumentNumber,
                                  SubTotal         = d.SubTotal,
                                  TotalTax         = d.TotalTax,
                                  EmployeeCreate   = d.EmployeeCreate,
                                  EmployeeModifyId = d.EmployeeModifyId,
                                  PurchaseOrderId  = d.PurchaseOrderId,
                                  Ncf              = d.Ncf,
                                  Ncfmodified      = d.Ncfmodified,
                                  CreateDate       = d.CreateDate,
                                  Total            = d.Total,
                                  DocumentEstatusId = d.DocumentEstatusId,
                                  Used            = d.Used
                              }).ToList();

            // var query = context.TblDebitNoteUsed.Where(x => queryDebit.Select(d => d.Id).Contains(x.DebitNoteId));

            //return queryDebit.Where(x => !query.Select(d => d.DebitNoteId).Contains(x.Id));
            return queryDebit;
        }

        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }
    }
    public interface IDebitNoteRepositoy : IQuicksalBase<TblDebitNote>
    {

        IQueryable<DebitNote> ListDebitNoteStatus(long branchOfficeId, long suplierId);

        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();

        /// Metodo que trae el listado de nota de Debito
        /// </summary>
        /// <returns></returns>
        IQueryable<DebitNote> ListDebitNote(long branchOfficeId);

        /// Metodo que trae e detalle de la  nota de Debito
        /// </summary>
        /// <returns></returns>
        TblDebitNote DebitNoteDetails(long Id);

        /// Metodo que trae el listado de detalle correspondiente
        /// </summary>
        /// <returns></returns>
        List<DebitNoteRowVM> GetDebitNoteRow(long DebitNoteId);

        /// <summary>
        /// Metodo para obtener la ultima nota de debito
        /// </summary>
        /// <param name="branchOfficeId">Primary key de la oficina</param>
        /// <returns></returns>
        string GetLastMoving(long branchOfficeId);

        /// Metodo que trae el listado de ordenes de compra disponible
        /// </summary>
        /// <returns></returns>
        IEnumerable<prc_PurchaseOrderRow> GetPurchOrderRow(long Id);

        /// <summary>
        /// Metodo que Modifica la cantidad disponible del inventario
        /// </summary>
        /// <param name="WareHouseId">Parametro que contiene el Id del amacen</param>
        /// <param name="ProductId">Parametro que contiene el Id del producto</param>
        /// <param name="cantidad">Parametro que contiene la cantidad enviada</param>
        /// <returns></returns>
        TaskResult UpdateInventoryWareHouse(long WareHouseId, long ProductId, decimal cantidad);

        /// <summary>
        /// Metodo validar la cantidad del productos debitados de una orden de compra
        /// </summary>
        /// <param name="PurchaseId">Parametro que contiene el Id del amacen</param>
        /// <param name="ProductId">Parametro que contiene el Id del producto</param>   
        /// <returns></returns>
        ValidacionProducto ValidarCantidadProducto(long PurchaseId, long ProductId);


        /// <summary>
        /// Metodo para agregar el NCF a la nota de debito
        /// </summary>
        /// <param name="branchOfficeId">Primary key de la sucursal</param>
        /// <param name="DebitNoteId">Primary key de la cabecera de la nota de debito</param>
        /// <returns></returns>
        bool AddNcfDebitNote(long businessId, long DebitNoteId);

        /// <summary>
        /// Obtiene los balances de las notas de debitos seleccionadas
        /// </summary>
        /// <param name="debitsNoteId">Listados con los primary key de las notas de debito</param>
        /// <returns></returns>
        Dictionary<long, decimal> GetPendingBalance(List<long> debitsNoteId);

        void DeleteRange(IEnumerable<TblDebitNoteRow> deleteRows);

        /// <summary>
        /// Metodo para obtener el listado de nota de debito que contiene esa orden de compra
        /// </summary>
        /// <param name="purchaseOrdeId">Id que contiene el la orden de compra</param>
        /// <returns></returns>

        ICollection<TblDebitNote> DebitNotePurchaseOder(long purchaseOrdeId);

    }
}
