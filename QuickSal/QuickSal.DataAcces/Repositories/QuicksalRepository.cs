﻿using Microsoft.EntityFrameworkCore;
using QuickSal.DataAcces.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
//using System.Data.Entity.Validation;
//using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public abstract class QuicksalRepository<T> : IQuicksalBase<T> where T : class
    {
        protected readonly DbContext context;
        protected TaskResult TaskResult;

        public QuicksalRepository(DbContext context)
        {
            if (context == null) throw new ArgumentNullException("context");
            this.context = context;
            TaskResult = new TaskResult { ExecutedSuccesfully = true };
        }

        public T Get(long id)
        {
            return this.context.Set<T>().Find(id);
        }

        public IEnumerable<T> GetALL()
        {
            return this.context.Set<T>().AsQueryable();
        }

        public TaskResult AddEntity(T entity)
        {
            try
            {
                this.context.Set<T>().Add(entity);
                return TaskResult;
            }
            catch (Exception ex)
            {
                TaskResult.Exception = ex;
                return TaskResult;
            }
        }

        public TaskResult AddRangeEntity(IEnumerable<T> entity)
        {
            try
            {
                this.context.Set<T>().AddRange(entity);
                return TaskResult;
            }
            catch (Exception ex)
            {
                TaskResult.Exception = ex;
                return TaskResult;
            }
        }

        public virtual IQueryable<T> Get(Expression<Func<T, bool>> where, string includeProperties = "")
        {
            var query = context.Set<T>().AsQueryable();

            foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (where != null)
                query = query.Where(where);

            return query;
        }

        public IQueryable<T> Get(Expression<Func<T, bool>> @where, params Expression<Func<T, object>>[] include)
        {
            var query = context.Set<T>().AsQueryable();

            foreach (var includeProperty in include)
            {
                query = query.Include(includeProperty);
            }

            if (where != null)
                query = query.Where(where);

            return query;
        }

        public IQueryable<T> Get(params Expression<Func<T, object>>[] include)
        {
            var query = context.Set<T>().AsQueryable();

            foreach (var includeProperty in include)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        public void Update(T entity)
        {
            this.context.Entry<T>(entity).State = EntityState.Modified;
        }

        public void UpdateRange(IEnumerable<T> entity)
        {

            this.context.UpdateRange(entity);
        }

        public async Task<TaskResult> Save()
        {
            try
            {
                TaskResult.ExecutedSuccesfully = ((await this.context.SaveChangesAsync() > 0) ? true : false);
                return TaskResult;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                string exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                //ErrorHandler.logError(exceptionMessage, "SaveDb", "CidominDb");
                TaskResult.AddErrorMessage(exceptionMessage);
                return TaskResult;
            }
            catch (DbUpdateException ex)
            {
                if (ex.InnerException != null)
                    TaskResult.AddErrorMessage(ex.InnerException.Message);
                else
                    TaskResult.AddErrorMessage(ex.Message);

                return TaskResult;
            }
            catch (Exception ex)
            {
                TaskResult.AddErrorMessage(ex.Message);
                return TaskResult;
            }
        }

        public void Reload(T entity)
        {
            this.context.Entry(entity).Reload();
        }

        public void Delete(T entity)
        {
            this.context.Set<T>().Remove(entity);
        }

        public void DeleteRange(IEnumerable<T> entity)
        {
            this.context.Set<T>().RemoveRange(entity);
        }

        public void Delete(int id)
        {
            var entity = this.context.Set<T>().Find(id);
            if (entity != null)
            {
                Delete(entity);
            }
        }

        public bool Exists(int id)
        {
            return this.context.Set<T>().Find(id) == null ? true : false;
        }

        public IQueryable<C> ExecuteQuerys<C>(C entity, string name, Dictionary<string, object> parameters) where C : class
        {
            var dbParameters = new List<SqlParameter>();
            var outputParameter = new SqlParameter();
            var execString = "exec " + name;
            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    dbParameters.Add(
                        new SqlParameter(parameter.Key, parameter.Value ?? DBNull.Value)
                    );
                    execString += " " + parameter.Key + ",";
                }
            }

            var query = ((parameters != null) ? execString.Substring(0, (execString.Length - 1)) : execString);
            return context.Set<C>().AsNoTracking().FromSql(query, dbParameters.ToArray());
        }

        public bool ExecuteProcedure(string name, Dictionary<string, object> parameters)
        {
            var dbParameters = new List<SqlParameter>();
            var outputParameter = new SqlParameter();
            var execString = "exec " + name;
            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    dbParameters.Add(
                        new SqlParameter(parameter.Key, parameter.Value ?? DBNull.Value)
                    );
                    execString += " " + parameter.Key + ",";
                }
            }

            var query = execString.Substring(0, (execString.Length - 1));
            int result = context.Database.ExecuteSqlCommand(query, dbParameters.ToArray());

            return ((result > 0) ? true : false);
        }

        public IQueryable<C> ExecuteQuery<C>(C entity, string name, Dictionary<string, object> parameters) where C : class
        {
            var dbParameters = new List<SqlParameter>();
            var outputParameter = new SqlParameter();
            var execString = "exec " + name;
            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    dbParameters.Add(
                        new SqlParameter(parameter.Key, parameter.Value ?? DBNull.Value)
                    );
                    execString += " " + parameter.Key + ",";
                }
            }

            var query = execString.Substring(0, (execString.Length - 1));
            return context.Set<C>().AsNoTracking().FromSql(query, dbParameters.ToArray());
        }
    }


    public interface IQuicksalBase<T> where T : class
    {
        T Get(long id);
        IEnumerable<T> GetALL();
        TaskResult AddEntity(T entity);
        Task<TaskResult> Save();
        bool Exists(int id);
        void Reload(T entity);
        void Delete(T entity);
        void Delete(int id);
        void Update(T entity);
        TaskResult AddRangeEntity(IEnumerable<T> entity);
        void DeleteRange(IEnumerable<T> entity);
        void UpdateRange(IEnumerable<T> entity);



        IQueryable<T> Get(Expression<Func<T, bool>> where, string includeProperties = "");
        IQueryable<T> Get(Expression<Func<T, bool>> @where, params Expression<Func<T, object>>[] include);
        IQueryable<T> Get(params Expression<Func<T, object>>[] include);
        IQueryable<C> ExecuteQuery<C>(C entity, string name, Dictionary<string, object> parameters) where C : class;
        bool ExecuteProcedure(string name, Dictionary<string, object> parameters);

        IQueryable<C> ExecuteQuerys<C>(C entity, string name, Dictionary<string, object> parameters) where C : class;
    }
}
