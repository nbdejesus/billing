﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class MeasureRepository : QuicksalRepository<TblMeasure>, IMeasureRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public MeasureRepository(QuickSalContext context) : base(context)
        { }

        /// <summary>
        /// Metodo para obtener todas las medidas del sistema
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        public IQueryable<TblMeasure> GetMeasurements(long BusinessId)
        {
            return context.TblMeasure.Where(x => x.BusinessId == BusinessId || !x.BusinessId.HasValue).OrderByDescending(x => x.CreateDate);
        }

        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }
    }

    public interface IMeasureRepository : IQuicksalBase<TblMeasure>
    {
        /// <summary>
        /// Metodo para obtener todas las medidas del sistema
        /// </summary>
        /// <param name="BusinessId">Primary key de la empresa</param>
        /// <returns></returns>
        IQueryable<TblMeasure> GetMeasurements(long BusinessId);


        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
