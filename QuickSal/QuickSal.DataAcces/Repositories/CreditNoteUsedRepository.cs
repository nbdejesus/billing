﻿using QuickSal.DataAcces.Contexts;

namespace QuickSal.DataAcces.Repositories
{
    public class CreditNoteUsedRepository : QuicksalRepository<TblCretitNoteUsed>, ICreditNoteUsedRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public CreditNoteUsedRepository(QuickSalContext context) : base(context)
        { }
    }

    public interface ICreditNoteUsedRepository : IQuicksalBase<TblCretitNoteUsed>
    {

    }
}
