﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
   public class CustomerPaymentsRepository : QuicksalRepository<TblCustomerPayments>, ICustomerPaymentsRepository
    {

        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public CustomerPaymentsRepository(QuickSalContext context) : base(context)
        {

        }

        public IQueryable<TblCustomerPayments> GetCustomerPayments(long BusinessId)
        {
            return context.TblCustomerPayments.Where(x => x.BranchOfficeId == BusinessId /*|| !x.BusinessId.HasValue*/).OrderByDescending(x => x.CreateDate);
        }

        public IQueryable<Invoice> GetInvoice(long businessID)
        {
            var queryPurchas = (from Inv in context.TblInvoice
                                join Sup in context.TblCustomers on Inv.CustomerId equals Sup.Id
                                where Inv.BranchOfficeId == businessID && Inv.DocumentEstatusId == 1 && Inv.Balance > 0
                                select new Invoice
                                {
                                    CustomerName = Sup.Names,
                                    CreateDate = Inv.CreateDate,
                                    Id = Inv.Id,
                                    CustomerId = Inv.CustomerId,
                                    Ncf = Inv.Ncf,
                                    DocumentNumber = Inv.DocumentNumber,
                                    TotalTax = Inv.TotalTax,
                                    Total = Inv.Total,
                                    Balance = Inv.Balance,
                                    DocumentEstatusId = Inv.DocumentEstatusId
                                });

            return queryPurchas;
        }

        public List<CustomerPayments> GetCustomerPaymentRow(long Id)
        {
            var queryPurchas = (from InvRow in context.TblCustomerPaymentsRow
                                where InvRow.CustomerPaymentId == Id

                                select new CustomerPayments
                                {
                                    Id=InvRow.Id,
                                    CustomerId = InvRow.CustomerId,
                                    CustomerPaymentId = InvRow.CustomerPaymentId,
                                    InvoiceId = InvRow.InvoiceId,
                                    InvoiceDocument = InvRow.InvoiceDocument,
                                    CustomerName = InvRow.CustomerName,
                                    Concept = InvRow.Concept,
                                    Owed = InvRow.Owed,
                                    PaidOut = InvRow.PaidOut,
                                    Payment = InvRow.Payment,
                                    Pending = InvRow.Pending,
                                }).ToList();

            return queryPurchas;
        }

        public decimal ValidarPagado(long Id)
        {
            var queryPurchas = (from Pur in context.TblCustomerPaymentsRow
                                join Pu in context.TblCustomerPayments on Pur.CustomerPaymentId equals Pu.Id
                                where Pur.InvoiceId == Id && Pu.DocumentEstatusId == (int)DataAcces.QuickSalEnum.DocumentEstatus.Aprobada
                                select new
                                {
                                    Pur.Payment
                                }).ToList();
            var pagado = queryPurchas.Select(x => x.Payment).Sum();

            return pagado;
        }

        public void DeleteRange(IEnumerable<TblCustomerPaymentsRow> deleteRows)
        {
            this.context.Set<TblCustomerPaymentsRow>().RemoveRange(deleteRows);
        }

        public IQueryable<TblCustomerPaymentsRow> InvoiceRow(long businessID, IEnumerable<long> datos)
        {
            // Todo: Verificar si el campo balance fue eliminado
            var queryPurchas = (from Pur in context.TblInvoice
                                join Sup in context.TblCustomers on Pur.CustomerId equals Sup.Id
                                where Pur.BranchOfficeId == businessID && datos.Contains(Pur.Id)
                                select new TblCustomerPaymentsRow
                                {
                                    InvoiceId = Pur.Id,
                                    CustomerName = Sup.Names,
                                    CustomerId = (int)Pur.CustomerId,
                                    InvoiceDocument = Pur.DocumentNumber,                                    
                                    Concept = "",
                                    //Balance = Pur.Balance,
                                    Pending = 0,
                                    Owed = 0,
                                    PaidOut = Pur.Total-Pur.Balance,
                                    Payment = 0
                                });
            return queryPurchas;
        }

        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }

      
    }

    public interface ICustomerPaymentsRepository : IQuicksalBase<TblCustomerPayments>
    {
        IQueryable<TblCustomerPayments> GetCustomerPayments(long BusinessId);
        IQueryable<Invoice> GetInvoice(long businessID);
        List<CustomerPayments> GetCustomerPaymentRow(long Id);
        Task<TaskResult> Savechanges();
        IQueryable<TblCustomerPaymentsRow> InvoiceRow(long businessID, IEnumerable<long> datos);
        void DeleteRange(IEnumerable<TblCustomerPaymentsRow> deleteRows);

        decimal ValidarPagado(long Id);
    }
}
