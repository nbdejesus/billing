﻿using QuickSal.DataAcces.Contexts;
using System.Linq;

namespace QuickSal.DataAcces.Repositories
{
    public class PrivilegeRepository : QuicksalRepository<TblPrivileges>, IPrivilegeRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public PrivilegeRepository(QuickSalContext context) : base(context)
        { }

        /// <summary>
        /// Proceso para obtener los privilegios registrados que tiene una empresa
        /// </summary>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        public IQueryable<TblPrivileges> GetPrivileges(long businessId)
        {
            //Dictionary<string, object> parametres = new Dictionary<string, object>();
            //parametres.Add("@BusinessId", 1);

            //var test = new PrcGetAllProducts();
            //var all = context.ExecuteQuerys(test, "prc_GetAllProducts", parametres);
            return context.TblPrivileges.Where(x => x.BusinessId == businessId);
        }

        //public TaskResult AddPrivilegePrincipal(TblBusiness business, int planId)
        //{

        //    //Creando el privilegio principal
        //    TblPrivileges privilege = new TblPrivileges();
        //    privilege.UserId = business.User.Id;
        //    privilege.Name = "Super Usuario";
        //    privilege.CreateDate = DateTime.Now;
        //    privilege.EditDate = DateTime.Now;

        //    //Agreando los objetos permitidos
        //    var objects = context.TblPlanSubObject.Where(x => x.PlanId == planId);
        //    IEnumerable<TblSegurity> segurity = (from c in objects
        //                                         select new TblSegurity
        //                                         {
        //                                             SubObjectId = c.SubObjectId
        //                                         }).ToList();
             
        //    context.TblSegurity.ad
        //    return null;
        //}
    }

    public interface IPrivilegeRepository : IQuicksalBase<TblPrivileges>
    {
        /// <summary>
        /// Proceso para obtener los privilegios registrados que tiene una empresa
        /// </summary>
        /// <param name="businessId">Primary key de la empresa</param>
        /// <returns></returns>
        IQueryable<TblPrivileges> GetPrivileges(long businessId);
    }
}
