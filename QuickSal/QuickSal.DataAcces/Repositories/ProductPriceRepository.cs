﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class ProductPriceRepository : QuicksalRepository<TblProductsPrices>, IProductPriceRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public ProductPriceRepository(QuickSalContext context) : base(context)
        { }

        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return  await Save();
        }
    }

    public interface IProductPriceRepository : IQuicksalBase<TblProductsPrices>
    { }
}
