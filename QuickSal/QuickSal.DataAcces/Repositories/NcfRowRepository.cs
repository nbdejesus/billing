﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{

    public class NcfRowRepository : QuicksalRepository<TblNcfrow>, INcfRowRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public NcfRowRepository(QuickSalContext context) : base(context)
        {

        }

        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }

    }
    public interface INcfRowRepository : IQuicksalBase<TblNcfrow>
    {

        /// <summary>
        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
