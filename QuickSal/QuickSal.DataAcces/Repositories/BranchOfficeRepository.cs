﻿using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.Models;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class BranchOfficeRepository : QuicksalRepository<TblBranchOffice>, IBranchOfficeRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public BranchOfficeRepository(QuickSalContext context) : base(context)
        { }

        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }

    }

    public interface IBranchOfficeRepository : IQuicksalBase<TblBranchOffice>
    {
        Task<TaskResult> Savechanges();
    }
}
