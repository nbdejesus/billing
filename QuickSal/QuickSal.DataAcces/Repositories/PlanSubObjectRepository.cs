﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions.Internal;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.CustomModels.Models.Plan;
using QuickSal.DataAcces.Models;
using Remotion.Linq.Utilities;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class PlanSubObjectRepository : QuicksalRepository<TblPlanSubObject>, IPlanSubObjectRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public PlanSubObjectRepository(QuickSalContext context) : base(context)
        { }
     

        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }

    }

    public interface IPlanSubObjectRepository : IQuicksalBase<TblPlanSubObject>
    {

        /// Metodo para procesar los cambios en la base de datos
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
