﻿using Microsoft.EntityFrameworkCore;
using QuickSal.DataAcces.Contexts;
using QuickSal.DataAcces.CustomModels.Procedure.Customer;
using QuickSal.DataAcces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickSal.DataAcces.Repositories
{
    public class OrderRepository : QuicksalRepository<TblOrders>, IOrderRepository
    {
        private new QuickSalContext context
        {
            get { return base.context as QuickSalContext; }
        }

        public OrderRepository(QuickSalContext context) : base(context)
        {

        }

        /// <summary>
        /// Metodo para obtener una orden
        /// </summary>
        /// <param name="OrderId">Primary key de la Orden</param>
        /// <param name="BanchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        public async Task<TblOrders> GetOrderHeader(long OrderId, long BanchOfficeId)
        {
            return await Task.Run(() => context.TblOrders.Where(x => x.Id == OrderId && x.BranchOfficeId == BanchOfficeId).Include(x => x.Customer)
                .Include(x => x.TblOrdersRow).FirstOrDefault());
        }


        /// <summary>
        /// Metodo para obtener todas las ordenes
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <param name="DocumentStatusId">Estatus de los pedidos a mostrar (opcional)</param>
        /// <param name="startDate">Rango de fecha de inicio (opcional)</param>
        /// <param name="endDate">Rango de fecha fin (opcional)</param>
        /// <returns></returns>
        public async Task<IQueryable<GetOrders>> GetAllOrders(long BranchOfficeId, int? DocumentStatusId = null, DateTime? startDate = null, DateTime? endDate = null)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("@BranchOfficeId", BranchOfficeId);
            parameters.Add("@DocumentStatusId", DocumentStatusId);
            parameters.Add("@StartDate", startDate);
            parameters.Add("@EndDate", endDate);
            return await Task.Run(() => ExecuteQuerys(new GetOrders(), "Customer.GetOrders", parameters));
        }


        /// <summary>
        /// Metodo para obtener el ultimo número de documento generado
        /// </summary>
        /// <returns></returns>
        public string GetLastCode(long businessId)
        {
            var data = context.TblOrders.Where(x => x.BranchOffice.IdBusiness == businessId).LastOrDefault();
            if (data == null)
                return null;
            else
                return data.DocumentNumber;

        }


        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> Savechanges()
        {
            return await Save();
        }
    }

    public interface IOrderRepository : IQuicksalBase<TblOrders>
    {
        /// <summary>
        /// Metodo para obtener una orden
        /// </summary>
        /// <param name="OrdenId">Primary key de la orden</param>
        /// <param name="BanchOfficeId">Primary key de la sucursal</param>
        /// <returns></returns>
        Task<TblOrders> GetOrderHeader(long OrdenId, long BanchOfficeId);

        /// <summary>
        /// Metodo para obtener el ultimo número de documento generado
        /// </summary>
        /// <returns></returns>
        string GetLastCode(long businessId);

        /// <summary>
        /// Metodo para obtener todas las ordenes
        /// </summary>
        /// <param name="BranchOfficeId">Primary key de la sucursal</param>
        /// <param name="DocumentStatusId">Estatus de los pedidos a mostrar (opcional)</param>
        /// <param name="startDate">Rango de fecha de inicio (opcional)</param>
        /// <param name="endDate">Rango de fecha fin (opcional)</param>
        /// <returns></returns>
        Task<IQueryable<GetOrders>> GetAllOrders(long BranchOfficeId, int? DocumentStatusId = null, DateTime? startDate = null, DateTime? endDate = null);

        /// <summary>
        /// Metodo para guardar los cambios del repositorio
        /// </summary>
        /// <returns></returns>
        Task<TaskResult> Savechanges();
    }
}
